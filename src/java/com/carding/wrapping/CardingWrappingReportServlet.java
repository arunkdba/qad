/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.carding.wrapping;

import com.carding.wrapping.*;
import com.common.Common;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 *
 * @author admin
 */
public class CardingWrappingReportServlet extends HttpServlet {
Common common = new Common();
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
             String sFromDate = common.pureDate(request.getParameter("tdatefrom"));
             String sToDate = common.pureDate(request.getParameter("tdateto"));
             String sTestNo = request.getParameter("testno");
             String sOrderNo = common.parseNull(request.getParameter("orderno"));
             String sAbst = common.parseNull(request.getParameter("abst"));
             int iShift         =   common.toInt(common.parseNull(request.getParameter("sshift")));
             String SUnit ="",SShift="",SUser="";;
             int iUnitCode = common.toInt(common.parseNull(request.getParameter("sunits")));
             if(iUnitCode==1){
                 SUnit = "A-UNIT";
             }
             else if(iUnitCode==2){
                 SUnit = "B-UNIT";
             }
             else if(iUnitCode ==10){
                 SUnit = "C-UNIT";
             }
             if(iShift==1){
                 SShift = "Shift-I";
             }
             else if(iShift==2){
                 SShift = "Shift-II";
             }
             else if(iShift ==3){
                 SShift = "Shift-III";
             }else{
                 SShift = "All";
             }
             CardingWrappingTestDetails cardingWrappingTestDetails = new CardingWrappingTestDetails();
             java.util.List theList = cardingWrappingTestDetails.getCardingTestDetails(sFromDate, sToDate, sOrderNo,iUnitCode,iShift);
             if(iShift !=0){
                SUser = cardingWrappingTestDetails.getUser(sFromDate, sToDate, sOrderNo, iUnitCode,iShift);
             }
             CardingWrappingTestPrint crdingWrappingTestPrint = new CardingWrappingTestPrint();
                                      crdingWrappingTestPrint . GeneratePrint(theList, SUser,SUnit,SShift);

             request . setAttribute("theCardingWrappingTestDetails", theList);
             request . setAttribute("TestNo", sTestNo);
             request . setAttribute("Fdate",sFromDate);
             request . setAttribute("Tdate",sToDate);
             request . setAttribute("UnitName", SUnit);
             request . setAttribute("Shift",SShift);

             request.getRequestDispatcher("cardingwrappingreport.jsp").forward(request, response);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
