/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.carding.wrapping;

/**
 *
 * @author admin
 */
public class CardingWrappingTestData {

    private String sSino;
    private String sTestno;
    private String sOrderNo;
    private String smach_name;    
    private String sShade;
    private String sStdHank;
    private String sHank1;
    private String sHank2;
    private String sHank3;
    private String sHank4;
    private String sHankAvg;
    private String sCcd;
    private String sSpeed;
    private String sentrydate;
    private String sstatus;
    private String sifcd;
    private String spascal;
    private String stime;
    
    
    public CardingWrappingTestData() {
    }

    public String getsSino() {
        return sSino;
    }

    public void setsSino(String sSino) {
        this.sSino = sSino;
    }

    public String getsTestno() {
        return sTestno;
    }

    public void setsTestno(String sTestno) {
        this.sTestno = sTestno;
    }

    public String getsOrderNo() {
        return sOrderNo;
    }

    public void setsOrderNo(String sOrderNo) {
        this.sOrderNo = sOrderNo;
    }

    public String getSmach_name() {
        return smach_name;
    }

    public void setSmach_name(String smach_name) {
        this.smach_name = smach_name;
    }
   
    public String getsShade() {
        return sShade;
    }

    public void setsShade(String sShade) {
        this.sShade = sShade;
    }

    public String getsStdHank() {
        return sStdHank;
    }

    public void setsStdHank(String sStdHank) {
        this.sStdHank = sStdHank;
    }

    public String getsHank1() {
        return sHank1;
    }

    public void setsHank1(String sHank1) {
        this.sHank1 = sHank1;
    }

    public String getsHank2() {
        return sHank2;
    }

    public void setsHank2(String sHank2) {
        this.sHank2 = sHank2;
    }

    public String getsHank3() {
        return sHank3;
    }

    public void setsHank3(String sHank3) {
        this.sHank3 = sHank3;
    }

    public String getsHank4() {
        return sHank4;
    }

    public void setsHank4(String sHank4) {
        this.sHank4 = sHank4;
    }

    public String getsHankAvg() {
        return sHankAvg;
    }

    public void setsHankAvg(String sHankAvg) {
        this.sHankAvg = sHankAvg;
    }

    public String getsCcd() {
        return sCcd;
    }

    public void setsCcd(String sCcd) {
        this.sCcd = sCcd;
    }

    public String getsSpeed() {
        return sSpeed;
    }

    public void setsSpeed(String sSpeed) {
        this.sSpeed = sSpeed;
    }
    
    public String getSentrydate() {
        return sentrydate;
    }

    public void setSentrydate(String sentrydate) {
        this.sentrydate = sentrydate;
    }

    public String getSstatus() {
        return sstatus;
    }

    public void setSstatus(String sstatus) {
        this.sstatus = sstatus;
    }

    public String getSifcd() {
        return sifcd;
    }

    public void setSifcd(String sifcd) {
        this.sifcd = sifcd;
    }

    public String getSpascal() {
        return spascal;
    }

    public void setSpascal(String spascal) {
        this.spascal = spascal;
    }

    public String getStime() {
        return stime;
    }

    public void setStime(String stime) {
        this.stime = stime;
    }
    
    
}
