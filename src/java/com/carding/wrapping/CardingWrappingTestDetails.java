/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.carding.wrapping;

import com.carding.wrapping.*;
import com.common.Common;
import com.jdbc.connection.JDBCProcessConnection;
import com.jdbc.connection.JDBCProcessaConnection;

/**
 *
 * @author admin
 */
public class CardingWrappingTestDetails {

    java.sql.Connection theProcessConnection  = null;
    java.sql.Connection theProcessaConnection  = null;
    Common common;

    public CardingWrappingTestDetails() {
        common = new Common();
    }

    public java.util.List getCardingTestDetails(String SFromDate,String SToDate,String sOrderNo,int iUnitCode,int iShift){
        java.util.List theList = new java.util.ArrayList();
        StringBuffer sb = new StringBuffer();
        //System.out.println("iUnitCode->"+iUnitCode);
                     sb . append(" ");

                     if(iUnitCode == 1){
                         // A Unit
                        sb . append(" SELECT PRO_CARDING_HANK_DETAILS.TEST_NO, MACHINE.MACH_NAME, PROCESSINGOFINDENT.RORDERNO, PROCESSINGOFINDENT.YSHNM ");
                        sb . append(" ,AUNIT_ORDERCARDINGSETTING_new.CARDHANK||'-'||AUNIT_ORDERCARDINGSETTING_new.CARDHANK as SettingHank  ");
                        sb . append(" , to_char(PRO_CARDING_HANK_DETAILS.ENTRYDATE,'DD.MM.YYYY HH24:MI') as dateTime, PRO_CARDING_HANK_DETAILS.CCD, PRO_CARDING_HANK_DETAILS.SPEED ");
                        sb . append(" , PRO_CARDING_HANK_STATUS.STATUS,PRO_CARDING_HANK_DETAILS.IFCD,PRO_CARDING_HANK_DETAILS.PASCAL ");
                        sb . append(" , to_char(PRO_CARDING_HANK_DETAILS.ENTRYDATE,'HH24:MI') as Time FROM PROCESS.PRO_CARDING_HANK_DETAILS ");
                        sb . append(" INNER JOIN MACHINE ON MACHINE.MACH_CODE = PRO_CARDING_HANK_DETAILS.MACH_CODE AND MACHINE.UNIT_CODE="+iUnitCode+" AND MACHINE.DEPT_CODE = 3 ");
                        sb . append(" INNER JOIN PROCESSINGOFINDENT ON PROCESSINGOFINDENT.RORDERNO = PRO_CARDING_HANK_DETAILS.RORDERNO ");
                        sb . append(" INNER JOIN AUNIT_ORDERCARDINGSETTING_new ON AUNIT_ORDERCARDINGSETTING_new.RORDERNO = PRO_CARDING_HANK_DETAILS.RORDERNO AND AUNIT_ORDERCARDINGSETTING_new.SETTINGTYPE = 0 ");
                        sb . append(" LEFT JOIN PROCESS.PRO_CARDING_HANK_STATUS ON PRO_CARDING_HANK_STATUS.CODE = PROCESS.PRO_CARDING_HANK_DETAILS.TESTSTATUS ");
                        sb . append(" WHERE to_char(PRO_CARDING_HANK_DETAILS.ENTRYDATE,'YYYYMMDD')>= ?  AND to_char(PRO_CARDING_HANK_DETAILS.ENTRYDATE,'YYYYMMDD')<= ? ");
                        if(!sOrderNo.equals("")){
                            sb.append(" AND PROCESSINGOFINDENT.RORDERNO = '"+sOrderNo+"' ");
                        }
                        if(iShift !=0){
                            sb . append(" AND PRO_CARDING_HANK_DETAILS.SHIFT ="+iShift+"");
                        }
                        sb . append(" ORDER BY 1,2 ");
                     }
                     if(iUnitCode == 2){
                         // B Unit
                        sb . append(" SELECT PRO_CARDING_HANK_DETAILS.TEST_NO, MACHINE.MACH_NAME, PROCESSINGOFINDENT.RORDERNO, PROCESSINGOFINDENT.YSHNM ");
                        sb . append(" ,ORDERCARDINGSETTING.CARDHANK||'-'||ORDERCARDINGSETTING.CARDHANK as SettingHank ");
                        sb . append(" , to_char(PRO_CARDING_HANK_DETAILS.ENTRYDATE,'DD.MM.YYYY HH24:MI') as dateTime, PRO_CARDING_HANK_DETAILS.CCD, PRO_CARDING_HANK_DETAILS.SPEED ");
                        sb . append(" , PRO_CARDING_HANK_STATUS.STATUS,PRO_CARDING_HANK_DETAILS.IFCD,PRO_CARDING_HANK_DETAILS.PASCAL ");
                        sb . append(" , to_char(PRO_CARDING_HANK_DETAILS.ENTRYDATE,'HH24:MI') as Time FROM PROCESS.PRO_CARDING_HANK_DETAILS ");
                        sb . append(" INNER JOIN MACHINE ON MACHINE.MACH_CODE = PRO_CARDING_HANK_DETAILS.MACH_CODE AND MACHINE.UNIT_CODE="+iUnitCode+" AND MACHINE.DEPT_CODE = 3 ");
                        sb . append(" INNER JOIN PROCESSINGOFINDENT ON PROCESSINGOFINDENT.RORDERNO = PRO_CARDING_HANK_DETAILS.RORDERNO ");
                        sb . append(" INNER JOIN ORDERCARDINGSETTING ON ORDERCARDINGSETTING.RORDERNO = PRO_CARDING_HANK_DETAILS.RORDERNO AND ORDERCARDINGSETTING.SETTINGTYPE = 0 ");
                        sb . append(" LEFT JOIN PRO_CARDING_HANK_STATUS ON PRO_CARDING_HANK_STATUS.CODE = PRO_CARDING_HANK_DETAILS.TESTSTATUS ");
                        sb . append(" WHERE to_char(PRO_CARDING_HANK_DETAILS.ENTRYDATE,'YYYYMMDD')>= ?  AND to_char(PRO_CARDING_HANK_DETAILS.ENTRYDATE,'YYYYMMDD')<= ? ");
                        if(!sOrderNo.equals("")){
                            sb.append(" AND PROCESSINGOFINDENT.RORDERNO = '"+sOrderNo+"' ");
                        }
                        if(iShift !=0){
                            sb . append(" AND PRO_CARDING_HANK_DETAILS.SHIFT ="+iShift+"");
                        }
                        sb . append(" ORDER BY 1,2 ");
                     }
                     if(iUnitCode == 10){ // C Unit
                        sb . append(" SELECT PRO_CARDING_HANK_DETAILS.TEST_NO, MACHINE.MACH_NAME, CUNIT_PROCESSGOFINDENT.RORDERNO, CUNIT_PROCESSGOFINDENT.YSHNM ");
                        sb . append(" ,CUNIT_ORDCARDINGSETTING.CARDHANK||'-'||CUNIT_ORDCARDINGSETTING.CARDHANK as SettingHank ");
                        sb . append(" , to_char(PRO_CARDING_HANK_DETAILS.ENTRYDATE,'DD.MM.YYYY HH24:MI') as dateTime, PRO_CARDING_HANK_DETAILS.CCD, PRO_CARDING_HANK_DETAILS.SPEED ");
                        sb . append(" , PRO_CARDING_HANK_STATUS.STATUS,PRO_CARDING_HANK_DETAILS.IFCD,PRO_CARDING_HANK_DETAILS.PASCAL ");
                        sb . append(" , to_char(PRO_CARDING_HANK_DETAILS.ENTRYDATE,'HH24:MI') as Time FROM PROCESS.PRO_CARDING_HANK_DETAILS ");
                        sb . append(" INNER JOIN MACHINE ON MACHINE.MACH_CODE = PRO_CARDING_HANK_DETAILS.MACH_CODE AND MACHINE.UNIT_CODE in(3,10) AND MACHINE.DEPT_CODE = 3 ");
                        sb . append(" INNER JOIN PROCESS.CUNIT_PROCESSGOFINDENT ON CUNIT_PROCESSGOFINDENT.RORDERNO = PRO_CARDING_HANK_DETAILS.RORDERNO ");
                        sb . append(" INNER JOIN PROCESS.CUNIT_ORDCARDINGSETTING ON CUNIT_ORDCARDINGSETTING.RORDERNO = PRO_CARDING_HANK_DETAILS.RORDERNO AND CUNIT_ORDCARDINGSETTING.SETTINGTYPE = 0 ");
                        sb . append(" And CUNIT_ORDCARDINGSETTING.MIXNO = 1 ");
                        sb . append(" LEFT JOIN PRO_CARDING_HANK_STATUS ON PRO_CARDING_HANK_STATUS.CODE = PRO_CARDING_HANK_DETAILS.TESTSTATUS ");
                        sb . append(" WHERE to_char(PRO_CARDING_HANK_DETAILS.ENTRYDATE,'YYYYMMDD')>= ?  AND to_char(PRO_CARDING_HANK_DETAILS.ENTRYDATE,'YYYYMMDD')<= ? ");
                        if(!sOrderNo.equals("")){
                            sb.append(" AND CUNIT_PROCESSGOFINDENT.RORDERNO = '"+sOrderNo+"' ");
                        }
                        if(iShift !=0){
                            sb . append(" AND PRO_CARDING_HANK_DETAILS.SHIFT ="+iShift+"");
                        }
                        sb . append(" ORDER BY 1,2 ");
                     }
                     System.out.println("Qry-->"+sb.toString());
        try{
            java.sql.PreparedStatement pst = null;
            if(iUnitCode ==1){
                if(theProcessaConnection==null){
                        JDBCProcessaConnection  jdbc = JDBCProcessaConnection.getJDBCConnection();
                        theProcessaConnection = jdbc . getConnection();
                 }
                pst = theProcessaConnection.prepareStatement(sb.toString());
            }else{
                if(theProcessConnection==null){
                        JDBCProcessConnection  jdbc = JDBCProcessConnection.getJDBCConnection();
                        theProcessConnection = jdbc . getConnection();
                 }
                pst = theProcessConnection.prepareStatement(sb.toString());
            }
                                        pst . setString(1,SFromDate);
                                        pst . setString(2,SToDate);
             java.sql.ResultSet   rst = pst . executeQuery();
             CardingWrappingTestData cardingWrappingTestData = null;
             int isino = 0;
             String[] SWeight = null;
             double dAvgHank = 0.0;
             while(rst.next()){
                 cardingWrappingTestData = new CardingWrappingTestData();
                 cardingWrappingTestData . setsSino(String.valueOf(++isino));
                 cardingWrappingTestData . setsTestno(rst.getString(1));
                 cardingWrappingTestData . setSmach_name(rst.getString(2));
                 cardingWrappingTestData . setsOrderNo(rst.getString(3));
                 cardingWrappingTestData . setsShade(rst.getString(4));
                 cardingWrappingTestData . setsStdHank(common.parseNull(rst.getString(5)));

                 SWeight = getSliverWeightDetails(rst.getString(1));
                 cardingWrappingTestData . setsHank1(common.parseNull((String)SWeight[0]));
                 cardingWrappingTestData . setsHank2(common.parseNull((String)SWeight[1]));
                 cardingWrappingTestData . setsHank3(common.parseNull((String)SWeight[2]));
                 cardingWrappingTestData . setsHank4(common.parseNull((String)SWeight[3]));
                 dAvgHank = 0;
                 dAvgHank = (common.toDouble(SWeight[0])+common.toDouble(SWeight[1])+common.toDouble(SWeight[2])+common.toDouble(SWeight[3]))/4;
                 cardingWrappingTestData . setsHankAvg(common.parseNull(String.valueOf(common.getRound(dAvgHank,3))));

                 cardingWrappingTestData . setSentrydate(rst.getString(6));
                 cardingWrappingTestData . setsCcd(common.parseNull(rst.getString(7)));
                 cardingWrappingTestData . setsSpeed(common.parseNull(rst.getString(8)));
                 cardingWrappingTestData . setSstatus(common.parseNull(rst.getString(9)));
                 cardingWrappingTestData . setSifcd(common.parseNull(rst.getString(10)));
                 cardingWrappingTestData . setSpascal(common.parseNull(rst.getString(11)));
                 cardingWrappingTestData . setStime(common.parseNull(rst.getString(12)));

                 theList.add(cardingWrappingTestData);
             }
             rst.close();
             rst = null;
             pst.close();
             pst = null;

        }catch(Exception ex){
            ex.printStackTrace();
        }

        return theList;
    }

    private String[] getSliverWeightDetails(String STestNo){
        String[] SWeight = null;
        StringBuffer sb = new StringBuffer();
                     sb . append(" SELECT  WEIGHT FROM PROCESS.CARDING_SLIVER_WEIGHT_DETAILS  WHERE TEST_NO = ? ");
        try{
            if(theProcessConnection==null){
                    JDBCProcessConnection  jdbc = JDBCProcessConnection.getJDBCConnection();
                    theProcessConnection = jdbc . getConnection();
             }
             java.sql.PreparedStatement pst = theProcessConnection.prepareStatement(sb.toString());
                                        pst . setString(1,STestNo);
             java.sql.ResultSet   rst = pst . executeQuery();
                SWeight = new String[4];
                int icnt = 0;
             while(rst.next()){

                 if(icnt<4) {
                    SWeight[icnt] = getHank(rst.getString(1)); 
                    //SWeight[icnt] = rst.getString(1);
                 }
                 icnt++;
             }
             rst.close();
             rst = null;
             pst.close();
             pst = null;

        }catch(Exception ex){
            ex.printStackTrace();
        }
        return SWeight;
    }

    private String getHank(String SWeight){
        return common.getRound((3.24/common.toDouble(common.parseNull((String)SWeight))),3); //Hank Calculation
    }
    public String getUser (String SFromDate,String SToDate,String sOrderNo,int iUnitCode,int iShift){
        StringBuffer sb = new StringBuffer();
        String sUser="";
        java.sql.PreparedStatement pst = null;

                        if(iUnitCode==1) {

                            sb . append(" SELECT wm_concat(DISTINCT onetouchemployee.DISPLAYNAME) as UserName FROM PRO_CARDING_HANK_DETAILS ");
                            sb . append(" LEFT JOIN onetouchemployee ON onetouchemployee.Empcode =  PRO_CARDING_HANK_DETAILS.ENTRY_UCODE ");
                            sb . append(" WHERE to_char(PRO_CARDING_HANK_DETAILS.ENTRYDATE,'YYYYMMDD')>= ?  AND to_char(PRO_CARDING_HANK_DETAILS.ENTRYDATE,'YYYYMMDD')<= ? ");
                            
                            if(iShift !=0){
                                 sb . append(" AND PRO_CARDING_HANK_DETAILS.SHIFT = "+iShift+"");
                                 sb . append(" AND PRO_CARDING_HANK_DETAILS.UNIT_CODE = "+iUnitCode+"");
                                //sb . append(" AND getcurrentshift(to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'HH24'),to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'MI'))="+iShift+"");
                            }
                        }    
                        else{     
        
                            sb . append(" SELECT wm_concat(DISTINCT QC_USER.USERNAME) as UserName FROM PRO_CARDING_HANK_DETAILS ");
                            sb . append(" LEFT JOIN QC_USER ON QC_USER.USERCODE =  PRO_CARDING_HANK_DETAILS.ENTRY_UCODE ");
                            sb . append(" WHERE to_char(PRO_CARDING_HANK_DETAILS.ENTRYDATE,'YYYYMMDD')>= ?  AND to_char(PRO_CARDING_HANK_DETAILS.ENTRYDATE,'YYYYMMDD')<= ? ");                        
                            if(iShift !=0){
                                sb . append(" AND PRO_CARDING_HANK_DETAILS.SHIFT = "+iShift+"");
                                sb . append(" AND PRO_CARDING_HANK_DETAILS.UNIT_CODE = "+iUnitCode+"");
                            }
                        }
                        try
                        {
                            if(theProcessConnection==null){
                            JDBCProcessConnection  jdbc = JDBCProcessConnection.getJDBCConnection();
                            theProcessConnection = jdbc . getConnection();
                            }
                            pst = theProcessConnection.prepareStatement(sb.toString());
                            pst . setString(1,SFromDate);
                                        pst . setString(2,SToDate);
                            java.sql.ResultSet   rst = pst . executeQuery();
                            while(rst.next()){
                                sUser = common.parseNull(rst.getString(1));
                            }
                            rst.close();
                            pst.close();
                        }
                        catch(Exception ex){
                            ex.printStackTrace();
                        }
        return sUser;
    }
}

//select count(*) from  PRO_CARDING_HANK_DETAILS where getcurrentshiftnew(to_char(PRO_CARDING_HANK_DETAILS.ENTRYDATE,'HH24'),to_char(PRO_CARDING_HANK_DETAILS.ENTRYDATE,'MI'))=1 and SHIFT=0
//update PRO_CARDING_HANK_DETAILS set SHIFT=3 where getcurrentshiftnew(to_char(PRO_CARDING_HANK_DETAILS.ENTRYDATE,'HH24'),to_char(PRO_CARDING_HANK_DETAILS.ENTRYDATE,'MI'))=3 and SHIFT=0