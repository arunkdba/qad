/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.common;


/**
 *
 * @author sjv
 */

import java.io.*;
import java.util.*;
import java.sql.*;
import java.net.*;

public class Common implements Serializable
{
     public String bgColor,bgHead,bgUom,bgBody,fgHead,fgUom,fgBody,tbgBody;
     HashMap theMap;

     String S0To9[]   = {"Zero ","One ","Two ","Three ","Four ","Five ","Six ","Seven ","Eight ","Nine "};
     String S10To19[] = {"Ten ","Eleven ","Twelve ","Thirteen ","Fourteen ","Fifteen ","Sixteen ","Seventeen ","Eightteen ","Nineteen "};
     String TensArr[] = {"Twenty ","Thirty ","Forty ","Fifty ","Sixty ","Seventy ","Eighty ","Ninety "};

     public Common()
     {
          bgColor = "#FEFCD8";
          bgHead  = "#DFD9F2";     // "#000080";
          bgUom   = "#FFB0B0";     // "#CCCCFF";
          bgBody  = "#9DECFF";
          fgHead  = "#000080";     // "#00FFFF";
          fgUom   = "#000080";
          fgBody  = "#000080";
          tbgBody = "#b0c4de";
         setMonthNames();
     }

     public String getRound(double dAmount,int iScale)
     {
          try
          {
               java.math.BigDecimal bd1 = new java.math.BigDecimal(String.valueOf(dAmount));
               return (bd1.setScale(iScale,4)).toString();
          }
          catch(Exception ex){}
          return "0";
     }

     public String getUpperRound(double dAmount,int iScale)
     {
          try
          {
               java.math.BigDecimal bd1 = new java.math.BigDecimal(String.valueOf(dAmount));
               return (bd1.setScale(iScale,3)).toString();
          }
          catch(Exception ex){}
          return "0";
     }

     public String getRound(String SAmount,int iScale)
     {
          try
          {
                  SAmount = SAmount.trim();
                  java.math.BigDecimal bd1 = new java.math.BigDecimal(SAmount);
                  return (bd1.setScale(iScale,4)).toString();
          }
          catch(Exception ex){}
          return "0";
     }

     public String Pad(String Str,int Len)
     {
            Str = ""+Str;
            if(Str.startsWith("null"))
               return Space(Len);

            if(Str.length() > Len)
                  return Str.substring(0,Len);
            else
                  return Str+Space(Len-Str.length());
     }

     public String Rad(String Str,int Len)
     {

          Str = ""+Str;
          if(Str.startsWith("null"))
               return Space(Len);

          if(Str.length() > Len)
               return Str.substring(0,Len);
          else
               return Space(Len-Str.length())+Str;
     }

     public String Space(int Len)
     {
            String RetStr="";
            for(int index=0;index<Len;index++)
                  RetStr=RetStr+" ";
            return RetStr;
     }

     public String Replicate(String Str,int Len)
     {
            String RetStr="";
            for(int index=0;index<Len;index++)
                  RetStr = RetStr+Str;
            return RetStr;
     }

     public String parseNull(String str)
     {
          String RetStr=str;
          str = ""+str;
          if(str.startsWith("null"))
               return "";
          return RetStr;
     }

     public String Cad(String str,int Len)
     {
          if(Len < str.length())
               return Pad(str,Len);

          String RetStr="";
          int FirstLen = (Len-str.length())/2;
          int SecondLen = Len-FirstLen;
          return Pad(Space(FirstLen)+str,Len);
     }

     public String parseDate(String str)
     {
          str=parseNull(str);
          if(str.length()!=8)
               return str;
          try
          {
               return str.substring(6,8)+"."+str.substring(4,6)+"."+str.substring(0,4);
          }
          catch(Exception ex){}
          return "";
     }
     public int getNextMonth(int iMonth)
     {
		iMonth = toInt(String.valueOf(iMonth).substring(0,6));
		String SMonth = String.valueOf(iMonth);
		String STmp = "";
		int iYear = toInt(SMonth.substring(0,4));
		int iNextMonth = iMonth+1;
		if(String.valueOf(iNextMonth).substring(4,6).equals("13"))
			STmp=String.valueOf(iYear+1)+"01";
		else
			STmp=String.valueOf(iNextMonth);
		return toInt(STmp);
     }

     public String pureDate(String str)
     {
          str=parseNull(str);
          if(str.length()!=10)
               return "0";
          try
          {
               return str.substring(6,10)+str.substring(3,5)+str.substring(0,2);
          }
          catch(Exception ex){}
          return "";
     }

     public double toDouble(String str)
     {
          str = parseNull(str);
          try
          {
               return Double.parseDouble(str.trim());
          }
          catch(Exception ex)
          {
               return 0;
          }
     }

     public int toInt(String str)
     {
          str = parseNull(str);
          try
          {
               return Integer.parseInt(str);
          }
          catch(Exception ex)
          {
               return 0;
          }
     }
     
     public float toFloat(String str)
     {
          str = parseNull(str);
          try
          {
               return Float.parseFloat(str);
          }
          catch(Exception ex)
          {
               return 0;
          }
     }

     public String getDateDiff(String str1,String str2)
     {
            int iYear1,iMonth1,iDay1;
            int iYear2,iMonth2,iDay2;

            try
            {
                  iYear1  = toInt(str1.substring(6,10))-1900;
                  iMonth1 = toInt(str1.substring(3,5))-1;
                  iDay1   = toInt(str1.substring(0,2));
            }
            catch(Exception ex)
            {
                  return "";
            }
            try
            {
                  iYear2  = toInt(str2.substring(6,10))-1900;
                  iMonth2 = toInt(str2.substring(3,5))-1;
                  iDay2   = toInt(str2.substring(0,2));
            }
            catch(Exception ex)
            {
                  return "";
            }
            java.util.Date dt1 = new java.util.Date(iYear1,iMonth1,iDay1);
            java.util.Date dt2 = new java.util.Date(iYear2,iMonth2,iDay2);
            return String.valueOf((dt1.getTime()-dt2.getTime())/(24*60*60*1000));
     }

     public int getDateDiff(int int1,int int2)
     {
            String str1 = "";
            String str2 = "";

            str1 = parseDate(String.valueOf(int1));
            str2 = parseDate(String.valueOf(int2));

            int iYear1,iMonth1,iDay1;
            int iYear2,iMonth2,iDay2;

            try
            {
                  iYear1  = toInt(str1.substring(6,10))-1900;
                  iMonth1 = toInt(str1.substring(3,5))-1;
                  iDay1   = toInt(str1.substring(0,2));
            }
            catch(Exception ex)
            {
                  return 0;
            }
            try
            {
                  iYear2  = toInt(str2.substring(6,10))-1900;
                  iMonth2 = toInt(str2.substring(3,5))-1;
                  iDay2   = toInt(str2.substring(0,2));
            }
            catch(Exception ex)
            {
                  return 0;
            }

            java.util.Date dt1 = new java.util.Date(iYear1,iMonth1,iDay1);
            java.util.Date dt2 = new java.util.Date(iYear2,iMonth2,iDay2);
            return toInt(String.valueOf((dt1.getTime()-dt2.getTime())/(24*60*60*1000)));
     }


     public String getDate(String str1,long days,int Sig)
     {
            String SDay="",SMonth="",SYear="";
            int iYear1=0,iMonth1=0,iDay1=0;
            try
            {
               iYear1  = toInt(str1.substring(6,10))-1900;
               iMonth1 = toInt(str1.substring(3,5))-1;
               iDay1   = toInt(str1.substring(0,2));
            }
            catch(Exception ex)
            {
               return "0";
            }
            java.util.Date dt1 = new java.util.Date(iYear1,iMonth1,iDay1);

            long num1 = dt1.getTime();
            long num2 = (days*24*60*60*1000);
            if (Sig == -1)
                  dt1.setTime(num1-num2);
            else
                  dt1.setTime(num1+num2);

            int iDay   = dt1.getDate();
            int iMonth = dt1.getMonth()+1;
            int iYear  = dt1.getYear()+1900;

            SDay   = (iDay<10 ?"0"+iDay:""+iDay);
            SMonth = (iMonth<10 ?"0"+iMonth:""+iMonth);
            SYear  = ""+iYear;
            return SDay+"."+SMonth+"."+SYear;
     }

     public int getNextDate(int int1)
     {
            String str1 = parseDate(String.valueOf(int1));
            String SDay="",SMonth="",SYear="";
            int iYear1=0,iMonth1=0,iDay1=0;
            try
            {
               iYear1  = toInt(str1.substring(6,10))-1900;
               iMonth1 = toInt(str1.substring(3,5))-1;
               iDay1   = toInt(str1.substring(0,2));
            }
            catch(Exception ex)
            {
               return 0;
            }
            java.util.Date dt1 = new java.util.Date(iYear1,iMonth1,iDay1);

            long num1 = dt1.getTime();
            long num2 = (24*60*60*1000);
             dt1.setTime(num1+num2);

            int iDay   = dt1.getDate();
            int iMonth = dt1.getMonth()+1;
            int iYear  = dt1.getYear()+1900;

            SDay   = (iDay<10 ?"0"+iDay:""+iDay);
            SMonth = (iMonth<10 ?"0"+iMonth:""+iMonth);
            SYear  = ""+iYear;
            return toInt(SYear+SMonth+SDay);
     }

     public String getCurrentDate()
     {
            String SDay,SMonth,SYear;

            java.util.Date dt    = new java.util.Date();
            int iDay   = dt.getDate();
            int iMonth = dt.getMonth()+1;
            int iYear  = dt.getYear()+1900;

            if(iDay < 10)
               SDay = "0"+iDay;
            else
               SDay = String.valueOf(iDay);

            if(iMonth < 10)
               SMonth = "0"+iMonth;
            else
               SMonth = String.valueOf(iMonth);

            SYear = String.valueOf(iYear);

            return(SYear+SMonth+SDay);
     }

     public int indexOf(Vector vect,String str)
     {
           int iindex=-1;
           str = str.trim();
           for(int i=0;i<vect.size();i++)
           {
                  String str1 = (String)vect.elementAt(i);
                  str1 = str1.trim();
                  if(str1.startsWith(str))
                        return i;
           }
           return iindex;
     }

     // SOp - operation : "S" - SUM; "A" - Average
     public String getSum(Vector Vx,String SOp)
     {

          double dAmount=0.0;
          for(int i=0;i<Vx.size();i++)
          {
               dAmount = dAmount+toDouble((String)Vx.elementAt(i));
          }
          if(SOp.equals("A"))
          {
               dAmount = dAmount/Vx.size();
          }
          return getRound(dAmount,3);
     }


     public String getDate()
     {
          java.util.Date date = new java.util.Date();

          int iDay   = date.getDate();
          int iMonth = date.getMonth()+1;
          String sYear  = String.valueOf(date.getYear()+1900);

          String sDay    = (iDay>=10?""+iDay:"0"+iDay);
          String sMonth  = (iMonth>=10?""+iMonth:"0"+iMonth);

          String sDate  = sYear+sMonth+sDay;

          return sDate;
     }
     public String getTime()
     {
          java.util.Date dt        = new java.util.Date();

          int iHrs       = dt.getHours();
          int iMin       = dt.getMinutes();

          String sHrs    =(iHrs<10?"0"+iHrs:""+iHrs);
          String sMin    =(iMin<10?"0"+iMin:""+iMin);

          return sHrs+":"+sMin;
     }
     public String getUser()
     {
          return "";
     }
     public String getFinancePeriod()
     {
          return "";
     }

     public String stuff(String str,int iLen)
     {

          for(;str.length()<iLen;)
          {
               str="0"+str;
          }
          return str;
     }
     public String getMonthName(int iMonth)
     {
                String SArr[] = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
				String SMonth = null;
                try
                {
                    SMonth = String.valueOf(iMonth);
                    iMonth = toInt(SMonth.substring(4,6))-1;
                    int iYear  = toInt(SMonth.substring(0,4));
                    SMonth = SArr[iMonth]+" "+iYear;
                }
                catch(Exception ex)
                {
                    return "";
                }
                return SMonth;
      }

     private void setMonthNames()
     {
          theMap = new HashMap();
          theMap.put("01","JAN");
          theMap.put("02","FEB");
          theMap.put("03","MAR");
          theMap.put("04","APR");
          theMap.put("05","MAY");
          theMap.put("06","JUN");
          theMap.put("07","JUL");
          theMap.put("08","AUG");
          theMap.put("09","SEP");
          theMap.put("10","OCT");
          theMap.put("11","NOV");
          theMap.put("12","DEC");
     }

     public String getMonthYear(int iYearMonth)
     {
          String str = "";
          String SYearMonth=String.valueOf(iYearMonth);
          str = theMap.get(SYearMonth.substring(4,6))+"_"+SYearMonth.substring(0,4);
          return str;
     }

     public int getNextMonthYear(int iYearMonth)
     {
          int nextMonth = 0;
          String SYearMonth=String.valueOf(iYearMonth);
          String SYear  = SYearMonth.substring(0,4);
          String SMonth = SYearMonth.substring(4,6);

          if(toInt(SMonth)>0 && toInt(SMonth)<9)
               nextMonth=toInt( SYear +"0"+String.valueOf( toInt(SMonth)+1 ) );
          else if(toInt(SMonth)>8 && toInt(SMonth)<12)
               nextMonth=toInt( SYear + String.valueOf( toInt(SMonth)+1 ) );
          else if(toInt(SMonth)==12)
               nextMonth=toInt( String.valueOf(toInt(SYear)+1) + "01");

          return nextMonth;
     }

     public String getRupee(String str)
     {
          str = getRound(str,2);
          int  lRupee=0;
          int  iPaise=0;
          java.util.StringTokenizer ST = new java.util.StringTokenizer(str,".");
          while(ST.hasMoreTokens())
          {
               lRupee=toInt(ST.nextToken());
               iPaise=toInt(ST.nextToken());
          }
          String SRs = (lRupee > 0 ? getCrores(lRupee):"Zero");
          String SPs = (iPaise > 0 ? " and paise "+getTens(iPaise):"");
          return "Rupees "+SRs+SPs+"Only";
     }

     private String getCrores(int i)
     {
          if(i<10000000)
               return getLacs(i);
          String str="";
          int j=0,k=0;
          if(i<=99999999)
          {
               str   = String.valueOf(i);
               j        = toInt(str.substring(0,1));
               k        = toInt(str.substring(1,8));
               return getSingle(j)+"Crore "+getLacs(k);
          }
          str   = String.valueOf(i);
          j        = toInt(str.substring(0,2));
          k        = toInt(str.substring(2,9));
          return getTens(j)+"Crore "+getLacs(k);
     }

     private String getLacs(int i)
     {
          if(i<100000)
               return getThousands(i);

          String str="";
          int j=0,k=0;
          if(i<=999999)
          {
               str   = String.valueOf(i);
               j        = toInt(str.substring(0,1));
               k        = toInt(str.substring(1,6));
               return getSingle(j)+"Lac "+getThousands(k);
          }
          str   = String.valueOf(i);
          j        = toInt(str.substring(0,2));
          k        = toInt(str.substring(2,7));
          return getTens(j)+"Lac "+getThousands(k);
     }

     private String getThousands(int i)
     {
          if(i<1000)
               return getHundreds(i);

          String str="";
          int j=0,k=0;
          if(i<=9999)
          {
               str   = String.valueOf(i);
               j        = toInt(str.substring(0,1));
               k        = toInt(str.substring(1,4));
               return getSingle(j)+"Thousand "+getHundreds(k);
          }
          str   = String.valueOf(i);
          j        = toInt(str.substring(0,2));
          k        = toInt(str.substring(2,5));
          return getTens(j)+"Thousand "+getHundreds(k);
     }

     private String getHundreds(int i)
     {
          if(i<100)
               return getTens(i);

          String str="";
          int j=0,k=0;
          str   = String.valueOf(i);
          j        = toInt(str.substring(0,1));
          k        = toInt(str.substring(1,3));
          return getSingle(j)+"Hundred "+getTens(k);
     }

     private String getTens(int i)
     {
          if(i==0)
               return "";
          if(i >= 0 && i <= 9)
               return getSingle(i);
          if(i >= 10 && i <= 19)
               return S10To19[i-10];

          String str   = String.valueOf(i);
          int j        = toInt(str.substring(0,1))-2;
          int k        = toInt(str.substring(1,2));
          return TensArr[j]+(k>0?getSingle(k):"");
     }

     private String getSingle(int i)
     {
          return S0To9[i];
     }

     public static void main(String[] args)
     {
          Common common = new Common();
          //System.out.println(common.parseDate(common.getCurrentDate()));
          try
          {

          String Sname = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
          String myString = Sname.substring(11,14);
          System.out.println(myString);

          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }

     public String getServerDate() throws Exception
     {

          Connection    theConnection = null;
          String        SServerDate   = "";

          if(theConnection == null)
          {
                Class           . forName("oracle.jdbc.OracleDriver");
                theConnection   = java.sql.DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","hrdnew","hrdnew");
          }

          Statement stat   = theConnection.createStatement();
          ResultSet result = stat.executeQuery(getDateQS());
          if(result.next())
               SServerDate = result.getString(1);
          result.close();
          stat.close();
          return SServerDate;
     }
     
    public String getLotname(String lotno) throws Exception
     {

          Connection    theConnection = null;
          String        lotname   = "";

          if(theConnection == null)
          {
                Class           . forName("oracle.jdbc.OracleDriver");
                theConnection   = java.sql.DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","process","process");
          }

          Statement stat   = theConnection.createStatement();
          ResultSet result = stat.executeQuery("select getlotname('"+lotno+"') from dual ");
          if(result.next())
               lotname = result.getString(1);
          result.close();
          stat.close();
          return lotname;
     }

     
     public String getServerTime() throws Exception
     {

          Connection theConnection = null;
          String SServerTime       = "";

          if(theConnection == null)
          {
                Class           . forName("oracle.jdbc.OracleDriver");
                theConnection   = java.sql.DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","hrdnew","hrdnew");
          }

          Statement stat   = theConnection.createStatement();
          ResultSet result = stat.executeQuery(getTimeQS());
          if(result.next())
               SServerTime = result.getString(1);
          result.close();
          stat.close();
          return SServerTime;
     }


     public String getSerDateTime()
     {
          Connection theConnection = null;

          String SDate = "";
          try
          {
               String QS = "Select to_Char(Sysdate,'DD.MM.YYYY HH24:MI:SS') From Dual";
               if(theConnection == null)
               {
                Class           . forName("oracle.jdbc.OracleDriver");
                theConnection   = java.sql.DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.28:1521:arun","hrdnew","hrdnew");
               }
               Statement stat   = theConnection.createStatement();

               ResultSet res    = stat.executeQuery(QS);
               while(res.next())
               {
                    SDate = res.getString(1);
               }
               res.close();
               stat.close();
          }
          catch(Exception e)
          {
               e.printStackTrace();
          }
          return SDate;
     }
     private String getTimeQS()
     {
          String QS = "Select to_Char(sysdate,'hh24:mi:ss') from Dual";
          return QS;
     }


     private String getDateQS()
     {
          String QS = "Select to_Char(sysdate,'yyyymmdd') from Dual";
          return QS;
     }

     public String getPreviousDate(String SDate)
     {
          return pureDate(getDate(parseDate(SDate),1,-1));
     }

     public String setDueDays(String SDate)
     {
          java.util.Date dt    = new java.util.Date();

          int iDay   = dt.getDate()+toInt(SDate);
          int iMonth = dt.getMonth()+1;
          int iYear  = dt.getYear()+1900;

          String SDay,SMonth,SYear;

          if(iDay < 10)
               SDay = "0"+iDay;
          else
               SDay = String.valueOf(iDay);

          if(iMonth < 10)
               SMonth = "0"+iMonth;
          else
               SMonth = String.valueOf(iMonth);

          SYear = String.valueOf(iYear);

          java.util.Date dt1 = new java.util.Date(toInt(SYear)-1900,toInt(SMonth)-1,toInt(SDay));

          int iDay1  = dt1.getDate();
          int iMonth1= dt1.getMonth()+1;
          int iYear1 = dt1.getYear()+1900;

          String SDay1,SMonth1,SYear1;

          SDay1  = (iDay1<10 ?"0"+iDay1:""+iDay1);
          SMonth1= (iMonth1<10 ?"0"+iMonth1:""+iMonth1);
          SYear1 = ""+iYear1;

          String SDate1 = SYear1+SMonth1+SDay1;

          return SDate1;
     }

     public String getDate1(String str1,long days,int Sig)
     {
          String SDay="",SMonth="",SYear="";
          int iYear1=0,iMonth1=0,iDay1=0;
          try
          {
               iYear1  = toInt(str1.substring(0,4))-1900;
               iMonth1 = toInt(str1.substring(4,6))-1;
               iDay1   = toInt(str1.substring(6,8));
          }
          catch(Exception ex)
          {
               return "0";
          }

          java.util.Date dt1 = new java.util.Date(iYear1,iMonth1,iDay1);

          long num1 = dt1.getTime();
          long num2 = (days*24*60*60*1000);
          if (Sig == -1)
               dt1.setTime(num1-num2);
          else
               dt1.setTime(num1+num2);

          int iDay   = dt1.getDate();
          int iMonth = dt1.getMonth()+1;
          int iYear  = dt1.getYear()+1900;

          SDay   = (iDay<10 ?"0"+iDay:""+iDay);
          SMonth = (iMonth<10 ?"0"+iMonth:""+iMonth);
          SYear  = ""+iYear;
          return SDay+"."+SMonth+"."+SYear;
     }
     public int getPreviousMonth(int iMonth)
     {
		String SMonth = null;
		SMonth = String.valueOf(iMonth);
		int iYear = toInt(SMonth.substring(0,4));
		String SPreMonth = String.valueOf(iMonth-1);
		if(SPreMonth.substring(4,6).equals("00"))
			SPreMonth=String.valueOf(iYear-1)+"12";
		return toInt(SPreMonth);
     }

     public String getPrevDate(String SDate)
     {
          return pureDate(getDate(parseDate(SDate),1,-1));
     }

     public int getIntCurrentDate()
     {

           return(toInt(getCurrentDate()));
     }
     public boolean isLeap(int iYear)
     {
          double dYear = iYear;
          if(Math.IEEEremainder(dYear,100.0)==0)
               return true;
          else if(Math.IEEEremainder(dYear,4.0)==0)
               return true;
          else
               return false;

     }

	public int getPreviousMonth(int iMonth,int iMany)
	{
		int iTemp = iMonth;
                for(int i = 0; i < iMany; i++)
                {
                        iTemp = getPreviousMonth(iTemp);
                }
                return iTemp;
	}

        public int[] getPreviousMonths(int iMonth,int iMany)
        {
                int iMonthArr[] = new int[iMany];
                for(int i=0;i<iMonthArr.length;i++)
                        iMonthArr[i]=0;
                for(int i=0;i<iMonthArr.length;i++)
                {
                        iMonthArr[i]=getPreviousMonth(iMonth);
                        iMonth=iMonthArr[i];
                }
                return iMonthArr;
        }

    public int getMonths(int iStartMonth,int iEndMonth)
    {
        int iCntr=0;

        while(iStartMonth<=iEndMonth)
        {
            iCntr++;
            int iMonth = getNextMonth(iStartMonth);
            iStartMonth=iMonth;
        }
        return iCntr;
    }
     public String getLocalHostName()
     {
          String         SComName="";
          try
          {
               InetAddress    host      =    InetAddress    . getLocalHost();
               SComName                 =    host           . getHostName();
          }catch(Exception ex)
          {
               System.out.println(ex);
          }

          return SComName;
     }

     // New Methods....

	public String getDailyReportHeader(String STitle,String SUnit,String SShift,String SDate,String RepType)
	{
	   return "<p align='center' style='COLOR: maroon; FONT-SIZE: large; FONT-WEIGHT: bold'>" + STitle + "</p>" +
		     "<p align='left' style='FONT-SIZE: small; FONT-WEIGHT: bold'>" +
		     "<font color='mediumblue'>Unit&nbsp;&nbsp;&nbsp;:&nbsp;</font><font color=purple> " + getUnit(SUnit) + "</font>&nbsp;&nbsp;&nbsp;&nbsp;" +
		     "<font color='mediumblue'>Shift&nbsp;&nbsp;:&nbsp;</font><font color=purple>" + getShift(SShift) + "</font>&nbsp;&nbsp;&nbsp;&nbsp;" +
		     "<font color='mediumblue'>Date&nbsp;&nbsp;&nbsp;:&nbsp;</font><font color=purple>" + parseDate(SDate) + "</font>" +
		     "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
			"<font color='mediumblue'><u>" + RepType + " </u></font>" +
		     "</p>";
	}

	public String getUnit(String SUnit)
	{
		int i = Integer.parseInt(SUnit);
		if(i==1)
			return "A-Unit";
		if(i==2)
			return "B-Unit";
		if(i==4)
			return "C-Uniy";
		return "General";
	}

	public String getShift(String SShift)
	{
		int i = Integer.parseInt(SShift);
		if(i==1)
			return "1st Shift";
		if(i==2)
			return "2nd Shift";
		if(i==3)
			return "3rd Shift";
		return "X Shift";
	}
        public String getMonthfName(int iMonth)
        {
            String smname ="";
            if(iMonth == 1){smname = "January";}
            else if (iMonth == 2){smname = "February";}
            else if (iMonth == 3){smname = "March";}
            else if (iMonth == 4){smname = "April";}
            else if (iMonth == 5){smname = "May";}
            else if (iMonth == 6){smname = "June";}
            else if (iMonth == 7){smname = "July";}
            else if (iMonth == 8){smname = "August";}
            else if (iMonth == 9){smname = "September";}
            else if (iMonth == 10){smname = "October";}
            else if (iMonth == 11){smname = "November";}
            else if (iMonth == 12){smname = "December";}
            return smname;
        }
        public int getMonthDay(int iMonth, int iYear)
        {
            int numDays = 0;
            if (iMonth == 2)
	            {
	             if((iYear % 4 == 0) && (iYear % 400 == 0)
	                     && !(iYear % 100 == 0) )
	                    numDays = 29;
	                else
	                    numDays = 28;
	            }
	        else if (iMonth == 1 || iMonth == 3 || iMonth == 5 || iMonth == 7 || iMonth == 8 
	                    || iMonth == 10 || iMonth == 12)
	            numDays = 31;
	        else
	            numDays = 30;
            return numDays;
        }
        public String removeNull(String Svalue){
        String sReturn="";
        if(Svalue.equals("") || Svalue.equals("0") || Svalue.equals("0.00") || Svalue.equals("null"))
        {
            sReturn = "";
        }
        else
        {
            sReturn = Svalue;
        }
            return sReturn;
        }
        
        public String getPrintPath(){
	
            System.out.println("Inside Common");
		String SFilePath="";
		String workingdir = System.getProperty("user.dir");
		String sfile = "One.txt";
		File fileName = new File(workingdir,sfile);
		File[] root = fileName.listRoots();
                
		String OSName = System.getProperty("os.name");
		String SActFile="";
		for (int i=0;i<root.length ;i++ ){
			String SCurFile = root[i].getAbsolutePath();
			if(OSName.startsWith("Lin")){
				SActFile = SCurFile+"soft29"+System.getProperty("file.separator");
				break;
			}
                        else{
				if(SCurFile.startsWith("D")){
					SActFile = SCurFile;
					break;
				}
			}
		}
                SFilePath = SActFile+System.getProperty("file.separator");
                System.out.println("Print File Path-->"+SFilePath);
		return SFilePath;
                
	}
}

