/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.communication;

import com.jdbc.connection.JDBCProcessConnection;
import com.jdbc.connection.JDBCProcessaConnection;

/**
 *
 * @author root
 */
public class Communication {

    java.sql.Connection theProcessConnection = null;

    public Communication() {
    }

    public java.util.List getCommunicationOrderNos() {
        java.util.List theList = new java.util.ArrayList();
        StringBuffer sb = new StringBuffer();
        sb.append(" SELECT RORDERNO FROM REGULARORDER WHERE ORDERDATE>=20140801 ORDER BY REGULARORDER.RORDERNO ");

        try {
            if (theProcessConnection == null) {
                JDBCProcessaConnection jdbc = JDBCProcessaConnection.getJDBCConnection();
                theProcessConnection = jdbc.getConnection();
            }
            java.sql.PreparedStatement pstmt = theProcessConnection.prepareStatement(sb.toString());
            java.sql.ResultSet rset = pstmt.executeQuery();
            CommunicationClass communicationClass = new CommunicationClass();

            while (rset.next()) {
                communicationClass = new CommunicationClass();
                communicationClass.setSorderno(rset.getString(1));
                theList.add(communicationClass);
            }
            rset.close();
            rset = null;
            pstmt.close();
            pstmt = null;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return theList;
    }

    public int IsCorrectionMixing(String SOrderNo) {
        int iCount = 0;
        StringBuffer sb = new StringBuffer();
        sb.append(" SELECT count(*) FROM PROCESSINGOFINDENT_CORRECTION WHERE RORDERNO = ? AND PREPCOMMSTATUS = 1 ");

        try {
            if (theProcessConnection == null) {
                JDBCProcessaConnection jdbc = JDBCProcessaConnection.getJDBCConnection();
                theProcessConnection = jdbc.getConnection();
            }
            java.sql.PreparedStatement pst = theProcessConnection.prepareStatement(sb.toString());
            pst.setString(1, SOrderNo);
            java.sql.ResultSet rst = pst.executeQuery();

            while (rst.next()) {
                iCount = rst.getInt(1);
            }
            rst.close();
            rst = null;
            pst.close();
            pst = null;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return iCount;
    }
    public int getUnitCode(String SOrderNo)
    {
        int iUnitCode = 0;
        StringBuffer sb = new StringBuffer();
        sb.append(" SELECT UNITCODE From RegularOrder Where RORDERNO=? ");
        try {
            if (theProcessConnection == null) {
                JDBCProcessaConnection jdbc = JDBCProcessaConnection.getJDBCConnection();
                theProcessConnection = jdbc.getConnection();
            }
            java.sql.PreparedStatement pst = theProcessConnection.prepareStatement(sb.toString());
            pst.setString(1, SOrderNo);
            java.sql.ResultSet rst = pst.executeQuery();
            while (rst.next()) {
                iUnitCode = rst.getInt(1);
            }
            rst.close();
            rst = null;
            pst.close();
            pst = null;
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return iUnitCode;
    }
}