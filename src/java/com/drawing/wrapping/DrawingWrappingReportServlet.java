package com.drawing.wrapping;

import com.common.Common;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DrawingWrappingReportServlet
  extends HttpServlet
{
  Common common = new Common();
  
  protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
    response.setContentType("text/html;charset=UTF-8");
    PrintWriter out = response.getWriter();
    try
    {
      String SUnit = "";String SShift = "";String SUser = "";
        
      String sFromDate = common.pureDate(request.getParameter("tdatefrom"));
      String sToDate = common.pureDate(request.getParameter("tdateto"));
      String sTestNo = request.getParameter("testno");
      String sOrderNo = common.parseNull(request.getParameter("orderno"));
      String sAbst = common.parseNull(request.getParameter("abst"));
      String sprocessType = common.parseNull(request.getParameter("ptype"));
      String syarnType = common.parseNull(request.getParameter("yarntype"));
      
      int iUnitCode = common.toInt(common.parseNull(request.getParameter("sunits")));
      int iPassId = common.toInt(common.parseNull(request.getParameter("spass")));
      int iShift = common.toInt(common.parseNull(request.getParameter("sshift")));
      int iMachine = common.toInt(common.parseNull(request.getParameter("machine")));
      if (iUnitCode == 1) {
        SUnit = "A-UNIT";
      } else if (iUnitCode == 2) {
        SUnit = "B-UNIT";
      } else if (iUnitCode == 10) {
        SUnit = "C-UNIT";
      }
      if (iShift == 1) {
        SShift = "Shift-I";
      } else if (iShift == 2) {
        SShift = "Shift-II";
      } else if (iShift == 3) {
        SShift = "Shift-III";
      } else {
        SShift = "All";
      }
      DrawingWrappingTestDetails drawingWrappingTestDetails = new DrawingWrappingTestDetails();
      List theList = drawingWrappingTestDetails.getDrawingTestDetails(sFromDate, sToDate, sOrderNo, iUnitCode, iPassId, iShift, iMachine,sprocessType,syarnType);
      if (iShift != 0) {
        SUser = drawingWrappingTestDetails.getUser(sFromDate, sToDate, sOrderNo, iUnitCode, iPassId, iShift);
      }
      System.out.println("SUser-->" + SUser);
      
      DrawingWrappingTestPrint drawingWrappingTestPrint = new DrawingWrappingTestPrint(sFromDate, sToDate,iUnitCode);
      drawingWrappingTestPrint.GeneratePrint(theList, SUser, SUnit, SShift);
      
      request.setAttribute("theDrawingWrappingTestDetails", theList);
      request.setAttribute("TestNo", sTestNo);
      request.setAttribute("Fdate", sFromDate);
      request.setAttribute("Tdate", sToDate);
      request.setAttribute("UnitName", SUnit);
      request.setAttribute("Shift", SShift);
      
      request.getRequestDispatcher("drawingwrappingreport.jsp").forward(request, response);
    }
    finally
    {
      out.close();
    }
  }
  
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
    processRequest(request, response);
  }
  
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
    processRequest(request, response);
  }
  
  public String getServletInfo()
  {
    return "Short description";
  }
}
