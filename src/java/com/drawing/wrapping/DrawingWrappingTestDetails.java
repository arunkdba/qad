package com.drawing.wrapping;

import com.common.Common;
import com.jdbc.connection.JDBCProcessConnection;
import com.jdbc.connection.JDBCProcessaConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.HashMap;


public class DrawingWrappingTestDetails {

    java.sql.Connection theProcessConnection  = null;
    java.sql.Connection theProcessaConnection  = null;
    Common common;

    public DrawingWrappingTestDetails() {
        common = new Common();
    }


    public java.util.List getDrawingTestDetails(String SFromDate,String SToDate,String sOrderNo,int iUnitCode,int iPassId,int iShift,int iMachine,String sprocessType,String sYarnType){
        java.util.List theList = new java.util.ArrayList();
        StringBuffer sb = new StringBuffer();
        //System.out.println("iUnitCode->"+iUnitCode);
                     sb . append(" ");
                     
                     if(iUnitCode == 1){
                         // A Unit
                        
                        sb . append(" SELECT PRO_DRAWING_HANK_DETAILS.TEST_NO, MACHINE.MACH_NAME, PROCESSINGOFINDENT.RORDERNO, PROCESSINGOFINDENT.YSHNM, ");
                        //sb . append(" decode(MACHINE.MAKE, ");
                        //sb . append(" 'RSB',AUNIT_ORDERDRAWINGSETTING.RSBHANK||'-'||AUNIT_ORDERDRAWINGSETTING.RSBHANKTO, ");
                        //sb . append(" 'DO/2S',AUNIT_ORDERDRAWINGSETTING.DO2SHANK||'-'||AUNIT_ORDERDRAWINGSETTING.DO2SHANKTO, ");
                        //sb . append(" 'D45',AUNIT_ORDERDRAWINGSETTING.DO6SHANK||'-'||AUNIT_ORDERDRAWINGSETTING.DO6SHANKTO, ");
                        //sb . append(" 'D50',AUNIT_ORDERDRAWINGSETTING.D50HANK||'-'||AUNIT_ORDERDRAWINGSETTING.D50HANKTO, ");
                        //sb . append(" 'D11',AUNIT_ORDERDRAWINGSETTING.D11HANK||'-'||AUNIT_ORDERDRAWINGSETTING.D11HANKTO,'') as SettingHank, ");
                        
                        sb . append(" decode( ");
                        sb . append(" MACHINE.MAKE,  'RSB',decode(AUNIT_ORDERDRAWINGSETTING.RSBHANK,'none',AUNIT_ORDERDRAWINGSETTING.RSBWHITEHANKFROM,AUNIT_ORDERDRAWINGSETTING.RSBHANK) ");
                        sb . append(" ||'-'||decode(AUNIT_ORDERDRAWINGSETTING.RSBHANKTO,'none',AUNIT_ORDERDRAWINGSETTING.RSBWHITEHANKTO,AUNIT_ORDERDRAWINGSETTING.RSBHANKTO), ");
                        sb . append(" 'DO/2S', decode(AUNIT_ORDERDRAWINGSETTING.DO2SHANK,'none',AUNIT_ORDERDRAWINGSETTING.DO2SWHITEHANKFROM,AUNIT_ORDERDRAWINGSETTING.DO2SHANK) ");
                        sb . append(" ||'-'|| decode(AUNIT_ORDERDRAWINGSETTING.DO2SHANKTO,'none',AUNIT_ORDERDRAWINGSETTING.DO2SWHITEHANKTO,AUNIT_ORDERDRAWINGSETTING.DO2SHANKTO), ");
                        sb . append(" 'D45',decode(AUNIT_ORDERDRAWINGSETTING.DO6SHANK,'none',AUNIT_ORDERDRAWINGSETTING.DO6SWHITEHANKFROM,AUNIT_ORDERDRAWINGSETTING.DO6SHANK) ");
                        sb . append(" ||'-'||decode(AUNIT_ORDERDRAWINGSETTING.DO6SHANKTO,'none',AUNIT_ORDERDRAWINGSETTING.DO6SWHITEHANKTO,AUNIT_ORDERDRAWINGSETTING.DO6SHANKTO), ");
                        sb . append(" 'D50', decode(AUNIT_ORDERDRAWINGSETTING.D50HANK,'none',AUNIT_ORDERDRAWINGSETTING.D50WHITEHANKFROM,AUNIT_ORDERDRAWINGSETTING.D50HANK) ");
                        sb . append(" ||'-'||decode(AUNIT_ORDERDRAWINGSETTING.D50HANKTO,'none',AUNIT_ORDERDRAWINGSETTING.D50WHITEHANKTO,AUNIT_ORDERDRAWINGSETTING.D50HANKTO), ");
                        sb . append(" 'D11', decode(AUNIT_ORDERDRAWINGSETTING.D11HANK,'none',AUNIT_ORDERDRAWINGSETTING.D11WHITEHANKFROM,AUNIT_ORDERDRAWINGSETTING.D11HANK) ");
                        sb . append(" ||'-'||decode(AUNIT_ORDERDRAWINGSETTING.D11HANKTO,'none',AUNIT_ORDERDRAWINGSETTING.D11WHITEHANKTO,AUNIT_ORDERDRAWINGSETTING.D11HANKTO)) as SettingHank, ");
                        sb . append(" DRAWINGPROCESS.PROCESSNAME, to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'HH24:MI') as Time, ");
                        sb . append(" PRO_DRAWING_HANK_DETAILS.RSB, PRO_DRAWING_HANK_DETAILS.CPWHEEL,PRO_DRAWING_HANK_DETAILS.DRAFT, ");
                        sb . append(" PRO_DRAWING_HANK_DETAILS.NO_ENDS,PRO_HANK_STATUS.SHORTNAME,PRO_DRAWING_HANK_DETAILS.REMARKS, ");
                        sb . append(" PRO_DRAWING_HANK_DETAILS.MIXNO,PROCESSINGOFINDENT.COUNTNAME,REGULARORDER.WEIGHT,PRO_HANK_STATUS.CODE, ");
                        sb . append(" to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'MM/dd/yyyy HH24:MI:SS') as EntryTime,DRAWINGPROCESS.ID FROM PROCESS.PRO_DRAWING_HANK_DETAILS ");
                        sb . append(" INNER JOIN MACHINE ON MACHINE.MACH_CODE = PRO_DRAWING_HANK_DETAILS.MACH_CODE AND MACHINE.UNIT_CODE="+iUnitCode+" AND MACHINE.DEPT_CODE = 5 ");
                        sb . append(" INNER JOIN PROCESSINGOFINDENT ON PROCESSINGOFINDENT.RORDERNO = PRO_DRAWING_HANK_DETAILS.RORDERNO ");                        
                        sb . append(" LEFT JOIN AUNIT_ORDERDRAWINGSETTING ON AUNIT_ORDERDRAWINGSETTING.RORDERNO = PRO_DRAWING_HANK_DETAILS.RORDERNO ");
                        sb . append(" AND AUNIT_ORDERDRAWINGSETTING.SETTINGTYPE = 0 AND AUNIT_ORDERDRAWINGSETTING.MIXNO=PRO_DRAWING_HANK_DETAILS.MIXNO ");
                        sb . append(" INNER JOIN DRAWINGPROCESS ON DRAWINGPROCESS.ID = PRO_DRAWING_HANK_DETAILS.PROCESSLEVELID ");
                        sb . append(" LEFT JOIN PROCESS.PRO_HANK_STATUS ON PRO_HANK_STATUS.CODE = PROCESS.PRO_DRAWING_HANK_DETAILS.TESTSTATUS ");
                        sb . append(" LEFT JOIN REGULARORDER ON REGULARORDER.RORDERNO=PRO_DRAWING_HANK_DETAILS.RORDERNO ");
                        sb . append(" Inner join scm.ProcessingType on scm.ProcessingType.ProcessCode = regularorder.ProcessTypeCode ");
                        sb . append(" Inner join scm.FibreForm on scm.FibreForm.FormCode = regularorder.FIBREFORMCODE ");
                        sb . append(" WHERE to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'YYYYMMDD')>="+SFromDate+" ");
                        sb . append(" AND to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'YYYYMMDD')<="+SToDate+" ");
                        if(!sOrderNo.equals("")){
                            sb.append(" AND PROCESSINGOFINDENT.RORDERNO = '"+sOrderNo+"' ");
                        }
                        if(iPassId !=0){
                            sb.append(" AND PRO_DRAWING_HANK_DETAILS.PROCESSLEVELID = "+iPassId+" ");
                        }
                        if(iShift !=0){
                            sb . append(" AND PRO_DRAWING_HANK_DETAILS.SHIFT ="+iShift+"");
                        }
                        if(iMachine !=0){
                            sb . append(" AND PRO_DRAWING_HANK_DETAILS.MACH_CODE ="+iMachine+"");
                        }
                        if(sprocessType.equals("43")){
                            sb.append(" and ProcessingType.OEStatus = 1 ");
                        }
                        else{
                            sb.append(" and ProcessingType.OEStatus = 0 ");
                        }
                        if(sYarnType.equals("4")){
                            sb.append(" and FibreForm.GROUPCODE = 4 ");
                        }
                        else{
                            sb.append(" and FibreForm.GROUPCODE != 4 ");
                        }
                        
                        if(!sprocessType.equals("43") && !sYarnType.equals("4") ){
                        sb . append(" UNION ALL ");         //      For Leveling
                        sb . append(" SELECT PRO_DRAWING_HANK_DETAILS.TEST_NO, MACHINE.MACH_NAME, PROCESSINGOFLOT.LOTNO, '' as YSHNM  , ");
                        //sb . append(" decode(MACHINE.MAKE, ");
                        //sb . append(" 'RSB',AUNIT_ORDERDRAWINGSETTING.RSBHANK||'-'||AUNIT_ORDERDRAWINGSETTING.RSBHANKTO, ");
                        //sb . append(" 'DO/2S',AUNIT_ORDERDRAWINGSETTING.DO2SHANK||'-'||AUNIT_ORDERDRAWINGSETTING.DO2SHANKTO, ");
                        //sb . append(" 'D45',AUNIT_ORDERDRAWINGSETTING.DO6SHANK||'-'||AUNIT_ORDERDRAWINGSETTING.DO6SHANKTO, ");
                        //sb . append(" 'D50',AUNIT_ORDERDRAWINGSETTING.D50HANK||'-'||AUNIT_ORDERDRAWINGSETTING.D50HANKTO, ");
                        //sb . append(" 'D11',AUNIT_ORDERDRAWINGSETTING.D11HANK||'-'||AUNIT_ORDERDRAWINGSETTING.D11HANKTO,'') as SettingHank, ");
                        sb . append(" decode( ");
                        sb . append(" MACHINE.MAKE,  'RSB',decode(AUNIT_ORDERDRAWINGSETTING.RSBHANK,'none',AUNIT_ORDERDRAWINGSETTING.RSBWHITEHANKFROM,AUNIT_ORDERDRAWINGSETTING.RSBHANK) ");
                        sb . append(" ||'-'||decode(AUNIT_ORDERDRAWINGSETTING.RSBHANKTO,'none',AUNIT_ORDERDRAWINGSETTING.RSBWHITEHANKTO,AUNIT_ORDERDRAWINGSETTING.RSBHANKTO), ");
                        sb . append(" 'DO/2S', decode(AUNIT_ORDERDRAWINGSETTING.DO2SHANK,'none',AUNIT_ORDERDRAWINGSETTING.DO2SWHITEHANKFROM,AUNIT_ORDERDRAWINGSETTING.DO2SHANK) ");
                        sb . append(" ||'-'|| decode(AUNIT_ORDERDRAWINGSETTING.DO2SHANKTO,'none',AUNIT_ORDERDRAWINGSETTING.DO2SWHITEHANKTO,AUNIT_ORDERDRAWINGSETTING.DO2SHANKTO), ");
                        sb . append(" 'D45',decode(AUNIT_ORDERDRAWINGSETTING.DO6SHANK,'none',AUNIT_ORDERDRAWINGSETTING.DO6SWHITEHANKFROM,AUNIT_ORDERDRAWINGSETTING.DO6SHANK) ");
                        sb . append(" ||'-'||decode(AUNIT_ORDERDRAWINGSETTING.DO6SHANKTO,'none',AUNIT_ORDERDRAWINGSETTING.DO6SWHITEHANKTO,AUNIT_ORDERDRAWINGSETTING.DO6SHANKTO), ");
                        sb . append(" 'D50', decode(AUNIT_ORDERDRAWINGSETTING.D50HANK,'none',AUNIT_ORDERDRAWINGSETTING.D50WHITEHANKFROM,AUNIT_ORDERDRAWINGSETTING.D50HANK) ");
                        sb . append(" ||'-'||decode(AUNIT_ORDERDRAWINGSETTING.D50HANKTO,'none',AUNIT_ORDERDRAWINGSETTING.D50WHITEHANKTO,AUNIT_ORDERDRAWINGSETTING.D50HANKTO), ");
                        sb . append(" 'D11', decode(AUNIT_ORDERDRAWINGSETTING.D11HANK,'none',AUNIT_ORDERDRAWINGSETTING.D11WHITEHANKFROM,AUNIT_ORDERDRAWINGSETTING.D11HANK) ");
                        sb . append(" ||'-'||decode(AUNIT_ORDERDRAWINGSETTING.D11HANKTO,'none',AUNIT_ORDERDRAWINGSETTING.D11WHITEHANKTO,AUNIT_ORDERDRAWINGSETTING.D11HANKTO)) as SettingHank, ");
                        sb . append(" AUNIT_DRAWINGLOTPROCESS.PROCESSNAME, to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'HH24:MI') as Time, ");
                        sb . append(" PRO_DRAWING_HANK_DETAILS.RSB, PRO_DRAWING_HANK_DETAILS.CPWHEEL,PRO_DRAWING_HANK_DETAILS.DRAFT, ");
                        sb . append(" PRO_DRAWING_HANK_DETAILS.NO_ENDS,PRO_HANK_STATUS.SHORTNAME,PRO_DRAWING_HANK_DETAILS.REMARKS, ");
                        sb . append(" PRO_DRAWING_HANK_DETAILS.MIXNO,'' as COUNTNAME ,REGULARORDER.WEIGHT,PRO_HANK_STATUS.CODE, ");
                        sb . append(" to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'MM/dd/yyyy HH24:MI:SS') as EntryTime,AUNIT_DRAWINGLOTPROCESS.ID FROM PROCESS.PRO_DRAWING_HANK_DETAILS ");
                        sb . append(" INNER JOIN MACHINE ON MACHINE.MACH_CODE = PRO_DRAWING_HANK_DETAILS.MACH_CODE AND MACHINE.UNIT_CODE="+iUnitCode+" AND MACHINE.DEPT_CODE = 5 ");
                        sb . append(" INNER JOIN PROCESSINGOFLOT ON PROCESSINGOFLOT.LOTNO = PRO_DRAWING_HANK_DETAILS.RORDERNO and PROCESSINGOFLOT.DRAWCOMP=0 ");
                        sb . append(" LEFT JOIN AUNIT_ORDERDRAWINGSETTING ON AUNIT_ORDERDRAWINGSETTING.RORDERNO = PRO_DRAWING_HANK_DETAILS.RORDERNO ");
                        sb . append(" AND AUNIT_ORDERDRAWINGSETTING.SETTINGTYPE = 2 AND AUNIT_ORDERDRAWINGSETTING.MIXNO=0 ");
                        sb . append(" INNER JOIN AUNIT_DRAWINGLOTPROCESS ON AUNIT_DRAWINGLOTPROCESS.ID = PRO_DRAWING_HANK_DETAILS.PROCESSLEVELID ");
                        sb . append(" LEFT JOIN PROCESS.PRO_HANK_STATUS ON PRO_HANK_STATUS.CODE = PROCESS.PRO_DRAWING_HANK_DETAILS.TESTSTATUS ");
                        sb . append(" LEFT JOIN REGULARORDER ON REGULARORDER.RORDERNO=PRO_DRAWING_HANK_DETAILS.RORDERNO ");
                        sb . append(" Left join scm.ProcessingType on scm.ProcessingType.ProcessCode = regularorder.ProcessTypeCode ");
                        sb . append(" Left join scm.FibreForm on scm.FibreForm.FormCode = regularorder.FIBREFORMCODE ");
                        sb . append(" WHERE to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'YYYYMMDD')>= "+SFromDate+" ");
                        sb . append(" AND to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'YYYYMMDD')<= "+SToDate+" ");
                        if(!sOrderNo.equals("")){
                            sb.append(" AND PROCESSINGOFLOT.LotNo = '"+sOrderNo+"' ");
                        }
                        if(iPassId !=0){
                            sb.append(" AND PRO_DRAWING_HANK_DETAILS.PROCESSLEVELID = "+iPassId+" ");
                        }
                        if(iShift !=0){
                            sb . append(" AND PRO_DRAWING_HANK_DETAILS.SHIFT ="+iShift+"");
                        }
                        if(iMachine !=0){
                            sb . append(" AND PRO_DRAWING_HANK_DETAILS.MACH_CODE ="+iMachine+"");
                        }
                        /*if(sprocessType.equals("43")){
                            sb.append(" and ProcessingType.OEStatus = 1 ");
                        }
                        else{
                            sb.append(" and ProcessingType.OEStatus = 0 ");
                        }
                        if(sYarnType.equals("4")){
                            sb.append(" and FibreForm.GROUPCODE = 4 ");
                        }
                        else{
                            sb.append(" and FibreForm.GROUPCODE != 4 ");
                        }*/
                        }
                        if(iPassId !=0){
                            sb . append(" ORDER BY 1,2 ");
                        }
                        else{
                            sb . append(" ORDER BY 19,1,2 ");
                        }
                     }
                     if(iUnitCode == 2){
                         // B Unit

                        sb . append(" SELECT PRO_DRAWING_HANK_DETAILS.TEST_NO, MACHINE.MACH_NAME, PROCESSINGOFINDENT.RORDERNO, PROCESSINGOFINDENT.YSHNM, ");
                        
                        //sb.append(" decode(MACHINE.MAKE, ");
                        //sb.append(" 'RSB 851',ORDERDRAWINGSETTING.RSBHANK||'-'||ORDERDRAWINGSETTING.RSBHANKTO, ");
                        //sb.append(" 'D45',ORDERDRAWINGSETTING.RSBHANK||'-'||ORDERDRAWINGSETTING.RSBHANKTO, ");
                        //sb.append(" 'D35',ORDERDRAWINGSETTING.RSBHANK||'-'||ORDERDRAWINGSETTING.RSBHANKTO, ");
                        //sb.append(" 'D50',ORDERDRAWINGSETTING.RSBHANK||'-'||ORDERDRAWINGSETTING.RSBHANKTO, ");
                        //sb.append(" 'D11',ORDERDRAWINGSETTING.RSBHANK||'-'||ORDERDRAWINGSETTING.RSBHANKTO, ");
                        //sb.append(" 'SB22',ORDERDRAWINGSETTING.RSBHANK||'-'||ORDERDRAWINGSETTING.RSBHANKTO, ");
                        //sb.append(" 'RSB LD A/2',ORDERDRAWINGSETTING.RSBHANK||'-'||ORDERDRAWINGSETTING.RSBHANKTO, ");
                        //sb.append(" 'DO/6',ORDERDRAWINGSETTING.DO2SHANK||'-'||ORDERDRAWINGSETTING.DO2SHANK, ");
                        //sb.append(" 'DO/2S',ORDERDRAWINGSETTING.DO2SHANK||'-'||ORDERDRAWINGSETTING.DO2SHANKTO,'') as SettingHank, ");
                        
                        sb . append(" decode(MACHINE.MAKE, ");
                        sb . append(" 'RSB 851',decode(ORDERDRAWINGSETTING.RSBHANK,'none',ORDERDRAWINGSETTING.RSBWHITEHANKFROM,ORDERDRAWINGSETTING.RSBHANK) ");
                        sb . append(" ||'-'||decode(ORDERDRAWINGSETTING.RSBHANKTO,'none',ORDERDRAWINGSETTING.RSBWHITEHANKTO,ORDERDRAWINGSETTING.RSBHANKTO), ");
                        sb . append(" 'D45',decode(ORDERDRAWINGSETTING.RSBHANK,'none',ORDERDRAWINGSETTING.RSBWHITEHANKFROM,ORDERDRAWINGSETTING.RSBHANK) ");
                        sb . append(" ||'-'||decode(ORDERDRAWINGSETTING.RSBHANKTO,'none',ORDERDRAWINGSETTING.RSBWHITEHANKTO,ORDERDRAWINGSETTING.RSBHANKTO), ");
                        sb . append(" 'D35',decode(ORDERDRAWINGSETTING.RSBHANK,'none',ORDERDRAWINGSETTING.RSBWHITEHANKFROM,ORDERDRAWINGSETTING.RSBHANK) ");
                        sb . append(" ||'-'||decode(ORDERDRAWINGSETTING.RSBHANKTO,'none',ORDERDRAWINGSETTING.RSBWHITEHANKTO,ORDERDRAWINGSETTING.RSBHANKTO), ");
                        sb . append(" 'D50',decode(ORDERDRAWINGSETTING.RSBHANK,'none',ORDERDRAWINGSETTING.RSBWHITEHANKFROM,ORDERDRAWINGSETTING.RSBHANK) ");
                        sb . append(" ||'-'||decode(ORDERDRAWINGSETTING.RSBHANKTO,'none',ORDERDRAWINGSETTING.RSBWHITEHANKTO,ORDERDRAWINGSETTING.RSBHANKTO), ");
                        sb . append(" 'D11',decode(ORDERDRAWINGSETTING.RSBHANK,'none',ORDERDRAWINGSETTING.RSBWHITEHANKFROM,ORDERDRAWINGSETTING.RSBHANK) ");
                        sb . append(" ||'-'||decode(ORDERDRAWINGSETTING.RSBHANKTO,'none',ORDERDRAWINGSETTING.RSBWHITEHANKTO,ORDERDRAWINGSETTING.RSBHANKTO), ");
                        sb . append(" 'SB22',decode(ORDERDRAWINGSETTING.RSBHANK,'none',ORDERDRAWINGSETTING.RSBWHITEHANKFROM,ORDERDRAWINGSETTING.RSBHANK) ");
                        sb . append(" ||'-'||decode(ORDERDRAWINGSETTING.RSBHANKTO,'none',ORDERDRAWINGSETTING.RSBWHITEHANKTO,ORDERDRAWINGSETTING.RSBHANKTO), ");
                        sb . append(" 'RSB LD A/2',decode(ORDERDRAWINGSETTING.RSBHANK,'none',ORDERDRAWINGSETTING.RSBWHITEHANKFROM,ORDERDRAWINGSETTING.RSBHANK) ");
                        sb . append(" ||'-'||decode(ORDERDRAWINGSETTING.RSBHANKTO,'none',ORDERDRAWINGSETTING.RSBWHITEHANKTO,ORDERDRAWINGSETTING.RSBHANKTO), ");
                        sb . append(" 'DO/6', decode(ORDERDRAWINGSETTING.DO2SHANK,'none',ORDERDRAWINGSETTING.DO2SWHITEHANKFROM,ORDERDRAWINGSETTING.DO2SHANK) ");
                        sb . append(" ||'-'|| decode(ORDERDRAWINGSETTING.DO2SHANKTO,'none',ORDERDRAWINGSETTING.DO2SWHITEHANKTO,ORDERDRAWINGSETTING.DO2SHANKTO), ");
                        sb . append(" 'DO/2S', decode(ORDERDRAWINGSETTING.DO2SHANK,'none',ORDERDRAWINGSETTING.DO2SWHITEHANKFROM,ORDERDRAWINGSETTING.DO2SHANK) ");
                        sb . append(" ||'-'|| decode(ORDERDRAWINGSETTING.DO2SHANKTO,'none',ORDERDRAWINGSETTING.DO2SWHITEHANKTO,ORDERDRAWINGSETTING.DO2SHANKTO), ");
                        sb . append(" '') as SettingHank, ");                        
                        sb . append(" DRAWINGPROCESS.PROCESSNAME, to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'HH24:MI') as Time, ");
                        sb . append(" PRO_DRAWING_HANK_DETAILS.RSB, PRO_DRAWING_HANK_DETAILS.CPWHEEL,PRO_DRAWING_HANK_DETAILS.DRAFT, ");
                        sb . append(" PRO_DRAWING_HANK_DETAILS.NO_ENDS,PRO_HANK_STATUS.SHORTNAME,PRO_DRAWING_HANK_DETAILS.REMARKS,PRO_DRAWING_HANK_DETAILS.MIXNO, ");
                        sb . append(" PROCESSINGOFINDENT.COUNTNAME,REGULARORDER.WEIGHT,PRO_HANK_STATUS.CODE, ");
                        sb . append(" to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'MM/dd/yyyy HH24:MI:SS') as EntryTime,DRAWINGPROCESS.ID FROM PROCESS.PRO_DRAWING_HANK_DETAILS ");
                        sb . append(" INNER JOIN MACHINE ON MACHINE.MACH_CODE = PRO_DRAWING_HANK_DETAILS.MACH_CODE AND MACHINE.UNIT_CODE="+iUnitCode+" AND MACHINE.DEPT_CODE = 5 ");
                        sb . append(" INNER JOIN PROCESSINGOFINDENT ON PROCESSINGOFINDENT.RORDERNO = PRO_DRAWING_HANK_DETAILS.RORDERNO ");
                        sb . append(" LEFT JOIN ORDERDRAWINGSETTING ON ORDERDRAWINGSETTING.RORDERNO = PRO_DRAWING_HANK_DETAILS.RORDERNO AND ORDERDRAWINGSETTING.SETTINGTYPE = 0 ");
                        sb . append(" AND ORDERDRAWINGSETTING.MIXNO=PRO_DRAWING_HANK_DETAILS.MIXNO ");
                        sb . append(" INNER JOIN DRAWINGPROCESS ON DRAWINGPROCESS.ID = PRO_DRAWING_HANK_DETAILS.PROCESSLEVELID ");
                        sb . append(" LEFT JOIN PRO_HANK_STATUS ON PRO_HANK_STATUS.CODE = PRO_DRAWING_HANK_DETAILS.TESTSTATUS ");
                        sb . append(" LEFT JOIN REGULARORDER ON REGULARORDER.RORDERNO=PRO_DRAWING_HANK_DETAILS.RORDERNO ");
                        sb . append(" WHERE to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'YYYYMMDD')>="+SFromDate+" ");
                        sb . append(" AND to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'YYYYMMDD')<="+SToDate+" ");
                        if(!sOrderNo.equals("")){
                            sb.append(" AND PROCESSINGOFINDENT.RORDERNO = '"+sOrderNo+"' ");
                        }
                        if(iPassId !=0){
                            sb.append(" AND PRO_DRAWING_HANK_DETAILS.PROCESSLEVELID = "+iPassId+" ");
                        }
                        if(iShift !=0){
                             sb . append(" AND PRO_DRAWING_HANK_DETAILS.SHIFT ="+iShift+"");
                        }
                        if(iMachine !=0){
                            sb . append(" AND PRO_DRAWING_HANK_DETAILS.MACH_CODE ="+iMachine+"");
                        }

                        sb . append(" UNION All ");
                        sb . append(" SELECT PRO_DRAWING_HANK_DETAILS.TEST_NO, MACHINE.MACH_NAME, PRO_DRAWINGLOT.LotNo as RORDERNO, '' as YSHNM, ");
                        sb . append(" '' as SettingHank,DRAWINGPROCESS.PROCESSNAME, to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'HH24:MI') as Time, ");
                        sb . append(" PRO_DRAWING_HANK_DETAILS.RSB, PRO_DRAWING_HANK_DETAILS.CPWHEEL,PRO_DRAWING_HANK_DETAILS.DRAFT, ");
                        sb . append(" PRO_DRAWING_HANK_DETAILS.NO_ENDS,PRO_HANK_STATUS.SHORTNAME,PRO_DRAWING_HANK_DETAILS.REMARKS,PRO_DRAWING_HANK_DETAILS.MIXNO, ");
                        sb . append(" '' as COUNTNAME,REGULARORDER.WEIGHT,PRO_HANK_STATUS.CODE, ");
                        sb . append(" to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'MM/dd/yyyy HH24:MI:SS') as EntryTime,DRAWINGPROCESS.ID FROM PROCESS.PRO_DRAWING_HANK_DETAILS ");
                        sb . append(" INNER JOIN MACHINE ON MACHINE.MACH_CODE = PRO_DRAWING_HANK_DETAILS.MACH_CODE AND MACHINE.UNIT_CODE="+iUnitCode+" AND MACHINE.DEPT_CODE = 5 ");
                        sb . append(" INNER JOIN PRO_DRAWINGLOT ON PRO_DRAWINGLOT.LotNo = PRO_DRAWING_HANK_DETAILS.RORDERNO  ");
                        sb . append(" INNER JOIN DRAWINGPROCESS ON DRAWINGPROCESS.ID = PRO_DRAWING_HANK_DETAILS.PROCESSLEVELID ");
                        sb . append(" LEFT JOIN PRO_HANK_STATUS ON PRO_HANK_STATUS.CODE = PRO_DRAWING_HANK_DETAILS.TESTSTATUS ");
                        sb . append(" LEFT JOIN REGULARORDER ON REGULARORDER.RORDERNO=PRO_DRAWING_HANK_DETAILS.RORDERNO ");
                        sb . append(" WHERE to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'YYYYMMDD')>= "+SFromDate+" ");
                        sb . append(" AND to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'YYYYMMDD')<= "+SToDate+" ");
                        if(!sOrderNo.equals("")){
                            sb.append(" AND PRO_DRAWINGLOT.LOTNO = '"+sOrderNo+"' ");
                        }
                        if(iPassId !=0){
                            sb.append(" AND PRO_DRAWING_HANK_DETAILS.PROCESSLEVELID = "+iPassId+" ");
                        }
                        if(iShift !=0){
                             sb . append(" AND PRO_DRAWING_HANK_DETAILS.SHIFT ="+iShift+"");
                        }
                        if(iMachine !=0){
                            sb . append(" AND PRO_DRAWING_HANK_DETAILS.MACH_CODE ="+iMachine+"");
                        }
                        if(iPassId !=0){
                            sb . append(" ORDER BY 1,2 ");
                        }
                        else{
                            sb . append(" ORDER BY 19,1,2 ");
                        }
                     }

                     if(iUnitCode == 10){
                        sb . append(" SELECT PRO_DRAWING_HANK_DETAILS.TEST_NO, MACHINE.MACH_NAME, CUNIT_PROCESSGOFINDENT.RORDERNO, CUNIT_PROCESSGOFINDENT.YSHNM, ");
                        
                        //sb . append(" , decode(MACHINE.MAKE, ");
                        //sb . append(" 'RSB',CUNIT_ORDDRAWINGSETTING.RSBHANK||'-'||CUNIT_ORDDRAWINGSETTING.RSBHANKTO, ");
                        //sb . append(" 'DO/2S',CUNIT_ORDDRAWINGSETTING.DO2SHANK||'-'||CUNIT_ORDDRAWINGSETTING.DO2SHANKTO, ");
                        //sb . append(" 'D11',CUNIT_ORDDRAWINGSETTING.D11HANK||'-'||CUNIT_ORDDRAWINGSETTING.D11HANKTO, ");
                        //sb . append(" 'D40',CUNIT_ORDDRAWINGSETTING.D40HANK||'-'||CUNIT_ORDDRAWINGSETTING.D40HANKTO, ");
                        //sb . append(" 'D45',CUNIT_ORDDRAWINGSETTING.D45HANK||'-'||CUNIT_ORDDRAWINGSETTING.D45HANKTO,'') as SettingHank ");
                        
                        sb . append(" decode(MACHINE.MAKE, ");
                        sb . append(" 'RSB',decode(CUNIT_ORDDRAWINGSETTING.RSBHANK,'none',CUNIT_ORDDRAWINGSETTING.RSBWHITEHANKFROM,CUNIT_ORDDRAWINGSETTING.RSBHANK) ");
                        sb . append(" ||'-'||decode(CUNIT_ORDDRAWINGSETTING.RSBHANKTO,'none',CUNIT_ORDDRAWINGSETTING.RSBWHITEHANKTO,CUNIT_ORDDRAWINGSETTING.RSBHANKTO), ");
                        sb . append(" 'DO/2S',decode(CUNIT_ORDDRAWINGSETTING.DO2SHANK,'none',CUNIT_ORDDRAWINGSETTING.DO2SWHITEHANKFROM,CUNIT_ORDDRAWINGSETTING.DO2SHANK) ");
                        sb . append(" ||'-'|| decode(CUNIT_ORDDRAWINGSETTING.DO2SHANKTO,'none',CUNIT_ORDDRAWINGSETTING.DO2SWHITEHANKTO,CUNIT_ORDDRAWINGSETTING.DO2SHANKTO), ");
                        sb . append(" 'D11',decode(CUNIT_ORDDRAWINGSETTING.D11HANK,'none',CUNIT_ORDDRAWINGSETTING.D11WHITEHANKFROM,CUNIT_ORDDRAWINGSETTING.D11HANK) ");
                        sb . append(" ||'-'||decode(CUNIT_ORDDRAWINGSETTING.D11HANKTO,'none',CUNIT_ORDDRAWINGSETTING.D11WHITEHANKTO,CUNIT_ORDDRAWINGSETTING.D11HANKTO), ");
                        sb . append(" 'D40',decode(CUNIT_ORDDRAWINGSETTING.D40HANK,'none',CUNIT_ORDDRAWINGSETTING.D40WHITEHANKFROM,CUNIT_ORDDRAWINGSETTING.D40HANK) ");
                        sb . append(" ||'-'||decode(CUNIT_ORDDRAWINGSETTING.D40HANKTO,'none',CUNIT_ORDDRAWINGSETTING.D40WHITEHANKTO,CUNIT_ORDDRAWINGSETTING.D40HANKTO), ");
                        sb . append(" 'D45',decode(CUNIT_ORDDRAWINGSETTING.D45HANK,'none',CUNIT_ORDDRAWINGSETTING.D45WHITEHANKFROM,CUNIT_ORDDRAWINGSETTING.D45HANK) ");
                        sb . append(" ||'-'||decode(CUNIT_ORDDRAWINGSETTING.D45HANKTO,'none',CUNIT_ORDDRAWINGSETTING.D45WHITEHANKTO,CUNIT_ORDDRAWINGSETTING.D45HANKTO), ");
                        sb . append(" '') as SettingHank ");
                        
                        sb . append(" , DRAWINGPROCESS.PROCESSNAME, to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'HH24:MI') as Time, PRO_DRAWING_HANK_DETAILS.RSB, PRO_DRAWING_HANK_DETAILS.CPWHEEL ");
                        sb . append(" , PRO_DRAWING_HANK_DETAILS.DRAFT,PRO_DRAWING_HANK_DETAILS.NO_ENDS,PRO_HANK_STATUS.SHORTNAME,PRO_DRAWING_HANK_DETAILS.REMARKS,PRO_DRAWING_HANK_DETAILS.MIXNO, ");
                        //sb . append(" , getcurrentshift(to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'HH24'),to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'MI')) as shift ");
                        sb . append(" CUNIT_PROCESSGOFINDENT.COUNTNAME,REGULARORDER.WEIGHT,PRO_HANK_STATUS.CODE, ");
                        sb . append(" to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'MM/dd/yyyy HH24:MI:SS') as EntryTime,DRAWINGPROCESS.ID FROM PROCESS.PRO_DRAWING_HANK_DETAILS ");
                        sb . append(" INNER JOIN MACHINE ON MACHINE.MACH_CODE = PRO_DRAWING_HANK_DETAILS.MACH_CODE AND MACHINE.UNIT_CODE in(3,10) AND MACHINE.DEPT_CODE = 5 ");
                        sb . append(" INNER JOIN PROCESS.CUNIT_PROCESSGOFINDENT ON CUNIT_PROCESSGOFINDENT.RORDERNO = PRO_DRAWING_HANK_DETAILS.RORDERNO ");
                        sb . append(" LEFT JOIN PROCESS.CUNIT_ORDDRAWINGSETTING ON CUNIT_ORDDRAWINGSETTING.RORDERNO = PRO_DRAWING_HANK_DETAILS.RORDERNO AND CUNIT_ORDDRAWINGSETTING.SETTINGTYPE = 0 And CUNIT_ORDDRAWINGSETTING.MIXNO=PRO_DRAWING_HANK_DETAILS.MIXNO ");
                        sb . append(" INNER JOIN PROCESS.DRAWINGPROCESS ON DRAWINGPROCESS.ID = PRO_DRAWING_HANK_DETAILS.PROCESSLEVELID ");
                        sb . append(" LEFT JOIN PRO_HANK_STATUS ON PRO_HANK_STATUS.CODE = PRO_DRAWING_HANK_DETAILS.TESTSTATUS ");
                        sb . append(" LEFT JOIN REGULARORDER ON REGULARORDER.RORDERNO=PRO_DRAWING_HANK_DETAILS.RORDERNO ");
                        sb . append(" WHERE to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'YYYYMMDD')>="+SFromDate+"  AND to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'YYYYMMDD')<="+SToDate+"  ");
                        if(!sOrderNo.equals("")){
                            sb.append(" AND CUNIT_PROCESSGOFINDENT.RORDERNO = '"+sOrderNo+"' ");
                        }
                        if(iPassId !=0){
                            sb.append(" AND PRO_DRAWING_HANK_DETAILS.PROCESSLEVELID = "+iPassId+" ");
                        }
                        if(iShift !=0){
                            sb . append(" AND PRO_DRAWING_HANK_DETAILS.SHIFT ="+iShift+"");
                            //sb . append(" AND getcurrentshift(to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'HH24'),to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'MI'))="+iShift+"");
                        }
                        if(iMachine !=0){
                            sb . append(" AND PRO_DRAWING_HANK_DETAILS.MACH_CODE ="+iMachine+"");
                        }
                        if(iPassId !=0){
                            sb . append(" ORDER BY 1,2 ");
                        }
                        else{
                            sb . append(" ORDER BY 19,1,2 ");
                        }
                     }
                     //in2015.selve
                     System.out.println("Qry-->"+sb.toString());
        try{
            java.sql.PreparedStatement pst = null;
            if(iUnitCode ==1){
                if(theProcessaConnection==null){
                        JDBCProcessaConnection  jdbc    =   JDBCProcessaConnection.getJDBCConnection();
                        theProcessaConnection           =   jdbc . getConnection();
                 }
                pst = theProcessaConnection.prepareStatement(sb.toString());
            }else{
                if(theProcessConnection==null){
                        JDBCProcessConnection   jdbc    =   JDBCProcessConnection.getJDBCConnection();
                        theProcessConnection            =   jdbc . getConnection();
                 }
                pst = theProcessConnection.prepareStatement(sb.toString());
            }
//                                        pst . setString(1,SFromDate);
//                                        pst . setString(2,SToDate);
             java.sql.ResultSet   rst = pst . executeQuery();
             DrawingWrappingTestDto drawingWrappingTestDto = null;
             int isino = 0;
             String[] SWeight = null;
             double dAvgHank = 0.0;
             while(rst.next()){
                 drawingWrappingTestDto =   new DrawingWrappingTestDto();
                 drawingWrappingTestDto .   setsSino(String.valueOf(++isino));
                 drawingWrappingTestDto .   setsTestno(rst.getString(1));
                 drawingWrappingTestDto .   setSmach_name(common.parseNull(rst.getString(2)));
                 drawingWrappingTestDto .   setsOrderNo(common.parseNull(rst.getString(3)));
                 drawingWrappingTestDto .   setsShade(common.parseNull(rst.getString(4)));
                 drawingWrappingTestDto .   setsStdHank(common.parseNull(rst.getString(5)));
                 drawingWrappingTestDto .   setsProcessLevel(common.parseNull(rst.getString(6)));
                 SWeight                =   getSliverWeightDetails(rst.getString(1));
                 drawingWrappingTestDto .   setsHank1(common.parseNull((String)SWeight[0]));
                 drawingWrappingTestDto .   setsHank2(common.parseNull((String)SWeight[1]));
                 drawingWrappingTestDto .   setsHank3(common.parseNull((String)SWeight[2]));
                 dAvgHank               =   0;
                 dAvgHank               =   (common.toDouble(SWeight[0])+common.toDouble(SWeight[1])+common.toDouble(SWeight[2]))/3;
                 drawingWrappingTestDto .   setsHankAvg(common.parseNull(String.valueOf(common.getRound(dAvgHank,3))));
                 drawingWrappingTestDto .   setSentrydate(rst.getString(7));
                 drawingWrappingTestDto .   setsRsb(common.parseNull(rst.getString(8)));
                 drawingWrappingTestDto .   setsCpWheel(common.parseNull(rst.getString(9)));
                 drawingWrappingTestDto .   setsDraft(common.parseNull(rst.getString(10)));
                 drawingWrappingTestDto .   setsEndno(common.parseNull(rst.getString(11)));
                 drawingWrappingTestDto .   setSstatus(common.parseNull(rst.getString(12)));
                 drawingWrappingTestDto .   setsRemark(common.parseNull(rst.getString(13)));
                 drawingWrappingTestDto .   setsMixno(common.parseNull(rst.getString(14)));
                 drawingWrappingTestDto .   setsCountName(common.parseNull(rst.getString(15)));
                 drawingWrappingTestDto .   setsOrderWeight(common.parseNull(rst.getString(16)));
                 drawingWrappingTestDto .   setSstatuscode(common.parseNull(rst.getString(17)));
                 drawingWrappingTestDto .   setsEntryTime(common.parseNull(rst.getString(18)));
                 theList                .   add(drawingWrappingTestDto);
             }
             rst    .   close();
             rst    =   null;
             pst    .   close();
             pst    =   null;

        }catch(Exception ex){
            ex.printStackTrace();
        }
        return theList;
    }
    
    private String[] getSliverWeightDetails(String STestNo){
        String[] SWeight = null;
        StringBuffer sb = new StringBuffer();
                     sb . append(" SELECT WEIGHT FROM PROCESS.SLIVER_WEIGHT_DETAILS WHERE TEST_NO = ? ");
        try{
            if(theProcessConnection==null){
                    JDBCProcessConnection   jdbc    =   JDBCProcessConnection.getJDBCConnection();
                    theProcessConnection            =   jdbc . getConnection();
             }
             java.sql.PreparedStatement pst =   theProcessConnection.prepareStatement(sb.toString());
                                        pst .   setString(1,STestNo);
             java.sql.ResultSet   rst = pst .   executeQuery();
                SWeight     =   new String[3];
                int icnt    =   0;
             while(rst.next()){

                 if(icnt<3) {
                    SWeight[icnt] = getHank(rst.getString(1));
                    //SWeight[icnt] = rst.getString(1);
                 }
                 icnt++;
             }
             rst.close();
             rst = null;
             pst.close();
             pst = null;

        }catch(Exception ex){
            ex.printStackTrace();
        }
        return SWeight;
    }
    
    public java.util.List getPassLevel() {
        java.util.List thelist = new java.util.ArrayList();
        StringBuffer sb = new StringBuffer();
                     sb.append(" SELECT DISTINCT DRAWINGPROCESS.PROCESSNAME,DRAWINGPROCESS.ID FROM DRAWINGPROCESS ");
                     sb.append(" Inner Join PRO_DRAWING_HANK_DETAILS on PRO_DRAWING_HANK_DETAILS.PROCESSLEVELID=DRAWINGPROCESS.ID ");
                     sb.append(" ORDER BY DRAWINGPROCESS.PROCESSNAME ");

        try {
            if(theProcessConnection==null){
                    JDBCProcessConnection   jdbc    =   JDBCProcessConnection.getJDBCConnection();
                    theProcessConnection            =   jdbc.getConnection();
             }
            java.sql.PreparedStatement pst=null;
            java.sql.ResultSet rst=null;
            pst = theProcessConnection.prepareStatement(sb.toString());
            rst = pst . executeQuery();
            ResultSetMetaData rsmd    = rst.getMetaData();
            while(rst.next())
            {
                    HashMap themap = new HashMap();
                    for(int i=0;i<rsmd.getColumnCount();i++)
                    {
                        themap.put(rsmd.getColumnName(i+1),rst.getString(i+1));
                    }
                    thelist.add(themap);
            }
            rst.close();
            pst.close();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return thelist;
    }
    
    public String getUser (String SFromDate,String SToDate,String sOrderNo,int iUnitCode,int iPassId,int iShift){
        StringBuffer sb = new StringBuffer();
        String sUser="";
        java.sql.PreparedStatement pst = null;
        if(iUnitCode==10){
            iUnitCode=3;
        }
                       if(iUnitCode==1) {
                            sb . append(" SELECT wm_concat(DISTINCT onetouchemployee.DISPLAYNAME) as UserName FROM PRO_DRAWING_HANK_DETAILS ");
                            sb . append(" LEFT JOIN onetouchemployee ON onetouchemployee.Empcode =  PRO_DRAWING_HANK_DETAILS.ENTRY_UCODE ");
                            sb . append(" WHERE to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'YYYYMMDD')>= ?  AND to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'YYYYMMDD')<= ? ");
                            if(iPassId !=0){
                                sb.append(" AND PRO_DRAWING_HANK_DETAILS.PROCESSLEVELID = "+iPassId+" ");
                            }
                            if(iShift !=0){
                                sb . append(" AND PRO_DRAWING_HANK_DETAILS.SHIFT = "+iShift+"");
                                sb . append(" AND PRO_DRAWING_HANK_DETAILS.UNIT_CODE = "+iUnitCode+"");
                                //sb . append(" AND getcurrentshift(to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'HH24'),to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'MI'))="+iShift+"");
                            }
                       }
                       else{
                           
                           
                           sb . append(" SELECT wm_concat(DISTINCT QC_USER.USERNAME) as UserName FROM PRO_DRAWING_HANK_DETAILS ");
                            sb . append(" LEFT JOIN QC_USER ON QC_USER.USERCODE =  PRO_DRAWING_HANK_DETAILS.ENTRY_UCODE ");
                            sb . append(" WHERE to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'YYYYMMDD')>= ?  AND to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'YYYYMMDD')<= ? ");
                            if(iPassId !=0){
                                sb.append(" AND PRO_DRAWING_HANK_DETAILS.PROCESSLEVELID = "+iPassId+" ");
                            }
                            if(iShift !=0){
                                sb . append(" AND PRO_DRAWING_HANK_DETAILS.SHIFT = "+iShift+"");
                                sb . append(" AND PRO_DRAWING_HANK_DETAILS.UNIT_CODE = "+iUnitCode+"");
                                //sb . append(" AND getcurrentshift(to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'HH24'),to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'MI'))="+iShift+"");
                            }
                           
                       }
                        //System.out.println("Drawing User-->"+sb.toString());

                        try
                        {
                            if(theProcessConnection==null){
                            JDBCProcessConnection  jdbc = JDBCProcessConnection.getJDBCConnection();
                            theProcessConnection = jdbc . getConnection();
                            }
                            pst = theProcessConnection.prepareStatement(sb.toString());
                            pst . setString(1,SFromDate);
                                        pst . setString(2,SToDate);
                            java.sql.ResultSet   rst = pst . executeQuery();
                            while(rst.next()){
                                sUser = common.parseNull(rst.getString(1));
                            }
                            rst.close();
                            pst.close();
                        }
                        catch(Exception ex){
                            ex.printStackTrace();
                        }
        return sUser;
    }
    public java.util.List getMachine(int iUnitCode)
    {
        PreparedStatement pst = null;
        ResultSet   rst = null;
        java.util.List thelist = new java.util.ArrayList();
        StringBuffer sb = new StringBuffer();

        if(iUnitCode==10){
            sb . append(" SELECT DISTINCT MACHINE.MACH_NAME,PRO_DRAWING_HANK_DETAILS.MACH_CODE FROM PRO_DRAWING_HANK_DETAILS ");
            sb . append(" INNER JOIN MACHINE ON MACHINE.MACH_CODE = PRO_DRAWING_HANK_DETAILS.MACH_CODE AND MACHINE.UNIT_CODE in(3,10) AND MACHINE.DEPT_CODE = 5 ");
            sb . append(" ORDER By TO_NUMBER(MACHINE.MACH_NAME) ");
        }
        else{
            sb . append(" SELECT DISTINCT MACHINE.MACH_NAME,PRO_DRAWING_HANK_DETAILS.MACH_CODE FROM PRO_DRAWING_HANK_DETAILS ");
            sb . append(" INNER JOIN MACHINE ON MACHINE.MACH_CODE = PRO_DRAWING_HANK_DETAILS.MACH_CODE AND MACHINE.UNIT_CODE="+iUnitCode+" AND MACHINE.DEPT_CODE = 5 ");
            sb . append(" ORDER By TO_NUMBER(MACHINE.MACH_NAME) ");
        }
        try
        {
            if(theProcessConnection==null){
                    JDBCProcessConnection  jdbc = JDBCProcessConnection.getJDBCConnection();
                    theProcessConnection = jdbc . getConnection();
             }
             pst = theProcessConnection.prepareStatement(sb.toString());
             rst = pst . executeQuery();
             ResultSetMetaData rsmd    = rst.getMetaData();
             while(rst.next()){
                 HashMap themap = new HashMap();
                    for(int i=0;i<rsmd.getColumnCount();i++)
                    {
                        themap.put(rsmd.getColumnName(i+1),rst.getString(i+1));
                    }
                    thelist.add(themap);
             }
             rst.close();
             pst.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return thelist;
    }
    private String getHank(String SWeight){
        return common.getRound((3.24/common.toDouble(common.parseNull((String)SWeight))),3); //Hank Calculation
    }
}
//select to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'HH24') from PRO_DRAWING_HANK_DETAILS;

//select to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'MI') from PRO_DRAWING_HANK_DETAILS;

//getcurrentshift(to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'HH24'),to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'MI')) as shift
//select getcurrentshiftnew(to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'HH24'),to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'MI')) as cshift,to_char(EntryDate,'HH24:MI:SS') as time,entrydate from PRO_DRAWING_HANK_DETAILS
//update PRO_DRAWING_HANK_DETAILS set SHIFT=1 where getcurrentshiftnew(to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'HH24'),to_char(PRO_DRAWING_HANK_DETAILS.ENTRYDATE,'MI'))=1 and SHIFT=0