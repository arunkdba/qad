/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.drawing.wrapping;

/**
 *
 * @author admin
 */
public class DrawingWrappingTestDto {

    private String sSino;
    private String sTestno;
    private String sOrderNo;
    private String smach_name;
    private String sProcessLevel;
    private String sShade;
    private String sStdHank;
    private String sHank1;
    private String sHank2;
    private String sHank3;
    private String sHankAvg;
    private String sRsb;
    private String sCpWheel;
    private String sentrydate;
    private String sstatus;
    private String sstatuscode;
    private String sDraft;
    private String sEndno;
    private String sRemark;
    private String sMixno;
    private String sCountName;
    private String sOrderWeight;
    private String sEntryTime;
    public DrawingWrappingTestDto() {
    }

    public String getsSino() {
        return sSino;
    }

    public void setsSino(String sSino) {
        this.sSino = sSino;
    }

    public String getsTestno() {
        return sTestno;
    }

    public void setsTestno(String sTestno) {
        this.sTestno = sTestno;
    }

    public String getsOrderNo() {
        return sOrderNo;
    }

    public void setsOrderNo(String sOrderNo) {
        this.sOrderNo = sOrderNo;
    }

    public String getSmach_name() {
        return smach_name;
    }

    public void setSmach_name(String smach_name) {
        this.smach_name = smach_name;
    }

    public String getsProcessLevel() {
        return sProcessLevel;
    }

    public void setsProcessLevel(String sProcessLevel) {
        this.sProcessLevel = sProcessLevel;
    }

    public String getsShade() {
        return sShade;
    }

    public void setsShade(String sShade) {
        this.sShade = sShade;
    }

    public String getsStdHank() {
        return sStdHank;
    }

    public void setsStdHank(String sStdHank) {
        this.sStdHank = sStdHank;
    }

    public String getsHank1() {
        return sHank1;
    }

    public void setsHank1(String sHank1) {
        this.sHank1 = sHank1;
    }

    public String getsHank2() {
        return sHank2;
    }

    public void setsHank2(String sHank2) {
        this.sHank2 = sHank2;
    }

    public String getsHank3() {
        return sHank3;
    }

    public void setsHank3(String sHank3) {
        this.sHank3 = sHank3;
    }

    public String getsHankAvg() {
        return sHankAvg;
    }

    public void setsHankAvg(String sHankAvg) {
        this.sHankAvg = sHankAvg;
    }

    public String getsRsb() {
        return sRsb;
    }

    public void setsRsb(String sRsb) {
        this.sRsb = sRsb;
    }

    public String getsCpWheel() {
        return sCpWheel;
    }

    public void setsCpWheel(String sCpWheel) {
        this.sCpWheel = sCpWheel;
    }

    public String getSentrydate() {
        return sentrydate;
    }

    public void setSentrydate(String sentrydate) {
        this.sentrydate = sentrydate;
    }

    public String getSstatus() {
        return sstatus;
    }

    public void setSstatus(String sstatus) {
        this.sstatus = sstatus;
    }

    public String getsDraft() {
        return sDraft;
    }

    public void setsDraft(String sDraft) {
        this.sDraft = sDraft;
    }

    public String getsEndno() {
        return sEndno;
    }

    public void setsEndno(String sEndno) {
        this.sEndno = sEndno;
    }

    public String getsRemark() {
        return sRemark;
    }

    public void setsRemark(String sRemark) {
        this.sRemark = sRemark;
    }    

    public String getsMixno() {
        return sMixno;
    }

    public void setsMixno(String sMixno) {
        this.sMixno = sMixno;
    }

    public String getsCountName() {
        return sCountName;
    }

    public void setsCountName(String sCountName) {
        this.sCountName = sCountName;
    }

    public String getsOrderWeight() {
        return sOrderWeight;
    }

    public void setsOrderWeight(String sOrderWeight) {
        this.sOrderWeight = sOrderWeight;
    }

    public String getSstatuscode() {
        return sstatuscode;
    }

    public void setSstatuscode(String sstatuscode) {
        this.sstatuscode = sstatuscode;
    }

    public String getsEntryTime() {
        return sEntryTime;
    }

    public void setsEntryTime(String sEntryTime) {
        this.sEntryTime = sEntryTime;
    }
    
}
