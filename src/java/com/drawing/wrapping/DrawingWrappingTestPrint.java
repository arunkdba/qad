/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.drawing.wrapping;

import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;
/**
 *
 * @author admin
 */

public class DrawingWrappingTestPrint {
    
    FileWriter fw;
    String SUserName = "";
    
    int    iPage    = 1;
    String PrnTitle = "";
    String Title    = "";
    String TTitle   = "";
    String tEnd     = "";
    String tEnd1    = "";
    String tEnd2    = "";
    int    Tline    = 0;
    int    iTEnd    = 0;
    int    sino     = 0;
    int    iUnitCode = 0;
    int    laplength = 20;
    int    iTotalEntry = 0;
    int    iTotalShadeChange  = 0;

    java.util.List theList;
    java.util.List theShadeList      = null;
    
    StringBuffer sb = new StringBuffer();
    
    com.common.Common   cm;
    
    String Head1 = "";
    String Head2 = "";
    String Head3 = "";
    String Head4 = "";
    String Head5 = "";
    String SUnit = "";
    String SShift = "";
    String sFromDate = "";
    String sToDate = "";
    SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
    public DrawingWrappingTestPrint(String sFromDate, String sToDate,int iUnitCode) {
        
        InitValues();
        
        this.sFromDate = sFromDate;
        this.sToDate = sToDate;
        this.iUnitCode = iUnitCode;
        theList           = new java.util.ArrayList();
        theShadeList      = new java.util.ArrayList();

        cm      = new com.common.Common();

        InitValues();
    }

    public void GeneratePrint(java.util.List theList,String SUserName,String SUnit,String SShift){
        try{
            this.theList   = theList;
            this.SUserName = SUserName;
            this.SUnit = SUnit;
            this.SShift = SShift;
            //this.SUserName = SUserName;

            createPrn();

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void createPrn() {

        try {
            String Sos = System.getProperty("os.name").toLowerCase();
            if(Sos.trim().startsWith("wind")){
                fw = new FileWriter("d:/drawingwrappingtest.prn");
            }else{
                fw = new FileWriter("//software/QcPrint/drawingwrappingtest.prn");
            }

            toPrintPrn();
            fw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void toPrintPrn() {

        PrnHead();
        PrnBody();
        PrintTotal();
    }

    private void InitValues() {
        Title  = "";
        TTitle = "";
        Tline  = 0;
        iPage  = 1;
    }

    private void PrnHead() {

        try{
            Head1 = null;
            Head2 = null;
            Head3 = null;
            Head4 = null;
                        
            Head1 = ("gEAMARJOTHI SPINNING MILLS Ltd , QUALITY CONTROL DEPARTMENT \n DRAWING HANK DETAILS (" + SUnit + ") From " + cm.parseDate(sFromDate) + " To " + cm.parseDate(sToDate) + "\n");                 
            Head2 = " "+PrnTitle+"\n";
            
            if (SUserName.length() > 0) {
                Head3 = ("Shift        : " + SShift + " , " + "Entry User : " + SUserName + "\n");
            }
            else {
                Head3 = ("Shift        : " + SShift + " \n");
            }            
            Head4 = "Page No      : " + iPage + "FgE        File generated Time:"+cm.getSerDateTime()+"\n";

            String tHead1 = "";
            
            /*String tHead2 = " ---------------------------------------------------------------------------------------------------------------------------------------------\n";
            String tHead3 = " |S  |M/Ch|Pass|  Order No     |      Shade      |Count| Std. Hank  |Hank1|Hank2|Hank3|Avg   | C.P  |Draft|NO|Time |Changes     |Remark      |\n";
            String tHead4 = " |No | No |    |  (MixNo)      |                 |     |            |     |     |     |      |      |     |E |     |            |            |\n";
            tHead4=tHead4 + " |---|----|----|---------------|-----------------|-----|------------|-----|-----|-----|------|------|-----|--|-----|------------|------------|\n";
            tEnd    =       " |---|----|----|---------------|-----------------|-----|------------|-----|-----|-----|------|------|-----|--|-----|------------|------------|\n";
            tEnd1   =       " ---------------------------------------------------------------------------------------------------------------------------------------------\f\n";
            tEnd2   =       " ---------------------------------------------------------------------------------------------------------------------------------------------\n";
            */
            
            String tHead2 = " ---------------------------------------------------------------------------------------------------------------------------------------------\n";
            String tHead3 = " |S  |M/Ch|Pass|  Order No     |OrdWeight|      Shade      |Count| Std. Hank  |Hank1|Hank2|Hank3|Avg   | C.P  |Draft|NO|Time |Changes        |\n";
            String tHead4 = " |No | No |    |  (MixNo)      |         |                 |     |            |     |     |     |      |      |     |E |     |               |\n";
            tHead4=tHead4 + " |---|----|----|---------------|---------|-----------------|-----|------------|-----|-----|-----|------|------|-----|--|-----|---------------|\n";
            tEnd    =       " |---|----|----|---------------|---------|-----------------|-----|------------|-----|-----|-----|------|------|-----|--|-----|---------------|\n";
            tEnd1   =       " --------------------------------------------------------------------------------------------------------------------------------------------|\n";
            tEnd2   =       " --------------------------------------------------------------------------------------------------------------------------------------------\n";
            

            Title = "";
            TTitle = "";

            Title = (" " + Head1 + " " + Head2 + " " + Head3 + " " + Head4);
            TTitle = (tHead1 + tHead2 + tHead3 + tHead4);

            Title  = "";
            TTitle = "";
            
            Title  = " " + Head1 + " " + Head2 + " " + Head3 + " " + Head5 + " " + Head4;
            TTitle = tHead1 + tHead2 + tHead3 + tHead4;

            fw . write(Title);
            fw . write(TTitle);

            Tline += 7;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void PrnBody() {

        try{
            iTotalShadeChange = 0;
            iTotalEntry = 0;
            String sStartTime ="",sEndTime="";
            
        if (theList.size() > 0) {

                String      Shade  = "";

                String laps      = "";
                String SData     = "";

                java.util.List shadelist = new java.util.ArrayList();

            for (int i = 0; i < theList.size(); i++) {
                iTotalEntry++;
                if ((theList.size()-1) == i) {
                    iTEnd = 1;
                }

                DrawingWrappingTestDto drawingWrappingTestDto = (DrawingWrappingTestDto)theList.get(i);

                Shade         = cm.parseNull(drawingWrappingTestDto.getsShade());

                if(i==0){

                        sStartTime = drawingWrappingTestDto.getsEntryTime();
                        sEndTime = drawingWrappingTestDto.getsEntryTime();

                }else{
                        //sStartTime = spreviousTime;
                        sEndTime = drawingWrappingTestDto.getsEntryTime();
                }
                Date d1 = null;
                Date d2 = null;
                 
                d1 = format.parse(sStartTime);
                d2 = format.parse(sEndTime);

                long diff = d2.getTime() - d1.getTime();

                long diffHours = diff / (60 * 60 * 1000) % 24;
                long diffMinutes = diff / (60 * 1000) % 60;
                
                 laps      = "";
                 SData     = "";

                 shadelist = new java.util.ArrayList();

                 shadelist =  getShadeLines(Shade, 17);

                int big = 1;
                int shadelistsize = 0;

                if(shadelist.size()>0){ shadelistsize = shadelist.size()-1; big = shadelist.size();}

                String shadename = "";
                if(drawingWrappingTestDto.getSstatuscode().equals("8") || drawingWrappingTestDto.getSstatuscode().equals("12")){

                    iTotalShadeChange++;
                }
                
                 if(diffHours >= 2)
                 {
                    sStartTime = sEndTime;
                    SData =    " |" + cm.Cad("", 3) +
                                "|" + cm.Cad("", 4) +
                                "|" + cm.Cad("", 4) +
                                "|" + cm.Cad("", 15) +
                                "|" + cm.Pad("", 9) +
                                "|" + cm.Pad("", 17) +
                                "|" + cm.Rad("", 5) +
                                "|" + cm.Rad("", 12) +
                                "|" + cm.Pad("", 5) +
                                "|" + cm.Pad("", 5) +
                                "|" + cm.Pad("", 5) +
                                "|" + cm.Pad("", 6) +
                                "|" + cm.Pad("", 6) +
                                "|" + cm.Pad("", 5) +
                                "|" + cm.Pad("", 2) +
                                "|" + cm.Pad("", 5) +
                                "|" + cm.Pad("", 15) + "|\n";
                    PrnWriter(laps);
                 }

                for (int l = 0; l < big; l++) {

                    shadename = "";

                        if(shadelist.size()>0){
                            if(l<=shadelistsize){
                                shadename = cm.parseNull(shadelist.get(l).toString());
                            }
                        }

                    if (l == 0) {
                       String SMixNo=cm.parseNull(drawingWrappingTestDto.getsMixno());
                       String SOrdNo = cm.parseNull(drawingWrappingTestDto.getsOrderNo());
                       
                       SData = " |" + cm.Cad(cm.parseNull(String.valueOf(drawingWrappingTestDto.getsSino())), 3) + 
                                "|" + cm.Cad(cm.parseNull(drawingWrappingTestDto.getSmach_name()), 4) +
                                "|" + cm.Cad(cm.parseNull(drawingWrappingTestDto.getsProcessLevel()), 4) + 
                                "|" + cm.Cad(SOrdNo+"("+SMixNo+")", 15)+
                                "|" + cm.Rad(cm.parseNull(drawingWrappingTestDto.getsOrderWeight()), 9) +
                                "|" + cm.Pad(shadename, 17) +
                                "|" + cm.Cad(cm.parseNull(drawingWrappingTestDto.getsCountName()), 5) +
                                "|" + cm.Rad(cm.parseNull(drawingWrappingTestDto.getsStdHank()), 12) + 
                                "|" + cm.Rad(cm.parseNull(drawingWrappingTestDto.getsHank1()), 5) + 
                                "|" + cm.Rad(cm.parseNull(drawingWrappingTestDto.getsHank2()), 5) + 
                                "|" + cm.Rad(cm.parseNull(drawingWrappingTestDto.getsHank3()), 5) + 
                                "|" + cm.Rad(cm.parseNull(drawingWrappingTestDto.getsHankAvg()), 6) + 
                                "|" + cm.Rad(cm.parseNull(drawingWrappingTestDto.getsCpWheel()), 6) + 
                                "|" + cm.Rad(cm.parseNull(drawingWrappingTestDto.getsDraft()), 5) + 
                                "|" + cm.Rad(cm.parseNull(drawingWrappingTestDto.getsEndno()), 2) + 
                                "|" + cm.Rad(cm.parseNull(drawingWrappingTestDto.getSentrydate()), 5) + 
                                "|" + cm.Pad(cm.parseNull(drawingWrappingTestDto.getSstatus()), 15) + "|\n";
                                //"|" + cm.Pad(cm.parseNull(drawingWrappingTestDto.getsRemark()), 12) + "|\n";

                        laps += SData;
                    } else {
                        Tline++;
                        //5+7+7+12+20+12+7+7+7+7+9+13+16+9
                       SData = " |" + cm.Cad("", 3) +
                                "|" + cm.Cad("", 4) +
                                "|" + cm.Cad("", 4) +
                                "|" + cm.Cad("", 15) +
                                "|" + cm.Pad("", 9) +
                                "|" + cm.Pad(shadename, 17) +
                                "|" + cm.Rad("", 5) +
                                "|" + cm.Rad("", 12) +
                                "|" + cm.Pad("", 5) +
                                "|" + cm.Pad("", 5) +
                                "|" + cm.Pad("", 5) +
                                "|" + cm.Pad("", 6) +
                                "|" + cm.Pad("", 6) +
                                "|" + cm.Pad("", 5) +
                                "|" + cm.Pad("", 2) +
                                "|" + cm.Pad("", 5) +
                                "|" + cm.Pad("", 15) + "|\n";
                                //"|" + cm.Pad("", 12) + "|\n";

                        laps += SData;
                    }
                }
                PrnWriter(laps);
            }
        }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    private void PrnWriter(String Data) {

        try {
            Tline = Tline + 2;
            
            if (Tline <= 64) {
                if (Tline >= 10) {
                    fw.write(tEnd);
                }
                fw.write(Data);

                if (iTEnd == 1 && Tline == 64) {

                    fw . write(tEnd1);
                    Tline = 0;
                } else if (iTEnd == 1 && Tline != 64) {
                    fw . write(tEnd1);
                    Tline = 0;
                }

            } else {

                fw.write(tEnd1);

                iPage = iPage + 1;
                Tline = 0;

                PrnHead();

                Tline = Tline + 2;
                if (Tline <= 64) {
                    if (Tline > 11) {
                        fw.write(tEnd);
                    }
                    fw.write(Data);


                    if (iTEnd == 1 && Tline == 64) {

                        fw.write(tEnd1);
                        Tline = 0;

                    } else if (iTEnd == 1 && Tline != 64) {

                        fw.write(tEnd1);
                        Tline = 0;
                    }
                }
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void PrintTotal(){
        String sm="";
        try{
            if (iUnitCode == 1) {
                 sm = "A";
            }
            else if (iUnitCode == 2) {
                 sm = "B";
            }
            else if (iUnitCode == 10)
            {
                 sm = "C";
            }

        fw.write(" Total Entry : " + iTotalEntry +" " +cm.Space(15)+" No .Of. Shade Change : "+iTotalShadeChange+"\n\n\n");

        fw.write(cm.Space(25)+" QA " +cm.Space(25) +" QAO "+cm.Space(35)+"SM("+sm+")"+"\n");
        
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }

    public java.util.List getShadeLines(String Shade, int iLength) {
        java.util.List theShadeList = new java.util.ArrayList();
        int iLen = 0;
        int iLen1 = 0;
        int iLen2 = 0;

        BigDecimal bd1, bd2;
        bd1 = new BigDecimal(Shade.length());
        bd2 = new BigDecimal(iLength);
        iLen = bd1.divide(bd2, RoundingMode.UP).intValue();

        iLen1 = 0;
        iLen2 = iLength;

        for (int i = 0; i < iLen; i++) {
            if (i == (iLen - 1)) {
                theShadeList.add(Shade.substring(iLen1));
            } else {
                theShadeList.add(Shade.substring(iLen1, iLen2));
                iLen1 = iLen2;
                iLen2 += iLength;
            }
        }
        return theShadeList;
    }
}