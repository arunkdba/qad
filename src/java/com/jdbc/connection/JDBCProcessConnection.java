package com.jdbc.connection;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author root
 */
public class JDBCProcessConnection {

     static JDBCProcessConnection connect = null;

	Connection theConnection = null;

     String SDriver   = "oracle.jdbc.OracleDriver";
     //String SDSN      = "jdbc:oracle:thin:@172.16.2.28:1521:arun";
     String SDSN      = "jdbc:oracle:thin:@117.239.247.18:1530:arun";
     String SUser     = "process";
     String SPassword = "process";

     private JDBCProcessConnection()
     {
            try
            {
                Class          . forName(SDriver);
                theConnection  = DriverManager.getConnection(SDSN,SUser,SPassword);
            }catch(Exception e) {
                System.out.println("JDBCConnection : "+e);
            }
     }

     public static JDBCProcessConnection getJDBCConnection()
	{
		if (connect == null)
		{
                    connect = new JDBCProcessConnection();
		}
		return connect;
	}

	public Connection getConnection()
	{
		return theConnection;
	}

	public void finalize()
	{
		if (theConnection != null)
		{
			try
			{
				theConnection.close();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}
}