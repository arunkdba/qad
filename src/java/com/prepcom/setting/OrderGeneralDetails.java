/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prepcom.setting;

import com.jdbc.connection.JDBCProcessConnection;
import com.jdbc.connection.JDBCScmConnection;
import com.jdbc.connection.JDBCProcessaConnection;

/**
 *
 * @author root
 */
public class OrderGeneralDetails {

    java.sql.Connection theProcessConnection = null;
    java.sql.Connection theScmConnection = null;

    com.common.Common   common;

    public OrderGeneralDetails() {
        common  = new com.common.Common();
    }

    public OrderGeneralDetailsClass getOrderGeneralDetails(String SOrderNo,int iOrderType,int iUnitCode)  {
        //java.util.HashMap theMap = new java.util.HashMap();
        OrderGeneralDetailsClass generalDetailsClass = new OrderGeneralDetailsClass();
        StringBuffer    sb  = new StringBuffer();
            if(iUnitCode == 1 || iUnitCode ==2 )
            {
                    if(iOrderType==0){
                        sb  . append(" select processingofindent.rorderno, orderdate, indentno, processingofindent.COUNTCODE, processingofindent.CLOTHCHECKSTATUS, partymaster.partyname, ");
                        sb  . append(" yshnm as shadename, countname, processingofindent.processtype, layer, batch, REALISATION_WEIGHT as mixingqty, ");
                        sb  . append(" processingofindent.Weight as orderqty, REALISATION_PER, CORRECTIONMIXING , DEPTH, 'C : '||COTTONPER||' V: '||VISCOSE||' P: '||POLYSTER as blend, ");
                        sb  . append(" processingofindent.WASTE, processingofindent.CONTAMINATION, CLOTHCHECKTPI , CLOTHCHECKCOUNTNAME, ");
                        sb  . append(" processingofindent.countcode, regularorder.weight, ordersplitdetails.baseorderno, ordersplitdetails.splitorderno, ordersplitdetails.splittypecode, ordersplitdetails.deptcode, ");
                        sb  . append(" ordersplitdetails.deptlevelcode, processingofindent.processgroupcode, processingtype.darkbase, processingtype.whitebase, decode(regularorder.cancelstatus,0,'',1,'Order Cancelled',regularorder.cancelstatus) as CancelStatus  from processingofindent ");
                        sb  . append(" inner join scm.regularorder on regularorder.rorderno = processingofindent.rorderno and regularorder.unitcode="+iUnitCode+" and processingofindent.rorderno = ? ");
                        sb  . append(" inner join scm.partymaster on partymaster.partycode = regularorder.partycode ");
                        sb  . append(" inner join scm.processingtype on processingtype.processcode = processingofindent.processcode and processingtype.processgroupcode = processingofindent.processgroupcode ");
                        sb  . append(" left  join scm.ordersplitdetails on ordersplitdetails.baseorderno = processingofindent.rorderno or ordersplitdetails.splitorderno = processingofindent.rorderno ");
                        sb  . append(" order by processingofindent.rorderno, ordersplitdetails.splittypecode ");
                    }else if(iOrderType==1){
                        sb  . append(" select processingofindent_correction.rorderno, orderdate, indentno, processingofindent_correction.COUNTCODE, processingofindent_correction.CLOTHCHECKSTATUS, partymaster.partyname,");
                        sb  . append(" yshnm as shadename, countname, processingofindent_correction.processtype,layer, batch, REALISATION_WEIGHT as mixingqty, ");
                        sb  . append(" processingofindent_correction.Weight as orderqty, REALISATION_PER, CORRECTIONMIXING , DEPTH, 'C : '||COTTONPER||' V: '||VISCOSE||' P: '||POLYSTER as blend,");
                        sb  . append(" processingofindent_correction.WASTE, processingofindent_correction.CONTAMINATION, CLOTHCHECKTPI, CLOTHCHECKCOUNTNAME, ");
                        sb  . append(" processingofindent_correction.countcode, regularorder.weight, ordersplitdetails.baseorderno, ordersplitdetails.splitorderno, ordersplitdetails.splittypecode, ordersplitdetails.deptcode, ");
                        sb  . append(" ordersplitdetails.deptlevelcode, processingofindent_correction.processgroupcode, processingtype.darkbase, processingtype.whitebase, decode(regularorder.cancelstatus,0,'',1,'Order Cancelled',regularorder.cancelstatus) as CancelStatus    from processingofindent_correction ");
                        sb  . append(" inner join scm.regularorder on regularorder.rorderno = processingofindent_correction.rorderno  and regularorder.unitcode="+iUnitCode+"  and processingofindent_correction.rorderno = ? ");
                        sb  . append(" inner join scm.partymaster on partymaster.partycode = regularorder.partycode");
                        sb  . append(" inner join scm.processingtype on processingtype.processcode = processingofindent_correction.processcode and processingtype.processgroupcode = processingofindent_correction.processgroupcode ");
                        sb  . append(" left  join scm.ordersplitdetails on ordersplitdetails.baseorderno = processingofindent_correction.rorderno or ordersplitdetails.splitorderno = processingofindent_correction.rorderno   ");
                    }
            }
                //Cunit
                if(iUnitCode == 10 || iUnitCode ==12)
                {
                    if(iOrderType==0){
                        sb  . append(" select cunit_processgofindent.rorderno, orderdate, indentno, cunit_processgofindent.COUNTCODE, cunit_processgofindent.CLOTHCHECKSTATUS, partymaster.partyname, ");
                        sb  . append(" yshnm as shadename, countname, cunit_processgofindent.processtype, layer, batch, REALISATION_WEIGHT as mixingqty, ");
                        sb  . append(" cunit_processgofindent.Weight as orderqty, REALISATION_PER, CORRECTIONMIXING , DEPTH, 'C : '||COTTONPER||' V: '||VISCOSE||' P: '||POLYSTER as blend, ");
                        sb  . append(" cunit_processgofindent.WASTE, cunit_processgofindent.CONTAMINATION, CLOTHCHECKTPI , CLOTHCHECKCOUNTNAME, ");
                        sb  . append(" cunit_processgofindent.countcode, regularorder.weight, ordersplitdetails.baseorderno, ordersplitdetails.splitorderno, ordersplitdetails.splittypecode, ordersplitdetails.deptcode, ");
                        sb  . append(" ordersplitdetails.deptlevelcode, cunit_processgofindent.processgroupcode, processingtype.darkbase, processingtype.whitebase, decode(regularorder.cancelstatus,0,'',1,'Order Cancelled',regularorder.cancelstatus) as CancelStatus  from cunit_processgofindent ");
                        sb  . append(" inner join scm.regularorder on regularorder.rorderno = cunit_processgofindent.rorderno and (regularorder.unitcode=10 or regularorder.unitcode=12) and cunit_processgofindent.rorderno = ? ");
                        sb  . append(" inner join scm.partymaster on partymaster.partycode = regularorder.partycode ");
                        sb  . append(" inner join scm.processingtype on processingtype.processcode = cunit_processgofindent.processcode and processingtype.processgroupcode = cunit_processgofindent.processgroupcode ");
                        sb  . append(" left  join scm.ordersplitdetails on ordersplitdetails.baseorderno = cunit_processgofindent.rorderno or ordersplitdetails.splitorderno = cunit_processgofindent.rorderno ");
                        sb  . append(" order by cunit_processgofindent.rorderno, ordersplitdetails.splittypecode ");
                    }else if(iOrderType==1){
                        sb  . append(" select cunit_processofindent_corr.rorderno, orderdate, indentno, cunit_processofindent_corr.COUNTCODE, cunit_processofindent_corr.CLOTHCHECKSTATUS, partymaster.partyname,");
                        sb  . append(" yshnm as shadename, countname, cunit_processofindent_corr.processtype,layer, batch, REALISATION_WEIGHT as mixingqty, ");
                        sb  . append(" cunit_processofindent_corr.Weight as orderqty, REALISATION_PER, CORRECTIONMIXING , DEPTH, 'C : '||COTTONPER||' V: '||VISCOSE||' P: '||POLYSTER as blend,");
                        sb  . append(" cunit_processofindent_corr.WASTE, cunit_processofindent_corr.CONTAMINATION, CLOTHCHECKTPI, CLOTHCHECKCOUNTNAME, ");
                        sb  . append(" cunit_processofindent_corr.countcode, regularorder.weight, ordersplitdetails.baseorderno, ordersplitdetails.splitorderno, ordersplitdetails.splittypecode, ordersplitdetails.deptcode, ");
                        sb  . append(" ordersplitdetails.deptlevelcode, cunit_processofindent_corr.processgroupcode, processingtype.darkbase, processingtype.whitebase, decode(regularorder.cancelstatus,0,'',1,'Order Cancelled',regularorder.cancelstatus) as CancelStatus    from cunit_processofindent_corr ");
                        sb  . append(" inner join scm.regularorder on regularorder.rorderno = cunit_processofindent_corr.rorderno  and (regularorder.unitcode=10 or regularorder.unitcode=12)  and cunit_processofindent_corr.rorderno = ? ");
                        sb  . append(" inner join scm.partymaster on partymaster.partycode = regularorder.partycode");
                        sb  . append(" inner join scm.processingtype on processingtype.processcode = cunit_processofindent_corr.processcode and processingtype.processgroupcode = cunit_processofindent_corr.processgroupcode ");
                        sb  . append(" left  join scm.ordersplitdetails on ordersplitdetails.baseorderno = cunit_processofindent_corr.rorderno or ordersplitdetails.splitorderno = cunit_processofindent_corr.rorderno   ");
                    }
                }
                
                //System.out.print("OrderGenDet--> : "+sb.toString());
                
        try {
            if(iUnitCode ==1)
            {
                if(theProcessConnection==null){
                    JDBCProcessaConnection jdbc = JDBCProcessaConnection.getJDBCConnection();
                    theProcessConnection       = jdbc.getConnection();
                }
            }
            else
            {
                if(theProcessConnection==null){
                    JDBCProcessConnection jdbc = JDBCProcessConnection.getJDBCConnection();
                    theProcessConnection       = jdbc.getConnection();
                }
            }
            java.sql.PreparedStatement  pstmt   = theProcessConnection.prepareStatement(sb.toString());
                                        pstmt   . setString(1, SOrderNo);
            java.sql.ResultSet          rset    = pstmt.executeQuery();

            while (rset.next()) {
                generalDetailsClass . setRorderno            (rset.getString(1));
                generalDetailsClass . setOrderdate           (rset.getString(2));
                generalDetailsClass . setIndentno            (rset.getString(3));
                generalDetailsClass . setCountcode           (rset.getString(4));
                generalDetailsClass . setClothcheckstatus    (rset.getString(5));
                generalDetailsClass . setPartyname           (rset.getString(6));
                generalDetailsClass . setShade               (rset.getString(7));
                generalDetailsClass . setProcesstype         (rset.getString(9));
                generalDetailsClass . setLayer               (rset.getString(10));
                generalDetailsClass . setBatch               (rset.getString(11));
                generalDetailsClass . setMixweight           (rset.getString(12));
                generalDetailsClass . setRealisation         (rset.getString(14));
                generalDetailsClass . setCorrectionmixing    (rset.getString(15));
                generalDetailsClass . setDepth               (rset.getString(16));
                generalDetailsClass . setBlend               (rset.getString(17));
                generalDetailsClass . setWaste               (rset.getString(18));
                generalDetailsClass . setContamination       (rset.getString(19));
                generalDetailsClass . setClothchecktpi       (rset.getString(20));
                generalDetailsClass . setClothcheckcountname (rset.getString(21));
                if(iOrderType==0){
                if(rset.getInt(22)==rset.getInt(4)) {
                generalDetailsClass . setCountname           (rset.getString(8));
                }else{
                generalDetailsClass . setCountname           ("Contact Process Dept");
                }
                
                if(rset.getInt(13)==rset.getInt(23)) {
                generalDetailsClass . setOrderweight         (rset.getString(13));
                }else{
                generalDetailsClass . setOrderweight         ("Contact Process Dept");
                }
                }else {
                generalDetailsClass . setCountname           (rset.getString(8));
                generalDetailsClass . setOrderweight         (rset.getString(13));
                }
                
                generalDetailsClass . setiProcessgroupcode(common.toInt(rset.getString(29)));
                generalDetailsClass . setIdarkbase(common.toInt(rset.getString(30)));
                generalDetailsClass . setIwhitebase(common.toInt(rset.getString(31)));
                
                generalDetailsClass . setIwhitebase(common.toInt(rset.getString(31)));
             
                //Group & Split Order Details
                generalDetailsClass . setOs_baseorderno(rset.getString(24));
                generalDetailsClass . setOs_splitorderno(rset.getString(25));
                generalDetailsClass . setOs_splittype(common.toInt(rset.getString(26)));
                generalDetailsClass . setOs_splitdept(common.toInt(rset.getString(27)));
                generalDetailsClass . setOs_splitdeptlvlcode(common.toInt(rset.getString(28)));
                
                generalDetailsClass . setSordercancelstatus(common.parseNull(rset.getString(32)));
                
                
                if(common.toInt(rset.getString(26))!=0){
                  int ibaseorsub = 0;
                  if(rset.getString(24).trim().equals(rset.getString(1).trim())){
                      ibaseorsub = 1;
                  }else
                  if(rset.getString(25).trim().equals(rset.getString(1).trim())){
                      ibaseorsub = 2;
                  }
                  
                   generalDetailsClass . setOs_baseorsuborder(ibaseorsub);
                }
                
            }
            rset.close();
            rset = null;
            pstmt.close();
            pstmt = null;
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return generalDetailsClass;
    }
    
    public java.util.List getGroupedOrderDetails(String SOrderNo,int iSplitType,int ibaseorsub){
        java.util.List theGroupOrderList = new java.util.ArrayList();
        StringBuffer sb = new StringBuffer();
            String STable =" ORDERGROUPMASTER";
            if(ibaseorsub==2){
                STable =" ORDERGROUPDETAILS";
            }
        
                if(iSplitType==2){
                     sb . append(" SELECT DISTINCT ORDERNO, REALISATION_PER, WEIGHT, COUNTNAME, YSHNM, BaseOrder, concat(DEPTNAME,decode(DEPTLEVELNAME,'NONE','',concat(' - ',DEPTLEVELNAME))) as SplitDept  FROM (  ");
                     sb . append(" SELECT ORDERGROUPMASTER.ORDERNO, SAMPLETOREGULAR.REALISATION_PER, REGULARORDER.WEIGHT, YARNCOUNT.COUNTNAME, YARNM.YSHNM, 1 as BaseOrder, ");
                     sb . append(" ORDERSPLITDEPT.DEPTNAME, ORDERSPLITDEPTLEVEL.DEPTLEVELNAME  FROM SCM.ORDERGROUPMASTER  ");
                     sb . append(" INNER JOIN SCM.ORDERGROUPDETAILS ON ORDERGROUPDETAILS.GROUPID = ORDERGROUPMASTER.ID  ");
                     sb . append(" INNER JOIN SCM.SAMPLETOREGULAR ON SAMPLETOREGULAR.REGULARORDERNO = ORDERGROUPMASTER.ORDERNO  ");
                     sb . append(" INNER JOIN SCM.REGULARORDER ON REGULARORDER.RORDERNO = ORDERGROUPMASTER.ORDERNO ");
                     sb . append(" INNER JOIN SCM.YARNCOUNT ON YARNCOUNT.COUNTCODE = REGULARORDER.COUNTCODE ");
                     sb . append(" INNER JOIN SCM.YARNM ON YARNM.YSHCD = REGULARORDER.SHADEID ");
                     sb . append(" LEFT JOIN SCM.ORDERSPLITDETAILS ON ORDERSPLITDETAILS.SPLITORDERNO = REGULARORDER.RORDERNO AND ORDERSPLITDETAILS.SPLITTYPECODE=2  ");
                     sb . append(" LEFT JOIN SCM.ORDERSPLITDEPT ON ORDERSPLITDEPT.DEPTCODE = ORDERSPLITDETAILS.DEPTCODE ");
                     sb . append(" LEFT JOIN SCM.ORDERSPLITDEPTLEVEL ON ORDERSPLITDEPTLEVEL.DEPTLEVELCODE = ORDERSPLITDETAILS.DEPTLEVELCODE ");
                     sb . append(" WHERE  "+STable+".ORDERNO = ? ");
                     sb . append(" UNION ALL  ");
                     sb . append(" SELECT ORDERGROUPDETAILS.ORDERNO, 0 as REALISATION_PER,REGULARORDER.WEIGHT, YARNCOUNT.COUNTNAME, YARNM.YSHNM, 2 as BaseOrder, ");
                     sb . append(" ORDERSPLITDEPT.DEPTNAME, ORDERSPLITDEPTLEVEL.DEPTLEVELNAME  FROM SCM.ORDERGROUPMASTER  ");
                     sb . append(" INNER JOIN SCM.ORDERGROUPDETAILS ON ORDERGROUPDETAILS.GROUPID = ORDERGROUPMASTER.ID  ");
                     sb . append(" INNER JOIN SCM.REGULARORDER ON REGULARORDER.RORDERNO = ORDERGROUPDETAILS.ORDERNO  ");
                     sb . append(" INNER JOIN SCM.YARNCOUNT ON YARNCOUNT.COUNTCODE = REGULARORDER.COUNTCODE  ");
                     sb . append(" INNER JOIN SCM.YARNM ON YARNM.YSHCD = REGULARORDER.SHADEID  ");
                     sb . append(" LEFT JOIN SCM.ORDERSPLITDETAILS ON ORDERSPLITDETAILS.SPLITORDERNO = REGULARORDER.RORDERNO AND ORDERSPLITDETAILS.SPLITTYPECODE=2 ");
                     sb . append(" LEFT JOIN SCM.ORDERSPLITDEPT ON ORDERSPLITDEPT.DEPTCODE = ORDERSPLITDETAILS.DEPTCODE ");
                     sb . append(" LEFT JOIN SCM.ORDERSPLITDEPTLEVEL ON ORDERSPLITDEPTLEVEL.DEPTLEVELCODE = ORDERSPLITDETAILS.DEPTLEVELCODE ");
                     sb . append(" WHERE "+STable+".ORDERNO = ? ) ORDER BY BaseOrder  ");
                }
        
                if(iSplitType==1)  {
                     sb . append(" SELECT DISTINCT RORDERNO, REALISATION_PER, WEIGHT, COUNTNAME, YSHNM, BaseOrder, concat(DEPTNAME,decode(DEPTLEVELNAME,'NONE','',concat(' - ',DEPTLEVELNAME))) as SplitDept  FROM ( ");
                     sb . append(" SELECT ORDERSPLITDETAILS.BASEORDERNO as RORDERNO, SAMPLETOREGULAR.REALISATION_PER, REGULARORDER.WEIGHT, YARNCOUNT.COUNTNAME, YARNM.YSHNM, 1 as BaseOrder, ");
                     sb . append(" ORDERSPLITDEPT.DEPTNAME, ORDERSPLITDEPTLEVEL.DEPTLEVELNAME  FROM SCM.ORDERSPLITDETAILS ");
                     sb . append(" INNER JOIN SCM.SAMPLETOREGULAR ON SAMPLETOREGULAR.REGULARORDERNO = ORDERSPLITDETAILS.BASEORDERNO ");
                     sb . append(" INNER JOIN SCM.REGULARORDER ON REGULARORDER.RORDERNO = ORDERSPLITDETAILS.BASEORDERNO ");
                     sb . append(" INNER JOIN SCM.YARNCOUNT ON YARNCOUNT.COUNTCODE = REGULARORDER.COUNTCODE ");
                     sb . append(" INNER JOIN SCM.YARNM ON YARNM.YSHCD = REGULARORDER.SHADEID ");
                     sb . append(" LEFT JOIN SCM.ORDERSPLITDEPT ON ORDERSPLITDEPT.DEPTCODE = ORDERSPLITDETAILS.DEPTCODE ");
                     sb . append(" LEFT JOIN SCM.ORDERSPLITDEPTLEVEL ON ORDERSPLITDEPTLEVEL.DEPTLEVELCODE = ORDERSPLITDETAILS.DEPTLEVELCODE ");
                     sb . append(" WHERE ORDERSPLITDETAILS.SPLITTYPECODE=1 AND (ORDERSPLITDETAILS.BASEORDERNO = ? OR ORDERSPLITDETAILS.SPLITORDERNO = '"+SOrderNo+"' ) ");
                     sb . append(" UNION ALL ");
                     sb . append(" SELECT ORDERSPLITDETAILS.SPLITORDERNO as RORDERNO, 0 as REALISATION_PER,REGULARORDER.WEIGHT, YARNCOUNT.COUNTNAME, YARNM.YSHNM, 2 as BaseOrder, ");
                     sb . append(" ORDERSPLITDEPT.DEPTNAME, ORDERSPLITDEPTLEVEL.DEPTLEVELNAME  FROM SCM.ORDERSPLITDETAILS ");
                     sb . append(" INNER JOIN SCM.REGULARORDER ON REGULARORDER.RORDERNO = ORDERSPLITDETAILS.SPLITORDERNO ");
                     sb . append(" INNER JOIN SCM.YARNCOUNT ON YARNCOUNT.COUNTCODE = REGULARORDER.COUNTCODE ");
                     sb . append(" INNER JOIN SCM.YARNM ON YARNM.YSHCD = REGULARORDER.SHADEID ");
                     sb . append(" LEFT JOIN SCM.ORDERSPLITDEPT ON ORDERSPLITDEPT.DEPTCODE = ORDERSPLITDETAILS.DEPTCODE ");
                     sb . append(" LEFT JOIN SCM.ORDERSPLITDEPTLEVEL ON ORDERSPLITDEPTLEVEL.DEPTLEVELCODE = ORDERSPLITDETAILS.DEPTLEVELCODE ");
                     sb . append(" WHERE  ORDERSPLITDETAILS.SPLITTYPECODE=1 AND (ORDERSPLITDETAILS.BASEORDERNO = ? OR ORDERSPLITDETAILS.SPLITORDERNO = '"+SOrderNo+"' ) ) ORDER BY BaseOrder  ");
                }

        try{
            
            if(theScmConnection==null){
                JDBCScmConnection jdbc = JDBCScmConnection.getJDBCConnection();
                theScmConnection       = jdbc.getConnection();
            }
            java.sql.PreparedStatement  pstmt   = theScmConnection.prepareStatement(sb.toString());
                                        pstmt   . setString(1, SOrderNo);
                                        pstmt   . setString(2, SOrderNo);
            java.sql.ResultSet          rset    = pstmt.executeQuery();

            java.util.HashMap theMap = new java.util.HashMap();
            double dMixWeight = 0;
            double dRealiPer  = 0;
            int i =0;
            while (rset.next()) {
                    theMap = new java.util.HashMap();
                    if(i==0){i++;
                    dRealiPer  = common.toDouble(rset.getString(2));
                    }
                    dMixWeight = common.toDouble(rset.getString(3))/dRealiPer*100;
                    
                    theMap . put("RORDERNO",            rset.getString(1));
                    theMap . put("MIXWEIGHT",           common.getRound(dMixWeight,2));
                    theMap . put("ORDWEIGHT",           rset.getString(3));
                    theMap . put("COUNTNAME",           rset.getString(4));
                    theMap . put("SHADE",               rset.getString(5));
                    theMap . put("SPLITDEPTNAME",       rset.getString(7));
                    
               theGroupOrderList.add(theMap);
            }
            rset.close();
            rset = null;
            pstmt.close();
            pstmt = null;
            
        }catch(Exception ex){
            ex.printStackTrace();
        }
        
        return theGroupOrderList;
    }
    
    public java.util.HashMap getFibreDetails(String sorderno,int ordertype) {

        StringBuffer        sb              = null;
        String              SConversionNo   = null;
        String              sqty            = null;
        java.util.List      list            = null;
                            list            = new java.util.ArrayList();
                            
      try {
            if(theProcessConnection==null){
                JDBCProcessaConnection jdbc = JDBCProcessaConnection.getJDBCConnection();
                theProcessConnection       = jdbc.getConnection();
            }
            if(theScmConnection==null){
                JDBCScmConnection jdbc = JDBCScmConnection.getJDBCConnection();
                theScmConnection       = jdbc.getConnection();
            }
          
      }catch(Exception ex){
          ex.printStackTrace();
      }
                                              
        java.util.HashMap   hm              = getConversiondetails(sorderno,ordertype);
                            SConversionNo   = (String)hm.get("CONVERSIONNO");
                            sqty            = (String)hm.get("MIXQTY");

                            
                            sb  = new StringBuffer();
                            sb  . append(" Select * from (");
                            sb  . append(" select t1.FibreCode,SampleToRegularDetails.Weight,");
                            sb  . append(" concat(concat(concat(concat(concat(concat(Fibre.FibreName,decode(Denier,' ',' ',','||Denier)),");
                            sb  . append(" decode(FibreLength,' ', ' ','*'||FibreLength)),");
                            sb  . append(" decode(t1.MixLotNo,'NONE','',t1.MixLotNo)),");
                            sb  . append(" decode(t1.MixLotName,'NONE','',','||t1.MixLotName)),','||substr(Supplier.Name,0,5)),','||FibreForm.FormName)");
                            sb  . append(" as FibreName,SampleToRegularDetails.AvailWeight,");
                            sb  . append(" SampleToRegularDetails.IndentWeight");
                            sb  . append(" from");
                            sb  . append(" (select distinct Receipt.AcCode as AcCode,Receipt.FibreCode as FibreCode,Receipt.MixLotNo  as MixLotNo,MixLot.MixLotName as MixLotName from");
                            sb  . append(" (Select  max(Id) as Id,FibreCode");
                            sb  . append(" from Receipt");
                            sb  . append(" group by FibreCode)  t");
                            sb  . append(" inner join Receipt on t.Id= Receipt.Id");
                            sb  . append(" Inner join MixLot on MixLot.MixLotNo=Receipt.MixLotNo");
                            sb  . append(" and t.FibreCode=Receipt.FibreCode");
                            sb  . append(" ) t1");
                            sb  . append(" Inner join Fibre on Fibre.FibreCode=t1.FibreCode");
                            sb  . append(" Inner join SampleToRegularDetails on SampleToRegularDetails.FibreCode=t1.fibreCode");
                            sb  . append(" Inner join Supplier on t1.AcCode=Supplier.AcCode");
                            sb  . append(" Left  join FibreForm on FibreForm.FormCode=SampleTORegularDetails.FibreFormCode");
                            sb  . append(" where SampleToRegularDetails.ConversionNo="+SConversionNo);
                            sb  . append(" union All");
                            sb  . append(" select MixLotMaster.FibreCode,SampleToRegularDetails.Weight,");
                            sb  . append(" concat(concat(concat(concat(MixLotMaster.MixLotNo,','||MixLot.MixLotName),','||WebType.WebTypeName),','||Noils||'%'),','||FibreForm.FormName) as Fibrename,");
                            sb  . append(" SampleToRegularDetails.AvailWeight,");
                            sb  . append(" SampleToRegularDetails.IndentWeight");
                            sb  . append(" from ((MixLotMaster");
                            sb  . append(" Inner join MixLot on MixLot.MixLotNo=MixLotMaster.MixLotNo)");
                            sb  . append(" Inner join WebType on WebType.WebTypeCode=MixLotMaster.WebType)");
                            sb  . append(" Inner join SampleToRegularDetails on SampleToRegularDetails.FibreCode=MixLotMaster.FibreCode");
                            sb  . append(" Left  join FibreForm on FibreForm.FormCode=SampleTORegularDetails.FibreFormCode");
                            sb  . append(" where SampleToRegularDetails.ConversionNo="+SConversionNo);
                            sb  . append(" union All");
                            sb  . append(" select WasteCodeALlocation.FibreCode as FibreCode,SampleToRegularDetails.Weight,");
                            sb  . append(" concat(concat(WasteVariety.VarietyName,','||WasteCodeAllocation.LotNo),','||FibreForm.FormName) as FibreName,");
                            sb  . append(" SampleToRegularDetails.AvailWeight,");
                            sb  . append(" SampleToRegularDetails.IndentWeight");
                            sb  . append(" from WasteCodeAllocation");
                            sb  . append(" inner join WasteVariety on WasteVariety.VarietyCode=WasteCodeAllocation.VarietyCode");
                            sb  . append(" Inner join SampleToRegularDetails on SampleToRegularDetails.FibreCode=WasteCodeAllocation.fibreCode");
                            sb  . append(" Left  join FibreForm on FibreForm.FormCode=SampleTORegularDetails.FibreFormCode");
                            sb  . append(" where SampleToRegularDetails.ConversionNo="+SConversionNo);
                            sb  . append(" Union All ");
                            sb  . append(" select Fibre.FibreCode,SampleToRegularDetails.Weight,");
                            sb  . append(" concat(concat(concat(Fibre.FibreName,','||Fibre.MixLotNo),','||MixLot.MixLotName),','||FibreForm.FormName),");
                            sb  . append(" SampleToRegularDetails.AvailWeight,");
                            sb  . append(" SampleToRegularDetails.IndentWeight");
                            sb  . append(" from Fibre");
                            sb  . append(" Inner join SampleToRegularDetails on");
                            sb  . append(" SampleToRegularDetails.FibreCode=Fibre.fibreCode");
                            sb  . append(" Left join MixLot on MixLot.MixLotNo=Fibre.MixLotNo");
                            sb  . append(" Left  join FibreForm on FibreForm.FormCode=SampleTORegularDetails.FibreFormCode");
                            sb  . append(" where SampleToRegularDetails.ConversionNo="+SConversionNo);
                            sb  . append(" and Fibre.FibreCode not like 'J%'");
                            sb  . append(" and Fibre.FibreCode not like 'S%'");
                            sb  . append(" and Fibre.FibreCode not like 'A%'");
                            sb  . append(" and Fibre.FibreCode not like 'M%'");
                            sb  . append(" ) order by 1");

        String  totalweight = "";
        String  totper      = "";
        double  dtot        = 0;
        double  dper        = 0;

        try{
            java.sql.PreparedStatement  pstmt       = null;
                                        pstmt       = theScmConnection.prepareStatement(sb.toString());
            java.sql.ResultSet          rst         = pstmt.executeQuery();
            
            while(rst.next())   {

                String FibreCode    = rst.getString(1);
                String FibreName    = rst.getString(3);
                String MixQty       = rst.getString(2);
                String MixPer       = common.getRound(String.valueOf((common.toDouble(MixQty)/common.toDouble(sqty)*100)),2);

                dtot += common.toDouble(common.getRound(MixQty,2));
                dper += common.toDouble(common.getRound(MixPer,2));

                java.util.HashMap   hmt = new java.util.HashMap();
                                    hmt . put("FIBRECODE",FibreCode);
                                    hmt . put("FIBRENAME",FibreName);
                                    hmt . put("MIXQTY",common.getRound(MixQty,2));
                                    hmt . put("MIXPER",MixPer);
                list .add(hmt);
            }
            rst     . close();
            rst     = null;
            pstmt   . close();
            pstmt   = null;
            
        }catch(Exception ex)   {
            ex.printStackTrace();
        }

                   totalweight = common.getRound(String.valueOf(dtot),2);
                   totper      = common.getRound(String.valueOf(dper),2);
        
        java.util.HashMap hmtl = new java.util.HashMap();
                          hmtl . put("TOTALWEIGHT", totalweight);
                          hmtl . put("TOTPER",      totper);
        
        java.util.HashMap myorderdetails = new java.util.HashMap();
                          myorderdetails . put("CONVMAP",hm);
                          myorderdetails . put("FIBREDETAILSLIST",list);
                          myorderdetails . put("TOTALMAP",hmtl);

        return myorderdetails;
    }
    
 
    private java.util.HashMap getConversiondetails(String SOrderNo,int iOrderType)    {
        java.util.HashMap   hm      = null;
                            hm      = new java.util.HashMap();
        StringBuffer        sb      = null;
                            sb      = new StringBuffer();
                            sb      . append(" select sampletoregular.conversionno, sampletoregular.realisation_weight from sampletoregular ");
                            //sb      . append(" inner join sampletoregular on sampletoregular.conversionno  = indent.conversionno ");
                            sb      . append(" where sampletoregular.regularorderno = ? and sampletoregular.correctionmixing = ? ");

        try {
            java.sql.PreparedStatement  pstmt       =   null;
                                        pstmt       =   theScmConnection . prepareStatement(sb.toString());
                                                        pstmt   . setString(1,SOrderNo);
                                                        pstmt   . setInt(2,iOrderType);
            java.sql.ResultSet          rst         =   pstmt.executeQuery();

            while(rst.next()) {
                hm      . put("CONVERSIONNO", rst.getString(1));
                hm      . put("MIXQTY",       rst.getString(2));
            }
            rst     . close();
            rst     = null;
            pstmt   . close();
            pstmt   = null;
            
        }catch(Exception ex)   {
            ex.printStackTrace();
        }
        
        return hm;
    }
    
    
    public int  CheckCorrMixing(String orderno){
        int cmix=0;
        StringBuffer sb = new StringBuffer();
                     sb . append(" SELECT count(*) FROM PROCESSINGOFINDENT_CORRECTION WHERE RORDERNO=? ");

    try{
        if(theProcessConnection==null){
            JDBCProcessaConnection jdbc = JDBCProcessaConnection.getJDBCConnection();
            theProcessConnection       = jdbc.getConnection();
        }
        java.sql.PreparedStatement pst = theProcessConnection.prepareStatement(sb.toString());
                                   pst  . setString(1,orderno);
        java.sql.ResultSet         rst  = pst.executeQuery();

        while(rst.next()){
            cmix = rst.getInt(1);
        }
        rst.close();
        rst = null;
        pst.close();
        pst = null;
        
      }catch(Exception e){
          e.printStackTrace();
      }
        return cmix;
    }
}