/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prepcom.setting;

/**
 *
 * @author root
 */

public class OrderGeneralDetailsClass {

    private String  rorderno;
    private String  orderdate;
    private String  indentno;
    private String  countcode;
    private String  countname;
    private String  shade;
    private String  partyname;
    private String  processtype;
    private String  layer;
    private String  batch;
    private String  mixweight;
    private String  orderweight;
    private String  depth;
    private String  blend;
    private String  waste;
    private String  realisation;
    private String  correctionmixing;
    private String  contamination;
    private String  clothcheckstatus;
    private String  clothchecktpi;
    private String  clothcheckcountname;
    private String  countcodecheck;
    
    private int     iProcessgroupcode;
    private int     idarkbase;
    private int     iwhitebase;
    
    //Group & Split Details
    private int     os_baseorsuborder;
    private String  os_baseorderno;
    private String  os_splitorderno;
    private int     os_splittype;
    private int     os_splitdept;
    private int     os_splitdeptlvlcode;

    private String  sordercancelstatus;
    
    public OrderGeneralDetailsClass() {
    }
    
    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public String getBlend() {
        return blend;
    }

    public void setBlend(String blend) {
        this.blend = blend;
    }

    public String getClothcheckcountname() {
        return clothcheckcountname;
    }

    public void setClothcheckcountname(String clothcheckcountname) {
        this.clothcheckcountname = clothcheckcountname;
    }

    public String getClothcheckstatus() {
        return clothcheckstatus;
    }

    public void setClothcheckstatus(String clothcheckstatus) {
        this.clothcheckstatus = clothcheckstatus;
    }

    public String getClothchecktpi() {
        return clothchecktpi;
    }

    public void setClothchecktpi(String clothchecktpi) {
        this.clothchecktpi = clothchecktpi;
    }

    public String getContamination() {
        return contamination;
    }

    public void setContamination(String contamination) {
        this.contamination = contamination;
    }

    public String getCorrectionmixing() {
        return correctionmixing;
    }

    public void setCorrectionmixing(String correctionmixing) {
        this.correctionmixing = correctionmixing;
    }

    public String getCountcode() {
        return countcode;
    }

    public void setCountcode(String countcode) {
        this.countcode = countcode;
    }

    public String getCountname() {
        return countname;
    }

    public void setCountname(String countname) {
        this.countname = countname;
    }

    public String getDepth() {
        return depth;
    }

    public void setDepth(String depth) {
        this.depth = depth;
    }

    public String getIndentno() {
        return indentno;
    }

    public void setIndentno(String indentno) {
        this.indentno = indentno;
    }

    public String getLayer() {
        return layer;
    }

    public void setLayer(String layer) {
        this.layer = layer;
    }

    public String getMixweight() {
        return mixweight;
    }

    public void setMixweight(String mixweight) {
        this.mixweight = mixweight;
    }

    public String getOrderdate() {
        return orderdate;
    }

    public void setOrderdate(String orderdate) {
        this.orderdate = orderdate;
    }

    public String getOrderweight() {
        return orderweight;
    }

    public void setOrderweight(String orderweight) {
        this.orderweight = orderweight;
    }

    public String getPartyname() {
        return partyname;
    }

    public void setPartyname(String partyname) {
        this.partyname = partyname;
    }

    public String getProcesstype() {
        return processtype;
    }

    public void setProcesstype(String processtype) {
        this.processtype = processtype;
    }

    public String getRealisation() {
        return realisation;
    }

    public void setRealisation(String realisation) {
        this.realisation = realisation;
    }

    public String getRorderno() {
        return rorderno;
    }

    public void setRorderno(String rorderno) {
        this.rorderno = rorderno;
    }

    public String getShade() {
        return shade;
    }

    public void setShade(String shade) {
        this.shade = shade;
    }

    public String getWaste() {
        return waste;
    }

    public void setWaste(String waste) {
        this.waste = waste;
    }

    public String getCountcodecheck() {
        return countcodecheck;
    }

    public void setCountcodecheck(String countcodecheck) {
        this.countcodecheck = countcodecheck;
    }

    public String getOs_baseorderno() {
        return os_baseorderno;
    }

    public void setOs_baseorderno(String os_baseorderno) {
        this.os_baseorderno = os_baseorderno;
    }

    public int getOs_splitdept() {
        return os_splitdept;
    }

    public void setOs_splitdept(int os_splitdept) {
        this.os_splitdept = os_splitdept;
    }

    public int getOs_splitdeptlvlcode() {
        return os_splitdeptlvlcode;
    }

    public void setOs_splitdeptlvlcode(int os_splitdeptlvlcode) {
        this.os_splitdeptlvlcode = os_splitdeptlvlcode;
    }

    public String getOs_splitorderno() {
        return os_splitorderno;
    }

    public void setOs_splitorderno(String os_splitorderno) {
        this.os_splitorderno = os_splitorderno;
    }

    public int getOs_splittype() {
        return os_splittype;
    }

    public void setOs_splittype(int os_splittype) {
        this.os_splittype = os_splittype;
    }

    public int getOs_baseorsuborder() {
        return os_baseorsuborder;
    }

    public void setOs_baseorsuborder(int os_baseorsuborder) {
        this.os_baseorsuborder = os_baseorsuborder;
    }

    public int getiProcessgroupcode() {
        return iProcessgroupcode;
    }

    public void setiProcessgroupcode(int iProcessgroupcode) {
        this.iProcessgroupcode = iProcessgroupcode;
    }

    public int getIdarkbase() {
        return idarkbase;
    }

    public void setIdarkbase(int idarkbase) {
        this.idarkbase = idarkbase;
    }

    public int getIwhitebase() {
        return iwhitebase;
    }

    public void setIwhitebase(int iwhitebase) {
        this.iwhitebase = iwhitebase;
    }

    public String getSordercancelstatus() {
        return sordercancelstatus;
    }

    public void setSordercancelstatus(String sordercancelstatus) {
        this.sordercancelstatus = sordercancelstatus;
    }
    
}
