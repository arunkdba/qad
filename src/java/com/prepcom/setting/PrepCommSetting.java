/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prepcom.setting;

import com.jdbc.connection.JDBCProcessConnection;
import com.jdbc.connection.JDBCProcessaConnection;

/**
 *
 * @author root
 */
public class PrepCommSetting {

    java.sql.Connection theProcessConnection = null;
    
    com.common.Common  common;
    
    public PrepCommSetting() {
        common = new com.common.Common();
    }
    
    public java.util.HashMap getPrepCommSetting(int iDeptCode,String SOrderNo,int iOrderType){
        java.util.HashMap theMap = new java.util.HashMap();
        String STable ="PROCESSINGOFINDENT";
               if(iOrderType==1){
                   STable ="PROCESSINGOFINDENT_CORRECTION";
               }

        StringBuffer sb = new StringBuffer();
                if(iDeptCode==1){
                     sb . append(" SELECT AUNIT_ORDERBLOWROOMSETTING.RORDERNO as RORDERNO, MBO, MONO1, MONO2, ERM, TBB1, TBB2, KB1, KB2, LABLENGTH, LABWEIGHT, "+STable+".REALISATION_WEIGHT as REALISATION_WEIGHT , "+STable+".BLOW_WASTE as BLOW_WASTE, PROCESSINGTYPE.OESTATUS as OESTATUS, "+STable+".PROCESSGROUPCODE as PROCESSGROUPCODE, PROCESSINGTYPE.DARKBASE as DARKBASE, PROCESSINGTYPE.WHITEBASE as WHITEBASE  FROM AUNIT_ORDERBLOWROOMSETTING ");
                     sb . append(" INNER JOIN PROCESSA."+STable+" ON  "+STable+".RORDERNO = AUNIT_ORDERBLOWROOMSETTING.RORDERNO ");
                     sb . append(" INNER JOIN SCM.PROCESSINGTYPE ON PROCESSINGTYPE.PROCESSCODE = "+STable+".PROCESSCODE ");
                     sb . append(" WHERE AUNIT_ORDERBLOWROOMSETTING.RORDERNO = ? AND AUNIT_ORDERBLOWROOMSETTING.SETTINGTYPE =  "+iOrderType+" ");
                }
                if(iDeptCode==2){
                     sb . append(" SELECT AUNIT_ORDERCARDINGSETTING.RORDERNO as RORDERNO,CARDSET, FEEDTOLICK, FRONTTOCYLINDER, FLATSET1, FLATSET2, FLATSET3, ");
                     sb . append(" FLATSET4, FLATSET5, FLATSPEED, WORMWHEEL, DOFFERSPEED, CARDHANK, COMBINGSETTING, MOTORPULLEY, "+STable+".REALISATION_WEIGHT as REALISATION_WEIGHT , "+STable+".CARD_COMPATEWASTE as CARD_COMPATEWASTE, "+STable+".PROCESSGROUPCODE as PROCESSGROUPCODE, PROCESSINGTYPE.DARKBASE as DARKBASE, PROCESSINGTYPE.WHITEBASE as WHITEBASE  FROM AUNIT_ORDERCARDINGSETTING ");
                     sb . append(" INNER JOIN PROCESSA."+STable+" ON  "+STable+".RORDERNO = AUNIT_ORDERCARDINGSETTING.RORDERNO ");
                     sb . append(" INNER JOIN SCM.PROCESSINGTYPE ON PROCESSINGTYPE.PROCESSCODE =  "+STable+".PROCESSCODE  AND PROCESSINGTYPE.PROCESSGROUPCODE = "+STable+".PROCESSGROUPCODE ");
                     sb . append(" WHERE AUNIT_ORDERCARDINGSETTING.RORDERNO = ? and AUNIT_ORDERCARDINGSETTING.SETTINGTYPE =  "+iOrderType+" ");
                }
                if(iDeptCode==3){
                     sb . append(" SELECT  AUNIT_ORDERDRAWINGSETTING.RORDERNO, RSBSET1, RSBSET2, WHITERSBSET1, WHITERSBSET2, RSBHANK, RSBHANKTO,RSBWHITEHANKFROM, RSBWHITEHANKTO, ");
                     sb . append(" RSBBREAKDRAFT, DO2SSET1, DO2SSET2, WHITEDO2SSET1, WHITEDO2SSET2, DO2SHANK, DO2SHANKTO, DO2SWHITEHANKFROM, DO2SWHITEHANKTO, ");
                     sb . append(" DO2SBREAKDRAFT, DRAWCANWGT, DRAWBLEND, DO6SSET1, DO6SSET2, DO6SHANK, DO6SBREAKDRAFT, DO6SHANKTO, DO6SWHITEHANKFROM, ");
                     sb . append(" DO6SWHITEHANKTO, WHITEDO6SSET1, WHITEDO6SSET2, D11SET1, D11SET2, D11HANK, D11BREAKDRAFT, D11HANKTO, D11WHITEHANKFROM, ");
                     sb . append(" D11WHITEHANKTO, WHITED11SET1, WHITED11SET2, decode(SUNITCLOTHCHKSTATUS,'','',0,'Pending',1,'OK',2,'Not Ok',SUNITCLOTHCHKSTATUS) as CLOTHSTATUS, ");
                     sb . append(" PRO_CLOTHSTATUS_REMARKS.REMARKS as REMARKS, DRAWCAN_WEIGHT, DRAWCAN_WEIGHT_AUTHREMARKS, REALISATION_WEIGHT, DRAW_WASTE, DRAW_WASTE_AUTHREMARKS FROM  AUNIT_ORDERDRAWINGSETTING ");
                     sb . append(" INNER JOIN "+STable+" ON  "+STable+".RORDERNO = AUNIT_ORDERDRAWINGSETTING.RORDERNO ");
                     sb . append(" INNER JOIN PROCESS.PRO_CLOTHSTATUS_REMARKS ON PRO_CLOTHSTATUS_REMARKS.ID = "+STable+".SUNITCLOTHCHKSTATUS_REMARKS ");
                     sb . append(" WHERE AUNIT_ORDERDRAWINGSETTING.RORDERNO= ? AND  AUNIT_ORDERDRAWINGSETTING.SETTINGTYPE =  "+iOrderType+" ");
                }
                if(iDeptCode==4){
                     sb . append(" SELECT RORDERNO, SIMPHANK, SIMPLEXHANKTO, BOBBINLENGTH, SIMPBDRAFT, SIMPRATCHET, SIMPTPI, ");
                     sb . append(" SIMPSPACER, FRONTROLE, MIDDLEROLE, BACKROLE   FROM AUNIT_ORDERSIMPLEXSETTING ");
                     sb . append(" WHERE RORDERNO = ?  AND SETTINGTYPE = 0 ");
                }
                
        
        try {
            if(theProcessConnection==null){
                JDBCProcessaConnection jdbc = JDBCProcessaConnection.getJDBCConnection();
                theProcessConnection       = jdbc.getConnection();
            }
            java.sql.PreparedStatement  pstmt = theProcessConnection.prepareStatement(sb.toString());
                                        pstmt . setString(1, SOrderNo);
            java.sql.ResultSet   rset = pstmt . executeQuery();
            java.sql.ResultSetMetaData rsm = rset .getMetaData();
            
            while (rset.next()) {
                for(int i=0;i<rsm.getColumnCount();i++){
                    theMap.put(rsm.getColumnName(i+1), rset.getString(i+1));
                }
            }
            rset.close();
            rset = null;
            pstmt.close();
            pstmt = null
                    ;
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        
        return theMap;
    }
    
    
    public java.util.List getPrepCommCanDetails(int iDeptCode,String SOrderNo,int iOrderType){
        java.util.List theList = new java.util.ArrayList();
        String STable ="PROCESSINGOFINDENT";
               if(iOrderType==1){
                   STable ="PROCESSINGOFINDENT_CORRECTION";
               }
               
        StringBuffer sb = new StringBuffer();
                if(iDeptCode==1){
                     sb  . append(" select mypictures.name as LAPSLIP, pro_blowroom_lapslip.ORDERFOLLOWSTATUS as LAPSLIP_STATUS from pro_blowroom_lapslip ");
                     sb  . append(" inner join "+STable+" on "+STable+".rorderno = pro_blowroom_lapslip.rorderno ");
                     sb  . append(" inner join mypictures on mypictures.id = pro_blowroom_lapslip.lapslipid ");
                     sb  . append(" where pro_blowroom_lapslip.rorderno = ? and pro_blowroom_lapslip.type = "+iOrderType+" ");
                }
                if(iDeptCode==2){
                     sb  . append(" select concat(pro_cardingcan.name,decode(pro_cardingcanband.name,'NIL','',concat(' with ',pro_cardingcanband.name))) as CARDINGCAN, pro_carding_can_details.ORDERFOLLOWSTATUS as CARDINGCAN_STATUS from pro_carding_can_details ");
                     sb  . append(" inner join "+STable+" on "+STable+".rorderno = pro_carding_can_details.rorderno ");
                     sb  . append(" inner join pro_cardingcan on pro_cardingcan.id=pro_carding_can_details.cardingcanid ");
                     sb  . append(" inner join pro_cardingcanband on pro_cardingcanband.id=pro_carding_can_details.cardingcanbandid ");
                     sb  . append(" where pro_carding_can_details.rorderno = ? and pro_carding_can_details.type = "+iOrderType+" ");
                }
                if(iDeptCode==3){
                     sb  . append(" select concat(pro_drawingcan.name,decode(pro_drawingcanband.name,'NIL','',concat(' with ',pro_drawingcanband.name))) as DRAWINGCAN, pro_drawing_can_details.processlevel as PROCESSLEVEL, pro_drawing_can_details.ORDERFOLLOWSTATUS as DRAWINGCAN_STATUS  from pro_drawing_can_details ");
                     sb  . append(" inner join "+STable+" on "+STable+".rorderno = pro_drawing_can_details.rorderno ");
                     sb  . append(" inner join pro_drawingcan on pro_drawingcan.id = pro_drawing_can_details.drawingcanid ");
                     sb  . append(" inner join pro_drawingcanband on pro_drawingcanband.id = pro_drawing_can_details.drawingcanbandid ");
                     sb  . append(" where  pro_drawing_can_details.rorderno = ?  and  pro_drawing_can_details.type = "+iOrderType+"  order by pro_drawing_can_details.processlevel ");
                }
                if(iDeptCode==4){
                     sb  . append(" select concat(t.SIMPPUMB, decode(pro_simplexring.name,'Select','',concat(' with ',pro_simplexring.name))) as SIMPLEXPUMB, t.ORDERFOLLOWSTATUS as SIMPLEXPUMB_STATUS  from ( ");
                     sb  . append(" select concat(pro_simplexpumb.name , decode(pro_simplexring.name,'Select','',concat(' with ',pro_simplexring.name))) as SIMPPUMB, processingofindent.rorderno, pro_simplex_can_details.ORDERFOLLOWSTATUS, pro_simplex_can_details.id as SID   from processa.processingofindent ");
                     sb  . append(" inner join processa.pro_simplex_can_details on pro_simplex_can_details.rorderno = processingofindent.rorderno ");
                     sb  . append(" inner join processa.pro_simplexpumb on pro_simplexpumb.id=pro_simplex_can_details.simplexpumbid ");
                     sb  . append(" inner join processa.pro_simplexring on pro_simplexring.id=pro_simplex_can_details.simplexringid ");
                     sb  . append(" where pro_simplex_can_details.rorderno= ?  ) t ");
                     sb  . append(" inner join processa.pro_simplex_can_details on pro_simplex_can_details.rorderno= t.rorderno and pro_simplex_can_details.id = t.SID ");
                     sb  . append(" inner join processa.pro_simplexring on pro_simplexring.id=pro_simplex_can_details.simplexringid2 ");
                }
        
        try {
            if(theProcessConnection==null){
                JDBCProcessaConnection jdbc = JDBCProcessaConnection.getJDBCConnection();
                theProcessConnection       = jdbc.getConnection();
            }
            java.sql.PreparedStatement  pstmt = theProcessConnection.prepareStatement(sb.toString());
                                        pstmt . setString(1, SOrderNo);
            java.sql.ResultSet   rset = pstmt . executeQuery();
            java.sql.ResultSetMetaData rsm = rset .getMetaData();
            java.util.HashMap theMap = new java.util.HashMap();
            while (rset.next()) {
                theMap = new java.util.HashMap();
                for(int i=0;i<rsm.getColumnCount();i++){
                    theMap.put(rsm.getColumnName(i+1), rset.getString(i+1));
                }
              theList.add(theMap);
            }
            rset.close();
            rset = null;
            pstmt.close();
            pstmt = null
                    ;
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        
        return theList;
    }

    
    public java.util.List getDrawingPassageSetting(String SOrderNo,int iOrderType) {
        java.util.List theList = new java.util.ArrayList();
        StringBuffer sb = new StringBuffer();
                     sb . append(" SELECT ID, RORDERNO, PROCESSLEVEL, MACHTYPENAME, SLIVERENDS, BREAKERFINISHER, REMARKS, PROCESSTYPE  FROM AUNIT_ORDERDRAWINGSETTINGDLS ");
                     sb . append(" WHERE RORDERNO= ? and SETTINGTYPE= ? ORDER BY ID ");
                     
        try {
            if(theProcessConnection==null){
                JDBCProcessaConnection jdbc = JDBCProcessaConnection . getJDBCConnection();
                theProcessConnection       = jdbc.getConnection();
            }
            java.sql.PreparedStatement pstmt = theProcessConnection.prepareStatement(sb.toString());
                                       pstmt . setString(1, SOrderNo);
                                       pstmt . setInt(2, iOrderType);
            java.sql.ResultSet rset =  pstmt . executeQuery();
            java.sql.ResultSetMetaData rsm = rset .getMetaData();
            java.util.HashMap theMap  = new java.util.HashMap();
            while (rset.next()) {
                theMap = new java.util.HashMap();
                for(int i=0;i<rsm.getColumnCount();i++){
                    theMap . put(rsm.getColumnName(i+1), rset.getString(i+1));
                }
                
                theList.add(theMap);
            }
            rset . close();
            rset = null;
            pstmt . close();
            pstmt = null;
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return theList;
    }
    
    public String getWastePercentage(int iDeptCode,java.util.HashMap theMap){
        String SWastePerc = "";
        
        double dMixWeight = common.toDouble(common.parseNull((String)theMap.get("REALISATION_WEIGHT")));
        int idarkbase     = common.toInt(common.parseNull((String)theMap.get("DARKBASE")));
        int iwhitebase    = common.toInt(common.parseNull((String)theMap.get("WHITEBASE")));
            
        if(common.toInt(common.parseNull((String)theMap.get("PROCESSGROUPCODE")))==2 && idarkbase!=0 && iwhitebase!=0) {
                    
            if(iDeptCode==1){
                double darkbaseweight = dMixWeight*idarkbase/(idarkbase+iwhitebase);
                SWastePerc = common.getRound(common.toDouble(common.parseNull((String) theMap.get("BLOW_WASTE")))/darkbaseweight*100,2); 
            }
            if(iDeptCode==2){
                double darkbaseweight = dMixWeight*idarkbase/(idarkbase+iwhitebase);
                SWastePerc = common.getRound(common.toDouble(common.parseNull((String) theMap.get("CARD_COMPATEWASTE")))/darkbaseweight*100,2); 
            }
        }else{
            if(iDeptCode==1){
                SWastePerc = common.getRound(common.toDouble(common.parseNull((String) theMap.get("BLOW_WASTE")))/dMixWeight*100,2);  
            }
            if(iDeptCode==2){
                SWastePerc = common.getRound(common.toDouble(common.parseNull((String) theMap.get("CARD_COMPATEWASTE")))/dMixWeight*100,2); 
            }
        }
        return SWastePerc;
    }
}