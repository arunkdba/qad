/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.classes;
import com.common.Common;
/**
 *
 * @author admin
 */
public class CoEffVarient {
    Common common = null;
    public String[] data;
    public double dcoEffVant;

    public CoEffVarient() {
        common = new Common();
    }
    public void setArrayDetails(String[] data) {
        this.data = data;
    }

    public double getCoEffVant(String[] data) {
        setArrayDetails(data);
        callCoeffvant();
        return dcoEffVant;
    }

    public double getMean() {

        double dmean = 0.000;
        double dtotmean = 0.00;
        int ic = 0;
        for (String ss : data) {
            double dd = common.toDouble(common.parseNull(ss));
            dtotmean += dd;
            ic++;
        }

        dmean = dtotmean / (double) ic;
        //System.out.println("dmean=" + dmean);
        return dmean;
    }
    public double getMean(String[] data) {

        double dmean = 0.000;
        double dtotmean = 0.00;
        int ic = 0;
        for (String ss : data) {
            double dd = common.toDouble(common.parseNull(ss));
            dtotmean += dd;
            ic++;
        }

        dmean = dtotmean / (double) ic;
        //System.out.println("dmean=" + dmean);
        return dmean;
    }

    public void callCoeffvant() {

        double dtotstddev = 0.000;
        double dmean = this.getMean();
        int ic = 0;

        for (String ss : data) {

            double ddiff = (common.toDouble(common.parseNull(ss)) - dmean);
            //System.out.println("ddiff" + ddiff);

            double dpow = Math.pow(ddiff, 2);
            //System.out.println("dpow" + dpow);

            dtotstddev += dpow;
            ic++;
        }
        //System.out.println("dtotstddev"+dtotstddev);
        dcoEffVant = ((Math.sqrt(dtotstddev / (double) (ic - 1)))/dmean)*100;
        dcoEffVant = common.toDouble(common.getRound(String.valueOf(dcoEffVant),3));
        //System.out.println("Co. Efficent Of Variation =" + dcoEffVant);
    }

    public static void main(String args[]) {
        /*
        CoEffVarient cev = new CoEffVarient();
        cev.setArrayDetails(new String[]{"6", "7", "8", "9", "10"});
        cev.getMean();
        cev.callCoeffvant();
        System.out.println("\n CO EFFCIENT OF VARIATION = "  + cev.getCoEffVant(new String[]{"4.22","4.77","4.78","5.17","4.52"}));
       */
    }
}