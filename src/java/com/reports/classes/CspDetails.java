/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.classes;

/**
 * For UT3 Reports 
 * @author admin
 */
public class CspDetails {

    private String testno="";
    private String testdate = "";
    private String mechinename="";
    private String cnt1="",cnt2="",cnt3="",cnt4="";
    private String strength1="",strength2="",strength3="",strength4;
    private String csp1="",csp2="",csp3="",csp4="";
    private String orderno="",cpwheel="",tpi="",draft="",bdraft="";
    private String pumpclr="",testtype="",type="";
    private String shadename="",countname="";
    public CspDetails() {
    }

    public String getTestno() {
        return testno;
    }

    public void setTestno(String testno) {
        this.testno = testno;
    }

    public String getTestdate() {
        return testdate;
    }

    public void setTestdate(String testdate) {
        this.testdate = testdate;
    }

    public String getMechinename() {
        return mechinename;
    }

    public void setMechinename(String mechinename) {
        this.mechinename = mechinename;
    }

    public String getCnt1() {
        return cnt1;
    }

    public void setCnt1(String cnt1) {
        this.cnt1 = cnt1;
    }

    public String getCnt2() {
        return cnt2;
    }

    public void setCnt2(String cnt2) {
        this.cnt2 = cnt2;
    }

    public String getCnt3() {
        return cnt3;
    }

    public void setCnt3(String cnt3) {
        this.cnt3 = cnt3;
    }

    public String getCnt4() {
        return cnt4;
    }

    public void setCnt4(String cnt4) {
        this.cnt4 = cnt4;
    }

    public String getStrength1() {
        return strength1;
    }

    public void setStrength1(String strength1) {
        this.strength1 = strength1;
    }

    public String getStrength2() {
        return strength2;
    }

    public void setStrength2(String strength2) {
        this.strength2 = strength2;
    }

    public String getStrength3() {
        return strength3;
    }

    public void setStrength3(String strength3) {
        this.strength3 = strength3;
    }

    public String getStrength4() {
        return strength4;
    }

    public void setStrength4(String strength4) {
        this.strength4 = strength4;
    }

    public String getCsp1() {
        return csp1;
    }

    public void setCsp1(String csp1) {
        this.csp1 = csp1;
    }

    public String getCsp2() {
        return csp2;
    }

    public void setCsp2(String csp2) {
        this.csp2 = csp2;
    }

    public String getCsp3() {
        return csp3;
    }

    public void setCsp3(String csp3) {
        this.csp3 = csp3;
    }

    public String getCsp4() {
        return csp4;
    }

    public void setCsp4(String csp4) {
        this.csp4 = csp4;
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public String getCpwheel() {
        return cpwheel;
    }

    public void setCpwheel(String cpwheel) {
        this.cpwheel = cpwheel;
    }

    public String getTpi() {
        return tpi;
    }

    public void setTpi(String tpi) {
        this.tpi = tpi;
    }

    public String getDraft() {
        return draft;
    }

    public void setDraft(String draft) {
        this.draft = draft;
    }

    public String getBdraft() {
        return bdraft;
    }

    public void setBdraft(String bdraft) {
        this.bdraft = bdraft;
    }

    public String getPumpclr() {
        return pumpclr;
    }

    public void setPumpclr(String pumpclr) {
        this.pumpclr = pumpclr;
    }

    public String getTesttype() {
        return testtype;
    }

    public void setTesttype(String testtype) {
        this.testtype = testtype;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getShadename() {
        return shadename;
    }

    public void setShadename(String shadename) {
        this.shadename = shadename;
    }

    public String getCountname() {
        return countname;
    }

    public void setCountname(String countname) {
        this.countname = countname;
    }    
}