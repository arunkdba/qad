/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.classes;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author root
 */
public class ErrorMessage {
 private List   ErrorMessageList;
 private String  sMessage="", sSentEmp="", sSentDateTime="";
 
public ErrorMessage(){
       ErrorMessageList  = new ArrayList();
}    

public String getErrorMessage() {
        return sMessage;
    }
public void setErrorMessage(String sMessage) {
        this.sMessage = sMessage;
    }

public String getSentEmp() {
        return sSentEmp;
    }
public void setSentEmp(String sSentEmp) {
        this.sSentEmp = sSentEmp;
    }

public String getSentDateTime() {
        return sSentDateTime;
    }  

public void setSentDateTime(String sSentDateTime) {
        this.sSentDateTime = sSentDateTime;
    }
}
