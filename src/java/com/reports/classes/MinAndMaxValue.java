/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.classes;

import java.util.Arrays;
import java.util.Collections;
import com.common.Common;
/**
 *
 * @author admin
 */
public class MinAndMaxValue {
    public String[] numbers;
    Common common = new Common();
    public MinAndMaxValue() {
        
    }
    public void setArrayDetails(String[] numbers) {
        this.numbers = numbers;
    }
    public double getMin(String[] numbers)
    {
        //setArrayDetails(numbers);
        String min="";
        try
        {
            if(numbers.length>0)
            min = (String) Collections.min(Arrays.asList(numbers));
        }
        catch(Exception ex)
        {
            //System.out.println("Min Error:");
            ex.printStackTrace();
        }
        return common.toDouble(min);
    }
    public double getMax(String[] numbers)
    {
       // setArrayDetails(numbers);
        String max ="";
        try{
          if(numbers.length>0)
          max = (String) Collections.max(Arrays.asList(numbers));
        }
        catch(Exception ex)
        {
            //System.out.println("MAx Error:");
            ex.printStackTrace();
        }
        return common.toDouble(max);
    }
    public double getMin(double[] numbers)
    {
        double smallest =0;
        double largest = 0;
        if(numbers.length>0)
        {
         smallest = numbers[0];
         largest = numbers[0];
                
        for(int i=1; i< numbers.length; i++)
        {
            if(numbers[i] > largest)
                    largest = numbers[i];
            else if (numbers[i] < smallest)
                    smallest = numbers[i];
        }
        }
        return smallest;
    }
    public double getMax(double[] numbers)
    {
        double smallest =0;
        double largest = 0;
        if(numbers.length>0)
        {
        smallest = numbers[0];
        largest = numbers[0];
        
        for(int i=1; i< numbers.length; i++)
        {
            if(numbers[i] > largest)
                    largest = numbers[i];
            else if (numbers[i] < smallest)
                    smallest = numbers[i];
        }
        }
        return largest;
    }
    public static void main(String args[]) {
        MinAndMaxValue mnv = new MinAndMaxValue();
        System.out.println("\n Min And MAx "  + mnv.getMin(new String[]{"19.58","19.19","19.10","19.46","19.88","19.31","20.15","19.97"}));
        System.out.println("\n Min And Max "+ mnv.getMin(new double[]{}));
    }
}