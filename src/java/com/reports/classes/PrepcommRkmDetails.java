/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.classes;

/**
 *
 * @author admin
 */
public class PrepcommRkmDetails {

    private String orderno="";
    private String entrydate ="";
    private String rkm="";
    private String rkmcv="";
    private String elg="";
    private String elgcv="";
    private String sys="";
    private String testno="";
    private String remarks="";
    private String syscv="";
    private String minrk="";

    public PrepcommRkmDetails() {
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public String getEntrydate() {
        return entrydate;
    }

    public void setEntrydate(String entrydate) {
        this.entrydate = entrydate;
    }

    public String getRkm() {
        return rkm;
    }

    public void setRkm(String rkm) {
        this.rkm = rkm;
    }

    public String getRkmcv() {
        return rkmcv;
    }

    public void setRkmcv(String rkmcv) {
        this.rkmcv = rkmcv;
    }

    public String getElg() {
        return elg;
    }

    public void setElg(String elg) {
        this.elg = elg;
    }

    public String getElgcv() {
        return elgcv;
    }

    public void setElgcv(String elgcv) {
        this.elgcv = elgcv;
    }

    public String getSys() {
        return sys;
    }

    public void setSys(String sys) {
        this.sys = sys;
    }

    public String getTestno() {
        return testno;
    }

    public void setTestno(String testno) {
        this.testno = testno;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }    

    public String getSyscv() {
        return syscv;
    }

    public void setSyscv(String syscv) {
        this.syscv = syscv;
    }    

    public String getMinrk() {
        return minrk;
    }

    public void setMinrk(String minrk) {
        this.minrk = minrk;
    }
    
}
