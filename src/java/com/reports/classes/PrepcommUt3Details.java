/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.classes;

/**
 *
 * @author admin
 */
public class PrepcommUt3Details {

    private String orderno="";
    private String entrydate ="";
    private String uper="";
    private String thin="";
    private String thick="";
    private String nep200="";
    private String nep280="";
    private String tot200="";
    private String tot280="";
    private String cvm="";
    private String testno="";
    private String remarks="";
    public PrepcommUt3Details() {
    }    

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public String getEntrydate() {
        return entrydate;
    }

    public void setEntrydate(String entrydate) {
        this.entrydate = entrydate;
    }

    public String getUper() {
        return uper;
    }

    public void setUper(String uper) {
        this.uper = uper;
    }

    public String getThin() {
        return thin;
    }

    public void setThin(String thin) {
        this.thin = thin;
    }

    public String getThick() {
        return thick;
    }

    public void setThick(String thick) {
        this.thick = thick;
    }

    public String getNep200() {
        return nep200;
    }

    public void setNep200(String nep200) {
        this.nep200 = nep200;
    }

    public String getNep280() {
        return nep280;
    }

    public void setNep280(String nep280) {
        this.nep280 = nep280;
    }

    public String getTot200() {
        return tot200;
    }

    public void setTot200(String tot200) {
        this.tot200 = tot200;
    }

    public String getTot280() {
        return tot280;
    }

    public void setTot280(String tot280) {
        this.tot280 = tot280;
    }

    public String getCvm() {
        return cvm;
    }

    public void setCvm(String cvm) {
        this.cvm = cvm;
    }

    public String getTestno() {
        return testno;
    }

    public void setTestno(String testno) {
        this.testno = testno;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }    
}
