/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.classes;

/**
 * For UT3 Reports
 * @author admin
 */
public class RkmDetails {

    private String minRKM = "";
    private String orderno="";
    private String entrydate ="";
    private String rkm="";
    private String rkmcv="";
    private String elg="";
    private String elgcv="";
    private String sys="";
    private String testno="";
    private String remarks="";
    private String unitname="";
    private String syscv ="";
    private String shadename="";
    private String countname="";
    private String partyname="";

    public RkmDetails() {
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public String getEntrydate() {
        return entrydate;
    }

    public void setEntrydate(String entrydate) {
        this.entrydate = entrydate;
    }

    public String getRkm() {
        return rkm;
    }

    public void setRkm(String rkm) {
        this.rkm = rkm;
    }

    public String getRkmcv() {
        return rkmcv;
    }

    public void setRkmcv(String rkmcv) {
        this.rkmcv = rkmcv;
    }

    public String getElg() {
        return elg;
    }

    public void setElg(String elg) {
        this.elg = elg;
    }

    public String getElgcv() {
        return elgcv;
    }

    public void setElgcv(String elgcv) {
        this.elgcv = elgcv;
    }

    public String getSys() {
        return sys;
    }

    public void setSys(String sys) {
        this.sys = sys;
    }

    public String getTestno() {
        return testno;
    }

    public void setTestno(String testno) {
        this.testno = testno;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }        
    public String getUnitname() {
        return unitname;
    }
    public void setUnitname(String unitname) {
        this.unitname = unitname;
    }   

    public String getSyscv() {
        return syscv;
    }

    public void setSyscv(String syscv) {
        this.syscv = syscv;
    }

    public String getShadename() {
        return shadename;
    }

    public void setShadename(String shadename) {
        this.shadename = shadename;
    }

    public String getCountname() {
        return countname;
    }

    public void setCountname(String countname) {
        this.countname = countname;
    }

    public String getPartyname() {
        return partyname;
    }

    public void setPartyname(String partyname) {
        this.partyname = partyname;
    }

    public String getMinRKM() {
        return minRKM;
    }

    public void setMinRKM(String minRKM) {
        this.minRKM = minRKM;
    }
    
}