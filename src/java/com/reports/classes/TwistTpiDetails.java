/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.classes;

/**
 *
 * @author root
 */
public class TwistTpiDetails {

    public TwistTpiDetails() {
    }
    private String testno;
    private String nomtwist,nomcount,frame,testdate,lot,counttype,tests;
    private String mintpi,maxtpi,avgtpi,rangtpi,shade,tpicv;

    public String getAvgtpi() {
        return avgtpi;
    }

    public void setAvgtpi(String avgtpi) {
        this.avgtpi = avgtpi;
    }

    public String getCounttype() {
        return counttype;
    }

    public void setCounttype(String counttype) {
        this.counttype = counttype;
    }

    public String getFrame() {
        return frame;
    }

    public void setFrame(String frame) {
        this.frame = frame;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public String getMaxtpi() {
        return maxtpi;
    }

    public void setMaxtpi(String maxtpi) {
        this.maxtpi = maxtpi;
    }

    public String getMintpi() {
        return mintpi;
    }

    public void setMintpi(String mintpi) {
        this.mintpi = mintpi;
    }

    public String getNomcount() {
        return nomcount;
    }

    public void setNomcount(String nomcount) {
        this.nomcount = nomcount;
    }

    public String getNomtwist() {
        return nomtwist;
    }

    public void setNomtwist(String nomtwist) {
        this.nomtwist = nomtwist;
    }

    public String getRangtpi() {
        return rangtpi;
    }

    public void setRangtpi(String rangtpi) {
        this.rangtpi = rangtpi;
    }

    public String getTestdate() {
        return testdate;
    }

    public void setTestdate(String testdate) {
        this.testdate = testdate;
    }

    public String getTestno() {
        return testno;
    }

    public void setTestno(String testno) {
        this.testno = testno;
    }

    public String getTests() {
        return tests;
    }

    public void setTests(String tests) {
        this.tests = tests;
    }

    public String getShade() {
        return shade;
    }

    public void setShade(String shade) {
        this.shade = shade;
    }

    public String getTpicv() {
        return tpicv;
    }

    public void setTpicv(String tpicv) {
        this.tpicv = tpicv;
    }
}
