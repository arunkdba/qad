/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.classes;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author admin
 */
public class Ut3Details {

    private String orderno="";
    private String entrydate ="";    
    private String partyname="";
    private String shadename="";
    private String ordweight="";
    private String countname="";
    private String blendname="";
    private String machine="";
    private List OrderList ;

    public Ut3Details() {
        OrderList = new ArrayList();
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public String getEntrydate() {
        return entrydate;
    }

    public void setEntrydate(String entrydate) {
        this.entrydate = entrydate;
    }
    public String getPartyname() {
        return partyname;
    }

    public void setPartyname(String partyname) {
        this.partyname = partyname;
    }

    public String getShadename() {
        return shadename;
    }

    public void setShadename(String shadename) {
        this.shadename = shadename;
    }

    public String getOrdweight() {
        return ordweight;
    }

    public void setOrdweight(String ordweight) {
        this.ordweight = ordweight;
    }   
    public String getCountname() {
        return countname;
    }

    public void setCountname(String countname) {
        this.countname = countname;
    }

    public String getBlendname() {
        return blendname;
    }

    public void setBlendname(String blendname) {
        this.blendname = blendname;
    }

    public String getMachine() {
        return machine;
    }

    public void setMachine(String machine) {
        this.machine = machine;
    }    

    public List getOrderList() {
        return OrderList;
    }
    
    public void setListDetails(java.util.HashMap hm){
        OrderList.add(hm);
    }
    public String getNull(String Svalue){
        String sReturn="";
        if(Svalue.equals("") || Svalue.equals("0") || Svalue.equals("0.00") || Svalue.equals("null"))
        {
            sReturn = "";
        }
        else
        {
            sReturn = Svalue;
        }
        return sReturn;
    }
}