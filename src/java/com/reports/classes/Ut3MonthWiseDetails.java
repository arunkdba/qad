/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.classes;

/**
 *
 * @author admin
 */
public class Ut3MonthWiseDetails {
    
    private String orderno="";
    private String entrydate ="";
    private String machinename="";
    private String partyname="";
    private String shadename="";
    private String countname="";
    private String ordweight="";
    private String cntreading="";
    private String strreading="";
    private String cspreading="";
    private String cnt1="";
    private String cnt2="";
    private String cnt3="";
    private String cnt4="";
    private String cnt5="";
    private String strength1="";
    private String strength2="";
    private String strength3="";
    private String strength4="";
    private String strength5="";
    private String avgcnt="";
    private String avgstr="";
    private String avgcsp="";

    public Ut3MonthWiseDetails() {
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public String getEntrydate() {
        return entrydate;
    }

    public void setEntrydate(String entrydate) {
        this.entrydate = entrydate;
    }

    public String getMachinename() {
        return machinename;
    }

    public void setMachinename(String machinename) {
        this.machinename = machinename;
    }

    public String getPartyname() {
        return partyname;
    }

    public void setPartyname(String partyname) {
        this.partyname = partyname;
    }

    public String getShadename() {
        return shadename;
    }

    public void setShadename(String shadename) {
        this.shadename = shadename;
    }

    public String getCountname() {
        return countname;
    }

    public void setCountname(String countname) {
        this.countname = countname;
    }

    public String getOrdweight() {
        return ordweight;
    }

    public void setOrdweight(String ordweight) {
        this.ordweight = ordweight;
    }

    public String getCntreading() {
        return cntreading;
    }

    public void setCntreading(String cntreading) {
        this.cntreading = cntreading;
    }

    public String getStrreading() {
        return strreading;
    }

    public void setStrreading(String strreading) {
        this.strreading = strreading;
    }

    public String getCspreading() {
        return cspreading;
    }

    public void setCspreading(String cspreading) {
        this.cspreading = cspreading;
    }

    public String getCnt1() {
        return cnt1;
    }

    public void setCnt1(String cnt1) {
        this.cnt1 = cnt1;
    }

    public String getCnt2() {
        return cnt2;
    }

    public void setCnt2(String cnt2) {
        this.cnt2 = cnt2;
    }

    public String getCnt3() {
        return cnt3;
    }

    public void setCnt3(String cnt3) {
        this.cnt3 = cnt3;
    }

    public String getCnt4() {
        return cnt4;
    }

    public void setCnt4(String cnt4) {
        this.cnt4 = cnt4;
    }

    public String getStrength1() {
        return strength1;
    }

    public void setStrength1(String strength1) {
        this.strength1 = strength1;
    }

    public String getStrength2() {
        return strength2;
    }

    public void setStrength2(String strength2) {
        this.strength2 = strength2;
    }

    public String getStrength3() {
        return strength3;
    }

    public void setStrength3(String strength3) {
        this.strength3 = strength3;
    }

    public String getStrength4() {
        return strength4;
    }

    public void setStrength4(String strength4) {
        this.strength4 = strength4;
    }

    public String getAvgcnt() {
        return avgcnt;
    }

    public void setAvgcnt(String avgcnt) {
        this.avgcnt = avgcnt;
    }

    public String getAvgstr() {
        return avgstr;
    }

    public void setAvgstr(String avgstr) {
        this.avgstr = avgstr;
    }

    public String getAvgcsp() {
        return avgcsp;
    }

    public void setAvgcsp(String avgcsp) {
        this.avgcsp = avgcsp;
    }  

    public String getCnt5() {
        return cnt5;
    }

    public void setCnt5(String cnt5) {
        this.cnt5 = cnt5;
    }

    public String getStrength5() {
        return strength5;
    }

    public void setStrength5(String strength5) {
        this.strength5 = strength5;
    }    
}
