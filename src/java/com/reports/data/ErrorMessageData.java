
package com.reports.data;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import com.jdbc.connection.JDBCProcessConnection;
import com.common.Common;
import com.reports.classes.ErrorMessage;

/**
 *
 * @author root
 */
public class ErrorMessageData {
    java.sql.Connection theProcessConnection = null;
    Common common = new Common();
    
public ErrorMessageData(){
    
}   

public java.util.List getErrorMessage(String sDate){

        java.util.List ErrorMessageList = new java.util.ArrayList();
        PreparedStatement pst           = null;
        ResultSet rst                   = null;
        StringBuilder sb                = new StringBuilder();
        ErrorMessage details            = new ErrorMessage();        

        sb.append(" Select Message,UserName,to_Char(to_Date(SentDateTime,'DD-MM-YYYY:hh24:mi:ss'),'DD-MM-YYYY : hh24:mi:ss') as SentDateTime ");
        sb.append(" from Pro_McError_Message");
        sb.append(" Inner Join RawUser on Pro_McError_Message.MESSAGESENT=RawUser.EmpCode");
        sb.append(" Where to_Char(to_Date(SentDateTime,'DD-MM-YYYY:hh24:mi:ss'),'YYYYMMDD')='"+sDate+"'");
        sb.append(" Order by SentDateTime");
        
        //System.out.println("ErrorMessageData  -->"+sb.toString());
        
        try
        {
            if(theProcessConnection == null)
            {
                JDBCProcessConnection jdbc = JDBCProcessConnection.getJDBCConnection();
                theProcessConnection = jdbc.getConnection();
            }
            pst = theProcessConnection.prepareStatement(sb.toString());
            rst = pst.executeQuery();
            while(rst.next()){

                 details = new ErrorMessage();
                 details.setErrorMessage(common.parseNull(rst.getString(1)));
                 details.setSentEmp(common.parseNull(rst.getString(2)));
                 details.setSentDateTime(common.parseNull(rst.getString(3)));
                 
                 ErrorMessageList.add(details);
                 
          }
            rst.close();
            pst.close();
      }
        catch(Exception ex){
      }
        return ErrorMessageList;
    }  
}
