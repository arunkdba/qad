/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.data;

import com.common.Common;
import com.jdbc.connection.JDBCProcessConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author root
 */
public class MagSpinningWrappingTestData {
    java.sql.Connection theProcessConnection  = null;
    Common common = new Common();
    java.util.List theRhlist = new java.util.ArrayList();
    public MagSpinningWrappingTestData() {
    }
    public java.util.List getSpinningWrappingTestData(String sFromDate,String sToDate,String sTestId){
         StringBuffer sb = new StringBuffer();
         java.util.List theList = new java.util.ArrayList();
         
        sb.append(" Select Testid,EntryDate,Test,Weight,Count,CorrCount,Strength,CorrStrength,Csp,CorrCsp ");
        sb.append(" From spinwrapmagDetails ");
        sb.append(" Where Testid="+sTestId+" and EntryDate="+sFromDate+" ");
        sb.append(" order by Test,Testid ");
        
         try{
            if(theProcessConnection==null){
                    JDBCProcessConnection  jdbc = JDBCProcessConnection.getJDBCConnection();
                    theProcessConnection = jdbc . getConnection();
             }
            java.sql.PreparedStatement pst = theProcessConnection.prepareStatement(sb.toString());
            java.sql.ResultSet rst = pst.executeQuery();

            java.sql.ResultSetMetaData rsm = rst.getMetaData();
            
            java.util.HashMap theMap = new java.util.HashMap();
            
            while(rst.next()){
                theMap = new java.util.HashMap();
                for(int i=0;i<rsm.getColumnCount();i++){
                    theMap.put(rsm.getColumnName(i+1), rst.getString(i+1));
                }
                theList.add(theMap);
            }
            rst.close();
            pst.close();
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return theList;
    }
        public java.util.List getSpinTestBase(String sFromDate,String sToDate,String sTestId){
         StringBuffer sb = new StringBuffer();         
         java.util.List theList = new java.util.ArrayList();

           sb.append(" SELECT TESTID,ENTRYDATE,to_char(to_date(time,'yyyy-mm-dd hh:mi:ss'),'hh24:mi:ss') AS TIME,SAMPLE,TEST,FRAME,MACHINE_NAME,SAMPLE_LENGTH,NOMCOUNT,NOMSTRENGTH,NOMCSP,OPERATOR, ");
           sb.append(" DEPARTMENT,COMMENTS,SHIFT,COUNTTYPE,DBT,WBT,RH,COUNTMIN,COUNTMAX,COUNTAVG,COUNTSD,COUNTCV,COUNTQ95,COUNTQ99, ");
           sb.append(" COUNTRANGE,STRMIN,STRMAX,STRAVG,STRSD,STRCV,STRQ95,STRQ99,STRRANGE,CSPMIN,CSPMAX,CSPAVG,CSPSD,CSPCV,CSPQ95, ");
           sb.append(" CSPQ99,CSPRANGE,RHCOUNTMIN,RHCOUNTMAX,RHCOUNTAVG,RHCOUNTCV,RHCOUNTSD,RHCOUNTQ95,RHCOUNTQ99,RHCOUNTRANGE, ");
           sb.append(" RHSTRMIN,RHSTRMAX,RHSTRAVG,RHSTRCV,RHSTRSD,RHSTRQ95,RHSTRQ99,RHSTRRANGE,RHCSPMIN,RHCSPMAX,RHCSPAVG,RHCSPCV, ");
           sb.append(" RHCSPSD,RHCSPQ95,RHCSPQ99,RHCSPRANGE,RKMVAL,ARKMVAL,REMARKS ");
           sb.append(" FROM spinwrapmag ");
           sb.append(" Where Testid="+sTestId+" and EntryDate="+sFromDate+" ");

           System.out.println("TestBas-->"+sb.toString());

         try{
            if(theProcessConnection==null){
                    JDBCProcessConnection  jdbc = JDBCProcessConnection.getJDBCConnection();
                    theProcessConnection = jdbc . getConnection();
             }
            java.sql.PreparedStatement pst = theProcessConnection.prepareStatement(sb.toString());
            java.sql.ResultSet rst = pst.executeQuery();
            
            java.sql.ResultSetMetaData rsm = rst.getMetaData();
            
            java.util.HashMap theMap = new java.util.HashMap();
            
            while(rst.next()){
                theMap = new java.util.HashMap();
                for(int i=0;i<rsm.getColumnCount();i++){
                    theMap.put(rsm.getColumnName(i+1), rst.getString(i+1));
                }
                theList.add(theMap);
            }
            rst.close();
            pst.close();
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return theList;
    } 
    public java.util.List getSpinTestAbst(String sFromDate,String sToDate,String sTestId){
         StringBuffer sb = new StringBuffer();         
         java.util.List theList = new java.util.ArrayList();

           sb.append(" SELECT TESTID,ENTRYDATE,to_char(to_date(time,'yyyy-mm-dd hh24:mi:ss'),'hh24:mi:ss') AS TIME,SAMPLE,TEST,FRAME,MACHINE_NAME,SAMPLE_LENGTH,NOMCOUNT,NOMSTRENGTH,NOMCSP,OPERATOR, ");
           sb.append(" DEPARTMENT,COMMENTS,SHIFT,COUNTTYPE,DBT,WBT,RH,COUNTMIN,COUNTMAX,COUNTAVG,COUNTSD,COUNTCV,COUNTQ95,COUNTQ99, ");
           sb.append(" COUNTRANGE,STRMIN,STRMAX,STRAVG,STRSD,STRCV,STRQ95,STRQ99,STRRANGE,CSPMIN,CSPMAX,CSPAVG,CSPSD,CSPCV,CSPQ95, ");
           sb.append(" CSPQ99,CSPRANGE,RHCOUNTMIN,RHCOUNTMAX,RHCOUNTAVG,RHCOUNTCV,RHCOUNTSD,RHCOUNTQ95,RHCOUNTQ99,RHCOUNTRANGE, ");
           sb.append(" RHSTRMIN,RHSTRMAX,RHSTRAVG,RHSTRCV,RHSTRSD,RHSTRQ95,RHSTRQ99,RHSTRRANGE,RHCSPMIN,RHCSPMAX,RHCSPAVG,RHCSPCV, ");
           sb.append(" RHCSPSD,RHCSPQ95,RHCSPQ99,RHCSPRANGE,RKMVAL,ARKMVAL,REMARKS,TPI,DRAFT ");
           sb.append(" FROM spinwrapmag ");
           sb.append(" Where EntryDate="+sFromDate+" Order by TESTID ");

           System.out.println("TestBaseee-->"+sb.toString());

         try{
            if(theProcessConnection==null){
                    JDBCProcessConnection  jdbc = JDBCProcessConnection.getJDBCConnection();
                    theProcessConnection = jdbc . getConnection();
             }
            java.sql.PreparedStatement pst = theProcessConnection.prepareStatement(sb.toString());
            java.sql.ResultSet rst = pst.executeQuery();
            
            java.sql.ResultSetMetaData rsm = rst.getMetaData();
            
            java.util.HashMap theMap = new java.util.HashMap();
            
            while(rst.next()){
                theMap = new java.util.HashMap();
                for(int i=0;i<rsm.getColumnCount();i++){
                    theMap.put(rsm.getColumnName(i+1), rst.getString(i+1));
                }
                theList.add(theMap);
            }
            rst.close();
            pst.close();
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return theList;
    } 
    public java.util.List getTestNo(String SAsonDate)
    {
        PreparedStatement pst = null;
        ResultSet   rst = null;
        java.util.List thelist = new java.util.ArrayList();
        StringBuffer sb = new StringBuffer();
        sb.append(" select Distinct TESTID From spinwrapmag ");
        sb.append(" where ENTRYDATE <= "+SAsonDate+"" );
        sb.append(" Order By 1 ");
         
        //System.out.println("Test-->"+sb.toString());
        try
        {
            if(theProcessConnection==null){
                    JDBCProcessConnection  jdbc = JDBCProcessConnection.getJDBCConnection();
                    theProcessConnection = jdbc . getConnection();
             }
             pst = theProcessConnection.prepareStatement(sb.toString());
             rst = pst . executeQuery();
             while(rst.next()){
                 thelist.add(rst.getString(1));
             }
             rst.close();
             pst.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return thelist;
    }
}
