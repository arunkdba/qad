/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.data;
import com.jdbc.connection.JDBCProcessConnection;
import com.jdbc.connection.JDBCProcessaConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import com.common.Common;
import com.reports.classes.PrepcommCspDetails;

/**
 *
 * @author admin
 */
public class PrepcommCspData {
    java.sql.Connection theProcessConnection  = null;
    Common common = new Common();
    public PrepcommCspData() {
    }
    public java.util.List getTestDetails(String SOrderNo)
    {
        StringBuilder sb = new StringBuilder();
        PreparedStatement pst = null;
        ResultSet   rst = null;
        java.util.List theList = new java.util.ArrayList();
        PrepcommCspDetails testdetails = new PrepcommCspDetails();
        sb.append(" select to_char(TEST_YARN_REG_DETAILS.INDATE,'yyyymmdd'),MACH_ST_NAME,CNT1,CNT2,CNT3,CNT4,CNT5,STR1,STR2,STR3,STR4,STR5,TEST_YARN_REG_DETAILS.TEST_NO, ");
        sb.append(" TEST_YARN_REG_DETAILS.ORDERNO,TPI,DRAFT,BREAKDRAFT,CPWHEEL,TEST_YARN_REG_DETAILS.PUMBCOLOR,TEST_YARN_REG_DETAILS.TESTTYPE,'Regular' as Type,YARNM.YSHNM,YARNCOUNT.COUNTNAME,test_yarn_reg_details.qadremarks as qadremarks FROM TEST_YARN_REG_DETAILS ");
        sb.append(" Inner join RegularOrder on Regularorder.Rorderno=TEST_YARN_REG_DETAILS.Orderno ");
        sb.append(" Inner Join test_yarn_regular on test_yarn_regular.Test_no=TEST_YARN_REG_DETAILS.Test_no and  ");
        sb.append(" to_char(TEST_YARN_REG_DETAILS.Indate,'yyyymmdd') = to_char(test_yarn_regular.Indate,'yyyymmdd') ");
        sb.append(" Inner Join Rmixir on Rmixir.ORDNO = RegularOrder.ROrderNo and Rmixir.CORRECTIONMIXING=0 ");
	sb.append(" Inner Join yarnm on yarnm.YSHCD = Rmixir.YARNSHADECODE ");
        sb.append(" Inner Join YarnCount on yarnCount.CountCode=RegularOrder.CountCode ");        
        sb.append(" where TEST_YARN_REG_DETAILS.ORDERNO ='").append(SOrderNo).append("'");
        sb.append(" Order By to_char(TEST_YARN_REG_DETAILS.INDATE,'yyyymmdd'),TEST_YARN_REG_DETAILS.TEST_NO ");

        //System.out.println("PrepcommCspData-->"+sb.toString());
        
        try
        {
            if(theProcessConnection==null){
                    JDBCProcessConnection  jdbc = JDBCProcessConnection.getJDBCConnection();
                    theProcessConnection = jdbc . getConnection();
             }
             pst = theProcessConnection.prepareStatement(sb.toString());
             rst = pst . executeQuery();
             while(rst.next()){
                 testdetails = new PrepcommCspDetails();
                 testdetails.setTestdate(rst.getString(1));
                 testdetails.setMechinename(rst.getString(2));
                 testdetails.setCnt1(rst.getString(3));
                 testdetails.setCnt2(rst.getString(4));
                 testdetails.setCnt3(rst.getString(5));
                 testdetails.setCnt4(rst.getString(6));
                 testdetails.setCnt5(rst.getString(7));
                 testdetails.setStrength1(rst.getString(8));
                 testdetails.setStrength2(rst.getString(9));
                 testdetails.setStrength3(rst.getString(10));
                 testdetails.setStrength4(rst.getString(11));
                 testdetails.setStrength5(rst.getString(12));
                 testdetails.setTestno(rst.getString(13));
                 testdetails.setOrderno(rst.getString(14));
                 testdetails.setTpi(rst.getString(15));
                 testdetails.setDraft(rst.getString(16));
                 testdetails.setBdraft(rst.getString(17));
                 testdetails.setCpwheel(rst.getString(18));
                 testdetails.setPumpclr(rst.getString(19));
                 testdetails.setTesttype(rst.getString(20));
                 testdetails.setType(rst.getString(21));
                 testdetails.setShadename(rst.getString(22));
                 testdetails.setCountname(rst.getString(23));
                 testdetails.setCsp1(common.getRound(common.toDouble(rst.getString(3))*common.toDouble(rst.getString(8)),0));
                 testdetails.setCsp2(common.getRound(common.toDouble(rst.getString(4))*common.toDouble(rst.getString(9)),0));
                 testdetails.setCsp3(common.getRound(common.toDouble(rst.getString(5))*common.toDouble(rst.getString(10)),0));
                 testdetails.setCsp4(common.getRound(common.toDouble(rst.getString(6))*common.toDouble(rst.getString(11)),0));
                 testdetails.setCsp5(common.getRound(common.toDouble(rst.getString(7))*common.toDouble(rst.getString(12)),0));
                 testdetails.setBqadremarks(common.parseNull(rst.getString(24)));

                 //System.out.println("Machine Name : "+rst.getString(2));

                 theList.add(testdetails);
             }
             rst.close();
             pst.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return theList;
    }
}