/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.data;
import com.jdbc.connection.JDBCProcessConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import com.common.Common;
import com.reports.classes.PrepcommCspDetailsFresh;
import java.util.StringTokenizer;

/**
 *
 * @author admin
 */
public class PrepcommCspDataFresh {

    java.sql.Connection theProcessConnection  = null;
    Common common = new Common();

    public PrepcommCspDataFresh() {
    }
    public java.util.List getTestDetails(String SOrderNo)
    {
        String sCnt1="",sCnt2="",sCnt3="",sCnt4="",sCnt5="";
        String sStr1="",sStr2="",sStr3="",sStr4="",sStr5="";
        StringBuffer sb = new StringBuffer();
        PreparedStatement pst = null;
        ResultSet   rst = null;
        java.util.List theList = new java.util.ArrayList();
        PrepcommCspDetailsFresh testdetails = new PrepcommCspDetailsFresh();
        sb.append(" SELECT to_char(Test_Yarn_Details.Indate,'yyyymmdd') as indate,Test_Yarn_Details.Test_no,Test_yarn.MACHNAME,wm_concat(Test_Yarn_Details.STRENGTH) as strength, ");
        sb.append(" wm_concat(Test_Yarn_Details.Count) as cnt,Test_yarn_Details.Orderno,'Fresh' as Type FROM Test_Yarn_Details ");
        sb.append(" Inner Join Test_Yarn on Test_Yarn.Test_no=Test_Yarn_Details.Test_no and Test_Yarn_Details.OrderNo=Test_Yarn.OrderNo and  ");
        sb.append(" to_char(Test_Yarn_Details.Indate,'yyyymmdd') = to_char(Test_Yarn.Indate,'yyyymmdd') ");
        sb.append(" where Test_yarn_details.ORDERNO ='"+SOrderNo+"' ");
        sb.append(" Group by Test_Yarn_Details.Indate,Test_Yarn_Details.Test_no,Test_yarn.MACHNAME,Test_yarn_Details.Orderno ");

        //sb.append(" select to_char(INDATE,'yyyymmdd'),MACH_ST_NAME,CNT1,CNT2,CNT3,CNT4,STR1,STR2,STR3,STR4,TEST_NO, ");
        //sb.append(" ORDERNO,TPI,DRAFT,BREAKDRAFT,CPWHEEL,PUMBCOLOR,TESTTYPE FROM TEST_YARN_REG_DETAILS ");
        //sb.append(" where ORDERNO ='"+SOrderNo+"'");

        try
        {
            if(theProcessConnection==null){
                    JDBCProcessConnection  jdbc = JDBCProcessConnection.getJDBCConnection();
                    theProcessConnection = jdbc . getConnection();
             }
             pst = theProcessConnection.prepareStatement(sb.toString());
             rst = pst . executeQuery();
             while(rst.next()){
                 testdetails = new PrepcommCspDetailsFresh();
                 testdetails.setTestdate(rst.getString(1));
                 testdetails.setTestno(rst.getString(2));
                 testdetails.setMechinename(rst.getString(3));
                 StringTokenizer ST  = new StringTokenizer(rst.getString(4),",");

                    if(ST.hasMoreTokens()){sStr1 = ST.nextToken();}
                    if(ST.hasMoreTokens()){sStr2 = ST.nextToken();}
                    if(ST.hasMoreTokens()){sStr3 = ST.nextToken();}
                    if(ST.hasMoreTokens()){sStr4 = ST.nextToken();}
                    if(ST.hasMoreTokens()){sStr5 = ST.nextToken();}

                 testdetails.setStrength1(sStr1);
                 testdetails.setStrength2(sStr2);
                 testdetails.setStrength3(sStr3);
                 testdetails.setStrength4(sStr4);
                 testdetails.setStrength5(sStr5);

                 StringTokenizer ST2  = new StringTokenizer(rst.getString(5),",");

                 if(ST.hasMoreTokens()){sCnt1 = ST2.nextToken();}
                 if(ST.hasMoreTokens()){sCnt2 = ST2.nextToken();}
                 if(ST.hasMoreTokens()){sCnt3 = ST2.nextToken();}
                 if(ST.hasMoreTokens()){sCnt4 = ST2.nextToken();}
                 if(ST.hasMoreTokens()){sCnt5 = ST2.nextToken();}

//                 System.out.println("");

                 testdetails.setCnt1(sCnt1);
                 testdetails.setCnt2(sCnt2);
                 testdetails.setCnt3(sCnt3);
                 testdetails.setCnt4(sCnt4);
                 testdetails.setCnt5(sCnt5);
                 testdetails.setOrderno(rst.getString(6));
                 testdetails.setType(rst.getString(7));
                 testdetails.setCsp1(common.getRound(common.toDouble(sCnt1)*common.toDouble(sStr1),0));
                 testdetails.setCsp2(common.getRound(common.toDouble(sCnt2)*common.toDouble(sStr2),0));
                 testdetails.setCsp3(common.getRound(common.toDouble(sCnt3)*common.toDouble(sStr3),0));
                 testdetails.setCsp4(common.getRound(common.toDouble(sCnt4)*common.toDouble(sStr4),0));
                 testdetails.setCsp5(common.getRound(common.toDouble(sCnt5)*common.toDouble(sStr5),0));
                 theList.add(testdetails);
             }
             rst.close();
             pst.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return theList;
    }
}