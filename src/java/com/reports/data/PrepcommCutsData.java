/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.data;

import com.common.Common;
import com.jdbc.connection.JDBCProcessConnection;
import com.reports.classes.PrepcommTwistTpiDetails;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;

public class PrepcommCutsData {
    java.sql.Connection theProcessConnection  = null;
    Common common = new Common();
    
    public PrepcommCutsData() {
    }
    
    public ArrayList getBreakData(String sOrderNo){
        ArrayList theList = null;
        theList = new ArrayList();
        theList.clear();
        StringBuffer sb = new StringBuffer();
                     sb.append(" select distinct PRODUCTIONGROUP.prod_id,pilotspindles,PRODUCTIONGROUP.prod_name, t_machine.machine_name, PRODUCTIONGROUP.prod_start, ");
                     sb.append(" DW_PRODGROUP_SHIFT.fragshift_id, DW_PRODGROUP_SHIFT.fragshift_start, TO_CHAR(fragshift_start,'YYYYMMDD') as shiftdate, ");
                     sb.append(" TO_CHAR(fragshift_start,'HH24:MI:SS') as starttime, PRODUCTIONGROUP.prod_end, ");
                     sb.append(" PRODUCTIONGROUP.spindle_first, PRODUCTIONGROUP.spindle_last,Cones, LEN,CCis,WEI,BOB,Ccit,ccil, CSIRO,lckSIRO,CS,CT,CP,COFFCNT, ");
                     sb.append(" COFFCNTPLUS,COFFCNTMINUS,CSHOFTOFFCNT, CL,CN,CVCV,Csp from PRODUCTIONGROUP ");
                     sb.append(" inner join DW_PRODGROUP_SHIFT on DW_PRODGROUP_SHIFT.prod_id = PRODUCTIONGROUP.prod_id ");
                     sb.append(" inner join t_machine on t_machine.machine_id = PRODUCTIONGROUP.machine_id ");
                     sb.append(" where NVL(SUBSTR(PRODUCTIONGROUP.prod_name, 0, INSTR(PRODUCTIONGROUP.prod_name, '/')-1), PRODUCTIONGROUP.prod_name)='"+sOrderNo+"' ");
                     sb.append(" order by PRODUCTIONGROUP.prod_id, TO_CHAR(fragshift_start,'YYYYMMDD'), ");
                     sb.append(" TO_CHAR(fragshift_start,'YYYYMMDD'), fragshift_id  ");

        try{
            if(theProcessConnection==null){
                    JDBCProcessConnection  jdbc = JDBCProcessConnection.getJDBCConnection();
                    theProcessConnection = jdbc . getConnection();
             }
            java.sql.PreparedStatement pst = theProcessConnection.prepareStatement(sb.toString());
            java.sql.ResultSet rst = pst.executeQuery();
            
            java.sql.ResultSetMetaData rsm = rst.getMetaData();
            
            java.util.HashMap theMap = new java.util.HashMap();
            
            while(rst.next()){
                theMap = new java.util.HashMap();
                for(int i=0;i<rsm.getColumnCount();i++){
                    theMap.put(rsm.getColumnName(i+1), rst.getString(i+1));
                }
                theList.add(theMap);
            }
            rst.close();
            pst.close();
            
          theList = arrangeData(sOrderNo,theList);
            
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return theList;
    }
     private ArrayList arrangeData(String SOrderNo,ArrayList theList){
        //java.util.ArrayList  theList= null;    
        try{
            java.util.List theCountList = new java.util.ArrayList();
            java.util.List theTempList = new java.util.ArrayList();
            
            for(int i=0;i<theList.size();i++){
                java.util.HashMap theMap = (java.util.HashMap)theList.get(i);
                
                String[] SValues = common.parseNull((String)theMap.get("PROD_NAME")).split("/");
                for(int j=0;j<SValues.length;j++){
                    if(j==0){
                        theMap.put("RORDERNO", SValues[j]);
                    }
                    if(j==1){
                        theMap.put("COUNTNAME", SValues[j]);
                        theMap.put("COUNT", SValues[1].substring(0, SValues[1].length()-1));
                    }
                }
                
                if(!theCountList.contains(common.parseNull((String)theMap.get("COUNT")))){
                    theCountList .add(common.parseNull((String)theMap.get("COUNT")));
                }
            }
            
            Collections.sort(theCountList);

            for(int i=0;i<theCountList.size();i++){
                String SCount = theCountList.get(i).toString();
                
                for(int j=0;j<theList.size();j++){
                    java.util.HashMap theMap = (java.util.HashMap)theList.get(j);
                    
                    if(SCount.trim().equals(common.parseNull((String)theMap.get("COUNT")).trim())){
                        theTempList.add(theMap);
                    }
                }
            }
            
            theList = new java.util.ArrayList();
            theList = (ArrayList) theTempList;

        }catch(Exception ex){
            ex.printStackTrace();
        }
    return theList;
    }
}
