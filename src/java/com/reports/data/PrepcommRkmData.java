/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.data;
import com.jdbc.connection.JDBCProcessConnection;
import com.jdbc.connection.JDBCProcessaConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import com.common.Common;
import java.sql.ResultSetMetaData;
import java.util.HashMap;
import com.reports.classes.PrepcommRkmDetails;
/**
 *
 * @author admin
 */
public class PrepcommRkmData {

    java.sql.Connection theProcessConnection  = null;
    Common common = new Common();
    public PrepcommRkmData() {
    }
    public java.util.List getRkm(String SOrderNo)
    {
        StringBuffer sb = new StringBuffer();
        PreparedStatement pst = null;
        ResultSet   rst = null;
        java.util.List theList = new java.util.ArrayList();
        PrepcommRkmDetails details = new PrepcommRkmDetails();

        sb.append(" SELECT ORDERNO,ENTRYDATE,RKM,RKMCV,ELGCV,ELG,SYS,TESTNO,REMARKS, ");
        sb.append(" SYSCV,MIN_RKM(test_id) as minrkm From rkm_entry ");
        sb.append(" INNER JOIN QAD.TEST_RESULT ON TEST_RESULT.TEST_ID = RKM_ENTRY.TESTNO");
        sb.append(" where ORDERNO='"+SOrderNo+"'");
        sb.append(" GROUP BY ORDERNO,ENTRYDATE,RKM,RKMCV,ELGCV,ELG,SYS,TESTNO,REMARKS,SYSCV,TEST_ID");
        sb.append(" Order by TestNo ");

        System.out.println(sb.toString());
        try
        {
            if(theProcessConnection==null){
                    JDBCProcessConnection  jdbc = JDBCProcessConnection.getJDBCConnection();
                    theProcessConnection = jdbc . getConnection();
             }
             pst = theProcessConnection.prepareStatement(sb.toString());
             rst = pst . executeQuery();
             while(rst.next()){
                 details = new PrepcommRkmDetails();
                 details.setOrderno(rst.getString(1));
                 details.setEntrydate(rst.getString(2));
                 details.setRkm(rst.getString(3));
                 details.setRkmcv(rst.getString(4));
                 details.setElgcv(rst.getString(5));
                 details.setElg(rst.getString(6));
                 details.setSys(rst.getString(7));
                 details.setTestno(rst.getString(8));
                 details.setRemarks(rst.getString(9));
                 details.setSyscv(rst.getString(10));
                 details.setMinrk(rst.getString(11));
                 theList.add(details);
             }
             rst.close();
             pst.close();
        }
         catch(Exception ex)
         {
             ex.printStackTrace();
         }
         return theList;
    }
}