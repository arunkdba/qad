/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.data;

import com.common.Common;
import com.jdbc.connection.JDBCProcessConnection;
import com.reports.classes.PrepcommTwistTpiDetails;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author root
 */
public class PrepcommTwistTpiData {
    java.sql.Connection theProcessConnection  = null;
    Common common = new Common();

    public PrepcommTwistTpiData() {
    }
    public java.util.List getTestDetails(String sOrderNo)
    {
        StringBuffer sb = new StringBuffer();
        PreparedStatement pst = null;
        ResultSet   rst = null;
        java.util.List theList = new java.util.ArrayList();
        PrepcommTwistTpiDetails ttd = new PrepcommTwistTpiDetails();

        sb.append(" SELECT TESTNO,NOMTWIST,NOMCOUNT,FRAME,TESTDATE,LOT,COUNTTYPE,TESTS,MINTPI,MAXTPI,AVGTPI,RANGTPI ");
        sb.append(" FROM TPITESTS ");
        sb.append(" WHERE LOT = '"+sOrderNo+"' ");
        sb.append(" Order by TESTNO ");

        //System.out.println("QRY-->"+sb.toString());

        try{
            if(theProcessConnection==null){
                    JDBCProcessConnection  jdbc = JDBCProcessConnection.getJDBCConnection();
                    theProcessConnection = jdbc . getConnection();
             }
             pst = theProcessConnection.prepareStatement(sb.toString());
             rst = pst . executeQuery();
             while(rst.next()){
                 ttd = new PrepcommTwistTpiDetails();
                 ttd.setTestno(rst.getString(1));
                 ttd.setNomtwist(rst.getString(2));
                 ttd.setNomcount(rst.getString(3));
                 ttd.setFrame(rst.getString(4));
                 ttd.setTestdate(rst.getString(5));
                 ttd.setLot(rst.getString(6));
                 ttd.setCounttype(rst.getString(7));
                 ttd.setTests(rst.getString(8));
                 ttd.setMintpi(rst.getString(9));
                 ttd.setMaxtpi(rst.getString(10));
                 ttd.setAvgtpi(rst.getString(11));
                 ttd.setRangtpi(rst.getString(12));
                 theList.add(ttd);
             }
             rst.close();
             pst.close();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return theList;
    }
}