/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.data;
import com.jdbc.connection.JDBCProcessConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import com.common.Common;
import com.reports.classes.PrepcommUt3Details;
/**
 *
 * @author admin
 */
public class PrepcommUt3Data {

    java.sql.Connection theProcessConnection  = null;
    Common common = new Common();
    public PrepcommUt3Data() {
    }
    public java.util.List getUt3(String SOrderNo)
    {
         StringBuffer sb = new StringBuffer();
         PreparedStatement pst = null;
         ResultSet   rst = null;
         java.util.List theList = new java.util.ArrayList();
         PrepcommUt3Details details = new PrepcommUt3Details();
         sb.append(" SELECT ORDERNO,ENTRYDATE,UPER,THIN,THICK,NEP200,NEP280,TOT200,TOT280,CVM,TESTNO,REMARKS ");
         sb.append(" From ut3_entry where ORDERNO='"+SOrderNo+"' ");
         try
         {
            if(theProcessConnection==null){
                    JDBCProcessConnection  jdbc = JDBCProcessConnection.getJDBCConnection();
                    theProcessConnection = jdbc . getConnection();
             }
             pst = theProcessConnection.prepareStatement(sb.toString());
             rst = pst . executeQuery();
             while(rst.next()){
                 details = new PrepcommUt3Details();
                 details.setOrderno(common.parseNull(rst.getString(1)));
                 details.setEntrydate(common.parseNull(rst.getString(2)));
                 details.setUper(common.parseNull(rst.getString(3)));
                 details.setThin(common.parseNull(rst.getString(4)));
                 details.setThick(common.parseNull(rst.getString(5)));
                 details.setNep200(common.parseNull(rst.getString(6)));
                 details.setNep280(common.parseNull(rst.getString(7)));
                 details.setTot200(common.parseNull(rst.getString(8)));
                 details.setTot280(common.parseNull(rst.getString(9)));
                 details.setCvm(common.parseNull(rst.getString(10)));
                 details.setTestno(common.parseNull(rst.getString(11)));
                 details.setRemarks(common.parseNull(rst.getString(12)));
                 theList.add(details);
             }
             rst.close();
             pst.close();
        }
         catch(Exception ex)
         {
             ex.printStackTrace();
         }
         return theList;
    }
     public java.util.List getUt3newAvg(String SOrderNo){
         StringBuffer sb = new StringBuffer();
         java.util.List theList = new java.util.ArrayList();
         
        /*sb.append(" SELECT UTTHREETEST.TESTID,UTTHREETEST.TESTDATE,ORDERNO,DEPTNAME,TESTNO,UM, ");
        sb.append(" CVM,CVM1M,CVM3M,INDEXX,THINMI30,THINMI40,THINMI50,THICKPL35,THICKPL50, ");
        sb.append(" NEPSPL140,NEPSPL200,NEPSPL280,RELCOUNT,UTTHREETEST.REMARKS,ProcessingType.OEStatus FROM UTTHREETEST ");
        sb.append(" INNER JOIN UTTHREETEST_DETAILS ON UTTHREETEST_DETAILS.TESTID = UTTHREETEST.TESTID ");
        sb.append(" AND UTTHREETEST_DETAILS.TESTDATE = UTTHREETEST.TESTDATE ");
        sb.append(" INNER JOIN RegularOrder on Regularorder.Rorderno=UTTHREETEST.ORDERNO ");
        sb.append(" INNER JOIN scm.ProcessingType on scm.ProcessingType.ProcessCode = regularorder.ProcessTypeCode ");
        sb.append(" WHERE UTTHREETEST.OrderNo = '"+SOrderNo+"' ");
        sb.append(" ORDER BY TESTID ");
        */

        /*sb.append(" SELECT UTTHREETEST.TESTDATE,ORDERNO,DEPTNAME,TESTNO, ");
        sb.append(" sum(UM)/count(UM) as UM, ");
        sb.append(" sum(CVM)/count(CVM) as CVM,sum(CVM1M)/count(CVM1M) as CVM1M,sum(CVM3M)/count(CVM3M) as CVM3M, ");
        sb.append(" sum(INDEXX)/count(INDEXX) as INDEXX,sum(THINMI30)/count(THINMI30) as THINMI30, ");
        sb.append(" sum(THINMI40)/count(THINMI40) as THINMI40,sum(THINMI50)/count(THINMI50) as THINMI50, ");
        sb.append(" sum(THICKPL35)/count(THICKPL35) as THICKPL35,sum(THICKPL50)/count(THICKPL35) as THICKPL35, ");
        sb.append(" sum(NEPSPL140)/count(NEPSPL140) as NEPSPL140,sum(NEPSPL200)/count(NEPSPL200) as NEPSPL200, ");
        sb.append(" sum(NEPSPL280)/count(NEPSPL280),RELCOUNT,UTTHREETEST.REMARKS,ProcessingType.OEStatus FROM UTTHREETEST ");
        sb.append(" INNER JOIN UTTHREETEST_DETAILS ON UTTHREETEST_DETAILS.TESTID = UTTHREETEST.TESTID ");
        sb.append(" AND UTTHREETEST_DETAILS.TESTDATE = UTTHREETEST.TESTDATE ");
        sb.append(" INNER JOIN RegularOrder on Regularorder.Rorderno=UTTHREETEST.ORDERNO ");
        sb.append(" INNER JOIN scm.ProcessingType on scm.ProcessingType.ProcessCode = regularorder.ProcessTypeCode ");
        sb.append(" WHERE UTTHREETEST.OrderNo = '"+SOrderNo+"' ");
        sb.append(" group by UTTHREETEST.TESTDATE,ORDERNO,DEPTNAME,TESTNO, ");
        sb.append(" RELCOUNT,UTTHREETEST.REMARKS,ProcessingType.OEStatus ");
        sb.append(" ORDER BY TESTDATE,TESTNO,DEPTNAME ");*/
        
        
        
        /*sb.append(" SELECT UTTHREETEST.TESTDATE,ORDERNO,DEPTNAME,TESTNO, ");
        sb.append(" UM,CVM,CVM1M,CVM3M,INDEXX,THINMI30,THINMI40,THINMI50, ");
        sb.append(" THICKPL35,THICKPL35,NEPSPL140,NEPSPL200,NEPSPL280,RELCOUNT,UTTHREETEST.REMARKS,ProcessingType.OEStatus FROM UTTHREETEST ");
        sb.append(" INNER JOIN UTTHREETEST_DETAILS ON UTTHREETEST_DETAILS.TESTID = UTTHREETEST.TESTID ");
        sb.append(" AND UTTHREETEST_DETAILS.TESTDATE = UTTHREETEST.TESTDATE ");
        sb.append(" INNER JOIN RegularOrder on Regularorder.Rorderno=UTTHREETEST.ORDERNO ");
        sb.append(" INNER JOIN scm.ProcessingType on scm.ProcessingType.ProcessCode = regularorder.ProcessTypeCode ");
        sb.append(" WHERE UTTHREETEST.OrderNo = '"+SOrderNo+"' ");
        sb.append(" ORDER BY TESTDATE,TESTNO,DEPTNAME ");*/
        
        sb.append(" SELECT ORDERNO,DEPTNAME,sum(UM)/count(UM) as UM, ");
        sb.append(" sum(CVM)/count(CVM) as CVM,sum(CVM1M)/count(CVM1M) as CVM1M,sum(CVM3M)/count(CVM3M) as CVM3M, ");
        sb.append(" sum(INDEXX)/count(INDEXX) as INDEXX,sum(THINMI30)/count(THINMI30) as THINMI30, ");
        sb.append(" sum(THINMI40)/count(THINMI40) as THINMI40,sum(THINMI50)/count(THINMI50) as THINMI50, ");
        sb.append(" sum(THICKPL35)/count(THICKPL35) as THICKPL35,sum(THICKPL50)/count(THICKPL50) as THICKPL50, ");
        sb.append(" sum(NEPSPL140)/count(NEPSPL140) as NEPSPL140,sum(NEPSPL200)/count(NEPSPL200) as NEPSPL200, ");
        sb.append(" sum(NEPSPL280)/count(NEPSPL280) as NEPSPL280,ProcessingType.OEStatus FROM UTTHREETEST ");
        sb.append(" INNER JOIN UTTHREETEST_DETAILS ON UTTHREETEST_DETAILS.TESTID = UTTHREETEST.TESTID ");
        sb.append(" AND UTTHREETEST_DETAILS.TESTDATE = UTTHREETEST.TESTDATE ");
        sb.append(" INNER JOIN RegularOrder on Regularorder.Rorderno=UTTHREETEST.ORDERNO ");
        sb.append(" INNER JOIN scm.ProcessingType on scm.ProcessingType.ProcessCode = regularorder.ProcessTypeCode ");
        sb.append(" WHERE UTTHREETEST.OrderNo = '"+SOrderNo+"' ");
        sb.append(" group by ORDERNO,DEPTNAME,ProcessingType.OEStatus ");
        sb.append(" ORDER BY DEPTNAME ");

        try{
            if(theProcessConnection==null){
                    JDBCProcessConnection  jdbc = JDBCProcessConnection.getJDBCConnection();
                    theProcessConnection = jdbc . getConnection();
             }
            java.sql.PreparedStatement pst = theProcessConnection.prepareStatement(sb.toString());
            java.sql.ResultSet rst = pst.executeQuery();
            
            java.sql.ResultSetMetaData rsm = rst.getMetaData();
            
            java.util.HashMap theMap = new java.util.HashMap();

            while(rst.next()){
                theMap = new java.util.HashMap();
                for(int i=0;i<rsm.getColumnCount();i++){
                    theMap.put(rsm.getColumnName(i+1), rst.getString(i+1));
                }
                theList.add(theMap);
            }
            rst.close();
            pst.close();
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return theList;
    }
    public java.util.List getUt3new(String SOrderNo){
         StringBuffer sb = new StringBuffer();
         java.util.List theList = new java.util.ArrayList();
         
        sb.append(" SELECT UTTHREETEST.TESTID,UTTHREETEST.TESTDATE,ORDERNO,DEPTNAME,TESTNO,UM, ");
        sb.append(" CVM,CVM1M,CVM3M,INDEXX,THINMI30,THINMI40,THINMI50,THICKPL35,THICKPL50, ");
        sb.append(" NEPSPL140,NEPSPL200,NEPSPL280,RELCOUNT,UTTHREETEST.REMARKS,ProcessingType.OEStatus FROM UTTHREETEST ");
        sb.append(" INNER JOIN UTTHREETEST_DETAILS ON UTTHREETEST_DETAILS.TESTID = UTTHREETEST.TESTID ");
        sb.append(" AND UTTHREETEST_DETAILS.TESTDATE = UTTHREETEST.TESTDATE ");
        sb.append(" INNER JOIN RegularOrder on Regularorder.Rorderno=UTTHREETEST.ORDERNO ");
        sb.append(" INNER JOIN scm.ProcessingType on scm.ProcessingType.ProcessCode = regularorder.ProcessTypeCode ");
        sb.append(" WHERE UTTHREETEST.OrderNo = '"+SOrderNo+"' ");
        sb.append(" ORDER BY TESTID ");

        try{
            if(theProcessConnection==null){
                    JDBCProcessConnection  jdbc = JDBCProcessConnection.getJDBCConnection();
                    theProcessConnection = jdbc . getConnection();
             }
            java.sql.PreparedStatement pst = theProcessConnection.prepareStatement(sb.toString());
            java.sql.ResultSet rst = pst.executeQuery();
            
            java.sql.ResultSetMetaData rsm = rst.getMetaData();
            
            java.util.HashMap theMap = new java.util.HashMap();

            while(rst.next()){
                theMap = new java.util.HashMap();
                for(int i=0;i<rsm.getColumnCount();i++){
                    theMap.put(rsm.getColumnName(i+1), rst.getString(i+1));
                }
                theList.add(theMap);
            }
            rst.close();
            pst.close();
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return theList;
    }
    public java.util.List getUt3Test(String SOrderNo){
         StringBuffer sb = new StringBuffer();
         java.util.List theList = new java.util.ArrayList();
         
        sb.append(" SELECT DISTINCT UTTHREETEST.TESTID,UTTHREETEST.TESTDATE,ORDERNO,DEPTNAME,MACHNO,MACHSIDE,PASSAGE,ARTNO,FIBREASSEMBLY,FIBER, ");
        sb.append(" VDATA,TDATA,TESTS,SLOT,YARNTENSION,IMPERFECTIONS FROM UTTHREETEST ");
        sb.append(" INNER JOIN UTTHREETEST_DETAILS ON UTTHREETEST_DETAILS.TESTID = UTTHREETEST.TESTID ");
        sb.append(" AND UTTHREETEST_DETAILS.TESTDATE = UTTHREETEST.TESTDATE ");
        sb.append(" WHERE UTTHREETEST.OrderNo = '"+SOrderNo+"' ");
        sb.append(" ORDER BY TESTID ");
        
         try{
            if(theProcessConnection==null){
                    JDBCProcessConnection  jdbc = JDBCProcessConnection.getJDBCConnection();
                    theProcessConnection = jdbc . getConnection();
             }
            java.sql.PreparedStatement pst = theProcessConnection.prepareStatement(sb.toString());
            java.sql.ResultSet rst = pst.executeQuery();
            
            java.sql.ResultSetMetaData rsm = rst.getMetaData();
            
            java.util.HashMap theMap = new java.util.HashMap();
            
            while(rst.next()){
                theMap = new java.util.HashMap();
                for(int i=0;i<rsm.getColumnCount();i++){
                    theMap.put(rsm.getColumnName(i+1), rst.getString(i+1));
                }
                theList.add(theMap);
            }
            rst.close();
            pst.close();
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return theList;
    }
}