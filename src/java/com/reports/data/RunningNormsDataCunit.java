/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.data;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import com.jdbc.connection.JDBCProcessConnection;
import com.common.Common;
import com.reports.classes.CoEffVarient;
import com.reports.classes.Ut3Details;
import com.reports.classes.RkmDetails;
import com.reports.classes.Ut3MonthWiseDetails;
import com.reports.classes.Ut3class;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.HashMap;
/**
 *
 * @author admin
 */
public class RunningNormsDataCunit {
    java.sql.Connection theProcessConnection = null;
    Common common = new Common();
    java.util.List theDateList;
    java.util.List theDateListAll = new java.util.ArrayList();
    java.util.List theOrdList = new java.util.ArrayList();
    java.util.List rkmList = new java.util.ArrayList();
    java.util.List Ut3List = new java.util.ArrayList();
    java.util.List CspList = new java.util.ArrayList();
    java.util.List MixingList = new java.util.ArrayList();
    java.util.List OrderList = new java.util.ArrayList();
    java.util.List MixingDepthList = new java.util.ArrayList();
    java.util.List MixingLmc = new java.util.ArrayList();
    java.util.List MixingOthers = new java.util.ArrayList();
    java.util.List theDataList =     null;

    private int idl=0;

    public RunningNormsDataCunit() {
    }    
    public java.util.List getUt3(String sFromDate, String sToDate,String sOrderNo)
    {        
        PreparedStatement pst = null;
        ResultSet rst = null;
        StringBuilder sb = new StringBuilder();
        
        sb.append(" SELECT ut3_entry.ORDERNO,ut3_entry.ENTRYDATE,sum(UPER)/count(UPER),sum(THIN)/count(THIN),sum(THICK)/count(THICK), ");
        sb.append(" sum(NEP200)/count(NEP200),sum(NEP280)/count(NEP280),sum(TOT200)/count(TOT200),sum(TOT280)/count(TOT280),sum(CVM)/count(CVM) From ut3_entry ");
        sb.append(" Inner join RegularOrder on Regularorder.Rorderno=ut3_entry.Orderno ");
        sb.append(" Inner join scm.ProcessingType on scm.ProcessingType.ProcessCode = regularorder.ProcessTypeCode ");        
        sb.append(" WHERE Regularorder.UnitCode in (10,12) ");
        if(!sOrderNo.equals("")){
                    //sb.append(" AND ORDERNO = '"+sOrderNo+"' ");
            sb.append(" AND ORDERNO in ("+sOrderNo+") ");
        }
        sb.append(" Group by ut3_entry.ENTRYDATE,ut3_entry.ORDERNO ");
        sb.append(" Order by 1,2 ");

        //System.out.println("Ut3-->"+sb.toString());
        try
        {
            if(theProcessConnection == null)
            {
                JDBCProcessConnection jdbc = JDBCProcessConnection.getJDBCConnection();
                theProcessConnection = jdbc.getConnection();
            }
            pst = theProcessConnection.prepareStatement(sb.toString());
            rst = pst.executeQuery();
            while(rst.next())
            {
                 Ut3class details = new Ut3class();
                 details.setOrderno(common.parseNull(rst.getString(1)));
                 details.setEntrydate(common.parseNull(rst.getString(2)));
                 details.setUper(common.parseNull(rst.getString(3)));
                 details.setThin(common.parseNull(rst.getString(4)));
                 details.setThick(common.parseNull(rst.getString(5)));
                 details.setNep200(common.parseNull(rst.getString(6)));
                 details.setNep280(common.parseNull(rst.getString(7)));
                 details.setTot200(common.parseNull(rst.getString(8)));
                 details.setTot280(common.parseNull(rst.getString(9)));
                 details.setCvm(common.parseNull(rst.getString(10)));                 
                 Ut3List.add(details);
            }
            rst.close();
            pst.close();
        }
        catch(Exception ex){
        }
        return Ut3List;
    }
    public void getRkm(String SFrom, String STo,String sOrderNo){
        rkmList = new java.util.ArrayList();
        PreparedStatement pst = null;
        ResultSet rst = null;
        StringBuilder sb = new StringBuilder();
        RkmDetails details = new RkmDetails();

        sb.append(" SELECT rkm_entry.ORDERNO,rkm_entry.ENTRYDATE,sum(RKM)/count(RKM),sum(RKMCV)/count(RKMCV),sum(ELGCV)/count(ELGCV), ");
        sb.append(" sum(ELG)/count(ELG),sum(SYS)/count(SYS) ");
        sb.append(" From rkm_entry ");
        sb.append(" Inner join RegularOrder on Regularorder.Rorderno=rkm_entry.Orderno ");
        sb.append(" Inner join scm.ProcessingType on scm.ProcessingType.ProcessCode = regularorder.ProcessTypeCode ");
        //sb.append(" WHERE rkm_entry.ENTRYDATE >= ").append(SFrom).append(" AND rkm_entry.ENTRYDATE <= ").append(STo).append(" and ProcessingType.OEStatus = 1 ");
        sb.append(" WHERE Regularorder.UnitCode in(10,12) "); //and Orderno='LM65228' ");
        if(!sOrderNo.equals("")){
                    //sb.append(" AND ORDERNO = '"+sOrderNo+"' ");
                    sb.append(" AND ORDERNO in ("+sOrderNo+") ");
        }        
        sb.append(" Group by ORDERNO,rkm_entry.ENTRYDATE ");
        sb.append(" Order by 1,2 ");
        
       // System.out.println("Ut3-->"+sb.toString());
        try{
            if(theProcessConnection==null){
                JDBCProcessConnection jdbc = JDBCProcessConnection.getJDBCConnection();
                theProcessConnection = jdbc.getConnection();
            }
            pst = theProcessConnection.prepareStatement(sb.toString());
            rst = pst.executeQuery();
            while(rst.next()){
                 details = new RkmDetails();
                 details.setOrderno(rst.getString(1));
                 details.setEntrydate(rst.getString(2));
                 details.setRkm(rst.getString(3));
                 details.setRkmcv(rst.getString(4));
                 details.setElgcv(rst.getString(5));
                 details.setElg(rst.getString(6));
                 details.setSys(rst.getString(7));
                 rkmList.add(details);
             }
             rst.close();
             pst.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }       
    }
                
    public int getcount(java.util.List TestUt3list,String SDate,String Machine){
        int iCnt = 0;        
        for (int i = 0; i < TestUt3list.size(); i++) {
            com.reports.classes.Ut3MonthWiseDetails Td = (com.reports.classes.Ut3MonthWiseDetails) TestUt3list.get(i);
            if (SDate.equals(Td.getEntrydate())) {
                iCnt +=1;
            }
        }
        return iCnt;
    }
 public java.util.List getOrderNo(String SFrom, String STo,String sOrderNo ) {
        java.util.List thelist = new java.util.ArrayList(); 
        PreparedStatement pst = null;
        ResultSet rst = null;
        StringBuilder sb = new StringBuilder();

                sb . append(" SELECT DISTINCT ORDERNO,ENTRYDATE,PARTYNAME,YSHNM,WEIGHT,COUNTNAME,BLEND FROM( ");
                sb . append(" SELECT ut3_entry.ORDERNO,ut3_entry.ENTRYDATE,PartyMaster.PartyName,YARNM.YSHNM,RegularOrder.Weight, ");
                sb . append(" yarnCount.CountName,Ut3_Report_Blend.Blend From ut3_entry ");
                sb . append(" Inner join RegularOrder on Regularorder.Rorderno=ut3_entry.Orderno ");
                sb . append(" Inner join PartyMaster on PartyMaster.Partycode= RegularOrder.PartyCode ");                
                sb . append(" INNER JOIN SAMPLETOREGULAR ON SAMPLETOREGULAR.REGULARORDERNO = RegularOrder.ROrderNo AND SAMPLETOREGULAR.CORRECTIONMIXING=0 ");
                sb . append(" LEFT JOIN YARNM ON SAMPLETOREGULAR.YARNSHADECODE = YARNM.YSHCD ");
                sb . append(" Inner Join YarnCount on yarnCount.CountCode=RegularOrder.CountCode ");
                sb . append(" Left Join Ut3_Report_Blend on Ut3_Report_Blend.Orderno = RegularOrder.ROrderNo AND ACTIVESTATUS=0 ");
                sb . append(" Inner join scm.ProcessingType on scm.ProcessingType.ProcessCode = regularorder.ProcessTypeCode ");
                //sb . append(" WHERE ut3_entry.ENTRYDATE >= "+SFrom+" AND ut3_entry.ENTRYDATE <= "+STo+" and ProcessingType.OEStatus = 1 ");
                sb . append(" WHERE Regularorder.UnitCode in (10,12) ");
                if(!sOrderNo.equals("")){
                    //sb.append(" AND ut3_entry.ORDERNO = '"+sOrderNo+"' ");
                    sb.append(" AND ut3_entry.ORDERNO in ("+sOrderNo+") ");
                }
                sb . append(" Union All ");
                sb . append(" SELECT rkm_entry.ORDERNO,rkm_entry.ENTRYDATE,PartyMaster.PartyName,YARNM.YSHNM,RegularOrder.Weight, ");
                sb . append(" yarnCount.CountName,Ut3_Report_Blend.Blend From rkm_entry ");
                sb . append(" Inner join RegularOrder on Regularorder.Rorderno=rkm_entry.Orderno ");
                sb . append(" Inner join PartyMaster on PartyMaster.Partycode= RegularOrder.PartyCode ");                
                sb . append(" INNER JOIN SAMPLETOREGULAR ON SAMPLETOREGULAR.REGULARORDERNO = RegularOrder.ROrderNo AND SAMPLETOREGULAR.CORRECTIONMIXING=0 ");
                sb . append(" LEFT JOIN YARNM ON SAMPLETOREGULAR.YARNSHADECODE = YARNM.YSHCD ");
                sb . append(" Inner Join YarnCount on yarnCount.CountCode=RegularOrder.CountCode ");
                sb . append(" Left Join Ut3_Report_Blend on Ut3_Report_Blend.Orderno = RegularOrder.ROrderNo AND ACTIVESTATUS=0 ");
                sb . append(" Inner join scm.ProcessingType on scm.ProcessingType.ProcessCode = regularorder.ProcessTypeCode ");
                //sb . append(" WHERE rkm_entry.ENTRYDATE >= "+SFrom+" AND rkm_entry.ENTRYDATE <= "+STo+" and ProcessingType.OEStatus = 1 ");
                sb . append(" WHERE Regularorder.UnitCode in(10,12) ");
                if(!sOrderNo.equals("")){
                    //sb.append(" AND rkm_entry.ORDERNO = '"+sOrderNo+"' ");
                    sb.append(" AND rkm_entry.ORDERNO in("+sOrderNo+") ");
                }
                sb . append(" Union All ");
                sb . append(" select orderno,to_number(entrydate),partyname,yarnm.YSHNM,t.Weight,countname,Blend ");
                sb . append(" from ( ");
                sb . append(" select to_char(indate,'yyyymmdd') as entrydate,test_no,mach_st_name,test_yarn_reg_details.orderno,dbcode, ");
                sb . append(" yarncount.countname,partymaster.partyname, ");
                sb . append(" nvl(cnt1,0) as cnt1,nvl(cnt2,0) as cnt2,nvl(cnt3,0) as cnt3,nvl(cnt4,0) as cnt4, ");
                sb . append(" nvl(str1,0) as str1,nvl(str2,0) as str2,nvl(str3,0) as str3,nvl(str4,0) as str4, ");
                sb . append(" nvl(cnt1,0)+nvl(cnt2,0)+nvl(cnt3,0)+nvl(cnt4,0) as sumcount, ");
                sb . append(" nvl(str1,0)+nvl(str2,0)+nvl(str3,0)+nvl(str4,0) as sumstrength, ");
                sb . append(" decode(nvl(cnt1,0),0,0,cnt1)*decode(nvl(str1,0),0,0,str1)+decode(nvl(cnt2,0),0,0,cnt2)*decode(nvl(str2,0),0,0,str2)+ ");
                sb . append(" decode(nvl(str3,0),0,0,cnt3)*decode(nvl(cnt3,0),0,0,str3)+decode(nvl(cnt4,0),0,0,cnt4)*decode(nvl(str4,0),0,0,str4) ");
                sb . append(" as csp, ");
                sb . append(" decode(nvl(cnt1,0),0,0,1)+decode(nvl(cnt2,0),0,0,1)+decode(nvl(cnt3,0),0,0,1)+decode(nvl(cnt4,0),0,0,1) as countavgfact, ");
                sb . append(" decode(nvl(str1,0),0,0,1)+decode(nvl(str2,0),0,0,1)+decode(nvl(str3,0),0,0,1)+decode(nvl(str4,0),0,0,1) as stravgfact, ");
                sb . append(" decode(decode(nvl(cnt1,0),0,0,cnt1)*decode(nvl(str1,0),0,0,str1),0,0,1)+decode(decode(nvl(cnt2,0),0,0,cnt2)*decode(nvl(str2,0),0,0,str2),0,0,1)+ ");
                sb . append(" decode(decode(nvl(str3,0),0,0,cnt3)*decode(nvl(cnt3,0),0,0,str3),0,0,1)+decode(decode(nvl(cnt4,0),0,0,cnt4)*decode(nvl(str4,0),0,0,str4),0,0,1) ");
                sb . append(" as cspavgfact,RegularOrder.Weight,Ut3_Report_Blend.Blend ");
                sb . append(" from test_yarn_reg_details ");
                sb . append(" inner join regularorder on regularorder.rorderno = test_yarn_reg_details.orderno and (Regularorder.UnitCode=10 or Regularorder.UnitCode=12) ");
                sb . append(" inner join partymaster on partymaster.partycode = regularorder.partycode ");
                sb . append(" inner join yarncount on yarncount.countcode=regularorder.countcode ");
                sb . append(" Left Join Ut3_Report_Blend on Ut3_Report_Blend.Orderno = RegularOrder.ROrderNo AND ACTIVESTATUS=0 ");
                sb . append(" Inner join scm.ProcessingType on scm.ProcessingType.ProcessCode = regularorder.ProcessTypeCode ");
                //sb . append(" where to_char(INDATE,'yyyymmdd') >= "+SFrom+" and to_char(INDATE,'yyyymmdd') <="+STo+" and ProcessingType.OEStatus = 1 ");
                if(!sOrderNo.equals("")){
                    //sb.append(" WHERE test_yarn_reg_details.ORDERNO = '"+sOrderNo+"' ");
                    sb.append(" WHERE test_yarn_reg_details.ORDERNO In("+sOrderNo+") ");
                }
                sb . append(" ) t ");
                sb . append(" INNER JOIN SAMPLETOREGULAR ON SAMPLETOREGULAR.REGULARORDERNO = t.ORDERNO AND SAMPLETOREGULAR.CORRECTIONMIXING=0 ");
                sb . append(" LEFT JOIN YARNM ON SAMPLETOREGULAR.YARNSHADECODE = YARNM.YSHCD ");
                //sb . append(" ) ORDER BY 2,1,6 ");
                
                sb . append(" ) ORDER BY 6,1,2 ");

        try {
            if(theProcessConnection==null){
                JDBCProcessConnection jdbc = JDBCProcessConnection.getJDBCConnection();
                theProcessConnection = jdbc.getConnection();
            }
            pst = theProcessConnection.prepareStatement(sb.toString());
            rst = pst.executeQuery();
            ResultSetMetaData rsmd    = rst.getMetaData();
            while(rst.next())
            {
                    HashMap themap = new HashMap();
                    for(int i=0;i<rsmd.getColumnCount();i++)
                    {
                        themap.put(rsmd.getColumnName(i+1),rst.getString(i+1));
                    }
                    thelist.add(themap);
            }
            rst.close();
            pst.close();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return thelist;
    }
    public java.util.List getUt3Data(String SFrom, String STo,String sUnitCode,String sOrderNo){
        CspList = new java.util.ArrayList();
        theDataList = new java.util.ArrayList();
        theOrdList = new java.util.ArrayList();
        java.util.HashMap theDataMap =new java.util.HashMap();
        PreparedStatement pst = null;
        ResultSet rst = null;
        StringBuilder sb = new StringBuilder();
        Ut3MonthWiseDetails details = new Ut3MonthWiseDetails();
        Ut3Details ut3 = new Ut3Details();
        sb.append(" select entrydate,test_no,mach_st_name,orderno,dbcode,countname,yarnm.YSHNM,partyname, ");
        sb.append(" sumcount/decode(countavgfact,0,1,countavgfact) as avgcnt, ");
        sb.append(" cnt1,cnt2,cnt3,cnt4,str1,str2,str3,str4, ");
        sb.append(" sumstrength/decode(stravgfact,0,1,stravgfact) as avgstr,csp/decode(cspavgfact,0,1,cspavgfact) as avgcsp, ");
        sb.append(" countavgfact,stravgfact,cspavgfact,t.Weight,cnt5,str5 ");
        sb.append(" from ");
        sb.append(" ( ");
        sb.append(" select to_char(indate,'yyyymmdd') as entrydate,test_no,mach_st_name,test_yarn_reg_details.orderno,dbcode, ");
        sb.append(" yarncount.countname,partymaster.partyname, ");
        sb.append(" nvl(cnt1,0) as cnt1,nvl(cnt2,0) as cnt2,nvl(cnt3,0) as cnt3,nvl(cnt4,0) as cnt4, ");
        sb.append(" nvl(str1,0) as str1,nvl(str2,0) as str2,nvl(str3,0) as str3,nvl(str4,0) as str4, ");
        sb.append(" nvl(cnt1,0)+nvl(cnt2,0)+nvl(cnt3,0)+nvl(cnt4,0)+nvl(cnt5,0) as sumcount, ");
        sb.append(" nvl(str1,0)+nvl(str2,0)+nvl(str3,0)+nvl(str4,0)+nvl(str5,0) as sumstrength, ");
        sb.append(" decode(nvl(cnt1,0),0,0,cnt1)*decode(nvl(str1,0),0,0,str1)+decode(nvl(cnt2,0),0,0,cnt2)*decode(nvl(str2,0),0,0,str2)+ ");
        sb.append(" decode(nvl(str3,0),0,0,cnt3)*decode(nvl(cnt3,0),0,0,str3)+decode(nvl(cnt4,0),0,0,cnt4)*decode(nvl(str4,0),0,0,str4)+ ");
        sb.append(" decode(nvl(cnt5,0),0,0,cnt5)*decode(nvl(str5,0),0,0,str5) as csp, ");
        sb.append(" decode(nvl(cnt1,0),0,0,1)+decode(nvl(cnt2,0),0,0,1)+decode(nvl(cnt3,0),0,0,1)+decode(nvl(cnt4,0),0,0,1)+decode(nvl(cnt5,0),0,0,1) as countavgfact, ");
        sb.append(" decode(nvl(str1,0),0,0,1)+decode(nvl(str2,0),0,0,1)+decode(nvl(str3,0),0,0,1)+decode(nvl(str4,0),0,0,1)+decode(nvl(str5,0),0,0,1) as stravgfact, ");
        sb.append(" decode(decode(nvl(cnt1,0),0,0,cnt1)*decode(nvl(str1,0),0,0,str1),0,0,1)+decode(decode(nvl(cnt2,0),0,0,cnt2)*decode(nvl(str2,0),0,0,str2),0,0,1)+ ");
        sb.append(" decode(decode(nvl(str3,0),0,0,cnt3)*decode(nvl(cnt3,0),0,0,str3),0,0,1)+decode(decode(nvl(cnt4,0),0,0,cnt4)*decode(nvl(str4,0),0,0,str4),0,0,1)+ ");
        sb.append(" decode(decode(nvl(cnt5,0),0,0,cnt5)*decode(nvl(str5,0),0,0,str5),0,0,1) as cspavgfact,RegularOrder.Weight,nvl(cnt5,0) as cnt5,nvl(str5,0) as str5 ");
        sb.append(" from test_yarn_reg_details ");
        sb.append(" inner join regularorder on regularorder.rorderno = test_yarn_reg_details.orderno and (Regularorder.UnitCode=10 or Regularorder.UnitCode=12) ");
        sb.append(" inner join partymaster on partymaster.partycode = regularorder.partycode ");
        sb.append(" inner join yarncount on yarncount.countcode=regularorder.countcode ");        
        sb.append(" Inner join scm.ProcessingType on scm.ProcessingType.ProcessCode = regularorder.ProcessTypeCode ");
        //sb.append(" where to_char(INDATE,'yyyymmdd') >="+SFrom+" and to_char(INDATE,'yyyymmdd') <="+STo+" and ProcessingType.OEStatus = 1 ");
        if(!sOrderNo.equals("")){
            //sb.append(" WHERE ORDERNO = '"+sOrderNo+"' ");
            sb.append(" WHERE ORDERNO in("+sOrderNo+") ");
        }
        sb.append(" ) t ");
        sb.append(" INNER JOIN SAMPLETOREGULAR ON SAMPLETOREGULAR.REGULARORDERNO = t.ORDERNO AND SAMPLETOREGULAR.CORRECTIONMIXING=0 ");
        sb.append(" LEFT JOIN YARNM ON SAMPLETOREGULAR.YARNSHADECODE = YARNM.YSHCD ");                
        sb.append(" order by 4,1,3 ");
        
        //System.out.println("qry==>"+sb.toString());
        
        try{
            if(theProcessConnection==null){
                JDBCProcessConnection jdbc = JDBCProcessConnection.getJDBCConnection();
                theProcessConnection = jdbc.getConnection();
            }
            pst = theProcessConnection.prepareStatement(sb.toString());
            rst = pst.executeQuery();
            while(rst.next()){
                 details = new Ut3MonthWiseDetails();
                 details.setOrderno(rst.getString(4));
                 details.setEntrydate(rst.getString(1));
                 details.setMachinename(rst.getString(3));
                 details.setShadename(rst.getString(7));
                 details.setPartyname(rst.getString(8));
                 details.setAvgcnt(rst.getString(9));
                 details.setCnt1(rst.getString(10));
                 details.setCnt2(rst.getString(11));
                 details.setCnt3(rst.getString(12));
                 details.setCnt4(rst.getString(13));
                 details.setStrength1(rst.getString(14));
                 details.setStrength2(rst.getString(15));
                 details.setStrength3(rst.getString(16));
                 details.setStrength4(rst.getString(17));
                 details.setAvgstr(rst.getString(18));
                 details.setAvgcsp(rst.getString(19));
                 details.setCntreading(rst.getString(20));
                 details.setStrreading(rst.getString(21));
                 details.setCspreading(rst.getString(22));
                 details.setOrdweight(rst.getString(23));
                 details.setCnt5(rst.getString(24));
                 details.setStrength5(rst.getString(25));
                 
                 CspList.add(details);
             }
             rst.close();
             pst.close();
             theOrdList = getOrderNo(SFrom, STo,sOrderNo);
             getRkm(SFrom, STo,sOrderNo);
             getUt3(SFrom, STo,sOrderNo);
             getMixingData(SFrom, STo,sOrderNo);
             getMixingDepth(SFrom, STo,sOrderNo);
             getLowMicPoc(SFrom, STo,sOrderNo);
             getPocOthers(SFrom, STo,sOrderNo);
             setDataList();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return theDataList;
    }

    public void setDataList(){

        HashMap theMap = null;
        HashMap OrderMap = null;        
        
        String SOrdno="";
        String SPrevOrdno="";
        String SMechine="",SParty="",SShade="",SOrderQty="",SCountName="", sBlend="";;
        String sUper="",SCvm="",SThin="",SThick="",sNeps="",sNeps280="",sTot200="",sTot280="";
        String SSys="",sRkm="",sRkmCv="",sElong="",sElongCv="";
        String sVsf="",sDvsf="",sGv="",sDgv="",sPocLmc="",sPocDch="",sDccombed="",sKarded="",sCombed="",sDepth="";
        String sKardVar="",sCombVar="",sLotNo="",sBCotton="",sWaste="",sModal="",sMixed="",sOthers="";
        String sDPocLmc="",sDPocDch="";
        String sPocOthers ="",sDPocOthers="";
        double dCount=0,dStr=0,dCountCv=0,dStrCv=0;
        double dMixTot=0;
        double dPocLmc=0,dPocDch=0,dDPocLmc=0,dDPocDch=0;
        int iNoReadingCnt=0,iNoReadingStr=0,iNoReadingCsp=0;

        for (int i = 0; i < theOrdList.size(); i++) {

                        OrderMap        =  (HashMap)theOrdList.get(i);
                        SOrdno          = (String)OrderMap.get("ORDERNO");
                        SCountName      = (String)OrderMap.get("COUNTNAME");
                        sBlend          = (String) OrderMap.get("BLEND");
                        SMechine        = getMachineName(SOrdno,(String)OrderMap.get("ENTRYDATE"));
                        SParty          = (String)OrderMap.get("PARTYNAME");
                        SShade          = (String)OrderMap.get("YSHNM");
                        SOrderQty       = (String)OrderMap.get("WEIGHT");
                        iNoReadingCnt   = getCntReading(SOrdno,(String)OrderMap.get("ENTRYDATE"));
                        iNoReadingStr   = getStrReading(SOrdno,(String)OrderMap.get("ENTRYDATE"));
                        iNoReadingCsp   = getCspReading(SOrdno,(String)OrderMap.get("ENTRYDATE"));
                        dCount          = getCount(SOrdno,(String)OrderMap.get("ENTRYDATE"));
                        dStr            = getStr(SOrdno,(String)OrderMap.get("ENTRYDATE"));
                        dCountCv        = getCntCv(SOrdno,(String)OrderMap.get("ENTRYDATE"));
                        dStrCv          = getStrCv(SOrdno,(String)OrderMap.get("ENTRYDATE"));
                        SSys            = getRkmSys(SOrdno,(String)OrderMap.get("ENTRYDATE"));
                        sRkm            = getRkmrkm(SOrdno,(String)OrderMap.get("ENTRYDATE"));
                        sRkmCv          = getRkmCv(SOrdno,(String)OrderMap.get("ENTRYDATE"));
                        sElong          = getElang(SOrdno,(String)OrderMap.get("ENTRYDATE"));
                        sElongCv        = getElangCv(SOrdno,(String)OrderMap.get("ENTRYDATE"));
                        sUper           = getUpercent(SOrdno,(String)OrderMap.get("ENTRYDATE"));
                        SCvm            = getCvm(SOrdno,(String)OrderMap.get("ENTRYDATE"));
                        SThin           = getThin(SOrdno,(String)OrderMap.get("ENTRYDATE"));
                        SThick          = getThick(SOrdno,(String)OrderMap.get("ENTRYDATE"));
                        sNeps           = getNeps(SOrdno,(String)OrderMap.get("ENTRYDATE"));
                        sNeps280        = getNeps280(SOrdno,(String)OrderMap.get("ENTRYDATE"));
                        sTot200         = getTot200(SOrdno,(String)OrderMap.get("ENTRYDATE"));
                        sTot280         = getTot280(SOrdno,(String)OrderMap.get("ENTRYDATE"));
                        // Mixing 
                        if(isExists(SOrdno)){
            
                        sVsf = getVsf(SOrdno,"1");
                        sDvsf = getDVsf(SOrdno,"2");
                        sGv  = getGV(SOrdno,"3");
                        sDgv  = getDGV(SOrdno,"4");
                        dPocLmc  = common.toDouble(getPOC(SOrdno,"5","1"));
                        dPocLmc += common.toDouble(getPOC(SOrdno,"5","2"));
                        dPocLmc += common.toDouble(getPOC(SOrdno,"5","3"));
                        
                        dPocDch  = common.toDouble(getPOC(SOrdno,"5","4"));
                        dPocDch += common.toDouble(getPOC(SOrdno,"5","5"));
                        dPocDch += common.toDouble(getPOC(SOrdno,"5","6"));
                        
                        dDPocLmc = common.toDouble(getPOC(SOrdno,"6","1"));
                        dDPocLmc += common.toDouble(getPOC(SOrdno,"6","2"));
                        dDPocLmc += common.toDouble(getPOC(SOrdno,"6","3"));
                        
                        dDPocDch = common.toDouble(getPOC(SOrdno,"6","4"));
                        dDPocDch += common.toDouble(getPOC(SOrdno,"6","5"));
                        dDPocDch += common.toDouble(getPOC(SOrdno,"6","6"));
                        sPocOthers = getPOCOthers(SOrdno,"5");
                        sDPocOthers = getDPOCOthers(SOrdno,"6");
                        sDccombed = getDcCombed(SOrdno,"10");
                        sKarded = getKarded(SOrdno,"11");
                        sCombed = getCombed(SOrdno,"12");
                        sDepth = getDepth(SOrdno,"15");
                        sKardVar = getKardVarity(SOrdno,"11");
                        sCombVar = getCombVarity(SOrdno,"12");
                        sLotNo = getLotno(SOrdno,"");
                        sBCotton = getBleach(SOrdno,"13");
                        sWaste = getWaste(SOrdno,"14");
                        sModal = getWhiteModal(SOrdno,"20");
                        sMixed = getMixed(SOrdno,"21");
                        sOthers = getOthers(SOrdno,"19");
                        dMixTot = common.toDouble(sVsf)+common.toDouble(sDvsf)+common.toDouble(sGv)+common.toDouble(sDgv)+dPocLmc+dPocDch+dDPocLmc+dDPocDch+common.toDouble(sPocOthers)+common.toDouble(sDPocOthers)+common.toDouble(sDccombed)+common.toDouble(sKarded)+common.toDouble(sCombed)+common.toDouble(sModal)+common.toDouble(sMixed)+common.toDouble(sOthers)+common.toDouble(sWaste);
                        }
                        if(isAlreadyOrderExists(SOrdno))
                        {     
                            Ut3Details details;
                            details = (Ut3Details)theDataList.get(idl);

                            theMap          = new HashMap();
                            theMap.put("DATE", (String)OrderMap.get("ENTRYDATE"));
                            theMap.put("MACHINE", SMechine);
                            //theMap.put("PARTY", SParty);
                            //theMap.put("ORDNO", SOrdno);
                            //theMap.put("SHADE", SShade);
                            //theMap.put("QTY", SOrderQty);
                            theMap.put("CNTREADING", String.valueOf(iNoReadingCnt));
                            theMap.put("STRREADING", String.valueOf(iNoReadingStr));
                            theMap.put("CSPREADING", String.valueOf(iNoReadingCsp));
                            theMap.put("COUNT", String.valueOf(dCount));
                            theMap.put("STRENGTH", String.valueOf(dStr));
                            theMap.put("COUNTCV",String.valueOf(dCountCv));
                            theMap.put("STRENGTHCV",String.valueOf(dStrCv));
                            theMap.put("UPER", sUper);
                            theMap.put("CVM", SCvm);
                            theMap.put("THIN", SThin);
                            theMap.put("THICK", SThick);
                            theMap.put("NEPS", sNeps);                            
                            theMap.put("NEPS280", sNeps280);
                            theMap.put("TOT200", sTot200);
                            theMap.put("TOT280", sTot280);
                            theMap.put("SYS", SSys);
                            theMap.put("RKM", sRkm);
                            theMap.put("RKMCV", sRkmCv);
                            theMap.put("ELANG", sElong);
                            theMap.put("ELANGCV", sElongCv);
                            theMap.put("VSF", sVsf);
                            theMap.put("DVSF", sDvsf);
                            theMap.put("GV", sGv);
                            theMap.put("DGV", sDgv);
                            theMap.put("POCDCH", String.valueOf(dPocDch));
                            theMap.put("POCLMC", String.valueOf(dPocLmc));
                            theMap.put("DPOCDCH", String.valueOf(dDPocDch));
                            theMap.put("DPOCLMC", String.valueOf(dDPocLmc));
                            theMap.put("POCOTHERS", sPocOthers);
                            theMap.put("DPOCOTHERS", sDPocOthers);
                            theMap.put("DCCOM", sDccombed);
                            theMap.put("KARD", sKarded);
                            theMap.put("COMBED", sCombed);
                            theMap.put("DEPTH", sDepth);
                            theMap.put("KARDVAR", sKardVar);
                            theMap.put("COMBVAR", sCombVar);
                            theMap.put("LOTNO", sLotNo);
                            theMap.put("BCOTTON", sBCotton);
                            theMap.put("USABLE", sWaste);
                            theMap.put("MODAL", sModal);
                            theMap.put("MIXED", sMixed);
                            theMap.put("OTHERS", sOthers);                
                            theMap.put("MIXTOT", String.valueOf(dMixTot));
                            
                            details.setListDetails(theMap);
                        }
                        else
                        {
                            Ut3Details details = new Ut3Details();
                            details.setEntrydate((String)OrderMap.get("ENTRYDATE"));
                            details.setOrderno(SOrdno);
                            details.setShadename(SShade);
                            details.setMachine(SMechine);
                            details.setOrdweight(SOrderQty);
                            details.setPartyname(SParty);
                            details.setCountname(SCountName);
                            details.setBlendname(sBlend);
                            theMap          = new HashMap();
                            theMap.put("DATE", (String)OrderMap.get("ENTRYDATE"));
                            theMap.put("MACHINE", SMechine);
                            //theMap.put("PARTY", SParty);
                            //theMap.put("ORDNO", SOrdno);
                            //theMap.put("SHADE", SShade);
                            //theMap.put("QTY", SOrderQty);
                            theMap.put("CNTREADING", String.valueOf(iNoReadingCnt));
                            theMap.put("STRREADING", String.valueOf(iNoReadingStr));
                            theMap.put("CSPREADING", String.valueOf(iNoReadingCsp));
                            theMap.put("COUNT", String.valueOf(dCount));
                            theMap.put("STRENGTH", String.valueOf(dStr));
                            theMap.put("COUNTCV",String.valueOf(dCountCv));
                            theMap.put("STRENGTHCV",String.valueOf(dStrCv));
                            theMap.put("UPER", sUper);
                            theMap.put("CVM", SCvm);
                            theMap.put("THIN", SThin);
                            theMap.put("THICK", SThick);
                            theMap.put("NEPS", sNeps);                            
                            theMap.put("NEPS280", sNeps280);
                            theMap.put("TOT200", sTot200);
                            theMap.put("TOT280", sTot280);
                            theMap.put("SYS", SSys);
                            theMap.put("RKM", sRkm);
                            theMap.put("RKMCV", sRkmCv);
                            theMap.put("ELANG", sElong);
                            theMap.put("ELANGCV", sElongCv);
                            theMap.put("VSF", sVsf);
                            theMap.put("DVSF", sDvsf);
                            theMap.put("GV", sGv);
                            theMap.put("DGV", sDgv);
                            theMap.put("POCDCH", String.valueOf(dPocDch));
                            theMap.put("POCLMC", String.valueOf(dPocLmc));
                            theMap.put("DPOCDCH", String.valueOf(dDPocDch));
                            theMap.put("DPOCLMC", String.valueOf(dDPocLmc));
                            theMap.put("POCOTHERS", sPocOthers);
                            theMap.put("DPOCOTHERS", sDPocOthers);
                            theMap.put("DCCOM", sDccombed);
                            theMap.put("KARD", sKarded);
                            theMap.put("COMBED", sCombed);
                            theMap.put("DEPTH", sDepth);
                            theMap.put("KARDVAR", sKardVar);
                            theMap.put("COMBVAR", sCombVar);
                            theMap.put("LOTNO", sLotNo);
                            theMap.put("BCOTTON", sBCotton);
                            theMap.put("USABLE", sWaste);
                            theMap.put("MODAL", sModal);
                            theMap.put("MIXED", sMixed);
                            theMap.put("OTHERS", sOthers);                
                            theMap.put("MIXTOT", String.valueOf(dMixTot));
                            details.setListDetails(theMap);
                            theDataList.add(details);
                        }
            sVsf="";sDvsf="";sGv="";sDgv="";sPocLmc="";sDccombed="";sKarded="";sCombed="";sDepth="";
            sKardVar="";sCombVar="";sLotNo="";sBCotton="";sWaste="";sModal="";sOthers="";
            dMixTot =0;
        }
    }
    public String getMachineName(String SOrdno,String SDate){
        String sMachine="",SMachines="";
        for (int i = 0; i < CspList.size(); i++) {
            com.reports.classes.Ut3MonthWiseDetails Td = (com.reports.classes.Ut3MonthWiseDetails) CspList.get(i);
            if (SOrdno.equals(Td.getOrderno()) && SDate.equals(Td.getEntrydate())) {
                sMachine += Td.getMachinename()+",";
            }
        }
        if(sMachine.endsWith(","))
        {					
                SMachines = sMachine.substring(0, sMachine.length() - 1);
        }
        else
        {
                SMachines = sMachine;
        }
        return SMachines;
    }
    public String getPartyName(java.util.List TestUt3list,String SOrdno,String SDate){
        String sParty="";
        for (int i = 0; i < TestUt3list.size(); i++) {
            com.reports.classes.Ut3MonthWiseDetails Td = (com.reports.classes.Ut3MonthWiseDetails) TestUt3list.get(i);
            if (SOrdno.equals(Td.getOrderno()) && SDate.equals(Td.getEntrydate())) {
                sParty = Td.getPartyname();
            }
        }
        return sParty;
    }
    public String getShadeName(java.util.List TestUt3list,String SOrdno,String SDate){
        String sShade="";
        for (int i = 0; i < TestUt3list.size(); i++) {
            com.reports.classes.Ut3MonthWiseDetails Td = (com.reports.classes.Ut3MonthWiseDetails) TestUt3list.get(i);
            if (SOrdno.equals(Td.getOrderno()) && SDate.equals(Td.getEntrydate())) {
                sShade = Td.getShadename();
            }
        }
        return sShade;
    }
    public int getCntReading(String SOrdno,String SDate){
        int iCnt=0;
        for (int i = 0; i < CspList.size(); i++) {
            com.reports.classes.Ut3MonthWiseDetails Td = (com.reports.classes.Ut3MonthWiseDetails) CspList.get(i);
            if (SOrdno.equals(Td.getOrderno()) && SDate.equals(Td.getEntrydate())) {
                iCnt += common.toInt(Td.getCntreading());
            }
        }
        return iCnt;
    }
    public int getStrReading(String SOrdno,String SDate){
        int iCnt=0;
        for (int i = 0; i < CspList.size(); i++) {
            com.reports.classes.Ut3MonthWiseDetails Td = (com.reports.classes.Ut3MonthWiseDetails) CspList.get(i);
            if (SOrdno.equals(Td.getOrderno()) && SDate.equals(Td.getEntrydate())) {
                iCnt += common.toInt(Td.getStrreading());
            }
        }
        return iCnt;
    }
    public int getCspReading(String SOrdno,String SDate){
        int iCnt=0;
        for (int i = 0; i < CspList.size(); i++) {
            com.reports.classes.Ut3MonthWiseDetails Td = (com.reports.classes.Ut3MonthWiseDetails) CspList.get(i);
            if (SOrdno.equals(Td.getOrderno()) && SDate.equals(Td.getEntrydate())) {
                iCnt += common.toInt(Td.getCspreading());
            }
        }
        return iCnt;
    }
    public double getCount(String SOrdno,String SDate){
        double dCnt=0;
        int iCnt=0;
        for (int i = 0; i < CspList.size(); i++) {
            com.reports.classes.Ut3MonthWiseDetails Td = (com.reports.classes.Ut3MonthWiseDetails) CspList.get(i);
            if (SOrdno.equals(Td.getOrderno()) && SDate.equals(Td.getEntrydate())) {
                dCnt += common.toDouble(Td.getAvgcnt());
                iCnt +=1;
            }
        }
        return (dCnt/iCnt);
    }
    public double getStr(String SOrdno,String SDate){
        double dStr=0;
        int iCnt=0;
        for (int i = 0; i < CspList.size(); i++) {
            com.reports.classes.Ut3MonthWiseDetails Td = (com.reports.classes.Ut3MonthWiseDetails) CspList.get(i);
            if (SOrdno.equals(Td.getOrderno()) && SDate.equals(Td.getEntrydate())) {
                dStr += common.toDouble(Td.getAvgstr());
                iCnt +=1;
            }
        }
        return (dStr/iCnt);
    }
    public double getCntCv( String SOrdno, String SDate){
        CoEffVarient Cev = new CoEffVarient();
        ArrayList<String> arrlistcnt = new ArrayList<String>();
        String[] CntArray;
        double dCnt=0;
        for (int i = 0; i < CspList.size(); i++) {
            com.reports.classes.Ut3MonthWiseDetails Td = (com.reports.classes.Ut3MonthWiseDetails) CspList.get(i);
            if (SOrdno.equals(Td.getOrderno()) && SDate.equals(Td.getEntrydate())) {
                    double dcnt1 = common.toDouble(Td.getCnt1());
                    double dcnt2 = common.toDouble(Td.getCnt2());
                    double dcnt3 = common.toDouble(Td.getCnt3());
                    double dcnt4 = common.toDouble(Td.getCnt4());
                    double dcnt5 = common.toDouble(Td.getCnt5());
                    if (dcnt1 != 0) {
                        arrlistcnt.add(Td.getCnt1());
                    }
                    if (dcnt2 != 0) {
                        arrlistcnt.add(Td.getCnt2());
                    }
                    if (dcnt3 != 0) {
                        arrlistcnt.add(Td.getCnt3());
                    }
                    if (dcnt4 != 0) {
                        arrlistcnt.add(Td.getCnt4());
                    }
                    if (dcnt5 != 0) {
                        arrlistcnt.add(Td.getCnt5());
                    }
            }
        }
        CntArray = (String[]) arrlistcnt.toArray(new String[arrlistcnt.size()]);
        dCnt = common.toDouble(common.getRound(Cev.getCoEffVant(CntArray), 2));
        return dCnt;
    }
    public double getStrCv(String SOrdno, String SDate){
        CoEffVarient Cev = new CoEffVarient();
        ArrayList<String> arrliststr = new ArrayList<String>();
        String[] StrArray;
        double dStr=0;
        for (int i = 0; i < CspList.size(); i++) {
            com.reports.classes.Ut3MonthWiseDetails Td = (com.reports.classes.Ut3MonthWiseDetails) CspList.get(i);
            if (SOrdno.equals(Td.getOrderno()) && SDate.equals(Td.getEntrydate())) {
                    double dstr1 = common.toDouble(Td.getStrength1());
                    double dstr2 = common.toDouble(Td.getStrength2());
                    double dstr3 = common.toDouble(Td.getStrength3());
                    double dstr4 = common.toDouble(Td.getStrength4());
                    double dstr5 = common.toDouble(Td.getStrength5());
                    if (dstr1 != 0) {
                        arrliststr.add(Td.getStrength1());
                    }
                    if (dstr2 != 0) {
                        arrliststr.add(Td.getStrength2());
                    }
                    if (dstr3 != 0) {
                        arrliststr.add(Td.getStrength3());
                    }
                    if (dstr4 != 0) {
                        arrliststr.add(Td.getStrength4());
                    }
                    if (dstr5 != 0) {
                        arrliststr.add(Td.getStrength5());
                    }
            }
        }
        StrArray = (String[]) arrliststr.toArray(new String[arrliststr.size()]);
        dStr = common.toDouble(common.getRound(Cev.getCoEffVant(StrArray), 2));
        return dStr;
    }
    public String getRkmSys(String SOrdno,String SDate){
        String sSys="";
        for (int i = 0; i < rkmList.size(); i++) {
            com.reports.classes.RkmDetails Td = (com.reports.classes.RkmDetails) rkmList.get(i);
           // System.out.println("Td.getOrderno()-->"+Td.getOrderno());
            if (SOrdno.equals(Td.getOrderno()) && SDate.equals(Td.getEntrydate())) {
                sSys = Td.getSys();
            }
        }
        return sSys;
    }
    public String getRkmrkm(String SOrdno,String SDate){
        String sSys="";
        for (int i = 0; i < rkmList.size(); i++) {
            com.reports.classes.RkmDetails Td = (com.reports.classes.RkmDetails) rkmList.get(i);
           // System.out.println("Td.getOrderno()-->"+Td.getOrderno());
            if (SOrdno.equals(Td.getOrderno()) && SDate.equals(Td.getEntrydate())) {
                sSys = Td.getRkm();
            }
        }
        return sSys;
    }
    public String getRkmCv(String SOrdno,String SDate){
        String sSys="";
        for (int i = 0; i < rkmList.size(); i++) {
            com.reports.classes.RkmDetails Td = (com.reports.classes.RkmDetails) rkmList.get(i);
           // System.out.println("Td.getOrderno()-->"+Td.getOrderno());
            if (SOrdno.equals(Td.getOrderno()) && SDate.equals(Td.getEntrydate())) {
                sSys = Td.getRkmcv();
            }
        }
        return sSys;
    }
    public String getElang(String SOrdno,String SDate){
        String sSys="";
        for (int i = 0; i < rkmList.size(); i++) {
            com.reports.classes.RkmDetails Td = (com.reports.classes.RkmDetails) rkmList.get(i);
           // System.out.println("Td.getOrderno()-->"+Td.getOrderno());
            if (SOrdno.equals(Td.getOrderno()) && SDate.equals(Td.getEntrydate())) {
                sSys = Td.getElg();
            }
        }
        return sSys;
    }
    public String getElangCv(String SOrdno,String SDate){
        String sSys="";
        for (int i = 0; i < rkmList.size(); i++) {
            com.reports.classes.RkmDetails Td = (com.reports.classes.RkmDetails) rkmList.get(i);
           // System.out.println("Td.getOrderno()-->"+Td.getOrderno());
            if (SOrdno.equals(Td.getOrderno()) && SDate.equals(Td.getEntrydate())) {
                sSys = Td.getElgcv();
            }
        }
        return sSys;
    }
    public String getUpercent(String SOrdno,String SDate){
        String sSys="";
        for (int i = 0; i < Ut3List.size(); i++) {
            com.reports.classes.Ut3class Td = (com.reports.classes.Ut3class) Ut3List.get(i);
           // System.out.println("Td.getOrderno()-->"+Td.getOrderno());
            if (SOrdno.equals(Td.getOrderno()) && SDate.equals(Td.getEntrydate())) {
                sSys = Td.getUper();
            }
        }
        return sSys;
    }
    public String getCvm(String SOrdno,String SDate){
        String sSys="";
        for (int i = 0; i < Ut3List.size(); i++) {
            com.reports.classes.Ut3class Td = (com.reports.classes.Ut3class) Ut3List.get(i);
           // System.out.println("Td.getOrderno()-->"+Td.getOrderno());
            if (SOrdno.equals(Td.getOrderno()) && SDate.equals(Td.getEntrydate())) {
                sSys = Td.getCvm();
            }
        }
        return sSys;
    }
    public String getThin(String SOrdno,String SDate){
        String sSys="";
        for (int i = 0; i < Ut3List.size(); i++) {
            com.reports.classes.Ut3class Td = (com.reports.classes.Ut3class) Ut3List.get(i);
           // System.out.println("Td.getOrderno()-->"+Td.getOrderno());
            if (SOrdno.equals(Td.getOrderno()) && SDate.equals(Td.getEntrydate())) {
                sSys = Td.getThin();
            }
        }
        return sSys;
    }
    public String getThick(String SOrdno,String SDate){
        String sSys="";
        for (int i = 0; i < Ut3List.size(); i++) {
            com.reports.classes.Ut3class Td = (com.reports.classes.Ut3class) Ut3List.get(i);
           // System.out.println("Td.getOrderno()-->"+Td.getOrderno());
            if (SOrdno.equals(Td.getOrderno()) && SDate.equals(Td.getEntrydate())) {
                sSys = Td.getThick();
            }
        }
        return sSys;
    }
    public String getNeps(String SOrdno,String SDate){
        String sSys="";
        for (int i = 0; i < Ut3List.size(); i++) {
            com.reports.classes.Ut3class Td = (com.reports.classes.Ut3class) Ut3List.get(i);
           // System.out.println("Td.getOrderno()-->"+Td.getOrderno());
            if (SOrdno.equals(Td.getOrderno()) && SDate.equals(Td.getEntrydate())) {
                sSys = Td.getNep200();
            }
        }
        return sSys;
    }
    public String getNeps280(String SOrdno,String SDate){
        String sSys="";
        for (int i = 0; i < Ut3List.size(); i++) {
            com.reports.classes.Ut3class Td = (com.reports.classes.Ut3class) Ut3List.get(i);
           // System.out.println("Td.getOrderno()-->"+Td.getOrderno());
            if (SOrdno.equals(Td.getOrderno()) && SDate.equals(Td.getEntrydate())) {
                sSys = Td.getNep280();
            }
        }
        return sSys;
    }
    public String getTot200(String SOrdno,String SDate){
        String sSys="";
        for (int i = 0; i < Ut3List.size(); i++) {
            com.reports.classes.Ut3class Td = (com.reports.classes.Ut3class) Ut3List.get(i);
           // System.out.println("Td.getOrderno()-->"+Td.getOrderno());
            if (SOrdno.equals(Td.getOrderno()) && SDate.equals(Td.getEntrydate())) {
                sSys = Td.getTot200();
            }
        }
        return sSys;
    }
    public String getTot280(String SOrdno,String SDate){
        String sSys="";
        for (int i = 0; i < Ut3List.size(); i++) {
            com.reports.classes.Ut3class Td = (com.reports.classes.Ut3class) Ut3List.get(i);
           // System.out.println("Td.getOrderno()-->"+Td.getOrderno());
            if (SOrdno.equals(Td.getOrderno()) && SDate.equals(Td.getEntrydate())) {
                sSys = Td.getTot280();
            }
        }
        return sSys;
    }
    public String getVsf(String SOrdno,String SType) {
        HashMap OrderMap = null;
        String sOno = "",sFtype="";
        double dvsf=0;
         for (int i = 0; i < MixingList.size(); i++) {
            OrderMap = (HashMap) MixingList.get(i);
            sOno = (String) OrderMap.get("RORDERNO");
            sFtype = (String) OrderMap.get("TYPE_CODE");            
            if (SOrdno.equals(sOno) && sFtype.equals(SType)) {
                dvsf += common.toDouble((String) OrderMap.get("TYPE_PERCENTAGE"));
            }
        }
        return common.getRound(dvsf,2);
    }
    public String getDVsf(String SOrdno,String SType) {
        HashMap OrderMap = null;
        String sOno = "",sFtype="";
        double dvsf=0;
         for (int i = 0; i < MixingList.size(); i++) {
            OrderMap = (HashMap) MixingList.get(i);
            sOno = (String) OrderMap.get("RORDERNO");
            sFtype = (String) OrderMap.get("TYPE_CODE");            
            if (SOrdno.equals(sOno) && sFtype.equals(SType)) {
                dvsf += common.toDouble((String) OrderMap.get("TYPE_PERCENTAGE"));
            }
        }
        return common.getRound(dvsf,2);
    }
    public String getGV(String SOrdno,String SType) {
        
        HashMap OrderMap = null;
        String sOno = "",sFtype="";
        double dvsf=0;
         for (int i = 0; i < MixingList.size(); i++) {
            OrderMap = (HashMap) MixingList.get(i);
            sOno = (String) OrderMap.get("RORDERNO");
            sFtype = (String) OrderMap.get("TYPE_CODE");            
            if (SOrdno.equals(sOno) && sFtype.equals(SType)) {
                dvsf += common.toDouble((String) OrderMap.get("TYPE_PERCENTAGE"));
            }
        }
        // System.out.println("Ord : "+SOrdno+" Value : "+dvsf);
        return common.getRound(dvsf,2);
    }
    public String getDGV(String SOrdno,String SType) {
        HashMap OrderMap = null;
        String sOno = "",sFtype="";
        double dvsf=0;
         for (int i = 0; i < MixingList.size(); i++) {
            OrderMap = (HashMap) MixingList.get(i);
            sOno = (String) OrderMap.get("RORDERNO");
            sFtype = (String) OrderMap.get("TYPE_CODE");            
            if (SOrdno.equals(sOno) && sFtype.equals(SType)) {
                dvsf += common.toDouble((String) OrderMap.get("TYPE_PERCENTAGE"));
            }
        }
        return common.getRound(dvsf,2);
    }
    public String getPOC(String SOrdno,String SType,String Group) {
        HashMap OrderMap = null;
        String sOno = "",sFtype="",sFGroup="";
        double dvsf=0;
        if(MixingLmc.size()>0){
            for (int i = 0; i < MixingLmc.size(); i++) {
               OrderMap = (HashMap) MixingLmc.get(i);
               sOno = (String) OrderMap.get("RORDERNO");
               sFtype = (String) OrderMap.get("TYPE_CODE");
               sFGroup = (String) OrderMap.get("GROUPCODE");
               if (SOrdno.equals(sOno) && sFtype.equals(SType) && sFGroup.equals(Group)) {
                   dvsf += common.toDouble((String) OrderMap.get("TYPE_PERCENTAGE"));
               }
           }
        }
        return common.getRound(dvsf,2);
    }
    public String getPOCOthers(String SOrdno,String SType) {
        HashMap OrderMap = null;
        String sOno = "",sFtype="",sFGroup="";
        double dvsf=0;
        if(MixingOthers.size()>0){
            for (int i = 0; i < MixingOthers.size(); i++) {
               OrderMap = (HashMap) MixingOthers.get(i);
               sOno = (String) OrderMap.get("RORDERNO");
               sFtype = (String) OrderMap.get("TYPE_CODE");
               sFGroup = (String) OrderMap.get("GROUPCODE");
               if (SOrdno.equals(sOno) && sFtype.equals(SType)) {
                   dvsf += common.toDouble((String) OrderMap.get("TYPE_PERCENTAGE"));
               }
           }
        }        
        return common.getRound(dvsf,2);
    }
    public String getDPOCOthers(String SOrdno,String SType) {
        HashMap OrderMap = null;
        String sOno = "",sFtype="",sFGroup="";
        double dvsf=0;
        if(MixingOthers.size()>0){
            for (int i = 0; i < MixingOthers.size(); i++) {
               OrderMap = (HashMap) MixingOthers.get(i);
               sOno = (String) OrderMap.get("RORDERNO");
               sFtype = (String) OrderMap.get("TYPE_CODE");
               sFGroup = (String) OrderMap.get("GROUPCODE");
               if (SOrdno.equals(sOno) && sFtype.equals(SType)) {
                   dvsf += common.toDouble((String) OrderMap.get("TYPE_PERCENTAGE"));
               }
           }
        }        
        return common.getRound(dvsf,2);
    }
    public String getDcCombed(String SOrdno,String SType) {
        
        HashMap OrderMap = null;
        String sOno = "",sFtype="";
        double dvsf=0;
         for (int i = 0; i < MixingList.size(); i++) {
            OrderMap = (HashMap) MixingList.get(i);
            sOno = (String) OrderMap.get("RORDERNO");
            sFtype = (String) OrderMap.get("TYPE_CODE");            
            if (SOrdno.equals(sOno) && sFtype.equals(SType)) {                
                dvsf += common.toDouble((String) OrderMap.get("TYPE_PERCENTAGE"));
            }
        }
        return common.getRound(dvsf,2);
    }
    public String getKarded(String SOrdno,String SType) {
        HashMap OrderMap = null;
        String sOno = "",sFtype="";
        double dvsf=0;
         for (int i = 0; i < MixingList.size(); i++) {
            OrderMap = (HashMap) MixingList.get(i);
            sOno = (String) OrderMap.get("RORDERNO");
            sFtype = (String) OrderMap.get("TYPE_CODE");            
            if (SOrdno.equals(sOno) && sFtype.equals("11")) {
                dvsf += common.toDouble((String) OrderMap.get("TYPE_PERCENTAGE"));
            }
        }
        return common.getRound(dvsf,2);
    }
    public String getCombed(String SOrdno,String SType) {
        HashMap OrderMap = null;
        String sOno = "",sFtype="";
        double dvsf=0;
         for (int i = 0; i < MixingList.size(); i++) {
            OrderMap = (HashMap) MixingList.get(i);
            sOno = (String) OrderMap.get("RORDERNO");
            sFtype = (String) OrderMap.get("TYPE_CODE");            
            if (SOrdno.equals(sOno) && sFtype.equals("12")) {
                dvsf += common.toDouble((String) OrderMap.get("TYPE_PERCENTAGE"));
            }
        }
        return common.getRound(dvsf,2);
    }
    public String getWaste(String SOrdno,String SType) {
        HashMap OrderMap = null;
        String sOno = "",sFtype="";
        double dvsf=0;
         for (int i = 0; i < MixingList.size(); i++) {
            OrderMap = (HashMap) MixingList.get(i);
            sOno = (String) OrderMap.get("RORDERNO");
            sFtype = (String) OrderMap.get("TYPE_CODE");            
            if (SOrdno.equals(sOno) && sFtype.equals(SType)) {
                dvsf += common.toDouble((String) OrderMap.get("TYPE_PERCENTAGE"));
            }
        }
        return common.getRound(dvsf,2);
    }
    public String getDepth(String SOrdno,String SType) {
        HashMap OrderMap = null;
        String sOno = "",sFtype="";
        double dvsf=0;
         for (int i = 0; i < MixingDepthList.size(); i++) {
            OrderMap = (HashMap) MixingDepthList.get(i);
            sOno = (String) OrderMap.get("ORDERNO");            
            if (SOrdno.equals(sOno)) {
                dvsf += common.toDouble((String) OrderMap.get("DEPTH"));
            }
        }
        return common.getRound(dvsf,2);
    }
    public String getKardVarity(String SOrdno,String SType) {
        HashMap OrderMap = null;
        String sOno = "",sFtype="";
        String sVarity="";
         for (int i = 0; i < MixingList.size(); i++) {
            OrderMap = (HashMap) MixingList.get(i);
            sOno = (String) OrderMap.get("RORDERNO");
            sFtype = (String) OrderMap.get("TYPE_CODE");            
            if (SOrdno.equals(sOno) && sFtype.equals(SType)) {
                sVarity += (String) OrderMap.get("VARIETY")+",";
            }
        }
         if(sVarity.length()>0){sVarity =  sVarity.substring(0, sVarity.length()-1);}
        return sVarity;
    }
    public String getCombVarity(String SOrdno,String SType) {
        HashMap OrderMap = null;
        String sOno = "",sFtype="";
        String sVarity="";
         for (int i = 0; i < MixingList.size(); i++) {
            OrderMap = (HashMap) MixingList.get(i);
            sOno = (String) OrderMap.get("RORDERNO");
            sFtype = (String) OrderMap.get("TYPE_CODE");            
            if (SOrdno.equals(sOno) && sFtype.equals(SType)) {
                sVarity += (String) OrderMap.get("VARIETY")+",";
            }
        }
         if(sVarity.length()>0){sVarity =  sVarity.substring(0, sVarity.length()-1);}
        return sVarity;
    }
    public String getLotno(String SOrdno,String SType) {
        HashMap OrderMap = null;
        String sOno = "",sFtype="";
        String sVarity="";
         for (int i = 0; i < MixingList.size(); i++) {
            OrderMap = (HashMap) MixingList.get(i);
            sOno = (String) OrderMap.get("RORDERNO");
            sFtype = (String) OrderMap.get("TYPE_CODE");            
            if (SOrdno.equals(sOno) && !sFtype.equals("1") && !sFtype.equals("2") && !sFtype.equals("3") && !sFtype.equals("4") && !sFtype.equals("5") && !sFtype.equals("10") && !sFtype.equals("13") && !sFtype.equals("19") && !sFtype.equals("14") && !sFtype.equals("20") && !sFtype.equals("21")) {
                sVarity += (String) OrderMap.get("MIXLOTNO")+",";
            }
        }
         if(sVarity.length()>0){sVarity =  sVarity.substring(0, sVarity.length()-1);}        
         return  sVarity;
    }
    public String getBleach(String SOrdno,String SType) {
        HashMap OrderMap = null;
        String sOno = "",sFtype="";
        double dvsf=0;
         for (int i = 0; i < MixingList.size(); i++) {
            OrderMap = (HashMap) MixingList.get(i);
            sOno = (String) OrderMap.get("RORDERNO");
            sFtype = (String) OrderMap.get("TYPE_CODE");            
            if (SOrdno.equals(sOno) && sFtype.equals(SType)) {
                dvsf += common.toDouble((String) OrderMap.get("TYPE_PERCENTAGE"));
            }
        }
        return common.getRound(dvsf,2);
    }
    public String getWhiteModal(String SOrdno,String SType) {
        HashMap OrderMap = null;
        String sOno = "",sFtype="";
        double dvsf=0;
         for (int i = 0; i < MixingList.size(); i++) {
            OrderMap = (HashMap) MixingList.get(i);
            sOno = (String) OrderMap.get("RORDERNO");
            sFtype = (String) OrderMap.get("TYPE_CODE");            
            if (SOrdno.equals(sOno) && sFtype.equals(SType)) {
                dvsf += common.toDouble((String) OrderMap.get("TYPE_PERCENTAGE"));
            }
        }
        return common.getRound(dvsf,2);
    }
    public String getMixed(String SOrdno,String SType) {
        HashMap OrderMap = null;
        String sOno = "",sFtype="";
        double dvsf=0;
         for (int i = 0; i < MixingList.size(); i++) {
            OrderMap = (HashMap) MixingList.get(i);
            sOno = (String) OrderMap.get("RORDERNO");
            sFtype = (String) OrderMap.get("TYPE_CODE");            
            if (SOrdno.equals(sOno) && sFtype.equals(SType)) {
                dvsf += common.toDouble((String) OrderMap.get("TYPE_PERCENTAGE"));
            }
        }
        return common.getRound(dvsf,2);
    }
    public String getOthers(String SOrdno,String SType) {
        HashMap OrderMap = null;
        String sOno = "",sFtype="";
        double dvsf=0;
         for (int i = 0; i < MixingList.size(); i++) {
            OrderMap = (HashMap) MixingList.get(i);
            sOno = (String) OrderMap.get("RORDERNO");
            sFtype = (String) OrderMap.get("TYPE_CODE");            
            if (SOrdno.equals(sOno) && sFtype.equals(SType)) {
                dvsf += common.toDouble((String) OrderMap.get("TYPE_PERCENTAGE"));
            }
        }
        return common.getRound(dvsf,2);
    }

    
    public java.util.List getDataList(){
       return this.theDateListAll;
    }
    public java.util.List getUt3DataDetails(String SDate,String SOrderNo,String sUnitCode){
        java.util.List theList = new java.util.ArrayList();
        PreparedStatement pst = null;
        ResultSet rst = null;
        StringBuilder sb = new StringBuilder();
        sb.append(" select entrydate,test_no,mach_st_name,orderno,dbcode,countname,yarnm.YSHNM,partyname, ");
        sb.append(" sumcount/decode(countavgfact,0,1,countavgfact) as avgcnt, ");
        sb.append(" cnt1,cnt2,cnt3,cnt4,str1,str2,str3,str4, ");
        sb.append(" sumstrength/decode(stravgfact,0,1,stravgfact) as avgstr,csp/decode(cspavgfact,0,1,cspavgfact) as avgcsp, ");
        sb.append(" countavgfact,stravgfact,cspavgfact,t.Weight,cnt5,str5 ");
        sb.append(" from ");
        sb.append(" ( ");
        sb.append(" select to_char(indate,'yyyymmdd') as entrydate,test_no,mach_st_name,test_yarn_reg_details.orderno,dbcode, ");
        sb.append(" yarncount.countname,partymaster.partyname, ");
        sb.append(" nvl(cnt1,0) as cnt1,nvl(cnt2,0) as cnt2,nvl(cnt3,0) as cnt3,nvl(cnt4,0) as cnt4, ");
        sb.append(" nvl(str1,0) as str1,nvl(str2,0) as str2,nvl(str3,0) as str3,nvl(str4,0) as str4, ");
        sb.append(" nvl(cnt1,0)+nvl(cnt2,0)+nvl(cnt3,0)+nvl(cnt4,0)+nvl(cnt5,0) as sumcount, ");
        sb.append(" nvl(str1,0)+nvl(str2,0)+nvl(str3,0)+nvl(str4,0)+nvl(str5,0) as sumstrength, ");
        sb.append(" decode(nvl(cnt1,0),0,0,cnt1)*decode(nvl(str1,0),0,0,str1)+decode(nvl(cnt2,0),0,0,cnt2)*decode(nvl(str2,0),0,0,str2)+ ");
        sb.append(" decode(nvl(str3,0),0,0,cnt3)*decode(nvl(cnt3,0),0,0,str3)+decode(nvl(cnt4,0),0,0,cnt4)*decode(nvl(str4,0),0,0,str4)+ ");
        sb.append(" decode(nvl(cnt5,0),0,0,cnt5)*decode(nvl(str5,0),0,0,str5) as csp, ");
        sb.append(" decode(nvl(cnt1,0),0,0,1)+decode(nvl(cnt2,0),0,0,1)+decode(nvl(cnt3,0),0,0,1)+decode(nvl(cnt4,0),0,0,1)+decode(nvl(cnt5,0),0,0,1) as countavgfact, ");
        sb.append(" decode(nvl(str1,0),0,0,1)+decode(nvl(str2,0),0,0,1)+decode(nvl(str3,0),0,0,1)+decode(nvl(str4,0),0,0,1)+decode(nvl(str5,0),0,0,1) as stravgfact, ");
        sb.append(" decode(decode(nvl(cnt1,0),0,0,cnt1)*decode(nvl(str1,0),0,0,str1),0,0,1)+decode(decode(nvl(cnt2,0),0,0,cnt2)*decode(nvl(str2,0),0,0,str2),0,0,1)+ ");
        sb.append(" decode(decode(nvl(str3,0),0,0,cnt3)*decode(nvl(cnt3,0),0,0,str3),0,0,1)+decode(decode(nvl(cnt4,0),0,0,cnt4)*decode(nvl(str4,0),0,0,str4),0,0,1)+ ");
        sb.append(" decode(decode(nvl(cnt5,0),0,0,cnt5)*decode(nvl(str5,0),0,0,str5),0,0,1) as cspavgfact,RegularOrder.Weight,nvl(cnt5,0) as cnt5,nvl(str5,0) as str5 ");
        sb.append(" from test_yarn_reg_details ");
        sb.append(" inner join regularorder on regularorder.rorderno = test_yarn_reg_details.orderno and Regularorder.UnitCode="+sUnitCode+"");
        sb.append(" inner join partymaster on partymaster.partycode = regularorder.partycode ");
        sb.append(" inner join yarncount on yarncount.countcode=regularorder.countcode ");
        //sb.append(" where to_char(INDATE,'yyyymmdd') ="+SDate+" ");
        //sb.append(" Where orderno = '"+SOrderNo+"'");
        sb.append(" Where orderno in("+SOrderNo+")");
        sb.append(" ) t ");
        sb.append(" INNER JOIN SAMPLETOREGULAR ON SAMPLETOREGULAR.REGULARORDERNO = t.ORDERNO AND SAMPLETOREGULAR.CORRECTIONMIXING=0 ");
        sb.append(" LEFT JOIN YARNM ON SAMPLETOREGULAR.YARNSHADECODE = YARNM.YSHCD ");
        sb.append(" order by 4,1,3 ");
        try{
                if(theProcessConnection==null){
                    JDBCProcessConnection jdbc = JDBCProcessConnection.getJDBCConnection();
                    theProcessConnection = jdbc.getConnection();
                }
                pst = theProcessConnection.prepareStatement(sb.toString());
                rst = pst.executeQuery();
                ResultSetMetaData rsmd    = rst.getMetaData();
                while(rst.next())
                {
                    HashMap themap = new HashMap();
                    for(int i=0;i<rsmd.getColumnCount();i++)
                    {
                        themap.put(rsmd.getColumnName(i+1),rst.getString(i+1));
                    }
                    theList.add(themap);
                    rst.close();
                    pst.close();
                }
            }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return theList;
    }
    
    private boolean isAlreadyOrderExists(String sord){

        boolean bs = false;
        
        for (int i = 0; i < theDataList.size(); i++) {

            Ut3Details details = (Ut3Details)theDataList.get(i);
            if(sord.equals(details.getOrderno())){
                idl = i;
                return true;
            }
        }
        return bs;
    }
    private boolean isExists(String SOrderNo){      
        boolean bs = false;
        //System.out.println("SOrderNo-->"+SOrderNo);
        if (!OrderList.contains(SOrderNo)) {
            OrderList.add(SOrderNo);
            bs = true;
        }
          //System.out.println("Size-->"+OrderList.size());
          //System.out.println("bs-->"+bs);
        return bs;
    }
    public void getMixingData(String sFromDate, String sToDate,String sOrderNo) {
        PreparedStatement pst = null;
        ResultSet rst = null;
        StringBuilder sb = new StringBuilder();

        sb.append(" SELECT RORDERNO,TYPE_CODE,TYPE_PERCENTAGE,Order_blend_details.VARIETY,Order_blend_details.MIXLOTNO FROM REGULARORDER ");
        sb.append(" Inner Join (  ");
        sb.append(" SELECT DISTINCT ORDERNO,ORDERID FROM(  ");
        sb.append(" SELECT ut3_entry.ORDERNO,RegularOrder.Orderid From ut3_entry  ");
        sb.append(" Inner join RegularOrder on Regularorder.Rorderno=ut3_entry.Orderno  ");
        sb.append(" Inner join PartyMaster on PartyMaster.Partycode= RegularOrder.PartyCode "); 
        sb.append(" Inner Join Rmixir on Rmixir.ORDNO = RegularOrder.ROrderNo  ");
        sb.append(" Inner Join yarnm on yarnm.YSHCD = Rmixir.YARNSHADECODE  ");
        sb.append(" Inner Join YarnCount on yarnCount.CountCode=RegularOrder.CountCode  ");
        //sb.append(" WHERE ut3_entry.ENTRYDATE >= "+sFromDate+" AND ut3_entry.ENTRYDATE <= "+sToDate+"");
        sb.append(" Where Regularorder.UnitCode in(10,12)  ");
        if(!sOrderNo.equals("")){
                    //sb.append(" AND ORDERNO = '"+sOrderNo+"' ");
            sb.append(" AND ORDERNO In("+sOrderNo+") ");
        }
        sb.append(" Union All  ");
        sb.append(" SELECT rkm_entry.ORDERNO,RegularOrder.Orderid From rkm_entry  ");
        sb.append(" Inner join RegularOrder on Regularorder.Rorderno=rkm_entry.Orderno  ");
        sb.append(" Inner join PartyMaster on PartyMaster.Partycode= RegularOrder.PartyCode "); 
        sb.append(" Inner Join Rmixir on Rmixir.ORDNO = RegularOrder.ROrderNo  ");
        sb.append(" Inner Join yarnm on yarnm.YSHCD = Rmixir.YARNSHADECODE  ");
        sb.append(" Inner Join YarnCount on yarnCount.CountCode=RegularOrder.CountCode  ");
        //sb.append(" WHERE rkm_entry.ENTRYDATE >= "+sFromDate+" AND rkm_entry.ENTRYDATE <= "+sToDate+"");
        sb.append(" Where Regularorder.UnitCode in(10,12)  ");
        if(!sOrderNo.equals("")){
                    //sb.append(" AND ORDERNO = '"+sOrderNo+"' ");
                sb.append(" AND ORDERNO in("+sOrderNo+") ");
        }
        sb.append(" Union All  ");
        sb.append(" select orderno,Orderid ");
        sb.append(" from (  ");
        sb.append(" select test_yarn_reg_details.orderno,RegularOrder.Orderid from test_yarn_reg_details  ");
        sb.append(" inner join regularorder on regularorder.rorderno = test_yarn_reg_details.orderno and (Regularorder.UnitCode=10 or Regularorder.UnitCode=12)  ");
        sb.append(" inner join partymaster on partymaster.partycode = regularorder.partycode  ");
        sb.append(" inner join yarncount on yarncount.countcode=regularorder.countcode  ");
        //sb.append(" where to_char(INDATE,'yyyymmdd') >="+sFromDate+" and to_char(INDATE,'yyyymmdd') <="+sToDate+"");
        if(!sOrderNo.equals("")){
                    //sb.append(" Where ORDERNO = '"+sOrderNo+"' ");
                sb.append(" Where ORDERNO in("+sOrderNo+") ");
        }
        sb.append(" ) t  ");
        sb.append(" INNER JOIN SAMPLETOREGULAR ON SAMPLETOREGULAR.REGULARORDERNO = t.ORDERNO AND SAMPLETOREGULAR.CORRECTIONMIXING=0  ");
        sb.append(" LEFT JOIN YARNM ON SAMPLETOREGULAR.YARNSHADECODE = YARNM.YSHCD  ");
        sb.append(" ))  tt on tt.Orderno=RegularOrder.ROrderNo  ");
        if (!sOrderNo.equals("")) {
        //sb.append(" Inner Join Order_blend_details on Order_blend_details.Order_id=tt.Orderid And tt.Orderno='"+sOrderNo+"'");
            sb.append(" Inner Join Order_blend_details on Order_blend_details.Order_id=tt.Orderid And tt.Orderno in("+sOrderNo+")");
        }
        else{
            sb.append(" Inner Join Order_blend_details on Order_blend_details.Order_id=tt.Orderid ");
        }

        try {
            if (theProcessConnection == null) {
                JDBCProcessConnection jdbc = JDBCProcessConnection.getJDBCConnection();
                theProcessConnection = jdbc.getConnection();
            }
            pst = theProcessConnection.prepareStatement(sb.toString());
            rst = pst.executeQuery();
            ResultSetMetaData rsmd = rst.getMetaData();
            while (rst.next()) {
                HashMap themap = new HashMap();
                for (int i = 0; i < rsmd.getColumnCount(); i++) {
                    themap.put(rsmd.getColumnName(i + 1), rst.getString(i + 1));
                }
                MixingList.add(themap);
            }
            rst.close();
            pst.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }       
    }
 public void getMixingDepth(String sFromDate, String sToDate,String sOrderNo) {
        PreparedStatement pst = null;
        ResultSet rst = null;
        StringBuilder sb = new StringBuilder();

        sb.append(" SELECT DISTINCT ORDERNO,DEPTH FROM REGULARORDER ");
        sb.append(" Inner Join (   ");
        sb.append(" SELECT DISTINCT ORDERNO,ORDERID,Depth FROM( ");
        sb.append(" SELECT ut3_entry.ORDERNO,RegularOrder.Orderid,Rmixir.Depth From ut3_entry  ");
        sb.append(" Inner join RegularOrder on Regularorder.Rorderno=ut3_entry.Orderno  ");
        sb.append(" Inner join PartyMaster on PartyMaster.Partycode= RegularOrder.PartyCode ");  
        sb.append(" Inner Join Rmixir on Rmixir.ORDNO = RegularOrder.ROrderNo ");
        sb.append(" Inner Join yarnm on yarnm.YSHCD = Rmixir.YARNSHADECODE "); 
        sb.append(" Inner Join YarnCount on yarnCount.CountCode=RegularOrder.CountCode "); 
        //sb.append(" WHERE ut3_entry.ENTRYDATE >= "+sFromDate+" AND ut3_entry.ENTRYDATE <= "+sToDate+" ");
        sb.append(" where Regularorder.UnitCode in(10,12) "); 
        if(!sOrderNo.equals("")){
                    //sb.append(" AND ORDERNO = '"+sOrderNo+"' ");
            sb.append(" AND ORDERNO in("+sOrderNo+") ");
        }
        sb.append(" Union All "); 
        sb.append(" SELECT rkm_entry.ORDERNO,RegularOrder.Orderid,Rmixir.Depth From rkm_entry "); 
        sb.append(" Inner join RegularOrder on Regularorder.Rorderno=rkm_entry.Orderno "); 
        sb.append(" Inner join PartyMaster on PartyMaster.Partycode= RegularOrder.PartyCode "); 
        sb.append(" Inner Join Rmixir on Rmixir.ORDNO = RegularOrder.ROrderNo "); 
        sb.append(" Inner Join yarnm on yarnm.YSHCD = Rmixir.YARNSHADECODE "); 
        sb.append(" Inner Join YarnCount on yarnCount.CountCode=RegularOrder.CountCode "); 
        //sb.append(" WHERE rkm_entry.ENTRYDATE >= "+sFromDate+" AND rkm_entry.ENTRYDATE <= "+sToDate+" ");
        sb.append(" Where Regularorder.UnitCode in(10,12) "); 
        if(!sOrderNo.equals("")){
                    //sb.append(" AND ORDERNO = '"+sOrderNo+"' ");
            sb.append(" AND ORDERNO in("+sOrderNo+") ");
        }
        sb.append(" Union All "); 
        sb.append(" select orderno,Orderid,Depth ");
        sb.append(" from ( "); 
        sb.append(" select test_yarn_reg_details.orderno,RegularOrder.Orderid,Rmixir.Depth from test_yarn_reg_details "); 
        sb.append(" inner join regularorder on regularorder.rorderno = test_yarn_reg_details.orderno and (Regularorder.UnitCode=10 or Regularorder.UnitCode=12) "); 
        sb.append(" inner join partymaster on partymaster.partycode = regularorder.partycode "); 
        sb.append(" inner join yarncount on yarncount.countcode=regularorder.countcode ");
        sb.append(" Inner Join Rmixir on Rmixir.ORDNO = RegularOrder.ROrderNo ");
        //sb.append(" where to_char(INDATE,'yyyymmdd') >="+sFromDate+" and to_char(INDATE,'yyyymmdd') <="+sToDate+" ");
        if(!sOrderNo.equals("")){
                    //sb.append(" WHERE ORDERNO = '"+sOrderNo+"' ");
            sb.append(" WHERE ORDERNO in("+sOrderNo+") ");
        }
        sb.append(" ) t ");
        sb.append(" INNER JOIN SAMPLETOREGULAR ON SAMPLETOREGULAR.REGULARORDERNO = t.ORDERNO AND SAMPLETOREGULAR.CORRECTIONMIXING=0 "); 
        sb.append(" LEFT JOIN YARNM ON SAMPLETOREGULAR.YARNSHADECODE = YARNM.YSHCD "); 
        sb.append(" ))  tt on tt.Orderno=RegularOrder.ROrderNo "); 

        if (!sOrderNo.equals("")) {
        //sb.append(" Inner Join Order_blend_details on Order_blend_details.Order_id=tt.Orderid And tt.Orderno='"+sOrderNo+"'");
            sb.append(" Inner Join Order_blend_details on Order_blend_details.Order_id=tt.Orderid And tt.Orderno in("+sOrderNo+")");
        }
        else{
            sb.append(" Inner Join Order_blend_details on Order_blend_details.Order_id=tt.Orderid ");
        }

        try {
            if (theProcessConnection == null) {
                JDBCProcessConnection jdbc = JDBCProcessConnection.getJDBCConnection();
                theProcessConnection = jdbc.getConnection();
            }
            pst = theProcessConnection.prepareStatement(sb.toString());
            rst = pst.executeQuery();
            ResultSetMetaData rsmd = rst.getMetaData();
            while (rst.next()) {
                HashMap themap = new HashMap();
                for (int i = 0; i < rsmd.getColumnCount(); i++) {
                    themap.put(rsmd.getColumnName(i + 1), rst.getString(i + 1));
                }
                MixingDepthList.add(themap);
            }
            rst.close();
            pst.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }       
    }
 
 public void getLowMicPoc(String sFromDate, String sToDate,String sOrderNo) {
        PreparedStatement pst = null;
        ResultSet rst = null;
        StringBuilder sb = new StringBuilder();

        sb.append(" SELECT RORDERNO,GROUPCODE,TYPE_CODE,GROUPNAME,TYPE_PERCENTAGE FROM REGULARORDER ");
        sb.append(" Inner Join ( ");
        sb.append(" SELECT DISTINCT ORDERNO,ORDERID FROM( ");
        sb.append(" SELECT ut3_entry.ORDERNO,RegularOrder.Orderid From ut3_entry ");
        sb.append(" Inner join RegularOrder on Regularorder.Rorderno=ut3_entry.Orderno ");
        sb.append(" Inner join PartyMaster on PartyMaster.Partycode= RegularOrder.PartyCode ");
        sb.append(" Inner Join Rmixir on Rmixir.ORDNO = RegularOrder.ROrderNo ");
        sb.append(" Inner Join yarnm on yarnm.YSHCD = Rmixir.YARNSHADECODE ");
        sb.append(" Inner Join YarnCount on yarnCount.CountCode=RegularOrder.CountCode ");
        //sb.append(" WHERE ut3_entry.ENTRYDATE >= "+sFromDate+" AND ut3_entry.ENTRYDATE <= "+sToDate+" ");
        sb.append(" Where Regularorder.UnitCode in(10,12) ");
        if(!sOrderNo.equals("")){
                    //sb.append(" AND ORDERNO = '"+sOrderNo+"' ");
            sb.append(" AND ORDERNO in("+sOrderNo+") ");
        }
        sb.append(" Union All ");
        sb.append(" SELECT rkm_entry.ORDERNO,RegularOrder.Orderid From rkm_entry ");
        sb.append(" Inner join RegularOrder on Regularorder.Rorderno=rkm_entry.Orderno ");
        sb.append(" Inner join PartyMaster on PartyMaster.Partycode= RegularOrder.PartyCode ");
        sb.append(" Inner Join Rmixir on Rmixir.ORDNO = RegularOrder.ROrderNo ");
        sb.append(" Inner Join yarnm on yarnm.YSHCD = Rmixir.YARNSHADECODE ");
        sb.append(" Inner Join YarnCount on yarnCount.CountCode=RegularOrder.CountCode ");
        //sb.append(" WHERE rkm_entry.ENTRYDATE >= "+sFromDate+" AND rkm_entry.ENTRYDATE <= "+sToDate+" ");
        sb.append(" where Regularorder.UnitCode in(10,12) ");
        if(!sOrderNo.equals("")){
                    //sb.append(" AND ORDERNO = '"+sOrderNo+"' ");
            sb.append(" AND ORDERNO in("+sOrderNo+") ");
        }
        sb.append(" Union All ");
        sb.append(" select orderno,Orderid ");
        sb.append(" from ( ");
        sb.append(" select test_yarn_reg_details.orderno,RegularOrder.Orderid from test_yarn_reg_details ");
        sb.append(" inner join regularorder on regularorder.rorderno = test_yarn_reg_details.orderno and (Regularorder.UnitCode=10 or Regularorder.UnitCode=12) ");
        sb.append(" inner join partymaster on partymaster.partycode = regularorder.partycode ");
        sb.append(" inner join yarncount on yarncount.countcode=regularorder.countcode ");
        //sb.append(" where to_char(INDATE,'yyyymmdd') >="+sFromDate+" and to_char(INDATE,'yyyymmdd') <="+sToDate+" ");
        if(!sOrderNo.equals("")){
                    //sb.append(" Where ORDERNO = '"+sOrderNo+"' ");
            sb.append(" Where ORDERNO in("+sOrderNo+") ");
        }
        sb.append(" ) t ");
        sb.append(" INNER JOIN SAMPLETOREGULAR ON SAMPLETOREGULAR.REGULARORDERNO = t.ORDERNO AND SAMPLETOREGULAR.CORRECTIONMIXING=0 ");
        sb.append(" LEFT JOIN YARNM ON SAMPLETOREGULAR.YARNSHADECODE = YARNM.YSHCD ");
        sb.append(" ))  tt on tt.Orderno=RegularOrder.ROrderNo ");
        if (!sOrderNo.equals("")) {
        //sb.append(" Inner Join Order_blend_details on Order_blend_details.Order_id=tt.Orderid And tt.Orderno='"+sOrderNo+"' and Order_blend_details.Type_Code in (5,6) ");
            sb.append(" Inner Join Order_blend_details on Order_blend_details.Order_id=tt.Orderid And tt.Orderno in("+sOrderNo+") and Order_blend_details.Type_Code in (5,6) ");
        }
        else{
            sb.append(" Inner Join Order_blend_details on Order_blend_details.Order_id=tt.Orderid and Order_blend_details.Type_Code in(5,6) ");
        }
        sb.append(" Inner Join scm.MixLot on scm.MixLot.MIXLOTNO = Order_blend_details.MIXLOTNO ");
        sb.append(" Inner Join scm.cottonmixgroup on scm.cottonmixgroup.MIXGROUPCODE = scm.MixLot.MIXGROUPCODE ");
        sb.append(" Inner Join scm.mixgroupabstract on scm.mixgroupabstract.GROUPCODE = scm.cottonmixgroup.ABSTRACTID ");
        //sb.append(" Inner Join scm.mixgroupabstract on scm.mixgroupabstract.GROUPCODE = scm.cottonmixgroup.ABSTRACTID And scm.cottonmixgroup.ABSTRACTID in(1,2,3,4,5,6) ");
        
        try {
            if (theProcessConnection == null) {
                JDBCProcessConnection jdbc = JDBCProcessConnection.getJDBCConnection();
                theProcessConnection = jdbc.getConnection();
            }
            pst = theProcessConnection.prepareStatement(sb.toString());
            rst = pst.executeQuery();
            ResultSetMetaData rsmd = rst.getMetaData();
            while (rst.next()) {
                HashMap themap = new HashMap();
                for (int i = 0; i < rsmd.getColumnCount(); i++) {
                    themap.put(rsmd.getColumnName(i + 1), rst.getString(i + 1));
                }
                MixingLmc.add(themap);
            }
            rst.close();
            pst.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
 public void getPocOthers(String sFromDate, String sToDate,String sOrderNo) {
        PreparedStatement pst = null;
        ResultSet rst = null;
        StringBuilder sb = new StringBuilder();

        sb.append(" SELECT RORDERNO,GROUPCODE,TYPE_CODE,GROUPNAME,TYPE_PERCENTAGE FROM REGULARORDER ");
        sb.append(" Inner Join ( ");
        sb.append(" SELECT DISTINCT ORDERNO,ORDERID FROM( ");
        sb.append(" SELECT ut3_entry.ORDERNO,RegularOrder.Orderid From ut3_entry ");
        sb.append(" Inner join RegularOrder on Regularorder.Rorderno=ut3_entry.Orderno ");
        sb.append(" Inner join PartyMaster on PartyMaster.Partycode= RegularOrder.PartyCode ");
        sb.append(" Inner Join Rmixir on Rmixir.ORDNO = RegularOrder.ROrderNo ");
        sb.append(" Inner Join yarnm on yarnm.YSHCD = Rmixir.YARNSHADECODE ");
        sb.append(" Inner Join YarnCount on yarnCount.CountCode=RegularOrder.CountCode ");
        //sb.append(" WHERE ut3_entry.ENTRYDATE >= "+sFromDate+" AND ut3_entry.ENTRYDATE <= "+sToDate+" ");
        sb.append(" Where Regularorder.UnitCode in(10,12) ");
        if(!sOrderNo.equals("")){
            //sb.append(" AND ORDERNO = '"+sOrderNo+"' ");
            sb.append(" AND ORDERNO in("+sOrderNo+") ");
        }
        sb.append(" Union All ");
        sb.append(" SELECT rkm_entry.ORDERNO,RegularOrder.Orderid From rkm_entry ");
        sb.append(" Inner join RegularOrder on Regularorder.Rorderno=rkm_entry.Orderno ");
        sb.append(" Inner join PartyMaster on PartyMaster.Partycode= RegularOrder.PartyCode ");
        sb.append(" Inner Join Rmixir on Rmixir.ORDNO = RegularOrder.ROrderNo ");
        sb.append(" Inner Join yarnm on yarnm.YSHCD = Rmixir.YARNSHADECODE ");
        sb.append(" Inner Join YarnCount on yarnCount.CountCode=RegularOrder.CountCode ");
        //sb.append(" WHERE rkm_entry.ENTRYDATE >= "+sFromDate+" AND rkm_entry.ENTRYDATE <= "+sToDate+" ");
        sb.append(" Where Regularorder.UnitCode in(10,12) ");
        sb.append(" Union All ");
        sb.append(" select orderno,Orderid ");
        sb.append(" from ( ");
        sb.append(" select test_yarn_reg_details.orderno,RegularOrder.Orderid from test_yarn_reg_details ");
        sb.append(" inner join regularorder on regularorder.rorderno = test_yarn_reg_details.orderno and (Regularorder.UnitCode=10 or Regularorder.UnitCode=12) ");
        sb.append(" inner join partymaster on partymaster.partycode = regularorder.partycode ");
        sb.append(" inner join yarncount on yarncount.countcode=regularorder.countcode ");
        //sb.append(" where to_char(INDATE,'yyyymmdd') >="+sFromDate+" and to_char(INDATE,'yyyymmdd') <="+sToDate+" ");
        if(!sOrderNo.equals("")){
                    //sb.append(" Where ORDERNO = '"+sOrderNo+"' ");
            sb.append(" Where ORDERNO in("+sOrderNo+") ");
        }
        sb.append(" ) t ");
        sb.append(" INNER JOIN SAMPLETOREGULAR ON SAMPLETOREGULAR.REGULARORDERNO = t.ORDERNO AND SAMPLETOREGULAR.CORRECTIONMIXING=0 ");
        sb.append(" LEFT JOIN YARNM ON SAMPLETOREGULAR.YARNSHADECODE = YARNM.YSHCD ");
        sb.append(" ))  tt on tt.Orderno=RegularOrder.ROrderNo ");
        if (!sOrderNo.equals("")) {
        //sb.append(" Inner Join Order_blend_details on Order_blend_details.Order_id=tt.Orderid And tt.Orderno='"+sOrderNo+"' and Order_blend_details.Type_Code in (5,6) ");
            sb.append(" Inner Join Order_blend_details on Order_blend_details.Order_id=tt.Orderid And tt.Orderno in("+sOrderNo+") and Order_blend_details.Type_Code in (5,6) ");
        }
        else{
            sb.append(" Inner Join Order_blend_details on Order_blend_details.Order_id=tt.Orderid and Order_blend_details.Type_Code in(5,6) ");
        }
        sb.append(" Inner Join scm.MixLot on scm.MixLot.MIXLOTNO = Order_blend_details.MIXLOTNO ");
        sb.append(" Inner Join scm.cottonmixgroup on scm.cottonmixgroup.MIXGROUPCODE = scm.MixLot.MIXGROUPCODE ");
        sb.append(" Inner Join scm.mixgroupabstract on scm.mixgroupabstract.GROUPCODE = scm.cottonmixgroup.ABSTRACTID And scm.cottonmixgroup.ABSTRACTID NOT in(1,2,3,4,5,6)");
        sb.append(" Union All "); // AbstractId is null
        sb.append(" SELECT RORDERNO,0 as GROUPCODE,TYPE_CODE,'' as GROUPNAME,TYPE_PERCENTAGE FROM REGULARORDER ");
        sb.append(" Inner Join ( ");
        sb.append(" SELECT DISTINCT ORDERNO,ORDERID FROM( ");
        sb.append(" SELECT ut3_entry.ORDERNO,RegularOrder.Orderid From ut3_entry ");
        sb.append(" Inner join RegularOrder on Regularorder.Rorderno=ut3_entry.Orderno ");
        sb.append(" Inner join PartyMaster on PartyMaster.Partycode= RegularOrder.PartyCode ");
        sb.append(" Inner Join Rmixir on Rmixir.ORDNO = RegularOrder.ROrderNo ");
        sb.append(" Inner Join yarnm on yarnm.YSHCD = Rmixir.YARNSHADECODE ");
        sb.append(" Inner Join YarnCount on yarnCount.CountCode=RegularOrder.CountCode ");
        //sb.append(" WHERE ut3_entry.ENTRYDATE >= "+sFromDate+" AND ut3_entry.ENTRYDATE <= "+sToDate+" ");
        sb.append(" Where Regularorder.UnitCode in(10,12) ");
        if(!sOrderNo.equals("")){
                    //sb.append(" AND ORDERNO = '"+sOrderNo+"' ");
            sb.append(" AND ORDERNO in("+sOrderNo+") ");
        }
        sb.append(" Union All ");
        sb.append(" SELECT rkm_entry.ORDERNO,RegularOrder.Orderid From rkm_entry ");
        sb.append(" Inner join RegularOrder on Regularorder.Rorderno=rkm_entry.Orderno ");
        sb.append(" Inner join PartyMaster on PartyMaster.Partycode= RegularOrder.PartyCode ");
        sb.append(" Inner Join Rmixir on Rmixir.ORDNO = RegularOrder.ROrderNo ");
        sb.append(" Inner Join yarnm on yarnm.YSHCD = Rmixir.YARNSHADECODE ");
        sb.append(" Inner Join YarnCount on yarnCount.CountCode=RegularOrder.CountCode ");
        //sb.append(" WHERE rkm_entry.ENTRYDATE >= "+sFromDate+" AND rkm_entry.ENTRYDATE <= "+sToDate+" ");
        sb.append(" Where Regularorder.UnitCode in(10,12) ");
        if(!sOrderNo.equals("")){
                    //sb.append(" AND ORDERNO = '"+sOrderNo+"' ");
            sb.append(" AND ORDERNO in("+sOrderNo+") ");
        }
        sb.append(" Union All ");
        sb.append(" select orderno,Orderid ");
        sb.append(" from ( ");
        sb.append(" select test_yarn_reg_details.orderno,RegularOrder.Orderid from test_yarn_reg_details ");
        sb.append(" inner join regularorder on regularorder.rorderno = test_yarn_reg_details.orderno and (Regularorder.UnitCode=10 or Regularorder.UnitCode=12) ");
        sb.append(" inner join partymaster on partymaster.partycode = regularorder.partycode ");
        sb.append(" inner join yarncount on yarncount.countcode=regularorder.countcode ");
        //sb.append(" where to_char(INDATE,'yyyymmdd') >="+sFromDate+" and to_char(INDATE,'yyyymmdd') <="+sToDate+" ");
        if(!sOrderNo.equals("")){
                    //sb.append(" Where ORDERNO = '"+sOrderNo+"' ");
            sb.append(" Where ORDERNO in("+sOrderNo+") ");
        }
        sb.append(" ) t ");
        sb.append(" INNER JOIN SAMPLETOREGULAR ON SAMPLETOREGULAR.REGULARORDERNO = t.ORDERNO AND SAMPLETOREGULAR.CORRECTIONMIXING=0 ");
        sb.append(" LEFT JOIN YARNM ON SAMPLETOREGULAR.YARNSHADECODE = YARNM.YSHCD ");
        sb.append(" ))  tt on tt.Orderno=RegularOrder.ROrderNo ");
        if (!sOrderNo.equals("")) {
        //sb.append(" Inner Join Order_blend_details on Order_blend_details.Order_id=tt.Orderid And tt.Orderno='"+sOrderNo+"' and Order_blend_details.Type_Code in (5,6) ");
            sb.append(" Inner Join Order_blend_details on Order_blend_details.Order_id=tt.Orderid And tt.Orderno in ("+sOrderNo+") and Order_blend_details.Type_Code in (5,6) ");
        }
        else{
            sb.append(" Inner Join Order_blend_details on Order_blend_details.Order_id=tt.Orderid and Order_blend_details.Type_Code in(5,6) ");
        }
        sb.append(" Inner Join scm.MixLot on scm.MixLot.MIXLOTNO = Order_blend_details.MIXLOTNO ");
        sb.append(" Inner Join scm.cottonmixgroup on scm.cottonmixgroup.MIXGROUPCODE = scm.MixLot.MIXGROUPCODE ");
        sb.append(" where cottonmixgroup.abstractid is null ");
        
        //System.out.println("qq->"+sb.toString());


        try {
            if (theProcessConnection == null) {
                JDBCProcessConnection jdbc = JDBCProcessConnection.getJDBCConnection();
                theProcessConnection = jdbc.getConnection();
            }
            pst = theProcessConnection.prepareStatement(sb.toString());
            rst = pst.executeQuery();
            ResultSetMetaData rsmd = rst.getMetaData();
            while (rst.next()) {
                HashMap themap = new HashMap();
                for (int i = 0; i < rsmd.getColumnCount(); i++) {
                    themap.put(rsmd.getColumnName(i + 1), rst.getString(i + 1));
                }
                MixingOthers.add(themap);
            }
            rst.close();
            pst.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}