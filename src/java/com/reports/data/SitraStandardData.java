/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.data;
import com.jdbc.connection.JDBCProcessConnection;
import com.jdbc.connection.JDBCProcessaConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import com.common.Common;
import java.sql.ResultSetMetaData;
import java.util.HashMap;
/**
 *
 * @author admin
 */
public class SitraStandardData {
    java.sql.Connection theProcessConnection  = null;
    Common common = new Common();
    private PreparedStatement pst=null;
    private ResultSet rst=null;
    public SitraStandardData() {
    }
    public java.util.List getSitraStandards()
    {
        StringBuffer sb = new StringBuffer();
        java.util.List thelist = new java.util.ArrayList();
        sb.append(" select * from SitraStandards ");
        try
        {
            if(theProcessConnection==null)
            {
                JDBCProcessConnection  jdbc = JDBCProcessConnection.getJDBCConnection();
                theProcessConnection = jdbc . getConnection();
            }
            pst = theProcessConnection.prepareStatement(sb.toString());
            rst = pst . executeQuery();
            ResultSetMetaData rsmd    = rst.getMetaData();
            while(rst.next())
            {
                    HashMap themap = new HashMap();
                    for(int i=0;i<rsmd.getColumnCount();i++)
                    {
                        themap.put(rsmd.getColumnName(i+1),rst.getString(i+1));
                    }
                    thelist.add(themap);
            }
            rst.close();
            pst.close();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return thelist;
    }
}
