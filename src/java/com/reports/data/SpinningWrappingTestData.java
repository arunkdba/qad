/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.data;

import com.jdbc.connection.JDBCProcessConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import com.common.Common;
import com.reports.classes.SpinningWrappingTestDetails;
import java.util.HashMap;
/**
 *
 * @author admin
 */
public class SpinningWrappingTestData {

    java.sql.Connection theProcessConnection  = null;
    Common common = new Common();
    java.util.List theRhlist = new java.util.ArrayList();
    public SpinningWrappingTestData() {
    }
    public java.util.List getTestDetails(String sFromDate,String sToDate,String sTestNo,String sOrderNo,String sAbst, String sdbcode)
    {
        StringBuffer sb = new StringBuffer();
        PreparedStatement pst = null;
        ResultSet   rst = null;
        java.util.List theList = new java.util.ArrayList();
        SpinningWrappingTestDetails testdetails = new SpinningWrappingTestDetails();
        if(sAbst.equals("1")){      // For Abstract Report
            sb.append(" select to_char(INDATE,'yyyymmdd'),TEST_YARN_REG_DETAILS.MACH_ST_NAME,CNT1,CNT2,CNT3,CNT4,STR1,STR2,STR3,STR4, ");
            sb.append(" TEST_YARN_REG_DETAILS.Orderno,TPI,DRAFT,BREAKDRAFT,CPWHEEL,PUMBCOLOR,TESTTYPE,YARNM.YSHNM,YARNCOUNT.COUNTNAME,");
            sb.append(" TEST_YARN_REG_DETAILS.REMARKS FROM TEST_YARN_REG_DETAILS ");
            sb.append(" Inner join Test_Yarn_Acc_Ora_Mach on Test_Yarn_Acc_Ora_Mach.Machno = TEST_YARN_REG_DETAILS.Mach_St_Name And Test_Yarn_Acc_Ora_Mach.Dbcode =  TEST_YARN_REG_DETAILS.Dbcode ");
            sb.append(" Left join RegularOrder on RegularOrder.ROrderNo=TEST_YARN_REG_DETAILS.Orderno ");
            sb.append(" Left Join Rmixir on Rmixir.ORDNO = RegularOrder.ROrderNo and Rmixir.CORRECTIONMIXING=0 ");
            sb.append(" Left Join yarnm on yarnm.YSHCD = Rmixir.YARNSHADECODE ");
            sb.append(" Left Join YarnCount on yarnCount.CountCode=RegularOrder.CountCode ");
            sb.append(" WHERE to_char(INDATE,'yyyymmdd') >= "+sFromDate+" AND  to_char(INDATE,'yyyymmdd') <= "+sToDate+" ");
            sb.append(" and TEST_YARN_REG_DETAILS.DBCODE = "+sdbcode+" ");
            if(!sTestNo.equals("All")){
            sb.append(" AND TEST_NO = "+sTestNo+" ");
            }
            if(!sOrderNo.equals("")){
                sb.append(" OR ORDERNO = '"+sOrderNo+"' ");
            }
            sb.append(" Order by to_number(Test_Yarn_Acc_Ora_Mach.Ora_Mach_Name), test_no ");
            //sb.append(" Order by MACH_ST_NAME,test_no ");
        }
        else
        {
            sb.append(" select to_char(INDATE,'yyyymmdd'),TEST_YARN_REG_DETAILS.MACH_ST_NAME,CNT1,CNT2,CNT3,CNT4,CNT5,STR1,STR2,STR3,STR4,STR5, ");
            sb.append(" TEST_YARN_REG_DETAILS.Orderno,TPI,DRAFT,BREAKDRAFT,CPWHEEL,wm_concat(PUMBCOLOR) as Pumbcolor,TESTTYPE,YARNM.YSHNM,YARNCOUNT.COUNTNAME,");
            sb.append(" TEST_YARN_REG_DETAILS.REMARKS,TEST_YARN_REG_DETAILS.TEST_NO FROM TEST_YARN_REG_DETAILS ");
            sb.append(" Left join RegularOrder on RegularOrder.ROrderNo=TEST_YARN_REG_DETAILS.Orderno ");
            sb.append(" Left Join Rmixir on Rmixir.ORDNO = RegularOrder.ROrderNo and Rmixir.CORRECTIONMIXING=0 ");
            sb.append(" Left Join yarnm on yarnm.YSHCD = Rmixir.YARNSHADECODE ");
            sb.append(" Left Join YarnCount on yarnCount.CountCode=RegularOrder.CountCode ");
            sb.append(" WHERE to_char(INDATE,'yyyymmdd') >= "+sFromDate+" AND  to_char(INDATE,'yyyymmdd') <= "+sToDate+" ");
            sb.append(" and TEST_YARN_REG_DETAILS.DBCODE = "+sdbcode+" ");
            if(!sTestNo.equals("All")){
                sb.append(" AND TEST_NO = "+sTestNo+" ");
            }
            if(!sOrderNo.equals("")){
                sb.append(" OR ORDERNO = '"+sOrderNo+"' ");
            }
            sb.append(" Group by to_char(INDATE,'yyyymmdd'),TEST_YARN_REG_DETAILS.MACH_ST_NAME,CNT1,CNT2, ");
            sb.append(" CNT3,CNT4,CNT5,STR1,STR2,STR3,STR4,STR5,TEST_YARN_REG_DETAILS.Orderno,TPI,DRAFT,BREAKDRAFT, ");
            sb.append(" CPWHEEL,TESTTYPE,YARNM.YSHNM,YARNCOUNT.COUNTNAME,TEST_YARN_REG_DETAILS.REMARKS, TEST_YARN_REG_DETAILS.TEST_NO ");
            sb.append(" Order by MACH_ST_NAME,test_no ");
        }
        System.out.println("QQQ-->"+sb.toString());
        try
        {
            if(theProcessConnection==null){
                    JDBCProcessConnection  jdbc = JDBCProcessConnection.getJDBCConnection();
                    theProcessConnection = jdbc . getConnection();
             }
             pst = theProcessConnection.prepareStatement(sb.toString());
             rst = pst . executeQuery();
             while(rst.next()){
                 testdetails = new SpinningWrappingTestDetails();
                 testdetails.setTestdate(rst.getString(1));
                 testdetails.setMechinename(rst.getString(2));
                 testdetails.setCnt1(rst.getString(3));
                 testdetails.setCnt2(rst.getString(4));
                 testdetails.setCnt3(rst.getString(5));
                 testdetails.setCnt4(rst.getString(6));
                 testdetails.setCnt5(rst.getString(7));
                 testdetails.setStrength1(rst.getString(8));
                 testdetails.setStrength2(rst.getString(9));
                 testdetails.setStrength3(rst.getString(10));
                 testdetails.setStrength4(rst.getString(11));
                 testdetails.setStrength5(rst.getString(12));
                 testdetails.setOrderno(common.parseNull(rst.getString(13)));
                 testdetails.setTpi(common.parseNull(rst.getString(14)));
                 testdetails.setDraft(common.parseNull(rst.getString(15)));
                 testdetails.setBdraft(common.parseNull(rst.getString(16)));
                 testdetails.setCpwheel(common.parseNull(rst.getString(17)));
                 testdetails.setPumpclr(common.parseNull(rst.getString(18)));
                 testdetails.setTesttype(common.parseNull(rst.getString(19)));
                 testdetails.setShadename(common.parseNull(rst.getString(20)));
                 testdetails.setCount(common.parseNull(rst.getString(21)));
                 testdetails.setRemarks(common.parseNull(rst.getString(22)));
                 testdetails.setCsp1(common.getRound(common.toDouble(rst.getString(3))*common.toDouble(rst.getString(8)),0));
                 testdetails.setCsp2(common.getRound(common.toDouble(rst.getString(4))*common.toDouble(rst.getString(9)),0));
                 testdetails.setCsp3(common.getRound(common.toDouble(rst.getString(5))*common.toDouble(rst.getString(10)),0));
                 testdetails.setCsp4(common.getRound(common.toDouble(rst.getString(6))*common.toDouble(rst.getString(11)),0));
                 testdetails.setCsp5(common.getRound(common.toDouble(rst.getString(7))*common.toDouble(rst.getString(12)),0));
                 
                 //System.out.println("Machine Name : "+rst.getString(2));
                 
                 theList.add(testdetails);
             }
             rst.close();
             pst.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return theList;
    }
    
    public java.util.List getTestNo(String SFrom,String STo,String SMachine)
    {
        PreparedStatement pst = null;
        ResultSet   rst = null;
        java.util.List thelist = new java.util.ArrayList();
        StringBuffer sb = new StringBuffer();
        sb.append(" select Distinct TEST_NO From test_yarn_reg_details ");
        sb.append(" where to_char(INDATE,'yyyymmdd') >= "+SFrom+" and  to_char(INDATE,'yyyymmdd') <= "+STo+"" );
        sb.append(" and DBCODE = "+SMachine+" Order by TEST_NO ");
         
        System.out.println("Test-->"+sb.toString());
        try
        {
            if(theProcessConnection==null){
                    JDBCProcessConnection  jdbc = JDBCProcessConnection.getJDBCConnection();
                    theProcessConnection = jdbc . getConnection();
             }
             pst = theProcessConnection.prepareStatement(sb.toString());
             rst = pst . executeQuery();
             while(rst.next()){
                 thelist.add(rst.getString(1));
             }
             rst.close();
             pst.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return thelist;
    }
    public java.util.List getTestBase(String SFrom,String STo,String sTestNo,String sdbcode)
    {
        PreparedStatement pst = null;
        ResultSet   rst = null;
        java.util.List thelist = new java.util.ArrayList();
        StringBuffer sb = new StringBuffer();

        sb.append(" select distinct test_no,nomhnkcnt,wbtemp,indate,nomstrength,dbtemp,ttime,samplelength,rh,shift,countname,operator from ");
        sb.append(" (select test_yarn_reg_details.TEST_NO, NOMHNKCNT,WBTEMP, ");
        sb.append(" to_char(test_yarn_reg_details.INDATE,'yyyymmdd') as indate,NOMSTRENGTH,DBTEMP, ");
        sb.append(" '' as TTime,SAMPLELENGTH,RH,'' as SHIFT,test_yarn_regular.COUNTNAME,'' as OPERATOR ");
        sb.append(" From test_yarn_reg_details ");
        sb.append(" inner Join test_yarn_regular on (test_yarn_regular.TEST_NO = test_yarn_reg_details.TEST_NO ");
        sb.append(" and test_yarn_regular.INDATE = test_yarn_reg_details.INDATE) ");
        sb.append(" and test_yarn_regular.DBCODE = "+sdbcode+" ");
        sb.append(" and to_char(test_yarn_reg_details.INDATE,'yyyymmdd')>="+SFrom+" and to_char(test_yarn_reg_details.INDATE,'yyyymmdd')<= "+STo+"");
        sb.append(" where test_yarn_reg_details.TEST_NO ="+sTestNo+" ");
        sb.append(" ) ");
        /*
        sb.append(" select Distinct test_yarn_reg_details.TEST_NO, NOMHNKCNT,WBTEMP, ");
        sb.append(" to_char(test_yarn_reg_details.INDATE,'yyyymmdd'),NOMSTRENGTH,DBTEMP, ");
        sb.append(" '' as TTime,SAMPLELENGTH,RH,'' as SHIFT,test_yarn_regular.COUNTNAME,'' as OPERATOR ");
        sb.append(" From test_yarn_reg_details ");
        sb.append(" Left Join test_yarn_regular on test_yarn_regular.TEST_NO = test_yarn_reg_details.TEST_NO and  test_yarn_regular.DBCODE = "+sdbcode+"");
        sb.append(" where to_char(test_yarn_reg_details.INDATE,'yyyymmdd') >= "+SFrom+" and  to_char(test_yarn_reg_details.INDATE,'yyyymmdd') <= "+STo+" ");
        sb.append(" AND test_yarn_reg_details.TEST_NO = "+sTestNo+" ");
        */
        
        System.out.println("Test-->"+sb.toString());
        
        try
        {
            if(theProcessConnection==null){
                    JDBCProcessConnection  jdbc = JDBCProcessConnection.getJDBCConnection();
                    theProcessConnection = jdbc . getConnection();
             }
             pst = theProcessConnection.prepareStatement(sb.toString());
             rst = pst . executeQuery();
             //ResultSetMetaData rsmd    = rst.getMetaData();
             while(rst.next()){
                 //HashMap themap = new HashMap();
                /* for(int i=0;i<rsmd.getColumnCount();i++)
                    {
                        themap.put(rsmd.getColumnName(i+1),rst.getString(i+1));
                    }*/                 
                 thelist.add(0,rst.getString(1));
                 thelist.add(1, rst.getString(2));
                 thelist.add(2,rst.getString(3));
                 thelist.add(3,rst.getString(4));
                 thelist.add(4,rst.getString(5));
                 thelist.add(5,rst.getString(6));
                 thelist.add(6,rst.getString(7));
                 thelist.add(7,rst.getString(8));
                 thelist.add(8,rst.getString(9));
                 thelist.add(9,rst.getString(10));
                 thelist.add(10,rst.getString(11));
                 thelist.add(11,rst.getString(12));
             }
             rst.close();
             pst.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return thelist;
    }
    public java.util.List getRhFactors()
    {
       PreparedStatement pst = null;
        ResultSet   rst = null;
        StringBuffer sb = new StringBuffer();
        HashMap theMap = new HashMap();
        sb.append(" Select RH,COTTON from rhcorrectionfactors Order by RH ");
        try
        {
            if(theProcessConnection==null){
                    JDBCProcessConnection  jdbc = JDBCProcessConnection.getJDBCConnection();
                    theProcessConnection = jdbc . getConnection();
             }
            pst = theProcessConnection.prepareStatement(sb.toString());
            rst = pst . executeQuery();
             while(rst.next()){
                  theMap = new HashMap();
                  theMap.put("RH",rst.getString(1));
                   theMap.put("COTTON",rst.getString(2));
                   theRhlist.add(theMap);
             }
             rst.close();
             pst.close();
        }
        catch(Exception ex)
        {
           ex.printStackTrace(); 
        }
        return theRhlist;
    }
    public double getrh (String sRh)
    {
        String SValue="";
        HashMap theMap = new HashMap();
        
        //System.out.println("Rhhh->"+sRh);
        
        for(int i =0;i<theRhlist.size();i++)
        {
            theMap = (HashMap)theRhlist.get(i);
            if(sRh.equals((String)theMap.get("RH")))
            {
                   SValue  = common.parseNull((String)theMap.get("COTTON"));
            }
            else if(common.toDouble((String)theMap.get("RH")) > common.toDouble(sRh))
            {
                 SValue  = common.parseNull((String)theMap.get("COTTON"));
                 break;
            }
            else if(common.toDouble(sRh)>90)
            {
                //SValue = "1.07";
            }
        }
        return common.toDouble(SValue);
    }
    public String removeNull(String Svalue){
        String sReturn="";
        if(Svalue.equals("") || Svalue.equals("0") || Svalue.equals("0.00") || Svalue.equals("null"))
        {
            sReturn = "";
        }
        else
        {
            sReturn = Svalue;
        }
        return sReturn;
    }
}