/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.data;

import com.jdbc.connection.JDBCProcessConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import com.common.Common;
import com.reports.classes.TwistTpiDetails;
/**
 *
 * @author root
 */
public class TwistTpiData {
    java.sql.Connection theProcessConnection  = null;
    Common common = new Common();

    public TwistTpiData() {
    }
    public java.util.List getTestDetails(String sFromDate,String sToDate,String sOrderNo)
    {
        StringBuffer sb = new StringBuffer();
        PreparedStatement pst = null;
        ResultSet   rst = null;
        java.util.List theList = new java.util.ArrayList();
        TwistTpiDetails ttd = new TwistTpiDetails();

        sb.append(" SELECT TPITESTS.TESTNO,TPITESTS.NOMTWIST,TPITESTS.NOMCOUNT,TPITESTS.FRAME, ");
        sb.append(" TPITESTS.TESTDATE,TPITESTS.LOT,TPITESTS.COUNTTYPE,TPITESTS.TESTS,TPITESTS.MINTPI, ");
        sb.append(" TPITESTS.MAXTPI,TPITESTS.AVGTPI,RANGTPI,YARNM.YSHNM,TPITESTS.CVTPI ");
        sb.append(" FROM TPITESTS ");
        sb.append(" Inner join RegularOrder on Regularorder.Rorderno=TPITESTS.LOT ");
        sb.append(" Inner Join Rmixir on Rmixir.ORDNO = RegularOrder.ROrderNo and Rmixir.CorrectionMixing=0 ");
        sb.append(" Inner Join yarnm on yarnm.YSHCD = Rmixir.YARNSHADECODE ");
        sb.append(" WHERE TESTDATE >= "+sFromDate+" AND TESTDATE<= "+sToDate+" ");
        if(!sOrderNo.equals("")){
            sb.append(" AND LOT = '"+sOrderNo+"' ");
        }
        sb.append(" Order by TESTNO ");
        
     //   System.out.println("Qry-->"+sb.toString());

        try{
            if(theProcessConnection==null){
                    JDBCProcessConnection  jdbc = JDBCProcessConnection.getJDBCConnection();
                    theProcessConnection = jdbc . getConnection();
             }
             pst = theProcessConnection.prepareStatement(sb.toString());
             rst = pst . executeQuery();
             while(rst.next()){
                 ttd = new TwistTpiDetails();
                 ttd.setTestno(rst.getString(1));
                 ttd.setNomtwist(rst.getString(2));
                 ttd.setNomcount(rst.getString(3));
                 ttd.setFrame(rst.getString(4));
                 ttd.setTestdate(rst.getString(5));
                 ttd.setLot(rst.getString(6));
                 ttd.setCounttype(rst.getString(7));
                 ttd.setTests(rst.getString(8));
                 ttd.setMintpi(rst.getString(9));
                 ttd.setMaxtpi(rst.getString(10));
                 ttd.setAvgtpi(rst.getString(11));
                 ttd.setRangtpi(rst.getString(12));
                 ttd.setShade(rst.getString(13));
                 ttd.setTpicv(rst.getString(14));

                 theList.add(ttd);
             }
             rst.close();
             pst.close();
        }
        catch(Exception ex){

        }
        return theList;
    }
}