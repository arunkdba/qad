/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.data;
import com.common.Common;
import com.jdbc.connection.JDBCProcessConnection;
import com.jdbc.connection.JDBCScmConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.HashMap;
/**
 *
 * @author admin
 */
public class Units {
    java.sql.Connection theScmConnection = null;
    java.sql.Connection theProcessConnection = null;
    Common common = new Common();
    java.util.List theUnitlist = new java.util.ArrayList();
    public Units() {
    }   
    public java.util.List getUnits() {
        java.util.List thelist = new java.util.ArrayList();        
        StringBuffer sb = new StringBuffer();
                     sb . append(" select UNITCODE,UNITNAME from unit order by UNITNAME ");
        
                     System.out.println("Units-->"+sb.toString());
        try {
            if(theScmConnection==null)
            {
                JDBCScmConnection jdbc = JDBCScmConnection.getJDBCConnection();
                theScmConnection       = jdbc.getConnection();
            }
            PreparedStatement pst=null;
            ResultSet rst=null;
            pst = theScmConnection.prepareStatement(sb.toString());
            rst = pst . executeQuery();
            ResultSetMetaData rsmd    = rst.getMetaData();
            while(rst.next())
            {
                    HashMap themap = new HashMap();
                    for(int i=0;i<rsmd.getColumnCount();i++)
                    {
                        themap.put(rsmd.getColumnName(i+1),rst.getString(i+1));                        
                    }                    
                    thelist.add(themap);
            }
            rst.close();
            pst.close();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return thelist;
    }
    public java.util.List getUnit() {
        java.util.List thelist = new java.util.ArrayList();        
        StringBuffer sb = new StringBuffer();
                     sb . append(" select UNITCODE,UNITNAME from unit where UNITCODE in(1,2,3,10) order by UNITNAME ");
        
        try {
            if(theScmConnection==null)
            {
                JDBCScmConnection jdbc = JDBCScmConnection.getJDBCConnection();
                theScmConnection       = jdbc.getConnection();
            }
            PreparedStatement pst=null;
            ResultSet rst=null;
            pst = theScmConnection.prepareStatement(sb.toString());
            rst = pst . executeQuery();
            ResultSetMetaData rsmd    = rst.getMetaData();
            while(rst.next())
            {
                    HashMap themap = new HashMap();
                    for(int i=0;i<rsmd.getColumnCount();i++)
                    {
                        themap.put(rsmd.getColumnName(i+1),rst.getString(i+1));                        
                    }                    
                    thelist.add(themap);
            }
            rst.close();
            pst.close();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return thelist;
    }
    public String getUnitName (String SCode)
    {
        String sUnitBName = "";
        StringBuffer sb = new StringBuffer();
                     sb . append(" select UNITNAME from unit where UNITCODE ="+SCode+" ");
                     //System.out.println("gg-->"+sb.toString());
        try {
            if(theScmConnection==null)
            {
                JDBCScmConnection jdbc = JDBCScmConnection.getJDBCConnection();
                theScmConnection       = jdbc.getConnection();
            }
            PreparedStatement pst=null;
            ResultSet rst=null;
            pst = theScmConnection.prepareStatement(sb.toString());
            rst = pst . executeQuery();            
            while(rst.next())
            {
                sUnitBName = rst.getString(1);
            }
            rst.close();
            pst.close();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return sUnitBName;
    }
    public java.util.List getCountName() {
        java.util.List thelist = new java.util.ArrayList();        
        StringBuffer sb = new StringBuffer();
                     sb . append(" select CountCode,CountName from yarncount where COUNTGROUPNO is not null order by COUNTGROUPNO ");
        
        try {
            if(theScmConnection==null)
            {
                JDBCScmConnection jdbc = JDBCScmConnection.getJDBCConnection();
                theScmConnection       = jdbc.getConnection();
            }
            PreparedStatement pst=null;
            ResultSet rst=null;
            pst = theScmConnection.prepareStatement(sb.toString());
            rst = pst . executeQuery();
            ResultSetMetaData rsmd    = rst.getMetaData();
            while(rst.next())
            {
                    HashMap themap = new HashMap();
                    for(int i=0;i<rsmd.getColumnCount();i++)
                    {
                        themap.put(rsmd.getColumnName(i+1),rst.getString(i+1));                        
                    }                    
                    thelist.add(themap);
            }
            rst.close();
            pst.close();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return thelist;
    }
    public java.util.List getExtCountAllowed() {
        java.util.List thelist = new java.util.ArrayList();        
        StringBuffer sb = new StringBuffer();
                     sb . append(" select Code,ExactCount From CountVariation order by 1 ");
        
        try {
            if(theScmConnection==null)
            {
                JDBCScmConnection jdbc = JDBCScmConnection.getJDBCConnection();
                theScmConnection       = jdbc.getConnection();
            }
            PreparedStatement pst=null;
            ResultSet rst=null;
            pst = theScmConnection.prepareStatement(sb.toString());
            rst = pst . executeQuery();
            ResultSetMetaData rsmd    = rst.getMetaData();
            while(rst.next())
            {
                    HashMap themap = new HashMap();
                    for(int i=0;i<rsmd.getColumnCount();i++)
                    {
                        themap.put(rsmd.getColumnName(i+1),rst.getString(i+1));                        
                    }                    
                    thelist.add(themap);
            }
            rst.close();
            pst.close();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return thelist;
    }
    public java.util.List getDbcode() {
        java.util.List thelist = new java.util.ArrayList();
        StringBuffer sb = new StringBuffer();
                     sb . append(" select distinct dbcode from test_yarn_regular order by dbcode ");
        
        try {
            if(theScmConnection==null)
            {
                JDBCScmConnection jdbc = JDBCScmConnection.getJDBCConnection();
                theScmConnection       = jdbc.getConnection();
            }
            PreparedStatement pst=null;
            ResultSet rst=null;
            pst = theScmConnection.prepareStatement(sb.toString());
            rst = pst . executeQuery();
            ResultSetMetaData rsmd    = rst.getMetaData();
            while(rst.next())
            {
                    HashMap themap = new HashMap();
                    for(int i=0;i<rsmd.getColumnCount();i++)
                    {
                        themap.put(rsmd.getColumnName(i+1),rst.getString(i+1));                        
                    }                    
                    thelist.add(themap);
            }
            rst.close();
            pst.close();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return thelist;
    }
    public java.util.List getDepartment(){
      java.util.List thelist = new java.util.ArrayList();       
      try{

         StringBuffer   sb = new StringBuffer();
                        sb . append(" select deptname,deptcode,suff_name from department");
                        sb . append(" where suff_name is not null");
                        sb . append(" order by deptcode");

          if(theProcessConnection==null){
                    JDBCProcessConnection  jdbc = JDBCProcessConnection.getJDBCConnection();
                    theProcessConnection = jdbc . getConnection();
          }

         java.sql.PreparedStatement    stat  = theProcessConnection.prepareStatement(sb.toString());
         ResultSet                     res   = stat.executeQuery();

         HashMap hmt = new HashMap();

         hmt.put("DEPTNAME","ALL");
         hmt.put("DEPTCODE","999");
         hmt.put("SUFF_NAME","ALL");

         thelist.add(hmt);

         while(res.next()){

            HashMap hm = new HashMap();

               hm.put("DEPTNAME",common.parseNull((String)res.getString(1)));
               hm.put("DEPTCODE",common.parseNull((String)res.getString(2)));
               hm.put("SUFF_NAME",common.parseNull((String)res.getString(3)));

            thelist.add(hm);
         }
         res   . close();
         stat  . close();
      }catch(Exception ex){

         ex.printStackTrace();
      }
      return thelist;
   }
}
