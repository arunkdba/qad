/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.data;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import com.jdbc.connection.JDBCProcessConnection;
import com.common.Common;
import com.reports.classes.CspDetails;
import com.reports.classes.RkmDetails;
import com.reports.classes.Ut3DateWise;
import java.util.HashMap;
/**
 *
 * @author admin
 */
public class Ut3RkmDateWiseData {
    java.sql.Connection theProcessConnection = null;
    Common common = new Common();
    public Ut3RkmDateWiseData() {
    }
    public java.util.List getUt3(String sFromDate, String sToDate,String sUnitCode,String sProcessCode,String sShift)
    {
        java.util.List Ut3List = new java.util.ArrayList();
        PreparedStatement pst = null;
        ResultSet rst = null;
        StringBuilder sb = new StringBuilder();
        Ut3DateWise details = new Ut3DateWise();
        sb.append(" SELECT ut3_entry.ORDERNO,ut3_entry.ENTRYDATE,UPER,THIN,THICK,NEP200,NEP280,TOT200,TOT280,CVM,TESTNO,ut3_entry.REMARKS, ");
        sb.append(" PartyMaster.PartyName,RegularOrder.Weight,YARNM.YSHNM,Scm.Unit.UNITNAME,YARNCOUNT.COUNTNAME ");
        sb.append(" From ut3_entry ");
        sb.append(" Inner join RegularOrder on Regularorder.Rorderno=ut3_entry.Orderno ");
        sb.append(" Inner join scm.ProcessingType on scm.ProcessingType.ProcessCode = regularorder.ProcessTypeCode ");
        sb.append(" Inner join PartyMaster on PartyMaster.Partycode= RegularOrder.PartyCode ");
        sb.append(" Inner join Scm.Unit on Scm.Unit.UnitCode= RegularOrder.UnitCode ");
        sb.append(" Inner Join Rmixir on Rmixir.ORDNO = RegularOrder.ROrderNo and Rmixir.CorrectionMixing=0 ");
	sb.append(" Inner Join yarnm on yarnm.YSHCD = Rmixir.YARNSHADECODE ");
        sb.append(" Inner Join YarnCount on yarnCount.CountCode=RegularOrder.CountCode ");
        sb.append(" WHERE ut3_entry.ENTRYDATE >= "+sFromDate+" AND ut3_entry.ENTRYDATE <= "+sFromDate+" ");
        if(!sUnitCode.equals("All")){

        if(sUnitCode.equals("10"))
            {
                sb.append(" and Regularorder.UnitCode in(10,12) ");
            }
            else{
                sb.append(" and Regularorder.UnitCode= "+sUnitCode+"");
            }
        }
        if(!sShift.equals("All")){
            sb.append(" and ut3_entry.SHIFT_NAME= "+sShift+"");
        }
        if(sProcessCode.equals("1")){
            sb.append(" and ProcessingType.OEStatus = 1 ");
        }
        else
        {
            sb.append(" and ProcessingType.OEStatus = 0 ");
        }
        sb.append(" Order by ut3_entry.ENTRYDATE,ut3_entry.ORDERNO,TESTNO,Scm.Unit.UNITNAME ");

        //System.out.println("Ut3-->"+sb.toString());
        try
        {
            if(theProcessConnection == null)
            {
                JDBCProcessConnection jdbc = JDBCProcessConnection.getJDBCConnection();
                theProcessConnection = jdbc.getConnection();
            }
            pst = theProcessConnection.prepareStatement(sb.toString());
            rst = pst.executeQuery();
            while(rst.next())
            {
                 details = new Ut3DateWise();
                 details.setOrderno(common.parseNull(rst.getString(1)));
                 details.setEntrydate(common.parseNull(rst.getString(2)));
                 details.setUper(common.parseNull(rst.getString(3)));
                 details.setThin(common.parseNull(rst.getString(4)));
                 details.setThick(common.parseNull(rst.getString(5)));
                 details.setNep200(common.parseNull(rst.getString(6)));
                 details.setNep280(common.parseNull(rst.getString(7)));                                  
                 details.setTot200(common.parseNull(rst.getString(8)));
                 details.setTot280(common.parseNull(rst.getString(9)));
                 details.setCvm(common.parseNull(rst.getString(10)));
                 details.setTestno(common.parseNull(rst.getString(11)));
                 details.setRemarks(common.parseNull(rst.getString(12)));
                 details.setPartyname(common.parseNull(rst.getString(13)));
                 details.setOrdweight(common.parseNull(rst.getString(14)));
                 details.setShadename(common.parseNull(rst.getString(15)));
                 details.setUnitname(common.parseNull(rst.getString(16)));
                 details.setCountname(common.parseNull(rst.getString(17)));

                 Ut3List.add(details);
            }
            rst.close();
            pst.close();
        }
        catch(Exception ex){
        }
        return Ut3List;
    }
    public java.util.List getCsp(String sFromDate, String sToDate){
        java.util.List cspList = new java.util.ArrayList();
        PreparedStatement pst = null;
        ResultSet rst = null;
        StringBuilder sb = new StringBuilder();
        CspDetails testdetails = new CspDetails();
        sb.append(" select to_char(INDATE,'yyyymmdd'),MACH_ST_NAME,CNT1,CNT2,CNT3,CNT4,STR1,STR2,STR3,STR4, ");
        sb.append(" ORDERNO,TPI,DRAFT,BREAKDRAFT,CPWHEEL,PUMBCOLOR,TESTTYPE,YARNM.YSHNM,YARNCOUNT.COUNTNAME FROM TEST_YARN_REG_DETAILS ");
        sb.append(" Inner join RegularOrder on Regularorder.Rorderno=TEST_YARN_REG_DETAILS.Orderno ");
        sb.append(" Inner Join Rmixir on Rmixir.ORDNO = RegularOrder.ROrderNo and Rmixir.CorrectionMixing=0 ");
	sb.append(" Inner Join yarnm on yarnm.YSHCD = Rmixir.YARNSHADECODE ");
        sb.append(" Inner Join YarnCount on yarnCount.CountCode=RegularOrder.CountCode ");
        sb.append(" WHERE to_char(INDATE,'yyyymmdd') >= "+sFromDate+" AND  to_char(INDATE,'yyyymmdd') <= "+sToDate+" ");
        sb.append(" Order by test_no ");

        try{
            if(theProcessConnection==null){
                JDBCProcessConnection jdbc = JDBCProcessConnection.getJDBCConnection();
                theProcessConnection = jdbc.getConnection();
            }
            pst = theProcessConnection.prepareStatement(sb.toString());
            rst = pst.executeQuery();
            while(rst.next()){
                 testdetails = new CspDetails();
                 testdetails.setTestdate(rst.getString(1));
                 testdetails.setMechinename(rst.getString(2));
                 testdetails.setCnt1(rst.getString(3));
                 testdetails.setCnt2(rst.getString(4));
                 testdetails.setCnt3(rst.getString(5));
                 testdetails.setCnt4(rst.getString(6));
                 testdetails.setStrength1(rst.getString(7));
                 testdetails.setStrength2(rst.getString(8));
                 testdetails.setStrength3(rst.getString(9));
                 testdetails.setStrength4(rst.getString(10));
                 testdetails.setOrderno(rst.getString(11));
                 testdetails.setTpi(rst.getString(12));
                 testdetails.setDraft(rst.getString(13));
                 testdetails.setBdraft(rst.getString(14));
                 testdetails.setCpwheel(rst.getString(15));
                 testdetails.setPumpclr(rst.getString(16));
                 testdetails.setTesttype(rst.getString(17));
                 testdetails.setShadename(rst.getString(18));
                 testdetails.setCountname(rst.getString(19));
                 testdetails.setCsp1(common.getRound(common.toDouble(rst.getString(3))*common.toDouble(rst.getString(7)),0));
                 testdetails.setCsp2(common.getRound(common.toDouble(rst.getString(4))*common.toDouble(rst.getString(8)),0));
                 testdetails.setCsp3(common.getRound(common.toDouble(rst.getString(5))*common.toDouble(rst.getString(9)),0));
                 testdetails.setCsp4(common.getRound(common.toDouble(rst.getString(6))*common.toDouble(rst.getString(10)),0));
                 cspList.add(testdetails);
            }
            rst.close();
            pst.close();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return cspList;
    }
    public java.util.List getRkm(String SFrom, String STo,String sUnitCode,String sProcessCode,String sShift){
        java.util.List rkmList = new java.util.ArrayList();
        PreparedStatement pst = null;
        ResultSet rst = null;
        StringBuilder sb = new StringBuilder();
        RkmDetails details = new RkmDetails();
        sb.append(" SELECT rkm_entry.ORDERNO,rkm_entry.ENTRYDATE,RKM,RKMCV,ELGCV,ELG,SYS,TESTNO,rkm_entry.REMARKS,Scm.Unit.UNITNAME, ");
        sb.append(" SYSCV,YARNM.YSHNM,YARNCOUNT.COUNTNAME From rkm_entry ");
        sb.append(" Inner join RegularOrder on Regularorder.Rorderno=rkm_entry.Orderno ");
        sb.append(" Inner join scm.ProcessingType on scm.ProcessingType.ProcessCode = regularorder.ProcessTypeCode ");
        sb.append(" Inner join Scm.Unit on Scm.Unit.UnitCode= RegularOrder.UnitCode ");
        sb.append(" Inner Join Rmixir on Rmixir.ORDNO = RegularOrder.ROrderNo and Rmixir.CorrectionMixing=0 ");
	sb.append(" Inner Join yarnm on yarnm.YSHCD = Rmixir.YARNSHADECODE ");
        sb.append(" Inner Join YarnCount on yarnCount.CountCode=RegularOrder.CountCode ");
        sb.append(" WHERE rkm_entry.ENTRYDATE >= ").append(SFrom).append(" AND rkm_entry.ENTRYDATE <= ").append(STo).append(" ");
        if(!sUnitCode.equals("All")){
            if(sUnitCode.equals("10"))
            {
                sb.append(" and Regularorder.UnitCode in(10,12) ");
            }
            else{
                sb.append(" and Regularorder.UnitCode= "+sUnitCode+"");
            }
        }
        if(!sShift.equals("All")){
            sb.append(" and rkm_entry.SHIFT_NAME= "+sShift+"");
        }
        if(sUnitCode.equals("All")){
               sb.append(" and (ProcessingType.OEStatus = 1 or ProcessingType.OEStatus = 0) ");
        }
        else{
            if(sProcessCode.equals("1")){
                sb.append(" and ProcessingType.OEStatus = 1 ");
            }
            else{
                sb.append(" and ProcessingType.OEStatus = 0 ");
            }
        }
        sb.append(" Order by rkm_entry.ENTRYDATE,rkm_entry.ORDERNO,TESTNO, Scm.Unit.UNITNAME");

        System.out.println("dd getRkm-->"+sb.toString());

        try{
            if(theProcessConnection==null){
                JDBCProcessConnection jdbc = JDBCProcessConnection.getJDBCConnection();
                theProcessConnection = jdbc.getConnection();
            }
            pst = theProcessConnection.prepareStatement(sb.toString());
            rst = pst.executeQuery();
            while(rst.next()){
                 details = new RkmDetails();
                 details.setOrderno(rst.getString(1));
                 details.setEntrydate(rst.getString(2));
                 details.setRkm(rst.getString(3));
                 details.setRkmcv(rst.getString(4));
                 details.setElgcv(rst.getString(5));
                 details.setElg(rst.getString(6));
                 details.setSys(rst.getString(7));
                 details.setTestno(rst.getString(8));
                 details.setRemarks(rst.getString(9));
                 details.setUnitname(common.parseNull(rst.getString(10)));
                 details.setSyscv(rst.getString(11));
                 details.setShadename(common.parseNull(rst.getString(12)));
                 details.setCountname(common.parseNull(rst.getString(13)));
                 rkmList.add(details);
             }
             rst.close();
             pst.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return rkmList;
    }
    public java.util.List getRkmNew(String SFrom, String STo,String sUnitCode,String sProcessCode,String sShift,String sOrderbyvalue){
        java.util.List rkmList = new java.util.ArrayList();
        PreparedStatement pst = null;
        ResultSet rst = null;
        StringBuilder sb = new StringBuilder();
        RkmDetails details = new RkmDetails();
        sb.append(" SELECT distinct rkm_entry.ORDERNO,rkm_entry.ENTRYDATE,RKM,RKMCV,ELGCV,ELG,SYS,TESTNO,rkm_entry.REMARKS,Scm.Unit.UNITNAME, ");
        sb.append(" SYSCV,YARNM.YSHNM,YARNCOUNT.COUNTNAME,PartyMaster.PartyName,MIN_RKM(test_result.test_id) From rkm_entry ");
        sb.append(" inner join qad.test_result on test_result.test_id = rkm_entry.testno");
        sb.append(" Inner join RegularOrder on Regularorder.Rorderno=rkm_entry.Orderno ");
        sb.append(" Inner join PartyMaster on PartyMaster.Partycode= RegularOrder.PartyCode ");
        sb.append(" Inner join scm.ProcessingType on scm.ProcessingType.ProcessCode = regularorder.ProcessTypeCode ");
        sb.append(" Inner join Scm.Unit on Scm.Unit.UnitCode= RegularOrder.UnitCode ");
        sb.append(" Inner Join Rmixir on Rmixir.ORDNO = RegularOrder.ROrderNo and Rmixir.CorrectionMixing=0 ");
	sb.append(" Inner Join yarnm on yarnm.YSHCD = Rmixir.YARNSHADECODE ");
        sb.append(" Inner Join YarnCount on yarnCount.CountCode=RegularOrder.CountCode ");
        sb.append(" WHERE rkm_entry.ENTRYDATE >= ").append(SFrom).append(" AND rkm_entry.ENTRYDATE <= ").append(STo).append(" ");
        if(!sUnitCode.equals("All")){
            if(sUnitCode.equals("10"))
            {
                sb.append(" and Regularorder.UnitCode in(10,12) ");
            }
            else{
                sb.append(" and Regularorder.UnitCode= "+sUnitCode+"");
            }
        }
        if(!sShift.equals("All")){
            sb.append(" and rkm_entry.SHIFT_NAME= "+sShift+"");
        }
        if(sUnitCode.equals("All")){
               sb.append(" and (ProcessingType.OEStatus = 1 or ProcessingType.OEStatus = 0) ");
        }
        else{
            if(sProcessCode.equals("1")){
                sb.append(" and ProcessingType.OEStatus = 1 ");
            }
            else{
                sb.append(" and ProcessingType.OEStatus = 0 ");
            }
        }
        if(sOrderbyvalue.equals("1"))
        {
                sb . append(" order by yarncount.countname,rkm_entry.ENTRYDATE,rkm_entry.ORDERNO,TESTNO, Scm.Unit.UNITNAME ");
        }
        else if(sOrderbyvalue.equals("2"))
        {
                sb . append(" order by yarnm.yshnm,rkm_entry.ENTRYDATE,rkm_entry.ORDERNO,TESTNO, Scm.Unit.UNITNAME ");
        }   
        else if(sOrderbyvalue.equals("3")){
                sb . append(" order by yarncount.countname,yarnm.yshnm,rkm_entry.ENTRYDATE,rkm_entry.ORDERNO,TESTNO, Scm.Unit.UNITNAME ");                
        }
        else if(sOrderbyvalue.equals("4")){
                sb.append(" Order by rkm_entry.ENTRYDATE,rkm_entry.ORDERNO,TESTNO, Scm.Unit.UNITNAME ");
        }

        System.out.println("dd getRkmNew-->"+sb.toString());

        try{
            if(theProcessConnection==null){
                JDBCProcessConnection jdbc = JDBCProcessConnection.getJDBCConnection();
                theProcessConnection = jdbc.getConnection();
            }
            pst = theProcessConnection.prepareStatement(sb.toString());
            rst = pst.executeQuery();
            while(rst.next()){
                 details = new RkmDetails();
                 details.setOrderno(rst.getString(1));
                 details.setEntrydate(rst.getString(2));
                 details.setRkm(rst.getString(3));
                 details.setRkmcv(rst.getString(4));
                 details.setElgcv(rst.getString(5));
                 details.setElg(rst.getString(6));
                 details.setSys(rst.getString(7));
                 details.setTestno(rst.getString(8));
                 details.setRemarks(rst.getString(9));
                 details.setUnitname(common.parseNull(rst.getString(10)));
                 details.setSyscv(rst.getString(11));
                 details.setShadename(common.parseNull(rst.getString(12)));
                 details.setCountname(common.parseNull(rst.getString(13)));
                 details.setPartyname(common.parseNull(rst.getString(14)));
                 details.setMinRKM(common.parseNull(rst.getString(15)));
                 rkmList.add(details);
             }
             rst.close();
             pst.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return rkmList;
    }

    public java.util.List getUt3New(String sFromDate, String sToDate,String sUnitCode,String sProcessCode,String sShift,String sDeprtCode,String sOrderbyvalue)
    {
        java.util.List Ut3List = new java.util.ArrayList();
        PreparedStatement pst = null;
        ResultSet rst = null;
        StringBuilder sb = new StringBuilder();
        
            sb . append(" select deptcode,orderno,department.deptname,utthreetest.deptname, ");
            sb . append(" utthreetest.TESTID,utthreetest.TESTDATE, sum(UM)/count(UM) as um,");
            sb . append(" sum(CVM)/count(CVM) as cvm, (sum(THINMI30)/count(THINMI30))*2.5 as thin30, ");
            sb . append(" (sum(THINMI40)/count(THINMI40))*2.5 as thin40,(sum(THINMI50)/count(THINMI50))*2.5 as thin50, ");
            sb . append(" (sum(THICKPL50)/count(THICKPL50))*2.5 as thick50, (sum(NEPSPL200)/count(NEPSPL200))*2.5 as neps200, ");
            sb . append(" (sum(NEPSPL280)/count(NEPSPL280))*2.5 as neps280, oestatus,utthreetest.remarks,yarncount.countname, ");
            sb . append(" yarnm.yshnm,Scm.Unit.UNITNAME,utthreetest.CV_UM,(sum(THICKPL35)/count(THICKPL35))*2.5 as thick35, ");
            sb . append(" (sum(NEPSPL140)/count(NEPSPL140))*2.5 as neps140,PartyMaster.PartyName,RegularOrder.Weight ");
            sb . append(" from utthreetest ");
            sb . append(" inner join utthreetest_details on utthreetest_details.testid = utthreetest.testid ");
            sb . append(" and utthreetest_details.testdate=utthreetest.testdate  ");
            sb . append(" inner join regularorder on regularorder.rorderno = utthreetest.orderno ");
            sb . append(" Inner join PartyMaster on PartyMaster.Partycode= RegularOrder.PartyCode ");
            sb . append(" inner join scm.processingtype on scm.processingtype.processcode = regularorder.processtypecode ");
            sb . append(" inner join yarncount on yarncount.countcode = regularorder.countcode");
            sb . append(" inner join sampletoregular on sampletoregular.regularorderno = regularorder.rorderno");
            sb . append(" and correctionmixing = 0");
            sb . append(" inner join yarnm on yarnm.yshcd = sampletoregular.yarnshadecode");
            sb . append(" inner join department on department.suff_name = utthreetest.deptname");
            sb . append(" Inner join Scm.Unit on Scm.Unit.UnitCode= RegularOrder.UnitCode ");
            sb . append(" where utthreetest.testdate>= ? and utthreetest.testdate<= ? ");

            if(!sDeprtCode.equals("999")){

            sb . append("and department.DEPTCODE = ?");
            }
            if(!sUnitCode.equals("All")){

            if(sUnitCode.equals("10"))
            {
                    sb.append(" and Regularorder.UnitCode in(10,12) ");
            }
            else{
                    sb.append(" and Regularorder.UnitCode= "+sUnitCode+"");
            }
            }
            if(!sShift.equals("All")){
                sb.append(" and utthreetest.SHIFT= "+sShift+"");
            }
            if(sUnitCode.equals("All")){
               sb.append(" and (ProcessingType.OEStatus = 1 or ProcessingType.OEStatus = 0) ");
            }
            else{
            if(sProcessCode.equals("1")){
                sb.append(" and ProcessingType.OEStatus = 1 ");
            }
            else{
                sb.append(" and ProcessingType.OEStatus = 0 ");
            }
            }
            sb . append(" group by utthreetest.testid,utthreetest.testdate,orderno,utthreetest.deptname,");
            sb . append(" department.deptname,oestatus,deptcode,utthreetest.remarks,yarncount.countname, ");
            sb . append(" yarnm.yshnm,Scm.Unit.UNITNAME,utthreetest.CV_UM,PartyMaster.PartyName,RegularOrder.Weight ");
            if(sOrderbyvalue.equals("1"))
            {
                sb . append(" order by yarncount.countname,utthreetest.TESTDATE,department.deptname,utthreetest.testid,orderno,");
                sb . append(" utthreetest.deptname,utthreetest.remarks");
            }
            else if(sOrderbyvalue.equals("2"))
            {
                sb . append(" order by yarnm.yshnm, utthreetest.TESTDATE,department.deptname,utthreetest.testid,orderno,");
                sb . append(" utthreetest.deptname,utthreetest.remarks");
            }   
            else if(sOrderbyvalue.equals("3")){
                sb . append(" order by yarncount.countname,yarnm.yshnm,utthreetest.TESTDATE,department.deptname,utthreetest.testid,orderno,");
                sb . append(" utthreetest.deptname,utthreetest.remarks");
            }
            else if(sOrderbyvalue.equals("4")){
                sb . append(" order by utthreetest.TESTDATE,department.deptname,utthreetest.testid,orderno,");
                sb . append(" utthreetest.deptname,utthreetest.remarks");
            }
            System.out.println(sb.toString());

        try
        {
            if(theProcessConnection == null)
            {
                JDBCProcessConnection jdbc = JDBCProcessConnection.getJDBCConnection();
                theProcessConnection = jdbc.getConnection();
            }
            pst = theProcessConnection.prepareStatement(sb.toString());
            pst  . setString(1,sFromDate);
            pst  . setString(2,sToDate);
            if(!sDeprtCode.equals("999")){
            pst  . setString(3,sDeprtCode);
            }
            rst = pst.executeQuery();
            while(rst.next()){

            HashMap hm = new HashMap();

            hm.put("DEPTCODE"    ,common.parseNull((String)rst.getString(1)));
            hm.put("ORDERNO"     ,common.parseNull((String)rst.getString(2)));
            hm.put("DEPTNAME"    ,common.parseNull((String)rst.getString(3)));
            hm.put("SUFFDEPTNAME",common.parseNull((String)rst.getString(4)));
            hm.put("TESTID"      ,common.parseNull((String)rst.getString(5)));
            hm.put("TESTDATE"    ,common.parseNull((String)rst.getString(6)));
            hm.put("UM"          ,common.parseNull((String)rst.getString(7)));
            hm.put("CVM"         ,common.parseNull((String)rst.getString(8)));
            hm.put("THIN30"      ,common.parseNull((String)rst.getString(9)));
            hm.put("THIN40"      ,common.parseNull((String)rst.getString(10)));
            hm.put("THIN50"      ,common.parseNull((String)rst.getString(11)));
            hm.put("THICK50"     ,common.parseNull((String)rst.getString(12)));
            hm.put("NEPS200"     ,common.parseNull((String)rst.getString(13)));
            hm.put("NEPS280"     ,common.parseNull((String)rst.getString(14)));
            hm.put("OESTATUS"    ,common.parseNull((String)rst.getString(15)));
            hm.put("REMARKS"     ,common.parseNull((String)rst.getString(16)));
            hm.put("COUNTNAME"   ,common.parseNull((String)rst.getString(17)));
            hm.put("SHADE"       ,common.parseNull((String)rst.getString(18)));
            hm.put("UNITNAME"    ,common.parseNull((String)rst.getString(19)));
            hm.put("CVUM"        ,common.parseNull((String)rst.getString(20)));
            hm.put("THICK35"     ,common.parseNull((String)rst.getString(21)));
            hm.put("NEPS140"     ,common.parseNull((String)rst.getString(22)));
            hm.put("PARTYNAME"   ,common.parseNull((String)rst.getString(23)));
            hm.put("WEIGHT"      ,common.parseNull((String)rst.getString(24)));

            Ut3List.add(hm);
         }
            rst.close();
            pst.close();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return Ut3List;
    }
}