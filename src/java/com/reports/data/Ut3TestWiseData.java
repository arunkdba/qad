/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.data;

import com.jdbc.connection.JDBCProcessConnection;
import com.common.Common;

public class Ut3TestWiseData {
    java.sql.Connection theProcessConnection  = null;
    Common common = new Common();
    public Ut3TestWiseData() {
    }
    public java.util.List getUt3Details(String sFromDate,String sToDate,String sTestId){
         StringBuffer sb = new StringBuffer();
         java.util.List theList = new java.util.ArrayList();
         
        sb.append(" SELECT UTTHREETEST.TESTID,UTTHREETEST.TESTDATE,ORDERNO,DEPTNAME,TESTNO,UM, ");
        sb.append(" CVM,CVM1M,CVM3M,INDEXX,THINMI30,THINMI40,THINMI50,THICKPL35,THICKPL50, ");
        sb.append(" NEPSPL140,NEPSPL200,NEPSPL280,RELCOUNT FROM UTTHREETEST ");
        sb.append(" INNER JOIN UTTHREETEST_DETAILS ON UTTHREETEST_DETAILS.TESTID = UTTHREETEST.TESTID ");
        sb.append(" AND UTTHREETEST_DETAILS.TESTDATE = UTTHREETEST.TESTDATE ");
        sb.append(" WHERE UTTHREETEST.TESTID = '"+sTestId+"' And UTTHREETEST.TESTDATE >= "+sFromDate+" And UTTHREETEST.TESTDATE<= "+sToDate+" ");
        sb.append(" ORDER BY TESTID ");
        
         try{
            if(theProcessConnection==null){
                    JDBCProcessConnection  jdbc = JDBCProcessConnection.getJDBCConnection();
                    theProcessConnection = jdbc . getConnection();
             }
            java.sql.PreparedStatement pst = theProcessConnection.prepareStatement(sb.toString());
            java.sql.ResultSet rst = pst.executeQuery();

            java.sql.ResultSetMetaData rsm = rst.getMetaData();
            
            java.util.HashMap theMap = new java.util.HashMap();
            
            while(rst.next()){
                theMap = new java.util.HashMap();
                for(int i=0;i<rsm.getColumnCount();i++){
                    theMap.put(rsm.getColumnName(i+1), rst.getString(i+1));
                }
                theList.add(theMap);
            }
            rst.close();
            pst.close();
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return theList;
    }
    public java.util.List getUt3TestBase(String sFromDate,String sToDate,String sTestId){
         StringBuffer sb = new StringBuffer();         
         java.util.List theList = new java.util.ArrayList();
         
        sb.append(" SELECT DISTINCT UTTHREETEST.TESTID,UTTHREETEST.TESTDATE,ORDERNO,DEPTNAME,MACHNO,MACHSIDE,PASSAGE,ARTNO,FIBREASSEMBLY,FIBER, ");
        sb.append(" VDATA,TDATA,TESTS,SLOT,YARNTENSION,IMPERFECTIONS FROM UTTHREETEST ");
        sb.append(" INNER JOIN UTTHREETEST_DETAILS ON UTTHREETEST_DETAILS.TESTID = UTTHREETEST.TESTID ");
        sb.append(" AND UTTHREETEST_DETAILS.TESTDATE = UTTHREETEST.TESTDATE ");
        sb.append(" WHERE UTTHREETEST.TESTID = '"+sTestId+"' And UTTHREETEST.TESTDATE >= "+sFromDate+" And UTTHREETEST.TESTDATE<= "+sToDate+" ");
        sb.append(" ORDER BY TESTID ");

        System.out.println("TestBas-->"+sb.toString());
        
         try{
            if(theProcessConnection==null){
                    JDBCProcessConnection  jdbc = JDBCProcessConnection.getJDBCConnection();
                    theProcessConnection = jdbc . getConnection();
             }
            java.sql.PreparedStatement pst = theProcessConnection.prepareStatement(sb.toString());
            java.sql.ResultSet rst = pst.executeQuery();
            
            java.sql.ResultSetMetaData rsm = rst.getMetaData();
            
            java.util.HashMap theMap = new java.util.HashMap();
            
            while(rst.next()){
                theMap = new java.util.HashMap();
                for(int i=0;i<rsm.getColumnCount();i++){
                    theMap.put(rsm.getColumnName(i+1), rst.getString(i+1));
                }
                theList.add(theMap);
            }
            rst.close();
            pst.close();
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return theList;
    }    
    public java.util.List getUt3TestNo(String sFromDate,String sToDate){
        java.util.List thelist = new java.util.ArrayList();
        StringBuffer sb = new StringBuffer();

        sb.append(" select DISTINCT TESTID from UTTHREETEST where TESTDATE>="+sFromDate+" and TESTDATE<="+sToDate+" order by 1 ");

        //System.out.println("Test-->"+sb.toString());
        try
        {
            if(theProcessConnection==null){
                    JDBCProcessConnection  jdbc = JDBCProcessConnection.getJDBCConnection();
                    theProcessConnection = jdbc . getConnection();
             }
             java.sql.PreparedStatement pst = theProcessConnection.prepareStatement(sb.toString());
             java.sql.ResultSet rst = pst.executeQuery();
             while(rst.next()){
                 thelist.add(rst.getString(1));
             }
             rst.close();
             pst.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return thelist;
    }
}