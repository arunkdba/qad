package com.reports.pdf;

import com.common.Common;
import java.io.*;
import java.util.*;
import javax.swing.JOptionPane;
import com.drawing.wrapping.DrawingWrappingTestDto;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;

public class DrawingWrappingTestPDF {

    String SFile                    = "";
    Common common                   = new Common();
    
    String[]                         ColumnName,ColumnType,ColumnName1,ColumnType1,ColumnName2,ColumnType2,sBody;
    int[]                            iColumnWidth,iColumnWidth1,iColumnWidth2;

     private static Font catFont     = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
     private static Font redFont     = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);
     private static Font subFont     = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);

     private static Font bigBold     = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
     private static Font bigNormal   = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.NORMAL);

     private static Font mediumBold  = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
     private static Font mediumNormal= new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

     private static Font smallBold   = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.BOLD);
     private static Font smallNormal = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.NORMAL);

     private static Font tinyBold    = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
     private static Font tinyNormal  = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

     private static Font underBold   = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.UNDERLINE);
    
     int    iWidth1[]   = {20,25,25,20,50,25,40,20,30,20,20,20,20,20,20,20,20,20,20,20};
     
     Document   document;
     PdfPTable  table1;
     PdfWriter  writer = null;
     double     dprocess =0;
     String     SProcess="",SRh="";

     
    public void createPDFFile(){
     try{
         
        SFile       =   "DrawingWrappingTestPDF.pdf";
        SFile       =   common.getPrintPath()+SFile;    
         System.out.println("FileName-->"+SFile);    
        document    =   new Document(PageSize.LEGAL.rotate());
        //PdfWriter   .   getInstance(document, new FileOutputStream(SFile));
        writer      =  PdfWriter.getInstance(document, new FileOutputStream(SFile));
        TableHeader event   = new TableHeader();
        writer      .   setPageEvent(event);
        document    .   open();

        table1      =   new PdfPTable(20);
        table1      .   setWidths(iWidth1);
        table1      .   setSpacingAfter(3);
        table1      .   setWidthPercentage(100);
        table1      .   setHeaderRows(6);
               
       }
        catch (Exception e){
           e.printStackTrace();
        }
    }
    
    public void setTestBase(String SDate,String SToDate, String SUnit,String SShift) {
        String Str1 = "Company    : AMARJOTHI SPINNING MILLS LIMITED\n";
        String Str2 = "Document   : DRAWING HANK DETAILS FROM ";
        String Str3 = "Unit             :  " + SUnit + "\n";
        String Str4 = "Department   :  QC  \n";
        String Str5 = "Shift            :  " + SShift + "\n";
        
        try {
            
            AddCellIntoTable(Str1, table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,25,20f,0,0,0,0,bigBold );
            AddCellIntoTable(Str2+common.parseDate(SDate)+" TO  "+common.parseDate(SToDate), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,25,20f,0,0,0,0,bigBold );
            AddCellIntoTable(Str3, table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,25,20f,0,0,0,0,bigBold );
            AddCellIntoTable(Str4, table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,25,20f,0,0,0,0,bigBold );
            AddCellIntoTable(Str5, table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,25,20f,0,0,0,0,bigBold );
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void setHead(){
        try{
            AddCellIntoTable("S.No", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddCellIntoTable("TestNo", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddCellIntoTable("Machine", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddCellIntoTable("Passage", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddCellIntoTable("Order No", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddCellIntoTable("Order Weight", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            
            AddCellIntoTable("Shade", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddCellIntoTable("Count", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            
            AddCellIntoTable("Std.Hank", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddCellIntoTable("Hank1", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddCellIntoTable("Hank2", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddCellIntoTable("Hank3", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddCellIntoTable("Avg", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddCellIntoTable("R.S.B.", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddCellIntoTable("C.P. Wheel", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddCellIntoTable("Draft", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddCellIntoTable("No.of. Ends", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddCellIntoTable("Time", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddCellIntoTable("Changes", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddCellIntoTable("Remark", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
        }
        catch(Exception Ex){
                Ex.printStackTrace();
        }
    }
    
    public void printPDFData(java.util.List TestList){
        try{
            for (int i = 0; i < TestList.size(); i++) {
                    DrawingWrappingTestDto drawingWrappingTestData = (DrawingWrappingTestDto)TestList.get(i);
                
                    AddCellIntoTable(String.valueOf(drawingWrappingTestData.getsSino()), table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,25f,5,3,8,3, tinyNormal );
                    AddCellIntoTable(String.valueOf(drawingWrappingTestData.getsTestno()), table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,25f,5,3,8,3, tinyNormal );
                    AddCellIntoTable(String.valueOf(drawingWrappingTestData.getSmach_name()), table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,25f,5,3,8,3, tinyNormal );
                    AddCellIntoTable(String.valueOf(drawingWrappingTestData.getsProcessLevel()), table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,25f,5,3,8,3, tinyNormal );
                    AddCellIntoTable(String.valueOf(drawingWrappingTestData.getsOrderNo()+"("+drawingWrappingTestData.getsMixno()+")"), table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,25f,5,3,8,3, tinyNormal );
                    AddCellIntoTable(String.valueOf(drawingWrappingTestData.getsOrderWeight()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,25f,5,3,8,3, tinyNormal );
                    AddCellIntoTable(String.valueOf(drawingWrappingTestData.getsShade()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,25f,5,3,8,3, tinyNormal );
                    AddCellIntoTable(String.valueOf(drawingWrappingTestData.getsCountName()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,25f,5,3,8,3, tinyNormal );
                    AddCellIntoTable(String.valueOf(drawingWrappingTestData.getsStdHank()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,25f,5,3,8,3, tinyNormal );
                    AddCellIntoTable(String.valueOf(drawingWrappingTestData.getsHank1()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,25f,5,3,8,3, tinyNormal );
                    AddCellIntoTable(String.valueOf(drawingWrappingTestData.getsHank2()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,25f,5,3,8,3, tinyNormal );
                    AddCellIntoTable(String.valueOf(drawingWrappingTestData.getsHank3()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,25f,5,3,8,3, tinyNormal );
                    AddCellIntoTable(String.valueOf(drawingWrappingTestData.getsHankAvg()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,25f,5,3,8,3, tinyNormal );
                    AddCellIntoTable(String.valueOf(drawingWrappingTestData.getsRsb()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,25f,5,3,8,3, tinyNormal );
                    AddCellIntoTable(String.valueOf(drawingWrappingTestData.getsCpWheel()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,25f,5,3,8,3, tinyNormal );
                    AddCellIntoTable(String.valueOf(drawingWrappingTestData.getsDraft()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,25f,5,3,8,3, tinyNormal );
                    AddCellIntoTable(String.valueOf(drawingWrappingTestData.getsEndno()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,25f,5,3,8,3, tinyNormal );
                    AddCellIntoTable(String.valueOf(drawingWrappingTestData.getSentrydate()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,25f,5,3,8,3, tinyNormal );
                    AddCellIntoTable(String.valueOf(drawingWrappingTestData.getSstatus()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,25f,5,3,8,3, tinyNormal );
                    AddCellIntoTable(String.valueOf(drawingWrappingTestData.getsRemark()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,25f,5,3,8,3, tinyNormal );
            }
                    
            document.add(table1);
            document.close();
        }
        catch(Exception ex){
            ex.printStackTrace();
            System.out.println("PrintData-->"+ex);
        }
    }
    
     private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      } 
    
    private void AddHeadCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iRowSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setRowspan(iRowSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
    
    private void AddCellIntoTable1(String Str,PdfPTable table,int iHorizontal,int iVertical,int iRowSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setRowspan(iRowSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
	  private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          //c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
	 private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,int iRowSpan)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          //c1.setRowspan(iRowSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
	  private void AddCellTable(Image image,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder)
      {
          PdfPCell c1 = new PdfPCell();
		  c1.addElement(new Chunk(image, 5, -5));
          //c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);

		  // image.scalePercent(90f);
      	  // paragraph.add(new Chunk(image, -1f, 1f));
      }
	 private void AddCellIntoTable(Image image,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder)
      {
          PdfPCell c1 = new PdfPCell();
		  //c1.addElement(new Chunk(image, 5, -5));
	  c1.addElement(image);
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);

      }
     private void AddCellIntoTable(double dValue,int iRound,int iRad,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder)
      {
           Double DValue = new Double(dValue);
           String Str;
           if (DValue.isNaN() || dValue ==0)
           {
               Str="";
           }
           else
           {
               Str=(common.Rad(common.getRound(dValue,iRound),iRad));
           }

          PdfPCell c1 = new PdfPCell(new Phrase(Str));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);

      }
     
     public void AddCellIntoTable2(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }
    
     class TableHeader extends PdfPageEventHelper {
        String header;
        PdfTemplate total;
        public void setHeader(String header) {
            this.header = header;
      }
      public void onOpenDocument(PdfWriter writer, Document document) {
            try{
            total = writer.getDirectContent().createTemplate(30, 16);
            }
            catch(Exception ex){
            }
      }

        public void onEndPage(PdfWriter writer, Document document) {
            PdfPTable table = new PdfPTable(3);
            try {
                table.setWidths(new int[]{24, 24, 2});
                table.setTotalWidth(520);
                table.setLockedWidth(true);
                table.getDefaultCell().setFixedHeight(20);
                table.getDefaultCell().setBorder(0);
                table.addCell(header);
                table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell("");
                table.addCell("");
                table.addCell("");
                table.addCell(String.format("Page %d of", writer.getPageNumber()));
                PdfPCell cell = new PdfPCell(Image.getInstance(total));
                cell.setBorder(0);
                table.addCell(cell);
                table.writeSelectedRows(0, -1, 10, 50, writer.getDirectContent());
            }
            catch(DocumentException de) {
                throw new ExceptionConverter(de);
            }
        }
        public void onCloseDocument(PdfWriter writer, Document document) {
          try{  
            ColumnText.showTextAligned(total, Element.ALIGN_LEFT,
                    new Phrase(String.valueOf(writer.getPageNumber() - 1)),
                    2, 2, 0);
             }
          catch(Exception ex){
              
          }
        }
    }
    
}
