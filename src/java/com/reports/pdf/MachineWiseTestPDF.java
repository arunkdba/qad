package com.reports.pdf;

import com.common.Common;
import java.io.*;
import java.util.*;
import javax.swing.JOptionPane;
import com.reports.classes.CoEffVarient;
import com.reports.classes.MinAndMaxValue;
import com.reports.data.MachineWiseTestData;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;

public class MachineWiseTestPDF {
    
    String SFile                    = "";
    Common common                   = new Common();
    MachineWiseTestData      Srh    = new MachineWiseTestData();
    CoEffVarient             cev    = new CoEffVarient();
    MinAndMaxValue           mval   = new MinAndMaxValue();
    double dprocess =0;
    String SProcess="",SRh="",sExtCnt="";
    
    String[]                         ColumnName,ColumnType,ColumnName1,ColumnType1,ColumnName2,ColumnType2,sBody;
    int[]                            iColumnWidth,iColumnWidth1,iColumnWidth2;

     private static Font catFont     = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
     private static Font redFont     = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);
     private static Font subFont     = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);

     private static Font bigBold     = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
     private static Font bigNormal   = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);

     private static Font mediumBold  = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
     private static Font mediumNormal= new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

     private static Font smallBold   = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.BOLD);
     private static Font smallNormal = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.NORMAL);

     private static Font tinyBold    = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.BOLD);
     private static Font tinyNormal  = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.NORMAL);

     private static Font underBold   = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.UNDERLINE);
    
     int    iWidth1[]   = {20,50,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,30,20,40,30,20,20};
     
     Document   document;
     PdfPTable  table1;
     PdfWriter  writer = null;

     
    public void createPDFFile(){
     try{
         
        SFile       =   "MachineWiseTestPDF.pdf";
        SFile       =   common.getPrintPath()+SFile;    
         System.out.println("FileName-->"+SFile);
            
        document    =   new Document(PageSize.LEGAL.rotate());
        //PdfWriter   .   getInstance(document, new FileOutputStream(SFile));
        writer      =  PdfWriter.getInstance(document, new FileOutputStream(SFile));
        TableHeader event   = new TableHeader();
        writer      .   setPageEvent(event);
        document    .   open();

        table1      =   new PdfPTable(25);
        table1      .   setWidths(iWidth1);
        table1      .   setSpacingAfter(3);
        table1      .   setWidthPercentage(100);
        table1      .   setHeaderRows(7);
               
       }
        catch (Exception e){
           e.printStackTrace();
        }
    }
    
    public void setTestBase(String SDate, String SUnit, String SProcess,String sExtCount) {
        String Str1 = "Company    : AMARJOTHI SPINNING MILLS LIMITED\n";
        String Str2 = "Document   : SPINNING / OE WRAPPING TEST REPORT AS ON ";
        String Str3 = "Unit            :  " + SUnit + "\n";
        String Str4 = "Process       :  " + SProcess + "\n";
        String Str5 = "ExactCount   :  " + sExtCount + "\n";
        
        try {
            
            AddCellIntoTable(Str1, table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,25,20f,0,0,0,0,bigBold );
            AddCellIntoTable(Str2+common.parseDate(SDate), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,25,20f,0,0,0,0,bigBold );
            AddCellIntoTable(Str3, table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,25,20f,0,0,0,0,bigBold );
            AddCellIntoTable(Str4, table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,25,20f,0,0,0,0,bigBold );
            if(!sExtCount.equals("All")){
            AddCellIntoTable(Str5, table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,25,20f,0,0,0,0,bigBold );
            }
            else
             AddCellIntoTable("", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,25,20f,0,0,0,0,bigBold );
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void setHead(String SDate,String STdate){
	
        try
        {
            AddHeadCellIntoTable("S.No", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("Machine", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("Actual Count", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("For", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,30f,5,3,8,3, tinyBold );
            
            AddCellIntoTable("Count", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,5,30f,5,3,8,3, tinyBold );
            
            AddHeadCellIntoTable("CntAvg", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("CntCV%", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("StrAvg", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("StrCV%", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("CspAvg", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("CspCV%", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,30f,5,3,8,3, tinyBold );
            
            AddHeadCellIntoTable("RHCVal", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("Change Adv", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("Cp", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("TPI", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("O.No", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("Weight", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("Shade", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("Pump/  Cheese", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("Draft", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("B.D", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,30f,5,3,8,3, tinyBold );
            
            AddCellIntoTable("1", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddCellIntoTable("2", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddCellIntoTable("3", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddCellIntoTable("4", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddCellIntoTable("5", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            
            
        }
        catch(Exception Ex){
                Ex.printStackTrace();
        }
    }
    
    public void printPDFData(java.util.List TestList){
        try{
            
                double dtotcount =0;
                double dtotstr = 0;
                double dtotcsp = 0;
                double dCnt1Total = 0,dCnt2Total = 0,dCnt3Total = 0,dCnt4Total = 0,dCnt5Total = 0;                
                double dCntAvgTotal = 0,dStrAvgTotal=0,dCspAvgTotal=0;
                double dCntCvTotal =0,dStrCvTotal=0,dCspCvTotal=0;
                double dRhValueTotal=0;
                int incount =0;
                int instr =0;
                int incsp=0;
                int i = 0;
                int iCnt1Total = 0,iCnt2Total = 0,iCnt3Total = 0,iCnt4Total = 0,iCnt5Total = 0;
                String sCountVar="";
                String sStar = "";
                java.util.List arrTotCntList = new ArrayList();
                java.util.List arrTotStrList = new ArrayList();
                java.util.List arrTotCspList = new ArrayList();

                java.util.List<Double> arrTotCnt= new ArrayList<Double>();
                java.util.List<Double> arrTotStr= new ArrayList<Double>();
                java.util.List<Double> arrTotCsp= new ArrayList<Double>();
                Srh.getRhFactors();
                
               for (i=0; i < TestList.size(); i++) {
                    com.reports.classes.SpinningWrappingTestDetails Td = (com.reports.classes.SpinningWrappingTestDetails) TestList.get(i);
                    sCountVar = common.parseNull(Td.getCountvar());
                    
                    int iCnt = 0;
                    int iStr = 0;
                    int iCsp = 0;
                    double drhStr=0,drhCnt=0;
                    double dcnt1 = common.toDouble(Td.getCnt1());
                    double dcnt2 = common.toDouble(Td.getCnt2());
                    double dcnt3 = common.toDouble(Td.getCnt3());
                    double dcnt4 = common.toDouble(Td.getCnt4());
                    double dcnt5 = common.toDouble(Td.getCnt5());
                    ArrayList<String> arrlistcnt = new ArrayList<String>();
                    if (dcnt1 != 0) {
                        iCnt += 1;
                        iCnt1Total +=1;
                        dCnt1Total += dcnt1;
                        arrlistcnt.add(Td.getCnt1());
                        arrTotCntList.add(Td.getCnt1());
                        arrTotCnt.add(dcnt1);
                    }
                    if (dcnt2 != 0) {
                        iCnt += 1;
                        iCnt2Total +=1;
                        dCnt2Total += dcnt2;
                        arrlistcnt.add(Td.getCnt2());
                        arrTotCntList.add(Td.getCnt2());
                        arrTotCnt.add(dcnt2);
                    }
                    if (dcnt3 != 0) {
                        iCnt += 1;
                        iCnt3Total +=1;
                        dCnt3Total += dcnt3;
                        arrlistcnt.add(Td.getCnt3());
                        arrTotCntList.add(Td.getCnt3());
                        arrTotCnt.add(dcnt3);
                    }
                    if (dcnt4 != 0) {
                        iCnt += 1;
                        iCnt4Total +=1;
                        dCnt4Total += dcnt4;
                        arrlistcnt.add(Td.getCnt4());
                        arrTotCntList.add(Td.getCnt4());
                        arrTotCnt.add(dcnt4);
                    }
                    if (dcnt5 != 0) {
                        iCnt += 1;
                        iCnt5Total +=1;
                        dCnt5Total += dcnt5;
                        arrlistcnt.add(Td.getCnt5());
                        arrTotCntList.add(Td.getCnt5());
                        arrTotCnt.add(dcnt5);
                    }
                    double dCntAvg = (dcnt1 + dcnt2 + dcnt3 + dcnt4 + dcnt5) / iCnt;
                    dtotcount += (dcnt1 + dcnt2 + dcnt3 + dcnt4 + dcnt5);
                    incount += iCnt;
                    String[] CntArray = (String[]) arrlistcnt.toArray(new String[arrlistcnt.size()]);

                    double dsrten1 = common.toDouble(Td.getStrength1());
                    double dsrten2 = common.toDouble(Td.getStrength2());
                    double dsrten3 = common.toDouble(Td.getStrength3());
                    double dsrten4 = common.toDouble(Td.getStrength4());
                    double dsrten5 = common.toDouble(Td.getStrength5());
                    
                    ArrayList<String> arrliststr = new ArrayList<String>();
                    if (dsrten1 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength1());
                        arrTotStrList.add(Td.getStrength1());
                        arrTotStr.add(dsrten1);
                    }
                    if (dsrten2 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength2());
                        arrTotStrList.add(Td.getStrength2());
                        arrTotStr.add(dsrten2);
                    }
                    if (dsrten3 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength3());
                        arrTotStrList.add(Td.getStrength3());
                        arrTotStr.add(dsrten3);
                    }
                    if (dsrten4 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength4());
                        arrTotStrList.add(Td.getStrength4());
                        arrTotStr.add(dsrten4);
                    }
                    if (dsrten5 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength5());
                        arrTotStrList.add(Td.getStrength5());
                        arrTotStr.add(dsrten5);
                    }
                    double dStrenAvg = (dsrten1 + dsrten2 + dsrten3 + dsrten4 + dsrten5) / iStr;
                    dtotstr += (dsrten1 + dsrten2 + dsrten3 + dsrten4 + dsrten5);
                    instr += iStr;
                    String[] StrArray = (String[]) arrliststr.toArray(new String[arrliststr.size()]);

                    double dcsp1 = common.toDouble(Td.getCsp1());
                    double dcsp2 = common.toDouble(Td.getCsp2());
                    double dcsp3 = common.toDouble(Td.getCsp3());
                    double dcsp4 = common.toDouble(Td.getCsp4());
                    double dcsp5 = common.toDouble(Td.getCsp5());
                    ArrayList<String> arrlistcsp = new ArrayList<String>();
                    if (dcsp1 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp1());
                        arrTotCspList.add(Td.getCsp1());
                        arrTotCsp.add(dcsp1);
                    }
                    if (dcsp2 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp2());
                        arrTotCspList.add(Td.getCsp2());
                        arrTotCsp.add(dcsp2);
                    }
                    if (dcsp3 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp3());
                        arrTotCspList.add(Td.getCsp3());
                        arrTotCsp.add(dcsp3);
                    }
                    if (dcsp4 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp4());
                        arrTotCspList.add(Td.getCsp4());
                        arrTotCsp.add(dcsp4);
                    }
                    if (dcsp5 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp5());
                        arrTotCspList.add(Td.getCsp5());
                        arrTotCsp.add(dcsp5);
                    }
                    double dCspAvg = (dcsp1 + dcsp2 + dcsp3 + dcsp4 + dcsp5) / iCsp;
                    dtotcsp += (dcsp1 + dcsp2 + dcsp3 + dcsp4 + dcsp5);
                    incsp += iCsp;
                    String[] CspArray = (String[]) arrlistcsp.toArray(new String[arrlistcsp.size()]);

                    if(dStrenAvg>0){
                    double dc1 = common.toDouble(common.getRound(dCntAvg,2));
                    double ds1 = common.toDouble(common.getRound(dStrenAvg,2));
                    double dc2 = (dprocess-1);
                    double dc1s1 = common.toDouble(common.getRound(dc1*ds1,2));
                    double dc2c1 = common.toDouble(common.getRound(13*(dc2-dc1),2));
                    double dupper = dc1s1-dc2c1;
                    drhStr = dupper/dc2;

                   /* System.out.println("Str Rh");
                    System.out.println("Proceee Value-->"+dprocess);
                    System.out.println("C1 Value-->"+dc1);
                    System.out.println("C2-->"+dc2);
                    System.out.println("S1 Value -->"+ds1);
                    System.out.println("C1*S1 Value-->"+dc1s1);
                    System.out.println("13 * (c2-c1)-->"+dc2c1);
                    System.out.println("C1 S1 - 13 * (c2-c1)-->"+dupper);*/
                    }
                    if(dCntAvg>0){
                    double drh = Srh.getrh(SRh);
                    drhCnt = (dCntAvg*drh);}

                   /* System.out.println("cnt Rh");
                    System.out.println("Rh Value-->"+drh);
                    System.out.println("Avg cnt -->"+dCntAvg);
                    System.out.println("dCntAvg* drh-->"+drhCnt);*/
                    if(sCountVar.equals("2")){
                       sStar = "*" ;
                    }
                    else{
                        sStar=" ";
                    }
                    dCntAvgTotal += common.toDouble(common.getRound(dCntAvg,2));
                    dStrAvgTotal += common.toDouble(common.getRound(dStrenAvg,2));
                    dCspAvgTotal += common.toDouble(common.getRound(dCspAvg,2));
                    dCntCvTotal  += common.toDouble(common.getRound(cev.getCoEffVant(CntArray), 2));
                    dStrCvTotal  += common.toDouble(common.getRound(cev.getCoEffVant(StrArray), 2));
                    dCspCvTotal  += common.toDouble(common.getRound(cev.getCoEffVant(CspArray), 2));
                    dRhValueTotal+= common.toDouble(common.getRound(drhCnt,2));
                    
                    int row = 0;
                    row = i+1;
                          
                    AddCellIntoTable(String.valueOf(row), table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                    AddCellIntoTable(common.parseNull(Td.getMechinename()), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                    AddCellIntoTable(common.parseNull(Td.getCount()), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                    AddCellIntoTable("Cnt", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                    AddCellIntoTable(common.parseNull(Td.getCnt1()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                    AddCellIntoTable(common.parseNull(Td.getCnt2()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                    AddCellIntoTable(common.parseNull(Td.getCnt3()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                    AddCellIntoTable(common.parseNull(Td.getCnt4()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                    AddCellIntoTable(common.parseNull(Srh.removeNull(Td.getCnt5())), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                    AddCellIntoTable(common.getRound(String.valueOf(dCntAvg),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                    AddCellIntoTable(common.getRound(String.valueOf(cev.getCoEffVant(CntArray)),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                    AddCellIntoTable(common.getRound(String.valueOf(dStrenAvg),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                    AddCellIntoTable(common.getRound(String.valueOf(cev.getCoEffVant(StrArray)),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                    AddCellIntoTable(common.getRound(String.valueOf(dCspAvg),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                    AddCellIntoTable(common.getRound(String.valueOf(cev.getCoEffVant(CspArray)),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                    AddCellIntoTable(common.getRound(String.valueOf(drhCnt),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                    AddCellIntoTable(common.parseNull(Td.getTesttype()), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );    
                    AddCellIntoTable(common.parseNull(Td.getCpwheel()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );    
                    AddCellIntoTable(common.parseNull(Td.getTpi()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );    
                    AddCellIntoTable(common.parseNull(Td.getOrderno()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );    
                    AddCellIntoTable(common.parseNull(Td.getWeight()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );    
                    AddCellIntoTable(common.parseNull(Td.getShadename()), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );    
                    AddCellIntoTable(common.parseNull(Td.getPumpclr()), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );    
                    AddCellIntoTable(common.parseNull(Td.getDraft()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );    
                    AddCellIntoTable(common.parseNull(Td.getBdraft()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );    
                
               }
               
                    AddCellIntoTable("Average", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,4,20f,5,3,8,3,bigNormal );
                    AddCellIntoTable(common.getRound((dCnt1Total/iCnt1Total),3), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                    AddCellIntoTable(common.getRound((dCnt2Total/iCnt2Total),3), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                    AddCellIntoTable(common.getRound((dCnt3Total/iCnt3Total),3), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                    AddCellIntoTable(common.getRound((dCnt4Total/iCnt4Total),3), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                    AddCellIntoTable(common.getRound((dCnt5Total/iCnt5Total),3), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                    AddCellIntoTable(common.getRound((dCntAvgTotal/i),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                    AddCellIntoTable(common.getRound((dCntCvTotal/i),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                    AddCellIntoTable(common.getRound((dStrAvgTotal/i),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                    AddCellIntoTable(common.getRound((dStrCvTotal/i),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                    AddCellIntoTable(common.getRound((dCspAvgTotal/i),0), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                    AddCellIntoTable(common.getRound((dCspCvTotal/i),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                    AddCellIntoTable(common.getRound((dRhValueTotal/i),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                    AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );    
                    AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );    
                    AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );    
                    AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );    
                    AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );    
                    AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );    
                    AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );    
                    AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );    
                    AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );    
                    
               document.add(table1);
               document.close();
            
        }
        catch(Exception ex){
            ex.printStackTrace();
            System.out.println("PrintDAta-->"+ex);
        }
    }
    
    private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      } 
    
    private void AddHeadCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iRowSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setRowspan(iRowSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
    
    private void AddCellIntoTable1(String Str,PdfPTable table,int iHorizontal,int iVertical,int iRowSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setRowspan(iRowSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
	  private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          //c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
	 private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,int iRowSpan)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          //c1.setRowspan(iRowSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
	  private void AddCellTable(Image image,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder)
      {
          PdfPCell c1 = new PdfPCell();
		  c1.addElement(new Chunk(image, 5, -5));
          //c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);

		  // image.scalePercent(90f);
      	  // paragraph.add(new Chunk(image, -1f, 1f));
      }
	 private void AddCellIntoTable(Image image,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder)
      {
          PdfPCell c1 = new PdfPCell();
		  //c1.addElement(new Chunk(image, 5, -5));
	  c1.addElement(image);
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);

      }
     private void AddCellIntoTable(double dValue,int iRound,int iRad,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder)
      {
           Double DValue = new Double(dValue);
           String Str;
           if (DValue.isNaN() || dValue ==0)
           {
               Str="";
           }
           else
           {
               Str=(common.Rad(common.getRound(dValue,iRound),iRad));
           }

          PdfPCell c1 = new PdfPCell(new Phrase(Str));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);

      }
     
     public void AddCellIntoTable2(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }
    
     class TableHeader extends PdfPageEventHelper {
        String header;
        PdfTemplate total;
        public void setHeader(String header) {
            this.header = header;
      }
      public void onOpenDocument(PdfWriter writer, Document document) {
            try{
            total = writer.getDirectContent().createTemplate(30, 16);
            }
            catch(Exception ex){
            }
      }

        public void onEndPage(PdfWriter writer, Document document) {
            PdfPTable table = new PdfPTable(3);
            try {
                table.setWidths(new int[]{24, 24, 2});
                table.setTotalWidth(520);
                table.setLockedWidth(true);
                table.getDefaultCell().setFixedHeight(20);
                table.getDefaultCell().setBorder(0);
                table.addCell(header);
                table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell("");
                table.addCell("");
                table.addCell("");
                table.addCell(String.format("Page %d of", writer.getPageNumber()));
                PdfPCell cell = new PdfPCell(Image.getInstance(total));
                cell.setBorder(0);
                table.addCell(cell);
                table.writeSelectedRows(0, -1, 10, 50, writer.getDirectContent());
            }
            catch(DocumentException de) {
                throw new ExceptionConverter(de);
            }
        }
        public void onCloseDocument(PdfWriter writer, Document document) {
          try{  
            ColumnText.showTextAligned(total, Element.ALIGN_LEFT,
                    new Phrase(String.valueOf(writer.getPageNumber() - 1)),
                    2, 2, 0);
             }
          catch(Exception ex){
              
          }
        }
    }   
}
