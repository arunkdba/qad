package com.reports.pdf;
import com.common.Common;
import java.io.*;
import java.util.*;
import javax.swing.JOptionPane;
import com.reports.classes.CoEffVarient;
import com.reports.classes.MinAndMaxValue;
import com.reports.data.CVReportData;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;

public class MagSpinningWrapingTestAbstractPDF {

    String          SFile           = "";
    Common          common          = new Common();
    CVReportData    Srh             = new CVReportData();
    CoEffVarient    cev             = new CoEffVarient();
    MinAndMaxValue  mval            = new MinAndMaxValue();
    
    String[]                         ColumnName,ColumnType,ColumnName1,ColumnType1,ColumnName2,ColumnType2,sBody;
    int[]                            iColumnWidth,iColumnWidth1,iColumnWidth2;

     private static Font catFont     = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
     private static Font redFont     = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);
     private static Font subFont     = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);

     private static Font bigBold     = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
     private static Font bigNormal   = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.NORMAL);

     private static Font mediumBold  = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
     private static Font mediumNormal= new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

     private static Font smallBold   = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.BOLD);
     private static Font smallNormal = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.NORMAL);

     private static Font tinyBold    = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
     private static Font tinyNormal  = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.NORMAL);

     private static Font underBold   = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.UNDERLINE);
    
     int    iWidth1[]   = {25,30,25,25,50,65,20,30,30,40,30,30,30,30,45};
     
     Document   document;
     PdfPTable  table1,table2;
     PdfWriter  writer = null;
     double     dprocess =0;
     String     SProcess="",SRh="";

    public void createPDFFile(){
     try{
         
        SFile       =   "MagSpinningTestAbstract.pdf";
        SFile       =   common.getPrintPath()+SFile;    
        document    =   new Document(PageSize.A4.rotate());
        //PdfWriter   .   getInstance(document, new FileOutputStream(SFile));
        writer      =  PdfWriter.getInstance(document, new FileOutputStream(SFile));
        TableHeader event   = new TableHeader();
        writer      .   setPageEvent(event);
        document    .   open();

        table1      =   new PdfPTable(15);
        table1      .   setWidths(iWidth1);
        table1      .   setSpacingAfter(3);
        table1      .   setWidthPercentage(100);
        //table1      .   setHeaderRows(5);
               
       }
        catch (Exception e){
           e.printStackTrace();
        }
    }
    
    public void setTestBase(String SDate) {
       
        String Str1 = "Company    : AMARJOTHI SPINNING MILLS LIMITED\n";
        String Str2 = "Document   : MAG - SPINNING WRAPPING ABSTRACT REPORT As On  ";
        
        try {
            
            AddCellIntoTable(Str1, table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,25,25f,0,0,0,0,bigBold );
            AddCellIntoTable(Str2+common.parseDate(SDate), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,25,25f,0,0,0,0,bigBold );
            
           // document.add(table1);
           // document.close();
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void printData(java.util.List TestList){
	
        try
        {
            
            for (int i = 0; i < TestList.size(); i++) {
                 java.util.HashMap theMap = (java.util.HashMap)TestList.get(i);
            
            AddCellIntoTable(common.parseNull((String) theMap.get("TESTID")), table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.parseDate((String) theMap.get("ENTRYDATE"))), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull((String) theMap.get("SHIFT")), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull((String) theMap.get("NOMCOUNT")), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull((String) theMap.get("MACHINE_NAME")), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull((String) theMap.get("FRAME")), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String) theMap.get("TPI"),3)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String) theMap.get("DRAFT"),3)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String) theMap.get("COUNTAVG"),3)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String) theMap.get("STRAVG"),3)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String) theMap.get("CSPAVG"),0)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull((String) theMap.get("COUNTCV")), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull((String) theMap.get("COUNTRANGE")), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull((String) theMap.get("RKMVAL")), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull((String) theMap.get("OPERATOR")), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            
            }
            
            document.add(table1);
            document.close();
        }
        catch(Exception Ex){
                Ex.printStackTrace();
        }
    }
    
    public void setHead(){
	
        try
        {
            AddCellIntoTable("Test No", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddCellIntoTable("Entry Date", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddCellIntoTable("Shift", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddCellIntoTable("Nom Count", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddCellIntoTable("Order No", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddCellIntoTable("Machine", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddCellIntoTable("TPI", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddCellIntoTable("Draft", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddCellIntoTable("Cont", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddCellIntoTable("Strength", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddCellIntoTable("CSP", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddCellIntoTable("Count", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddCellIntoTable("Count Range", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddCellIntoTable("RKM Value", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddCellIntoTable("Operator", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
           
        }
        catch(Exception Ex){
                Ex.printStackTrace();
        }
    }
    
private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      } 
    
    private void AddHeadCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iRowSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setRowspan(iRowSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
    
    private void AddCellIntoTable1(String Str,PdfPTable table,int iHorizontal,int iVertical,int iRowSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setRowspan(iRowSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
	  private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          //c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
	 private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,int iRowSpan)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          //c1.setRowspan(iRowSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
	  private void AddCellTable(Image image,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder)
      {
          PdfPCell c1 = new PdfPCell();
		  c1.addElement(new Chunk(image, 5, -5));
          //c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);

		  // image.scalePercent(90f);
      	  // paragraph.add(new Chunk(image, -1f, 1f));
      }
	 private void AddCellIntoTable(Image image,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder)
      {
          PdfPCell c1 = new PdfPCell();
		  //c1.addElement(new Chunk(image, 5, -5));
	  c1.addElement(image);
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);

      }
     private void AddCellIntoTable(double dValue,int iRound,int iRad,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder)
      {
           Double DValue = new Double(dValue);
           String Str;
           if (DValue.isNaN() || dValue ==0)
           {
               Str="";
           }
           else
           {
               Str=(common.Rad(common.getRound(dValue,iRound),iRad));
           }

          PdfPCell c1 = new PdfPCell(new Phrase(Str));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);

      }
     
     public void AddCellIntoTable2(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }
    
     class TableHeader extends PdfPageEventHelper {
        String header;
        PdfTemplate total;
        public void setHeader(String header) {
            this.header = header;
      }
      public void onOpenDocument(PdfWriter writer, Document document) {
            try{
            total = writer.getDirectContent().createTemplate(30, 16);
            }
            catch(Exception ex){
            }
      }

      public void onEndPage(PdfWriter writer, Document document) {
            PdfPTable table = new PdfPTable(3);
            try {
                table.setWidths(new int[]{24, 24, 2});
                table.setTotalWidth(520);
                table.setLockedWidth(true);
                table.getDefaultCell().setFixedHeight(20);
                table.getDefaultCell().setBorder(0);
                table.addCell(header);
                table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell("");
                table.addCell("");
                table.addCell("");
                table.addCell(String.format("Page %d of", writer.getPageNumber()));
                PdfPCell cell = new PdfPCell(Image.getInstance(total));
                cell.setBorder(0);
                table.addCell(cell);
                table.writeSelectedRows(0, -1, 10, 50, writer.getDirectContent());
            }
            catch(DocumentException de) {
                throw new ExceptionConverter(de);
            }
        }
      public void onCloseDocument(PdfWriter writer, Document document) {
          try{  
                    ColumnText.showTextAligned(total, Element.ALIGN_LEFT,
                    new Phrase(String.valueOf(writer.getPageNumber() - 1)),
                    2, 2, 0);
             }
          catch(Exception ex){
              
          }
        }
    }
    
}
