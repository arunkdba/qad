package com.reports.pdf;
import com.common.Common;
import java.io.*;
import java.util.*;
import javax.swing.JOptionPane;
import com.reports.classes.CoEffVarient;
import com.reports.classes.MinAndMaxValue;
import com.reports.data.CVReportData;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;

public class MagSpinningWrapingTestPDF {

    String          SFile           = "";
    Common          common          = new Common();
    CVReportData    Srh             = new CVReportData();
    CoEffVarient    cev             = new CoEffVarient();
    MinAndMaxValue  mval            = new MinAndMaxValue();
    
    String[]                         ColumnName,ColumnType,ColumnName1,ColumnType1,ColumnName2,ColumnType2,sBody;
    int[]                            iColumnWidth,iColumnWidth1,iColumnWidth2;

     private static Font catFont     = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
     private static Font redFont     = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);
     private static Font subFont     = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);

     private static Font bigBold     = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
     private static Font bigNormal   = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.NORMAL);

     private static Font mediumBold  = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
     private static Font mediumNormal= new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

     private static Font smallBold   = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.BOLD);
     private static Font smallNormal = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.NORMAL);

     private static Font tinyBold    = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
     private static Font tinyNormal  = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.NORMAL);

     private static Font underBold   = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.UNDERLINE);
    
     int    iWidth1[]   = {35,35,35,35,35,35,35,35};
     
     Document   document;
     PdfPTable  table1,table2;
     PdfWriter  writer = null;
     double     dprocess =0;
     String     SProcess="",SRh="";

     
    public void createPDFFile(){
     try{
         
        SFile       =   "MagSpinningTest.pdf";
        SFile       =   common.getPrintPath()+SFile;    
         System.out.println("FileName-->"+SFile);    
        document    =   new Document(PageSize.A4);
        //PdfWriter   .   getInstance(document, new FileOutputStream(SFile));
        writer      =  PdfWriter.getInstance(document, new FileOutputStream(SFile));
        TableHeader event   = new TableHeader();
        writer      .   setPageEvent(event);
        document    .   open();

        table1      =   new PdfPTable(8);
        table1      .   setWidths(iWidth1);
        table1      .   setSpacingAfter(3);
        table1      .   setWidthPercentage(100);
        table1      .   setHeaderRows(5);
               
       }
        catch (Exception e){
           e.printStackTrace();
        }
    }
    
    public void setTestBase(String SDate,java.util.List TestList) {
       
        String Str1 = "Company    : AMARJOTHI SPINNING MILLS LIMITED\n";
        String Str2 = "Document   : MAG - SPINNING WRAPPING TEST REPORT As on ";
        
        try {
            
            java.util.HashMap theBaseMap = (java.util.HashMap)TestList.get(0);
            
            AddCellIntoTable(Str1, table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,25,25f,0,0,0,0,bigBold );
            AddCellIntoTable(Str2+common.parseDate(SDate), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,25,25f,0,0,0,0,bigBold );
            
            AddCellIntoTable("Dry : "+common.parseNull((String)theBaseMap.get("DBT")), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,2,20f,5,3,8,3,bigBold );
            AddCellIntoTable("Wet : "+common.parseNull((String)theBaseMap.get("WBT"))+"F", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,2,20f,5,3,8,3,bigBold );
            AddCellIntoTable("RH : "+common.parseNull((String)theBaseMap.get("RH")), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,2,20f,5,3,8,3,bigBold );
            AddCellIntoTable("Test Date : "+common.parseDate(common.parseNull((String)theBaseMap.get("ENTRYDATE"))), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,25,20f,5,3,8,3,bigBold );
            
            AddCellIntoTable("Test ID : "+common.parseNull((String)theBaseMap.get("TESTID")), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,2,20f,5,3,8,3,bigBold );
            AddCellIntoTable("No.Of.Test : "+common.parseNull((String)theBaseMap.get("TEST")), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,2,20f,5,3,8,3,bigBold );
            AddCellIntoTable("Count Type : "+common.parseNull((String)theBaseMap.get("COUNTTYPE")), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,2,20f,5,3,8,3,bigBold );
            AddCellIntoTable("Nom.Strength : "+common.parseNull((String)theBaseMap.get("NOMSTRENGTH")), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,2,20f,5,3,8,3,bigBold );
            
            AddCellIntoTable("Dept : "+common.parseNull((String)theBaseMap.get("DEPARTMENT")), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,2,20f,5,3,8,3,bigBold );
            AddCellIntoTable("Lot No : "+common.parseNull((String)theBaseMap.get("MACHINE_NAME")), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,2,20f,5,3,8,3,bigBold );
            AddCellIntoTable("Nom.Count : "+common.parseNull((String)theBaseMap.get("NOMCOUNT")), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,2,20f,5,3,8,3,bigBold );
            AddCellIntoTable("Sam.Length : "+common.parseNull((String)theBaseMap.get("SAMPLE_LENGTH"))+"yard", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,2,20f,5,3,8,3,bigBold );
            
            AddCellIntoTable("Shift : "+common.parseNull((String)theBaseMap.get("SHIFT")), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,2,20f,5,3,8,3,bigBold );
            AddCellIntoTable("Operator : "+common.parseNull((String)theBaseMap.get("OPERATOR")), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,2,20f,5,3,8,3,bigBold );
            AddCellIntoTable("Machine : "+common.parseNull((String)theBaseMap.get("FRAME")), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,2,20f,5,3,8,3,bigBold );
            AddCellIntoTable("Time : "+common.parseNull((String)theBaseMap.get("TIME")), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,2,20f,5,3,8,3,bigBold );
            
           // document.add(table1);
           // document.close();
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void printData(java.util.List TestList,java.util.List TBase){
	
        try
        {
            java.util.HashMap theBaseMap = (java.util.HashMap)TBase.get(0);
            
            double dweightTotal = 0,dweightCnt=0;
            for (int i = 0; i < TestList.size(); i++) {
                    java.util.HashMap theMap = (java.util.HashMap)TestList.get(i);
                    dweightTotal += common.toDouble(common.parseNull((String)theMap.get("WEIGHT")));
                    if(dweightTotal>0){
                        dweightCnt +=1;
                    }
            
            AddCellIntoTable(common.parseNull((String)theMap.get("TEST")), table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theMap.get("WEIGHT"),3)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theMap.get("COUNT"),3)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theMap.get("CORRCOUNT"),3)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theMap.get("STRENGTH"),2)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theMap.get("CSP"),0)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull((String)theMap.get("CORRCSP")), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            }
            
            AddCellIntoTable("", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,8,20f,5,3,8,3, tinyNormal );
            
            AddCellIntoTable("Average", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,2,20f,5,3,8,3, tinyBold );
            AddCellIntoTable(common.getRound((dweightTotal/dweightCnt),3), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("COUNTAVG"),3)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("RHCOUNTAVG"),3)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("STRAVG"),2)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("CSPAVG"),0)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("RHCSPAVG"),0)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            
            AddCellIntoTable("CV%", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,2,20f,5,3,8,3, tinyBold );
            AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("COUNTCV"),2)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("RHCOUNTCV"),2)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("STRCV"),2)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("CSPCV"),2)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("RHCSPCV"),2)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            
            AddCellIntoTable("Min", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,2,20f,5,3,8,3, tinyBold );
            AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("COUNTMIN"),3)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("RHCOUNTMIN"),3)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("STRMIN"),2)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("CSPMIN"),0)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("RHCSPMIN"),0)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            
            AddCellIntoTable("Max", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,2,20f,5,3,8,3, tinyBold );
            AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("COUNTMAX"),3)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("RHCOUNTMAX"),3)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("STRMAX"),2)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("CSPMAX"),0)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("RHCSPMAX"),0)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            
            AddCellIntoTable("Range", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,2,20f,5,3,8,3, tinyBold );
            AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("COUNTRANGE"),3)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("RHCOUNTRANGE"),3)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("STRRANGE"),2)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("CSPRANGE"),0)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("RHCSPRANGE"),0)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            
            AddCellIntoTable("SD", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,2,20f,5,3,8,3, tinyBold );
            AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("COUNTSE"),2)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("RHCOUNTSD"),2)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("STRSD"),2)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("CSPSD"),2)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("RHCSPSD"),2)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            
            AddCellIntoTable("Q95(+/-)", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,2,20f,5,3,8,3, tinyBold );
            AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("COUNTQ95"),2)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("RHCOUNTQ95"),2)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("STRQ95"),2)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("CSPQ95"),2)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("RHCSPQ95"),2)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            
            AddCellIntoTable("Q99(+/-)", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,2,20f,5,3,8,3, tinyBold );
            AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("COUNTQ99"),2)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("RHCOUNTQ99"),2)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("STRQ99"),2)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("CSPQ99"),2)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            AddCellIntoTable(common.parseNull(common.getRound((String)theBaseMap.get("RHCSPQ99"),2)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            
            AddCellIntoTable("RKM Value", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,4,20f,5,3,8,3, tinyBold );
            AddCellIntoTable("RH RKM Value", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,4,20f,5,3,8,3, tinyBold );
            
            AddCellIntoTable("Remarks Value : "+(String)theBaseMap.get("REMARKS"), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,8,20f,5,3,8,3, tinyBold );
            
            document.add(table1);
            document.close();
        }
        catch(Exception Ex){
                Ex.printStackTrace();
        }
    }
    
    public void setHead(){
	
        try
        {
            AddCellIntoTable("Test No", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,20f,5,3,8,3, tinyBold );
            AddCellIntoTable("Weight(g)", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyBold );
            AddCellIntoTable("Count(Nec)", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyBold );
            AddCellIntoTable("RH Count(Nec)", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyBold );
            AddCellIntoTable("Strength(lbf)", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyBold );
            AddCellIntoTable("CSP", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyBold );
            AddCellIntoTable("RH CSP", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyBold );
           
        }
        catch(Exception Ex){
                Ex.printStackTrace();
        }
    }
    
private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      } 
    
    private void AddHeadCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iRowSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setRowspan(iRowSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
    
    private void AddCellIntoTable1(String Str,PdfPTable table,int iHorizontal,int iVertical,int iRowSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setRowspan(iRowSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
	  private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          //c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
	 private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,int iRowSpan)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          //c1.setRowspan(iRowSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
	  private void AddCellTable(Image image,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder)
      {
          PdfPCell c1 = new PdfPCell();
		  c1.addElement(new Chunk(image, 5, -5));
          //c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);

		  // image.scalePercent(90f);
      	  // paragraph.add(new Chunk(image, -1f, 1f));
      }
	 private void AddCellIntoTable(Image image,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder)
      {
          PdfPCell c1 = new PdfPCell();
		  //c1.addElement(new Chunk(image, 5, -5));
	  c1.addElement(image);
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);

      }
     private void AddCellIntoTable(double dValue,int iRound,int iRad,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder)
      {
           Double DValue = new Double(dValue);
           String Str;
           if (DValue.isNaN() || dValue ==0)
           {
               Str="";
           }
           else
           {
               Str=(common.Rad(common.getRound(dValue,iRound),iRad));
           }

          PdfPCell c1 = new PdfPCell(new Phrase(Str));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);

      }
     
     public void AddCellIntoTable2(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }
    
     class TableHeader extends PdfPageEventHelper {
        String header;
        PdfTemplate total;
        public void setHeader(String header) {
            this.header = header;
      }
      public void onOpenDocument(PdfWriter writer, Document document) {
            try{
            total = writer.getDirectContent().createTemplate(30, 16);
            }
            catch(Exception ex){
            }
      }

      public void onEndPage(PdfWriter writer, Document document) {
            PdfPTable table = new PdfPTable(3);
            try {
                table.setWidths(new int[]{24, 24, 2});
                table.setTotalWidth(520);
                table.setLockedWidth(true);
                table.getDefaultCell().setFixedHeight(20);
                table.getDefaultCell().setBorder(0);
                table.addCell(header);
                table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell("");
                table.addCell("");
                table.addCell("");
                table.addCell(String.format("Page %d of", writer.getPageNumber()));
                PdfPCell cell = new PdfPCell(Image.getInstance(total));
                cell.setBorder(0);
                table.addCell(cell);
                table.writeSelectedRows(0, -1, 10, 50, writer.getDirectContent());
            }
            catch(DocumentException de) {
                throw new ExceptionConverter(de);
            }
        }
      public void onCloseDocument(PdfWriter writer, Document document) {
          try{  
                    ColumnText.showTextAligned(total, Element.ALIGN_LEFT,
                    new Phrase(String.valueOf(writer.getPageNumber() - 1)),
                    2, 2, 0);
             }
          catch(Exception ex){
              
          }
        }
    }
        
}
