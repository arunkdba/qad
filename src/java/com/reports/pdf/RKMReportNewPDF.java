package com.reports.pdf;

import com.common.Common;
import java.io.*;
import java.util.*;
import javax.swing.JOptionPane;
import com.reports.classes.CoEffVarient;
import com.reports.classes.MinAndMaxValue;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;


public class RKMReportNewPDF {
    String SFile                    = "";
    Common common                   = new Common();
    
    String[]                         ColumnName,ColumnType,ColumnName1,ColumnType1,ColumnName2,ColumnType2,sBody;
    int[]                            iColumnWidth,iColumnWidth1,iColumnWidth2;

     private static Font catFont     = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
     private static Font redFont     = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);
     private static Font subFont     = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);

     private static Font bigBold     = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
     private static Font bigNormal   = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

     private static Font mediumBold  = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
     private static Font mediumNormal= new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

     private static Font smallBold   = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.BOLD);
     private static Font smallNormal = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.NORMAL);

     private static Font tinyBold    = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.BOLD);
     private static Font tinyNormal  = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.NORMAL);

     private static Font underBold   = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.UNDERLINE);
    
     int    iWidth1[]   = {15,65,30,20,50,20,28,20,20,20,20,20,20,20,35,30};
     String SUnitName  = "";
     
     Document   document;
     PdfPTable  table1;
     
     PdfWriter   writer = null;
     
     public void createPDFFile(){
          try
          {

            SFile       =   "RKMDateWiseReportNew.pdf";
            SFile       =   common.getPrintPath()+SFile;    
            
               document =  new Document(PageSize.LEGAL.rotate());
               writer   =  PdfWriter.getInstance(document, new FileOutputStream(SFile));
               TableHeader event  = new TableHeader();
               writer   .  setPageEvent(event);
               document .  open();

               table1= new PdfPTable(16);
               table1.setWidths(iWidth1);
//               table1.setSpacingAfter(2);
               table1.setWidthPercentage(100);
               table1.setHeaderRows(6);
               
          }
          catch (Exception e)
          {
               e.printStackTrace();
          }
    }
     
    public void setHead (String SDate,String SToDate,String SUnitName,String SProcess,String Shift)
    {
        String Str1    = "Company    : AMARJOTHI SPINNING MILLS LIMITED";
        String Str2    = "Document   : RKM REPORT FROM ";
        String Str3    = "Unit             : "+SUnitName+"\n";
        String Str5    = "Process       : "+SProcess+"\n";
        String Str6    = "Shift           : "+Shift+"\n";
        
        this.SUnitName   = SUnitName;    
        
         if(SProcess.endsWith("s") || SProcess.endsWith("S")){
              SProcess = SProcess.substring(0, SProcess.length()-1);
            }
        try
        {
//               Paragraph paragraph;
//
//               paragraph = new Paragraph(Str1,bigBold);
//               paragraph.setAlignment(Element.ALIGN_LEFT);
//               document.add(paragraph);
//
//               paragraph = new Paragraph(Str2+common.parseDate(SDate)+" To  "+common.parseDate(SToDate)+" \n",bigBold);
//               paragraph.setAlignment(Element.ALIGN_LEFT);
//               document.add(paragraph);
//               
//               paragraph = new Paragraph(Str3,bigBold);
//               paragraph.setAlignment(Element.ALIGN_LEFT);
//               document.add(paragraph);
//               
//               paragraph = new Paragraph(Str5,bigBold);
//               paragraph.setAlignment(Element.ALIGN_LEFT);
//               document.add(paragraph);
               
//               paragraph = new Paragraph(Str6,bigBold);
//               paragraph.setAlignment(Element.ALIGN_LEFT);
//               paragraph.setSpacingAfter(8);
//               document.add(paragraph);

               AddCellIntoTable(Str1, table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,16,20f,0,0,0,0,bigBold );
               AddCellIntoTable(Str2+common.parseDate(SDate)+" To  "+common.parseDate(SToDate), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,16,20f,0,0,0,0,bigBold );
               AddCellIntoTable(Str3, table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,16,20f,0,0,0,0,bigBold );
               AddCellIntoTable(Str5, table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,16,20f,0,0,0,0,bigBold );
               AddCellIntoTable(Str6, table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,16,20f,0,0,0,0,bigBold );
               
               AddCellIntoTable("S.No", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3,tinyBold );
               AddCellIntoTable("Party Name", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3,tinyBold );
               AddCellIntoTable("Order No", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3,tinyBold );
               AddCellIntoTable("Count", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3,tinyBold );
               AddCellIntoTable("Shade", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3,tinyBold );
               AddCellIntoTable("TestNo", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3,tinyBold );
               AddCellIntoTable("Entry Date", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3,tinyBold );
               AddCellIntoTable("RKM", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3,tinyBold );
               AddCellIntoTable("RKM CV", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3,tinyBold );
               AddCellIntoTable("Min RKM", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3,tinyBold );
               AddCellIntoTable("Elong", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3,tinyBold );
               AddCellIntoTable("Elong CV", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3,tinyBold );
               AddCellIntoTable("Sys", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3,tinyBold );
               AddCellIntoTable("Sys CV", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3,tinyBold );
               AddCellIntoTable("Remarks", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3,tinyBold );
               AddCellIntoTable("Unit", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3,tinyBold );
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    public void printPDFData(java.util.List TestRKMList){
        try{
               for (int i = 0; i < TestRKMList.size(); i++) {
                
                com.reports.classes.RkmDetails Td = (com.reports.classes.RkmDetails) TestRKMList.get(i);
                int irow           = 0;
                    irow           = i+1;
                
                AddCellIntoTable(String.valueOf(irow), table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,25f,5,3,8,3,bigNormal );
                AddCellIntoTable(common.parseNull(Td.getPartyname()), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,25f,5,3,8,3,bigNormal );
                AddCellIntoTable(common.parseNull(Td.getOrderno()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,25f,5,3,8,3,bigNormal );
                AddCellIntoTable(common.parseNull(Td.getCountname()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,25f,5,3,8,3,bigNormal );
                AddCellIntoTable(common.parseNull(Td.getShadename()), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,25f,5,3,8,3,bigNormal );
                AddCellIntoTable(common.parseNull(Td.getTestno()), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,25f,5,3,8,3,bigNormal );
                AddCellIntoTable(common.parseNull(common.parseDate(String.valueOf(Td.getEntrydate()))), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,25f,5,3,8,3,bigNormal );
                AddCellIntoTable(common.parseNull(Td.getRkm()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,25f,5,3,8,3,bigNormal );
                AddCellIntoTable(common.parseNull(Td.getRkmcv()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,25f,5,3,8,3,bigNormal );
                AddCellIntoTable(common.parseNull(Td.getMinRKM()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,25f,5,3,8,3,bigNormal );
                AddCellIntoTable(common.parseNull(Td.getElg()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,25f,5,3,8,3,bigNormal );
                AddCellIntoTable(common.parseNull(Td.getElgcv()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,25f,5,3,8,3,bigNormal );
                AddCellIntoTable(common.parseNull(Td.getSys()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,25f,5,3,8,3,bigNormal );
                AddCellIntoTable(common.parseNull(Td.getSyscv()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,25f,5,3,8,3,bigNormal );
                AddCellIntoTable(common.parseNull(Td.getRemarks()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,25f,5,3,8,3,bigNormal );
                if(SUnitName.equals("All"))
                 AddCellIntoTable(common.parseNull(Td.getUnitname()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,25f,5,3,8,3,bigNormal );
                else
                 AddCellIntoTable(SUnitName, table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,25f,5,3,8,3,bigNormal );   
            }
            document.add(table1);
            document.close();
            
        }
        catch(Exception ex){
            ex.printStackTrace();
            System.out.println("printdata-->"+ex);
        }
    }
     private void addEmptyCell(PdfPTable table)
     {
          PdfPCell c1 = new PdfPCell();
          table.addCell(c1);
     }

     private void addEmptyCell(PdfPTable table,String Left,String Right)
     {
          PdfPCell c1 = new PdfPCell();
          c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
          table.addCell(c1);
     }

     private void addEmptyRow(PdfPTable table)
     {
//          for(int i=0;i<SHead.length;i++)
//          {
//               PdfPCell c1 = new PdfPCell();
//               table.addCell(c1);
//          }
     }

     private void addEmptySpanRow(PdfPTable table,String Left,String Right)
     {
          PdfPCell c1 = new PdfPCell();
          c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
         // c1.setColspan(SHead.length);
          table.addCell(c1);
     }

     private static void addEmptyLine(Paragraph paragraph, int number)
     {
          for (int i = 0; i < number; i++)
          {
               paragraph.add(new Paragraph(" "));
          }
     }
    private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      } 
    
    private void AddHeadCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
    
    private void AddCellIntoTable1(String Str,PdfPTable table,int iHorizontal,int iVertical,int iRowSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setRowspan(iRowSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
     private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          //c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
     private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,int iRowSpan)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          //c1.setRowspan(iRowSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
     private void AddCellTable(Image image,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder)
      {
          PdfPCell c1 = new PdfPCell();
		  c1.addElement(new Chunk(image, 5, -5));
          //c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);

		  // image.scalePercent(90f);
      	  // paragraph.add(new Chunk(image, -1f, 1f));
      }
      private void AddCellIntoTable(Image image,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder)
      {
          PdfPCell c1 = new PdfPCell();
		  //c1.addElement(new Chunk(image, 5, -5));
	  c1.addElement(image);
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);

      }
     private void AddCellIntoTable(double dValue,int iRound,int iRad,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder)
      {
           Double DValue = new Double(dValue);
           String Str;
           if (DValue.isNaN() || dValue ==0)
           {
               Str="";
           }
           else
           {
               Str=(common.Rad(common.getRound(dValue,iRound),iRad));
           }

          PdfPCell c1 = new PdfPCell(new Phrase(Str));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);

      }
     
     public void AddCellIntoTable2(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }
    

    class TableHeader extends PdfPageEventHelper {
        String header;
        PdfTemplate total;
        public void setHeader(String header) {
            this.header = header;
        }
        public void onOpenDocument(PdfWriter writer, Document document) {
            try{
            total = writer.getDirectContent().createTemplate(30, 16);
            }
            catch(Exception ex){
            }
        }

        public void onEndPage(PdfWriter writer, Document document) {
            PdfPTable table = new PdfPTable(3);
            try {
                table.setWidths(new int[]{24, 24, 2});
                table.setTotalWidth(520);
                table.setLockedWidth(true);
                table.getDefaultCell().setFixedHeight(20);
                table.getDefaultCell().setBorder(0);
                table.addCell(header);
                table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell("");
                table.addCell("");
                table.addCell("");
                table.addCell(String.format("Page %d of", writer.getPageNumber()));
                PdfPCell cell = new PdfPCell(Image.getInstance(total));
                cell.setBorder(0);
                table.addCell(cell);
                table.writeSelectedRows(0, -1, 10, 50, writer.getDirectContent());
            }
            catch(DocumentException de) {
                throw new ExceptionConverter(de);
            }
        }
        public void onCloseDocument(PdfWriter writer, Document document) {
          try{  
            ColumnText.showTextAligned(total, Element.ALIGN_LEFT,
                    new Phrase(String.valueOf(writer.getPageNumber() - 1)),
                    2, 2, 0);
             }
          catch(Exception ex){
              
          }
        }
    }     
 
}
