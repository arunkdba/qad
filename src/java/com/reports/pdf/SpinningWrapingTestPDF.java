package com.reports.pdf;

import com.common.Common;
import java.io.*;
import java.util.*;
import javax.swing.JOptionPane;
import com.reports.data.SpinningWrappingTestData;
import com.reports.classes.CoEffVarient;
import com.reports.classes.MinAndMaxValue;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;

public class SpinningWrapingTestPDF {
    
    String SFile                    = "";
    Common common                   = new Common();
    SpinningWrappingTestData Srh    = new SpinningWrappingTestData();
    CoEffVarient             cev    = new CoEffVarient();
    MinAndMaxValue           mval   = new MinAndMaxValue();
    
    
    java.util.List arrTotCntList    = new ArrayList();
    java.util.List arrTotStrList    = new ArrayList();
    java.util.List arrTotCspList    = new ArrayList();

    java.util.List<Double> arrTotCnt= new ArrayList<Double>();
    java.util.List<Double> arrTotStr= new ArrayList<Double>();
    java.util.List<Double> arrTotCsp= new ArrayList<Double>();
    
    double  dtotcount=0.00, dtotstr=0.00, dtotcsp=0.00;
    double dnominalCnt = 0.00, dnominalStr = 0.00, dprocess=0.00;
    
    int     incount =0, instr=0,incsp=0;
    String SRh="", SProcess="";
    
     String[]                         ColumnName,ColumnType,ColumnName1,ColumnType1,ColumnName2,ColumnType2,sBody;
     int[]                            iColumnWidth,iColumnWidth1,iColumnWidth2;

     private static Font catFont     = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
     private static Font redFont     = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);
     private static Font subFont     = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);

     private static Font bigBold     = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
     private static Font bigNormal   = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);

     private static Font mediumBold  = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
     private static Font mediumNormal= new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

     private static Font smallBold   = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.BOLD);
     private static Font smallNormal = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.NORMAL);

     private static Font tinyBold    = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.BOLD);
     private static Font tinyNormal  = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.NORMAL);

     private static Font underBold   = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.UNDERLINE);
    
     int    iWidth[]    = {35,35,35};
     int    iWidth1[]   = {20,20,20,20,20,20,20,10,20,20,30,20,20,20,28,20,20,20,20,20};
     int    iWidth2[]   = {20,20,20,20,20,20,20,20,20};
     
     Document   document;
     PdfPTable  table,table1,table2;
     PdfWriter   writer = null;
     
     public void createPDFFile(){
          try
          {

            SFile      =   "SpinningWrapingTest.pdf";
            SFile      =   common.getPrintPath()+SFile;    
            
               document = new Document(PageSize.LEGAL.rotate());
               //PdfWriter.getInstance(document, new FileOutputStream(SFile));
               writer   =  PdfWriter.getInstance(document, new FileOutputStream(SFile));
               TableHeader event       = new TableHeader();
               writer.setPageEvent(event);
               document.open();

//               table = new PdfPTable(3);
//               table.setWidths(iWidth);
//               table.setSpacingAfter(10);
//               table.setWidthPercentage(100);
//               table.setHeaderRows(4);

               table1 = new PdfPTable(20);
               table1.setWidths(iWidth1);
               table1.setSpacingAfter(10);
               table1.setWidthPercentage(100);
               table1.setHeaderRows(5);
               
               
               table2 = new PdfPTable(9);
               table2.setWidths(iWidth2);
               table2.setSpacingAfter(310);
               table2.setWidthPercentage(100);

          }
          catch (Exception e)
          {
               e.printStackTrace();
          }
    }
    
    public void setTestBase (String SDate,String STdate,java.util.List TBase)
    {
        String Str1    = "Company    : AMARJOTHI SPINNING MILLS LIMITED";
        String Str2    = "Document   : SPINNING / OE WRAPPING TEST REPORT FROM ";
        dnominalCnt    = common.toDouble(common.parseNull((String) TBase.get(1)));
        dnominalStr    = common.toDouble(common.parseNull((String) TBase.get(4)));
        SProcess       = common.parseNull((String) TBase.get(10));
        
         if(SProcess.endsWith("s") || SProcess.endsWith("S")){
              SProcess = SProcess.substring(0, SProcess.length()-1);
            }
              dprocess = common.toDouble(SProcess);
              SRh      = common.parseNull((String) TBase.get(8));
        
        try
        {
               Paragraph paragraph;

//               paragraph = new Paragraph(Str1,bigBold);
//               paragraph.setAlignment(Element.ALIGN_LEFT);
//               paragraph.setSpacingAfter(7);
//               document.add(paragraph);

//               paragraph = new Paragraph(Str2+common.parseDate(SDate)+" To  "+common.parseDate(STdate)+" \n",bigBold);
//               paragraph.setAlignment(Element.ALIGN_LEFT);
//               paragraph.setSpacingAfter(7);
//               document.add(paragraph);

            AddCellIntoTable(Str1, table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,20,20f,0,0,0,0,bigBold );               
            AddCellIntoTable(Str2+common.parseDate(SDate)+" To  "+common.parseDate(STdate), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,20,20f,0,0,0,0,bigBold );
            
            AddCellIntoTable("Test No :"+common.parseNull((String)TBase.get(0)), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,7,25f,5,3,8,3, bigBold);
            AddCellIntoTable("Nominal Count :"+common.parseNull((String) TBase.get(1))+" Nec", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,7,25f,5,3,8,3, bigBold);
            AddCellIntoTable("WB Temp : "+common.parseNull((String) TBase.get(2)), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,6,25f,5,3,8,3, bigBold);
            
            AddCellIntoTable("Test Date : "+common.parseDate((String) TBase.get(3)), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,7,25f,5,3,8,3, bigBold);
            AddCellIntoTable("Nominal Strength : "+common.parseNull((String) TBase.get(4))+" lbs", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,7,25f,5,3,8,3, bigBold);
            AddCellIntoTable("DB Temp : "+common.parseNull((String) TBase.get(5)), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,6,25f,5,3,8,3, bigBold);
		
            
            AddCellIntoTable("Test Time : "+common.parseNull((String) TBase.get(6)), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,7,25f,5,3,8,3, bigBold);
            AddCellIntoTable("Sample Length : "+common.parseNull((String) TBase.get(7))+" Yards", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,7,25f,5,3,8,3, bigBold);
            AddCellIntoTable("RH : "+common.parseNull((String) TBase.get(8))+"%", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,6,25f,5,3,8,3, bigBold);
	
            AddCellIntoTable("Shift : "+common.parseNull((String) TBase.get(9)), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,7,25f,5,3,8,3, bigBold);
            AddCellIntoTable("Process : "+common.parseNull((String) TBase.get(10)), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,7,25f,5,3,8,3, bigBold);
            AddCellIntoTable("Operator : "+common.parseNull((String) TBase.get(11)), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,6,25f,5,3,8,3, bigBold);
            
//            document.add(table);
          //  document.close();
          
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    public void setHead(String SDate,String STdate){
	
        try
        {
            AddHeadCellIntoTable("S.No", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("Machine/Change Adv", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("For", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("1", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("2", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("3", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("4", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("5", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("Avg  Value", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("RHC Value", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            
            AddHeadCellIntoTable("Cnt.Cor. Strength", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("CV%", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("Cp", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("TPI", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("O.No", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("Shade", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("Count", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("Pump/  Cheese", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("Draft", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("B.D", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            
          //  document.add(table1);
          //  document.close();
            
        }
        catch(Exception Ex)
        {
                Ex.printStackTrace();
        }
    }
    
    public void setPrintData(String SDate,String STdate,java.util.List TestList){
	
        try
        {
            Srh.getRhFactors();
            for (int i=0; i < TestList.size(); i++) {
                int row = 0;
                    row = i+1;
                    
                com.reports.classes.SpinningWrappingTestDetails Td = (com.reports.classes.SpinningWrappingTestDetails) TestList.get(i);
                
                    int iCnt = 0;
                    int iStr = 0;
                    int iCsp = 0;
                    double drhStr=0,drhCnt=0;
                    double dcnt1 = common.toDouble(Td.getCnt1());
                    double dcnt2 = common.toDouble(Td.getCnt2());
                    double dcnt3 = common.toDouble(Td.getCnt3());
                    double dcnt4 = common.toDouble(Td.getCnt4());
                    double dcnt5 = common.toDouble(Td.getCnt5());
                    ArrayList<String> arrlistcnt = new ArrayList<String>();
                    if (dcnt1 != 0) {
                        iCnt += 1;
                        arrlistcnt.add(Td.getCnt1());
                        arrTotCntList.add(Td.getCnt1());
                        arrTotCnt.add(dcnt1);
                    }
                    if (dcnt2 != 0) {
                        iCnt += 1;
                        arrlistcnt.add(Td.getCnt2());
                        arrTotCntList.add(Td.getCnt2());
                        arrTotCnt.add(dcnt2);
                    }
                    if (dcnt3 != 0) {
                        iCnt += 1;
                        arrlistcnt.add(Td.getCnt3());
                        arrTotCntList.add(Td.getCnt3());
                        arrTotCnt.add(dcnt3);
                    }
                    if (dcnt4 != 0) {
                        iCnt += 1;
                        arrlistcnt.add(Td.getCnt4());
                        arrTotCntList.add(Td.getCnt4());
                        arrTotCnt.add(dcnt4);
                    }
                    if (dcnt5 != 0) {
                        iCnt += 1;
                        arrlistcnt.add(Td.getCnt5());
                        arrTotCntList.add(Td.getCnt5());
                        arrTotCnt.add(dcnt5);
                    }
                    double dCntAvg = (dcnt1 + dcnt2 + dcnt3 + dcnt4+dcnt5) / iCnt;
                    dtotcount += (dcnt1 + dcnt2 + dcnt3 + dcnt4+dcnt5);
                    incount += iCnt;
                    String[] CntArray = (String[]) arrlistcnt.toArray(new String[arrlistcnt.size()]);

                    double dsrten1 = common.toDouble(Td.getStrength1());
                    double dsrten2 = common.toDouble(Td.getStrength2());
                    double dsrten3 = common.toDouble(Td.getStrength3());
                    double dsrten4 = common.toDouble(Td.getStrength4());
                    double dsrten5 = common.toDouble(Td.getStrength5());
                    ArrayList<String> arrliststr = new ArrayList<String>();
                    if (dsrten1 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength1());
                        arrTotStrList.add(Td.getStrength1());
                        arrTotStr.add(dsrten1);
                    }
                    if (dsrten2 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength2());
                        arrTotStrList.add(Td.getStrength2());
                        arrTotStr.add(dsrten2);
                    }
                    if (dsrten3 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength3());
                        arrTotStrList.add(Td.getStrength3());
                        arrTotStr.add(dsrten3);
                    }
                    if (dsrten4 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength4());
                        arrTotStrList.add(Td.getStrength4());
                        arrTotStr.add(dsrten4);
                    }
                    if (dsrten5 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength5());
                        arrTotStrList.add(Td.getStrength5());
                        arrTotStr.add(dsrten5);
                    }
                    double dStrenAvg = (dsrten1 + dsrten2 + dsrten3 + dsrten4 + dsrten5) / iStr;
                    dtotstr += (dsrten1 + dsrten2 + dsrten3 + dsrten4 + dsrten5);
                    instr += iStr;
                    String[] StrArray = (String[]) arrliststr.toArray(new String[arrliststr.size()]);
                    
                    double dcsp1 = common.toDouble(Td.getCsp1());
                    double dcsp2 = common.toDouble(Td.getCsp2());
                    double dcsp3 = common.toDouble(Td.getCsp3());
                    double dcsp4 = common.toDouble(Td.getCsp4());
                    double dcsp5 = common.toDouble(Td.getCsp5());
                    ArrayList<String> arrlistcsp = new ArrayList<String>();
                    if (dcsp1 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp1());
                        arrTotCspList.add(Td.getCsp1());
                        arrTotCsp.add(dcsp1);
                    }
                    if (dcsp2 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp2());
                        arrTotCspList.add(Td.getCsp2());
                        arrTotCsp.add(dcsp2);
                    }
                    if (dcsp3 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp3());
                        arrTotCspList.add(Td.getCsp3());
                        arrTotCsp.add(dcsp3);
                    }
                    if (dcsp4 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp4());
                        arrTotCspList.add(Td.getCsp4());
                        arrTotCsp.add(dcsp4);
                    }
                    if (dcsp5 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp5());
                        arrTotCspList.add(Td.getCsp5());
                        arrTotCsp.add(dcsp5);
                    }
                    double dCspAvg = (dcsp1 + dcsp2 + dcsp3 + dcsp4 + dcsp5) / iCsp;
                    dtotcsp += (dcsp1 + dcsp2 + dcsp3 + dcsp4 + dcsp5);
                    incsp += iCsp;
                    String[] CspArray = (String[]) arrlistcsp.toArray(new String[arrlistcsp.size()]);
                    
                    if(dStrenAvg>0){
                    double dc1 = common.toDouble(common.getRound(dCntAvg,2));
                    double ds1 = common.toDouble(common.getRound(dStrenAvg,2));
                    double dc2 = (dprocess-1);
                    double dc1s1 = common.toDouble(common.getRound(dc1*ds1,2));
                    double dc2c1 = common.toDouble(common.getRound(13*(dc2-dc1),2));
                    double dupper = dc1s1-dc2c1;
                    drhStr = dupper/dc2;
                    
//                    System.out.println("Str Rh");
//                    System.out.println("Proceee Value-->"+dprocess);
//                    System.out.println("C1 Value-->"+dc1);
//                    System.out.println("C2-->"+dc2);
//                    System.out.println("S1 Value -->"+ds1);
//                    System.out.println("C1*S1 Value-->"+dc1s1);
//                    System.out.println("13 * (c2-c1)-->"+dc2c1);
//                    System.out.println("C1 S1 - 13 * (c2-c1)-->"+dupper);
                    }
                    if(dCntAvg>0){
                    double drh = Srh.getrh(SRh);
                    drhCnt = (dCntAvg*drh);
                    }
                    //System.out.println("Rh Value-->"+drh);                    
                    //System.out.println("Avg cnt -->"+dCntAvg);
                    //System.out.println("Avg cnt -->"+dCntAvg);
                    //System.out.println("dCntAvg* drh-->"+drhCnt);
//                    System.out.println("drhCnt-->"+drhCnt);
//                    System.out.println("drhStr-->"+drhStr);                    
                    
                AddCellIntoTable1( String.valueOf(row),table1, Element.ALIGN_CENTER, Element.ALIGN_TOP,3,20f,5,3,8,3, bigNormal );
                AddCellIntoTable1(Td.getMechinename()+common.parseNull(Td.getTesttype())+common.parseNull(Td.getRemarks()),table1, Element.ALIGN_LEFT, Element.ALIGN_TOP,3,20f,5,3,8,3,bigNormal );
                AddCellIntoTable("Cnt", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable(Td.getCnt1(), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable(Td.getCnt2(), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );    
                AddCellIntoTable(Td.getCnt3(), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable(Td.getCnt4(), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable(Td.getCnt5(), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable(common.getRound(dCntAvg, 2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable(common.getRound(drhCnt, 2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable(common.getRound(cev.getCoEffVant(CntArray),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                
                AddCellIntoTable("Str", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable(Td.getStrength1(), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable(Td.getStrength2(), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );    
                AddCellIntoTable(Td.getStrength3(), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable(Td.getStrength4(), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable(Srh.removeNull(Td.getStrength5()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable(common.getRound(dStrenAvg, 2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal);
                AddCellIntoTable(common.getRound(drhStr,2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable("", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable(common.getRound(cev.getCoEffVant(StrArray), 2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable("", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable("", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable("", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable("", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable("", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable("", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable("", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable("", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                
                AddCellIntoTable("CSP", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable(Td.getCsp1(), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable(Td.getCsp2(), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );    
                AddCellIntoTable(Td.getCsp3(), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable(Td.getCsp4(), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable(Srh.removeNull(Td.getCsp5()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable(common.getRound(dCspAvg, 0), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable(common.parseNull(common.getRound((drhCnt*drhStr), 0)), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable("", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable(common.getRound(cev.getCoEffVant(CspArray), 2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable(common.parseNull(Td.getCpwheel()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable(common.parseNull(Td.getTpi()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable(common.parseNull(Td.getOrderno()), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable(common.parseNull(Td.getShadename()), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable(common.parseNull(Td.getCount()), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable(common.parseNull(Td.getPumpclr()), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable(common.parseNull(Td.getDraft()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                AddCellIntoTable(common.parseNull(Td.getBdraft()), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3,bigNormal );
                
            }
            
            document.add(table1);            
//            document.close();
            
        }
        catch(Exception Ex)
        {
                Ex.printStackTrace();
        }
    }
    
    public void setStatisticalData(){
        
        try
        {
            Paragraph paragraph;
            
            AddCellIntoTable2("Statistical Report ", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,9,20f,5,3,8,3, bigBold );
            AddCellIntoTable2("", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, bigBold );
            AddCellIntoTable2("Nom", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, bigBold );
            AddCellIntoTable2("Avg", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, bigBold );
            AddCellIntoTable2("Min", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, bigBold );
            AddCellIntoTable2("Max", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, bigBold );
            AddCellIntoTable2("Range", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, bigBold );
            AddCellIntoTable2("CV%", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, bigBold );
            AddCellIntoTable2("RHC", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, bigBold );
            AddCellIntoTable2("Q95", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, bigBold );
            
            
                    String[] cntTotArray = (String[]) arrTotCntList.toArray(new String[arrTotCntList.size()]);
                    String[] strTotArray = (String[]) arrTotStrList.toArray(new String[arrTotStrList.size()]);
                    String[] cspTotArray = (String[]) arrTotCspList.toArray(new String[arrTotCspList.size()]);

                    double cntTot[] = new double[arrTotCnt.size()];
                    double strTot[] = new double[arrTotStr.size()];
                    double cspTot[] = new double[arrTotCsp.size()];

                    for(int i = 0; i < strTot.length; i++){
                        strTot[i] = arrTotStr.get(i);
                    }
                    for(int i = 0; i < cspTot.length; i++){
                        cspTot[i] = arrTotCsp.get(i);
                    }
                    for(int i = 0; i < cntTot.length; i++){
                        cntTot[i] = arrTotCnt.get(i);
                    }
                    double drhStr = (common.toDouble(common.getRound((dtotcount/incount),2))*common.toDouble(common.getRound((dtotstr/instr),2)));
                    drhStr = drhStr-13 *((dprocess-1)-(common.toDouble(common.getRound((dtotcount/incount),2))));
                    drhStr = drhStr/(dprocess-1);

                    double drh = Srh.getrh(SRh);
                    double drhCnt = ((dtotcount/incount)*drh);
                    
                    AddCellIntoTable("Count", table2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, bigBold );
                    AddCellIntoTable(common.getRound(dnominalCnt,2), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, bigBold );
                    AddCellIntoTable(common.getRound(dtotcount/incount,2), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, bigBold );
                    AddCellIntoTable(common.getRound(mval.getMin(cntTot),2), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, bigBold );
                    AddCellIntoTable(common.getRound(mval.getMax(cntTot),2), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, bigBold );
                    AddCellIntoTable(common.getRound(mval.getMax(cntTot)-mval.getMin(cntTot),2), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, bigBold );
                    AddCellIntoTable(common.getRound(cev.getCoEffVant(cntTotArray),2), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, bigBold );
                    AddCellIntoTable(common.getRound(drhCnt,2), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, bigBold );
                    AddCellIntoTable("", table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, bigBold );
                    
                    AddCellIntoTable("Strength", table2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, bigBold );
                    AddCellIntoTable(common.getRound(dnominalStr,2), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, bigBold );
                    AddCellIntoTable(common.getRound((dtotstr/instr),2), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, bigBold );
                    AddCellIntoTable(common.getRound(mval.getMin(strTot),2), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, bigBold );
                    AddCellIntoTable(common.getRound(mval.getMax(strTot),2), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, bigBold );
                    AddCellIntoTable(common.getRound(mval.getMax(strTot)-mval.getMin(strTot),2), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, bigBold );
                    AddCellIntoTable(common.getRound(cev.getCoEffVant(strTotArray),2), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, bigBold );
                    AddCellIntoTable(common.getRound(drhStr,2), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, bigBold );
                    AddCellIntoTable("", table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, bigBold );
                    
                    AddCellIntoTable("Csp", table2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, bigBold );
                    AddCellIntoTable(common.getRound(dnominalCnt*dnominalStr,0), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, bigBold );
                    AddCellIntoTable(common.getRound((dtotcsp/incsp),0), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, bigBold );
                    AddCellIntoTable(common.getRound(mval.getMin(cspTot),0), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, bigBold );
                    AddCellIntoTable(common.getRound(mval.getMax(cspTot),0), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, bigBold );
                    AddCellIntoTable(common.getRound(mval.getMax(cspTot)-mval.getMin(cspTot),0), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, bigBold );
                    AddCellIntoTable(common.getRound(cev.getCoEffVant(cspTotArray),0), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, bigBold );
                    AddCellIntoTable(common.getRound(drhCnt*drhStr,0), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, bigBold );
                    AddCellIntoTable("", table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, bigBold );

                   
                    
                    
                    int iStdCdCount = 2;
                    int iStdCdStrength = 4;
                    double dNominalCnt = common.toDouble(common.getRound(dnominalCnt,2));
                    double dActualCnt = common.toDouble(common.getRound((dtotcount/incount),2));
                    double dNominalStr = common.toDouble(common.getRound(dnominalStr,2));
                    double dActualStr = common.toDouble(common.getRound((dtotstr/instr),2));
               
                    double X = common.toDouble(common.getRound(dNominalCnt-dActualCnt,2));  // For Count
                    double X1 =common.toDouble(common.getRound((dNominalCnt+dActualCnt)/2,2)); // For Count
               
                    double XX = common.toDouble(common.getRound(dNominalStr-dActualStr,2));  // For Strength
                    double XX1 =common.toDouble(common.getRound((dNominalStr+dActualStr)/2,2)); // For Strength
               
                    String sCalculatedCount = common.getRound((X/X1)*100,4);
                    String sCalculatedStr = common.getRound((XX/XX1)*100,4);
               
                    double dNormalCount = (Math.sqrt(40)/Math.sqrt(incount))*iStdCdCount;
                    double dNormalStr = (Math.sqrt(40)/Math.sqrt(instr))*iStdCdStrength;
               
               //System.out.println("Nom Cnt-->"+dNominalCnt);
               //System.out.println("Act Cnt-->"+dActualCnt);
               
               //System.out.println("X-->"+X);
               //System.out.println("X1-->"+X1);
               
               //System.out.println("Nom Str-->"+dNominalStr);
               //System.out.println("Act Str-->"+dActualStr);
                              
               //System.out.println("XX-->"+XX);
               //System.out.println("XX1-->"+XX1);
               
               //System.out.println("Total Cnt-->"+incount);
               //System.out.println("Total Str-->"+instr);
               
                    String sDiffCnt ="",sDiffStr="";
                    double dcalcCnt = common.toDouble(sCalculatedCount);
                    double dcalcStr = common.toDouble(sCalculatedStr);
                    double dnormalCnt = common.toDouble(common.getRound(dNormalCount,4));
                    double dnormalStr = common.toDouble(common.getRound(dNormalStr,4));
                    if(dcalcCnt>dnormalCnt){
                        sDiffCnt = "Critically different";
                    }
                    else
                    {
                        sDiffCnt = "No critical difference";
                    }
                    if(dcalcStr>dnormalStr){
                        sDiffStr = "Critically different";
                    }
                    else
                    {
                        sDiffStr = "No critical difference";
                    }
               
               String sDiffCount     =   "Count     : "+sDiffCnt+"( Calculated  :   "+sCalculatedCount+" Normal :   "+common.getRound(dNormalCount,4);
               String sDiffStrength  =   "Strength  : "+sDiffStr+"( Calculated  :   "+sCalculatedStr+" Normal   :   "+common.getRound(dNormalStr,4);
               String sReportTakenon =   "Report Taken on : "+common.parseDate(common.getSerDateTime());
               
               AddCellIntoTable(sDiffCount, table2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,9,20f,0,0,0,0, bigBold );    
               AddCellIntoTable(sDiffStrength, table2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,9,20f,0,0,0,0, bigBold );    
               AddCellIntoTable("CSP       : In units of Nec.lbs", table2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,9,20f,0,0,0,0, bigBold );    
               AddCellIntoTable(sReportTakenon, table2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,9,20f,0,0,0,0, bigBold );    
                    
               
//               
//               
//               paragraph = new Paragraph(sDiffCount,bigBold);
//               paragraph.setAlignment(Element.ALIGN_LEFT);
//               document.add(paragraph);
//
//               paragraph = new Paragraph(sDiffStrength,bigBold);
//               paragraph.setAlignment(Element.ALIGN_LEFT);
//               document.add(paragraph);
//               
//               paragraph = new Paragraph("CSP       : In units of Nec.lbs",bigBold);
//               paragraph.setAlignment(Element.ALIGN_LEFT);
//               document.add(paragraph);
//            
//               paragraph = new Paragraph(sReportTakenon,bigBold);
//               paragraph.setAlignment(Element.ALIGN_LEFT);
//               document.add(paragraph);
//            
            document.add(table2);   
            document.close();

           // JOptionPane.showMessageDialog(null, "PDF File Created in "+SFile,"Info",JOptionPane.INFORMATION_MESSAGE);
            
            
        }
        catch(Exception Ex)
        {
                Ex.printStackTrace();
        }
    }
    
    
     private void addEmptyCell(PdfPTable table)
     {
          PdfPCell c1 = new PdfPCell();
          table.addCell(c1);
     }

     private void addEmptyCell(PdfPTable table,String Left,String Right)
     {
          PdfPCell c1 = new PdfPCell();
          c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
          table.addCell(c1);
     }

     private void addEmptyRow(PdfPTable table)
     {
//          for(int i=0;i<SHead.length;i++)
//          {
//               PdfPCell c1 = new PdfPCell();
//               table.addCell(c1);
//          }
     }

     private void addEmptySpanRow(PdfPTable table,String Left,String Right)
     {
          PdfPCell c1 = new PdfPCell();
          c1.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
         // c1.setColspan(SHead.length);
          table.addCell(c1);
     }

     private static void addEmptyLine(Paragraph paragraph, int number)
     {
          for (int i = 0; i < number; i++)
          {
               paragraph.add(new Paragraph(" "));
          }
     }
    private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      } 
    
    private void AddHeadCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
    
    private void AddCellIntoTable1(String Str,PdfPTable table,int iHorizontal,int iVertical,int iRowSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setRowspan(iRowSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
     private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          //c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
     private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,int iRowSpan)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          //c1.setRowspan(iRowSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
     private void AddCellTable(Image image,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder)
      {
          PdfPCell c1 = new PdfPCell();
		  c1.addElement(new Chunk(image, 5, -5));
          //c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);

		  // image.scalePercent(90f);
      	  // paragraph.add(new Chunk(image, -1f, 1f));
      }
      private void AddCellIntoTable(Image image,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder)
      {
          PdfPCell c1 = new PdfPCell();
		  //c1.addElement(new Chunk(image, 5, -5));
	  c1.addElement(image);
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);

      }
     private void AddCellIntoTable(double dValue,int iRound,int iRad,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder)
      {
           Double DValue = new Double(dValue);
           String Str;
           if (DValue.isNaN() || dValue ==0)
           {
               Str="";
           }
           else
           {
               Str=(common.Rad(common.getRound(dValue,iRound),iRad));
           }

          PdfPCell c1 = new PdfPCell(new Phrase(Str));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);

      }
     
     public void AddCellIntoTable2(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }
     
      class TableHeader extends PdfPageEventHelper {
        String header;
        PdfTemplate total;
        public void setHeader(String header) {
            this.header = header;
      }
      public void onOpenDocument(PdfWriter writer, Document document) {
            try{
            total = writer.getDirectContent().createTemplate(30, 16);
            }
            catch(Exception ex){
            }
      }

        public void onEndPage(PdfWriter writer, Document document) {
            PdfPTable table = new PdfPTable(3);
            try {
                table.setWidths(new int[]{24, 24, 2});
                table.setTotalWidth(520);
                table.setLockedWidth(true);
                table.getDefaultCell().setFixedHeight(20);
                table.getDefaultCell().setBorder(0);
                table.addCell(header);
                table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell("");
                table.addCell("");
                table.addCell("");
                table.addCell(String.format("Page %d of", writer.getPageNumber()));
                PdfPCell cell = new PdfPCell(Image.getInstance(total));
                cell.setBorder(0);
                table.addCell(cell);
                table.writeSelectedRows(0, -1, 10, 50, writer.getDirectContent());
            }
            catch(DocumentException de) {
                throw new ExceptionConverter(de);
            }
        }
        public void onCloseDocument(PdfWriter writer, Document document) {
          try{  
            ColumnText.showTextAligned(total, Element.ALIGN_LEFT,
                    new Phrase(String.valueOf(writer.getPageNumber() - 1)),
                    2, 2, 0);
             }
          catch(Exception ex){
              
          }
        }
    }     

    
 
}
