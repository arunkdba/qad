package com.reports.pdf;
import com.common.Common;
import java.io.*;
import java.util.*;
import javax.swing.JOptionPane;
import com.reports.classes.Ut3Details;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;


public class UT3CUnitPDF {

    String SFile                    = "";
    Common common                   = new Common();
    
    String[]                         ColumnName,ColumnType,ColumnName1,ColumnType1,ColumnName2,ColumnType2,sBody;
    int[]                            iColumnWidth,iColumnWidth1,iColumnWidth2;

     private static Font catFont     = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
     private static Font redFont     = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);
     private static Font subFont     = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);

     private static Font bigBold     = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
     private static Font bigNormal   = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

     private static Font mediumBold  = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
     private static Font mediumNormal= new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

     private static Font smallBold   = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.BOLD);
     private static Font smallNormal = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.NORMAL);

     private static Font tinyBold    = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.BOLD);
     private static Font tinyNormal  = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.NORMAL);

     private static Font underBold   = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.UNDERLINE);
    
     int    iWidth1[]   = {30,30,30,30};
     int    iWidth2[]   = {20,20,38,20,30,25,20,20,20,20,20,25,20,20,20,20,20,20,20,20,20,20,18,18,18,18,18,18,18,18,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20};
//     int    iWidth2[]   = {20,20,38,20,30,25,20,20,20,20,20,25,20,20,20,20,20};//,20,20,20,20,20,18,18,18,18,18,18,18,18,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20};
     int    iWidth3[]   = {20,20,38,20,30,25,20,20,20,20,20,25,20,20,20,20,20,20,20,20,20,20,18,18,18,18,18,18,18,18,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20};
     
     
     Document   document;
     PdfPTable  table1,table2;
     PdfWriter  writer = null;
     double     dprocess =0;
     String     SProcess="",SRh="";

     
    public void createPDFFile(){
     try{
        
        SFile       =   "UT3CunitPDF.pdf";
        SFile       =   common.getPrintPath()+SFile;    
        document    =   new Document(PageSize.A3.rotate());
        //PdfWriter   .   getInstance(document, new FileOutputStream(SFile));
        writer      =  PdfWriter.getInstance(document, new FileOutputStream(SFile));
        TableHeader event   = new TableHeader();
        writer      .   setPageEvent(event);
        document    .   open();

        table1      =   new PdfPTable(4);
        table1      .   setWidths(iWidth1);
        table1      .   setSpacingAfter(10);
        table1      .   setWidthPercentage(30);
        table1      .   setHorizontalAlignment(Element.ALIGN_LEFT);
        
        table2      =   new PdfPTable(49);
        table2      .   setWidths(iWidth2);
        table2      .   setSpacingAfter(10);
        table2      .   setWidthPercentage(100);
        table2      .   setHeaderRows(2);
        table2      .   setHorizontalAlignment(Element.ALIGN_LEFT);
        
       }
        catch (Exception e){
           e.printStackTrace();
            System.out.println("Error-->"+e);
        }
    }
    
    public void setSITRAStandard(String SFromDate,String SToDate,java.util.List theList) {
        String Str1 = "";
            Str1 = "C-Unit UT3 Report From ";
        try {
            
            Paragraph paragraph;

               paragraph = new Paragraph(Str1+common.parseDate(SFromDate)+" To  "+common.parseDate(SToDate),bigBold);
               paragraph.setAlignment(Element.ALIGN_LEFT);
               paragraph.setSpacingAfter(4);
               document.add(paragraph);

            AddCellIntoTable("SITRA STANDARD", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,4,30f,5,3,8,3, bigBold );
            
            AddCellIntoTable("Count", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, bigBold );
            AddCellIntoTable("Good", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, bigBold );
            AddCellIntoTable("Avg", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, bigBold );
            AddCellIntoTable("Poor", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, bigBold );
            
            for(int i=0;i<theList.size();i++) {
               HashMap     theMap     =  (HashMap)theList.get(i);

                AddCellIntoTable(common.parseNull(String.valueOf(theMap.get("COUNT"))), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, bigNormal );
                AddCellIntoTable(common.parseNull(String.valueOf(theMap.get("GOOD"))), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, bigNormal );
                AddCellIntoTable(common.parseNull(String.valueOf(theMap.get("AVERAGE"))), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, bigNormal );
                AddCellIntoTable(common.parseNull(String.valueOf(theMap.get("POOR"))), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, bigNormal );
            }
            document.add(table1);
//            document.close();
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
     
    public void setUT3OEDataHead() {

         try {
            
            AddHeadCellIntoTable("O.NO", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("SHADE", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("PARTY NAME", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("O.QTY", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("DATE", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("OE NO", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("U%", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("CVM", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("THIN", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("THICK", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("NEPS (+200)", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("NEPS (+280)", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("TOTAL (+200)", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("TOTAL (+280)", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
            
            AddCellIntoTable("NO RE", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,3,35f,5,3,8,3, tinyBold );
          
            AddHeadCellIntoTable("COUNT", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("COUNT CV%", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("STRENGTH", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("STRENGTH CV%", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("CSP", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("SYS", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("RKM", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("RKM CV%", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("ELONG", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("ELONG CV%", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
     
            AddHeadCellIntoTable("VSF", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("DVSF", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("GV", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("DGV", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
         
            AddCellIntoTable("POC", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,3,35f,5,3,8,3, tinyBold );
        
            AddHeadCellIntoTable("DC COM", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
       
            AddCellIntoTable("DYED POC", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,3,35f,5,3,8,3, tinyBold );
     
            AddHeadCellIntoTable("KARDED", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("COMBED", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("BLEACHED COTTON", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("USABLE", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("MIXED", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("DEPTH", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
           
            AddCellIntoTable("COTTON", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
      
            AddHeadCellIntoTable("LOT", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
            
            AddHeadCellIntoTable("OTHERS", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("TOTAL ", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,35f,5,3,8,3, tinyBold );
            
            AddCellIntoTable("Cnt", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,35f,5,3,8,3, tinyBold );
            AddCellIntoTable("Str", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,35f,5,3,8,3, tinyBold );
            AddCellIntoTable("Csp", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,35f,5,3,8,3, tinyBold );
            
            AddCellIntoTable("DCH-32", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,35f,5,3,8,3, tinyBold );
            AddCellIntoTable("LMC", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,35f,5,3,8,3, tinyBold );
            AddCellIntoTable("OTHERS", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,35f,5,3,8,3, tinyBold );
            
            AddCellIntoTable("DCH-32", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,35f,5,3,8,3, tinyBold );
            AddCellIntoTable("LMC", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,35f,5,3,8,3, tinyBold );
            AddCellIntoTable("OTHERS", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,35f,5,3,8,3, tinyBold );
            
            AddCellIntoTable("CARD", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,35f,5,3,8,3, tinyBold );
            AddCellIntoTable("CBD", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,35f,5,3,8,3, tinyBold );
            
//            document.add(table2);
//            document.close();
//            
        } catch (Exception ex) {
            ex.printStackTrace();
             System.out.println("Error Head-->"+ex);
        }
    }
    
    
    
    public void setUT3OEData(java.util.List TestUt3list) {
        try {
                java.util.HashMap theDataMap =new java.util.HashMap();
              
                    double dCsp = 0,dCnt = 0,dStr = 0;
                    double dUper = 0,dUperTot =0;
                    double dvCvm = 0,dTotCvm = 0;
                    double dThin = 0,dThinTot =0;
                    double dThick =0,dThickTot =0;
                    double dNeps = 0,dNepsTot=0;
                    double dNeps280 = 0,dNeps280Tot=0;
                    double dTotal =0,dTotalTot =0;
                    double dTotal200 =0,dTotal200Tot =0;
                    double dTotal280 =0,dTotal280Tot =0;
                    double dCount = 0,dCountTot = 0;
                    double dCountCv = 0,dCountCvTot = 0;
                    double dStrTot = 0,dStrCv = 0;
                    double dStrCvTot = 0, dCspTot = 0;
                    double dSys = 0,dSysTot = 0;
                    double dRkm = 0,dRkmTot = 0;
                    double dRkmCv = 0,dRkmCvTot = 0;
                    double dElang = 0, dElangTot = 0;
                    double dElangCv = 0, dElangCvTot = 0;
                    double dMixPer = 0;
                    int    iUperCount = 0;
                    int    iCvmCount = 0;
                    int    iThinCount=0;
                    int    iThickCount =0;
                    int    iNepsCount = 0;
                    int    iNeps280Count = 0;
                    int    iTotalCount = 0;
                    int    iTotal200Count = 0;
                    int    iTotal280Count = 0;
                    int    iCountCount = 0;
                    int    iCountCvCount =0;
                    int    iStrCount = 0;
                    int    iStrCvCount = 0;
                    int    iCspCount = 0;
                    int    iSysCount = 0;
                    int    iRkmCount = 0;
                    int    iRkmCvCount = 0;
                    int    iElangCount = 0;
                    int    iElangCvCount = 0;
                    String sPrevOrdNo ="";
                    String sPrevParty ="";
                    String sPrevShade = "";
                    String sPrevQty   = "";
                    String sPrevDate  = "";
                    String ColorCode="";
                    for (int i = 0; i < TestUt3list.size(); i++) {
                       Ut3Details details   = (Ut3Details) TestUt3list.get(i);
                       java.util.List ls    = details.getOrderList();
                       String sOrderNo      = details.getOrderno();
                       if(!sPrevOrdNo.equals(common.parseNull(sOrderNo))){
                           
                     AddCellIntoTable(common.parseNull(String.valueOf(details.getCountname())), table2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );  
                     AddCellIntoTable(common.parseNull(String.valueOf(details.getBlendname())), table2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,48,30f,5,3,8,3, tinyNormal );  
                    }
                    for(int j=0;j<ls.size();j++){
                       theDataMap     =  (HashMap)ls.get(j);
                       dCnt = common.toDouble(common.getRound((String)theDataMap.get("COUNT"),2));
                       dStr = common.toDouble(common.getRound((String)theDataMap.get("STRENGTH"),2));
                       dCsp = dCnt*dStr;
                       
                       dUper = common.toDouble(common.getRound((String)theDataMap.get("UPER"),2));
                       dvCvm = common.toDouble(common.getRound((String)theDataMap.get("CVM"),2));
                       dThin = common.toDouble(common.getRound((String)theDataMap.get("THIN"),2));
                       dThick = common.toDouble(common.getRound((String)theDataMap.get("THICK"),2));
                       dNeps  = common.toDouble(common.getRound((String)theDataMap.get("NEPS"),2));
                       dNeps280 = common.toDouble(common.getRound((String)theDataMap.get("NEPS280"),2));
                       
                       dTotal = common.toDouble(common.getRound((String)theDataMap.get("TOTAL"),2));
                       dTotal200 = common.toDouble(common.getRound((String)theDataMap.get("TOT200"),2));
                       dTotal280 = common.toDouble(common.getRound((String)theDataMap.get("TOT280"),2));
                       dCount    = common.toDouble(common.getRound((String)theDataMap.get("COUNT"),2));
                       dCountCv  = common.toDouble(common.getRound((String)theDataMap.get("COUNTCV"),2));
                       dStrCv    = common.toDouble(common.getRound((String)theDataMap.get("STRENGTHCV"),2));
                       dSys      = common.toDouble(common.getRound((String)theDataMap.get("SYS"),2));
                       dRkm      = common.toDouble(common.getRound((String)theDataMap.get("RKM"),2));
                       dRkmCv    = common.toDouble(common.getRound((String)theDataMap.get("RKMCV"),2));
                       dElang    = common.toDouble(common.getRound((String)theDataMap.get("ELANG"),2));
                       dElangCv  = common.toDouble(common.getRound((String)theDataMap.get("ELANGCV"),2));
                       String sLink    = common.parseDate((String)theDataMap.get("DATE"));
//                       String sLink  = "<a class='ajax' href='Ut3ReportDetails?Unit=1&sorderno="+details.getOrderno()+"&datefrom="+(String)theDataMap.get("DATE")+"'>"+common.parseDate((String)theDataMap.get("DATE"))+"</a>";
                      // String sLink  = "<a class='ajax' href='Ut3ReportDetails?sorderno="+(String)theDataMap.get("ORDNO")+"&datefrom="+(String)theDataMap.get("DATE")+"'>"+(String)theDataMap.get("ORDNO")+"</a>";
                       if (dUper != 0) {
                           iUperCount +=1;
                           dUperTot +=dUper;
                       }
                       if (dvCvm != 0) {
                           iCvmCount +=1;
                           dTotCvm +=dvCvm;
                       }
                       if (dThin != 0) {
                           iThinCount +=1;
                           dThinTot +=dThin;
                       }
                       if (dThick != 0) {
                           iThickCount +=1;
                           dThickTot +=dThick;
                       }
                       if (dNeps != 0) {
                           iNepsCount +=1;
                           dNepsTot +=dNeps;
                       }
                       if (dNeps280 != 0) {
                           iNeps280Count +=1;
                           dNeps280Tot +=dNeps280;
                       }
                       if (dTotal != 0) {
                           iTotalCount +=1;
                           dTotalTot +=dTotal;
                       }
                       if (dTotal200 != 0) {
                           iTotal200Count +=1;
                           dTotal200Tot +=dTotal200;
                       }
                       if (dTotal280 != 0) {
                           iTotal280Count +=1;
                           dTotal280Tot +=dTotal280;
                       }
                       if (dCount != 0) {
                           iCountCount +=1;
                           dCountTot +=dCount;
                       }
                       if (dCountCv != 0) {
                           iCountCvCount +=1;
                           dCountCvTot +=dCountCv;
                       }
                       if (dStr !=0){
                           iStrCount += 1;
                           dStrTot += dStr;
                       }
                       if (dStrCv !=0){
                           iStrCvCount += 1;
                           dStrCvTot += dStrCv;
                       }
                       if (dCsp !=0){
                           iCspCount += 1;
                           dCspTot += dCsp;
                       }
                       if (dSys !=0){
                           iSysCount += 1;
                           dSysTot += dSys;
                       }
                       if (dRkm !=0){
                           iRkmCount += 1;
                           dRkmTot += dRkm;
                       }
                       if (dRkmCv !=0){
                           iRkmCvCount += 1;
                           dRkmCvTot += dRkmCv;
                       }
                       if (dElang !=0){
                           iElangCount += 1;
                           dElangTot += dElang;
                       }
                       if (dElangCv !=0){
                           iElangCvCount += 1;
                           dElangCvTot += dElangCv;
                       }
                       dMixPer = common.toDouble(common.getRound((String)theDataMap.get("MIXTOT"),0));                       
                       if(dMixPer<100 || dMixPer>100 ){
                           ColorCode="#E91717";
                       }
                       else
                       {
                          ColorCode=""; 
                       }
                if(j==0){
                    
                    AddCellIntoTable(common.parseNull(String.valueOf(details.getOrderno())), table2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );  
                    AddCellIntoTable(common.parseNull(String.valueOf(details.getShadename())), table2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );  
                    AddCellIntoTable(common.parseNull(String.valueOf(details.getPartyname())), table2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );  
                    AddCellIntoTable(common.parseNull(String.valueOf(details.getOrdweight())), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );  
                
                }else{
                      AddCellIntoTable("", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );  
                      AddCellIntoTable("", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );  
                      AddCellIntoTable("", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );    
                      AddCellIntoTable("", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );  
                  }
                  AddCellIntoTable(sLink, table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );  
                  AddCellIntoTable(details.getNull(common.parseNull((String)theDataMap.get("MACHINE"))), table2, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );  
                  AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("UPER"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );  
                  AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("CVM"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );  
                  AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("THIN"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );  
                  AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("THICK"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );  
                  

                AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("NEPS"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );  
                AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("NEPS280"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );  
                AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("TOT200"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );  
                AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("TOT280"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );  

               AddCellIntoTable(details.getNull(common.parseNull((String)theDataMap.get("CNTREADING"))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
               AddCellIntoTable(details.getNull(common.parseNull((String)theDataMap.get("STRREADING"))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
               AddCellIntoTable(details.getNull(common.parseNull((String)theDataMap.get("CSPREADING"))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
               AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("COUNT"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
               AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("COUNTCV"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
               AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("STRENGTH"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
               AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("STRENGTHCV"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
               AddCellIntoTable(details.getNull(common.getRound(dCsp,2)), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
               AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("SYS"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
               AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("RKM"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
               AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("RKMCV"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );                          
               AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("ELANG"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
               AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("ELANGCV"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
               

                if(j==0){
                
                    AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("VSF"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                    AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("DVSF"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                    AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("GV"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                    AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("DGV"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                    
                
                    AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("POCDCH"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                    AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("POCLMC"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                    AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("POCOTHERS"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                

                   AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("DCCOM"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           

                    AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("DPOCDCH"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                    AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("DPOCLMC"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                    AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("DPOCOTHERS"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           


                AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("KARD"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("COMBED"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("BCOTTON"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("USABLE"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("MIXED"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                
                AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("DEPTH"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                AddCellIntoTable(details.getNull(common.parseNull((String)theDataMap.get("KARDVAR"))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                AddCellIntoTable(details.getNull(common.parseNull((String)theDataMap.get("COMBVAR"))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                AddCellIntoTable(details.getNull(common.parseNull((String)theDataMap.get("LOTNO"))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                
//                AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("BCOTTON"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
//                AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("USABLE"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
//                AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("MODAL"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
//                AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("MIXED"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("OTHERS"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                AddCellIntoTable(details.getNull(common.parseNull(common.getRound((String)theDataMap.get("MIXTOT"),2))), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                
                }else{
                
                AddCellIntoTable("", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                AddCellIntoTable("", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                AddCellIntoTable("", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                AddCellIntoTable("", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                AddCellIntoTable("", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                AddCellIntoTable("", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                AddCellIntoTable("", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                AddCellIntoTable("", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                AddCellIntoTable("", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                AddCellIntoTable("", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                AddCellIntoTable("", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                AddCellIntoTable("", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                AddCellIntoTable("", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                AddCellIntoTable("", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                AddCellIntoTable("", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                AddCellIntoTable("", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                AddCellIntoTable("", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                AddCellIntoTable("", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                AddCellIntoTable("", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
//                AddCellIntoTable("", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                AddCellIntoTable("", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                AddCellIntoTable("", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                AddCellIntoTable("", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyNormal );           
                }
                 sPrevOrdNo     = common.parseNull(details.getOrderno());
                 sPrevDate      = common.parseNull((String)theDataMap.get("DATE"));
                 sPrevParty     = common.parseNull((String)theDataMap.get("PARTY"));
               }
          
             AddCellIntoTable("AVG", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,6,30f,5,3,8,3, tinyBold );                  
             AddCellIntoTable(details.getNull(common.getRound((dUperTot/iUperCount),2)), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );           
             AddCellIntoTable(details.getNull(common.getRound((dTotCvm/iCvmCount),2)), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );           
             AddCellIntoTable(details.getNull(common.getRound((dThinTot/iThinCount),2)), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );           
             AddCellIntoTable(details.getNull(common.getRound((dThickTot/iThickCount),2)), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );           

             AddCellIntoTable(details.getNull(common.getRound((dNepsTot/iNepsCount),2)), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );           
             AddCellIntoTable(details.getNull(common.getRound((dNeps280Tot/iNeps280Count),2)), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );           
             AddCellIntoTable(details.getNull(common.getRound((dTotal200Tot/iTotal200Count),2)), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );           
             AddCellIntoTable(details.getNull(common.getRound((dTotal280Tot/iTotal280Count),2)), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );           
             
             
             AddCellIntoTable("", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );                  
             AddCellIntoTable("", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );                  
             AddCellIntoTable("", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );                  


             AddCellIntoTable(details.getNull(common.getRound((dCountTot/iCountCount),2)), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );           
             AddCellIntoTable(details.getNull(common.getRound((dCountCvTot/iCountCvCount),2)), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );           
             AddCellIntoTable(details.getNull(common.getRound((dStrTot/iStrCount),2)), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );           
             AddCellIntoTable(details.getNull(common.getRound((dStrCvTot/iStrCvCount),2)), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );           
             AddCellIntoTable(details.getNull(common.getRound((dCspTot/iCspCount),2)), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );           
             AddCellIntoTable(details.getNull(common.getRound((dSysTot/iSysCount),2)), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );           
             AddCellIntoTable(details.getNull(common.getRound((dRkmTot/iRkmCount),2)), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );           
             AddCellIntoTable(details.getNull(common.getRound((dRkmCvTot/iRkmCvCount),2)), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );           
             AddCellIntoTable(details.getNull(common.getRound((dElangTot/iElangCount),2)), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );           
             AddCellIntoTable(details.getNull(common.getRound((dElangCvTot/iElangCvCount),2)), table2, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );           
             
             AddCellIntoTable("", table2, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,23,30f,5,3,8,3, tinyBold );                  
             
                    dUperTot =0;
                    dUper = 0;
                    iUperCount =0;
                    dvCvm = 0;
                    dTotCvm = 0;
                    iCvmCount = 0;
                    dThin = 0;
                    dThinTot =0;
                    iThinCount=0;
                    dThick = 0;
                    dThickTot =0;
                    iThickCount=0;
                    dNeps = 0;
                    dNepsTot = 0;
                    iNepsCount = 0;
                    dNeps280 = 0;
                    dNeps280Tot = 0;
                    iNeps280Count = 0;
                    dTotal = 0;
                    dTotalTot = 0;
                    iTotalCount = 0;
                    dTotal200 = 0;
                    dTotal200Tot = 0;
                    iTotal200Count = 0;
                    dTotal280 = 0;
                    dTotal280Tot = 0;
                    iTotal280Count = 0;
                    dCount = 0;
                    dCountTot = 0;
                    iCountCount = 0;
                    dCountCv=0;
                    dCountCvTot = 0;
                    iCountCvCount = 0;
                    dStr = 0;
                    dStrTot = 0;
                    iStrCount = 0;
                    dStrCv = 0;
                    dStrCvTot = 0;
                    iStrCvCount = 0;
                    dCsp = 0;
                    dCspTot = 0;
                    iCspCount = 0;
                    dSys =0;
                    dSysTot = 0;
                    iSysCount = 0;
                    dRkm = 0;
                    dRkmTot = 0;
                    iRkmCount = 0;
                    dRkmCv = 0;
                    dRkmCvTot = 0;
                    iRkmCvCount = 0;
                    dElang = 0;
                    dElangTot = 0;
                    iElangCount = 0;
                    dElangCv = 0;
                    dElangCvTot = 0;
                    iElangCvCount = 0;
              }
            document.add(table2);
            document.close();
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
   
    
    private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      } 
    
    private void AddHeadCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iRowSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setRowspan(iRowSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
    
    private void AddCellIntoTable1(String Str,PdfPTable table,int iHorizontal,int iVertical,int iRowSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setRowspan(iRowSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
	  private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          //c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
	 private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,int iRowSpan)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          //c1.setRowspan(iRowSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
	  private void AddCellTable(Image image,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder)
      {
          PdfPCell c1 = new PdfPCell();
		  c1.addElement(new Chunk(image, 5, -5));
          //c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);

		  // image.scalePercent(90f);
      	  // paragraph.add(new Chunk(image, -1f, 1f));
      }
	 private void AddCellIntoTable(Image image,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder)
      {
          PdfPCell c1 = new PdfPCell();
		  //c1.addElement(new Chunk(image, 5, -5));
	  c1.addElement(image);
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);

      }
     private void AddCellIntoTable(double dValue,int iRound,int iRad,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder)
      {
           Double DValue = new Double(dValue);
           String Str;
           if (DValue.isNaN() || dValue ==0)
           {
               Str="";
           }
           else
           {
               Str=(common.Rad(common.getRound(dValue,iRound),iRad));
           }

          PdfPCell c1 = new PdfPCell(new Phrase(Str));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);

      }
     
     public void AddCellIntoTable2(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }
    
     class TableHeader extends PdfPageEventHelper {
        String header;
        PdfTemplate total;
        public void setHeader(String header) {
            this.header = header;
      }
      public void onOpenDocument(PdfWriter writer, Document document) {
            try{
            total = writer.getDirectContent().createTemplate(30, 16);
            }
            catch(Exception ex){
            }
      }

        public void onEndPage(PdfWriter writer, Document document) {
            PdfPTable table = new PdfPTable(3);
            try {
                table.setWidths(new int[]{24, 24, 2});
                table.setTotalWidth(520);
                table.setLockedWidth(true);
                table.getDefaultCell().setFixedHeight(20);
                table.getDefaultCell().setBorder(0);
                table.addCell(header);
                table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell("");
                table.addCell("");
                table.addCell("");
                table.addCell(String.format("Page %d of", writer.getPageNumber()));
                PdfPCell cell = new PdfPCell(Image.getInstance(total));
                cell.setBorder(0);
                table.addCell(cell);
                table.writeSelectedRows(0, -1, 10, 50, writer.getDirectContent());
            }
            catch(DocumentException de) {
                throw new ExceptionConverter(de);
            }
        }
        public void onCloseDocument(PdfWriter writer, Document document) {
          try{  
            ColumnText.showTextAligned(total, Element.ALIGN_LEFT,
                    new Phrase(String.valueOf(writer.getPageNumber() - 1)),
                    2, 2, 0);
             }
          catch(Exception ex){
              
          }
        }
    }
    
}
