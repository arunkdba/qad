package com.reports.pdf;

import com.common.Common;
import java.io.*;
import java.util.*;
import javax.swing.JOptionPane;
import com.reports.classes.Ut3Details;
import com.reports.classes.CoEffVarient;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfBorderDictionary;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.RadioCheckField;
import com.itextpdf.text.pdf.PushbuttonField;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;

public class UT3TestWisePDF {
    String SFile                    = "";
    Common common                   = new Common();
    CoEffVarient    cev             = new CoEffVarient();
    
    String[]                         ColumnName,ColumnType,ColumnName1,ColumnType1,ColumnName2,ColumnType2,sBody;
    int[]                            iColumnWidth,iColumnWidth1,iColumnWidth2;

     private static Font catFont     = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
     private static Font redFont     = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);
     private static Font subFont     = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);

     private static Font bigBold     = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
     private static Font bigNormal   = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

     private static Font mediumBold  = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
     private static Font mediumNormal= new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

     private static Font smallBold   = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.BOLD);
     private static Font smallNormal = new Font(Font.FontFamily.TIMES_ROMAN, 8, Font.NORMAL);

     private static Font tinyBold    = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
     private static Font tinyNormal  = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

     private static Font underBold   = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.UNDERLINE);
    
     int    iWidth1[]   = {30,30,30,30,30,30,30,30,30,30,30,30,40,40,40};
     
     Document   document;
     PdfPTable  table1;
     PdfWriter  writer = null;
     double     dprocess =0;
     String     SProcess="",SRh="";

     
    public void createPDFFile(){
     try{
        
        SFile       =   "UT3TestWisePDF.pdf";
        SFile       =   common.getPrintPath()+SFile;    
        document    =   new Document(PageSize.LEGAL.rotate());
        //PdfWriter   .   getInstance(document, new FileOutputStream(SFile));
        writer      =  PdfWriter.getInstance(document, new FileOutputStream(SFile));
        TableHeader event   = new TableHeader();
        writer      .   setPageEvent(event);
        document    .   open();

        table1      =   new PdfPTable(15);
        table1      .   setWidths(iWidth1);
        table1      .   setSpacingAfter(8);
        table1      .   setWidthPercentage(100);
        table1      .   setHorizontalAlignment(Element.ALIGN_LEFT);
        table1      .   setHeaderRows(5);
        
       }
        catch (Exception e){
           e.printStackTrace();
            System.out.println("Error-->"+e);
        }
    }
    
    public void setTestBase (String SDate,String STDate,java.util.HashMap theBaseMap)
    {
        String Str1    = "Company    : AMARJOTHI SPINNING MILLS LIMITED";
        String Str2    = "Document   : UT3 TEST REPORT As on  ";
        
        try
        {
            AddCellIntoTable(Str1, table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,15,25f,0,0,0,0, bigBold);
            AddCellIntoTable(Str2+common.parseDate(SDate), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,15,25f,0,0,0,0, bigBold);
            AddCellIntoTable(" ", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,15,25f,0,0,0,0, bigBold);
           
            AddCellIntoTable("Art.No :"+common.parseNull((String)theBaseMap.get("ARTNO")), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,5,25f,5,3,8,3, bigBold);
            AddCellIntoTable("Test No :"+common.parseNull((String)theBaseMap.get("TESTID")), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,5,25f,5,3,8,3, bigBold);
            AddCellIntoTable("Fiber Assembly : "+common.parseNull((String)theBaseMap.get("FIBREASSEMBLY")), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,5,25f,5,3,8,3, bigBold);

            AddCellIntoTable("Fiber : "+common.parseNull((String)theBaseMap.get("FIBER")), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,5,25f,5,3,8,3, bigBold);
            AddCellIntoTable("Order No : "+common.parseNull((String)theBaseMap.get("ORDERNO")), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,5,25f,5,3,8,3, bigBold);
            AddCellIntoTable(" Unit :"+common.parseNull((String)theBaseMap.get("UNIT")), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,5,25f,5,3,8,3, bigBold);

            AddCellIntoTable("V : "+common.parseNull((String)theBaseMap.get("VDATA"))+" lbs", table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,5,25f,5,3,8,3, bigBold);
            AddCellIntoTable("T : "+common.parseNull((String)theBaseMap.get("TDATA")), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,5,25f,5,3,8,3, bigBold);
            AddCellIntoTable("Tests :"+common.parseNull((String)theBaseMap.get("TESTS")), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,5,25f,5,3,8,3, bigBold);
            
            AddCellIntoTable("Slot  : "+common.parseNull((String)theBaseMap.get("SLOT")), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,5,25f,5,3,8,3, bigBold);
            AddCellIntoTable("Yarn Tension :  "+common.parseNull((String)theBaseMap.get("YARNTENSION")), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,5,25f,5,3,8,3, bigBold);
            AddCellIntoTable("Imperfections : "+common.parseNull((String)theBaseMap.get("IMPERFECTIONS")), table1, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,5,25f,5,3,8,3, bigBold);
            
//            document.add(table1);
//            document.close();
          
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
    public void setHead(String SDate){
	
        try
        {
            AddHeadCellIntoTable("S.No", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("Test No", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("UM", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("CVM", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("CVM(1m)", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("CVM(3m)", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("Index", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("Thin(-30%)", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("Thin(-40%)", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("Thin(-50%)", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            
            AddHeadCellIntoTable("Thin(+35%)", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("Thin(+50%)", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("Neps(+140%)", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("Neps(+200%)", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            AddHeadCellIntoTable("Neps(+280%)", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,30f,5,3,8,3, tinyBold );
            
        }
        catch(Exception Ex)
        {
                Ex.printStackTrace();
        }
    }
    
    public void setPrintData(String SDate,java.util.List TestList,int iTotalTest){
	
        try
        {
            
            String[] UmArray = new String[iTotalTest];
            String[] CvmArray = new String[iTotalTest];
            String[] Cvm1Array = new String[iTotalTest];
            String[] Cvm3Array = new String[iTotalTest];
            String[] IndexArray = new String[iTotalTest];
            String[] Thin30Array = new String[iTotalTest];
            String[] Thin40Array = new String[iTotalTest];
            String[] Thin50Array = new String[iTotalTest];
            String[] Thin35Array = new String[iTotalTest];
            String[] ThinP50Array = new String[iTotalTest];
            String[] Neps140Array = new String[iTotalTest];
            String[] Neps200Array = new String[iTotalTest];
            String[] Neps280Array = new String[iTotalTest];
            int irow=0;
            
            for (int i = 0; i < TestList.size(); i++) {
                
                irow=irow+1;
                java.util.HashMap theMap = (java.util.HashMap)TestList.get(i);
                UmArray[i] = common.parseNull((String)theMap.get("UM"));
                CvmArray[i]   = common.parseNull((String)theMap.get("CVM"));
                Cvm1Array[i]   = common.parseNull((String)theMap.get("CVM1M"));
                Cvm3Array[i]   = common.parseNull((String)theMap.get("CVM3M"));
                IndexArray[i]   = common.parseNull((String)theMap.get("INDEXX"));
                Thin30Array[i]  = common.parseNull((String)theMap.get("THINMI30"));      
                Thin40Array[i]  = common.parseNull((String)theMap.get("THINMI40"));
                Thin50Array[i]  = common.parseNull((String)theMap.get("THINMI50"));
                Thin35Array[i]  = common.parseNull((String)theMap.get("THICKPL35"));
                ThinP50Array[i] = common.parseNull((String)theMap.get("THICKPL50"));
                Neps140Array[i] = common.parseNull((String)theMap.get("NEPSPL140"));
                Neps200Array[i] = common.parseNull((String)theMap.get("NEPSPL200"));
                Neps280Array[i] = common.parseNull((String)theMap.get("NEPSPL280"));
                
                AddHeadCellIntoTable(String.valueOf(irow), table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
                AddHeadCellIntoTable(common.parseNull((String)theMap.get("TESTNO")), table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
                AddHeadCellIntoTable(common.parseNull((String)theMap.get("UM")), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
                AddHeadCellIntoTable(common.parseNull((String)theMap.get("CVM")), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
                AddHeadCellIntoTable(common.parseNull((String)theMap.get("CVM1M")), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
                AddHeadCellIntoTable(common.parseNull((String)theMap.get("CVM3M")), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
                AddHeadCellIntoTable(common.parseNull((String)theMap.get("INDEXX")), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
                AddHeadCellIntoTable(common.parseNull((String)theMap.get("THINMI30")), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
                AddHeadCellIntoTable(common.parseNull((String)theMap.get("THINMI40")), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
                AddHeadCellIntoTable(common.parseNull((String)theMap.get("THINMI50")), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
                AddHeadCellIntoTable(common.parseNull((String)theMap.get("THICKPL35")), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
                AddHeadCellIntoTable(common.parseNull((String)theMap.get("THICKPL50")), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
                AddHeadCellIntoTable(common.parseNull((String)theMap.get("NEPSPL140")), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
                AddHeadCellIntoTable(common.parseNull((String)theMap.get("NEPSPL200")), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
                AddHeadCellIntoTable(common.parseNull((String)theMap.get("NEPSPL280")), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            }
                AddCellIntoTable("Mean Value", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,20f,5,3,8,3, bigBold );
                AddHeadCellIntoTable(common.getRound(cev.getMean(UmArray),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyBold );
                AddHeadCellIntoTable(common.getRound(cev.getMean(CvmArray),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyBold );
                AddHeadCellIntoTable(common.getRound(cev.getMean(Cvm1Array),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyBold );
                AddHeadCellIntoTable(common.getRound(cev.getMean(Cvm3Array),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyBold );
                AddHeadCellIntoTable(common.getRound(cev.getMean(IndexArray),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyBold );
                AddHeadCellIntoTable(common.getRound(cev.getMean(Thin30Array),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyBold );
                AddHeadCellIntoTable(common.getRound(cev.getMean(Thin40Array),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyBold );
                AddHeadCellIntoTable(common.getRound(cev.getMean(Thin50Array),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyBold );
                AddHeadCellIntoTable(common.getRound(cev.getMean(Thin35Array),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyBold );
                AddHeadCellIntoTable(common.getRound(cev.getMean(ThinP50Array),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyBold );
                AddHeadCellIntoTable(common.getRound(cev.getMean(Neps140Array),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyBold );
                AddHeadCellIntoTable(common.getRound(cev.getMean(Neps200Array),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyBold );
                AddHeadCellIntoTable(common.getRound(cev.getMean(Neps280Array),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyBold );
                
                AddCellIntoTable("CV(%)", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,20f,5,3,8,3, bigBold );
                AddHeadCellIntoTable(common.getRound(cev.getCoEffVant(UmArray),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyBold );
                AddHeadCellIntoTable(common.getRound(cev.getCoEffVant(CvmArray),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyBold );
                AddHeadCellIntoTable(common.getRound(cev.getCoEffVant(Cvm1Array),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyBold );
                AddHeadCellIntoTable(common.getRound(cev.getCoEffVant(Cvm3Array),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyBold );
                AddHeadCellIntoTable(common.getRound(cev.getCoEffVant(IndexArray),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyBold );
                AddHeadCellIntoTable(common.getRound(cev.getCoEffVant(Thin30Array),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyBold );
                AddHeadCellIntoTable(common.getRound(cev.getCoEffVant(Thin40Array),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyBold );
                AddHeadCellIntoTable(common.getRound(cev.getCoEffVant(Thin50Array),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyBold );
                AddHeadCellIntoTable(common.getRound(cev.getCoEffVant(Thin35Array),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyBold );
                AddHeadCellIntoTable(common.getRound(cev.getCoEffVant(ThinP50Array),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyBold );
                AddHeadCellIntoTable(common.getRound(cev.getCoEffVant(Neps140Array),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyBold );
                AddHeadCellIntoTable(common.getRound(cev.getCoEffVant(Neps200Array),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyBold );
                AddHeadCellIntoTable(common.getRound(cev.getCoEffVant(Neps280Array),2), table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyBold );
                
                AddCellIntoTable("Q95% +/-", table1, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,2,20f,5,3,8,3, bigBold );
                AddHeadCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
                AddHeadCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
                AddHeadCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
                AddHeadCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
                AddHeadCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
                AddHeadCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
                AddHeadCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
                AddHeadCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
                AddHeadCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
                AddHeadCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
                AddHeadCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
                AddHeadCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
                AddHeadCellIntoTable("", table1, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE,1,20f,5,3,8,3, tinyNormal );
            
            document.add(table1);            
            document.close();
            
        }
        catch(Exception Ex)
        {
                Ex.printStackTrace();
        }
    }
    
    
    
    private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      } 
    
    private void AddHeadCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iRowSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setRowspan(iRowSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
    
    private void AddCellIntoTable1(String Str,PdfPTable table,int iHorizontal,int iVertical,int iRowSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setRowspan(iRowSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
	  private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          //c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
	 private void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,int iRowSpan)
      {
          PdfPCell c1 = new PdfPCell(new Phrase(Str));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          //c1.setRowspan(iRowSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
      }
	  private void AddCellTable(Image image,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder)
      {
          PdfPCell c1 = new PdfPCell();
		  c1.addElement(new Chunk(image, 5, -5));
          //c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);

		  // image.scalePercent(90f);
      	  // paragraph.add(new Chunk(image, -1f, 1f));
      }
	 private void AddCellIntoTable(Image image,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder)
      {
          PdfPCell c1 = new PdfPCell();
		  //c1.addElement(new Chunk(image, 5, -5));
	  c1.addElement(image);
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);

      }
     private void AddCellIntoTable(double dValue,int iRound,int iRad,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder)
      {
           Double DValue = new Double(dValue);
           String Str;
           if (DValue.isNaN() || dValue ==0)
           {
               Str="";
           }
           else
           {
               Str=(common.Rad(common.getRound(dValue,iRound),iRad));
           }

          PdfPCell c1 = new PdfPCell(new Phrase(Str));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);

      }
     
     public void AddCellIntoTable2(String Str, PdfPTable table, int iHorizontal, int iVertical, int iColSpan, float fHeight, int iLeftBorder, int iTopBorder, int iCENTERBorder, int iBottomBorder, Font DocFont) {
        PdfPCell c1 = new PdfPCell(new Phrase(Str, DocFont));
        c1.setFixedHeight(fHeight);
        c1.setHorizontalAlignment(iHorizontal);
        c1.setVerticalAlignment(iVertical);
        c1.setColspan(iColSpan);
        c1.setBorder(iLeftBorder | iTopBorder | iCENTERBorder | iBottomBorder);
        table.addCell(c1);
    }
    
     class TableHeader extends PdfPageEventHelper {
        String header;
        PdfTemplate total;
        public void setHeader(String header) {
            this.header = header;
      }
      public void onOpenDocument(PdfWriter writer, Document document) {
            try{
            total = writer.getDirectContent().createTemplate(30, 16);
            }
            catch(Exception ex){
            }
      }

        public void onEndPage(PdfWriter writer, Document document) {
            PdfPTable table = new PdfPTable(3);
            try {
                table.setWidths(new int[]{24, 24, 2});
                table.setTotalWidth(520);
                table.setLockedWidth(true);
                table.getDefaultCell().setFixedHeight(20);
                table.getDefaultCell().setBorder(0);
                table.addCell(header);
                table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell("");
                table.addCell("");
                table.addCell("");
                table.addCell(String.format("Page %d of", writer.getPageNumber()));
                PdfPCell cell = new PdfPCell(Image.getInstance(total));
                cell.setBorder(0);
                table.addCell(cell);
                table.writeSelectedRows(0, -1, 10, 50, writer.getDirectContent());
            }
            catch(DocumentException de) {
                throw new ExceptionConverter(de);
            }
        }
        public void onCloseDocument(PdfWriter writer, Document document) {
          try{  
            ColumnText.showTextAligned(total, Element.ALIGN_LEFT,
                    new Phrase(String.valueOf(writer.getPageNumber() - 1)),
                    2, 2, 0);
             }
          catch(Exception ex){
              
          }
        }
    }
       
}
