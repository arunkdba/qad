/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.print;

import com.common.Common;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 *
 * @author admin
 */
public class LowCspReportPrint {

    private String SprnHead = "";
    FileWriter fw;
    int Lctr = 0, Pctr = 1;
    int iLineSize = 0;
    Common common = new Common();

    public LowCspReportPrint() {
    }

    public void printData(String printbody, String Sdate, String Todate, String SProcess,String SUnitName) {
        try {
            fw.write(printbody);
            Lctr += 1;
            iLineSize = (SprnHead.length()) / 3;
            fw.write(common.Replicate("-", iLineSize+iLineSize+iLineSize) + "\n");
            if (Lctr == 26) //36
            {
                fw.write("Report Taken on :" + common.parseDate(common.getSerDateTime()) + "\n");
                Pctr += 1;
                setTestBase(Sdate, Todate, SProcess,SUnitName);
                setHead(Sdate,SUnitName);
                Lctr = 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setHead(String SDate,String SUnitName) {
        SprnHead = "";
        try {
            
            SprnHead = SprnHead + common.Pad("| S.No", 6) + "|";
            SprnHead = SprnHead + common.Cad("Count", 6) + "|";
            SprnHead = SprnHead + common.Cad("O.No", 13) + "|";
            SprnHead = SprnHead + common.Cad("Shade", 21) + "|";
            SprnHead = SprnHead + common.Pad("Machine", 16) + "|";
                        
            SprnHead = SprnHead + common.Cad("Act Cnt", 8) + "|";
            SprnHead = SprnHead + common.Cad("Csp", 8) + "|";
            SprnHead = SprnHead + common.Cad("TPI", 7) + "|";
            SprnHead = SprnHead + common.Cad("TPI", 8) + "|";
            SprnHead = SprnHead + common.Cad("Act Cnt", 8) + "|";
            SprnHead = SprnHead + common.Cad("Csp", 8) + "|";
            
            if(SUnitName.equals("All")){
            SprnHead = SprnHead+common.Cad("Unit", 10)+"|";
            }
            iLineSize = (SprnHead.length())/2;
            fw.write(common.Replicate("-",iLineSize+iLineSize)+"\n");
            fw.write(SprnHead+"\n");
            fw.write(common.Replicate("-",iLineSize+iLineSize)+"\n");
            Lctr = 0;
        } catch (Exception Ex) {
            Ex.printStackTrace();
        }
    }

    public void setTestBase(String SDate, String Todate, String SProcess,String SUnitName) {
        String Str1 = "Company   : AMARJOTHI SPINNING MILLS LIMITED\n";
        String Str2 = "Document   : LOW CSP ORDER REPORT FROM ";
        String Str3 = "Unit       :  " + SUnitName + "\n";
        String Str4 = "Process    :  " + SProcess + "\n";
        SprnHead = "";
        try {
            fw.write(Str1);
            fw.write(Str2 + common.parseDate(SDate) + " TO "+common.parseDate(Todate)+" \n");
            fw.write("Page" + common.Space(7) + ": " + Pctr + "FPE\n");
            fw.write(Str3);
            fw.write(Str4);
            fw.write("Page" + common.Space(7) + ": " + Pctr + "P\n");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void createPrn() { 
        try {
            String Sos = System.getProperty("os.name").toLowerCase();
            if (Sos.trim().startsWith("wind")) {
                fw = new FileWriter("d:/LowCsp.prn");
            } else {
                fw = new FileWriter("//software/QcPrint/LowCsp.prn");
            }
            //printData(sData);
            //fw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Closefile() throws IOException {
        fw.write("Report Taken on :" + common.parseDate(common.getSerDateTime()) + "\n");
        fw.close();
    }

    public java.util.List getShadeLines(String Shade, int iLength) {
        java.util.List theShadeList = new java.util.ArrayList();
        int iLen = 0;
        int iLen1 = 0;
        int iLen2 = 0;

        BigDecimal bd1, bd2;
        bd1 = new BigDecimal(Shade.length());
        bd2 = new BigDecimal(iLength);
        iLen = bd1.divide(bd2, RoundingMode.UP).intValue();

        iLen1 = 0;
        iLen2 = iLength;

        for (int i = 0; i < iLen; i++) {
            if (i == (iLen - 1)) {
                theShadeList.add(Shade.substring(iLen1));
            } else {
                theShadeList.add(Shade.substring(iLen1, iLen2));
                iLen1 = iLen2;
                iLen2 += iLength;
            }
        }
        return theShadeList;
    }

    public java.util.List getCanLines(String[] str, int iLength) {
        java.util.List theCanList = new java.util.ArrayList();
        StringBuffer sb = new StringBuffer();
        String SData = "";

        int iLen = 0;
        iLen = iLength;

        for (int i = 0; i < str.length; i++) {

            SData = common.parseNull((String) str[i]);

            if (i == (str.length - 1)) {

                if (SData.length() <= iLen) {
                    sb.append(SData);
                    iLen = iLen - SData.length();
                    theCanList.add(sb);
                } else if (SData.length() > iLen) {
                    theCanList.add(sb);
                    sb = new StringBuffer();
                    iLen = iLength;
                    sb.append(SData);
                    iLen = iLen - SData.length();
                    theCanList.add(sb);
                }
            } else {
                if (SData.length() <= iLen) {
                    sb.append(SData).append(",");
                    iLen = iLen - (SData.length() + 1);
                } else if (SData.length() > iLen) {

                    theCanList.add(sb);
                    sb = new StringBuffer();
                    iLen = iLength;
                    sb.append(SData).append(",");
                    iLen = iLen - (SData.length() + 1);
                }
            }

        }
        return theCanList;
    }

    public int getMaxSize(int isize1, int isize2, int isize3, int isize4, int isize5) {
        int largest = java.util.Collections.max(java.util.Arrays.asList(isize1, isize2, isize3, isize4, isize5));
        return largest;
    }
}