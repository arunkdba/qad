/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.print;

import com.common.Common;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 *
 * @author admin
 */
public class MachineReportPrint {

    private String SprnHead = "";
    FileWriter fw;
    int Lctr = 0, Pctr = 1;
    int iLineSize = 0;
    Common common = new Common();

    public MachineReportPrint() {
    }

    public void printData(String printbody, String Sdate, String SUnit, String SProcess,String sExtCount) {
        try {
            fw.write(printbody);
            Lctr += 1;
            iLineSize = (SprnHead.length()) / 3;
            fw.write(common.Replicate("-", iLineSize) + "\n");
            if (Lctr == 26) //36
            {
                fw.write("Report Taken on :" + common.parseDate(common.getSerDateTime()) + "\n");
                Pctr += 1;
                setTestBase(Sdate, SUnit, SProcess,sExtCount);
                setHead(Sdate);
                Lctr = 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setHead(String SDate) {
        SprnHead = "";
        try {

            SprnHead = (SprnHead + common.Pad("| ", 6) + "|");
            SprnHead = (SprnHead + common.Pad(" ", 16) + "|");
            SprnHead = (SprnHead + common.Pad(" ", 4) + "|");
            SprnHead = (SprnHead + common.Cad("Count ", 39) + "|");
            SprnHead = (SprnHead + common.Pad(" ", 6) + "|");
            SprnHead = (SprnHead + common.Pad(" ", 6) + "|");
            SprnHead = (SprnHead + common.Pad(" ", 6) + "|");
            SprnHead = (SprnHead + common.Pad(" ", 6) + "|");
            SprnHead = (SprnHead + common.Pad(" ", 8) + "|");
            SprnHead = (SprnHead + common.Pad(" ", 6) + "|");
            SprnHead = (SprnHead + common.Pad(" ", 6) + "|");
            SprnHead = (SprnHead + common.Pad(" ", 10) + "|");
            SprnHead = (SprnHead + common.Pad(" ", 8) + "|");
            SprnHead = (SprnHead + common.Pad(" ", 6) + "|");
            SprnHead = (SprnHead + common.Pad(" ", 13) + "|");
            SprnHead = (SprnHead + common.Pad(" ", 7) + "|");
            SprnHead = (SprnHead + common.Pad(" ", 21) + "|");
            SprnHead = (SprnHead + common.Pad(" ", 22) + "|");
            SprnHead = (SprnHead + common.Pad(" ", 5) + "|");
            SprnHead = (SprnHead + common.Pad(" ", 5) + "|\n");

            SprnHead = (SprnHead + common.Pad("| ", 6) + "|");
            SprnHead = (SprnHead + common.Pad(" ", 16) + "|");
            SprnHead = (SprnHead + common.Pad("Act", 4) + "|");
            SprnHead = (SprnHead + common.Pad("---------------------------------------", 39) + "|");
            SprnHead = (SprnHead + common.Pad(" ", 6) + "|");
            SprnHead = (SprnHead + common.Pad(" ", 6) + "|");
            SprnHead = (SprnHead + common.Pad(" ", 6) + "|");
            SprnHead = (SprnHead + common.Pad(" ", 6) + "|");
            SprnHead = (SprnHead + common.Pad(" ", 8) + "|");
            SprnHead = (SprnHead + common.Pad(" ", 6) + "|");
            SprnHead = (SprnHead + common.Pad(" ", 6) + "|");
            SprnHead = (SprnHead + common.Pad(" ", 10) + "|");
            SprnHead = (SprnHead + common.Pad(" ", 8) + "|");
            SprnHead = (SprnHead + common.Pad(" ", 6) + "|");
            SprnHead = (SprnHead + common.Pad(" ", 13) + "|");
            SprnHead = (SprnHead + common.Pad(" ", 7) + "|");
            SprnHead = (SprnHead + common.Pad(" ", 21) + "|");
            SprnHead = (SprnHead + common.Pad(" ", 22) + "|");
            SprnHead = (SprnHead + common.Pad(" ", 5) + "|");
            SprnHead = (SprnHead + common.Pad(" ", 5) + "|\n");

            SprnHead = (SprnHead + common.Pad("| S.No", 6) + "|");
            SprnHead = (SprnHead + common.Pad("Machine", 16) + "|");
            SprnHead = (SprnHead + common.Cad("Cnt", 4) + "|");
            SprnHead = (SprnHead + common.Cad("1", 7) + "|");
            SprnHead = (SprnHead + common.Cad("2", 7) + "|");
            SprnHead = (SprnHead + common.Cad("3", 7) + "|");
            SprnHead = (SprnHead + common.Cad("4", 7) + "|");
            SprnHead = (SprnHead + common.Cad("5", 7) + "|");
            SprnHead = (SprnHead + common.Cad("CntAvg ", 6) + "|");
            SprnHead = (SprnHead + common.Cad("CntCV%", 6) + "|");
            SprnHead = (SprnHead + common.Cad("StrAvg ", 6) + "|");
            SprnHead = (SprnHead + common.Cad("StrCV%", 6) + "|");
            SprnHead = (SprnHead + common.Cad("Csp Avg ", 8) + "|");
            SprnHead = (SprnHead + common.Cad("CspCV%", 6) + "|");
            SprnHead = (SprnHead + common.Cad("RHCVal ", 6) + "|");

            SprnHead = (SprnHead + common.Cad("Change Adv%", 10) + "|");

            SprnHead = (SprnHead + common.Cad("Cp", 8) + "|");
            SprnHead = (SprnHead + common.Cad("TPI", 6) + "|");
            SprnHead = (SprnHead + common.Cad("O.No", 13) + "|");
            SprnHead = (SprnHead + common.Pad("Weight", 7) + "|");
            SprnHead = (SprnHead + common.Cad("Shade", 21) + "|");

            SprnHead = (SprnHead + common.Cad("Pump/Cheese", 22) + "|");
            SprnHead = (SprnHead + common.Cad("Draft", 5) + "|");
            SprnHead = (SprnHead + common.Cad("B.D", 5) + "|\n");

            iLineSize = (SprnHead.length()) / 3;
            fw.write(common.Replicate("-", iLineSize) + "\n");

            //fw.write(SprnHead + "\n");
            fw.write(SprnHead);
            fw.write(common.Replicate("-", iLineSize) + "\n");
            Lctr = 0;
        } catch (Exception Ex) {
            Ex.printStackTrace();
        }
    }

    public void setTestBase(String SDate, String SUnit, String SProcess,String sExtCount) {
        String Str1 = "Company   : AMARJOTHI SPINNING MILLS LIMITED\n";
        String Str2 = "Document   : SPINNING / OE WRAPPING TEST REPORT AS ON ";
        String Str3 = "Unit       :  " + SUnit + "\n";
        String Str4 = "Process    :  " + SProcess + "\n";
        String Str5 = "ExactCount :  " + sExtCount + "\n";
        SprnHead = "";
        try {
            fw.write(Str1);
            fw.write(Str2 + common.parseDate(SDate) + " \n");
            fw.write("Page" + common.Space(7) + ": " + Pctr + "FPE\n");
            fw.write(Str3);
            fw.write(Str4);
            if(!sExtCount.equals("All")){
              fw.write(Str5);
            }
            fw.write("Page" + common.Space(7) + ": " + Pctr + "P\n");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void createPrn() {
        try {
            String Sos = System.getProperty("os.name").toLowerCase();
            if (Sos.trim().startsWith("wind")) {
                fw = new FileWriter("d:/MachineWiseTest.prn");
            } else {
                //fw = new FileWriter("//root/QcPrint/MachineWiseTest.prn");
                fw = new FileWriter("//software/QcPrint/MachineWiseTest.prn");
            }
            //printData(sData);
            //fw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void printAvg(String AvgStr){
        try{
            fw.write(AvgStr);
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
    public void Closefile() throws IOException {
        fw.write("Report Taken on :" + common.parseDate(common.getSerDateTime()) + "\n");
        fw.close();
    }

    public java.util.List getShadeLines(String Shade, int iLength) {
        java.util.List theShadeList = new java.util.ArrayList();
        int iLen = 0;
        int iLen1 = 0;
        int iLen2 = 0;

        BigDecimal bd1, bd2;
        bd1 = new BigDecimal(Shade.length());
        bd2 = new BigDecimal(iLength);
        iLen = bd1.divide(bd2, RoundingMode.UP).intValue();

        iLen1 = 0;
        iLen2 = iLength;

        for (int i = 0; i < iLen; i++) {
            if (i == (iLen - 1)) {
                theShadeList.add(Shade.substring(iLen1));
            } else {
                theShadeList.add(Shade.substring(iLen1, iLen2));
                iLen1 = iLen2;
                iLen2 += iLength;
            }
        }
        return theShadeList;
    }

    public java.util.List getCanLines(String[] str, int iLength) {
        java.util.List theCanList = new java.util.ArrayList();
        StringBuffer sb = new StringBuffer();
        String SData = "";

        int iLen = 0;
        iLen = iLength;

        for (int i = 0; i < str.length; i++) {

            SData = common.parseNull((String) str[i]);

            if (i == (str.length - 1)) {

                if (SData.length() <= iLen) {
                    sb.append(SData);
                    iLen = iLen - SData.length();
                    theCanList.add(sb);
                } else if (SData.length() > iLen) {
                    theCanList.add(sb);
                    sb = new StringBuffer();
                    iLen = iLength;
                    sb.append(SData);
                    iLen = iLen - SData.length();
                    theCanList.add(sb);
                }
            } else {
                if (SData.length() <= iLen) {
                    sb.append(SData).append(",");
                    iLen = iLen - (SData.length() + 1);
                } else if (SData.length() > iLen) {

                    theCanList.add(sb);
                    sb = new StringBuffer();
                    iLen = iLength;
                    sb.append(SData).append(",");
                    iLen = iLen - (SData.length() + 1);
                }
            }

        }
        return theCanList;
    }

    public int getMaxSize(int isize1, int isize2, int isize3, int isize4, int isize5) {

        int largest = java.util.Collections.max(java.util.Arrays.asList(isize1, isize2, isize3, isize4, isize5));

        return largest;
    }
}
//32.465