
package com.reports.print;
import java.io.FileWriter;
import com.common.Common;
import java.io.IOException;
/**
 *
 * @author admin
 */
public class MagSpinningWrapingTestAbstractPrint {
    private String SprnHead="";
    FileWriter fw;
    int Lctr=0,Pctr=1;
    int iLineSize=0;
    Common common = new Common();
    public MagSpinningWrapingTestAbstractPrint() {
    }
    public void printData(String printbody,String Sdate){
        try
            {
                    fw.write(printbody);
                    Lctr += 1;
                    iLineSize = (SprnHead.length())/2;
                    fw.write(common.Replicate("-",iLineSize)+"\n");
                    if(Lctr ==28) //36
                    {
                        fw.write("Report Taken on :"+common.parseDate(common.getSerDateTime())+"\n");
                        Pctr +=1;                        
                        setHead(Sdate);
                        Lctr =0;
                    }
            }
            catch(Exception e)
            {
                    e.printStackTrace();
            }
    }
    public void setHead(String SDate){

        String Str1    = "Company"+common.Space(1)+": AMARJOTHI SPINNING MILLS LIMITED\n";
        String Str2    = "Document"+common.Space(2)+": MAG - SPINNING WRAPPING ABSTRACT REPORT As On ";
        SprnHead = "";
       try
       {
                        fw.write(Str1);
                        fw.write(Str2+common.parseDate(SDate)+" \n");
                        //fw.write("Page"+common.Space(6)+": "+Pctr+"g\n");
                        fw.write("Page"+common.Space(6)+": "+Pctr+"FgE\n");
                        SprnHead = SprnHead+common.Pad("Test", 10)+"|";
                        SprnHead = SprnHead+common.Cad("Entry", 10)+"|";
                        SprnHead = SprnHead+common.Cad("Shi", 5)+"|";
                        SprnHead = SprnHead+common.Cad("Nom", 7)+"|";
                        SprnHead = SprnHead+common.Cad("OrderNo", 12)+"|";
                        SprnHead = SprnHead+common.Cad("Machi", 7)+"|";
                        SprnHead = SprnHead+common.Cad("TPI", 8)+"|";
                        SprnHead = SprnHead+common.Cad("Draft", 6)+"|";
                        SprnHead = SprnHead+common.Cad("Count", 8)+"|";

                        SprnHead = SprnHead+common.Pad("Strength", 8)+"|";
                        SprnHead = SprnHead+common.Cad("Csp", 6)+"|";                        
                        SprnHead = SprnHead+common.Cad("Count", 6)+"|";
                        SprnHead = SprnHead+common.Cad("Count", 6)+"|";
                        SprnHead = SprnHead+common.Cad("Rkm ", 6)+"|";
                        SprnHead = SprnHead+common.Cad("Operator", 10)+"\n";
                        
                        
                        SprnHead = SprnHead+common.Pad("No", 10)+"|";
                        SprnHead = SprnHead+common.Cad("Date", 10)+"|";
                        SprnHead = SprnHead+common.Cad("ft ", 5)+"|";
                        SprnHead = SprnHead+common.Cad("Count", 7)+"|";
                        SprnHead = SprnHead+common.Cad(" ", 12)+"|";
                        SprnHead = SprnHead+common.Cad("ne", 7)+"|";
                        SprnHead = SprnHead+common.Cad(" ", 8)+"|";
                        SprnHead = SprnHead+common.Cad(" ", 6)+"|";
                        SprnHead = SprnHead+common.Cad(" ", 8)+"|";

                        SprnHead = SprnHead+common.Pad(" ", 8)+"|";
                        SprnHead = SprnHead+common.Cad(" ", 6)+"|";
                        SprnHead = SprnHead+common.Cad(" ", 6)+"|";
                        SprnHead = SprnHead+common.Cad("Range", 6)+"|";                        
                        SprnHead = SprnHead+common.Cad(" Value ", 6)+"|";
                        SprnHead = SprnHead+common.Cad(" ", 10)+"";

                        iLineSize = (SprnHead.length())/2;
                        fw.write(common.Replicate("-",iLineSize)+"\n");
                        fw.write(SprnHead+"\n");
                        fw.write(common.Replicate("-",iLineSize)+"\n");
                        Lctr = 0;
        }
        catch(Exception Ex)
        {
                Ex.printStackTrace();
        }
    }        
    public void createPrn() {
        try {
            String Sos = System.getProperty("os.name").toLowerCase();
            if(Sos.trim().startsWith("wind")){
                fw = new FileWriter("d:/MagSpinningTestAbstract.prn");
            }else{
                fw = new FileWriter("//software/QcPrint/MagSpinningTestAbstract.prn");
            }
            //printData(sData);
            //fw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void Closefile() throws IOException
    {
        fw.write("Report Taken on :"+common.parseDate(common.getSerDateTime())+"\n");
        fw.close();
    }
}