
package com.reports.print;
import java.io.FileWriter;
import com.common.Common;
import java.io.IOException;
/**
 *
 * @author admin
 */
public class MagSpinningWrapingTestPrint {
    private String SprnHead="";
    FileWriter fw;
    int Lctr=0,Pctr=1;
    int iLineSize=0;
    Common common = new Common();
    public MagSpinningWrapingTestPrint() {
    }
    public void printData(String printbody,String Sdate,java.util.HashMap theBaseMap){
        try
            {
                    fw.write(printbody);
                    Lctr += 1;
                    iLineSize = (SprnHead.length())/2;
                    fw.write(common.Replicate("-",iLineSize)+"\n");
                    if(Lctr ==28) //36
                    {
                        fw.write("Report Taken on :"+common.parseDate(common.getSerDateTime())+"\n");
                        Pctr +=1;
                        setTestBase(Sdate,Sdate,theBaseMap);
                        setHead(Sdate);
                        Lctr =0;
                    }
            }
            catch(Exception e)
            {
                    e.printStackTrace();
            }
    }
    public void setHead(String SDate){

       SprnHead = "";
       try
       {
                        SprnHead = SprnHead+common.Pad(" Test No", 10)+"|";
                        SprnHead = SprnHead+common.Rad(" weight", 18)+"|";
                        SprnHead = SprnHead+common.Rad(" Count", 18)+"|";
                        SprnHead = SprnHead+common.Rad(" RH Count", 18)+"|";
                        SprnHead = SprnHead+common.Rad(" Strength", 18)+"|";
                        SprnHead = SprnHead+common.Rad(" Csp", 18)+"|";
                        SprnHead = SprnHead+common.Rad(" RH Csp", 18)+"\n";

                        SprnHead = SprnHead+common.Pad(" ", 10)+"|";
                        SprnHead = SprnHead+common.Rad(" (g)", 18)+"|";
                        SprnHead = SprnHead+common.Rad(" (Noc)", 18)+"|";
                        SprnHead = SprnHead+common.Rad(" (Noc)", 18)+"|";
                        SprnHead = SprnHead+common.Rad(" (lbf)", 18)+"|";
                        SprnHead = SprnHead+common.Rad(" ", 18)+"|";
                        SprnHead = SprnHead+common.Rad(" ", 18)+"";

                        iLineSize = (SprnHead.length())/2;
                        fw.write(common.Replicate("-",iLineSize)+"\n");
                        fw.write(SprnHead+"\n");
                        fw.write(common.Replicate("-",iLineSize)+"\n");
                        Lctr = 0;
        }
        catch(Exception Ex)
        {
                Ex.printStackTrace();
        }
    }
    public void setTestBase (String SDate,String STdate,java.util.HashMap theBaseMap)
    {
        String Str1    = "Company   : AMARJOTHI SPINNING MILLS LIMITED\n";
        String Str2    = "Document   "+common.Space(1)+": MAG - SPINNING WRAPPING TEST REPORT As On ";
        SprnHead = "";
        try
        {
            fw.write(Str1);
            fw.write(Str2+common.parseDate(STdate)+" \n");
            fw.write("Page"+common.Space(8)+": "+Pctr+"g\n");
            SprnHead = SprnHead+(common.Replicate("-",125)+"\n");

            SprnHead = SprnHead+common.Pad(" Dry"+common.Space(5)+": "+common.parseNull((String)theBaseMap.get("DBT")), 30)+"|";
            SprnHead = SprnHead+common.Pad(" Wet"+common.Space(8)+": "+common.parseNull((String)theBaseMap.get("WBT")), 30)+"|";
            SprnHead = SprnHead+common.Pad(" RH"+common.Space(9)+": "+common.parseNull((String)theBaseMap.get("RH")),30)+"|";
            SprnHead = SprnHead+common.Pad(" Test Date"+common.Space(4)+": "+common.parseDate(common.parseNull((String)theBaseMap.get("ENTRYDATE"))),30)+"\n";

            SprnHead = SprnHead+common.Pad(" Test ID : "+common.parseNull((String)theBaseMap.get("TESTID")), 30)+"|";
            SprnHead = SprnHead+common.Pad(" No.Of.Test : "+common.parseNull((String)theBaseMap.get("TEST")), 30)+"|";
            SprnHead = SprnHead+common.Pad(" Count Type : "+common.parseNull((String)theBaseMap.get("COUNTTYPE")), 30)+"|";
            SprnHead = SprnHead+common.Pad(" Nom.Strength : "+common.parseNull((String)theBaseMap.get("NOMSTRENGTH"))+"", 30)+"\n";

            SprnHead = SprnHead+common.Pad(" Dept"+common.Space(4)+": "+common.parseNull((String)theBaseMap.get("DEPARTMENT")), 30)+"|";
            SprnHead = SprnHead+common.Pad(" Lot No"+common.Space(5)+": "+common.parseNull((String)theBaseMap.get("MACHINE_NAME")), 30)+"|";
            SprnHead = SprnHead+common.Pad(" Nom.Count"+common.Space(2)+": "+common.parseNull((String)theBaseMap.get("NOMCOUNT")), 30)+"|";
            SprnHead = SprnHead+common.Pad(" Sam.Length"+common.Space(3)+": "+common.parseNull((String)theBaseMap.get("SAMPLE_LENGTH")), 30)+"\n";

            SprnHead = SprnHead+common.Pad(" Shift"+common.Space(3)+": "+common.parseNull((String)theBaseMap.get("SHIFT")), 30)+"|";
            SprnHead = SprnHead+common.Pad(" Operator"+common.Space(3)+": "+common.parseNull((String)theBaseMap.get("OPERATOR")), 30)+"|";
            SprnHead = SprnHead+common.Pad(" Machine"+common.Space(4)+": "+common.parseNull((String)theBaseMap.get("FRAME")), 30)+"|";
            SprnHead = SprnHead+common.Pad(" Time"+common.Space(9)+": "+common.parseNull((String)theBaseMap.get("TIME")),30 )+"\n";

            SprnHead = SprnHead+(common.Replicate("-",125)+"\n");
            fw.write(SprnHead+"\n");
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public void printMean(String Sprints)
    {
        String SprnHead = "",Sprint="";
        Sprint = Sprints;
        try{
            fw.write(Sprint+"\n");
            //fw.write(common.Replicate("-",125)+"\n\n");            
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public void createPrn() {
        try {
            String Sos = System.getProperty("os.name").toLowerCase();
            if(Sos.trim().startsWith("wind")){
                fw = new FileWriter("d:/MagSpinningTest.prn");
            }else{
                fw = new FileWriter("//software/QcPrint/MagSpinningTest.prn");
            }
            //printData(sData);
            //fw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void Closefile() throws IOException
    {
        fw.write("Report Taken on :"+common.parseDate(common.getSerDateTime())+"\n");
        fw.close();
    }
}