/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.print;
import java.io.FileWriter;
import com.common.Common;
import java.io.IOException;


public class RkmDateWisePrint {
    private String SprnHead="";
    FileWriter fw;
    int Lctr=0,Pctr=1;
    int iLineSize=0;
    Common common = new Common();

    public RkmDateWisePrint() {
    }
    public void printData(String printbody,String Sdate,String sToDate,String SUnitName,String SProcess,String Shift){
        try
            {
                    fw.write(printbody);				
                    Lctr += 1;
                    iLineSize = (SprnHead.length())/2;
                    fw.write(common.Replicate("-",iLineSize+iLineSize)+"\n");
                    if(Lctr ==28) //36
                    {
                        fw.write("Report Taken on :"+common.parseDate(common.getSerDateTime())+"\n");
                        Pctr +=1;
                        setHead(Sdate,sToDate,SUnitName,SProcess,Shift);
                        Lctr =0;
                    }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
    }
    public void setHead(String SDate,String sToDate,String SUnitName,String SProcess,String Shift){

        String Str1    = "Company   : AMARJOTHI SPINNING MILLS LIMITED\n";
        //String Str2    = "Document   : RKM REPORT AS ON  ";
        String Str2    = "Document   : RKM REPORT FROM ";
        String Str3    = "Unit       : "+SUnitName+"\n";
        String Str4    = "Process    : "+SProcess+"\n";
        String Str5    = "Shift      : "+Shift+"\n";
        SprnHead = "";
        try
       {
            fw.write(Str1);
            fw.write(Str2+common.parseDate(SDate)+" TO "+common.parseDate(sToDate)+"\n");
            fw.write(Str3);
            fw.write(Str4);
            fw.write(Str5);
            //fw.write("Page"+common.Space(7)+": "+Pctr+"FPE\n");

            fw.write("Page"+common.Space(7)+": "+Pctr+"g\n");
            SprnHead = SprnHead+common.Pad("S.No ",6)+"|";
            SprnHead = SprnHead+common.Pad("Order No ",10)+"|";
            SprnHead = SprnHead+common.Cad("Count", 8)+"|";
            SprnHead = SprnHead+common.Cad("Shade", 22)+"|";
            SprnHead = SprnHead+common.Pad("Test No", 10)+"|";
            SprnHead = SprnHead+common.Cad("Entry Date ", 10)+"|";
            SprnHead = SprnHead+common.Cad("Rkm", 10)+"|";
            SprnHead = SprnHead+common.Cad("Rkm CV", 10)+"|";
            SprnHead = SprnHead+common.Cad("Elong", 10)+"|";
            SprnHead = SprnHead+common.Cad("Elong CV ", 10)+"|";
            SprnHead = SprnHead+common.Cad("Sys", 10)+"|";
            SprnHead = SprnHead+common.Cad("SysCv", 10)+"|";

            SprnHead = SprnHead+common.Cad("Remarks", 10)+"|";
            if(SUnitName.equals("All")){
                SprnHead = SprnHead+common.Cad("Unit", 10)+"|";
            }
            iLineSize = (SprnHead.length())/2;
            fw.write(common.Replicate("-",iLineSize+iLineSize)+"\n");
            fw.write(SprnHead+"\n");
            fw.write(common.Replicate("-",iLineSize+iLineSize)+"\n");
            Lctr = 0;

        }
        catch(Exception Ex)
        {
                Ex.printStackTrace();
        }
    }
    public void createPrn() {
        try {
            String Sos = System.getProperty("os.name").toLowerCase();
            if(Sos.trim().startsWith("wind")){
                fw = new FileWriter("d:/RkmDateWise.prn");
            }else{
                fw = new FileWriter("//software/QcPrint/RkmDateWise.prn");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void Closefile() throws IOException
    {
            fw.write("Report Taken on :"+common.parseDate(common.getSerDateTime())+"\n");
            fw.close();
    }
}