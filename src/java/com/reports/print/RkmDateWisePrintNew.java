/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.print;
import java.io.FileWriter;
import com.common.Common;
import java.io.IOException;


public class RkmDateWisePrintNew {
    private String SprnHead="";
    FileWriter fw;
    int Lctr=0,Pctr=1;
    int iLineSize=0;
    Common common = new Common();

    public RkmDateWisePrintNew() {
    }
    public void printData(String printbody,String Sdate,String sToDate,String SUnitName,String SProcess,String Shift){
        try
            {
                fw.write(printbody);
                Lctr += 1;
                iLineSize = (SprnHead.length())/4;
                fw.write(common.Replicate("-",iLineSize+iLineSize)+"\n");
                if(Lctr ==28) //36
                {
                    fw.write("Report Taken on :"+common.parseDate(common.getSerDateTime())+"\n");
                    Pctr +=1;
                    setHead(Sdate,sToDate,SUnitName,SProcess,Shift);
                    Lctr =0;
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
    }
    public void setHead(String SDate,String sToDate,String SUnitName,String SProcess,String Shift){

        String Str1    = "Company   : AMARJOTHI SPINNING MILLS LIMITED\n";
        //String Str2    = "Document   : RKM REPORT AS ON  ";
        String Str2    = "Document   : RKM REPORT FROM ";
        String Str3    = "Unit       : "+SUnitName+"\n";
        String Str4    = "Process    : "+SProcess+"\n";
        String Str5    = "Shift      : "+Shift+"\n";
        SprnHead = "";
        try
       {
            fw.write(Str1);
            fw.write(Str2+common.parseDate(SDate)+" TO "+common.parseDate(sToDate)+"\n");
            fw.write(Str3);
            fw.write(Str4);
            fw.write(Str5);
            //fw.write("Page"+common.Space(7)+": "+Pctr+"FPE\n");

            fw.write("Page"+common.Space(7)+": "+Pctr+"g\n");

            SprnHead = SprnHead+common.Cad("S.No",6)+"|";
            SprnHead = SprnHead+common.Cad("Party", 27)+"|";
            SprnHead = SprnHead+common.Cad("Order",10)+"|";
            SprnHead = SprnHead+common.Cad("Cnt", 4)+"|";
            SprnHead = SprnHead+common.Cad("Shade", 22)+"|";
            SprnHead = SprnHead+common.Cad("Test", 7)+"|";
            SprnHead = SprnHead+common.Cad("Entry", 10)+"|";
            SprnHead = SprnHead+common.Cad("Rkm", 7)+"|";
            SprnHead = SprnHead+common.Cad("Rkm", 7)+"|";
            SprnHead = SprnHead+common.Cad("Rkm", 7)+"|";
            SprnHead = SprnHead+common.Cad("Elong",7)+"|";
            SprnHead = SprnHead+common.Cad("Elong", 7)+"|";
            SprnHead = SprnHead+common.Cad("Sys", 7)+"|";
            SprnHead = SprnHead+common.Cad("Sys", 7)+"|";
            SprnHead = SprnHead+common.Cad("Re", 10)+"|\n";

            SprnHead = SprnHead+common.Cad("",6)+"|";
            SprnHead = SprnHead+common.Cad("Name", 27)+"|";
            SprnHead = SprnHead+common.Cad("No",10)+"|";
            SprnHead = SprnHead+common.Cad("", 4)+"|";
            SprnHead = SprnHead+common.Cad("", 22)+"|";
            SprnHead = SprnHead+common.Cad("No", 7)+"|";
            SprnHead = SprnHead+common.Cad("Date", 10)+"|";
            SprnHead = SprnHead+common.Cad("", 7)+"|";
            SprnHead = SprnHead+common.Cad("CV",7)+"|";
            SprnHead = SprnHead+common.Cad("(min)",7)+"|";
            SprnHead = SprnHead+common.Cad("", 7)+"|";
            SprnHead = SprnHead+common.Cad("CV ",7)+"|";
            SprnHead = SprnHead+common.Cad("",7)+"|";
            SprnHead = SprnHead+common.Cad("Cv", 7)+"|";
            SprnHead = SprnHead+common.Cad("marks",10)+"|";

            if(SUnitName.equals("All")){
                SprnHead = SprnHead+common.Cad("Unit", 10)+"|\n";
                SprnHead = SprnHead+common.Cad("", 10)+"|";
            }
            
            iLineSize = (SprnHead.length())/4;
            fw.write(common.Replicate("-",iLineSize+iLineSize)+"\n");
            fw.write(SprnHead+"\n");
            fw.write(common.Replicate("-",iLineSize+iLineSize)+"\n");
            Lctr = 0;

        }
        catch(Exception Ex)
        {
                Ex.printStackTrace();
        }
    }
    public void createPrn() {
        try {
            String Sos = System.getProperty("os.name").toLowerCase();
            if(Sos.trim().startsWith("wind")){
                fw = new FileWriter("d:/RkmDateWise.prn");
            }else{
                fw = new FileWriter("//software/QcPrint/RkmDateWiseNew.prn");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void Closefile() throws IOException
    {
            fw.write("Report Taken on :"+common.parseDate(common.getSerDateTime())+"\n");
            fw.close();
    }
}