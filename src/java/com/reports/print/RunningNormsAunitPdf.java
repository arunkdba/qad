
package com.reports.print;

import com.common.Common;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Element;
import java.io.FileOutputStream;

public class RunningNormsAunitPdf {

        private Document    document;
        private PdfPTable   table;
        private int         iTotalColumns = 44;
        private int         iWidth[] = {10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10};

        private static Font bigbold=FontFactory.getFont("TIMES_ROMAN",14,Font.BOLD);
        private static Font mediumbold=FontFactory.getFont("TIMES_ROMAN",8,Font.BOLD);
        private static Font smallnormal=FontFactory.getFont("TIMES_ROMAN",8,Font.NORMAL);
        private static Font smallbold=FontFactory.getFont("TIMES_ROMAN",12,Font.BOLD);

        Common common = new Common();

    public RunningNormsAunitPdf() {
        
    }

  private void createPDFFile()
  {
        try{
            String OSName = System.getProperty("os.name");
            String PDFFile = "";
            if(OSName.startsWith("Lin")){
                PDFFile="/root/CashPaymentVoucher.pdf";
            }
            else{
                PDFFile = "D:\\CashPaymentVoucher.pdf";
            }
            document  = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(PDFFile));
            //document . setPageSize(PageSize.LEGAL.rotate());
            document . setPageSize(PageSize.A4);
            document.open();
            table = new PdfPTable(iTotalColumns);
            table.setWidths(iWidth);
            table.setWidthPercentage(100);
            //table.setHeaderRows(3);
            pdfHead();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
  
  private void pdfHead(){
       try{
           AddCellIntoTable("Amarjothi Spinning Mills Limited",table,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,6,20f,4,1,8,2,bigbold);
       }
       catch(Exception ex){
           ex.printStackTrace();
       }
    }
  
  public void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
    {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
    }
    public void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,float fHeight,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont,int iRowSpan)
    {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setRowspan(iRowSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
    }
    public void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont,int iRowSpan)
    {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));          
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setRowspan(iRowSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
    }
    public void AddCellIntoTable(String Str,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
    {
          PdfPCell c1 = new PdfPCell(new Phrase(Str,DocFont));
          //c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);
    }
    public void AddCellIntoTable(Paragraph p1,PdfPTable table,int iHorizontal,int iVertical,int iColSpan,int iLeftBorder,int iTopBorder,int iRightBorder,int iBottomBorder,Font DocFont)
    {          
		  PdfPCell c1 = new PdfPCell(p1);
          //c1.setFixedHeight(fHeight);
          c1.setHorizontalAlignment(iHorizontal);
          c1.setVerticalAlignment(iVertical);
          c1.setColspan(iColSpan);
          c1.setBorder( iLeftBorder | iTopBorder | iRightBorder | iBottomBorder);
          table.addCell(c1);		   
    }
}
