/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.print;

import java.io.FileWriter;
import com.common.Common;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.InetAddress;
/**
 *
 * @author admin
 */
public class SpinningWrapingTestPrint {
    private String SprnHead="";
    FileWriter fw;
    int Lctr=0,Pctr=1;
    int iLineSize=0;
    Common common = new Common();
    public SpinningWrapingTestPrint() {
    }
    public void printData(String printbody,String Sdate,String STdate,java.util.List TBase){
    try
            {
                    fw.write(printbody);
                    Lctr += 3;
                    iLineSize = (SprnHead.length())/2;
                    fw.write(common.Replicate("-",iLineSize+ iLineSize)+"\n");
                    if(Lctr ==30) //36
                    {
                        fw.write("Report Taken on :"+common.parseDate(common.getSerDateTime())+"\n");
                        Pctr +=1;
                        setTestBase(Sdate,STdate,TBase);
                        setHead(Sdate,STdate);
                        Lctr =0;
                    }
            }
            catch(Exception e)
            {
                    e.printStackTrace();
            }
    }
    public void printStatistical(String Sprints,String sCalculatedCount,String sCalculatedStr,double dNormalCount,double dNormalStr)
    {
        String sDiffCnt ="",sDiffStr="";
        String Str1 ="", Str2="",Str3="";
        String SprnHead = "",Sprint="";
        double dcalcCnt = common.toDouble(sCalculatedCount);
        double dcalcStr = common.toDouble(sCalculatedCount);
        double dnormalCnt = common.toDouble(common.getRound(dNormalCount,4));
        double dnormalStr = common.toDouble(common.getRound(dNormalStr,4));
        if(dcalcCnt>dnormalCnt){
                sDiffCnt = "Critically different";
        }
        else
        {
                sDiffCnt = "No critical difference";
        }
        if(dcalcStr>dnormalStr){
                sDiffStr = "Critically different";
        }
        else
        {
                sDiffStr = "No critical difference";
        }
        Str1 = "Count"+common.Space(6)+": "+sDiffCnt+" (Calculated : "+sCalculatedCount+"  Normal : "+common.getRound(dNormalCount,4)+")";
        Str2 = "Strength"+common.Space(3)+": "+sDiffStr+" (Calculated : "+sCalculatedStr+" Normal : "+common.getRound(dNormalStr,4)+" )";
        Str3 = "CSP"+common.Space(8)+": In units of Nec.lbs ";
        
        Sprint = Sprints;
        try{

            SprnHead = SprnHead+common.Cad("Statistical Report", 200)+"|\n";
            SprnHead = SprnHead+(common.Replicate("-",200)+"\n");
            SprnHead = SprnHead+common.Pad("| ", 15)+"|";
            SprnHead = SprnHead+common.Cad("Nom",20)+"|";
            SprnHead = SprnHead+common.Cad("Avg",23)+"|";
            SprnHead = SprnHead+common.Cad("Min",23)+"|";
            SprnHead = SprnHead+common.Cad("Max",23)+"|";
            SprnHead = SprnHead+common.Cad("Range",23)+"|";
            SprnHead = SprnHead+common.Cad("CV%",23)+"|";
            SprnHead = SprnHead+common.Cad("RHC",23)+"|";
            SprnHead = SprnHead+common.Cad("Q95",19)+"|";
            fw.write("\n");
            fw.write(common.Replicate("-",200)+"\n");
            fw.write(SprnHead+"\n");
            fw.write(common.Replicate("-",200)+"\n");

            fw.write(Sprint+"\n");
            fw.write(common.Replicate("-",200)+"\n\n");

            fw.write(Str1+"\n");
            fw.write(Str2+"\n");
            fw.write(Str3+"\n\n");
            //fw.write("Report Taken on :"+common.parseDate(common.getServerDate())+"\n");
            fw.write("Report Taken on :"+common.parseDate(common.getSerDateTime())+"\n");
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public void setHead(String SDate,String STdate){
	
        String Str1    = "Company   : AMARJOTHI SPINNING MILLS LIMITED\n";
        String Str2    = "Document   : SPINNING / OE WRAPPING TEST REPORT FROM";
        SprnHead = "";
        try
        {
                        SprnHead = SprnHead+common.Pad("| S.No", 5)+"|";
                        SprnHead = SprnHead+common.Pad("Machine/Change Adv", 24)+"|";
                        SprnHead = SprnHead+common.Pad("For", 4)+"|";
                        SprnHead = SprnHead+common.Cad("1", 7)+"|";
                        SprnHead = SprnHead+common.Cad("2", 7)+"|";
                        SprnHead = SprnHead+common.Cad("3", 7)+"|";
                        SprnHead = SprnHead+common.Cad("4", 7)+"|";
                        SprnHead = SprnHead+common.Cad("5", 7)+"|";
                        SprnHead = SprnHead+common.Cad("Avg Value ", 10)+"|";
                        SprnHead = SprnHead+common.Cad("RHC Value ", 10)+"|";
                        SprnHead = SprnHead+common.Cad("Cnt.Cor.Strength ", 8)+"|";
                        SprnHead = SprnHead+common.Cad("CV%", 7)+"|";

                        SprnHead = SprnHead+common.Cad("Cp", 10)+"|";
                        SprnHead = SprnHead+common.Cad("TPI", 7)+"|";
                        SprnHead = SprnHead+common.Cad("O.No", 21)+"|";

                        SprnHead = SprnHead+common.Cad("Pump/Cheese", 21)+"|";
                        SprnHead = SprnHead+common.Cad("Draft", 10)+"|";
                        SprnHead = SprnHead+common.Cad("B.D", 10)+"|\n";

                        iLineSize = (SprnHead.length())/2;
                        fw.write(common.Replicate("-",iLineSize+ iLineSize)+"\n");
                        fw.write(SprnHead+"\n");
                        fw.write(common.Replicate("-",iLineSize+iLineSize)+"\n");
                        Lctr = 0;
        }
        catch(Exception Ex)
        {
                Ex.printStackTrace();
        }
    }
    public void setTestBase (String SDate,String STdate,java.util.List TBase)
    {
        String Str1    = "Company   : AMARJOTHI SPINNING MILLS LIMITED\n";
        String Str2    = "Document   : SPINNING / OE WRAPPING TEST REPORT FROM ";
        SprnHead = "";
        try
        {
            fw.write(Str1);
            fw.write(Str2+common.parseDate(SDate)+" To  "+common.parseDate(STdate)+" \n");
            //fw.write("Page"+common.Space(7)+": "+Pctr+"FPE\n");
            fw.write("Page"+common.Space(7)+": "+Pctr+"g\n");
            SprnHead = SprnHead+(common.Replicate("-",198)+"\n");
            SprnHead = SprnHead+common.Pad("| Test No : "+common.parseNull((String)TBase.get(0)), 72)+"|";
            SprnHead = SprnHead+common.Pad(" Nominal Count : "+common.parseNull((String) TBase.get(1))+" Nec", 73)+"|";
            SprnHead = SprnHead+common.Pad(" WB Temp : "+common.parseNull((String) TBase.get(2))+"", 50)+"|\n";

            SprnHead = SprnHead+common.Pad("| Test Date : "+common.parseDate((String) TBase.get(3)), 72)+"|";
            SprnHead = SprnHead+common.Pad(" Nominal Strength : "+common.parseNull((String) TBase.get(4))+" lbs", 73)+"|";
            SprnHead = SprnHead+common.Pad(" DB Temp :"+common.parseNull((String) TBase.get(5))+"", 50)+"|\n";

            SprnHead = SprnHead+common.Pad("| Test Time : "+common.parseNull((String) TBase.get(6)), 72)+"|";
            SprnHead = SprnHead+common.Pad(" Sample Length : "+common.parseNull((String) TBase.get(7))+" Yards", 73)+"|";
            SprnHead = SprnHead+common.Pad(" RH :"+common.parseNull((String) TBase.get(8))+"%", 50)+"|\n";

            SprnHead = SprnHead+common.Pad("| Shift  : "+common.parseNull((String) TBase.get(9)), 72)+"|";
            SprnHead = SprnHead+common.Pad(" Process : "+common.parseNull((String) TBase.get(10)), 73)+"|";
            SprnHead = SprnHead+common.Pad(" Operator :"+common.parseNull((String) TBase.get(11)),50 )+"|\n";
            SprnHead = SprnHead+(common.Replicate("-",198)+"\n");
            fw.write(SprnHead+"\n");
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public void createPrn(String Sysip) {
        try {
            String Sos = System.getProperty("os.name").toLowerCase();
            String sip = Sysip.substring(Sysip.lastIndexOf(".") + 1);
            System.out.println(sip);

            if(Sos.trim().startsWith("wind")){
                //fw = new FileWriter("d:/SpinningWrapingTest"+sip+".prn");
                fw = new FileWriter("d:/SpinningWrapingTest.prn");
            }else{
                //fw = new FileWriter("//software/QcPrint/SpinningWrapingTest"+sip+".prn");
                fw = new FileWriter("//software/QcPrint/SpinningWrapingTest.prn");
            }
            //printData(sData);
            //fw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void Closefile() throws IOException
    {
            fw.close();
    }
    public java.util.List getShadeLines(String Shade, int iLength) {
        java.util.List theShadeList = new java.util.ArrayList();
        int iLen = 0;
        int iLen1 = 0;
        int iLen2 = 0;

        BigDecimal bd1, bd2;
        bd1 = new BigDecimal(Shade.length());
        bd2 = new BigDecimal(iLength);
        iLen = bd1.divide(bd2, RoundingMode.UP).intValue();

        iLen1 = 0;
        iLen2 = iLength;

        for (int i = 0; i < iLen; i++) {
            if (i == (iLen - 1)) {
                theShadeList.add(Shade.substring(iLen1));
            } else {
                theShadeList.add(Shade.substring(iLen1, iLen2));
                iLen1 = iLen2;
                iLen2 += iLength;
            }
        }
        return theShadeList;
    }

    public java.util.List getCanLines(String[] str, int iLength) {
        java.util.List theCanList = new java.util.ArrayList();
        StringBuffer sb = new StringBuffer();
        String SData = "";

        int iLen = 0;
        iLen = iLength;

        for (int i = 0; i < str.length; i++) {

            SData = common.parseNull((String) str[i]);

            if (i == (str.length - 1)) {

                if (SData.length() <= iLen) {
                    sb.append(SData);
                    iLen = iLen - SData.length();
                    theCanList.add(sb);
                } else if (SData.length() > iLen) {
                    theCanList.add(sb);
                    sb = new StringBuffer();
                    iLen = iLength;
                    sb.append(SData);
                    iLen = iLen - SData.length();
                    theCanList.add(sb);
                }
            } else {
                if (SData.length() <= iLen) {
                    sb.append(SData).append(",");
                    iLen = iLen - (SData.length() + 1);
                } else if (SData.length() > iLen) {

                    theCanList.add(sb);
                    sb = new StringBuffer();
                    iLen = iLength;
                    sb.append(SData).append(",");
                    iLen = iLen - (SData.length() + 1);
                }
            }
        }
        return theCanList;
    }

    public int getMaxSize(int isize1, int isize2, int isize3, int isize4, int isize5) {

        int largest = java.util.Collections.max(java.util.Arrays.asList(isize1, isize2, isize3, isize4, isize5));

        return largest;
    }
}

/*Test 5829
Rhc : 2374
rhc : 2387

OE - 9 Rc
*/