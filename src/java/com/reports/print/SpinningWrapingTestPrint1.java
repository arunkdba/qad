/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.print;

import java.io.FileWriter;
import com.common.Common;
import java.io.IOException;
/**
 *
 * @author admin
 */
public class SpinningWrapingTestPrint1 {
    private String SprnHead="";
    FileWriter fw;
    int Lctr=0,Pctr=1;
    int iLineSize=0;
    Common common = new Common();
    public SpinningWrapingTestPrint1() {
    }
    public void printData(String printbody,String Sdate,String STdate,java.util.List TBase){
            try
            {
                    fw.write(printbody);				
                    Lctr += 3;
                    iLineSize = (SprnHead.length())/2;
                    fw.write(common.Replicate("-",iLineSize+ iLineSize)+"\n");
                    if(Lctr ==30) //36
                    {
                        fw.write("Report Taken on :"+common.parseDate(common.getSerDateTime())+"\n");
                        Pctr +=1;
                        setTestBase(Sdate,STdate,TBase);
                        setHead(Sdate,STdate);
                        Lctr =0;
                    }
            }
            catch(Exception e)
            {
                    e.printStackTrace();
            }
    }
    public void printStatistical(String Sprints)
    {
        String Str1 = "Count : No critical difference (Calculated :  Normal : )";
        String Str2 = "Strength : Critically different (Calculated : Normal : )";
        String Str3 = "CSP : In units of Nec.lbs ";
        String SprnHead = "",Sprint="";
        Sprint = Sprints;
        try{
            
            SprnHead = SprnHead+common.Cad("Statistical Report", 220)+"|\n";
            SprnHead = SprnHead+(common.Replicate("-",220)+"\n");
            SprnHead = SprnHead+common.Pad("| ", 15)+"|";
            SprnHead = SprnHead+common.Cad("Nom",22)+"|";
            SprnHead = SprnHead+common.Cad("Avg",25)+"|";
            SprnHead = SprnHead+common.Cad("Min",25)+"|";
            SprnHead = SprnHead+common.Cad("Max",25)+"|";
            SprnHead = SprnHead+common.Cad("Range",25)+"|";
            SprnHead = SprnHead+common.Cad("CV%",25)+"|";
            SprnHead = SprnHead+common.Cad("RHC",25)+"|";
            SprnHead = SprnHead+common.Cad("Q95",25)+"|";
            fw.write("\n");
            fw.write(common.Replicate("-",220)+"\n");
            fw.write(SprnHead+"\n");
            fw.write(common.Replicate("-",220)+"\n");
            
            fw.write(Sprint+"\n");
            fw.write(common.Replicate("-",220)+"\n\n");
            
            fw.write(Str1+"\n");
            fw.write(Str2+"\n");
            fw.write(Str3+"\n\n");
            //fw.write("Report Taken on :"+common.parseDate(common.getServerDate())+"\n");
            fw.write("Report Taken on :"+common.parseDate(common.getSerDateTime())+"\n");
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public void setHead(String SDate,String STdate){
	
        String Str1    = "Company   : AMARJOTHI SPINNING MILLS LIMITED\n";
        String Str2    = "Document   : SPINNING WRAPPING TEST REPORT FROM";
        SprnHead = "";
        try
        {
                        //fw.write(Str1);
                        //fw.write(Str2+common.parseDate(SDate)+" To  "+common.parseDate(STdate)+" \n");
                        //fw.write("Page"+common.Space(7)+": "+Pctr+"g\n");
                        //fw.write("Page"+common.Space(7)+": "+Pctr+"FPE\n");
                        SprnHead = SprnHead+common.Pad("| S.No", 6)+"|";
                        SprnHead = SprnHead+common.Pad("Machine/Change Adv", 24)+"|";
                        SprnHead = SprnHead+common.Pad("For", 6)+"|";
                        SprnHead = SprnHead+common.Cad("1", 10)+"|";
                        SprnHead = SprnHead+common.Cad("2", 10)+"|";
                        SprnHead = SprnHead+common.Cad("3", 10)+"|";
                        SprnHead = SprnHead+common.Cad("4", 10)+"|";
                        SprnHead = SprnHead+common.Cad("Avg Value ", 10)+"|";
                        SprnHead = SprnHead+common.Cad("RHC Value ", 10)+"|";
                        SprnHead = SprnHead+common.Cad("Cnt.Corr Strength ", 10)+"|";
                        SprnHead = SprnHead+common.Cad("CV%", 10)+"|";
                        //SprnHead = SprnHead+common.Cad("Change Advice", 10)+"|";
                        SprnHead = SprnHead+common.Cad("Cp", 10)+"|";
                        SprnHead = SprnHead+common.Cad("TPI", 10)+"|";
                        SprnHead = SprnHead+common.Cad("O.No", 10)+"|";
                        SprnHead = SprnHead+common.Cad("Shade", 14)+"|";
                        SprnHead = SprnHead+common.Cad("Count", 10)+"|";
                        SprnHead = SprnHead+common.Cad("Pump", 10)+"|";
                        SprnHead = SprnHead+common.Cad("Draft", 10)+"|";
                        SprnHead = SprnHead+common.Cad("B.D", 10)+"|\n";

                        iLineSize = (SprnHead.length())/2;
                        fw.write(common.Replicate("-",iLineSize+ iLineSize)+"\n");
                        fw.write(SprnHead+"\n");
                        fw.write(common.Replicate("-",iLineSize+iLineSize)+"\n");
                        Lctr = 0;
        }
        catch(Exception Ex)
        {
                Ex.printStackTrace();
        }
    }
    public void setTestBase (String SDate,String STdate,java.util.List TBase)
    {
        String Str1    = "Company   : AMARJOTHI SPINNING MILLS LIMITED\n";
        String Str2    = "Document   : SPINNING WRAPPING TEST REPORT FROM ";
        SprnHead = "";
        try
        {
            fw.write(Str1);
            fw.write(Str2+common.parseDate(SDate)+" To  "+common.parseDate(STdate)+" \n");
            //fw.write("Page"+common.Space(7)+": "+Pctr+"FPE\n");
            fw.write("Page"+common.Space(7)+": "+Pctr+"g\n");
            SprnHead = SprnHead+(common.Replicate("-",220)+"\n");
            SprnHead = SprnHead+common.Pad("| Test No : "+common.parseNull((String)TBase.get(0)), 72)+"|";
            SprnHead = SprnHead+common.Pad(" Nominal Count : "+common.parseNull((String) TBase.get(1))+" Nec", 73)+"|";
            SprnHead = SprnHead+common.Pad(" WB Temp : "+common.parseNull((String) TBase.get(2))+"", 72)+"|\n";
            //SprnHead = SprnHead+(common.Replicate("-",220)+"\n");
            SprnHead = SprnHead+common.Pad("| Test Date : "+common.parseDate((String) TBase.get(3)), 72)+"|";
            SprnHead = SprnHead+common.Pad(" Nominal Strength : "+common.parseNull((String) TBase.get(4))+" lbs", 73)+"|";
            SprnHead = SprnHead+common.Pad(" DB Temp :"+common.parseNull((String) TBase.get(5))+"", 72)+"|\n";
            //SprnHead = SprnHead+(common.Replicate("-",220)+"\n");
            SprnHead = SprnHead+common.Pad("| Test Time : "+common.parseNull((String) TBase.get(6)), 72)+"|";
            SprnHead = SprnHead+common.Pad(" Sample Length : "+common.parseNull((String) TBase.get(7))+" Yards", 73)+"|";
            SprnHead = SprnHead+common.Pad(" RH :"+common.parseNull((String) TBase.get(8))+"%", 72)+"|\n";
            //SprnHead = SprnHead+(common.Replicate("-",220)+"\n");
            SprnHead = SprnHead+common.Pad("| Shift  : "+common.parseNull((String) TBase.get(9)), 72)+"|";
            SprnHead = SprnHead+common.Pad(" Process : "+common.parseNull((String) TBase.get(10)), 73)+"|";
            SprnHead = SprnHead+common.Pad(" Operator :"+common.parseNull((String) TBase.get(11)), 72)+"|\n";
            SprnHead = SprnHead+(common.Replicate("-",220)+"\n");
            fw.write(SprnHead+"\n");
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public void createPrn() {
        try {
            String Sos = System.getProperty("os.name").toLowerCase();
            if(Sos.trim().startsWith("wind")){
                fw = new FileWriter("d:/SpinningWrapingTest.prn");
            }else{
                fw = new FileWriter("//software/QcPrint/SpinningWrapingTest.prn");
            }
            //printData(sData);
            //fw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void Closefile() throws IOException
    {
            fw.close();
    }
}