/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.print;

import com.common.Common;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author root
 */
public class TwistTpiPrint {
    private String SprnHead = "";
    FileWriter fw;
    int Lctr = 0, Pctr = 1;
    int iLineSize = 0;
    Common common = new Common();
    public TwistTpiPrint() {
    }
    public void printData(String printbody, String Sdate, String STdate) {
        try {
            fw.write(printbody);
            Lctr += 1;
            //iLineSize = (SprnHead.length()) / 2;
            iLineSize = 131;
            fw.write(common.Replicate("-", iLineSize) + "\n");
            if (Lctr == 26) //36
            {
                fw.write("Report Taken on :" + common.parseDate(common.getSerDateTime()) + "\n");
                Pctr += 1;
                setTestBase(Sdate, STdate);
                setHead(Sdate);
                Lctr = 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void setHead(String SDate) {
        SprnHead = "";
        try {

            SprnHead = SprnHead + common.Pad("| S.No", 4) + "|";
            SprnHead = SprnHead + common.Pad("Test No", 7) + "|";
            SprnHead = SprnHead + common.Pad("Nof.Test", 8) + "|";
            SprnHead = SprnHead + common.Pad("Machine", 12) + "|";
            SprnHead = SprnHead + common.Pad("Order No", 17) + "|";
            SprnHead = SprnHead + common.Pad("Shade", 20) + "|";
            SprnHead = SprnHead + common.Pad("Count", 8) + "|";
            SprnHead = SprnHead + common.Pad("N.Twist", 7) + "|";
            SprnHead = SprnHead + common.Pad("Min Tpi", 7) + "|";
            SprnHead = SprnHead + common.Pad("Max Tpi", 7) + "|";
            SprnHead = SprnHead + common.Pad("Avg Tpi", 7) + "|";
            SprnHead = SprnHead + common.Pad("Rang Tpi", 8) + "|";
            SprnHead = SprnHead + common.Pad("Tpi CV", 6) + "|\n";

            //iLineSize = (SprnHead.length()) / 3;
            iLineSize = 131;
            fw.write(common.Replicate("-", iLineSize) + "\n");

            //fw.write(SprnHead + "\n");
            fw.write(SprnHead);
            fw.write(common.Replicate("-", iLineSize) + "\n");
            Lctr = 0;
        } catch (Exception Ex) {
            Ex.printStackTrace();
        }
    }
    public void setTestBase(String SDate, String STdate) {
        String Str1 = "Company   : AMARJOTHI SPINNING MILLS LIMITED\n";
        String Str2 = "Document   : TWIST TPI TEST REPORT FROM ";

        SprnHead = "";
        try {
            fw.write(Str1);
            fw.write(Str2+common.parseDate(SDate)+" To  "+common.parseDate(STdate)+" \n");
            //fw.write("Page" + common.Space(7) + ": " + Pctr + "FPE\n"); 
            fw.write("Page" + common.Space(7) + ": " + Pctr + "P\n");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    public void createPrn() {
        try {
            String Sos = System.getProperty("os.name").toLowerCase();
            if (Sos.trim().startsWith("wind")) {
                fw = new FileWriter("d:/TwistTpi.prn");
            } else {
                //fw = new FileWriter("//root/QcPrint/TwistTpi.prn");
                fw = new FileWriter("//software/QcPrint/TwistTpi.prn");
            }
            //printData(sData);
            //fw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void Closefile() throws IOException {
        fw.write("Report Taken on :" + common.parseDate(common.getSerDateTime()) + "\n");
        fw.close();
    }
}
