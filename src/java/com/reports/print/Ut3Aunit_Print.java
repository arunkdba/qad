/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.print;
import com.common.Common;
import com.reports.classes.Ut3Details;
import java.io.File;
import java.util.HashMap;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.PageOrientation;
import jxl.format.PaperSize;
import jxl.format.UnderlineStyle;
import jxl.format.CellFormat;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableCell;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

/**
 *
 * @author admin
 */
public class Ut3Aunit_Print {
     WritableWorkbook    workbook;
     WritableSheet       sheet,sheet2,sheet3;
     WritableCell cell   = null;
     CellFormat   cf     = null; 
     jxl.write.Label     label;
     int iRowCount       =0;
     Common Common=new Common();
    public Ut3Aunit_Print() {
        Common Common=new Common();
    }
     public void InitExcel()
     {
          try
          {
              String Sos = System.getProperty("os.name").toLowerCase();
              if (Sos.trim().startsWith("wind")) {
               workbook  = Workbook.createWorkbook(new File("d:/Ut3Report_Aunit.xls"));
              }else{
                  workbook  = Workbook.createWorkbook(new File("//software/QcPrint/Ut3Report_Aunit.xls"));
              }
                sheet     = workbook.createSheet("Sheet1",0);

                sheet     . getSettings().setPaperSize(PaperSize.A3); 
                sheet     . getSettings().setOrientation(PageOrientation.LANDSCAPE);              
                sheet     . getSettings().setTopMargin(0.5);
                sheet     . getSettings().setBottomMargin(0.5);
                sheet     . getSettings().setLeftMargin(0.5);
                sheet     . getSettings().setRightMargin(0.5);
                sheet     . getSettings().setProtected(false);
                sheet     . getSettings().setPassword("Print");
                sheet     . getSettings().setFitToPages(true);
              //  sheet	 .getSettings().setHorizontalCentre(true);
			   
                sheet2    = workbook.createSheet("Sheet2",1);
		sheet2	  .getSettings().setPaperSize(PaperSize.A3);
		sheet2    . getSettings().setOrientation(PageOrientation.LANDSCAPE); 
                sheet2    . getSettings().setTopMargin(0.5);
                sheet2    . getSettings().setBottomMargin(0.5);
                sheet2    . getSettings().setLeftMargin(0.5);
                sheet2    . getSettings().setRightMargin(0.5);
                sheet2    . getSettings().setProtected(false);
                sheet2    . getSettings().setPassword("Print");
                sheet2    . getSettings().setFitToPages(true);
                // sheet2 	  .getSettings().setHorizontalCentre(true);
			   
		sheet3    = workbook.createSheet("Sheet3",2);
		sheet3	  . getSettings().setPaperSize(PaperSize.A3);
		sheet3    . getSettings().setOrientation(PageOrientation.LANDSCAPE); 
                sheet3    . getSettings().setTopMargin(0.5);
                sheet3    . getSettings().setBottomMargin(0.5);
                sheet3    . getSettings().setLeftMargin(0.5);
                sheet3    . getSettings().setRightMargin(0.5);
                sheet3    . getSettings().setProtected(false);
                sheet3    . getSettings().setPassword("Print");
                sheet3    . getSettings().setFitToPages(true);
               //sheet3	  .getSettings().setHorizontalCentre(true);
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
     }
    public void CloseExcel()
    {
          try
          {
               workbook.write();
               workbook.close();
          }
          catch(Exception ex)
          {
               ex.printStackTrace();
          }
    }
    public void WriteStandards(java.util.List thelist,String sFdate,String sTdate)
    {
        java.util.HashMap theMap =new java.util.HashMap();
        try{
            WritableFont   wfHeadRed           	= new WritableFont(WritableFont.TIMES,10,WritableFont.BOLD,false,UnderlineStyle.NO_UNDERLINE,Colour.RED);
            WritableCellFormat AllBordersHeadRed 	= new WritableCellFormat(wfHeadRed);
            AllBordersHeadRed                  	. setBorder(Border.ALL, BorderLineStyle.THIN);
            AllBordersHeadRed                  	. setAlignment(Alignment.CENTRE);
            
            WritableCellFormat HeadRedLeft 	= new WritableCellFormat(wfHeadRed);
            HeadRedLeft                  	. setBorder(Border.ALL, BorderLineStyle.THIN);
            HeadRedLeft                  	. setAlignment(Alignment.LEFT);
            
            WritableFont   wf                  		= new WritableFont(WritableFont.TIMES,7);
            WritableFont   wfBold              		= new WritableFont(WritableFont.TIMES,8,WritableFont.BOLD);
            
            WritableCellFormat AllBorders      		= new WritableCellFormat(wfBold);
            AllBorders                         		. setBorder(Border.ALL, BorderLineStyle.THIN);	
            
            label         = new Label(1,iRowCount,"A-Unit UT3 Report From "+sFdate+" To "+sTdate+"",HeadRedLeft);
            sheet         . addCell(label);
            sheet         . mergeCells(1,iRowCount,44,iRowCount);
            iRowCount     += 1;
            
            label         = new Label(4,iRowCount,"SITRA STANDARD  ",AllBorders);
            sheet         . addCell(label);
            sheet         . mergeCells(4,iRowCount,7,iRowCount);
            iRowCount     += 1;
            
            label         = new Label(4,iRowCount,"Count",AllBorders);
            sheet         . addCell(label);
            label         = new Label(5,iRowCount,"Good",AllBorders);
            sheet         . addCell(label);
            label         = new Label(6,iRowCount,"AVG",AllBorders);
            sheet         . addCell(label);
            label         = new Label(7,iRowCount,"POOR",AllBorders);
            sheet         . addCell(label);
            iRowCount     += 1;
            for(int i=0;i<thelist.size();i++) {
                theMap    =  (HashMap)thelist.get(i);
                label     = new Label(4,iRowCount,Common.parseNull((String)theMap.get("COUNT")),AllBorders);
                sheet     . addCell(label);
                label     = new Label(5,iRowCount,Common.parseNull((String)theMap.get("GOOD")),AllBorders);
                sheet     . addCell(label);
                label     = new Label(6,iRowCount,Common.parseNull((String)theMap.get("AVERAGE")),AllBorders);
                sheet     . addCell(label);
                label     = new Label(7,iRowCount,Common.parseNull((String)theMap.get("POOR")),AllBorders);
                sheet     . addCell(label);
                iRowCount += 1;
            }
            label         = new Label(1,iRowCount,"",AllBorders);
            sheet         . addCell(label);
            sheet         . mergeCells(1,iRowCount,44,iRowCount);
            iRowCount     += 1;
            
            label         = new Label(1,iRowCount,"",AllBorders);
            sheet         . addCell(label);
            sheet         . mergeCells(1,iRowCount,44,iRowCount);
            iRowCount     += 1;
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
    public void WriteHeading(){
        
        try{
            WritableFont   wf                  		= new WritableFont(WritableFont.TIMES,7);
            WritableFont   wfBold              		= new WritableFont(WritableFont.TIMES,8,WritableFont.BOLD);
            
            WritableCellFormat AllBorders      		= new WritableCellFormat(wfBold);
            AllBorders                         		. setBorder(Border.ALL, BorderLineStyle.THIN);
            
            label          = new Label(1,iRowCount,"O.NO",AllBorders);
            sheet          . addCell(label);
            label          = new Label(2,iRowCount,"SHADE",AllBorders);
            sheet          . addCell(label);
            label          = new Label(3,iRowCount,"PARTY NAME ",AllBorders);
            sheet          . addCell(label);
            label          = new Label(4,iRowCount,"O.QTY",AllBorders);
            sheet          . addCell(label);
            label          = new Label(5,iRowCount,"DATE",AllBorders);
            sheet          . addCell(label);
            label          = new Label(6,iRowCount,"RF NO ",AllBorders);
            sheet          . addCell(label);
            label          = new Label(7,iRowCount,"U%",AllBorders);
            sheet          . addCell(label);
            label          = new Label(8,iRowCount,"CVM",AllBorders);
            sheet          . addCell(label);
            label          = new Label(9,iRowCount,"THIN",AllBorders);
            sheet          . addCell(label);
            label          = new Label(10,iRowCount,"THICK",AllBorders);
            sheet          . addCell(label);
            label          = new Label(11,iRowCount,"NEPS",AllBorders);
            sheet          . addCell(label);
            label          = new Label(12,iRowCount,"TOTAL",AllBorders);
            sheet          . addCell(label);
            label          = new Label(13,iRowCount,"NO RE",AllBorders);
            sheet          . addCell(label);
            sheet          . mergeCells(13,iRowCount,15,iRowCount);
            label          = new Label(16,iRowCount,"COUNT",AllBorders);
            sheet          . addCell(label);
            label          = new Label(17,iRowCount,"COUNT CV% ",AllBorders);
            sheet          . addCell(label);
            label          = new Label(18,iRowCount,"STRENGTH",AllBorders);
            sheet          . addCell(label);
            label          = new Label(19,iRowCount,"STRENGTH CV",AllBorders);
            sheet          . addCell(label);
            label          = new Label(20,iRowCount,"CSP",AllBorders);
            sheet          . addCell(label);
            label          = new Label(21,iRowCount,"SYS",AllBorders);
            sheet          . addCell(label);
            label          = new Label(22,iRowCount,"RKM",AllBorders);
            sheet          . addCell(label);
            label          = new Label(23,iRowCount,"RKM CV%",AllBorders);
            sheet          . addCell(label);
            label          = new Label(24,iRowCount,"ELONG",AllBorders);
            sheet          . addCell(label);
            label          = new Label(25,iRowCount,"ELONG CV%",AllBorders);
            sheet          . addCell(label);
            label          = new Label(26,iRowCount,"VSF",AllBorders);
            sheet          . addCell(label);
            label          = new Label(27,iRowCount,"DVSF",AllBorders);
            sheet          . addCell(label);
            label          = new Label(28,iRowCount,"GV",AllBorders);
            sheet          . addCell(label);
            label          = new Label(29,iRowCount,"DGV",AllBorders);
            sheet          . addCell(label);
            label          = new Label(30,iRowCount,"POC",AllBorders);
            sheet          . addCell(label);
            label          = new Label(31,iRowCount,"DPOC",AllBorders);
            sheet          . addCell(label);
            label          = new Label(32,iRowCount,"DC COM ",AllBorders);
            sheet          . addCell(label);
            label          = new Label(33,iRowCount,"KARDED",AllBorders);
            sheet          . addCell(label);
            label          = new Label(34,iRowCount,"COMBED",AllBorders);
            sheet          . addCell(label);
            label          = new Label(35,iRowCount,"DEPTH",AllBorders);
            sheet          . addCell(label);
            label          = new Label(36,iRowCount,"COTTON",AllBorders);
            sheet          . addCell(label);
            sheet          . mergeCells(36,iRowCount,37,iRowCount);
            label          = new Label(38,iRowCount,"LOT",AllBorders);
            sheet          . addCell(label);
            label          = new Label(39,iRowCount,"OTHERS",AllBorders);
            sheet          . addCell(label);
            sheet          . mergeCells(39,iRowCount,43,iRowCount);
            label          = new Label(44,iRowCount,"TOTAL",AllBorders);
            sheet          . addCell(label);
            iRowCount      += 1;
            
            label          = new Label(1,iRowCount," ",AllBorders);
            sheet          . addCell(label);
	    label          = new Label(2,iRowCount," ",AllBorders);
            sheet          . addCell(label);
	    label          = new Label(3,iRowCount," ",AllBorders);
            sheet          . addCell(label);
	    label          = new Label(4,iRowCount," ",AllBorders);
            sheet          . addCell(label);
            label          = new Label(5,iRowCount," ",AllBorders);
            sheet          . addCell(label);
	    label          = new Label(6,iRowCount," ",AllBorders);
            sheet          . addCell(label);
	    label          = new Label(7,iRowCount," ",AllBorders);
            sheet          . addCell(label);
	    label          = new Label(8,iRowCount," ",AllBorders);
            sheet          . addCell(label);
            label          = new Label(9,iRowCount," ",AllBorders);
            sheet          . addCell(label);
            label          = new Label(10,iRowCount," ",AllBorders);
            sheet          . addCell(label);
            
            label          = new Label(11,iRowCount," ",AllBorders);
            sheet          . addCell(label);
	    label          = new Label(12,iRowCount," ",AllBorders);
            sheet          . addCell(label);
	    label          = new Label(13,iRowCount,"Cnt",AllBorders);
            sheet          . addCell(label);
	    label          = new Label(14,iRowCount,"Str",AllBorders);
            sheet          . addCell(label);
            label          = new Label(15,iRowCount,"Csp",AllBorders);
            sheet          . addCell(label);
	    label          = new Label(16,iRowCount," ",AllBorders);
            sheet          . addCell(label);
	    label          = new Label(17,iRowCount," ",AllBorders);
            sheet          . addCell(label);
	    label          = new Label(18,iRowCount," ",AllBorders);
            sheet          . addCell(label);
            label          = new Label(19,iRowCount," ",AllBorders);
            sheet          . addCell(label);
            label          = new Label(20,iRowCount," ",AllBorders);
            sheet          . addCell(label);
            
            label          = new Label(21,iRowCount," ",AllBorders);
            sheet          . addCell(label);
	    label          = new Label(22,iRowCount," ",AllBorders);
            sheet          . addCell(label);
	    label          = new Label(23,iRowCount," ",AllBorders);
            sheet          . addCell(label);
	    label          = new Label(24,iRowCount," ",AllBorders);
            sheet          . addCell(label);
            label          = new Label(25,iRowCount," ",AllBorders);
            sheet          . addCell(label);
	    label          = new Label(26,iRowCount," ",AllBorders);
            sheet          . addCell(label);
	    label          = new Label(27,iRowCount," ",AllBorders);
            sheet          . addCell(label);
	    label          = new Label(28,iRowCount," ",AllBorders);
            sheet          . addCell(label);
            label          = new Label(29,iRowCount," ",AllBorders);
            sheet          . addCell(label);
            label          = new Label(30,iRowCount," ",AllBorders);
            sheet          . addCell(label);
            label          = new Label(31,iRowCount," ",AllBorders);
            sheet          . addCell(label);
            
            label          = new Label(32,iRowCount," ",AllBorders);
            sheet          . addCell(label);
	    label          = new Label(33,iRowCount," ",AllBorders);
            sheet          . addCell(label);
	    label          = new Label(34,iRowCount," ",AllBorders);
            sheet          . addCell(label);
	    label          = new Label(35,iRowCount," ",AllBorders);
            sheet          . addCell(label);
            label          = new Label(36,iRowCount,"CARD",AllBorders);
            sheet          . addCell(label);
	    label          = new Label(37,iRowCount,"CBD",AllBorders);
            sheet          . addCell(label);
	    label          = new Label(38,iRowCount," ",AllBorders);
            sheet          . addCell(label);
            label          = new Label(39,iRowCount,"B.COTTON",AllBorders);
            sheet          . addCell(label);
            label          = new Label(40,iRowCount,"USABLE",AllBorders);
            sheet          . addCell(label);
	    label          = new Label(41,iRowCount,"WHITE MODEL",AllBorders);
            sheet          . addCell(label);
            label          = new Label(42,iRowCount,"MIXED",AllBorders);
            sheet          . addCell(label);
            label          = new Label(43,iRowCount,"OTHERS",AllBorders);
            sheet          . addCell(label);
	    label          = new Label(44,iRowCount," ",AllBorders);
            sheet          . addCell(label);
            iRowCount      += 1;
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
    public void WriteReportData(java.util.List TestUt3list){
        double dCsp = 0,dCnt = 0,dStr = 0;
        double dUper = 0,dUperTot =0;
        double dvCvm = 0,dTotCvm = 0;
        double dThin = 0,dThinTot =0;
        double dThick =0,dThickTot =0;
        double dNeps = 0,dNepsTot=0;
        double dNeps280 = 0,dNeps280Tot=0;
        double dTotal =0,dTotalTot =0;
        double dTotal200 =0,dTotal200Tot =0;
        double dTotal280 =0,dTotal280Tot =0;
        double dCount = 0,dCountTot = 0;
        double dCountCv = 0,dCountCvTot = 0;
        double dStrTot = 0,dStrCv = 0;
        double dStrCvTot = 0, dCspTot = 0;
        double dSys = 0,dSysTot = 0;
        double dRkm = 0,dRkmTot = 0;
        double dRkmCv = 0,dRkmCvTot = 0;
        double dElang = 0, dElangTot = 0;
        double dElangCv = 0, dElangCvTot = 0;
        int    iUperCount = 0;
        int    iCvmCount = 0;
        int    iThinCount=0;
        int    iThickCount =0;
        int    iNepsCount = 0;
        int    iNeps280Count = 0;
        int    iTotalCount = 0;
        int    iTotal200Count = 0;
        int    iTotal280Count = 0;
        int    iCountCount = 0;
        int    iCountCvCount =0;
        int    iStrCount = 0;
        int    iStrCvCount = 0;
        int    iCspCount = 0;
        int    iSysCount = 0;
        int    iRkmCount = 0;
        int    iRkmCvCount = 0;
        int    iElangCount = 0;
        int    iElangCvCount = 0;
        String sPrevOrdNo ="";
        WritableFont   wf                  		= new WritableFont(WritableFont.TIMES,7);
        WritableFont   wfBold              		= new WritableFont(WritableFont.TIMES,8,WritableFont.BOLD);

        WritableCellFormat AllBorders      		= new WritableCellFormat(wf);
        WritableCellFormat BoldFont      		= new WritableCellFormat(wfBold);
        WritableCellFormat wrappedText                  = new WritableCellFormat(wf);
        
        java.util.HashMap theDataMap =new java.util.HashMap();
        try{
            AllBorders                         		. setBorder(Border.ALL, BorderLineStyle.THIN);
            BoldFont                    		. setBorder(Border.ALL, BorderLineStyle.THIN);
            wrappedText                                 . setBorder(Border.ALL, BorderLineStyle.THIN);
            wrappedText                                 . setWrap(true);
            
            for (int i = 0; i < TestUt3list.size(); i++) {
                Ut3Details details = (Ut3Details) TestUt3list.get(i);
                java.util.List ls = details.getOrderList();
                if(!sPrevOrdNo.equals(Common.parseNull(details.getOrderno()))){
                    label          = new Label(1,iRowCount,Common.parseNull(details.getCountname()),AllBorders);
                    sheet          . addCell(label);
                    label          = new Label(2,iRowCount,Common.parseNull(details.getBlendname()),AllBorders);
                    sheet          . addCell(label);
                    sheet          . mergeCells(2,iRowCount,44,iRowCount);
                    iRowCount      += 	1;
                }
                for(int j=0;j<ls.size();j++){
                    theDataMap     =  (HashMap)ls.get(j);
                    dCnt = Common.toDouble(Common.getRound((String)theDataMap.get("COUNT"),2));
                    dStr = Common.toDouble(Common.getRound((String)theDataMap.get("STRENGTH"),2));
                    dCsp = dCnt*dStr;

                    dUper = Common.toDouble(Common.getRound((String)theDataMap.get("UPER"),2));
                    dvCvm = Common.toDouble(Common.getRound((String)theDataMap.get("CVM"),2));
                    dThin = Common.toDouble(Common.getRound((String)theDataMap.get("THIN"),2));
                    dThick = Common.toDouble(Common.getRound((String)theDataMap.get("THICK"),2));
                    dNeps  = Common.toDouble(Common.getRound((String)theDataMap.get("NEPS"),2));
                    dNeps280 = Common.toDouble(Common.getRound((String)theDataMap.get("NEPS280"),2));

                    dTotal = Common.toDouble(Common.getRound((String)theDataMap.get("TOTAL"),2));
                    dTotal200 = Common.toDouble(Common.getRound((String)theDataMap.get("TOT200"),2));
                    dTotal280 = Common.toDouble(Common.getRound((String)theDataMap.get("TOT280"),2));
                    dCount = Common.toDouble(Common.getRound((String)theDataMap.get("COUNT"),2));
                    dCountCv = Common.toDouble(Common.getRound((String)theDataMap.get("COUNTCV"),2));
                    dStrCv  = Common.toDouble(Common.getRound((String)theDataMap.get("STRENGTHCV"),2));
                    dSys = Common.toDouble(Common.getRound((String)theDataMap.get("SYS"),2));
                    dRkm = Common.toDouble(Common.getRound((String)theDataMap.get("RKM"),2));
                    dRkmCv = Common.toDouble(Common.getRound((String)theDataMap.get("RKMCV"),2));
                    dElang = Common.toDouble(Common.getRound((String)theDataMap.get("ELANG"),2));
                    dElangCv = Common.toDouble(Common.getRound((String)theDataMap.get("ELANGCV"),2));
                    if (dUper != 0) {
                           iUperCount +=1;
                           dUperTot +=dUper;
                       }
                       if (dvCvm != 0) {
                           iCvmCount +=1;
                           dTotCvm +=dvCvm;
                       }
                       if (dThin != 0) {
                           iThinCount +=1;
                           dThinTot +=dThin;
                       }
                       if (dThick != 0) {
                           iThickCount +=1;
                           dThickTot +=dThick;
                       }
                       if (dNeps != 0) {
                           iNepsCount +=1;
                           dNepsTot +=dNeps;
                       }
                       if (dNeps280 != 0) {
                           iNeps280Count +=1;
                           dNeps280Tot +=dNeps280;
                       }
                       if (dTotal != 0) {
                           iTotalCount +=1;
                           dTotalTot +=dTotal;
                       }
                       if (dTotal200 != 0) {
                           iTotal200Count +=1;
                           dTotal200Tot +=dTotal200;
                       }
                       if (dTotal280 != 0) {
                           iTotal280Count +=1;
                           dTotal280Tot +=dTotal280;
                       }
                       if (dCount != 0) {
                           iCountCount +=1;
                           dCountTot +=dCount;
                       }
                       if (dCountCv != 0) {
                           iCountCvCount +=1;
                           dCountCvTot +=dCountCv;
                       }
                       if (dStr !=0){
                           iStrCount += 1;
                           dStrTot += dStr;
                       }
                       if (dStrCv !=0){
                           iStrCvCount += 1;
                           dStrCvTot += dStrCv;
                       }
                       if (dCsp !=0){
                           iCspCount += 1;
                           dCspTot += dCsp;
                       }
                       if (dSys !=0){
                           iSysCount += 1;
                           dSysTot += dSys;
                       }
                       if (dRkm !=0){
                           iRkmCount += 1;
                           dRkmTot += dRkm;
                       }
                       if (dRkmCv !=0){
                           iRkmCvCount += 1;
                           dRkmCvTot += dRkmCv;
                       }
                       if (dElang !=0){
                           iElangCount += 1;
                           dElangTot += dElang;
                       }
                       if (dElangCv !=0){
                           iElangCvCount += 1;
                           dElangCvTot += dElangCv;
                       }
                       if(j==0){
                            label          = new Label(1,iRowCount,Common.parseNull(details.getOrderno()),AllBorders);
                            sheet          . addCell(label);
                            label          = new Label(2,iRowCount,Common.parseNull(details.getShadename()),wrappedText);
                            sheet          . addCell(label);
                            label          = new Label(3,iRowCount,Common.parseNull(details.getPartyname()),wrappedText);
                            sheet          . addCell(label);
                            label          = new Label(4,iRowCount,Common.parseNull(details.getOrdweight()),AllBorders);
                            sheet          . addCell(label);
                            //iRowCount      += 	1;
                       }
                       else{
                            label          = new Label(1,iRowCount," ",AllBorders);
                            sheet          . addCell(label);
                            label          = new Label(2,iRowCount," ",AllBorders);
                            sheet          . addCell(label);
                            label          = new Label(3,iRowCount," ",AllBorders);
                            sheet          . addCell(label);
                            label          = new Label(4,iRowCount," ",AllBorders);
                            sheet          . addCell(label);
                            //iRowCount      += 	1;
                       }
                        label          = new Label(5,iRowCount,Common.parseDate((String)theDataMap.get("DATE")),AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(6,iRowCount,details.getNull(Common.parseNull((String)theDataMap.get("MACHINE"))),wrappedText);
                        sheet          . addCell(label);
                        label          = new Label(7,iRowCount,details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("UPER"),2))),AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(8,iRowCount,details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("CVM"),2))),AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(9,iRowCount,details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("THIN"),2))),AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(10,iRowCount,details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("THICK"),2))),AllBorders);
                        sheet          . addCell(label);

                        label          = new Label(11,iRowCount,details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("NEPS"),2))),AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(12,iRowCount,details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("TOTAL"),2))),AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(13,iRowCount,details.getNull(Common.parseNull((String)theDataMap.get("CNTREADING"))),AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(14,iRowCount,details.getNull(Common.parseNull((String)theDataMap.get("STRREADING"))),AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(15,iRowCount,details.getNull(Common.parseNull((String)theDataMap.get("CSPREADING"))),AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(16,iRowCount,details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("COUNT"),2))),AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(17,iRowCount,details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("COUNTCV"),2))),AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(18,iRowCount,details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("STRENGTH"),2))),AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(19,iRowCount,details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("STRENGTHCV"),2))),AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(20,iRowCount,details.getNull(Common.getRound(dCsp,2)),AllBorders);
                        sheet          . addCell(label);

                        label          = new Label(21,iRowCount,details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("SYS"),2))),AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(22,iRowCount,details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("RKM"),2))),AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(23,iRowCount,details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("RKMCV"),2))),AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(24,iRowCount,details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("ELANG"),2))),AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(25,iRowCount,details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("ELANGCV"),2))),AllBorders);
                        sheet          . addCell(label);
                        if(j==0){ // Mixing 
                        label          = new Label(26,iRowCount,details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("VSF"),2))),AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(27,iRowCount,details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("DVSF"),2))),AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(28,iRowCount,details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("GV"),2))),AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(29,iRowCount,details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("DGV"),2))),AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(30,iRowCount,details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("POC"),2))),AllBorders);
                        sheet          . addCell(label);
                        
                        label          = new Label(31,iRowCount,details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("DPOC"),2))),AllBorders);
                        sheet          . addCell(label);

                        label          = new Label(32,iRowCount,details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("DCCOM"),2))),AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(33,iRowCount,details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("KARD"),2))),AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(34,iRowCount,details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("COMBED"),2))),AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(35,iRowCount,details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("DEPTH"),2))),AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(36,iRowCount,details.getNull(Common.parseNull((String)theDataMap.get("KARDVAR"))),AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(37,iRowCount,details.getNull(Common.parseNull((String)theDataMap.get("COMBVAR"))),AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(38,iRowCount,details.getNull(Common.parseNull((String)theDataMap.get("LOTNO"))),AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(39,iRowCount,details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("BCOTTON"),2))),AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(40,iRowCount,details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("USABLE"),2))),AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(41,iRowCount,details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("MODAL"),2))),AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(42,iRowCount,details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("MIXED"),2))),AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(43,iRowCount,details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("OTHERS"),2))),AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(44,iRowCount,details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("MIXTOT"),0))),AllBorders);
                        sheet          . addCell(label);
                        }
                        else{
                        label          = new Label(26,iRowCount," ",AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(27,iRowCount," ",AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(28,iRowCount," ",AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(29,iRowCount," ",AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(30,iRowCount," ",AllBorders);
                        sheet          . addCell(label);

                        label          = new Label(31,iRowCount," ",AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(32,iRowCount," ",AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(33,iRowCount," ",AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(34,iRowCount," ",AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(35,iRowCount," ",AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(36,iRowCount," ",AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(37,iRowCount," ",AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(38,iRowCount," ",AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(39,iRowCount," ",AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(40,iRowCount," ",AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(41,iRowCount," ",AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(42,iRowCount," ",AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(43,iRowCount," ",AllBorders);
                        sheet          . addCell(label);
                        label          = new Label(44,iRowCount," ",AllBorders);
                        sheet          . addCell(label);
                        }
                        iRowCount      += 	1;
                        
                        sPrevOrdNo = Common.parseNull(details.getOrderno());
                }
                        label          = new Label(1,iRowCount,"AVG",BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(2,iRowCount," ",BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(3,iRowCount," ",BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(4,iRowCount," ",BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(5,iRowCount," ",BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(6,iRowCount," ",BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(7,iRowCount,details.getNull(Common.getRound((dUperTot/iUperCount),2)),BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(8,iRowCount,details.getNull(Common.getRound((dTotCvm/iCvmCount),2)),BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(9,iRowCount,details.getNull(Common.getRound((dThinTot/iThinCount),2)),BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(10,iRowCount,details.getNull(Common.getRound((dThickTot/iThickCount),2)),BoldFont);
                        sheet          . addCell(label);

                        label          = new Label(11,iRowCount,details.getNull(Common.getRound((dNepsTot/iNepsCount),2)),BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(12,iRowCount,details.getNull(Common.getRound((dTotalTot/iTotalCount),2)),BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(13,iRowCount," ",BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(14,iRowCount," ",BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(15,iRowCount," ",BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(16,iRowCount,details.getNull(Common.getRound((dCountTot/iCountCount),2)),BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(17,iRowCount,details.getNull(Common.getRound((dCountCvTot/iCountCvCount),2)),BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(18,iRowCount,details.getNull(Common.getRound((dStrTot/iStrCount),2)),BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(19,iRowCount,details.getNull(Common.getRound((dStrCvTot/iStrCvCount),2)),BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(20,iRowCount,details.getNull(Common.getRound((dCspTot/iCspCount),2)),BoldFont);
                        sheet          . addCell(label);

                        label          = new Label(21,iRowCount,details.getNull(Common.getRound((dSysTot/iSysCount),2)),BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(22,iRowCount,details.getNull(Common.getRound((dRkmTot/iRkmCount),2)),BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(23,iRowCount,details.getNull(Common.getRound((dRkmCvTot/iRkmCvCount),2)),BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(24,iRowCount,details.getNull(Common.getRound((dElangTot/iElangCount),2)),BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(25,iRowCount,details.getNull(Common.getRound((dElangCvTot/iElangCvCount),2)),BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(26,iRowCount," ",BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(27,iRowCount," ",BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(28,iRowCount," ",BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(29,iRowCount," ",BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(30,iRowCount," ",BoldFont);
                        sheet          . addCell(label);

                        label          = new Label(31,iRowCount," ",BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(32,iRowCount," ",BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(33,iRowCount," ",BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(34,iRowCount," ",BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(35,iRowCount," ",BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(36,iRowCount," ",BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(37,iRowCount," ",BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(38,iRowCount," ",BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(39,iRowCount," ",BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(40,iRowCount," ",BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(41,iRowCount," ",BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(42,iRowCount," ",BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(43,iRowCount," ",BoldFont);
                        sheet          . addCell(label);
                        label          = new Label(44,iRowCount," ",BoldFont);
                        sheet          . addCell(label);
                        iRowCount      += 1;
                    dUperTot =0;dUper = 0;iUperCount =0;dvCvm = 0;dTotCvm = 0;iCvmCount = 0;
                    dThin = 0;dThinTot =0;iThinCount=0;dThick = 0;dThickTot =0;iThickCount=0;
                    dNeps = 0;dNepsTot = 0;iNepsCount = 0;dNeps280 = 0;dNeps280Tot = 0;
                    iNeps280Count = 0;dTotal = 0;dTotalTot = 0;iTotalCount = 0;dTotal200 = 0;
                    dTotal200Tot = 0;iTotal200Count = 0;dTotal280 = 0;dTotal280Tot = 0;iTotal280Count = 0;
                    dCount = 0;dCountTot = 0;iCountCount = 0;dCountCv=0;dCountCvTot = 0;iCountCvCount = 0;
                    dStr = 0;dStrTot = 0;iStrCount = 0;dStrCv = 0;dStrCvTot = 0;iStrCvCount = 0;dCsp = 0;
                    dCspTot = 0;iCspCount = 0;dSys =0;dSysTot = 0;iSysCount = 0;dRkm = 0;dRkmTot = 0;iRkmCount = 0;
                    dRkmCv = 0;dRkmCvTot = 0;iRkmCvCount = 0;dElang = 0;dElangTot = 0;iElangCount = 0;dElangCv = 0;
                    dElangCvTot = 0;iElangCvCount = 0;
            }
        }
        catch(Exception ex){
        ex.printStackTrace();
        }
    }
}