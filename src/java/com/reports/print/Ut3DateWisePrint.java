/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.print;
import java.io.FileWriter;
import com.common.Common;
import java.io.IOException;
/**
 *
 * @author admin
 */
public class Ut3DateWisePrint {
    private String SprnHead="";
    FileWriter fw;
    int Lctr=0,Pctr=1;
    int iLineSize=0;
    Common common = new Common();
    public Ut3DateWisePrint() {        
    }
    public void printData(String printbody,String Sdate,String SUnitName,String SProcess,String Shift){
        try
            {
                    fw.write(printbody);				
                    Lctr += 1;
                    iLineSize = (SprnHead.length())/2;
                    fw.write(common.Replicate("-",iLineSize+iLineSize)+"\n");
                    if(Lctr ==28) //36
                    {
                        fw.write("Report Taken on :"+common.parseDate(common.getSerDateTime())+"\n");
                        Pctr +=1;                        
                        setHead(Sdate,SUnitName,SProcess,Shift);
                        Lctr =0;
                    }
            }
            catch(Exception e)
            {
                    e.printStackTrace();
            }     
    }
    public void setHead(String SDate,String SUnitName,String SProcess,String Shift){
	
        String Str1    = "Company   : AMARJOTHI SPINNING MILLS LIMITED\n";
        String Str2    = "Document   : UT3 REPORT AS ON ";
        String Str3    = "Unit       : "+SUnitName+"\n";
        String Str4    = "Process    : "+SProcess+"\n";
        String Str5    = "Shift      : "+Shift+"\n";
        SprnHead = "";
        try
       {                        
                        fw.write(Str1);
                        fw.write(Str2+common.parseDate(SDate)+" \n");
                        fw.write(Str3);
                        fw.write(Str4);
                        fw.write(Str5);
                        //fw.write("Page"+common.Space(7)+": "+Pctr+"FPE\n");
                        fw.write("Page"+common.Space(7)+": "+Pctr+"g\n");  
                        SprnHead = SprnHead+common.Pad("S.No ",6)+"|";
                        SprnHead = SprnHead+common.Pad("Order No ",10)+"|";
                        SprnHead = SprnHead+common.Cad("Count", 8)+"|";
                        SprnHead = SprnHead+common.Cad("Shade", 22)+"|";
                        SprnHead = SprnHead+common.Pad("Test No", 10)+"|";
                        SprnHead = SprnHead+common.Cad("Entry Date ", 10)+"|";
                        SprnHead = SprnHead+common.Cad("U%", 10)+"|";
                        SprnHead = SprnHead+common.Cad("Thin", 10)+"|";
                        SprnHead = SprnHead+common.Cad("Thick", 10)+"|";
                        SprnHead = SprnHead+common.Cad("Neps200 ", 10)+"|";
                        SprnHead = SprnHead+common.Cad("Neps280", 10)+"|";
                        SprnHead = SprnHead+common.Cad("Total200", 10)+"|";
                        SprnHead = SprnHead+common.Cad("Total280", 10)+"|";
                        
                        SprnHead = SprnHead+common.Cad("Cvm", 10)+"|";
                        
                        
                        SprnHead = SprnHead+common.Cad("Remarks", 10)+"|";
                        if(SUnitName.equals("All")){
                            SprnHead = SprnHead+common.Cad("Unit", 10)+"|";
                        }
                        
                        iLineSize = (SprnHead.length())/2;
                        fw.write(common.Replicate("-",iLineSize+iLineSize)+"\n");
                        fw.write(SprnHead+"\n");
                        fw.write(common.Replicate("-",iLineSize+iLineSize)+"\n");
                        Lctr = 0;
        }
        catch(Exception Ex)
        {
                Ex.printStackTrace();
        }
    }    
    public void createPrn() {
        try {
            String Sos = System.getProperty("os.name").toLowerCase();
            if(Sos.trim().startsWith("wind")){
                fw = new FileWriter("d:/Ut3DateWise.prn");
            }else{
                fw = new FileWriter("//software/QcPrint/Ut3DateWise.prn");
            }
            //printData(sData);
            //fw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void Closefile() throws IOException
    {
        fw.write("Report Taken on :"+common.parseDate(common.getSerDateTime())+"\n");
        fw.close();
    }    
}