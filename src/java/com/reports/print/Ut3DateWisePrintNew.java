/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.print;
import java.io.FileWriter;
import com.common.Common;
import java.io.IOException;
/**
 *
 * @author admin
 */
public class Ut3DateWisePrintNew {
    private String SprnHead="";
    FileWriter fw;
    int Lctr=0,Pctr=1;
    int iLineSize=0;
    Common common = new Common();
    public Ut3DateWisePrintNew() {
    }
    public void printData(String printbody,String Sdate,String SUnitName,String SProcess,String Shift,String SToDate,String sDeptName,int linecnt){
            try
            {
                    fw.write(printbody);
                    Lctr += 1;

                    iLineSize = (SprnHead.length())/4;
                    fw.write(common.Replicate("-",iLineSize+iLineSize)+"\n");
                    if(Lctr ==26) //36
                    {
                        fw.write("Report Taken on :"+common.parseDate(common.getSerDateTime())+"\n");
                        Pctr +=1;
                        setHead(Sdate,SUnitName,SProcess,Shift,SToDate,sDeptName);
                        Lctr =0;
                    }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
    }
    public void setHead(String SDate,String SUnitName,String SProcess,String Shift,String SToDate,String sDeptName){

        String Str1    = "Company    : AMARJOTHI SPINNING MILLS LIMITED\n";
        String Str2    = "Document   : UT3 REPORT From  ";
        String Str3    = "Unit       : "+SUnitName+"\n";
        String Str4    = "Department : "+sDeptName+"\n";
        String Str5    = "Process    : "+SProcess+"\n";
        String Str6    = "Shift      : "+Shift+"\n";
        SprnHead = "";
        try
        {
                        fw.write(Str1);
                        fw.write(Str2+common.parseDate(SDate)+" To "+common.parseDate(SToDate)+" \n");
                        fw.write(Str3);
                        fw.write(Str4);
                        fw.write(Str5);
                        fw.write(Str6);
                        //fw.write("Page"+common.Space(7)+": "+Pctr+"FPE\n");
                        fw.write("Page"+common.Space(7)+": "+Pctr+"g\n");

                        SprnHead = SprnHead+common.Pad("S.No",4)+"|";
                        SprnHead = SprnHead+common.Pad("Test", 5)+"|";
                        SprnHead = SprnHead+common.Cad("Party", 27)+"|";
                        SprnHead = SprnHead+common.Pad("Order No ",9)+"|";
                        SprnHead = SprnHead+common.Pad("Order ",6)+"|";
                        SprnHead = SprnHead+common.Cad("Cou", 3)+"|";
                        SprnHead = SprnHead+common.Cad("Shade", 22)+"|";
                        SprnHead = SprnHead+common.Cad("Entry", 10)+"|";
                        SprnHead = SprnHead+common.Cad("Dept", 7)+"|";
                        SprnHead = SprnHead+common.Cad("U%", 7)+"|";
                        SprnHead = SprnHead+common.Cad("Cvw", 7)+"|";
                        SprnHead = SprnHead+common.Cad("Cvm", 7)+"|";
                        SprnHead = SprnHead+common.Cad("Re-", 4)+"|";
                        SprnHead = SprnHead+common.Cad("Thin", 5)+"|";
                        SprnHead = SprnHead+common.Cad("Thin", 5)+"|";
                        SprnHead = SprnHead+common.Cad("Thin", 5)+"|";
                        SprnHead = SprnHead+common.Cad("Thick", 5)+"|";
                        SprnHead = SprnHead+common.Cad("Thick", 5)+"|";
                        SprnHead = SprnHead+common.Cad("Neps ", 5)+"|";
                        SprnHead = SprnHead+common.Cad("Neps ", 5)+"|";
                        SprnHead = SprnHead+common.Cad("Neps", 5)+"|";
                        SprnHead = SprnHead+common.Cad("Total", 6)+"|";
                        SprnHead = SprnHead+common.Cad("Total", 6)+"|\n";

                        SprnHead = SprnHead+common.Pad("",4)+"|";
                        SprnHead = SprnHead+common.Pad("No", 5)+"|";
                        SprnHead = SprnHead+common.Cad("Name", 27)+"|";
                        SprnHead = SprnHead+common.Pad("",9)+"|";
                        SprnHead = SprnHead+common.Pad("Weight ",6)+"|";
                        SprnHead = SprnHead+common.Cad("nt", 3)+"|";
                        SprnHead = SprnHead+common.Cad("", 22)+"|";
                        SprnHead = SprnHead+common.Cad("Date", 10)+"|";
                        SprnHead = SprnHead+common.Cad("", 7)+"|";
                        SprnHead = SprnHead+common.Cad("", 7)+"|";
                        SprnHead = SprnHead+common.Cad("", 7)+"|";
                        SprnHead = SprnHead+common.Cad("", 7)+"|";
                        SprnHead = SprnHead+common.Cad("marks", 4)+"|";
                        SprnHead = SprnHead+common.Cad("30", 5)+"|";
                        SprnHead = SprnHead+common.Cad("40", 5)+"|";
                        SprnHead = SprnHead+common.Cad("50", 5)+"|";
                        SprnHead = SprnHead+common.Cad("35", 5)+"|";
                        SprnHead = SprnHead+common.Cad("50", 5)+"|";
                        SprnHead = SprnHead+common.Cad("140", 5)+"|";
                        SprnHead = SprnHead+common.Cad("200", 5)+"|";
                        SprnHead = SprnHead+common.Cad("280", 5)+"|";
                        SprnHead = SprnHead+common.Cad("200", 6)+"|";
                        SprnHead = SprnHead+common.Cad("280", 6)+"|";
                        
                        if(SUnitName.equals("All")){
                            SprnHead = SprnHead+common.Cad("Unit", 10)+"|\n";
                            SprnHead = SprnHead+common.Cad("", 10)+"|";
                        }

                        iLineSize = (SprnHead.length())/4;
                        fw.write(common.Replicate("-",iLineSize+iLineSize)+"\n");
                        fw.write(SprnHead+"\n");
                        fw.write(common.Replicate("-",iLineSize+iLineSize)+"\n");
                        Lctr = 0;
        }
        catch(Exception Ex)
        {
                Ex.printStackTrace();
        }
    }
    public void createPrn() {
        try {
            String Sos = System.getProperty("os.name").toLowerCase();
            if(Sos.trim().startsWith("wind")){
                fw = new FileWriter("d:/Ut3DateWiseNew.prn");
            }else{
                fw = new FileWriter("//software/QcPrint/Ut3DateWiseNew.prn");
            }
            //printData(sData);
            //fw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void Closefile() throws IOException
    {
        fw.write("Report Taken on :"+common.parseDate(common.getSerDateTime())+"\n");
        fw.close();
    }
}