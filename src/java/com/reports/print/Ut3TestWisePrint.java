/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.print;
import java.io.FileWriter;
import com.common.Common;
import java.io.IOException;
/**
 *
 * @author admin
 */
public class Ut3TestWisePrint {
    private String SprnHead="";
    FileWriter fw;
    int Lctr=0,Pctr=1;
    int iLineSize=0;
    Common common = new Common();
    public Ut3TestWisePrint() {
    }
    public void printData(String printbody,String Sdate,java.util.HashMap theBaseMap){
        try
            {
                    fw.write(printbody);
                    Lctr += 1;
                    iLineSize = (SprnHead.length())/2;
                    fw.write(common.Replicate("-",iLineSize+iLineSize)+"\n");
                    if(Lctr ==28) //36
                    {
                        fw.write("Report Taken on :"+common.parseDate(common.getSerDateTime())+"\n");
                        Pctr +=1;
                        setTestBase(Sdate,Sdate,theBaseMap);
                        setHead(Sdate);
                        Lctr =0;
                    }
            }
            catch(Exception e)
            {
                    e.printStackTrace();
            }     
    }
    public void setHead(String SDate){

        //String Str1    = "Company   : AMARJOTHI SPINNING MILLS LIMITED\n";
        //String Str2    = "Document   : UT3 REPORT AS ON ";
        //String Str3    = "Unit       : "+SUnitName+"\n";
        //String Str4    = "Process    : "+SProcess+"\n";
        //String Str5    = "Shift      : "+Shift+"\n";
        SprnHead = "";
       try
       {
                        //fw.write(Str1);
                        //fw.write(Str2+common.parseDate(SDate)+" \n");

                        //fw.write(Str3);
                        //fw.write(Str4);
                        //fw.write(Str5);
           
                        SprnHead = SprnHead+common.Pad("S.No ",6)+"|";
                        SprnHead = SprnHead+common.Pad("Test No", 10)+"|";
                        SprnHead = SprnHead+common.Cad("UM", 10)+"|";
                        SprnHead = SprnHead+common.Cad("CVM", 10)+"|";
                        SprnHead = SprnHead+common.Cad("CVM(1m)", 10)+"|";
                        SprnHead = SprnHead+common.Cad("CVM(3m) ", 10)+"|";
                        SprnHead = SprnHead+common.Cad("Index", 10)+"|";
                        SprnHead = SprnHead+common.Cad("Thin(-30%)", 10)+"|";
                        SprnHead = SprnHead+common.Cad("Thin(-40%)", 10)+"|";
                        SprnHead = SprnHead+common.Cad("Thin(-50%) ", 10)+"|";
                        SprnHead = SprnHead+common.Cad("Thin(+35%)", 10)+"|";
                        SprnHead = SprnHead+common.Cad("Thin(+50%)", 10)+"|";
                        SprnHead = SprnHead+common.Cad("Neps(+140%)", 12)+"|";
                        SprnHead = SprnHead+common.Cad("Neps(+200%)", 12)+"|";
                        SprnHead = SprnHead+common.Cad("Neps(+280%)", 12)+"|";

                        iLineSize = (SprnHead.length())/2;
                        fw.write(common.Replicate("-",iLineSize+iLineSize)+"\n");
                        fw.write(SprnHead+"\n");
                        fw.write(common.Replicate("-",iLineSize+iLineSize)+"\n");
                        Lctr = 0;
        }
        catch(Exception Ex)
        {
                Ex.printStackTrace();
        }
    }
    public void setTestBase (String SDate,String STdate,java.util.HashMap theBaseMap)
    {
        String Str1    = "Company   : AMARJOTHI SPINNING MILLS LIMITED\n";
        String Str2    = "Document   : UT3 TEST REPORT As On ";
        SprnHead = "";
        try
        {
            fw.write(Str1);
            fw.write(Str2+common.parseDate(STdate)+" \n");
            fw.write("Page"+common.Space(7)+": "+Pctr+"g\n");
            SprnHead = SprnHead+(common.Replicate("-",168)+"\n");
            SprnHead = SprnHead+common.Pad("| Art No : "+common.parseNull((String)theBaseMap.get("ARTNO")), 66)+"|";
            SprnHead = SprnHead+common.Pad(" Test No : "+common.parseNull((String)theBaseMap.get("TESTID")), 66)+"|";
            SprnHead = SprnHead+common.Pad(" Fiber Assembly : "+common.parseNull((String)theBaseMap.get("FIBREASSEMBLY")),30)+"|\n";

            SprnHead = SprnHead+common.Pad("| Fiber : "+common.parseNull((String)theBaseMap.get("FIBER")), 66)+"|";
            SprnHead = SprnHead+common.Pad(" Order No : "+common.parseNull((String)theBaseMap.get("ORDERNO")), 66)+"|";
            SprnHead = SprnHead+common.Pad(" Unit :"+common.parseNull((String)theBaseMap.get("ORDERNO"))+"", 30)+"|\n";

            SprnHead = SprnHead+common.Pad("| V : "+common.parseNull((String)theBaseMap.get("VDATA")), 66)+"|";
            SprnHead = SprnHead+common.Pad(" T : "+common.parseNull((String)theBaseMap.get("TDATA")), 66)+"|";
            SprnHead = SprnHead+common.Pad(" Tests :"+common.parseNull((String)theBaseMap.get("TESTS")), 30)+"|\n";

            SprnHead = SprnHead+common.Pad("| Slot  : "+common.parseNull((String)theBaseMap.get("SLOT")), 66)+"|";
            SprnHead = SprnHead+common.Pad(" Yarn Tension : "+common.parseNull((String)theBaseMap.get("YARNTENSION")), 66)+"|";
            SprnHead = SprnHead+common.Pad(" Imperfections :"+common.parseNull((String)theBaseMap.get("IMPERFECTIONS")),30 )+"|\n";
            SprnHead = SprnHead+(common.Replicate("-",168)+"\n");
            fw.write(SprnHead+"\n");
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
     public void printMean(String Sprints)
    {
        String SprnHead = "",Sprint="";
        Sprint = Sprints;
        try{
            fw.write(Sprint+"\n");
            fw.write(common.Replicate("-",168)+"\n\n");
            //fw.write("Report Taken on :"+common.parseDate(common.getSerDateTime())+"\n");
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public void createPrn() {
        try {
            String Sos = System.getProperty("os.name").toLowerCase();
            if(Sos.trim().startsWith("wind")){
                fw = new FileWriter("d:/Ut3TestWise.prn");
            }else{
                fw = new FileWriter("//software/QcPrint/Ut3TestWise.prn");
            }
            //printData(sData);
            //fw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void Closefile() throws IOException
    {
        fw.write("Report Taken on :"+common.parseDate(common.getSerDateTime())+"\n");
        fw.close();
    }    
}