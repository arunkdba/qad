package com.reports.servlets;

import com.common.Common;
import com.reports.data.MachineWiseTestData;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MachineReport
  extends HttpServlet
{
  Common common = new Common();
  
  protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
    response.setContentType("text/html;charset=UTF-8");
    PrintWriter out = response.getWriter();
    try
    {
      String sFromDate = this.common.pureDate(request.getParameter("tdatefrom"));
      getData(request, response);
      request.getRequestDispatcher("MachineReport.jsp").forward(request, response);
    }
    finally
    {
      out.close();
    }
  }
  
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
    processRequest(request, response);
  }
  
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
    processRequest(request, response);
  }
  
  public String getServletInfo()
  {
    return "Short description";
  }
  
  private void getData(HttpServletRequest request, HttpServletResponse response)
  {
    String sFromDate = common.pureDate(request.getParameter("tdatefrom"));
    String sCountcode = common.parseNull(request.getParameter("countname"));
    String sUnits = common.parseNull(request.getParameter("sunits"));
    String sDeptCode = common.parseNull(request.getParameter("ptype"));
    String sDbCode = common.parseNull(request.getParameter("machine"));
    String sExtCount = common.parseNull(request.getParameter("extcount"));
    String SUnitName = "";String sProcessType = "";String sExtc = "ALL";
    if (sUnits.trim().equals("1")) {
      SUnitName = "A-Unit";
    }
    if (sUnits.trim().equals("2")) {
      SUnitName = "B-Unit";
    }
    if (sUnits.trim().equals("3")) {
      SUnitName = "Sample-Unit";
    }
    if (sUnits.trim().equals("10")) {
      SUnitName = "C-Unit";
    }
    if (sExtCount.equals("1")) {
      sExtc = "NO";
    }
    if (sExtCount.equals("2")) {
      sExtc = "YES";
    }
    if (sDeptCode.equals("43")) {
      sProcessType = "OE";
    } else {
      sProcessType = "SPINNING";
    }
    MachineWiseTestData getdata = new MachineWiseTestData();
    List theList = getdata.getTestDetails(sFromDate, sUnits, sDbCode, sDeptCode, sCountcode, sExtCount);
    
    request.setAttribute("Unit", SUnitName);
    request.setAttribute("Process", sProcessType);
    request.setAttribute("theTestDetailsList", theList);
    request.setAttribute("Fdate", sFromDate);
    request.setAttribute("ExtCount", sExtc);
  }
}
