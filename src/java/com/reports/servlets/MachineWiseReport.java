/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.servlets;
import com.common.Common;
import com.reports.data.MachineWiseTestData;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author root
 */
public class MachineWiseReport extends HttpServlet {  
    Common common = new Common();
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String sFromDate = common.pureDate(request.getParameter("tdatefrom"));
             getData(request, response);
             request.getRequestDispatcher("MachineWiseReport.jsp").forward(request, response);
        } finally {            
            out.close();
        }
    }
    private void getData(HttpServletRequest request, HttpServletResponse response) {
        String sFromDate=   common.pureDate(request.getParameter("tdatefrom"));
        String sToDate=   common.pureDate(request.getParameter("tdateto"));
        String sdbcode  =   common.parseNull(request.getParameter("machine"));
        String sUnits   =   common.parseNull(request.getParameter("sunits"));
        String sDeptCode=   common.parseNull(request.getParameter("ptype"));
        String sDbCode="";        
        String SUnitName = "",sProcessType="";
            if(sUnits.trim().equals("1"))  {  SUnitName = "A-Unit";}
            if(sUnits.trim().equals("2"))  {  SUnitName = "B-Unit";}
            if(sUnits.trim().equals("3"))  {  SUnitName = "Sample-Unit";}
            if(sUnits.trim().equals("10")) {  SUnitName = "C-Unit";}
            
            if(sDeptCode.equals("43")){sProcessType = "OE";}
            else{sProcessType="SPINNING";}
        if(sUnits.equals("1")){sDbCode="2";}
        if(sUnits.equals("2")){sDbCode="1";}
        MachineWiseTestData getdata = new MachineWiseTestData();
        java.util.List theList = getdata.getTestDetails(sFromDate,sToDate,sUnits,sDbCode,sDeptCode);
        
        request . setAttribute("Unit",SUnitName);
        request . setAttribute("Process",sProcessType);
        request . setAttribute("theTestDetailsList", theList);        
        request . setAttribute("Fdate",sFromDate);
        request . setAttribute("Todate",sToDate);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
