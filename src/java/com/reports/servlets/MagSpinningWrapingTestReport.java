/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.servlets;

import com.common.Common;
import com.reports.data.MagSpinningWrappingTestData;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author root
 */
public class MagSpinningWrapingTestReport extends HttpServlet {
        Common common = new Common();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String sAbst = common.parseNull(request.getParameter("abst"));
        try {
            getData(request, response);
            if(sAbst.equals("1")){
                request.getRequestDispatcher("magSpinningWrappingTestabstract.jsp").forward(request, response);
            }
            else{
                request.getRequestDispatcher("magSpinningWrappingTest.jsp").forward(request, response);
            }
            
        } finally {
            out.close();
        }
    }
    private void getData(HttpServletRequest request, HttpServletResponse response) {
        String sToDate = common.pureDate(request.getParameter("tdateto"));
        String sTestNo = common.parseNull(request.getParameter("testno"));
        String sAbst = common.parseNull(request.getParameter("abst"));
        java.util.List theList =null;
        java.util.List theBase = null;
        MagSpinningWrappingTestData getdata = new MagSpinningWrappingTestData();
        if(sAbst.equals("1")){
            theList = getdata.getSpinTestAbst(sToDate,sToDate,sTestNo);
        }
        else{
            theList = getdata.getSpinningWrappingTestData(sToDate,sToDate,sTestNo);
            theBase = getdata.getSpinTestBase(sToDate,sToDate,sTestNo);
        }
        
        request . setAttribute("theTestDetailsList", theList);
        request . setAttribute("theTestBase",theBase);
        request . setAttribute("TestNo", sTestNo);
        request . setAttribute("Fdate",sToDate);
        request . setAttribute("Tdate",sToDate);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
