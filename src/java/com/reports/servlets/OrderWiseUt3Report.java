/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.servlets;

import com.common.Common;
import com.reports.classes.Ut3Details;
import com.reports.data.OrderWiseUt3DataBUnit;
import com.reports.data.Ut3DataAunit;
import com.reports.data.Ut3DataCunit;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author admin
 */
public class OrderWiseUt3Report extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    Common common = new Common();
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String SOrderNo = common.parseNull(request.getParameter("sorderno"));
            String sFromDate = common.pureDate(request.getParameter("tdatefrom"));
            String sToDate = common.pureDate(request.getParameter("tdateto"));
            String sUnit = common.parseNull(request.getParameter("Unit"));
            getData(request, response);
            if(sUnit.equals("1"))
            {
                request.getRequestDispatcher("Ut3Aunit.jsp").forward(request, response);
            }
            else if(sUnit.equals("2"))
            {
                request.getRequestDispatcher("OrderWiseUt3Bunit.jsp").forward(request, response);
            }
            else if(sUnit.equals("10"))
            {
                request.getRequestDispatcher("Ut3Cunit.jsp").forward(request, response);
            }
            
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    private void getData(HttpServletRequest request, HttpServletResponse response) {
        String sFromDate = common.pureDate(request.getParameter("tdatefrom"));
        String sToDate = common.pureDate(request.getParameter("tdateto"));
        String sUnit = common.parseNull(request.getParameter("Unit"));
        String sProcessCode=   common.parseNull(request.getParameter("ptype"));
        String sOrderNo = common.parseNull(request.getParameter("orderno"));
        java.util.List theList = new java.util.ArrayList();
        java.util.List theRmk  = new java.util.ArrayList();
        
        if(sUnit.equals("1")){
            Ut3DataAunit ut3data = new Ut3DataAunit();
            theList = ut3data.getUt3Data(sFromDate,sToDate,sUnit,sProcessCode,sOrderNo);
        }
        else if(sUnit.equals("2")){
            OrderWiseUt3DataBUnit ut3data = new OrderWiseUt3DataBUnit();
            theList = ut3data.getUt3Data(sFromDate,sToDate,sUnit,sOrderNo);
            //getsame(theList);
        }
        else if(sUnit.equals("10")){
            Ut3DataCunit ut3data = new Ut3DataCunit();
            theList = ut3data.getUt3Data(sFromDate,sToDate,sUnit,sOrderNo);
        }
        //java.util.List theList = getdata.getUt3(sFromDate,sToDate,sUnit);
        //java.util.List cspList = getdata.getCsp(sFromDate, sToDate,sUnit);
        //java.util.List rkmList = getdata.getRkm(sFromDate, sToDate,sUnit);
        //request . setAttribute("theUt3List", theList);
        //request . setAttribute("theCspList", cspList);
        //request . setAttribute("theRkmList", rkmList);
        request . setAttribute("theUt3List", theList);
        request . setAttribute("theRkmList", theRmk);
        request . setAttribute("Fdate",sFromDate);
        request . setAttribute("Tdate",sToDate);
        request . setAttribute("Process",sProcessCode);
    }

    private void getsame(java.util.List theList){
        for(int i=0;i<theList.size();i++){
 
            Ut3Details details = (Ut3Details) theList.get(i);
            
            System.out.print(details.getOrderno()+"\t\t");
            System.out.println(details.getMachine()+"\n\n");

                    java.util.List ls = details.getOrderList();
                    
                    for(int j=0;j<ls.size();j++){
                        java.util.HashMap hs = (java.util.HashMap)ls.get(j);
                        System.out.print(hs.get("DATE"));
                        System.out.print("\t");
                        System.out.print(hs.get("COUNT"));
                        System.out.print("\t");
                        System.out.print(hs.get("STRENGTH"));
                        System.out.print("\t");
                        System.out.print(hs.get("CNTREADING"));
                        System.out.print("\t");
                        System.out.print(hs.get("SYS"));
                        System.out.print("\n");
                    }
        }
    }
}