/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.servlets;

import com.common.Common;
import com.reports.data.Ut3RkmDateWiseData;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author root
 */
public class RkmReportDateWiseNew extends HttpServlet {

        Common common = new Common();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        try {
           String sFromDate = common.pureDate(request.getParameter("tdatefrom"));
            String sToDate = common.pureDate(request.getParameter("tdateto"));
            String sUnitCode = common.parseNull(request.getParameter("sunits"));
            getData(request, response);
            if(!sUnitCode.equals("All"))
            {
                com.reports.data.Units unit = new com.reports.data.Units();
                String SUnitName = unit.getUnitName(sUnitCode);
                session.removeAttribute("UnitName");
                session.setAttribute("UnitName", SUnitName);
            }
            else
            {
                session.removeAttribute("UnitName");
                session.setAttribute("UnitName", "All");
            }
            request.getRequestDispatcher("rkmcritnew.jsp").forward(request, response);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    private void getData(HttpServletRequest request, HttpServletResponse response) {
        String sFromDate = common.pureDate(request.getParameter("tdatefrom"));
        String sToDate = common.pureDate(request.getParameter("tdateto"));
        String sUnitCode = common.parseNull(request.getParameter("sunits"));
        String sProcessCode=   common.parseNull(request.getParameter("ptype"));
        String sShift =   common.parseNull(request.getParameter("shift"));
        String sOrderbyvalue = common.parseNull(request.getParameter("orderby"));
        String sProcessType="",ShiftName="";
        
        if(sProcessCode.equals("1")){sProcessType = "OE";}
        else{sProcessType="SPINNING";}
        
        if(sShift.trim().equals("1"))  {  ShiftName = "I-Shift";}
            if(sShift.trim().equals("2"))  {  ShiftName = "II-Shift";}
            if(sShift.trim().equals("3"))  {  ShiftName = "III-Shift";}
            if(sShift.trim().equals("All"))  {  ShiftName = "All";}
            
        Ut3RkmDateWiseData getdata = new Ut3RkmDateWiseData();
        java.util.List theList = getdata.getRkmNew(sFromDate,sToDate,sUnitCode,sProcessCode,sShift,sOrderbyvalue);        
        request . setAttribute("theRkmList", theList);
        request . setAttribute("Fdate",sFromDate);
        request . setAttribute("Tdate",sToDate);
        request . setAttribute("Process",sProcessType);
        request . setAttribute("Shift",ShiftName);
    }

}
