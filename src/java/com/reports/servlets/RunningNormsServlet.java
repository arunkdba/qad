
package com.reports.servlets;

import com.common.Common;
import com.jdbc.connection.JDBCScmConnection;
import com.reports.data.RunningNormsDataAunit;
import com.reports.data.RunningNormsDataBunit;
import com.reports.data.RunningNormsDataCunit;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author root
 */
public class RunningNormsServlet extends HttpServlet {
    java.sql.Connection theScmConnection  = null;
    Common common = new Common();
    String sProcessCode="",sUnit="",sUnits="";
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            //sUnit = common.parseNull(request.getParameter("units"));
            //sProcessCode = common.parseNull(request.getParameter("ptype"));
            String sOrderNo = common.parseNull(request.getParameter("ROrderNo"));
            String sFlag = common.parseNull(request.getParameter("Orders"));
            String spolyfrom = common.parseNull(request.getParameter("polyfrom"));

            //System.out.println("sUnit-->"+sUnit);

                if(sFlag.equals("1")){
                    getOrders(request,response);
                    request.getRequestDispatcher("runningnorms.jsp").forward(request, response);
                }
                else{
                    System.out.println("In Run condition");
                    getData(request,response);
                    request.getRequestDispatcher("runningnormsAunit.jsp").forward(request, response);
                    /*if(sUnit.equals("1")){
                        request.getRequestDispatcher("runningnormsAunit.jsp").forward(request, response);
                    }
                    if(sUnit.equals("2")){
                        request.getRequestDispatcher("runningnormsBunit.jsp").forward(request, response);
                    } */
                }

        } finally {
            out.close();
        }
    }

    public void getOrders(HttpServletRequest request, HttpServletResponse response)
    {
        sUnit = common.parseNull(request.getParameter("units"));
        sProcessCode = common.parseNull(request.getParameter("ptype"));

        String sdepthfrom = common.parseNull(request.getParameter("depthfrom"));
        String sdepthto = common.parseNull(request.getParameter("depthto"));

        String sbleachabove = common.parseNull(request.getParameter("bleachabove"));
        String sbleachbelow = common.parseNull(request.getParameter("bleachbelow"));

        String sbleachout = common.parseNull(request.getParameter("bleachout"));

        String spolyfrom = common.parseNull(request.getParameter("polyfrom"));
        String spolyto = common.parseNull(request.getParameter("polyto"));

        String spolyabove = common.parseNull(request.getParameter("polyabove"));
        String spolybelow = common.parseNull(request.getParameter("polybelow"));

        String spolyupto = common.parseNull(request.getParameter("polyupto"));

        String sFromDate = common.pureDate(request.getParameter("tdatefrom"));
        String sToDate = common.pureDate(request.getParameter("tdateto"));

        String sUnitCondition="";

        if(sUnit.equals("10")){
            sUnitCondition = "and RegularOrder.UnitCode in (10,12)";
        }
        else{
            sUnitCondition = "and RegularOrder.UnitCode="+sUnit;
        }

        StringBuffer sb = new StringBuffer();
        PreparedStatement pst=null;
        ResultSet rst=null;
        java.util.List thelist = new java.util.ArrayList();

            sb.append(" select rmixir.ordno,fibre.fibretypecode,sum(Mrat) as Mrat,yarnCount.CountName from ");
            sb.append(" rmixir ");
            sb.append(" inner join rblenr on rblenr.mixno= rmixir.mixno ");
            sb.append(" inner join fibre on fibre.fibrecode= rblenr.fibcd ");
            sb.append(" inner join RegularOrder on Regularorder.Rorderno=rmixir.ordno "+sUnitCondition+" ");
            if(sProcessCode.equals("1")){
                sb.append(" inner join ProcessingType on ProcessingType.ProcessCode = regularorder.ProcessTypeCode and ProcessingType.OEStatus = 1 ");
            }
            else{
                sb.append(" inner join ProcessingType on ProcessingType.ProcessCode = regularorder.ProcessTypeCode and ProcessingType.OEStatus = 0 ");
            }
            sb.append(" Inner Join YarnCount on yarnCount.CountCode=RegularOrder.CountCode ");
            sb.append(" Left Join FibreForm on FibreForm.FormCode = RegularOrder.FibreFormCode ");
            sb.append(" where rmixir.ordno not like '%DM%' ");
            sb.append(" and RegularOrder.OrderDate >= "+sFromDate+" and  RegularOrder.OrderDate <= "+sToDate+" ");

            if(!sbleachabove.equals("") && !sbleachbelow.equals("")){
                sb.append(" and fibre.colorcode in(561,241) ");
                sb.append(" and GET_BLEACH_ORDER(RegularOrder.Orderid)>0 ");
            }
            if(sbleachout.equals("1")){
                sb.append(" and GET_BLEACH_ORDER(RegularOrder.Orderid)=0 ");
            }
            if(!spolyfrom.equals("") && !spolyto.equals("")){
                sb.append(" and fibre.fibretypecode=3 and FibreForm.FORMCODE=0 ");
                sb.append(" and GET_POLY_ORDER(RegularOrder.Orderid,2)=0 ");
            }
            else{
                sb.append(" and GET_POLY_ORDER(RegularOrder.Orderid,1)=0 ");
            }
            if(!sdepthfrom.equals("") && !sdepthto.equals("")){
                sb.append(" and rmixir.Depth >= "+sdepthfrom+" and rmixir.Depth <= "+sdepthto+" ");
            }
            sb.append(" group by rmixir.ordno,fibre.fibretypecode,CountName ");
            if(!spolyfrom.equals("") && !spolyto.equals("")){
              sb.append(" having sum(Mrat) >= "+spolyfrom+" and sum(Mrat) <= "+spolyto+" ");
            }
            if(!sbleachabove.equals("") && !sbleachbelow.equals("")){
                sb.append(" having sum(Mrat) >= "+sbleachabove+" and sum(Mrat) <= "+sbleachbelow+" ");
            }
            sb.append(" order by CountName ");

            System.out.println("In Qry-->"+sb.toString());

            /*sb.append(" select ordno,CountName,sum(Mrat) from ( ");
            sb.append(" select rmixir.ordno,fibre.fibretypecode,sum(Mrat) as Mrat,rmixir.Depth,yarnCount.CountName from ");
            sb.append(" rmixir ");
            sb.append(" inner join rblenr on rblenr.mixno= rmixir.mixno ");
            sb.append(" inner join fibre on fibre.fibrecode= rblenr.fibcd ");
            sb.append(" inner join RegularOrder on Regularorder.Rorderno=rmixir.ordno "+sUnitCondition+" ");
            if(sProcessCode.equals("1")){
                sb.append(" inner join ProcessingType on ProcessingType.ProcessCode = regularorder.ProcessTypeCode and ProcessingType.OEStatus = 1 ");
            }
            else{
                sb.append(" inner join ProcessingType on ProcessingType.ProcessCode = regularorder.ProcessTypeCode and ProcessingType.OEStatus = 0 ");
            }
            sb.append(" Inner Join YarnCount on yarnCount.CountCode=RegularOrder.CountCode ");
            sb.append(" Left Join FibreForm on FibreForm.FormCode = RegularOrder.FibreFormCode ");
            sb.append(" where rmixir.ordno not like '%DM%' ");
            sb.append(" and RegularOrder.OrderDate >= "+sFromDate+" and  RegularOrder.OrderDate <= "+sToDate+" ");

            if(!sbleachabove.equals("") && !sbleachbelow.equals("")){
                sb.append(" and fibre.colorcode in(561,241) ");
            }
            if(!spolyfrom.equals("") && !spolyto.equals("")){
                sb.append(" and fibre.fibretypecode=3 and FibreForm.FORMCODE=0 ");
                sb.append(" and GET_POLY_ORDER(RegularOrder.Orderid)>0 ");
                sb.append(" and Mrat >= "+spolyfrom+" and Mrat <= "+spolyto+" ");
            }
            else{
                sb.append(" and GET_POLY_ORDER(RegularOrder.Orderid)=0 ");
            }
            if(!sdepthfrom.equals("") && !sdepthto.equals("")){
                sb.append(" and rmixir.Depth >= "+sdepthfrom+" and rmixir.Depth <= "+sdepthto+" ");
            }
            if(!sbleachabove.equals("") && !sbleachbelow.equals("")){
                sb.append(" and Mrat >= "+sbleachabove+" and Mrat <= "+sbleachbelow+" ");
            }
            //sb.append(" and RegularOrder.Rorderno='LM74692' ");
            sb.append(" group by rmixir.ordno,fibre.fibretypecode,rmixir.Depth,CountName ");
            sb.append(" ) group by OrdNo,CountName ");
            sb.append(" order by CountName ");*/

            
        try
        {
            if(theScmConnection==null)
            {
                JDBCScmConnection  jdbc = JDBCScmConnection.getJDBCConnection();
                theScmConnection = jdbc . getConnection();
            }
            pst = theScmConnection.prepareStatement(sb.toString());
            rst = pst . executeQuery();
            ResultSetMetaData rsmd    = rst.getMetaData();
            while(rst.next())
            {
                    HashMap themap = new HashMap();
                    for(int i=0;i<rsmd.getColumnCount();i++)
                    {
                        themap.put(rsmd.getColumnName(i+1),rst.getString(i+1));
                    }
                    thelist.add(themap);
            }    
            rst.close();
            pst.close();
            rst=null;
            pst=null;
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        request . setAttribute("theOrderList", thelist);
    }
    
    private void getData(HttpServletRequest request, HttpServletResponse response) {
        String sFromDate = common.pureDate(request.getParameter("tdatefrom"));
        String sToDate = common.pureDate(request.getParameter("tdateto"));
        //String sOrderNo = common.parseNull(request.getParameter("ROrderNo"));
        String sOrderNo = "";
        String[] OrdsReceived = request.getParameterValues("ROrderNo");
        java.util.List theList = new java.util.ArrayList();
        try{
        if(OrdsReceived!=null && OrdsReceived.length>0){
            for(int i=0;i<OrdsReceived.length;i++)
            {
                sOrderNo += "'"+OrdsReceived[i]+"',";
            }
        }

        //System.out.println("sProcessCode-->"+sProcessCode);
        //System.out.println("sUnit-->"+sUnit);
        
        if(sUnit.equals("1")){
                RunningNormsDataAunit ut3data = new RunningNormsDataAunit();
                theList = ut3data.getUt3Data(sFromDate,sToDate,sUnit,sProcessCode,sOrderNo.substring(0,sOrderNo.length()-1));
        }

        if(sUnit.equals("2")){
            RunningNormsDataBunit ut3data = new RunningNormsDataBunit();
            theList = ut3data.getUt3Data(sFromDate,sToDate,sUnit,sOrderNo.substring(0,sOrderNo.length()-1));
        }
        if(sUnit.equals("10")){
            RunningNormsDataCunit ut3data = new RunningNormsDataCunit();
            theList = ut3data.getUt3Data(sFromDate,sToDate,sUnit,sOrderNo.substring(0,sOrderNo.length()-1));
        }
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        request . setAttribute("theUt3List", theList);
        request . setAttribute("Process",sProcessCode);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}



/*public void getOrders(HttpServletRequest request, HttpServletResponse response)
    {
        String sUnits = common.parseNull(request.getParameter("units"));

        String sdepthfrom = common.parseNull(request.getParameter("depthfrom"));
        String sdepthto = common.parseNull(request.getParameter("depthto"));

        String sbleachabove = common.parseNull(request.getParameter("bleachabove"));
        String sbleachbelow = common.parseNull(request.getParameter("bleachbelow"));

        String sbleachupto = common.parseNull(request.getParameter("bleachupto"));

        String spolyfrom = common.parseNull(request.getParameter("polyfrom"));
        String spolyto = common.parseNull(request.getParameter("polyto"));

        String spolyabove = common.parseNull(request.getParameter("polyabove"));
        String spolybelow = common.parseNull(request.getParameter("polybelow"));

        String spolyupto = common.parseNull(request.getParameter("polyupto"));
        
        String sFromDate = common.pureDate(request.getParameter("tdatefrom"));
        String sToDate = common.pureDate(request.getParameter("tdateto"));

        StringBuffer sb = new StringBuffer();
        PreparedStatement pst=null;
        ResultSet rst=null;
        java.util.List thelist = new java.util.ArrayList();
        
        if(!spolyfrom.equals("") || !spolyto.equals("") || !spolyabove.equals("") || !spolybelow.equals("") || !spolyupto.equals("") || !sUnits.equals("")){

            sUnit = sUnits;
            sProcessCode = "0";

            sb.append(" select Distinct ordno,CountName from ( ");
            sb.append(" select rmixir.ordno,rblenr.fibcd,sum(Mrat) as Mrat,rmixir.Depth,yarnCount.CountName from ");
            sb.append(" rmixir ");
            sb.append(" inner join rblenr on rblenr.mixno= rmixir.mixno ");
            sb.append(" inner join fibre on fibre.fibrecode= rblenr.fibcd and fibre.fibretypecode=3 ");
            sb.append(" inner join RegularOrder on Regularorder.Rorderno=rmixir.ordno and RegularOrder.UnitCode = "+sUnits+" ");
            sb.append(" inner join ProcessingType on ProcessingType.ProcessCode = regularorder.ProcessTypeCode and ProcessingType.OEStatus = 0 ");
            sb.append(" Inner Join YarnCount on yarnCount.CountCode=RegularOrder.CountCode ");
            sb.append(" Left Join FibreForm on FibreForm.FormCode = RegularOrder.FibreFormCode and FibreForm.FORMCODE=0 ");
            sb.append(" where rmixir.ordno not like '%DM%' ");
            sb.append(" and RegularOrder.OrderDate >= "+sFromDate+" and  RegularOrder.OrderDate <= "+sToDate+" ");
            sb.append(" group by rmixir.ordno,rblenr.fibcd,rmixir.Depth,CountName ");
            sb.append(" ) ");
            if(!spolyfrom.equals("") && !spolyto.equals("")){
                sb.append(" where Mrat >= "+spolyfrom+" and Mrat <= "+spolyto+" ");
            }
            if(!spolyabove.equals("") && !spolybelow.equals("")){
                sb.append(" where Mrat >= "+spolyabove+" and Mrat <= "+spolybelow+" ");
            }
            if(!spolyupto.equals("")){
                sb.append(" where Mrat >= 0 and Mrat <= "+spolyupto+" ");
            }
            sb.append(" order by CountName ");
            
            //System.out.println("In Qry Poly-->"+sb.toString());
                        
        }
        else{
            sProcessCode = "1";
            sUnit = "1";

            sb.append(" select Distinct ordno,CountName from ( ");
            sb.append(" select rmixir.ordno,rblenr.fibcd,sum(Mrat) as Mrat,rmixir.Depth,yarnCount.CountName from ");
            sb.append(" rmixir ");
            sb.append(" inner join rblenr on rblenr.mixno= rmixir.mixno ");
            sb.append(" inner join fibre on fibre.fibrecode= rblenr.fibcd ");
            sb.append(" inner join RegularOrder on Regularorder.Rorderno=rmixir.ordno and RegularOrder.UnitCode=1 ");
            sb.append(" inner join ProcessingType on ProcessingType.ProcessCode = regularorder.ProcessTypeCode and ProcessingType.OEStatus = 1 ");
            sb.append(" Inner Join YarnCount on yarnCount.CountCode=RegularOrder.CountCode ");
            sb.append(" where rmixir.ordno not like '%DM%' ");
            sb.append(" and RegularOrder.OrderDate >= "+sFromDate+" and  RegularOrder.OrderDate <= "+sToDate+" ");
            if(!sbleachabove.equals("") && !sbleachbelow.equals("")){
                sb.append(" and fibre.colorcode in(561,241) ");
            }
            if(!sdepthfrom.equals("") && !sdepthto.equals("")){
                sb.append(" and rmixir.Depth >= "+sdepthfrom+" and rmixir.Depth <= "+sdepthto+" ");
            }
            //sb.append(" and RegularOrder.Rorderno='LM74692' ");
            sb.append(" group by rmixir.ordno,rblenr.fibcd,rmixir.Depth,CountName ");
            sb.append(" ) ");
            
            if(!sbleachabove.equals("") && !sbleachbelow.equals("")){
                sb.append(" where Mrat >= "+sbleachabove+" and Mrat <= "+sbleachbelow+" ");
            }
            if(!sbleachupto.equals("")){
                sb.append(" where Mrat >= 0 and Mrat <= "+sbleachupto+" ");
            }
            sb.append(" order by CountName ");
            
            //System.out.println("In Qry-->"+sb.toString());
        }
        try
        {
            if(theScmConnection==null)
            {
                JDBCScmConnection  jdbc = JDBCScmConnection.getJDBCConnection();
                theScmConnection = jdbc . getConnection();
            }
            pst = theScmConnection.prepareStatement(sb.toString());
            rst = pst . executeQuery();
            ResultSetMetaData rsmd    = rst.getMetaData();
            while(rst.next())
            {
                    HashMap themap = new HashMap();
                    for(int i=0;i<rsmd.getColumnCount();i++)
                    {
                        themap.put(rsmd.getColumnName(i+1),rst.getString(i+1));
                    }
                    thelist.add(themap);
            }            
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        request . setAttribute("theOrderList", thelist);
    }*/