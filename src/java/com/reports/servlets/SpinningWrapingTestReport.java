/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.common.Common;
import com.reports.data.SpinningWrappingTestData;

/**
 *
 * @author admin
 */
public class SpinningWrapingTestReport extends HttpServlet {
     Common common = new Common();
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
             String sFromDate = common.pureDate(request.getParameter("tdatefrom"));
             String sToDate = common.pureDate(request.getParameter("tdateto"));
             String sTestNo = request.getParameter("testno");
             String sOrderNo = common.parseNull(request.getParameter("orderno"));
             String sAbst = common.parseNull(request.getParameter("abst"));
             getData(request, response);
             if(sAbst.equals("1")){
                request.getRequestDispatcher("SpinningWrappingAbstract.jsp").forward(request, response);
             }
             else
             {
                request.getRequestDispatcher("SpinningWrappingTest.jsp").forward(request, response);
             }
        }
        finally {
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void getData(HttpServletRequest request, HttpServletResponse response) {
        String sFromDate = common.pureDate(request.getParameter("tdatefrom"));
        String sToDate = common.pureDate(request.getParameter("tdateto"));
        String sTestNo = common.parseNull(request.getParameter("testno"));
        String sOrderNo = common.parseNull(request.getParameter("orderno"));
        String sAbst    = common.parseNull(request.getParameter("abst"));
        String sdbcode = common.parseNull(request.getParameter("machine"));  // sdbcode = 1-Bunit,sdbcode = 2-CUnit
        SpinningWrappingTestData getdata = new SpinningWrappingTestData();
        java.util.List theList = getdata.getTestDetails(sFromDate,sToDate,sTestNo,sOrderNo,sAbst,sdbcode);
        java.util.List theBase = getdata.getTestBase(sFromDate,sToDate,sTestNo,sdbcode);
        request . setAttribute("theTestDetailsList", theList);
        request . setAttribute("theTestBase",theBase);
        request . setAttribute("TestNo", sTestNo);
        request . setAttribute("Fdate",sFromDate);
        request . setAttribute("Tdate",sToDate);
    }
}