/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.servlets;

import com.common.Common;
import com.reports.classes.CoEffVarient;
import com.reports.data.Ut3DataAunit;
import com.reports.data.Ut3DataBUnit;
import com.reports.data.Ut3DataCunit;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author admin
 */
public class Ut3ReportDetails extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    Common common = new Common();
    java.util.List theList;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            String SOrderNo = common.parseNull(request.getParameter("sorderno"));
            String sFromDate = request.getParameter("datefrom");
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Ut3ReportDetails</title>");            
            out.println("</head>");
            out.println("<body>");
            getData(request,response);
            HtmlBody(out);            
            out.println("</body>");
            out.println("</html>");
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    private void getData(HttpServletRequest request, HttpServletResponse response) {
        theList = new java.util.ArrayList();
        String SOrderNo = common.parseNull(request.getParameter("sorderno"));
        String sFromDate = request.getParameter("datefrom");
        String sUnit = common.parseNull(request.getParameter("Unit"));
        if(sUnit.equals("1")){
            Ut3DataAunit ut3data = new Ut3DataAunit();
            theList = ut3data.getUt3DataDetails(sFromDate,SOrderNo,sUnit);
        }
        else if(sUnit.equals("2")){
            Ut3DataBUnit ut3data = new Ut3DataBUnit();
            theList = ut3data.getUt3DataDetails(sFromDate,SOrderNo,sUnit);
        }
        else if(sUnit.equals("10")){
            Ut3DataCunit ut3data = new Ut3DataCunit();
            theList = ut3data.getUt3DataDetails(sFromDate,SOrderNo,sUnit);
        }
        
    }
    private void HtmlBody(PrintWriter out) {
        java.util.HashMap theDataMap =new java.util.HashMap(); 
        ArrayList<String> arrlistcnt = new ArrayList<String>();
        ArrayList<String> arrliststr = new ArrayList<String>();
        CoEffVarient cev = new CoEffVarient();
        out.println("<table border='1' width='100%' class='ut3report'> ");
            out.println("<tr> ");
                out.println("<th rowspan='2' align='center' valign='middle' scope='col'>S.No</th> ");
                out.println("<th rowspan='2' align='center' valign='middle' scope='col'>DATE</th> ");
                out.println("<th rowspan='2' align='center' valign='middle' scope='col'>RF<br> NO </th> ");
                out.println("<th rowspan='2' align='center' valign='middle' scope='col'>PARTY NAME </th> ");
                out.println("<th rowspan='2' align='center' valign='middle' scope='col'>O.NO</th> ");
                out.println("<th rowspan='2' align='center' valign='middle' scope='col'>SHADE</th> ");
                out.println("<th rowspan='2' align='center' valign='middle' scope='col'>O.QTY</th> ");
                out.println("<th rowspan='2' align='center' valign='middle' scope='col'>U%</th> ");
                out.println("<th rowspan='2' align='center' valign='middle' scope='col'>CVM</th> ");
                out.println("<th rowspan='2' align='center' valign='middle' scope='col'>THIN</th> ");
                out.println("<th rowspan='2' align='center' valign='middle' scope='col'>THICK</th> ");
                out.println("<th rowspan='2' align='center' valign='middle' scope='col'>NEPS</th> ");
                out.println("<th rowspan='2' align='center' valign='middle' scope='col'>TOTAL<br> IMP </th> ");
                out.println("<th colspan='3' align='center' valign='middle' scope='col'>NO<br> RE </th> ");
                out.println("<th colspan='5' align='center' valign='middle' scope='col'>COUNT</th> ");
                out.println("<th rowspan='2' align='center' valign='middle' scope='col'>COUNT<br> CV% </th> ");
                out.println("<th rowspan='2' align='center' valign='middle' scope='col'>COUNT<br> AVG </th> ");
                out.println("<th colspan='5' align='center' valign='middle' scope='col'>STRENGTH</th> ");
                out.println("<th rowspan='2' align='center' valign='middle' scope='col'>STRENGTH<br> CV% </th> ");
                out.println("<th rowspan='2' align='center' valign='middle' scope='col'>STRENGTH<br> AVG </th> ");
                out.println("<th colspan='5' align='center' valign='middle' scope='col'>CSP</th> ");
                out.println("<th rowspan='2' align='center' valign='middle' scope='col'>CSP<br> AVG </th> ");
                out.println("<th rowspan='2' align='center' valign='middle' scope='col'>SYS</th> ");
                out.println("<th rowspan='2' align='center' valign='middle' scope='col'>RKM</th> ");
                out.println("<th rowspan='2' align='center' valign='middle' scope='col'>RKM CV% </th> ");
                out.println("<th rowspan='2' align='center' valign='middle' scope='col'>ELONG</th> ");
                out.println("<th rowspan='2' align='center' valign='middle' scope='col'>VSF</th> ");
                out.println("<th rowspan='2' align='center' valign='middle' scope='col'>DVSF</th> ");
                out.println("<th rowspan='2' align='center' valign='middle' scope='col'>GV</th> ");
                out.println("<th rowspan='2' align='center' valign='middle' scope='col'>DGV</th> ");
                out.println("<th rowspan='2' align='center' valign='middle' scope='col'>POC</th> ");
                out.println("<th rowspan='2' align='center' valign='middle' scope='col'>DC COMB<br>AJSM </th> ");
                out.println("<th rowspan='2' align='center' valign='middle' scope='col'>CARD</th> ");
                out.println("<th rowspan='2' align='center' valign='middle' scope='col'>CBD</th> ");
                out.println("<th rowspan='2' align='center' valign='middle' scope='col'>DEP</th> ");
                out.println("<th colspan='2' align='center' valign='middle' scope='col'>COTTON</th> ");
                out.println("<th rowspan='2' align='center' valign='middle' scope='col'>L.NO</th> ");
                out.println("<th colspan='3' align='center' valign='middle' scope='col'>OTHERS</th> ");
                out.println("<th rowspan='2' align='center' valign='middle' scope='col'>TOTAL%</th> ");
            out.println("</tr> ");
            out.println("<tr> ");
                out.println("<th align='center' valign='middle' scope='col'>Cnt</th> ");
                out.println("<th align='center' valign='middle' scope='col'>Str</th> ");
                out.println("<th align='center' valign='middle' scope='col'>Csp</th> ");
                out.println("<th align='center' valign='middle' scope='col'>Cnt1</th> ");
                out.println("<th align='center' valign='middle' scope='col'>Cnt2</th> ");
                out.println("<th align='center' valign='middle' scope='col'>Cnt3</th> ");
                out.println("<th align='center' valign='middle' scope='col'>Cnt4</th> ");
                out.println("<th align='center' valign='middle' scope='col'>Cnt5</th> ");
                out.println("<th align='center' valign='middle' scope='col'>Str1</th> ");
                out.println("<th align='center' valign='middle' scope='col'>Str2</th> ");
                out.println("<th align='center' valign='middle' scope='col'>Str3</th> ");
                out.println("<th align='center' valign='middle' scope='col'>Str4</th> ");
                out.println("<th align='center' valign='middle' scope='col'>Str5</th> ");
                out.println("<th align='center' valign='middle' scope='col'>Csp1</th> ");
                out.println("<th align='center' valign='middle' scope='col'>Csp2</th> ");
                out.println("<th align='center' valign='middle' scope='col'>Csp3</th> ");
                out.println("<th align='center' valign='middle' scope='col'>Csp4</th> ");
                out.println("<th align='center' valign='middle' scope='col'>Csp5</th> ");
                out.println("<th align='center' valign='middle' scope='col'>CARD</th> ");
                out.println("<th align='center' valign='middle' scope='col'>CBD</th> ");
                out.println("<th align='center' valign='middle' scope='col'>B.COTTON</th> ");
                out.println("<th align='center' valign='middle' scope='col'>USABLE</th> ");
                out.println("<th align='center' valign='middle' scope='col'>WHITE<br> MODEL </th> ");
            out.println("</tr> ");
            
            for (int i = 0; i < theList.size(); i++) {
                theDataMap     =  (HashMap)theList.get(i);
                    double dcnt1 = common.toDouble((String)theDataMap.get("CNT1"));
                    double dcnt2 = common.toDouble((String)theDataMap.get("CNT2"));
                    double dcnt3 = common.toDouble((String)theDataMap.get("CNT3"));
                    double dcnt4 = common.toDouble((String)theDataMap.get("CNT4"));
                    double dcnt5 = common.toDouble((String)theDataMap.get("CNT5"));
                    
                    double dstren1 = common.toDouble((String)theDataMap.get("STR1"));
                    double dstren2 = common.toDouble((String)theDataMap.get("STR2"));
                    double dstren3 = common.toDouble((String)theDataMap.get("STR3"));
                    double dstren4 = common.toDouble((String)theDataMap.get("STR4"));
                    double dstren5 = common.toDouble((String)theDataMap.get("STR5"));
                    
                    double dcsp1 = dcnt1*dstren1;
                    double dcsp2 = dcnt2*dstren2;
                    double dcsp3 = dcnt3*dstren3;
                    double dcsp4 = dcnt4*dstren4;
                    double dcsp5 = dcnt5*dstren5;
                    
                    if (dcnt1 != 0) {                       
                        arrlistcnt.add((String)theDataMap.get("CNT1"));
                    }
                    if (dcnt2 != 0) {
                        arrlistcnt.add((String)theDataMap.get("CNT2"));
                    }
                    if (dcnt3 != 0) {
                        arrlistcnt.add((String)theDataMap.get("CNT3"));
                    }
                    if (dcnt4 != 0) {
                        arrlistcnt.add((String)theDataMap.get("CNT4"));
                    }
                    if (dcnt5 != 0) {
                        arrlistcnt.add((String)theDataMap.get("CNT5"));
                    }
                    String[] CntArray = (String[]) arrlistcnt.toArray(new String[arrlistcnt.size()]);
                    if (dstren1 != 0) {                       
                        arrliststr.add((String)theDataMap.get("STR1"));
                    }
                    if (dstren2 != 0) {
                        arrliststr.add((String)theDataMap.get("STR2"));
                    }
                    if (dstren3 != 0) {
                        arrliststr.add((String)theDataMap.get("STR3"));
                    }
                    if (dstren4 != 0) {
                        arrliststr.add((String)theDataMap.get("STR4"));
                    }
                    if (dstren5 != 0) {
                        arrliststr.add((String)theDataMap.get("STR5"));
                    }
                    String[] StrArray = (String[]) arrliststr.toArray(new String[arrliststr.size()]);
                out.println("<tr> ");
                out.println("<td>"+String.valueOf(i+1)+"</td> ");
                out.println("<td>"+common.parseDate((String)theDataMap.get("ENTRYDATE"))+"</td>");
                out.println("<td>"+common.parseNull((String)theDataMap.get("MACH_ST_NAME"))+"</td>");
                out.println("<td>"+common.parseNull((String)theDataMap.get("PARTYNAME"))+"</td>");
                out.println("<td>"+common.parseNull((String)theDataMap.get("ORDERNO"))+"</td>");
                out.println("<td>"+common.parseNull((String)theDataMap.get("YSHNM"))+"</td>");
                out.println("<td>"+common.parseNull((String)theDataMap.get("WEIGHT"))+"</td>");
                out.println("<td>&nbsp;</td> ");
                out.println("<td>&nbsp;</td> ");
                out.println("<td>&nbsp;</td> ");
                out.println("<td>&nbsp;</td> ");
                out.println("<td>&nbsp;</td> ");
                out.println("<td>&nbsp;</td> ");
                out.println("<td>"+common.parseNull((String)theDataMap.get("COUNTAVGFACT"))+"</td>");
                out.println("<td>"+common.parseNull((String)theDataMap.get("STRAVGFACT"))+"</td>");
                out.println("<td>"+common.parseNull((String)theDataMap.get("CSPAVGFACT"))+"</td>");
                out.println("<td>"+common.parseNull(common.getRound((String)theDataMap.get("CNT1"),2))+"</td>");
                out.println("<td>"+common.parseNull(common.getRound((String)theDataMap.get("CNT2"),2))+"</td>");
                out.println("<td>"+common.parseNull(common.getRound((String)theDataMap.get("CNT3"),2))+"</td>");
                out.println("<td>"+common.parseNull(common.getRound((String)theDataMap.get("CNT4"),2))+"</td>");
                out.println("<td>"+common.parseNull(common.removeNull(common.getRound((String)theDataMap.get("CNT5"),2)))+"</td>");
                out.println("<td>"+common.getRound(cev.getCoEffVant(CntArray), 2)+"</td> ");
                out.println("<td>"+common.parseNull(common.getRound((String)theDataMap.get("AVGCNT"),2))+"</td>");
                out.println("<td>"+common.parseNull(common.getRound((String)theDataMap.get("STR1"),2))+"</td>");
                out.println("<td>"+common.parseNull(common.getRound((String)theDataMap.get("STR2"),2))+"</td>");
                out.println("<td>"+common.parseNull(common.getRound((String)theDataMap.get("STR3"),2))+"</td>");
                out.println("<td>"+common.parseNull(common.getRound((String)theDataMap.get("STR4"),2))+"</td>");
                out.println("<td>"+common.parseNull(common.removeNull(common.getRound((String)theDataMap.get("STR5"),2)))+"</td>");
                out.println("<td>"+common.getRound(cev.getCoEffVant(StrArray), 2)+"</td> ");
                out.println("<td>"+common.parseNull(common.getRound((String)theDataMap.get("AVGSTR"),2))+"</td>");
                out.println("<td>"+common.getRound(dcsp1,2)+"</td> ");
                out.println("<td>"+common.getRound(dcsp2,2)+"</td> ");
                out.println("<td>"+common.getRound(dcsp3,2)+"</td> ");
                out.println("<td>"+common.getRound(dcsp4,2)+"</td> ");
                out.println("<td>"+common.removeNull(common.getRound(dcsp5,2))+"</td> ");
                out.println("<td>"+common.parseNull(common.getRound((String)theDataMap.get("AVGCSP"),2))+"</td>");
                out.println("</tr> ");
            }
    }
}