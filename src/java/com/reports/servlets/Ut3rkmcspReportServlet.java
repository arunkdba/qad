/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reports.servlets;

import com.communication.Communication;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.common.Common;
import com.reports.data.*;
import javax.servlet.http.HttpSession;

/**
 *
 * @author root
 */
public class Ut3rkmcspReportServlet extends HttpServlet {

    Common common = new Common();
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
           String SOrderNo = common.parseNull(request.getParameter("sorderno"));
            Communication       communication = new Communication();
             int      type   =  communication . IsCorrectionMixing(SOrderNo);
             int      iUnitCode= communication.getUnitCode(SOrderNo);

             //System.out.println("UnitCode11-->"+iUnitCode);

             session.removeAttribute("sorderno");
             session.removeAttribute("type");
             session.removeAttribute("Unit");

            session.setAttribute("sorderno", SOrderNo);
            session.setAttribute("type", type);
            session.setAttribute("Unit", iUnitCode);
            getData(request, response);
            request.getRequestDispatcher("ut3rkmcsp.jsp").forward(request, response);
        } finally {
            out.close();
        }
    }

    public void getData(HttpServletRequest request, HttpServletResponse response)
    {
        String SOrderNo = common.parseNull(request.getParameter("sorderno"));
        PrepcommUt3Data put3 = new PrepcommUt3Data();
        //java.util.List theList = put3.getUt3(SOrderNo);
        java.util.List theList = put3.getUt3newAvg(SOrderNo);
        java.util.List theut3List = put3.getUt3new(SOrderNo);
        java.util.List theut3Test = put3.getUt3Test(SOrderNo);

        PrepcommRkmData rkm = new PrepcommRkmData();
        java.util.List theRkm = rkm.getRkm(SOrderNo);

        PrepcommCspData Csp = new PrepcommCspData();
        java.util.List theCsp = Csp.getTestDetails(SOrderNo);

        PrepcommCspDataFresh fCsp = new PrepcommCspDataFresh();
        java.util.List listcsp = fCsp.getTestDetails(SOrderNo);

        PrepcommTwistTpiData tpi = new PrepcommTwistTpiData();
        java.util.List listtpi = tpi.getTestDetails(SOrderNo);

        PrepcommCutsData cuts = new PrepcommCutsData();
        java.util.List listbreak = cuts.getBreakData(SOrderNo);                

        request . setAttribute("theUt3List", theList);
        request . setAttribute("theRkmList", theRkm);
        request . setAttribute("theCspList",theCsp);
        request . setAttribute("theFCspList",listcsp);
        request . setAttribute("theTpiList",listtpi);
        request . setAttribute("theBreakList",listbreak);
        request . setAttribute("theListut3",theut3List);
        request . setAttribute("theut3Test",theut3Test);
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
