/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simplex.wrapping;

/**
 *
 * @author admin
 */
public class SimplexWrappingTestData {

    private String sSino;
    private String sTestno;
    private String sOrderNo;
    private String smach_name;    
    private String sShade;
    private String sStdHank;
    private String sHank1;
    private String sHank2;
    private String sHank3;
    private String sHank4;
    private String sHankAvg;
    private String sPumpColor;
    private String sBdWheel;
    private String sBreakDraft;
    private String sCpWheel;
    private String sTotalDraft;
    private String sTwistWheel;
    private String sTpi;
    private String sSpendleNo;
    private String sEntryDate;
    private String sStatus;
    
    public SimplexWrappingTestData() {
    }

    public String getsBdWheel() {
        return sBdWheel;
    }

    public void setsBdWheel(String sBdWheel) {
        this.sBdWheel = sBdWheel;
    }

    public String getsBreakDraft() {
        return sBreakDraft;
    }

    public void setsBreakDraft(String sBreakDraft) {
        this.sBreakDraft = sBreakDraft;
    }

    public String getsCpWheel() {
        return sCpWheel;
    }

    public void setsCpWheel(String sCpWheel) {
        this.sCpWheel = sCpWheel;
    }

    public String getsHank1() {
        return sHank1;
    }

    public void setsHank1(String sHank1) {
        this.sHank1 = sHank1;
    }

    public String getsHank2() {
        return sHank2;
    }

    public void setsHank2(String sHank2) {
        this.sHank2 = sHank2;
    }

    public String getsHank3() {
        return sHank3;
    }

    public void setsHank3(String sHank3) {
        this.sHank3 = sHank3;
    }

    public String getsHank4() {
        return sHank4;
    }

    public void setsHank4(String sHank4) {
        this.sHank4 = sHank4;
    }

    public String getsHankAvg() {
        return sHankAvg;
    }

    public void setsHankAvg(String sHankAvg) {
        this.sHankAvg = sHankAvg;
    }

    public String getsOrderNo() {
        return sOrderNo;
    }

    public void setsOrderNo(String sOrderNo) {
        this.sOrderNo = sOrderNo;
    }

    public String getsPumpColor() {
        return sPumpColor;
    }

    public void setsPumpColor(String sPumpColor) {
        this.sPumpColor = sPumpColor;
    }

    public String getsShade() {
        return sShade;
    }

    public void setsShade(String sShade) {
        this.sShade = sShade;
    }

    public String getsSino() {
        return sSino;
    }

    public void setsSino(String sSino) {
        this.sSino = sSino;
    }

    public String getsSpendleNo() {
        return sSpendleNo;
    }

    public void setsSpendleNo(String sSpendleNo) {
        this.sSpendleNo = sSpendleNo;
    }

    public String getsStdHank() {
        return sStdHank;
    }

    public void setsStdHank(String sStdHank) {
        this.sStdHank = sStdHank;
    }

    public String getsTestno() {
        return sTestno;
    }

    public void setsTestno(String sTestno) {
        this.sTestno = sTestno;
    }

    public String getsTotalDraft() {
        return sTotalDraft;
    }

    public void setsTotalDraft(String sTotalDraft) {
        this.sTotalDraft = sTotalDraft;
    }

    public String getsTwistWheel() {
        return sTwistWheel;
    }

    public void setsTwistWheel(String sTwistWheel) {
        this.sTwistWheel = sTwistWheel;
    }

    public String getSmach_name() {
        return smach_name;
    }

    public void setSmach_name(String smach_name) {
        this.smach_name = smach_name;
    }

    public String getsEntryDate() {
        return sEntryDate;
    }

    public void setsEntryDate(String sEntryDate) {
        this.sEntryDate = sEntryDate;
    }

    public String getsTpi() {
        return sTpi;
    }

    public void setsTpi(String sTpi) {
        this.sTpi = sTpi;
    }

    public String getsStatus() {
        return sStatus;
    }

    public void setsStatus(String sStatus) {
        this.sStatus = sStatus;
    }
}
