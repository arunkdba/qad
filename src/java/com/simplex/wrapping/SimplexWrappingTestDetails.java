/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simplex.wrapping;

import com.common.Common;
import com.jdbc.connection.JDBCProcessConnection;
import com.jdbc.connection.JDBCProcessaConnection;

/**
 *
 * @author admin
 */
public class SimplexWrappingTestDetails {

    java.sql.Connection theProcessConnection  = null;
    java.sql.Connection theProcessaConnection  = null;
    Common common;

    public SimplexWrappingTestDetails() {
        common = new Common();
    }

    public java.util.List getSimplexTestDetails(String SFromDate,String SToDate,String sOrderNo,int iUnitCode,int iShift){
        java.util.List theList = new java.util.ArrayList();
        StringBuffer sb = new StringBuffer();
        //System.out.println("iUnitCode->"+iUnitCode);
                     sb . append(" ");

                     if(iUnitCode == 1){
                         // A Unit
                        sb . append(" SELECT PRO_SIMPLEX_HANK_DETAILS.TEST_NO, MACHINE.MACH_NAME, PRO_SIMPLEX_HANK_DETAILS.RORDERNO, PROCESSINGOFINDENT.YSHNM ");
                        sb . append(" ,AUNIT_ORDERSIMPLEXSETTING.SIMPHANK||'-'||AUNIT_ORDERSIMPLEXSETTING.SIMPLEXHANKTO as SettingHank  ");
                        sb . append(" ,to_char(PRO_SIMPLEX_HANK_DETAILS.ENTRYDATE,'DD.MM.YYYY HH24:MI') as Time, PRO_SIMPLEX_HANK_DETAILS.SIMPLEXPUMB, PRO_SIMPLEX_HANK_DETAILS.BDWHEEL ");
                        sb . append(" ,AUNIT_ORDERSIMPLEXSETTING.SIMPBDRAFT ,PRO_SIMPLEX_HANK_DETAILS.CPWHEEL,PRO_SIMPLEX_HANK_DETAILS.DRAFT ,PRO_SIMPLEX_HANK_DETAILS.TWIST_WHEEL ");
                        sb . append(" ,AUNIT_ORDERSIMPLEXSETTING.SIMPTPI, PRO_SIMPLEX_HANK_DETAILS.SPINDLE,PRO_HANK_STATUS.STATUS FROM PROCESS.PRO_SIMPLEX_HANK_DETAILS ");
                        sb . append(" INNER JOIN MACHINE ON MACHINE.MACH_CODE = PRO_SIMPLEX_HANK_DETAILS.MACH_CODE AND MACHINE.UNIT_CODE=").append(iUnitCode) . append(" AND MACHINE.DEPT_CODE = 3 ");
                        sb . append(" INNER JOIN PROCESSINGOFINDENT ON PROCESSINGOFINDENT.RORDERNO = PRO_SIMPLEX_HANK_DETAILS.RORDERNO ");
                        sb . append(" INNER JOIN AUNIT_ORDERSIMPLEXSETTING ON AUNIT_ORDERSIMPLEXSETTING.RORDERNO = PRO_SIMPLEX_HANK_DETAILS.RORDERNO AND AUNIT_ORDERSIMPLEXSETTING.MIXNO=PRO_SIMPLEX_HANK_DETAILS.MIXNO ");
                        sb . append(" LEFT JOIN PROCESS.PRO_HANK_STATUS ON PRO_HANK_STATUS.CODE = PROCESS.PRO_SIMPLEX_HANK_DETAILS.TESTSTATUS ");
                        sb . append(" WHERE to_char(PRO_SIMPLEX_HANK_DETAILS.ENTRYDATE,'YYYYMMDD')>= ?  AND to_char(PRO_SIMPLEX_HANK_DETAILS.ENTRYDATE,'YYYYMMDD')<= ? ");
                        if(!sOrderNo.equals("")){
                            sb.append(" AND PROCESSINGOFINDENT.RORDERNO = '"+sOrderNo+"' ");
                        }
                        if(iShift !=0){
                            sb . append(" AND PRO_SIMPLEX_HANK_DETAILS.SHIFT ="+iShift+"");
                        }
                        sb . append(" ORDER BY 1,2 ");
                     }
                     if(iUnitCode == 2){
                         // B Unit
                        sb . append(" SELECT PRO_SIMPLEX_HANK_DETAILS.TEST_NO, MACHINE.MACH_NAME, PROCESSINGOFINDENT.RORDERNO, PROCESSINGOFINDENT.YSHNM ");
                        sb . append(" ,ORDERSIMPLEXSETTING.SIMPHANK||'-'||ORDERSIMPLEXSETTING.SIMPLEXHANKTO as SettingHank ");
                        sb . append(" ,to_char(PRO_SIMPLEX_HANK_DETAILS.ENTRYDATE,'DD.MM.YYYY HH24:MI') as Time, PRO_SIMPLEX_HANK_DETAILS.SIMPLEXPUMB, PRO_SIMPLEX_HANK_DETAILS.BDWHEEL ");
                        sb . append(" ,ORDERSIMPLEXSETTING.SIMPBDRAFT ,PRO_SIMPLEX_HANK_DETAILS.CPWHEEL,PRO_SIMPLEX_HANK_DETAILS.DRAFT ,PRO_SIMPLEX_HANK_DETAILS.TWIST_WHEEL, ");
                        sb . append(" ,ORDERSIMPLEXSETTING.SIMPTPI, PRO_SIMPLEX_HANK_DETAILS.SPINDLE,PRO_HANK_STATUS.STATUS FROM PROCESS.PRO_SIMPLEX_HANK_DETAILS ");
                        sb . append(" INNER JOIN MACHINE ON MACHINE.MACH_CODE = PRO_SIMPLEX_HANK_DETAILS.MACH_CODE AND MACHINE.UNIT_CODE=").append(iUnitCode) . append(" AND MACHINE.DEPT_CODE = 3 ");
                        sb . append(" INNER JOIN PROCESSINGOFINDENT ON PROCESSINGOFINDENT.RORDERNO = PRO_SIMPLEX_HANK_DETAILS.RORDERNO ");
                        sb . append(" INNER JOIN ORDERSIMPLEXSETTING ON ORDERSIMPLEXSETTING.RORDERNO = PRO_SIMPLEX_HANK_DETAILS.RORDERNO AND ORDERSIMPLEXSETTING.MIXNO=PRO_SIMPLEX_HANK_DETAILS.MIXNO ");
                        sb . append(" LEFT JOIN PRO_HANK_STATUS ON PRO_HANK_STATUS.CODE = PRO_SIMPLEX_HANK_DETAILS.TESTSTATUS ");
                        sb . append(" WHERE to_char(PRO_SIMPLEX_HANK_DETAILS.ENTRYDATE,'YYYYMMDD')>= ?  AND to_char(PRO_SIMPLEX_HANK_DETAILS.ENTRYDATE,'YYYYMMDD')<= ? ");
                        if(!sOrderNo.equals("")){
                            sb.append(" AND PROCESSINGOFINDENT.RORDERNO = '"+sOrderNo+"' ");
                        }
                        if(iShift !=0){
                            sb . append(" AND PRO_SIMPLEX_HANK_DETAILS.SHIFT ="+iShift+"");
                        }
                        sb . append(" ORDER BY 1,2 ");
                     }
                     
                     System.out.println("Qry-->"+sb.toString());
        try{
            java.sql.PreparedStatement pst = null;
            if(iUnitCode ==1){
                if(theProcessaConnection==null){
                        JDBCProcessaConnection  jdbc = JDBCProcessaConnection.getJDBCConnection();
                        theProcessaConnection = jdbc . getConnection();
                 }
                pst = theProcessaConnection.prepareStatement(sb.toString());
            }else{
                if(theProcessConnection==null){
                        JDBCProcessConnection  jdbc = JDBCProcessConnection.getJDBCConnection();
                        theProcessConnection = jdbc . getConnection();
                 }
                pst = theProcessConnection.prepareStatement(sb.toString());
            }
                                        pst . setString(1,SFromDate);
                                        pst . setString(2,SToDate);
             java.sql.ResultSet   rst = pst . executeQuery();
             SimplexWrappingTestData simplexWrappingTestData = null;
             int isino = 0;
             String[] SWeight = null;
             double dAvgHank = 0.0;
             while(rst.next()){
                 simplexWrappingTestData = new SimplexWrappingTestData();
                 simplexWrappingTestData . setsSino(String.valueOf(++isino));
                 simplexWrappingTestData . setsTestno(rst.getString(1));
                 simplexWrappingTestData . setSmach_name(rst.getString(2));
                 simplexWrappingTestData . setsOrderNo(rst.getString(3));
                 simplexWrappingTestData . setsShade(rst.getString(4));
                 simplexWrappingTestData . setsStdHank(common.parseNull(rst.getString(5)));

                 SWeight = getSliverWeightDetails(rst.getString(1));

                 simplexWrappingTestData . setsHank1(common.parseNull((String)SWeight[0]));
                 simplexWrappingTestData . setsHank2(common.parseNull((String)SWeight[1]));
                 simplexWrappingTestData . setsHank3(common.parseNull((String)SWeight[2]));
                 simplexWrappingTestData . setsHank4(common.parseNull((String)SWeight[3]));
                 dAvgHank = 0;
                 dAvgHank = (common.toDouble(SWeight[0])+common.toDouble(SWeight[1])+common.toDouble(SWeight[2])+common.toDouble(SWeight[3]))/4;
                 simplexWrappingTestData . setsHankAvg(common.parseNull(String.valueOf(common.getRound(dAvgHank,3))));

                 simplexWrappingTestData . setsEntryDate(rst.getString(6));
                 simplexWrappingTestData .setsPumpColor(rst.getString(7));
                 simplexWrappingTestData .setsBdWheel(rst.getString(8));
                 simplexWrappingTestData .setsBreakDraft(rst.getString(9));
                 simplexWrappingTestData .setsCpWheel(rst.getString(10));
                 simplexWrappingTestData .setsTotalDraft(rst.getString(11));
                 simplexWrappingTestData .setsTwistWheel(rst.getString(12));
                 simplexWrappingTestData .setsTpi(rst.getString(13));
                 simplexWrappingTestData .setsSpendleNo(rst.getString(14));
                 simplexWrappingTestData . setsStatus(common.parseNull(rst.getString(15)));

                 theList.add(simplexWrappingTestData);
             }
             rst.close();
             rst = null;
             pst.close();
             pst = null;

        }catch(Exception ex){
            ex.printStackTrace();
        }

        return theList;
    }

    private String[] getSliverWeightDetails(String STestNo){
        String[] SWeight = null;
        StringBuffer sb = new StringBuffer();
                     sb . append(" SELECT  WEIGHT FROM PROCESS.Simplex_Bobin_WEIGHT_DETAILS WHERE TEST_NO = ? ");
        try{
            if(theProcessConnection==null){
                    JDBCProcessConnection  jdbc = JDBCProcessConnection.getJDBCConnection();
                    theProcessConnection = jdbc . getConnection();
             }
             java.sql.PreparedStatement pst = theProcessConnection.prepareStatement(sb.toString());
                                        pst . setString(1,STestNo);
             java.sql.ResultSet   rst = pst . executeQuery();
                SWeight = new String[4];
                int icnt = 0;
             while(rst.next()){

                 if(icnt<4) {
                    SWeight[icnt] = getHank(rst.getString(1)); 
                    //SWeight[icnt] = rst.getString(1);
                 }
                 icnt++;
             }
             rst.close();
             rst = null;
             pst.close();
             pst = null;

        }catch(Exception ex){
            ex.printStackTrace();
        }
        return SWeight;
    }

    private String getHank(String SWeight){
        return common.getRound((8.1/common.toDouble(common.parseNull((String)SWeight))),3); //Hank Calculation
    }
    public String getUser (String SFromDate,String SToDate,String sOrderNo,int iUnitCode,int iShift){
        StringBuffer sb = new StringBuffer();
        String sUser="";
        java.sql.PreparedStatement pst = null;
                        sb . append(" SELECT wm_concat(DISTINCT QC_USER.USERNAME) as UserName FROM PRO_SIMPLEX_HANK_DETAILS ");
                        sb . append(" LEFT JOIN QC_USER ON QC_USER.USERCODE =  PRO_SIMPLEX_HANK_DETAILS.ENTRY_UCODE ");
                        sb . append(" WHERE to_char(PRO_SIMPLEX_HANK_DETAILS.ENTRYDATE,'YYYYMMDD')>= ?  AND to_char(PRO_SIMPLEX_HANK_DETAILS.ENTRYDATE,'YYYYMMDD')<= ? ");                        
                       if(iShift !=0){
                            sb . append(" AND PRO_SIMPLEX_HANK_DETAILS.SHIFT = "+iShift+"");
                            sb . append(" AND PRO_SIMPLEX_HANK_DETAILS.UNIT_CODE = "+iUnitCode+"");
                        }
                        try
                        {
                            if(theProcessConnection==null){
                            JDBCProcessConnection  jdbc = JDBCProcessConnection.getJDBCConnection();
                            theProcessConnection = jdbc . getConnection();
                            }
                            pst = theProcessConnection.prepareStatement(sb.toString());
                            pst . setString(1,SFromDate);
                                        pst . setString(2,SToDate);
                            java.sql.ResultSet   rst = pst . executeQuery();
                            while(rst.next()){
                                sUser = common.parseNull(rst.getString(1));
                            }
                            rst.close();
                            pst.close();
                            rst=null;
                            pst=null;
                        }
                        catch(Exception ex){
                            ex.printStackTrace();
                        }
        return sUser;
    }
}

//select count(*) from  PRO_CARDING_HANK_DETAILS where getcurrentshiftnew(to_char(PRO_CARDING_HANK_DETAILS.ENTRYDATE,'HH24'),to_char(PRO_CARDING_HANK_DETAILS.ENTRYDATE,'MI'))=1 and SHIFT=0
//update PRO_CARDING_HANK_DETAILS set SHIFT=3 where getcurrentshiftnew(to_char(PRO_CARDING_HANK_DETAILS.ENTRYDATE,'HH24'),to_char(PRO_CARDING_HANK_DETAILS.ENTRYDATE,'MI'))=3 and SHIFT=0