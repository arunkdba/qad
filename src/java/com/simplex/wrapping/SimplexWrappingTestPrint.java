/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simplex.wrapping;

import java.io.FileWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 *
 * @author admin
 */

public class SimplexWrappingTestPrint {
    
    FileWriter fw;
    String SUserName = "";
    
    int    iPage    = 1;
    String PrnTitle = "";
    String Title    = "";
    String TTitle   = "";
    String tEnd     = "";
    String tEnd1    = "";
    String tEnd2    = "";
    int    Tline    = 0;
    int    iTEnd    = 0;
    int    sino     = 0;
    
    int    laplength = 20;

    java.util.List theList;
    java.util.List theShadeList      = null;
    
    StringBuffer sb = new StringBuffer();
    
    com.common.Common      cm;
    
    String Head1 = "";
    String Head2 = "";
    String Head3 = "";
    String Head4 = "";
    String Head5 = "";
    String SUnit = "";
    String SShift = "";
    
    
    public SimplexWrappingTestPrint() {
        InitValues();
        
        theList           = new java.util.ArrayList();
        theShadeList      = new java.util.ArrayList();
        
        cm      = new com.common.Common();
        
        InitValues();
    }
    
    public void GeneratePrint(java.util.List theList,String SUserName,String SUnit,String SShift){
        try{
            this.theList   = theList;
            this.SUserName = SUserName;
            this.SUnit = SUnit;
            this.SShift=SShift;            
        
            createPrn();
            
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void createPrn() {

        try {
            String Sos = System.getProperty("os.name").toLowerCase();
            if(Sos.trim().startsWith("wind")){
                fw = new FileWriter("d:/simplexwrappingtest.prn");
            }else{
                fw = new FileWriter("//software/QcPrint/simplexwrappingtest.prn");
            }
            
            toPrintPrn();
            fw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void toPrintPrn() {

        PrnHead();
        PrnBody();
    }

    private void InitValues() {
        Title  = "";
        TTitle = "";
        Tline  = 0;
        iPage  = 1;
    }

    private void PrnHead() {

        try{
            Head1 = null;
            Head2 = null;
            Head3 = null;
            Head4 = null;
            Head5 = null;
            
            Head1 = "gEAMARJOTHI SPINNING MILLS Ltd" + "\n";
            Head2 = " "+PrnTitle+"\n";
            Head3 = "Dept         : QC \n";
            Head5 = "Document     : SIMPLEX HANK DETAILS ("+SUnit+") \n";
            Head5 += " Shift        : "+SShift+" \n";
            Head5 += " User         : "+SUserName+" \n";
            Head4 = "Page No      : " + iPage + "FgE        File generated Time:"+cm.getSerDateTime()+"\n";
            
            String tHead1 = "";
            String tHead2 = " -----------------------------------------------------------------------------------------------------------------------------------------------------------------------\n";
            String tHead3 = " |SI |M/ch| Order No   |        Shade       | Std. Hank  |Hank1|Hank2|Hank3|Hank4|  Avg  |  Pump color   | BD  | BD |  Cp   |Total |Twist|TPI |Spendle| Time  | Changes |\n";
            String tHead4 = " |No | No |            |                    |            |     |     |     |     |       |               |Wheel|    | Wheel |Draft |Wheel|    |  No   |       |         |\n";
            tHead4        +=" |---|----|------------|--------------------|------------|-----|-----|-----|-----|-------|---------------|-----|----|-------|------|-----|----|-------|-------|---------|\n";
                     tEnd = " |---|----|------------|--------------------|------------|-----|-----|-----|-----|-------|---------------|-----|----|-------|------|-----|----|-------|-------|---------|\n";
                    tEnd1 = " -----------------------------------------------------------------------------------------------------------------------------------------------------------------------\n";
                    tEnd2 = " -----------------------------------------------------------------------------------------------------------------------------------------------------------------------\n";

            Title  = "";
            TTitle = "";

            Title  = " " + Head1 + " " + Head2 + " " + Head3 + " " + Head5 + " " + Head4;
            TTitle = tHead1 + tHead2 + tHead3 + tHead4;

            fw . write(Title);
            fw . write(TTitle);

            Tline += 7;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void PrnBody() {

        try{

        if (theList.size() > 0) {

                String      Shade  = "";

                String laps      = "";
                String SData     = "";

                java.util.List shadelist = new java.util.ArrayList();

            for (int i = 0; i < theList.size(); i++) {
                sino++;
                if ((theList.size()-1) == i) {
                    iTEnd = 1;
                }

                SimplexWrappingTestData simplexWrappingTestData = (SimplexWrappingTestData)theList.get(i);

                 Shade         = cm.parseNull(simplexWrappingTestData.getsShade());

                 laps      = "";
                 SData     = "";

                 shadelist = new java.util.ArrayList();

                 shadelist =  getShadeLines(Shade, 20);

                int big = 1;
                int shadelistsize = 0;

                if(shadelist.size()>0){ shadelistsize = shadelist.size()-1; big = shadelist.size();}

                String shadename = "";

                for (int l = 0; l < big; l++) {

                    shadename = "";

                        if(shadelist.size()>0){
                            if(l<=shadelistsize){
                                shadename = cm.parseNull(shadelist.get(l).toString());
                            }
                        }
                        // 3,4,12,20,12,5,5,5,5,7,15,5,6,7,6,5,4,7,7,12
                    if (l == 0) {
                       SData =  " |" + cm.Cad(cm.parseNull(String.valueOf(simplexWrappingTestData.getsSino())), 3)+
                                "|" + cm.Cad(cm.parseNull(simplexWrappingTestData.getSmach_name()), 4)+
                                "|" + cm.Cad(cm.parseNull(simplexWrappingTestData.getsOrderNo()), 12)+
                                "|" + cm.Pad(shadename, 20)+
                                "|" + cm.Rad(cm.parseNull(simplexWrappingTestData.getsStdHank()),12)+
                                "|" + cm.Pad(cm.parseNull(simplexWrappingTestData.getsHank1()),5)+
                                "|" + cm.Pad(cm.parseNull(simplexWrappingTestData.getsHank2()),5)+
                                "|" + cm.Pad(cm.parseNull(simplexWrappingTestData.getsHank3()),5)+
                                "|" + cm.Pad(cm.parseNull(simplexWrappingTestData.getsHank4()),5)+
                                "|" + cm.Pad(cm.parseNull(simplexWrappingTestData.getsHankAvg()),7)+
                                "|" + cm.Pad(cm.parseNull(simplexWrappingTestData.getsPumpColor()),15)+
                                "|" + cm.Pad(cm.parseNull(simplexWrappingTestData.getsBdWheel()),5)+
                                "|" + cm.Pad(cm.parseNull(simplexWrappingTestData.getsBreakDraft()),4)+
                                "|" + cm.Pad(cm.parseNull(simplexWrappingTestData.getsCpWheel()),7)+
                                "|" + cm.Pad(cm.parseNull(simplexWrappingTestData.getsTotalDraft()),6)+
                                "|" + cm.Pad(cm.parseNull(simplexWrappingTestData.getsTwistWheel()),5)+
                                "|" + cm.Pad(cm.parseNull(simplexWrappingTestData.getsTpi()),4)+
                                "|" + cm.Pad(cm.parseNull(simplexWrappingTestData.getsSpendleNo()),7)+
                                "|" + cm.Pad(cm.parseNull(simplexWrappingTestData.getsEntryDate()),7)+
                                "|" + cm.Pad(cm.parseNull(simplexWrappingTestData.getsStatus()),9)+ "|\n";


                        laps += SData;
                    } else {
                        Tline++;
                        // 3,4,12,20,12,5,5,5,5,7,15,5,6,7,6,5,4,7,7,12
                       SData = " |" + cm.Cad("", 3) +
                                "|" + cm.Cad("", 4) +
                                "|" + cm.Cad("", 12) +
                                "|" + cm.Pad(shadename, 20)+
                                "|" + cm.Rad("",12) +
                                "|" + cm.Pad("", 5) +
                                "|" + cm.Pad("", 5) +
                                "|" + cm.Pad("", 5) +
                                "|" + cm.Pad("", 5) +
                                "|" + cm.Pad("", 7) +
                                "|" + cm.Pad("", 15)+
                                "|" + cm.Pad("", 5)+
                                "|" + cm.Pad("", 4)+
                                "|" + cm.Pad("", 7)+
                                "|" + cm.Pad("", 6)+
                                "|" + cm.Pad("", 5)+
                                "|" + cm.Pad("", 4)+
                                "|" + cm.Pad("", 7)+
                                "|" + cm.Pad("", 7)+
                                "|" + cm.Pad("", 9) + "|\n";

                        laps += SData;
                    }
                }
                PrnWriter(laps);
            }
        }
        
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void PrnWriter(String Data) {

        try {
            Tline = Tline + 2;

            if (Tline <= 64) {
                if (Tline >= 10) {
                    fw.write(tEnd);
                }
                fw.write(Data);

                if (iTEnd == 1 && Tline == 64) {

                    fw . write(tEnd1);
                    Tline = 0;
                } else if (iTEnd == 1 && Tline != 64) {
                    fw . write(tEnd1);
                    Tline = 0;
                }

            } else {

                fw.write(tEnd1);

                iPage = iPage + 1;
                Tline = 0;
                
                PrnHead();
                
                Tline = Tline + 2;
                if (Tline <= 64) {
                    if (Tline > 11) {
                        fw.write(tEnd);
                    }
                    fw.write(Data);

                    if (iTEnd == 1 && Tline == 64) {

                        fw.write(tEnd1);
                        Tline = 0;

                    } else if (iTEnd == 1 && Tline != 64) {

                        fw.write(tEnd1);
                        Tline = 0;
                    }
                }
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public java.util.List getShadeLines(String Shade, int iLength) {
        java.util.List theShadeList = new java.util.ArrayList();
        int iLen = 0;
        int iLen1 = 0;
        int iLen2 = 0;

        BigDecimal bd1, bd2;
        bd1 = new BigDecimal(Shade.length());
        bd2 = new BigDecimal(iLength);
        iLen = bd1.divide(bd2, RoundingMode.UP).intValue();

        iLen1 = 0;
        iLen2 = iLength;

        for (int i = 0; i < iLen; i++) {
            if (i == (iLen - 1)) {
                theShadeList.add(Shade.substring(iLen1));
            } else {
                theShadeList.add(Shade.substring(iLen1, iLen2));
                iLen1 = iLen2;
                iLen2 += iLength;
            }
        }
        return theShadeList;
    }
}

//73443