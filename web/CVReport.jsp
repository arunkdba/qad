<%-- 
    Document   : CVReport
    Created on : Agu 4, 2015, 02:00:23 AM
    Author     : admin
--%>


<%@page import="com.reports.data.CVReportData"%>
<%@page import="com.reports.print.CVReportPrint"%>
<%@page import="com.reports.pdf.CVReportPDF"%>
<%@page import="java.util.HashMap"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.reports.classes.MinAndMaxValue"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.reports.classes.CoEffVarient"%>
<%@page import="com.common.Common"%>
<!DOCTYPE html>
<%
    com.reports.data.Units unit = new com.reports.data.Units();
    java.util.List theList = unit.getUnit();
    request.setAttribute("theUnitList", theList);
    java.util.HashMap theMap =new java.util.HashMap();
    java.util.List Unitlist = new java.util.ArrayList();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="css/styles-menu.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.css" type="text/css">
        <script src="script/jquery-latest.min.js" type="text/javascript"></script>
        <script src="script/script-menu.js" type="text/javascript"></script>
        <script src="script/jquery-ui.js"></script>
        <script src="script/DateValidate.js" type="text/javascript"></script>
        <title>Machine Wise Wrapping Test</title>  
        <script type="text/javascript">
             function showAlert(){
                var msg = "PDF File Created in D:\CVReportPDF, PDF in A4 Sheet";
                alert(msg);
            }
        </script>
        
    </head>
    <body>
        <%!
            java.util.List Testlist = new java.util.ArrayList();
            Common common = new Common();
            String SprnBody = "",SprnBody2="";
            double dnominalCnt=0,dnominalStr=0;
            double dprocess =0;
            String SProcess="",SRh="";
        %>
        <%@include file="header.jsp" %>
        <div class="elegant-aero-demo">

            <form action="CVReport" method="post" class="elegant-aero">
                <h1>CV Report (CV% 4 and above)<span>Please fill the texts in the fields.</span></h1>
                <span id="error"></span>
                <p>
                    <label>
                        <span>Unit</span><select name="sunits" id="sunits">                            
                            <%
                                Unitlist = (java.util.List) request.getAttribute("theUnitList");
                                for(int i=0;i<Unitlist.size();i++) { 
                                    theMap     =  (HashMap)Unitlist.get(i);%>                                    
                            <option value="<%=(String)theMap.get("UNITCODE")%>"><%=(String)theMap.get("UNITNAME")%></option>
                            <% }%>
                        </select></label>
                    <!--<label><span>Machine </span><select name="machine" id="machine" >
                            <option selected="selected" value="1">Statex-I (B-Unit)</option>
                            <option value="2">Statex-II (C-Unit)</option>
                    </select></label>-->
                    <label><span>Test From </span><input id="tdatefrom" name="tdatefrom" placeholder="FromDate" type="text" class="sdate"></label>
                    <label><span>Test To </span><input id="tdateto" name="tdateto" placeholder="ToDate" type="text" class="sdate"></label>
                    <label>
                            <span>&nbsp;</span>
                            <input type="checkbox" id="ptype" name="ptype" value="43"/>OE
                        </label>
                    <label><span>&nbsp;</span><input class="button" value="Submit" type="submit" id="btnSubmit" name="btnSubmit" onClick="showAlert()">
                        <input class="button" value="Reset" type="reset" id="btnreset" name="btnreset">
                    </label>
                    <input type="hidden" name ="abst" id="abst" value="1"/>
                </p>
            </form>
        </div>
        <br><br>
        <%
            if (request.getAttribute("theTestDetailsList") != null) {
                CVReportPrint sprint = new CVReportPrint();
                CVReportPDF   spdf   = new CVReportPDF();   
                CVReportData Srh = new CVReportData();
        %>
        <h4 style="margin:0px;">From : <%=common.parseDate((String) request.getAttribute("Fdate"))%> To <%=common.parseDate((String) request.getAttribute("Todate"))%></h4>
        <h4 style="margin:0px;">Unit : <%=request.getAttribute("Unit")%></h4>
        <h4 style="margin:0px;">ProcessType : <%=request.getAttribute("Process")%></h4>
        <table width="100%" border="1" class="spinningwrapping">
            <tr>
                <th width="42" align="center" valign="top" scope="col">S.No</th>
                <th width="58" scope="col" align="center" valign="top">Date</th>
                <th width="58" scope="col" align="center" valign="top">O.No</th>
                <th width="65" scope="col" align="center" valign="top" >Shade</th>                
                <th width="41" scope="col" align="center" valign="top">Actual Count</th>
                <th width="113" scope="col" align="center" valign="top">Machine</th>
                <th width="40" align="center" valign="top" colspan="5">Count Reading</th>                
                <th width="76" scope="col" align="center" valign="top">Cnt Avg </th>
                <th width="68" scope="col" align="center" valign="top">Cnt CV%</th>
            </tr>
            <tr>
                <th width="42" align="center" valign="top" scope="col">&nbsp;</th>
                <th width="58" scope="col" align="center" valign="top">&nbsp;</th>
                <th width="58" scope="col" align="center" valign="top">&nbsp;</th>
                <th width="65" scope="col" align="center" valign="top">&nbsp;</th>
                <th width="41" scope="col" align="center" valign="top">&nbsp;</th>
                <th width="113" scope="col" align="center" valign="top">&nbsp;</th>
                <th width="82" scope="col" align="center" valign="top">1</th>
                <th width="96" scope="col" align="center" valign="top">2</th>
                <th width="92" scope="col" align="center" valign="top">3</th>
                <th width="89" scope="col" align="center" valign="top">4</th>
                <th width="89" scope="col" align="center" valign="top">5</th>
                <th width="76" scope="col" align="center" valign="top">&nbsp;</th>
                <th width="68" scope="col" align="center" valign="top">&nbsp;</th>
            </tr>
            <%
                Testlist = (java.util.List) request.getAttribute("theTestDetailsList");
                sprint.createPrn();
                sprint.setTestBase((String) request.getAttribute("Fdate"),(String) request.getAttribute("Todate"),(String)request.getAttribute("Unit"),(String)request.getAttribute("Process"));
                sprint.setHead((String) request.getAttribute("Fdate"));
                CoEffVarient cev = new CoEffVarient();
                MinAndMaxValue mval = new MinAndMaxValue();
                Srh.getRhFactors();
                
                spdf.createPDFFile();
                spdf.setTestBase((String) request.getAttribute("Fdate"),(String) request.getAttribute("Todate"),(String)request.getAttribute("Unit"),(String)request.getAttribute("Process"));
                spdf.setHead();
                spdf.printPDFData(Testlist);

                double dtotcount =0;
                double dtotstr = 0;
                double dtotcsp = 0;
                double dcv=0;
                int incount =0;
                int instr =0;
                int incsp=0;
                int icount=1;
                String sCountVar="";
                String sStar = "";
                java.util.List arrTotCntList = new ArrayList();
                java.util.List arrTotStrList = new ArrayList();
                java.util.List arrTotCspList = new ArrayList();
                
                java.util.List<Double> arrTotCnt= new ArrayList<Double>();
                java.util.List<Double> arrTotStr= new ArrayList<Double>();
                java.util.List<Double> arrTotCsp= new ArrayList<Double>();
                
                for (int i = 0; i < Testlist.size(); i++) {
                    com.reports.classes.SpinningWrappingTestDetails Td = (com.reports.classes.SpinningWrappingTestDetails) Testlist.get(i);
                    sCountVar = common.parseNull(Td.getCountvar());
                    int iCnt = 0;
                    int iStr = 0;
                    int iCsp = 0;
                    double drhStr=0,drhCnt=0;
                    double dcnt1 = common.toDouble(Td.getCnt1());
                    double dcnt2 = common.toDouble(Td.getCnt2());
                    double dcnt3 = common.toDouble(Td.getCnt3());
                    double dcnt4 = common.toDouble(Td.getCnt4());
                    double dcnt5 = common.toDouble(Td.getCnt5());
                    ArrayList<String> arrlistcnt = new ArrayList<String>();
                    if (dcnt1 != 0) {
                        iCnt += 1;
                        arrlistcnt.add(Td.getCnt1());
                        arrTotCntList.add(Td.getCnt1());
                        arrTotCnt.add(dcnt1);
                    }
                    if (dcnt2 != 0) {
                        iCnt += 1;
                        arrlistcnt.add(Td.getCnt2());
                        arrTotCntList.add(Td.getCnt2());
                        arrTotCnt.add(dcnt2);
                    }
                    if (dcnt3 != 0) {
                        iCnt += 1;
                        arrlistcnt.add(Td.getCnt3());
                        arrTotCntList.add(Td.getCnt3());
                        arrTotCnt.add(dcnt3);
                    }
                    if (dcnt4 != 0) {
                        iCnt += 1;
                        arrlistcnt.add(Td.getCnt4());
                        arrTotCntList.add(Td.getCnt4());
                        arrTotCnt.add(dcnt4);
                    }
                    if (dcnt5 != 0) {
                        iCnt += 1;
                        arrlistcnt.add(Td.getCnt5());
                        arrTotCntList.add(Td.getCnt5());
                        arrTotCnt.add(dcnt5);
                    }
                    double dCntAvg = (dcnt1 + dcnt2 + dcnt3 + dcnt4 + dcnt5) / iCnt;
                    dtotcount += (dcnt1 + dcnt2 + dcnt3 + dcnt4 + dcnt5);
                    incount += iCnt;
                    String[] CntArray = (String[]) arrlistcnt.toArray(new String[arrlistcnt.size()]);
                                        
                    double dsrten1 = common.toDouble(Td.getStrength1());
                    double dsrten2 = common.toDouble(Td.getStrength2());
                    double dsrten3 = common.toDouble(Td.getStrength3());
                    double dsrten4 = common.toDouble(Td.getStrength4());
                    double dsrten5 = common.toDouble(Td.getStrength5());
                    ArrayList<String> arrliststr = new ArrayList<String>();
                    if (dsrten1 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength1());
                        arrTotStrList.add(Td.getStrength1());
                        arrTotStr.add(dsrten1);
                    }
                    if (dsrten2 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength2());
                        arrTotStrList.add(Td.getStrength2());
                        arrTotStr.add(dsrten2);
                    }
                    if (dsrten3 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength3());
                        arrTotStrList.add(Td.getStrength3());
                        arrTotStr.add(dsrten3);
                    }
                    if (dsrten4 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength4());
                        arrTotStrList.add(Td.getStrength4());
                        arrTotStr.add(dsrten4);
                    }
                    if (dsrten5 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength5());
                        arrTotStrList.add(Td.getStrength5());
                        arrTotStr.add(dsrten5);
                    }
                    double dStrenAvg = (dsrten1 + dsrten2 + dsrten3 + dsrten4 + dsrten5) / iStr;
                    dtotstr += (dsrten1 + dsrten2 + dsrten3 + dsrten4 + dsrten5);
                    instr += iStr;
                    String[] StrArray = (String[]) arrliststr.toArray(new String[arrliststr.size()]);
                    
                    double dcsp1 = common.toDouble(Td.getCsp1());
                    double dcsp2 = common.toDouble(Td.getCsp2());
                    double dcsp3 = common.toDouble(Td.getCsp3());
                    double dcsp4 = common.toDouble(Td.getCsp4());
                    double dcsp5 = common.toDouble(Td.getCsp5());
                    ArrayList<String> arrlistcsp = new ArrayList<String>();
                    if (dcsp1 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp1());
                        arrTotCspList.add(Td.getCsp1());
                        arrTotCsp.add(dcsp1);
                    }
                    if (dcsp2 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp2());
                        arrTotCspList.add(Td.getCsp2());
                        arrTotCsp.add(dcsp2);
                    }
                    if (dcsp3 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp3());
                        arrTotCspList.add(Td.getCsp3());
                        arrTotCsp.add(dcsp3);
                    }
                    if (dcsp4 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp4());
                        arrTotCspList.add(Td.getCsp4());
                        arrTotCsp.add(dcsp4);
                    }
                    if (dcsp5 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp5());
                        arrTotCspList.add(Td.getCsp5());
                        arrTotCsp.add(dcsp5);
                    }
                    double dCspAvg = (dcsp1 + dcsp2 + dcsp3 + dcsp4 + dcsp5) / iCsp;
                    dtotcsp += (dcsp1 + dcsp2 + dcsp3 + dcsp4 + dcsp5);
                    incsp += iCsp;
                    String[] CspArray = (String[]) arrlistcsp.toArray(new String[arrlistcsp.size()]);
                    
                    if(dStrenAvg>0){
                    double dc1 = common.toDouble(common.getRound(dCntAvg,2));
                    double ds1 = common.toDouble(common.getRound(dStrenAvg,2));
                    double dc2 = (dprocess-1);
                    double dc1s1 = common.toDouble(common.getRound(dc1*ds1,2));
                    double dc2c1 = common.toDouble(common.getRound(13*(dc2-dc1),2));
                    double dupper = dc1s1-dc2c1;
                    drhStr = dupper/dc2;
                    
                   /* System.out.println("Str Rh");
                    System.out.println("Proceee Value-->"+dprocess);
                    System.out.println("C1 Value-->"+dc1);
                    System.out.println("C2-->"+dc2);
                    System.out.println("S1 Value -->"+ds1);
                    System.out.println("C1*S1 Value-->"+dc1s1);
                    System.out.println("13 * (c2-c1)-->"+dc2c1);
                    System.out.println("C1 S1 - 13 * (c2-c1)-->"+dupper);*/
                    }
                    if(dCntAvg>0){
                    double drh = Srh.getrh(SRh);
                    drhCnt = (dCntAvg*drh);}
                    
                   /* System.out.println("cnt Rh");
                    System.out.println("Rh Value-->"+drh);
                    System.out.println("Avg cnt -->"+dCntAvg);
                    System.out.println("dCntAvg* drh-->"+drhCnt);*/
                    if(sCountVar.equals("2")){
                       sStar = "*" ;
                    }
                    else{
                        sStar=" ";
                    }
                    dcv = common.toDouble(common.getRound(cev.getCoEffVant(CntArray), 2));                    
            %>
            <%if(dcv >=4.0){%>
            <tr>
                <td><%=icount%></td>
                <td><%=common.parseDate(Td.getTestdate())%></td>
                 <td><%=common.parseNull(Td.getOrderno())%></td>
                 <td><%=common.parseNull(Td.getShadename())%></td>
                 <td><%=common.parseNull(Td.getCount())%></td>
                <td><%=Td.getMechinename()%><!--<br><%=common.parseNull(Td.getRemarks())%>--></td>
                <td><%=Td.getCnt1()%></td>
                <td><%=Td.getCnt2()%></td>
                <td><%=Td.getCnt3()%></td>
                <td><%=Td.getCnt4()%></td>
                <td><%=common.removeNull(Td.getCnt5())%></td>
                <td><%=common.getRound(dCntAvg, 2)%></td>
                <td><%=common.getRound(cev.getCoEffVant(CntArray), 2)%></td>                
            </tr>
            <%
                SprnBody = common.Pad("| " + String.valueOf(icount), 5) + "|";
                SprnBody = SprnBody +common.Pad(common.parseDate(Td.getTestdate()), 10) + "|";
                SprnBody = SprnBody +common.Pad(Td.getOrderno(), 12) + "|";
                SprnBody = SprnBody + common.Pad(Td.getShadename(), 21) + "|";
                SprnBody = SprnBody + common.Pad(Td.getCount(), 6) + "|";
                SprnBody = SprnBody + common.Pad(Td.getMechinename(), 16) + "|";
                SprnBody = SprnBody + common.Rad(Td.getCnt1(), 7) + "|";
                SprnBody = SprnBody + common.Rad(Td.getCnt2(), 7) + "|";
                SprnBody = SprnBody + common.Rad(Td.getCnt3(), 7) + "|";
                SprnBody = SprnBody + common.Rad(Td.getCnt4(), 7) + "|";
                SprnBody = SprnBody + common.Rad(common.removeNull(Td.getCnt5()), 7) + "|";
                SprnBody = SprnBody + common.Rad(common.getRound(dCntAvg, 2), 6) + "|";
                SprnBody = SprnBody + common.Rad(common.getRound(cev.getCoEffVant(CntArray), 2), 6) + "|\n";                
                sprint.printData(SprnBody, (String) request.getAttribute("Fdate"),(String) request.getAttribute("Todate"),(String)request.getAttribute("Unit"),(String)request.getAttribute("Process"));
                icount = icount+1;                
                }
                }
                SprnBody2="";
            %>
        </table>
        <%
               sprint.Closefile();            
        }%>
    </body>
</html>