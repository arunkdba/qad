<%-- 
    Document   : ErrorMessageReport
    Created on : Jan 25, 2016, 11:30:31 AM
    Author     : root
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<jsp:useBean id="Common" class="com.common.Common"/>
<!DOCTYPE html>
<%
    java.util.HashMap theMap =new java.util.HashMap();
    
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="css/styles-menu.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.css" type="text/css"/>
        <script src="script/jquery-latest.min.js" type="text/javascript"></script>
        <script src="script/script-menu.js" type="text/javascript"></script>
        <script src="script/jquery-ui.js"></script>
        <script src="script/FormValidateUt3C.js" type="text/javascript"></script>
                
        <title>JSP Page</title>
    </head>
    <body>
        <%!
            java.util.List ErrorMessagelist = new java.util.ArrayList();
        %>
        <%@include file="header.jsp" %>
        <div class="elegant-aero-demo">

            <form action="ErrorMessageServlet" method="post" class="elegant-aero">
                <h1>Error Message Sent Report<span>Please fill the texts in the fields.</span></h1>
                <span id="error"></span>
                <p>
                    <label><span>As On Date</span><input id="tdatefrom" name="tdatefrom" placeholder="FromDate" type="text" class="sdate"></label>                    
                    <label><span>&nbsp;</span><input class="button" value="Submit" type="submit" id="btnSubmit" name="btnSubmit">
                        <input class="button" value="Reset" type="reset" id="btnreset" name="btnreset">
                    </label>
                    
                </p>
            </form> 
        </div>
        <br><br>
    <%if (request.getAttribute("theErrorMessageList") != null ) {
        ErrorMessagelist = (java.util.List) request.getAttribute("theErrorMessageList");
    %>                   
     <h2 style="color: #465AA1;">Error Message Details &nbsp;&nbsp;AS ON <%=Common.parseDate((String) request.getAttribute("Fdate"))%></h2>
     <table width="100%" border="1" class="ut3report">
            <tr>
                <th align="center" valign="top" scope="col">S.No </th>
                <th align="center" valign="top" scope="col">Message </th>
                <th align="center" valign="top" scope="col">Message Sent(Emp Name)</th>
                <th align="center" valign="top" scope="col">Sent DateTime</th>
            </tr>
            <%
                //sprint.createPrn();                
                //sprint.setHead((String) request.getAttribute("Fdate"));
                for (int i = 0; i < ErrorMessagelist.size(); i++) {
                    com.reports.classes.ErrorMessage Td = (com.reports.classes.ErrorMessage) ErrorMessagelist.get(i);
            %>
            <tr>
                <td align ='center'><%=i + 1%></td>
                <td><%=Common.parseNull(Td.getErrorMessage())%></td>
                <td><%=Common.parseNull(Td.getSentEmp())%></td>
                <td><%=Common.parseNull(Td.getSentDateTime())%></td>
             <% 
                }
            %>      
            
            </table>
            <%
                }
            %>
    </body>
</html>
