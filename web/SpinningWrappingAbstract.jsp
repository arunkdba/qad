<%--
    Document   : SpinningWrappingAbstract
    Created on : Dec 25, 2014, 4:45:23 PM
    Author     : admin
--%>


<%@page import="com.reports.print.SpinningWrapingAbstractPrint"%>
<%@page import="com.reports.data.SpinningWrappingTestData"%>
<%@page import="com.reports.classes.MinAndMaxValue"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.reports.classes.CoEffVarient"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.common.Common"%>

<!DOCTYPE html>

<html>
    <head>
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="css/styles-menu.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.css" type="text/css">
        <script src="script/jquery-latest.min.js" type="text/javascript"></script>
        <script src="script/script-menu.js" type="text/javascript"></script>
        <script src="script/jquery-ui.js"></script>
        <script src="script/FormValidate.js" type="text/javascript"></script>
        <title>Spinning Wrapping Test</title>
        <script>
            $(document).ready(function(){
                //$('tr:not(:has(td[rowspan])):even').addClass('oddrow');
            });
        </script>
    </head>
    <body>
        <%!
            java.util.List Testlist = new java.util.ArrayList();
            Common common = new Common();
            String SprnBody = "",SprnBody2="";
            double dnominalCnt=0,dnominalStr=0;
            double dprocess =0;
            String SProcess="",SRh="";
        %>
        <%@include file="header.jsp" %>
        <div class="elegant-aero-demo">

            <form action="SpinningWrapingTestReport" method="post" class="elegant-aero">
                <h1>Spinning Wrapping Test<span>Please fill the texts in the fields.</span></h1>
                <span id="error"></span>
                <p>
                    <label><span>Machine </span><select name="machine" id="machine" >
                            <option selected="selected" value="1">Statex-I (B-Unit)</option>
                            <option value="2">Statex-II (C-Unit)</option>
                    </select></label>
                    <label><span>Test From </span><input id="tdatefrom" name="tdatefrom" placeholder="FromDate" type="text" class="sdate"></label>
                    <label><span>Test To </span><input id="tdateto" name="tdateto" placeholder="ToDate" type="text" class="sdate"></label>
                    <label><span>Test No </span><select name="testno" id="testno" class="testno">
                           <!-- <option selected="selected">--Select Testno--</option>
                            <option value="All" >All</option>-->
                    </select></label>
                    <label><span>Order No</span><input id="orderno" name="orderno" placeholder="Valid Order No" type="text"></label>
                    <label><span>&nbsp;</span><input class="button" value="Submit" type="submit" id="btnSubmit" name="btnSubmit">
                        <input class="button" value="Reset" type="reset" id="btnreset" name="btnreset">
                    </label>
                    <input type="hidden" name ="abst" id="abst" value="1"/>
                </p>
            </form>
        </div>
        <br><br>
        <%
            if (request.getAttribute("theTestDetailsList") != null) {
                SpinningWrapingAbstractPrint sprint = new SpinningWrapingAbstractPrint();
                SpinningWrappingTestData Srh = new SpinningWrappingTestData();
                java.util.List TBase = new java.util.ArrayList();
                TBase = (java.util.List) request.getAttribute("theTestBase");
                dnominalCnt =   common.toDouble(common.parseNull((String) TBase.get(1)));
                dnominalStr =   common.toDouble(common.parseNull((String) TBase.get(4)));
                SProcess = common.parseNull((String) TBase.get(10));
                if(SProcess.endsWith("s") || SProcess.endsWith("S")){
                SProcess = SProcess.substring(0, SProcess.length()-1);
                }
                dprocess = common.toDouble(SProcess);
                SRh = common.parseNull((String) TBase.get(8));
        %>

        <table width="100%" border="1" class="spinningwrapping">
            <tr>
                <td width="183">Test No : <%=common.parseNull((String) TBase.get(0))%> </td>
                <td width="236">Nominal Count :<%=common.parseNull((String) TBase.get(1))%> Nec </td>
                <td width="209">WB Temp :<%=common.parseNull((String) TBase.get(2))%> </td>
            </tr>
            <tr>
                <td>Test Date :<%=common.parseDate((String) TBase.get(3))%></td>
                <td>Nominal Strength : <%=common.parseNull((String) TBase.get(4))%> lbs</td>
                <td>DB Temp : <%=common.parseNull((String) TBase.get(5))%></td>
            </tr>
            <tr>
                <td>Test Time : <%=common.parseNull((String) TBase.get(6))%></td>
                <td>Sample Length : <%=common.parseNull((String) TBase.get(7))%> Yards</td>
                <td>RH :  <%=common.parseNull((String) TBase.get(8))%> %</td>
            </tr>
            <tr>
                <td>Shift :  <%=common.parseNull((String) TBase.get(9))%></td>
                <td>Process :  <%=common.parseNull((String) TBase.get(10))%></td>
                <td>Operator : <%=common.parseNull((String) TBase.get(11))%></td>
            </tr>
        </table>
        <table width="100%" border="1" class="spinningwrapping">
            <tr>
                <th width="42" align="center" valign="top" scope="col">S.No</th>
                <th width="113" scope="col" align="center" valign="top">Machine</th>
                <th width="40" align="center" valign="top" scope="col">For</th>
                <th width="82" scope="col" align="center" valign="top">1</th>
                <th width="96" scope="col" align="center" valign="top">2</th>
                <th width="92" scope="col" align="center" valign="top">3</th>
                <th width="89" scope="col" align="center" valign="top">4</th>
                <th width="76" scope="col" align="center" valign="top">Avg Value </th>
                <th width="91" scope="col" align="center" valign="top">RHC Value </th>
                
                <th width="68" scope="col" align="center" valign="top">CV%</th>
                <th width="75" scope="col" align="center" valign="top">Change Advice </th>
                <th width="49" scope="col" align="center" valign="top">Cp</th>
                <th width="41" scope="col" align="center" valign="top">TPI</th>
                <th width="58" scope="col" align="center" valign="top">O.No</th>
                <th width="65" scope="col" align="center" valign="top" >Shade</th>
                <th width="41" scope="col" align="center" valign="top">Count</th>
                <th width="71" scope="col" align="center" valign="top">Pump / Cheese</th>
                <th width="63" scope="col" align="center" valign="top">Draft</th>
                <th width="63" scope="col" align="center" valign="top">B.D</th>
            </tr>
            <%
                Testlist = (java.util.List) request.getAttribute("theTestDetailsList");
                sprint.createPrn();
                sprint.setTestBase((String) request.getAttribute("Fdate"), (String) request.getAttribute("Tdate"), TBase);
                sprint.setHead((String) request.getAttribute("Fdate"), (String) request.getAttribute("Tdate"));
                CoEffVarient cev = new CoEffVarient();
                MinAndMaxValue mval = new MinAndMaxValue();
                Srh.getRhFactors();
                double dtotcount =0;
                double dtotstr = 0;
                double dtotcsp = 0;
                int incount =0;
                int instr =0;
                int incsp=0;
                
                java.util.List arrTotCntList = new ArrayList();
                java.util.List arrTotStrList = new ArrayList();
                java.util.List arrTotCspList = new ArrayList();
                
                java.util.List<Double> arrTotCnt= new ArrayList<Double>();
                java.util.List<Double> arrTotStr= new ArrayList<Double>();
                java.util.List<Double> arrTotCsp= new ArrayList<Double>();
                
                for (int i = 0; i < Testlist.size(); i++) {
                    com.reports.classes.SpinningWrappingTestDetails Td = (com.reports.classes.SpinningWrappingTestDetails) Testlist.get(i);
                    int iCnt = 0;
                    int iStr = 0;
                    int iCsp = 0;
                    double drhStr=0,drhCnt=0;
                    double dcnt1 = common.toDouble(Td.getCnt1());
                    double dcnt2 = common.toDouble(Td.getCnt2());
                    double dcnt3 = common.toDouble(Td.getCnt3());
                    double dcnt4 = common.toDouble(Td.getCnt4());
                    ArrayList<String> arrlistcnt = new ArrayList<String>();
                    if (dcnt1 != 0) {
                        iCnt += 1;
                        arrlistcnt.add(Td.getCnt1());
                        arrTotCntList.add(Td.getCnt1());
                        arrTotCnt.add(dcnt1);
                    }
                    if (dcnt2 != 0) {
                        iCnt += 1;
                        arrlistcnt.add(Td.getCnt2());
                        arrTotCntList.add(Td.getCnt2());
                        arrTotCnt.add(dcnt2);
                    }
                    if (dcnt3 != 0) {
                        iCnt += 1;
                        arrlistcnt.add(Td.getCnt3());
                        arrTotCntList.add(Td.getCnt3());
                        arrTotCnt.add(dcnt3);
                    }
                    if (dcnt4 != 0) {
                        iCnt += 1;
                        arrlistcnt.add(Td.getCnt4());
                        arrTotCntList.add(Td.getCnt4());
                        arrTotCnt.add(dcnt4);
                    }
                    double dCntAvg = (dcnt1 + dcnt2 + dcnt3 + dcnt4) / iCnt;
                    dtotcount += (dcnt1 + dcnt2 + dcnt3 + dcnt4);
                    incount += iCnt;
                    String[] CntArray = (String[]) arrlistcnt.toArray(new String[arrlistcnt.size()]);
                                        
                    double dsrten1 = common.toDouble(Td.getStrength1());
                    double dsrten2 = common.toDouble(Td.getStrength2());
                    double dsrten3 = common.toDouble(Td.getStrength3());
                    double dsrten4 = common.toDouble(Td.getStrength4());
                    ArrayList<String> arrliststr = new ArrayList<String>();
                    if (dsrten1 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength1());
                        arrTotStrList.add(Td.getStrength1());
                        arrTotStr.add(dsrten1);
                    }
                    if (dsrten2 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength2());
                        arrTotStrList.add(Td.getStrength2());
                        arrTotStr.add(dsrten2);
                    }
                    if (dsrten3 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength3());
                        arrTotStrList.add(Td.getStrength3());
                        arrTotStr.add(dsrten3);
                    }
                    if (dsrten4 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength4());
                        arrTotStrList.add(Td.getStrength4());
                        arrTotStr.add(dsrten4);
                    }
                    double dStrenAvg = (dsrten1 + dsrten2 + dsrten3 + dsrten4) / iStr;
                    dtotstr += (dsrten1 + dsrten2 + dsrten3 + dsrten4);
                    instr += iStr;
                    String[] StrArray = (String[]) arrliststr.toArray(new String[arrliststr.size()]);
                    
                    double dcsp1 = common.toDouble(Td.getCsp1());
                    double dcsp2 = common.toDouble(Td.getCsp2());
                    double dcsp3 = common.toDouble(Td.getCsp3());
                    double dcsp4 = common.toDouble(Td.getCsp4());
                    ArrayList<String> arrlistcsp = new ArrayList<String>();
                    if (dcsp1 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp1());
                        arrTotCspList.add(Td.getCsp1());
                        arrTotCsp.add(dcsp1);
                    }
                    if (dcsp2 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp2());
                        arrTotCspList.add(Td.getCsp2());
                        arrTotCsp.add(dcsp2);
                    }
                    if (dcsp3 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp3());
                        arrTotCspList.add(Td.getCsp3());
                        arrTotCsp.add(dcsp3);
                    }
                    if (dcsp4 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp4());
                        arrTotCspList.add(Td.getCsp4());
                        arrTotCsp.add(dcsp4);
                    }
                    double dCspAvg = (dcsp1 + dcsp2 + dcsp3 + dcsp4) / iCsp;
                    dtotcsp += (dcsp1 + dcsp2 + dcsp3 + dcsp4);
                    incsp += iCsp;
                    String[] CspArray = (String[]) arrlistcsp.toArray(new String[arrlistcsp.size()]);
                    
                    if(dStrenAvg>0){
                    double dc1 = common.toDouble(common.getRound(dCntAvg,2));
                    double ds1 = common.toDouble(common.getRound(dStrenAvg,2));
                    double dc2 = (dprocess-1);
                    double dc1s1 = common.toDouble(common.getRound(dc1*ds1,2));
                    double dc2c1 = common.toDouble(common.getRound(13*(dc2-dc1),2));
                    double dupper = dc1s1-dc2c1;
                    drhStr = dupper/dc2;
                    
                   /* System.out.println("Str Rh");
                    System.out.println("Proceee Value-->"+dprocess);
                    System.out.println("C1 Value-->"+dc1);
                    System.out.println("C2-->"+dc2);
                    System.out.println("S1 Value -->"+ds1);
                    System.out.println("C1*S1 Value-->"+dc1s1);
                    System.out.println("13 * (c2-c1)-->"+dc2c1);
                    System.out.println("C1 S1 - 13 * (c2-c1)-->"+dupper);*/
                    }
                    if(dCntAvg>0){
                    double drh = Srh.getrh(SRh);
                    drhCnt = (dCntAvg*drh);}
                    
                   /* System.out.println("cnt Rh");
                    System.out.println("Rh Value-->"+drh);
                    System.out.println("Avg cnt -->"+dCntAvg);
                    System.out.println("dCntAvg* drh-->"+drhCnt);*/
            %>
            <tr>
                <td><%=i + 1%></td>
                <td><%=Td.getMechinename()%><!--<br><%=common.parseNull(Td.getRemarks())%>--></td>
                <td>Cnt</td>
                <td><%=Td.getCnt1()%></td>
                <td><%=Td.getCnt2()%></td>
                <td><%=Td.getCnt3()%></td>
                <td><%=Td.getCnt4()%></td>
                <td><%=common.getRound(dCntAvg, 2)%></td>
                <td><%=common.getRound(drhCnt, 2)%></td>
                
                <td><%=common.getRound(cev.getCoEffVant(CntArray), 2)%></td>
                <td><%=common.parseNull(Td.getTesttype())%></td>
                <td><%=common.parseNull(Td.getCpwheel())%></td>
                <td><%=common.parseNull(Td.getTpi())%></td>
                <td><%=common.parseNull(Td.getOrderno())%></td>
                <td><%=common.parseNull(Td.getShadename())%></td>
                <td><%=common.parseNull(Td.getCount())%></td>
                <td><%=common.parseNull(Td.getPumpclr())%></td>
                <td><%=common.parseNull(Td.getDraft())%></td>
                <td><%=common.parseNull(Td.getBdraft())%></td>
            </tr>
            <%
                SprnBody = common.Pad("| " + String.valueOf(i + 1), 6) + "|";
                SprnBody = SprnBody + common.Pad(Td.getMechinename(), 18) + "|";
                SprnBody = SprnBody + common.Pad("Cnt", 4) + "|";
                SprnBody = SprnBody + common.Rad(Td.getCnt1(), 7) + "|";
                SprnBody = SprnBody + common.Rad(Td.getCnt2(), 7) + "|";
                SprnBody = SprnBody + common.Rad(Td.getCnt3(), 7) + "|";
                SprnBody = SprnBody + common.Rad(Td.getCnt4(), 7) + "|";
                SprnBody = SprnBody + common.Rad(common.getRound(dCntAvg, 2), 8) + "|";
                SprnBody = SprnBody + common.Rad(common.getRound(drhCnt, 2), 8) + "|";
                
                SprnBody = SprnBody + common.Rad(common.getRound(cev.getCoEffVant(CntArray), 2), 6) + "|";
                SprnBody = SprnBody + common.Rad(Td.getTesttype(), 10) + "|";
                SprnBody = SprnBody + common.Rad(Td.getCpwheel(), 8) + "|";
                SprnBody = SprnBody + common.Rad(Td.getTpi(), 7) + "|";
                SprnBody = SprnBody + common.Rad(Td.getOrderno(), 12) + "|";
                                
                SprnBody = SprnBody + common.Rad(Td.getShadename(), 23) + "|";
                SprnBody = SprnBody + common.Rad(Td.getCount(), 6) + "|";
                SprnBody = SprnBody + common.Rad(Td.getPumpclr(), 15) + "|";
                SprnBody = SprnBody + common.Rad(Td.getDraft(), 10) + "|";
                SprnBody = SprnBody + common.Rad(Td.getBdraft(), 10) + "|\n";
                     
                sprint.printData(SprnBody, (String) request.getAttribute("Fdate"), (String) request.getAttribute("Tdate"), TBase);
                }
                SprnBody2="";
            %>
        </table>
        <%
               sprint.Closefile();
            }%>
</body>
</html>