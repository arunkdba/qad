<%--
    Document   : SpinningWrappingTest
    Created on : Oct 29, 2014, 4:53:53 PM
    Author     : admin
--%>

<%@page import="com.reports.data.SpinningWrappingTestData"%>
<%@page import="com.reports.classes.MinAndMaxValue"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.reports.classes.CoEffVarient"%>
<%@page import="com.reports.print.SpinningWrapingTestPrint"%>
<%@page import="com.reports.pdf.SpinningWrapingTestPDF"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.common.Common"%>

<!DOCTYPE html>

<html>
    <head>
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="css/styles-menu.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.css" type="text/css">
        <script src="script/jquery-latest.min.js" type="text/javascript"></script>
        <script src="script/script-menu.js" type="text/javascript"></script>
        <script src="script/jquery-ui.js"></script>
        <script src="script/FormValidate.js" type="text/javascript"></script>
        <script type="text/javascript">
        <title>Spinning Wrapping Test</title>
        <script>
            $(document).ready(function(){
                //$('tr:not(:has(td[rowspan])):even').addClass('oddrow');
            });
        </script>
        
        <script type="text/javascript">
             function showAlert(){
                var msg = "PDF File Created in D:\SpinningWrappingTestPDF, PDF in Legal Sheet";
                alert(msg);
            }
        </script>
    </head>
    <body>
        <%!
            java.util.List Testlist = new java.util.ArrayList();
            Common common = new Common();
            String SprnBody = "",SprnBody2="";
            double dnominalCnt=0,dnominalStr=0;
            double dprocess =0;
            String SProcess="",SRh="";
        %>

        <%@include file="header.jsp" %>
        <div class="elegant-aero-demo">
            <form action="SpinningWrapingTestReport" method="post" class="elegant-aero">
                <h1>Spinning Wrapping Test<span>Please fill the texts in the fields.</span></h1>
                <span id="error"></span>
                <p>
                    <label><span>Machine </span><select name="machine" id="machine" >
                            <option selected="selected" value="1">Statex-I (B-Unit)</option>
                            <option value="2">Statex-II (C-Unit)</option>
                    </select></label>
                    <label><span>Test From </span><input id="tdatefrom" name="tdatefrom" placeholder="FromDate" type="text" class="sdate"></label>
                    <label><span>Test To </span><input id="tdateto" name="tdateto" placeholder="ToDate" type="text" class="sdate"></label>
                    <label><span>Test No </span><select name="testno" id="testno" class="testno">
                           <!-- <option selected="selected">--Select Testno--</option>
                            <option value="All" >All</option>-->
                    </select></label>
                    <label><span>Order No</span><input id="orderno" name="orderno" placeholder="Valid Order No" type="text"></label>
                    <label><span>&nbsp;</span><input class="button" value="Submit" type="submit" id="btnSubmit" name="btnSubmit" onClick="showAlert()">
                    <input class="button" value="Reset" type="reset" id="btnreset" name="btnreset">
                    </label
                    
                </p>
            </form>
        </div>
        <br><br>
        <%
               
               
            if (request.getAttribute("theTestDetailsList") != null) {
                SpinningWrapingTestPrint sprint = new SpinningWrapingTestPrint();
                SpinningWrapingTestPDF   spdf   = new SpinningWrapingTestPDF();
                SpinningWrappingTestData Srh    = new SpinningWrappingTestData();
                java.util.List TBase = new java.util.ArrayList();
                TBase = (java.util.List) request.getAttribute("theTestBase");
                dnominalCnt =   common.toDouble(common.parseNull((String) TBase.get(1)));
                dnominalStr =   common.toDouble(common.parseNull((String) TBase.get(4)));
                SProcess = common.parseNull((String) TBase.get(10));
                if(SProcess.endsWith("s") || SProcess.endsWith("S")){
                    SProcess = SProcess.substring(0, SProcess.length()-1);
                }
                dprocess = common.toDouble(SProcess);
                SRh = common.parseNull((String) TBase.get(8));
        %>

        <table width="100%" border="1" class="spinningwrapping">
            <tr>
                <td width="183">Test No : <%=common.parseNull((String) TBase.get(0))%> </td>
                <td width="236">Nominal Count :<%=common.parseNull((String) TBase.get(1))%> Nec </td>
                <td width="209">WB Temp :<%=common.parseNull((String) TBase.get(2))%> </td>
            </tr>
            <tr>
                <td>Test Date :<%=common.parseDate((String) TBase.get(3))%></td>
                <td>Nominal Strength : <%=common.parseNull((String) TBase.get(4))%> lbs</td>
                <td>DB Temp : <%=common.parseNull((String) TBase.get(5))%></td>
            </tr>
            <tr>
                <td>Test Time : <%=common.parseNull((String) TBase.get(6))%></td>
                <td>Sample Length : <%=common.parseNull((String) TBase.get(7))%> Yards</td>
                <td>RH :  <%=common.parseNull((String) TBase.get(8))%> %</td>
            </tr>
            <tr>
                <td>Shift :  <%=common.parseNull((String) TBase.get(9))%></td>
                <td>Process :  <%=common.parseNull((String) TBase.get(10))%></td>
                <td>Operator : <%=common.parseNull((String) TBase.get(11))%></td>
            </tr>
        </table>
        <table width="100%" border="1" class="spinningwrapping">
            <tr>
                <th width="42" align="center" valign="top" scope="col">S.No</th>
                <th width="113" scope="col" align="center" valign="top">Machine/<br>Change Adv</th>
                <th width="40" align="center" valign="top" scope="col">For</th>
                <th width="82" scope="col" align="center" valign="top">1</th>
                <th width="96" scope="col" align="center" valign="top">2</th>
                <th width="92" scope="col" align="center" valign="top">3</th>
                <th width="89" scope="col" align="center" valign="top">4</th>
                <th width="89" scope="col" align="center" valign="top">5</th>
                <th width="76" scope="col" align="center" valign="top">Avg Value </th>
                <th width="91" scope="col" align="center" valign="top">RHC Value </th>
                <th width="92" scope="col" align="center" valign="top">Cnt.Corr Strength </th>
                <th width="68" scope="col" align="center" valign="top">CV%</th>
                <th width="49" scope="col" align="center" valign="top">Cp</th>
                <th width="41" scope="col" align="center" valign="top">TPI</th>
                <th width="58" scope="col" align="center" valign="top">O.No</th>
                <th width="65" scope="col" align="center" valign="top" >Shade</th>
                <th width="41" scope="col" align="center" valign="top">Count</th>
                <th width="71" scope="col" align="center" valign="top">Pump / Cheese</th>
                <th width="63" scope="col" align="center" valign="top">Draft</th>
                <th width="63" scope="col" align="center" valign="top">B.D</th>
            </tr>
            <%
                Testlist = (java.util.List) request.getAttribute("theTestDetailsList");
                CoEffVarient cev = new CoEffVarient();
                MinAndMaxValue mval = new MinAndMaxValue();
                Srh.getRhFactors();
                double dtotcount =0;
                double dtotstr = 0;
                double dtotcsp = 0;
                int incount =0;
                int instr =0;
                int incsp=0;

                java.util.List arrTotCntList = new ArrayList();
                java.util.List arrTotStrList = new ArrayList();
                java.util.List arrTotCspList = new ArrayList();

                java.util.List<Double> arrTotCnt= new ArrayList<Double>();
                java.util.List<Double> arrTotStr= new ArrayList<Double>();
                java.util.List<Double> arrTotCsp= new ArrayList<Double>();

                for (int i = 0; i < Testlist.size(); i++) {
                    com.reports.classes.SpinningWrappingTestDetails Td = (com.reports.classes.SpinningWrappingTestDetails) Testlist.get(i);
                    int iCnt = 0;
                    int iStr = 0;
                    int iCsp = 0;
                    double drhStr=0,drhCnt=0;
                    double dcnt1 = common.toDouble(Td.getCnt1());
                    double dcnt2 = common.toDouble(Td.getCnt2());
                    double dcnt3 = common.toDouble(Td.getCnt3());
                    double dcnt4 = common.toDouble(Td.getCnt4());
                    double dcnt5 = common.toDouble(Td.getCnt5());
                    ArrayList<String> arrlistcnt = new ArrayList<String>();
                    if (dcnt1 != 0) {
                        iCnt += 1;
                        arrlistcnt.add(Td.getCnt1());
                        arrTotCntList.add(Td.getCnt1());
                        arrTotCnt.add(dcnt1);
                    }
                    if (dcnt2 != 0) {
                        iCnt += 1;
                        arrlistcnt.add(Td.getCnt2());
                        arrTotCntList.add(Td.getCnt2());
                        arrTotCnt.add(dcnt2);
                    }
                    if (dcnt3 != 0) {
                        iCnt += 1;
                        arrlistcnt.add(Td.getCnt3());
                        arrTotCntList.add(Td.getCnt3());
                        arrTotCnt.add(dcnt3);
                    }
                    if (dcnt4 != 0) {
                        iCnt += 1;
                        arrlistcnt.add(Td.getCnt4());
                        arrTotCntList.add(Td.getCnt4());
                        arrTotCnt.add(dcnt4);
                    }
                    if (dcnt5 != 0) {
                        iCnt += 1;
                        arrlistcnt.add(Td.getCnt5());
                        arrTotCntList.add(Td.getCnt5());
                        arrTotCnt.add(dcnt5);
                    }
                    double dCntAvg = (dcnt1 + dcnt2 + dcnt3 + dcnt4+dcnt5) / iCnt;
                    dtotcount += (dcnt1 + dcnt2 + dcnt3 + dcnt4+dcnt5);
                    incount += iCnt;
                    String[] CntArray = (String[]) arrlistcnt.toArray(new String[arrlistcnt.size()]);

                    double dsrten1 = common.toDouble(Td.getStrength1());
                    double dsrten2 = common.toDouble(Td.getStrength2());
                    double dsrten3 = common.toDouble(Td.getStrength3());
                    double dsrten4 = common.toDouble(Td.getStrength4());
                    double dsrten5 = common.toDouble(Td.getStrength5());
                    ArrayList<String> arrliststr = new ArrayList<String>();
                    if (dsrten1 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength1());
                        arrTotStrList.add(Td.getStrength1());
                        arrTotStr.add(dsrten1);
                    }
                    if (dsrten2 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength2());
                        arrTotStrList.add(Td.getStrength2());
                        arrTotStr.add(dsrten2);
                    }
                    if (dsrten3 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength3());
                        arrTotStrList.add(Td.getStrength3());
                        arrTotStr.add(dsrten3);
                    }
                    if (dsrten4 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength4());
                        arrTotStrList.add(Td.getStrength4());
                        arrTotStr.add(dsrten4);
                    }
                    if (dsrten5 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength5());
                        arrTotStrList.add(Td.getStrength5());
                        arrTotStr.add(dsrten5);
                    }
                    double dStrenAvg = (dsrten1 + dsrten2 + dsrten3 + dsrten4 + dsrten5) / iStr;
                    dtotstr += (dsrten1 + dsrten2 + dsrten3 + dsrten4 + dsrten5);
                    instr += iStr;
                    String[] StrArray = (String[]) arrliststr.toArray(new String[arrliststr.size()]);
                    
                    double dcsp1 = common.toDouble(Td.getCsp1());
                    double dcsp2 = common.toDouble(Td.getCsp2());
                    double dcsp3 = common.toDouble(Td.getCsp3());
                    double dcsp4 = common.toDouble(Td.getCsp4());
                    double dcsp5 = common.toDouble(Td.getCsp5());
                    ArrayList<String> arrlistcsp = new ArrayList<String>();
                    if (dcsp1 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp1());
                        arrTotCspList.add(Td.getCsp1());
                        arrTotCsp.add(dcsp1);
                    }
                    if (dcsp2 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp2());
                        arrTotCspList.add(Td.getCsp2());
                        arrTotCsp.add(dcsp2);
                    }
                    if (dcsp3 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp3());
                        arrTotCspList.add(Td.getCsp3());
                        arrTotCsp.add(dcsp3);
                    }
                    if (dcsp4 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp4());
                        arrTotCspList.add(Td.getCsp4());
                        arrTotCsp.add(dcsp4);
                    }
                    if (dcsp5 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp5());
                        arrTotCspList.add(Td.getCsp5());
                        arrTotCsp.add(dcsp5);
                    }
                    double dCspAvg = (dcsp1 + dcsp2 + dcsp3 + dcsp4 + dcsp5) / iCsp;
                    dtotcsp += (dcsp1 + dcsp2 + dcsp3 + dcsp4 + dcsp5);
                    incsp += iCsp;
                    String[] CspArray = (String[]) arrlistcsp.toArray(new String[arrlistcsp.size()]);
                    
                    if(dStrenAvg>0){
                    double dc1 = common.toDouble(common.getRound(dCntAvg,2));
                    double ds1 = common.toDouble(common.getRound(dStrenAvg,2));
                    double dc2 = (dprocess-1);
                    double dc1s1 = common.toDouble(common.getRound(dc1*ds1,2));
                    double dc2c1 = common.toDouble(common.getRound(13*(dc2-dc1),2));
                    double dupper = dc1s1-dc2c1;
                   
                    drhStr = dupper/dc2;
                    
//                    System.out.println("Str Rh");
//                    System.out.println("Proceee Value-->"+dprocess);
//                    System.out.println("C1 Value-->"+dc1);
//                    System.out.println("C2-->"+dc2);
//                    System.out.println("S1 Value -->"+ds1);
//                    System.out.println("C1*S1 Value-->"+dc1s1);
//                    System.out.println("13 * (c2-c1)-->"+dc2c1);
//                    System.out.println("C1 S1 - 13 * (c2-c1)-->"+dupper);
                    }
                    if(dCntAvg>0){
                    double drh = Srh.getrh(SRh);
                    drhCnt = (dCntAvg*drh);
                    }
                    //System.out.println("Rh Value-->"+drh);                    
                    //System.out.println("Avg cnt -->"+dCntAvg);
                    //System.out.println("Avg cnt -->"+dCntAvg);
                    //System.out.println("dCntAvg* drh-->"+drhCnt);
//                    System.out.println("drhCnt-->"+drhCnt);
//                    System.out.println("drhStr-->"+drhStr);                    
            %>
            <tr>
                <td rowspan="3"><%=i + 1%></td>
                <td rowspan="3"><%=Td.getMechinename()%><br><%=common.parseNull(Td.getTesttype())%><br><%=common.parseNull(Td.getRemarks())%></td>
                <td>Cnt</td>
                <td><%=Td.getCnt1()%></td>
                <td><%=Td.getCnt2()%></td>
                <td><%=Td.getCnt3()%></td>
                <td><%=Td.getCnt4()%></td>
                <td><%=Srh.removeNull(Td.getCnt5())%></td>
                <td><%=common.getRound(dCntAvg, 2)%></td>
                <td><%=common.getRound(drhCnt, 2)%></td>
                <td>&nbsp;</td>
                <td><%=common.getRound(cev.getCoEffVant(CntArray), 2)%></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            
            <tr>
                <td>Str</td>
                <td><%=Td.getStrength1()%></td>
                <td><%=Td.getStrength2()%></td>
                <td><%=Td.getStrength3()%></td>
                <td><%=Td.getStrength4()%></td>
                <td><%=Srh.removeNull(Td.getStrength5())%></td>
                <td><%=common.getRound(dStrenAvg, 2)%></td>
                <td><%=common.getRound(drhStr,2)%></td>
                <td>&nbsp;</td>
                <td><%=common.getRound(cev.getCoEffVant(StrArray), 2)%></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            
            <tr>
                <td>CSP</td>
                <td><%=Td.getCsp1()%></td>
                <td><%=Td.getCsp2()%></td>
                <td><%=Td.getCsp3()%></td>
                <td><%=Td.getCsp4()%></td>
                <td><%=Srh.removeNull(Td.getCsp5())%></td>
                <td><%=common.getRound(dCspAvg, 0)%></td>
                <td><%=common.parseNull(common.getRound((drhCnt*drhStr), 0))%></td>                
                <td>&nbsp;</td>
                <td><%=common.getRound(cev.getCoEffVant(CspArray), 2)%></td>
                <!--<td><%=common.parseNull(Td.getTesttype())%></td>-->
                <td><%=common.parseNull(Td.getCpwheel())%></td>
                <td><%=common.parseNull(Td.getTpi())%></td>
                <td><%=common.parseNull(Td.getOrderno())%></td>
                <td><%=common.parseNull(Td.getShadename())%></td>
                <td><%=common.parseNull(Td.getCount())%></td>
                <td><%=common.parseNull(Td.getPumpclr())%></td>
                <td><%=common.parseNull(Td.getDraft())%></td>
                <td><%=common.parseNull(Td.getBdraft())%></td>
            </tr>
               <%}%>
               <%
                Testlist = (java.util.List) request.getAttribute("theTestDetailsList");
                sprint.createPrn(request.getRemoteAddr().toString());
                sprint.setTestBase((String) request.getAttribute("Fdate"), (String) request.getAttribute("Tdate"), TBase);
                sprint.setHead((String) request.getAttribute("Fdate"), (String) request.getAttribute("Tdate"));
                
                spdf.createPDFFile();
                spdf.setTestBase((String) request.getAttribute("Fdate"), (String) request.getAttribute("Tdate"), TBase);
                spdf.setHead((String) request.getAttribute("Fdate"), (String) request.getAttribute("Tdate"));
                spdf.setPrintData((String) request.getAttribute("Fdate"), (String) request.getAttribute("Tdate"), Testlist);
                spdf.setStatisticalData();
               
                Srh.getRhFactors();
                
                dtotcount =0;
                dtotstr = 0;
                dtotcsp = 0;
                incount =0;
                instr =0;
                incsp=0;
                
                arrTotCntList = new ArrayList();
                arrTotStrList = new ArrayList();
                arrTotCspList = new ArrayList();
                
                arrTotCnt= new ArrayList<Double>();
                arrTotStr= new ArrayList<Double>();
                arrTotCsp= new ArrayList<Double>();
                
                java.util.List arrlistPump = new ArrayList();
                
                for (int j = 0; j < Testlist.size(); j++) {
                    com.reports.classes.SpinningWrappingTestDetails Td = (com.reports.classes.SpinningWrappingTestDetails) Testlist.get(j);
                    int iCnt = 0;
                    int iStr = 0;
                    int iCsp = 0;
                    double drhStr=0,drhCnt=0;
                    double dcnt1 = common.toDouble(Td.getCnt1());
                    double dcnt2 = common.toDouble(Td.getCnt2());
                    double dcnt3 = common.toDouble(Td.getCnt3());
                    double dcnt4 = common.toDouble(Td.getCnt4());
                    double dcnt5 = common.toDouble(Td.getCnt5());
                    ArrayList<String> arrlistcnt = new ArrayList<String>();
                    if (dcnt1 != 0) {
                        iCnt += 1;
                        arrlistcnt.add(Td.getCnt1());
                        arrTotCntList.add(Td.getCnt1());
                        arrTotCnt.add(dcnt1);
                    }
                    if (dcnt2 != 0) {
                        iCnt += 1;
                        arrlistcnt.add(Td.getCnt2());
                        arrTotCntList.add(Td.getCnt2());
                        arrTotCnt.add(dcnt2);
                    }
                    if (dcnt3 != 0) {
                        iCnt += 1;
                        arrlistcnt.add(Td.getCnt3());
                        arrTotCntList.add(Td.getCnt3());
                        arrTotCnt.add(dcnt3);
                    }
                    if (dcnt4 != 0) {
                        iCnt += 1;
                        arrlistcnt.add(Td.getCnt4());
                        arrTotCntList.add(Td.getCnt4());
                        arrTotCnt.add(dcnt4);
                    }
                    if (dcnt5 != 0) {
                        iCnt += 1;
                        arrlistcnt.add(Td.getCnt5());
                        arrTotCntList.add(Td.getCnt5());
                        arrTotCnt.add(dcnt5);
                    }
                    double dCntAvg = (dcnt1 + dcnt2 + dcnt3 + dcnt4 + dcnt5) / iCnt;
                    dtotcount += (dcnt1 + dcnt2 + dcnt3 + dcnt4 + dcnt5);                    
                    incount += iCnt;
                    String[] CntArray = (String[]) arrlistcnt.toArray(new String[arrlistcnt.size()]);

                    double dsrten1 = common.toDouble(Td.getStrength1());
                    double dsrten2 = common.toDouble(Td.getStrength2());
                    double dsrten3 = common.toDouble(Td.getStrength3());
                    double dsrten4 = common.toDouble(Td.getStrength4());
                    double dsrten5 = common.toDouble(Td.getStrength5());
                    ArrayList<String> arrliststr = new ArrayList<String>();
                    if (dsrten1 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength1());
                        arrTotStrList.add(Td.getStrength1());
                        arrTotStr.add(dsrten1);
                    }
                    if (dsrten2 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength2());
                        arrTotStrList.add(Td.getStrength2());
                        arrTotStr.add(dsrten2);
                    }
                    if (dsrten3 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength3());
                        arrTotStrList.add(Td.getStrength3());
                        arrTotStr.add(dsrten3);
                    }
                    if (dsrten4 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength4());
                        arrTotStrList.add(Td.getStrength4());
                        arrTotStr.add(dsrten4);
                    }
                    if (dsrten5 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength5());
                        arrTotStrList.add(Td.getStrength5());
                        arrTotStr.add(dsrten5);
                    }
                    double dStrenAvg = (dsrten1 + dsrten2 + dsrten3 + dsrten4 + dsrten5) / iStr;
                    dtotstr += (dsrten1 + dsrten2 + dsrten3 + dsrten4 + dsrten5);
                    instr += iStr;
                    String[] StrArray = (String[]) arrliststr.toArray(new String[arrliststr.size()]);
                    
                    double dcsp1 = common.toDouble(Td.getCsp1());
                    double dcsp2 = common.toDouble(Td.getCsp2());
                    double dcsp3 = common.toDouble(Td.getCsp3());
                    double dcsp4 = common.toDouble(Td.getCsp4());
                    double dcsp5 = common.toDouble(Td.getCsp5());
                    ArrayList<String> arrlistcsp = new ArrayList<String>();
                    if (dcsp1 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp1());
                        arrTotCspList.add(Td.getCsp1());
                        arrTotCsp.add(dcsp1);
                    }
                    if (dcsp2 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp2());
                        arrTotCspList.add(Td.getCsp2());
                        arrTotCsp.add(dcsp2);
                    }
                    if (dcsp3 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp3());
                        arrTotCspList.add(Td.getCsp3());
                        arrTotCsp.add(dcsp3);
                    }
                    if (dcsp4 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp4());
                        arrTotCspList.add(Td.getCsp4());
                        arrTotCsp.add(dcsp4);
                    }
                    if (dcsp5 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp5());
                        arrTotCspList.add(Td.getCsp5());
                        arrTotCsp.add(dcsp5);
                    }
                    double dCspAvg = (dcsp1 + dcsp2 + dcsp3 + dcsp4 + dcsp5) / iCsp;
                    dtotcsp += (dcsp1 + dcsp2 + dcsp3 + dcsp4 + dcsp5);
                    incsp += iCsp;
                    String[] CspArray = (String[]) arrlistcsp.toArray(new String[arrlistcsp.size()]);
                    
                    if(dStrenAvg>0){
                    double dc1 = common.toDouble(common.getRound(dCntAvg,2));
                    double ds1 = common.toDouble(common.getRound(dStrenAvg,2));
                    double dc2 = (dprocess-1);
                    double dc1s1 = common.toDouble(common.getRound(dc1*ds1,2));
                    double dc2c1 = common.toDouble(common.getRound(13*(dc2-dc1),2));
                    double dupper = dc1s1-dc2c1;
                    drhStr = dupper/dc2;
                    }
                    if(dCntAvg>0){
                    double drh = Srh.getrh(SRh);
                    drhCnt = (dCntAvg*drh);}
                    //System.out.println("Pump clr->"+Td.getPumpclr());
                    
                String[] Pumps = Td.getPumpclr().split(",");
                //arrlistPump = sprint.getCanLines(Pumps, 18);
                String SPumb = "";
                /*if(arrlistPump.size()>0){
                    SPumb  = arrlistPump.get(0).toString();
                    arrlistPump.remove(0);
                }*/
                //System.out.println("Pumps LEngth"+Pumps.length);

                SprnBody = common.Pad("| " + String.valueOf(j + 1), 5) + "|";
                SprnBody = SprnBody + common.Pad(Td.getMechinename(), 24) + "|";
                SprnBody = SprnBody + common.Pad("Cnt", 4) + "|";
                SprnBody = SprnBody + common.Rad(Td.getCnt1(), 7) + "|";
                SprnBody = SprnBody + common.Rad(Td.getCnt2(), 7) + "|";
                SprnBody = SprnBody + common.Rad(Td.getCnt3(), 7) + "|";
                SprnBody = SprnBody + common.Rad(Td.getCnt4(), 7) + "|";
                SprnBody = SprnBody + common.Rad(Srh.removeNull(Td.getCnt5()), 7) + "|";
                SprnBody = SprnBody + common.Rad(common.getRound(dCntAvg, 2), 10) + "|";
                SprnBody = SprnBody + common.Rad(common.getRound(drhCnt, 2), 10) + "|";
                SprnBody = SprnBody + common.Rad("", 8) + "|";
                SprnBody = SprnBody + common.Rad(common.getRound(cev.getCoEffVant(CntArray), 2), 7) + "|";
                SprnBody = SprnBody + common.Rad("", 10) + "|";
                SprnBody = SprnBody + common.Rad("", 7) + "|";
                SprnBody = SprnBody + common.Rad(Td.getOrderno(), 21) + "|";

                
                SprnBody = SprnBody + common.Rad(Pumps[0], 21) + "|";
                SprnBody = SprnBody + common.Rad(Td.getDraft(), 10) + "|";
                SprnBody = SprnBody + common.Rad(Td.getBdraft(), 10) + "|\n";

                SprnBody = SprnBody + common.Pad("|", 5) + "|";
                SprnBody = SprnBody + common.Pad(Td.getTesttype(), 24) + "|";
                SprnBody = SprnBody + common.Pad("Str", 4) + "|";
                SprnBody = SprnBody + common.Rad(Td.getStrength1(), 7) + "|";
                SprnBody = SprnBody + common.Rad(Td.getStrength2(), 7) + "|";
                SprnBody = SprnBody + common.Rad(Td.getStrength3(), 7) + "|";
                SprnBody = SprnBody + common.Rad(Td.getStrength4(), 7) + "|";
                SprnBody = SprnBody + common.Rad(Srh.removeNull(Td.getStrength5()), 7) + "|";
                SprnBody = SprnBody + common.Rad(common.getRound(dStrenAvg, 2), 10) + "|";
                SprnBody = SprnBody + common.Rad(common.getRound(drhStr,2), 10) + "|";
                SprnBody = SprnBody + common.Rad("", 8) + "|";
                SprnBody = SprnBody + common.Rad(common.getRound(cev.getCoEffVant(StrArray), 2), 7) + "|";
                SprnBody = SprnBody + common.Rad("", 10) + "|";
                SprnBody = SprnBody + common.Rad("", 7) + "|";
                SprnBody = SprnBody + common.Rad(Td.getShadename(), 21) + "|";

                if(Pumps.length >=2 && Pumps.length <=3){
                    SprnBody = SprnBody + common.Rad(Pumps[1], 21) + "|";
                }
                else{
                    SprnBody = SprnBody + common.Rad("", 21) + "|";
                }
                SprnBody = SprnBody + common.Rad("", 10) + "|";
                SprnBody = SprnBody + common.Rad("", 10) + "|\n";

                SprnBody = SprnBody + common.Pad("| ", 5) + "|";
                SprnBody = SprnBody + common.Pad(Td.getRemarks(), 24) + "|";
                SprnBody = SprnBody + common.Pad("CSP", 4) + "|";
                SprnBody = SprnBody + common.Rad(Td.getCsp1(), 7) + "|";
                SprnBody = SprnBody + common.Rad(Td.getCsp2(), 7) + "|";
                SprnBody = SprnBody + common.Rad(Td.getCsp3(), 7) + "|";
                SprnBody = SprnBody + common.Rad(Td.getCsp4(), 7) + "|";
                SprnBody = SprnBody + common.Rad(Srh.removeNull(Td.getCsp5()), 7) + "|";
                SprnBody = SprnBody + common.Rad(common.getRound(dCspAvg, 0), 10) + "|";
                SprnBody = SprnBody + common.Rad(common.getRound((drhCnt*drhStr), 0), 10) + "|";
                SprnBody = SprnBody + common.Rad("", 8) + "|";
                SprnBody = SprnBody + common.Rad(common.getRound(cev.getCoEffVant(CspArray), 2), 7) + "|";
                SprnBody = SprnBody + common.Rad(Td.getCpwheel(), 10) + "|";
                SprnBody = SprnBody + common.Rad(Td.getTpi(), 7) + "|";
                SprnBody = SprnBody + common.Rad(Td.getCount(), 21) + "|";

                if(Pumps.length >2 && Pumps.length <=3){
                    SprnBody = SprnBody + common.Rad(Pumps[1], 21) + "|";
                }
                else{
                    SprnBody = SprnBody + common.Rad("", 21) + "|";
                }
                SprnBody = SprnBody + common.Rad("", 10) + "|";
                SprnBody = SprnBody + common.Rad("", 10) + "|\n";

                if(Pumps.length >3){
                    for(int k=0;k<Pumps.length-3;k++){
                    SprnBody = common.Pad("| " + "", 5) + "|";
                    SprnBody = SprnBody + common.Pad("", 24) + "|";
                    SprnBody = SprnBody + common.Pad("", 4) + "|";
                    SprnBody = SprnBody + common.Rad("", 7) + "|";
                    SprnBody = SprnBody + common.Rad("", 7) + "|";
                    SprnBody = SprnBody + common.Rad("", 7) + "|";
                    SprnBody = SprnBody + common.Rad("", 7) + "|";
                    SprnBody = SprnBody + common.Rad("", 7) + "|";
                    SprnBody = SprnBody + common.Rad("", 2) + "|";
                    SprnBody = SprnBody + common.Rad("", 2) + "|";
                    SprnBody = SprnBody + common.Rad("", 8) + "|";
                    SprnBody = SprnBody + common.Rad("", 7) + "|";
                    SprnBody = SprnBody + common.Rad("", 10) + "|";
                    SprnBody = SprnBody + common.Rad("", 7) + "|";
                    SprnBody = SprnBody + common.Rad("", 21) + "|";

                    SprnBody = SprnBody + common.Rad("", 21) + "|";
                    SprnBody = SprnBody + common.Rad("", 10) + "|";
                    SprnBody = SprnBody + common.Rad("", 10) + "|\n";
                    }
                }
                sprint.printData(SprnBody, (String) request.getAttribute("Fdate"), (String) request.getAttribute("Tdate"), TBase);
                }
                SprnBody2="";
            %>
        </table>
        
        <table width="100%" border="1" class="spinningwrapping">
                <tr>
                    <th colspan="9" scope="col"><h3>Statistical Report </h3></th>
                </tr>
                <tr>
                    <th width="11%" scope="col">&nbsp;</th>
                    <th width="14%" scope="col">Nom</th>
                    <th width="9%" scope="col">Avg</th>
                    <th width="9%" scope="col">Min</th>
                    <th width="10%" scope="col">Max</th>
                    <th width="14%" scope="col">Range</th>
                    <th width="12%" scope="col">CV%</th>
                    <th width="11%" scope="col">RHC</th>
                    <th width="10%" scope="col">Q95</th>
                </tr>
                <%
                    String[] cntTotArray = (String[]) arrTotCntList.toArray(new String[arrTotCntList.size()]);
                    String[] strTotArray = (String[]) arrTotStrList.toArray(new String[arrTotStrList.size()]);
                    String[] cspTotArray = (String[]) arrTotCspList.toArray(new String[arrTotCspList.size()]);

                    double cntTot[] = new double[arrTotCnt.size()];
                    double strTot[] = new double[arrTotStr.size()];
                    double cspTot[] = new double[arrTotCsp.size()];

                    for(int i = 0; i < strTot.length; i++){
                        strTot[i] = arrTotStr.get(i);
                    }
                    for(int i = 0; i < cspTot.length; i++){
                        cspTot[i] = arrTotCsp.get(i);
                    }
                    for(int i = 0; i < cntTot.length; i++){
                        cntTot[i] = arrTotCnt.get(i);
                    }
                    double drhStr = (common.toDouble(common.getRound((dtotcount/incount),2))*common.toDouble(common.getRound((dtotstr/instr),2)));
                    drhStr = drhStr-13 *((dprocess-1)-(common.toDouble(common.getRound((dtotcount/incount),2))));
                    drhStr = drhStr/(dprocess-1);

                    double drh = Srh.getrh(SRh);
                    double drhCnt = ((dtotcount/incount)*drh);
                %>
                <tr>
                    <td><strong>Count</strong></td>
                    <td align="right" valign="middle"><%=common.getRound(dnominalCnt,2)%></td>
                    <td align="right" valign="middle"><%=common.getRound((dtotcount/incount),2)%></td>
                    <td align="right" valign="middle"><%=common.getRound(mval.getMin(cntTot),2)%></td>
                    <td align="right" valign="middle"><%=common.getRound(mval.getMax(cntTot),2)%></td>
                    <td align="right" valign="middle"><%=common.getRound(mval.getMax(cntTot)-mval.getMin(cntTot),2)%></td>
                    <td align="right" valign="middle"><%=common.getRound(cev.getCoEffVant(cntTotArray), 2)%></td>
                    <td align="right" valign="middle"><%=common.getRound(drhCnt,2)%></td>
                    <td align="right" valign="middle">&nbsp;</td>
                </tr>
                <%
                SprnBody2 = SprnBody2 + common.Pad("|Count ", 15) + "|";
                SprnBody2 = SprnBody2 + common.Cad(common.getRound(dnominalCnt,2), 20) + "|";
                SprnBody2 = SprnBody2 + common.Cad(common.getRound((dtotcount/incount),2), 23) + "|";
                SprnBody2 = SprnBody2 + common.Cad(common.getRound(mval.getMin(cntTot),2), 23) + "|";
                SprnBody2 = SprnBody2 + common.Cad(common.getRound(mval.getMax(cntTot),2), 23) + "|";
                SprnBody2 = SprnBody2 + common.Cad(common.getRound(mval.getMax(cntTot)-mval.getMin(cntTot),2), 23) + "|";
                SprnBody2 = SprnBody2 + common.Cad(common.getRound(cev.getCoEffVant(cntTotArray), 2), 23) + "|";
                SprnBody2 = SprnBody2 + common.Cad(common.getRound(drhCnt,2), 23) + "|";
                SprnBody2 = SprnBody2 + common.Cad("", 19) + "|\n";
                %>
                <tr>
                    <td><strong>Strength</strong></td>
                    <td align="right" valign="middle"><%=common.getRound(dnominalStr,2)%></td>
                    <td align="right" valign="middle"><%=common.getRound((dtotstr/instr),2)%></td>
                    <td align="right" valign="middle"><%=common.getRound(mval.getMin(strTot),2)%></td>
                    <td align="right" valign="middle"><%=common.getRound(mval.getMax(strTot),2)%></td>
                    <td align="right" valign="middle"><%=common.getRound(mval.getMax(strTot)-mval.getMin(strTot),2)%></td>
                    <td align="right" valign="middle"><%=common.getRound(cev.getCoEffVant(strTotArray), 2)%></td>
                    <td align="right" valign="middle"><%=common.getRound(drhStr,2)%></td>
                    <td align="right" valign="middle">&nbsp;</td>
                </tr>
                <%
                SprnBody2 = SprnBody2 + common.Pad("|Strength ", 15) + "|";
                SprnBody2 = SprnBody2 + common.Cad(common.getRound(dnominalStr,2), 20) + "|";
                SprnBody2 = SprnBody2 + common.Cad(common.getRound((dtotstr/instr),2), 23) + "|";
                SprnBody2 = SprnBody2 + common.Cad(common.getRound(mval.getMin(strTot),2), 23) + "|";
                SprnBody2 = SprnBody2 + common.Cad(common.getRound(mval.getMax(strTot),2), 23) + "|";
                SprnBody2 = SprnBody2 + common.Cad(common.getRound(mval.getMax(strTot)-mval.getMin(strTot),2), 23) + "|";
                SprnBody2 = SprnBody2 + common.Cad(common.getRound(cev.getCoEffVant(strTotArray), 2), 23) + "|";
                SprnBody2 = SprnBody2 + common.Cad(common.getRound(drhStr,2), 23) + "|";
                SprnBody2 = SprnBody2 + common.Cad("", 19) + "|\n";
                %>
                <tr>
                    <td><strong>Csp</strong></td>
                    <td align="right" valign="middle"><%=common.getRound((dnominalCnt*dnominalStr),0)%></td>
                    <td align="right" valign="middle"><%=common.getRound((dtotcsp/incsp),0)%></td>
                    <td align="right" valign="middle"><%=common.getRound(mval.getMin(cspTot),0)%></td>
                    <td align="right" valign="middle"><%=common.getRound(mval.getMax(cspTot),0)%></td>
                    <td align="right" valign="middle"><%=common.getRound(mval.getMax(cspTot)-mval.getMin(cspTot),0)%></td>
                    <td align="right" valign="middle"><%=common.getRound(cev.getCoEffVant(cspTotArray), 2)%></td>
                    <td align="right" valign="middle"><%=common.getRound((drhCnt*drhStr), 0)%></td>
                    <td align="right" valign="middle">&nbsp;</td>
                </tr>
                <%
                SprnBody2 = SprnBody2 + common.Pad("|Csp ", 15) + "|";
                SprnBody2 = SprnBody2 + common.Cad(common.getRound((dnominalCnt*dnominalStr),0), 20) + "|";
                SprnBody2 = SprnBody2 + common.Cad(common.getRound((dtotcsp/incsp),0), 23) + "|";
                SprnBody2 = SprnBody2 + common.Cad(common.getRound(mval.getMin(cspTot),0), 23) + "|";
                SprnBody2 = SprnBody2 + common.Cad(common.getRound(mval.getMax(cspTot),0), 23) + "|";
                SprnBody2 = SprnBody2 + common.Cad(common.getRound(mval.getMax(cspTot)-mval.getMin(cspTot),0), 23) + "|";
                SprnBody2 = SprnBody2 + common.Cad(common.getRound(cev.getCoEffVant(cspTotArray), 2), 23) + "|";
                SprnBody2 = SprnBody2 + common.Cad(common.getRound((drhCnt*drhStr), 0), 23) + "|";
                SprnBody2 = SprnBody2 + common.Cad("", 19) + "|";
                %>
        </table>
        <div class="roundcorner">
        <%
                // Different Calculation
                
               int iStdCdCount = 2;
               int iStdCdStrength = 4;
               double dNominalCnt = common.toDouble(common.getRound(dnominalCnt,2));
               double dActualCnt = common.toDouble(common.getRound((dtotcount/incount),2));
               double dNominalStr = common.toDouble(common.getRound(dnominalStr,2));
               double dActualStr = common.toDouble(common.getRound((dtotstr/instr),2));
               
               double X = common.toDouble(common.getRound(dNominalCnt-dActualCnt,2));  // For Count
               double X1 =common.toDouble(common.getRound((dNominalCnt+dActualCnt)/2,2)); // For Count
               
               double XX = common.toDouble(common.getRound(dNominalStr-dActualStr,2));  // For Strength
               double XX1 =common.toDouble(common.getRound((dNominalStr+dActualStr)/2,2)); // For Strength
               
               String sCalculatedCount = common.getRound((X/X1)*100,4);
               String sCalculatedStr = common.getRound((XX/XX1)*100,4);
               
               double dNormalCount = (Math.sqrt(40)/Math.sqrt(incount))*iStdCdCount;
               double dNormalStr = (Math.sqrt(40)/Math.sqrt(instr))*iStdCdStrength;
               
               //System.out.println("Nom Cnt-->"+dNominalCnt);
               //System.out.println("Act Cnt-->"+dActualCnt);
               
               //System.out.println("X-->"+X);
               //System.out.println("X1-->"+X1);
               
               //System.out.println("Nom Str-->"+dNominalStr);
               //System.out.println("Act Str-->"+dActualStr);
                              
               //System.out.println("XX-->"+XX);
               //System.out.println("XX1-->"+XX1);
               
               //System.out.println("Total Cnt-->"+incount);
               //System.out.println("Total Str-->"+instr);
               
               String sDiffCnt ="",sDiffStr="";
               double dcalcCnt = common.toDouble(sCalculatedCount);
               double dcalcStr = common.toDouble(sCalculatedCount);
               double dnormalCnt = common.toDouble(common.getRound(dNormalCount,4));
               double dnormalStr = common.toDouble(common.getRound(dNormalStr,4));
               if(dcalcCnt>dnormalCnt){
                   sDiffCnt = "Critically different";
               }
               else
               {
                   sDiffCnt = "No critical difference";
               }
               if(dcalcStr>dnormalStr){
                   sDiffStr = "Critically different";
               }
               else
               {
                   sDiffStr = "No critical difference";
               }
            %>
            <p>Count : <%=sDiffCnt%>(Calculated :<%=sCalculatedCount%>  Normal : <%=common.getRound(dNormalCount,4)%>) </p>
            <p>Strength : <%=sDiffStr%> (Calculated :<%=sCalculatedStr%>  Normal : <%=common.getRound(dNormalStr,4)%>)</p>
            <p>CSP : In units of Nec.lbs </p>
        </div>
            <%
                sprint.printStatistical(SprnBody2,sCalculatedCount,sCalculatedStr,dNormalCount,dNormalStr);
                sprint.Closefile();
                
                //How to get only Numbers from Alphanumeric String
                 
                /*
                System.out.println("abasdfasdf12wasdfasdf9_8asdfasdfzasdfasdfyx7".replaceAll("\\D", ""));
                String str="sdfvsdf68fsdfsf8999fsdf09";
                String numberOnly= str.replaceAll("[^0-9]", "");
                System.out.println("Num-->"+numberOnly);
                
                String s1 = " 8 sometext 7 3";
                String arr[] = s1.trim().split("[a-zA-Z ]+"); // Please note a space is there after Z

                int sum = 0;

                for (int i = 0; i < arr.length; i++)
                    sum += Integer.parseInt(arr[i]);
                System.out.println(sum);
                */
            }%>
           
</body> 