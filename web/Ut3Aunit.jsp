<%--
    Document   : Ut3Aunit
    Created on : Nov 6, 2014, 2:51:08 PM
    Author     : admin
--%>

<%@page import="com.reports.print.Ut3Aunit_OE_Print"%>
<%@page import="com.reports.print.Ut3Aunit_Print"%>
<%@page import="com.reports.pdf.UT3AunitPDF"%>
<%@page import="com.reports.classes.Ut3Details"%>
<%@page import="com.reports.classes.CoEffVarient"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="SitraStandards" class="com.reports.data.SitraStandardData"/>
<jsp:useBean id="Common" class="com.common.Common"/>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/ut3report.css" type="text/css">
        <link rel="stylesheet" href="css/styles-menu.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.css" type="text/css"/>
        <link rel="stylesheet" href="css/colorbox.css" type="text/css">
        <script src="script/jquery-latest.min.js" type="text/javascript"></script>
        <script src="script/script-menu.js" type="text/javascript"></script>
        <script src="script/jquery-ui.js"></script>
        <script src="script/FormValidateUt3C.js" type="text/javascript"></script>
        <script src="script/jquery.colorbox-min.js"></script>        
        <title>Ut3 A-unit</title>
        <script>
            $(document).ready(function(){
                $(".ajax").colorbox();
            });
        </script>
        
        <script type="text/javascript">
             function showAlert(){
                var msg = "PDF File Created in D:\UT3Aunit, PDF in A3 Sheet";
                alert(msg);
            }
        </script>
        
    </head>
    <body>
        <%!
            java.util.List TestUt3list = new java.util.ArrayList();
            String sProcess ="";
        %>
        <%@include file="header.jsp" %>
        <div class="elegant-aero-demo">

            <form action="Ut3Report" method="post" class="elegant-aero">
                <h1>A-Unit UT3 Report<span>Please fill the texts in the fields.</span></h1>
                <span id="error"></span>
                <p>
                    <label><span>Test From </span><input id="tdatefrom" name="tdatefrom" placeholder="FromDate" type="text" class="sdate"></label>
                    <label><span>Test To </span><input id="tdateto" name="tdateto" placeholder="ToDate" type="text" class="sdate"></label>
                    <label><span>Order No</span><input id="orderno" name="orderno" placeholder="Valid Order No" type="text"></label>
                    <label>
                            <span>&nbsp;</span>
                            <input type="checkbox" id="ptype" name="ptype" value="1"/>OE
                    </label>
                    <label><span>&nbsp;</span><input class="button" value="Submit" type="submit" id="btnSubmit" name="btnSubmit" onClick="showAlert()">
                        <input class="button" value="Reset" type="reset" id="btnreset" name="btnreset">
                    </label>
                </p>
                <input type="hidden" name="Unit" id="Unit" value="1"/>
            </form>
        </div>
        <br><br>
        <%if (request.getAttribute("theUt3List") != null) {
            TestUt3list = (java.util.List) request.getAttribute("theUt3List");
        %>
        <h2 class="heading">A-Unit UT3 Report From <%=Common.parseDate((String) request.getAttribute("Fdate"))%> To <%=Common.parseDate((String) request.getAttribute("Tdate"))%></h2>        
        <br><br>
        <%
            UT3AunitPDF    spdf         = new UT3AunitPDF();
            java.util.List thelist      = new java.util.ArrayList();
            java.util.HashMap theMap    = new java.util.HashMap();
            thelist                     = SitraStandards.getSitraStandards();
            sProcess                    = Common.parseNull((String) request.getAttribute("Process"));
            spdf . createPDFFile(sProcess);
            spdf . setSITRAStandard(String.valueOf(request.getAttribute("Fdate")),String.valueOf(request.getAttribute("Tdate")),thelist);
            
             if(sProcess.equals("1")){
                spdf . setUT3OEDataHead();
                spdf . setUT3OEData(TestUt3list);
             }
            else{
                spdf . setUT3DataHead();
                spdf . setUT3Data(TestUt3list);
            }
        %>
        <table width="525" border="1" align="center" class="standards">
            <tr>
                <th colspan="6" scope="col">SITRA STANDARD </th>
            </tr>
            <tr>
                <th scope="row">Count</th>
                <th scope="row">Good</th>
                <th scope="row">AVG</th>
                <th scope="row">POOR</th>
            </tr>
            <%for(int i=0;i<thelist.size();i++) {
            theMap     =  (HashMap)thelist.get(i);%>
            <tr align="center">
                <td><%=Common.parseNull((String)theMap.get("COUNT"))%></td>
                <td><%=Common.parseNull((String)theMap.get("GOOD"))%></td>
                <td><%=Common.parseNull((String)theMap.get("AVERAGE"))%></td>
                 <td><%=Common.parseNull((String)theMap.get("POOR"))%></td>
            </tr>
            <%}%>
        </table>
        <BR><BR>
        
        <table border="1" class="ut3report" id="utereporttable">
            <thead>
            <tr>
                <th rowspan="2" scope="col">O.NO</th>
                <th rowspan="2" scope="col">SHADE</th>
                <th rowspan="2" scope="col">PARTY NAME </th>
                <th rowspan="2" scope="col">O.QTY</th>
                <th rowspan="2" scope="col">DATE</th>
                <th rowspan="2" scope="col">RF NO </th>
                
                <th rowspan="2" scope="col">U%</th>
                <th rowspan="2" scope="col">CVM</th>
                <th rowspan="2" scope="col">THIN</th>
                <th rowspan="2" scope="col">THICK</th>
                <%if(sProcess.equals("1")){%>
                <th rowspan="2" scope="col">NEPS<br />(+200)</th>
                <th rowspan="2" scope="col">NEPS<br />(+280)</th>
                <th rowspan="2" scope="col">TOTAL<br />(+200)</th>
                <th rowspan="2" scope="col">TOTAL<br />(+280)</th>
                <%}else{%>
                <th rowspan="2" scope="col">NEPS</th>
                <th rowspan="2" scope="col">TOTAL</th>
                <%}%>
                <th colspan="3" align="center" valign="middle" scope="col">NO<br> RE </th>
                <th rowspan="2" align="center" valign="middle" scope="col">COUNT</th>
                <th rowspan="2" align="center" valign="middle" scope="col">COUNT CV% </th>
                <th rowspan="2" align="center" valign="middle" scope="col">STRENGTH</th>
                <th rowspan="2" align="center" valign="middle" scope="col">STRENGTH CV% </th>
                <th rowspan="2" align="center" valign="middle" scope="col">CSP</th>
                <th rowspan="2" align="center" valign="middle" scope="col">SYS</th>
                <th rowspan="2" align="center" valign="middle" scope="col">RKM</th>
                <th rowspan="2" align="center" valign="middle" scope="col">RKM CV% </th>
                <th rowspan="2" align="center" valign="middle" scope="col">ELONG</th>
                <th rowspan="2" align="center" valign="middle" scope="col">ELONG CV% </th>
                <th rowspan="2" align="center" valign="middle" scope="col">VSF</th>
                <th rowspan="2" align="center" valign="middle" scope="col">DVSF</th>
                <th rowspan="2" align="center" valign="middle" scope="col">GV</th>
                <th rowspan="2" align="center" valign="middle" scope="col">DGV</th>
                <%if(sProcess.equals("1")){%>
                <th colspan="3" align="center" valign="middle" scope="col">POC</th>
                <%}else{%>
                <th rowspan="2" align="center" valign="middle" scope="col">POC</th>
                <%}%>
                <th rowspan="2" align="center" valign="middle" scope="col">DC COM </th>
                <%if(sProcess.equals("1")){%>
                <th colspan="3" align="center" valign="middle" scope="col">DYED POC </th>
                <%}else{%>
                <th rowspan="2" align="center" valign="middle" scope="col">DYED POC</th>
                <%}%>
                <th rowspan="2" align="center" valign="middle" scope="col">KARDED</th>
                <th rowspan="2" align="center" valign="middle" scope="col">COMBED</th>
                <th rowspan="2" align="center" valign="middle" scope="col">DEPTH</th>
                <th colspan="2" align="center" valign="middle" scope="col">COTTON</th>
                <th rowspan="2" align="center" valign="middle" scope="col">LOT</th>
                <th colspan="5" align="center" valign="middle" scope="col">OTHERS</th>
                <th rowspan="2" align="center" valign="middle" scope="col">TOTAL%</th>
            </tr>
            <tr>
                <th align="center" valign="middle" scope="col">Cnt</th>
                <th align="center" valign="middle" scope="col">Str</th>
                <th align="center" valign="middle" scope="col">Csp</th>
               <%if(sProcess.equals("1")){%> 
                <th align="center" valign="middle" scope="col">DCH-32</th>
                <th align="center" valign="middle" scope="col">LMC</th>
                <th align="center" valign="middle" scope="col">OTHERS </th>
                <th align="center" valign="middle" scope="col">DCH-32</th>
                <th align="center" valign="middle" scope="col">LMC</th>
                <th align="center" valign="middle" scope="col">OTHERS </th>
                <%}%>
                <th align="center" valign="middle" scope="col">CARD</th>
                <th align="center" valign="middle" scope="col">CBD</th>
                <th align="center" valign="middle" scope="col">B.COTTON</th>
                <th align="center" valign="middle" scope="col">USABLE</th>
                <th align="center" valign="middle" scope="col">WHITE<br> MODEL </th>
                <th align="center" valign="middle" scope="col">MIXED</th>
                <th align="center" valign="middle" scope="col">OTHERS </th>
            </tr>
            </thead>
            <%
                   java.util.HashMap theDataMap =new java.util.HashMap();
                    double dCsp = 0,dCnt = 0,dStr = 0;
                    double dUper = 0,dUperTot =0;
                    double dvCvm = 0,dTotCvm = 0;
                    double dThin = 0,dThinTot =0;
                    double dThick =0,dThickTot =0;
                    double dNeps = 0,dNepsTot=0;
                    double dNeps280 = 0,dNeps280Tot=0;
                    double dTotal =0,dTotalTot =0;
                    double dTotal200 =0,dTotal200Tot =0;
                    double dTotal280 =0,dTotal280Tot =0;
                    double dCount = 0,dCountTot = 0;
                    double dCountCv = 0,dCountCvTot = 0;
                    double dStrTot = 0,dStrCv = 0;
                    double dStrCvTot = 0, dCspTot = 0;
                    double dSys = 0,dSysTot = 0;
                    double dRkm = 0,dRkmTot = 0;
                    double dRkmCv = 0,dRkmCvTot = 0;
                    double dElang = 0, dElangTot = 0;
                    double dElangCv = 0, dElangCvTot = 0;
                    double dMixPer = 0;
                    int    iUperCount = 0;
                    int    iCvmCount = 0;
                    int    iThinCount=0;
                    int    iThickCount =0;
                    int    iNepsCount = 0;
                    int    iNeps280Count = 0;
                    int    iTotalCount = 0;
                    int    iTotal200Count = 0;
                    int    iTotal280Count = 0;
                    int    iCountCount = 0;
                    int    iCountCvCount =0;
                    int    iStrCount = 0;
                    int    iStrCvCount = 0;
                    int    iCspCount = 0;
                    int    iSysCount = 0;
                    int    iRkmCount = 0;
                    int    iRkmCvCount = 0;
                    int    iElangCount = 0;
                    int    iElangCvCount = 0;
                    String sPrevOrdNo ="";
                    String sPrevParty ="";
                    String sPrevShade = "";
                    String sPrevQty   = "";
                    String sPrevDate  = "";
                    String ColorCode="";
                    for (int i = 0; i < TestUt3list.size(); i++) {
                       Ut3Details details = (Ut3Details) TestUt3list.get(i);
                       java.util.List ls = details.getOrderList();
                       if(!sPrevOrdNo.equals(Common.parseNull(details.getOrderno()))){%>
             <tbody>          
             <tr>
                 <td><%=Common.parseNull(details.getCountname())%></td>
                 <td colspan="49"><%=Common.parseNull(details.getBlendname())%></td>
             </tr>
             <%}
                       for(int j=0;j<ls.size();j++){
                       theDataMap     =  (HashMap)ls.get(j);
                       dCnt = Common.toDouble(Common.getRound((String)theDataMap.get("COUNT"),2));
                       dStr = Common.toDouble(Common.getRound((String)theDataMap.get("STRENGTH"),2));
                       dCsp = dCnt*dStr;
                       
                       dUper = Common.toDouble(Common.getRound((String)theDataMap.get("UPER"),2));
                       dvCvm = Common.toDouble(Common.getRound((String)theDataMap.get("CVM"),2));
                       dThin = Common.toDouble(Common.getRound((String)theDataMap.get("THIN"),2));
                       dThick = Common.toDouble(Common.getRound((String)theDataMap.get("THICK"),2));
                       dNeps  = Common.toDouble(Common.getRound((String)theDataMap.get("NEPS"),2));
                       dNeps280 = Common.toDouble(Common.getRound((String)theDataMap.get("NEPS280"),2));
                       
                       dTotal = Common.toDouble(Common.getRound((String)theDataMap.get("TOTAL"),2));
                       dTotal200 = Common.toDouble(Common.getRound((String)theDataMap.get("TOT200"),2));
                       dTotal280 = Common.toDouble(Common.getRound((String)theDataMap.get("TOT280"),2));
                       dCount = Common.toDouble(Common.getRound((String)theDataMap.get("COUNT"),2));
                       dCountCv = Common.toDouble(Common.getRound((String)theDataMap.get("COUNTCV"),2));
                       dStrCv  = Common.toDouble(Common.getRound((String)theDataMap.get("STRENGTHCV"),2));
                       dSys = Common.toDouble(Common.getRound((String)theDataMap.get("SYS"),2));
                       dRkm = Common.toDouble(Common.getRound((String)theDataMap.get("RKM"),2));
                       dRkmCv = Common.toDouble(Common.getRound((String)theDataMap.get("RKMCV"),2));
                       dElang = Common.toDouble(Common.getRound((String)theDataMap.get("ELANG"),2));
                       dElangCv = Common.toDouble(Common.getRound((String)theDataMap.get("ELANGCV"),2));
                       String sLink  = "<a class='ajax' href='Ut3ReportDetails?Unit=1&sorderno="+details.getOrderno()+"&datefrom="+(String)theDataMap.get("DATE")+"'>"+Common.parseDate((String)theDataMap.get("DATE"))+"</a>";
                      // String sLink  = "<a class='ajax' href='Ut3ReportDetails?sorderno="+(String)theDataMap.get("ORDNO")+"&datefrom="+(String)theDataMap.get("DATE")+"'>"+(String)theDataMap.get("ORDNO")+"</a>";
                       if (dUper != 0) {
                           iUperCount +=1;
                           dUperTot +=dUper;
                       }
                       if (dvCvm != 0) {
                           iCvmCount +=1;
                           dTotCvm +=dvCvm;
                       }
                       if (dThin != 0) {
                           iThinCount +=1;
                           dThinTot +=dThin;
                       }
                       if (dThick != 0) {
                           iThickCount +=1;
                           dThickTot +=dThick;
                       }
                       if (dNeps != 0) {
                           iNepsCount +=1;
                           dNepsTot +=dNeps;
                       }
                       if (dNeps280 != 0) {
                           iNeps280Count +=1;
                           dNeps280Tot +=dNeps280;
                       }
                       if (dTotal != 0) {
                           iTotalCount +=1;
                           dTotalTot +=dTotal;
                       }
                       if (dTotal200 != 0) {
                           iTotal200Count +=1;
                           dTotal200Tot +=dTotal200;
                       }
                       if (dTotal280 != 0) {
                           iTotal280Count +=1;
                           dTotal280Tot +=dTotal280;
                       }
                       if (dCount != 0) {
                           iCountCount +=1;
                           dCountTot +=dCount;
                       }
                       if (dCountCv != 0) {
                           iCountCvCount +=1;
                           dCountCvTot +=dCountCv;
                       }
                       if (dStr !=0){
                           iStrCount += 1;
                           dStrTot += dStr;
                       }
                       if (dStrCv !=0){
                           iStrCvCount += 1;
                           dStrCvTot += dStrCv;
                       }
                       if (dCsp !=0){
                           iCspCount += 1;
                           dCspTot += dCsp;
                       }
                       if (dSys !=0){
                           iSysCount += 1;
                           dSysTot += dSys;
                       }
                       if (dRkm !=0){
                           iRkmCount += 1;
                           dRkmTot += dRkm;
                       }
                       if (dRkmCv !=0){
                           iRkmCvCount += 1;
                           dRkmCvTot += dRkmCv;
                       }
                       if (dElang !=0){
                           iElangCount += 1;
                           dElangTot += dElang;
                       }
                       if (dElangCv !=0){
                           iElangCvCount += 1;
                           dElangCvTot += dElangCv;
                       }
                       dMixPer = Common.toDouble(Common.getRound((String)theDataMap.get("MIXTOT"),0));                       
                       if(dMixPer<100 || dMixPer>100 ){
                           ColorCode="#E91717";
                       }
                       else
                       {
                          ColorCode=""; 
                       }
             %> 
              
            <tr>
                <%if(j==0){%>
                    <td style="background-color:<%=ColorCode%>"><%=Common.parseNull(details.getOrderno())%></td>
                    <td style="background-color:<%=ColorCode%>"><%=Common.parseNull(details.getShadename())%></td>
                    <td style="background-color:<%=ColorCode%>"><%=Common.parseNull(details.getPartyname())%></td>
                    <td style="background-color:<%=ColorCode%>"><%=Common.parseNull(details.getOrdweight())%></td>
                <%}else{%>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                <%}%>
                <td><%=sLink%></td>
                <td><%=details.getNull(Common.parseNull((String)theDataMap.get("MACHINE")))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("UPER"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("CVM"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("THIN"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("THICK"),2)))%></td>
                
                <%if(sProcess.equals("1")){%>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("NEPS"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("NEPS280"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("TOT200"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("TOT280"),2)))%></td>
                <%}else{%>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("NEPS"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("TOTAL"),2)))%></td>
                <%}%>
                <td><%=details.getNull(Common.parseNull((String)theDataMap.get("CNTREADING")))%></td>
                <td><%=details.getNull(Common.parseNull((String)theDataMap.get("STRREADING")))%></td>
                <td><%=details.getNull(Common.parseNull((String)theDataMap.get("CSPREADING")))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("COUNT"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("COUNTCV"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("STRENGTH"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("STRENGTHCV"),2)))%></td>
                <td><%=details.getNull(Common.getRound(dCsp,2))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("SYS"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("RKM"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("RKMCV"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("ELANG"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("ELANGCV"),2)))%></td>
                <%if(j==0){%>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("VSF"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("DVSF"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("GV"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("DGV"),2)))%></td>
                <%if(sProcess.equals("1")){%>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("POCDCH"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("POCLMC"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("POCOTHERS"),2)))%></td>
                <%}else{%>
                 <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("POC"),2)))%></td>
                <%}%>                
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("DCCOM"),2)))%></td>
                <%if(sProcess.equals("1")){%>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("DPOCDCH"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("DPOCLMC"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("DPOCOTHERS"),2)))%></td>
                <%}else{%>
                    <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("DPOC"),2)))%></td>
                <%}%>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("KARD"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("COMBED"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("DEPTH"),2)))%></td>
                <td><%=details.getNull(Common.parseNull((String)theDataMap.get("KARDVAR")))%></td>
                <td><%=details.getNull(Common.parseNull((String)theDataMap.get("COMBVAR")))%></td>
                <td><%=details.getNull(Common.parseNull((String)theDataMap.get("LOTNO")))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("BCOTTON"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("USABLE"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("MODAL"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("MIXED"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("OTHERS"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("MIXTOT"),0)))%></td>
                <%}else{%>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <%if(sProcess.equals("1")){%>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <%}else{%>
                 <td>&nbsp;</td>
                <%}%>
                <td>&nbsp;</td>
                <%if(sProcess.equals("1")){%>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <%}else{%>
                <td>&nbsp;</td>
                <%}%>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>                
                <%}%>
            </tr>
            
            <%
                        sPrevOrdNo = Common.parseNull(details.getOrderno());
                        sPrevDate = Common.parseNull((String)theDataMap.get("DATE"));
                        sPrevParty = Common.parseNull((String)theDataMap.get("PARTY"));
            }%>
            <tr style="background-color: #C7EFF2;">
                       <td>AVG</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       
                       <td><%=details.getNull(Common.getRound((dUperTot/iUperCount),2))%></td>
                       <td><%=details.getNull(Common.getRound((dTotCvm/iCvmCount),2))%></td>
                       <td><%=details.getNull(Common.getRound((dThinTot/iThinCount),2))%></td>
                       <td><%=details.getNull(Common.getRound((dThickTot/iThickCount),2))%></td>
                       
                       <%if(sProcess.equals("1")){%>
                       <td><%=details.getNull(Common.getRound((dNepsTot/iNepsCount),2))%></td>
                       <td><%=details.getNull(Common.getRound((dNeps280Tot/iNeps280Count),2))%></td>
                       <td><%=details.getNull(Common.getRound((dTotal200Tot/iTotal200Count),2))%></td>
                       <td><%=details.getNull(Common.getRound((dTotal280Tot/iTotal280Count),2))%></td>
                       <%}else{%>
                       <td><%=details.getNull(Common.getRound((dNepsTot/iNepsCount),2))%></td>
                        <td><%=details.getNull(Common.getRound((dTotalTot/iTotalCount),2))%></td>
                       <%}%>
                       <td>&nbsp;</td>                       
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       
                       <td><%=details.getNull(Common.getRound((dCountTot/iCountCount),2))%></td>
                       <td><%=details.getNull(Common.getRound((dCountCvTot/iCountCvCount),2))%></td>
                       <td><%=details.getNull(Common.getRound((dStrTot/iStrCount),2))%></td>
                       <td><%=details.getNull(Common.getRound((dStrCvTot/iStrCvCount),2))%></td>
                       <td><%=details.getNull(Common.getRound((dCspTot/iCspCount),2))%></td>
                       <td><%=details.getNull(Common.getRound((dSysTot/iSysCount),2))%></td>
                       <td><%=details.getNull(Common.getRound((dRkmTot/iRkmCount),2))%></td>
                       <td><%=details.getNull(Common.getRound((dRkmCvTot/iRkmCvCount),2))%></td>
                       
                       <td><%=details.getNull(Common.getRound((dElangTot/iElangCount),2))%></td>
                       <td><%=details.getNull(Common.getRound((dElangCvTot/iElangCvCount),2))%></td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <%if(sProcess.equals("1")){%>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <%}%>
            </tr>
            </tbody>
              <%
                    dUperTot =0;
                    dUper = 0;
                    iUperCount =0;
                    dvCvm = 0;
                    dTotCvm = 0;
                    iCvmCount = 0;
                    dThin = 0;
                    dThinTot =0;
                    iThinCount=0;
                    dThick = 0;
                    dThickTot =0;
                    iThickCount=0;
                    dNeps = 0;
                    dNepsTot = 0;
                    iNepsCount = 0;
                    dNeps280 = 0;
                    dNeps280Tot = 0;
                    iNeps280Count = 0;
                    dTotal = 0;
                    dTotalTot = 0;
                    iTotalCount = 0;
                    dTotal200 = 0;
                    dTotal200Tot = 0;
                    iTotal200Count = 0;
                    dTotal280 = 0;
                    dTotal280Tot = 0;
                    iTotal280Count = 0;
                    dCount = 0;
                    dCountTot = 0;
                    iCountCount = 0;
                    dCountCv=0;
                    dCountCvTot = 0;
                    iCountCvCount = 0;
                    dStr = 0;
                    dStrTot = 0;
                    iStrCount = 0;
                    dStrCv = 0;
                    dStrCvTot = 0;
                    iStrCvCount = 0;
                    dCsp = 0;
                    dCspTot = 0;
                    iCspCount = 0;
                    dSys =0;
                    dSysTot = 0;
                    iSysCount = 0;
                    dRkm = 0;
                    dRkmTot = 0;
                    iRkmCount = 0;
                    dRkmCv = 0;
                    dRkmCvTot = 0;
                    iRkmCvCount = 0;
                    dElang = 0;
                    dElangTot = 0;
                    iElangCount = 0;
                    dElangCv = 0;
                    dElangCvTot = 0;
                    iElangCvCount = 0;
                    }
            %>
        </table>
        <%
                    if(sProcess.equals("1")){
                    Ut3Aunit_OE_Print ut3print = new Ut3Aunit_OE_Print();
                    ut3print.InitExcel();
                    ut3print.WriteStandards(thelist,Common.parseDate((String) request.getAttribute("Fdate")),Common.parseDate((String) request.getAttribute("Tdate")));
                    ut3print.WriteHeading();
                    ut3print.WriteReportData(TestUt3list);
                    ut3print.CloseExcel();
                    }
                    else{
                    Ut3Aunit_Print ut3print = new Ut3Aunit_Print();
                    ut3print.InitExcel();
                    ut3print.WriteStandards(thelist,Common.parseDate((String) request.getAttribute("Fdate")),Common.parseDate((String) request.getAttribute("Tdate")));
                    ut3print.WriteHeading();
                    ut3print.WriteReportData(TestUt3list);
                    ut3print.CloseExcel();
                    }
        }%>
    </body>
</html>