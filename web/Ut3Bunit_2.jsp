<%--
    Document   : Ut3Bunit
    Created on : Nov 4, 2014, 12:12:01 PM
    Author     : admin
--%>

<%@page import="java.util.StringTokenizer"%>
<%@page import="com.reports.classes.CoEffVarient"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="SitraStandards" class="com.reports.data.SitraStandardData"/>
<jsp:useBean id="Common" class="com.common.Common"/>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="css/styles-menu.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.css" type="text/css"/>
        <script src="script/jquery-latest.min.js" type="text/javascript"></script>
        <script src="script/script-menu.js" type="text/javascript"></script>
        <script src="script/jquery-ui.js"></script>
        <script src="script/FormValidateUt3C.js" type="text/javascript"></script>
        <title>Ut3 B-unit</title>
    </head>
    <body>
        <%!
            java.util.List TestUt3list = new java.util.ArrayList();
            java.util.List TestCsplist = new java.util.ArrayList();
            java.util.List TestRkmlist = new java.util.ArrayList();
            java.util.List theDateList = new java.util.ArrayList();
            String SprnBody = "",SprnBody2="";
            double dnominalCnt=0,dnominalStr=0;
        %>
        <%@include file="header.jsp" %>
        <div class="elegant-aero-demo">

            <form action="Ut3Report" method="post" class="elegant-aero">
                <h1>B-Unit UT3 Report<span>Please fill the texts in the fields.</span></h1>
                <span id="error"></span>
                <p>
                    <label><span>Test From </span><input id="tdatefrom" name="tdatefrom" placeholder="FromDate" type="text" class="sdate"></label>
                    <label><span>Test To </span><input id="tdateto" name="tdateto" placeholder="ToDate" type="text" class="sdate"></label>
                    <label><span>&nbsp;</span><input class="button" value="Submit" type="submit" id="btnSubmit" name="btnSubmit">
                        <input class="button" value="Reset" type="reset" id="btnreset" name="btnreset">
                    </label>
                </p>
                <input type="hidden" name="Unit" id="Unit" value="2"/>
            </form>
        </div>
        <br><br>
        <%
            java.util.List thelist = new java.util.ArrayList();
            java.util.HashMap theMap =new java.util.HashMap();
            thelist = SitraStandards.getSitraStandards();
        %>
        <table width="525" border="1" align="center" style="background-color: #D2E9FF;">
            <tr>
                <th colspan="6" scope="col">SITRA STANDARD </th>
            </tr>
            <tr>
                <th scope="row">Count</th>
                <th scope="row">Good</th>
                <th scope="row">AVG</th>
                <th scope="row">POOR</th>
            </tr>
            <%for(int i=0;i<thelist.size();i++) {
            theMap     =  (HashMap)thelist.get(i);%>
            <tr align="center">
                <td><%=Common.parseNull((String)theMap.get("COUNT"))%></td>
                <td><%=Common.parseNull((String)theMap.get("GOOD"))%></td>
                <td><%=Common.parseNull((String)theMap.get("AVERAGE"))%></td>
                <td><%=Common.parseNull((String)theMap.get("POOR"))%></td>
            </tr>
            <%}%>
        </table>
        <br><br>
         <%if (request.getAttribute("theUt3List") != null) {
            TestUt3list = (java.util.List) request.getAttribute("theUt3List");
            com.reports.data.Ut3Data ut3 = new com.reports.data.Ut3Data();
            theDateList = ut3.getDateList((String)request.getAttribute("Fdate"), (String)request.getAttribute("Tdate"));
        %>
        <table border="1" width="100%" class="ut3report">
            <tr>
                <th rowspan="2" align="center" valign="middle" scope="col">DATE</th>
                <th rowspan="2" align="center" valign="middle" scope="col">RF<br> NO </th>
                <th rowspan="2" align="center" valign="middle" scope="col">PARTY NAME </th>
                <th rowspan="2" align="center" valign="middle" scope="col">O.NO</th>
                <th rowspan="2" align="center" valign="middle" scope="col">SHADE</th>
                <th rowspan="2" align="center" valign="middle" scope="col">O.QTY</th>
                <th rowspan="2" align="center" valign="middle" scope="col">U%</th>
                <th rowspan="2" align="center" valign="middle" scope="col">CVM</th>
                <th rowspan="2" align="center" valign="middle" scope="col">THIN</th>
                <th rowspan="2" align="center" valign="middle" scope="col">THICK</th>
                <th rowspan="2" align="center" valign="middle" scope="col">NEPS</th>
                <th rowspan="2" align="center" valign="middle" scope="col">TOTAL<br> IMP </th>
                <th colspan="3" align="center" valign="middle" scope="col">NO<br> RE </th>
                <th rowspan="2" align="center" valign="middle" scope="col">COUNT</th>
                <th rowspan="2" align="center" valign="middle" scope="col">COUNT<br> CV% </th>
                <th rowspan="2" align="center" valign="middle" scope="col">STRENGTH</th>
                <th rowspan="2" align="center" valign="middle" scope="col">STRENGTH<br> CV% </th>
                <th rowspan="2" align="center" valign="middle" scope="col">CSP</th>
                <th rowspan="2" align="center" valign="middle" scope="col">SYS</th>
                <th rowspan="2" align="center" valign="middle" scope="col">RKM</th>
                <th rowspan="2" align="center" valign="middle" scope="col">RKM CV% </th>
                <th rowspan="2" align="center" valign="middle" scope="col">ELONG</th>
                <th rowspan="2" align="center" valign="middle" scope="col">VSF</th>
                <th rowspan="2" align="center" valign="middle" scope="col">DVSF</th>
                <th rowspan="2" align="center" valign="middle" scope="col">GV</th>
                <th rowspan="2" align="center" valign="middle" scope="col">DGV</th>
                <th rowspan="2" align="center" valign="middle" scope="col">POC</th>
                <th rowspan="2" align="center" valign="middle" scope="col">DC COMB<br>AJSM </th>
                <th rowspan="2" align="center" valign="middle" scope="col">CARD</th>
                <th rowspan="2" align="center" valign="middle" scope="col">CBD</th>
                <th rowspan="2" align="center" valign="middle" scope="col">DEP</th>
                <th colspan="2" align="center" valign="middle" scope="col">COTTON</th>
                <th rowspan="2" align="center" valign="middle" scope="col">L.NO</th>
                <th colspan="3" align="center" valign="middle" scope="col">OTHERS</th>
                <th rowspan="2" align="center" valign="middle" scope="col">TOTAL%</th>
            </tr>
            <tr>
                <th align="center" valign="middle" scope="col">Cnt</th>
                <th align="center" valign="middle" scope="col">Str</th>
                <th align="center" valign="middle" scope="col">Csp</th>
                <th align="center" valign="middle" scope="col">CARD</th>
                <th align="center" valign="middle" scope="col">CBD</th>
                <th align="center" valign="middle" scope="col">B.COTTON</th>
                <th align="center" valign="middle" scope="col">USABLE</th>
                <th align="center" valign="middle" scope="col">WHITE<br> MODEL </th>
            </tr>
             <%
                for(int j=0;j< theDateList.size();j++){
                    //System.out.println("theDateList-->"+theDateList.size());
                    for (int i = 0; i < TestUt3list.size(); i++) {
                    com.reports.classes.Ut3MonthWiseDetails Td = (com.reports.classes.Ut3MonthWiseDetails) TestUt3list.get(i);                    
             %>
            <tr>
                <%//}
                    String SDate        = (String)theDateList.get(j);
                    
                    CoEffVarient cev = new CoEffVarient();
                    int iCnt = 0;
                    int iStr = 0;
                    int iCsp = 0;
                    double dcnt1 = Common.toDouble(Td.getCnt1());
                    double dcnt2 = Common.toDouble(Td.getCnt2());
                    double dcnt3 = Common.toDouble(Td.getCnt3());
                    double dcnt4 = Common.toDouble(Td.getCnt4());
                    double dAvgCnt = Common.toDouble(Td.getAvgcnt());
                    double dAvgStr = Common.toDouble(Td.getAvgstr());
                    double dAvgCsp = Common.toDouble(Td.getAvgcsp());
                    String sOrderWeight=Td.getOrdweight();
                    String sPartyName=Td.getPartyname();
                    String sOrderno = Td.getOrderno();
                    String sShadeName=Td.getShadename();
                    String SMechine = Td.getMachinename();
                    //StringTokenizer st = new StringTokenizer(SMechine);
                    //while (st.hasMoreTokens()){
                        //iCnt +=1;
                        //iStr += 1;
                   //}
                    iCnt = ut3.getcount(TestUt3list, SDate,Td.getMachinename());
                    ArrayList<String> arrlistcnt = new ArrayList<String>();
                    if (dcnt1 != 0) {
                        arrlistcnt.add(Td.getCnt1());
                    }
                    if (dcnt2 != 0) {
                        arrlistcnt.add(Td.getCnt2());
                    }
                    if (dcnt3 != 0) {                        
                        arrlistcnt.add(Td.getCnt3());
                    }
                    if (dcnt4 != 0) {                        
                        arrlistcnt.add(Td.getCnt4());
                    }
                    double dCntAvg = (dcnt1 + dcnt2 + dcnt3 + dcnt4) / iCnt;
                    String[] CntArray = (String[]) arrlistcnt.toArray(new String[arrlistcnt.size()]);
                    double dsrten1 = Common.toDouble(Td.getStrength1());
                    double dsrten2 = Common.toDouble(Td.getStrength2());
                    double dsrten3 = Common.toDouble(Td.getStrength3());
                    double dsrten4 = Common.toDouble(Td.getStrength4());
                    ArrayList<String> arrliststr = new ArrayList<String>();
                    if (dsrten1 != 0) {                        
                        arrliststr.add(Td.getStrength1());
                    }
                    if (dsrten2 != 0) {                        
                        arrliststr.add(Td.getStrength2());
                    }
                    if (dsrten3 != 0) {                        
                        arrliststr.add(Td.getStrength3());
                    }
                    if (dsrten4 != 0) {
                        arrliststr.add(Td.getStrength4());
                    }
                    double dStrenAvg = (dsrten1 + dsrten2 + dsrten3 + dsrten4) / iStr;
                    String[] StrArray = (String[]) arrliststr.toArray(new String[arrliststr.size()]);
                    
                    double dCount = dAvgCnt/iCnt;
                    double dStr = dAvgStr/iStr;
                    double dCsp = dAvgCsp/iCsp;  
                    System.out.println("iCnt-->"+iCnt);
                %>
                <% if (SDate.equals(Td.getEntrydate())) {%>
                <td><%=Common.parseDate(Td.getEntrydate())%></td>
                <td><%=Td.getMachinename()%></td>
                <td><%=sPartyName%></td>
                <td><%=sOrderno%></td>
                <td><%=sShadeName%></td>
                <td><%=sOrderWeight%></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td><%=Td.getCntreading()%></td>
                <td><%=Td.getStrreading()%></td>
                <td><%=Td.getCspreading()%></td>
                <td><%=dCount%></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>            
            <%}
                    }
                    }%>
        </table>
        <%}%>
    </body>
</html>
