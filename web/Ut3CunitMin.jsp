<%--
    Document   : Ut3CunitMin
    Created on : Oct 12, 2015, 11:44:05 AM
    Author     : admin
--%>

<%@page import="com.reports.classes.MinAndMaxValue"%>
<%@page import="com.reports.print.Ut3Cunit_Print"%>
<%@page import="com.reports.classes.Ut3Details"%>
<%@page import="com.reports.classes.CoEffVarient"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="SitraStandards" class="com.reports.data.SitraStandardData"/>
<jsp:useBean id="Common" class="com.common.Common"/>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="css/styles-menu.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.css" type="text/css">
        <link rel="stylesheet" href="css/colorbox.css" type="text/css">
        <script src="script/jquery-latest.min.js" type="text/javascript"></script>
        <script src="script/script-menu.js" type="text/javascript"></script>
        <script src="script/jquery-ui.js"></script>
        <script src="script/Ut3AuntiMin.js" type="text/javascript"></script>
         <script src="script/jquery.colorbox-min.js"></script>
        <title>Ut3 Cunit</title>
        <script>
            $(document).ready(function(){
                $(".ajax").colorbox();
            });
        </script>
    </head>
    <body>
         <%!
            java.util.List TestUt3list = new java.util.ArrayList();
        %>
        <%@include file="header.jsp" %>
        <div class="elegant-aero-demo">

            <form action="Ut3Report" method="post" class="elegant-aero">
                <h1>C-Unit UT3 Report<span>Please fill the texts in the fields.</span></h1>
                <span id="error"></span>
                <p>
                   <!-- <label><span>Test From </span><input id="tdatefrom" name="tdatefrom" placeholder="FromDate" type="text" class="sdate"></label>
                    <label><span>Test To </span><input id="tdateto" name="tdateto" placeholder="ToDate" type="text" class="sdate"></label>-->
                    <label><span>Order No</span><input id="orderno" name="orderno" placeholder="Valid Order No" type="text"></label>
                    <label><span>&nbsp;</span><input class="button" value="Submit" type="submit" id="btnSubmit" name="btnSubmit">
                        <input class="button" value="Reset" type="reset" id="btnreset" name="btnreset">
                    </label>
                </p>
                <input type="hidden" name="Unit" id="Unit" value="10"/>
                <input type="hidden" name="ReportType" id="ReportType" value="1"/>
            </form>
        </div>
        <br><br>
         <%if (request.getAttribute("theUt3List") != null) {
            MinAndMaxValue mval = new MinAndMaxValue();
            TestUt3list = (java.util.List) request.getAttribute("theUt3List");
         %>
        <!--<h2 class="heading">C-Unit UT3 Report From <%=Common.parseDate((String) request.getAttribute("Fdate"))%> To <%=Common.parseDate((String) request.getAttribute("Tdate"))%></h2>-->
        <br><br>
        <%
            java.util.List thelist = new java.util.ArrayList();
            java.util.HashMap theMap =new java.util.HashMap();
            thelist = SitraStandards.getSitraStandards();
        %>
        <table width="525" border="1" align="center" class="standards">
            <tr>
                <th colspan="6" scope="col">SITRA STANDARD </th>
            </tr>
            <tr>
                <th scope="row">Count</th>
                <th scope="row">Good</th>
                <th scope="row">AVG</th>
                <th scope="row">POOR</th>
            </tr>
            <%for(int i=0;i<thelist.size();i++) {
            theMap     =  (HashMap)thelist.get(i);%>
            <tr align="center">
                <td><%=Common.parseNull((String)theMap.get("COUNT"))%></td>
                <td><%=Common.parseNull((String)theMap.get("GOOD"))%></td>
                <td><%=Common.parseNull((String)theMap.get("AVERAGE"))%></td>
                <td><%=Common.parseNull((String)theMap.get("POOR"))%></td>
            </tr>
            <%}%>
        </table>
        <br><br>
       
        <table border="1" width="100%" class="ut3report">
            <thead>
            <tr>
                <th rowspan="2" scope="col">O.NO</th>
                <th rowspan="2" scope="col">SHADE</th>
                <th rowspan="2" scope="col">PARTY NAME </th>
                <th rowspan="2" scope="col">O.QTY</th>
                <th rowspan="2" scope="col">DATE</th>
                <th rowspan="2" scope="col">OE NO </th>
                
                <th rowspan="2" scope="col">U%</th>
                <th rowspan="2" scope="col">CVM</th>
                <th rowspan="2" scope="col">THIN</th>
                <th rowspan="2" scope="col">THICK</th>
                <th rowspan="2" scope="col">NEPS<br />(+200)</th>
                <th rowspan="2" scope="col">NEPS<br />(+280)</th>
                <th rowspan="2" scope="col">TOTAL<br />(+200)</th>
                <th rowspan="2" scope="col">TOTAL<br />(+280)</th>
                <th colspan="3" align="center" valign="middle" scope="col">NO<br> RE </th>
                <th rowspan="2" scope="col">COUNT</th>
                <th rowspan="2" scope="col">COUNT CV% </th>
                <th rowspan="2" align="center" valign="middle" scope="col">STRENGTH</th>
                <th rowspan="2" align="center" valign="middle" scope="col">STRENGTH<br /> CV% </th>
                <th rowspan="2" align="center" valign="middle" scope="col">CSP</th>
                <th rowspan="2" align="center" valign="middle" scope="col">SYS</th>
                <th rowspan="2" align="center" valign="middle" scope="col">RKM</th>
                <th rowspan="2" align="center" valign="middle" scope="col">RKM CV% </th>
                <th rowspan="2" align="center" valign="middle" scope="col">ELONG</th>
                <th rowspan="2" align="center" valign="middle" scope="col">ELONG<br /> CV% </th>
                <th rowspan="2" align="center" valign="middle" scope="col">VSF</th>
                <th rowspan="2" align="center" valign="middle" scope="col">DVSF</th>
                <th rowspan="2" align="center" valign="middle" scope="col">GV</th>
                <th rowspan="2" align="center" valign="middle" scope="col">DGV</th>
                <th colspan="3" align="center" valign="middle" scope="col">POC</th>
                <th rowspan="2" align="center" valign="middle" scope="col">DC COM </th>
                <th colspan="3" align="center" valign="middle" scope="col">DYED POC </th>
                <th rowspan="2" align="center" valign="middle" scope="col">KARDED</th>
                <th rowspan="2" align="center" valign="middle" scope="col">COMBED</th>
                <th rowspan="2" align="center" valign="middle" scope="col">BLEACHED<br /> COTTON </th>
                <th rowspan="2" align="center" valign="middle" scope="col">USBLE</th>
                <th rowspan="2" align="center" valign="middle" scope="col">MIXED</th>
                <th rowspan="2" align="center" valign="middle" scope="col">DEPTH</th>
                <th colspan="2" align="center" valign="middle" scope="col">COTTON</th>
                <th rowspan="2" align="center" valign="middle" scope="col">LOT</th>
                <th rowspan="2" align="center" valign="middle" scope="col">OTHERS</th>
                <th rowspan="2" align="center" valign="middle" scope="col">TOTAL</th>
            </tr>
            <tr>
                <th align="center" valign="middle" scope="col">Cnt</th>
                <th align="center" valign="middle" scope="col">Str</th>
                <th align="center" valign="middle" scope="col">Csp</th>
                <th align="center" valign="middle" scope="col">DCH-32</th>
                <th align="center" valign="middle" scope="col">LMC</th>
                <th align="center" valign="middle" scope="col">OTHERS</th>
                <th align="center" valign="middle" scope="col">DCH-32</th>
                <th align="center" valign="middle" scope="col">LMC</th>
                <th align="center" valign="middle" scope="col">OTHERS</th>
                <th align="center" valign="middle" scope="col">CARD</th>
                <th align="center" valign="middle" scope="col">CBD</th>
            </tr>
            </thead>
             <%
                    java.util.HashMap theDataMap =new java.util.HashMap();
                    double dCsp = 0,dCnt = 0,dStr = 0;
                    double dUper = 0,dUperTot =0;
                    double dvCvm = 0,dTotCvm = 0;
                    double dThin = 0,dThinTot =0;
                    double dThick =0,dThickTot =0;
                    double dNeps = 0,dNepsTot=0;
                    double dNeps280 = 0,dNeps280Tot=0;
                    double dTotal =0,dTotalTot =0;
                    double dTotal280 =0,dTotal280Tot =0;
                    double dCount = 0,dCountTot = 0;
                    double dCountCv = 0,dCountCvTot = 0;
                    double dStrTot = 0,dStrCv = 0;
                    double dStrCvTot = 0, dCspTot = 0;
                    double dSys = 0,dSysTot = 0;
                    double dRkm = 0,dRkmTot = 0;
                    double dRkmCv = 0,dRkmCvTot = 0;
                    double dElang = 0, dElangTot = 0;
                    double dElangCv = 0, dElangCvTot = 0;
                    double dMixPer = 0;
                    int    iUperCount = 0;
                    int    iCvmCount = 0;
                    int    iThinCount=0;
                    int    iThickCount =0;
                    int    iNepsCount = 0;
                    int    iNeps280Count = 0;
                    int    iTotalCount = 0;
                    int    iTotal280Count = 0;
                    int    iCountCount = 0;
                    int    iCountCvCount =0;
                    int    iStrCount = 0;
                    int    iStrCvCount = 0;
                    int    iCspCount = 0;
                    int    iSysCount = 0;
                    int    iRkmCount = 0;
                    int    iRkmCvCount = 0;
                    int    iElangCount = 0;
                    int    iElangCvCount = 0;
                    String sPrevOrdNo ="";
                    String sPrevParty ="";
                    String sPrevShade = "";
                    String sPrevQty   = "";
                    String sPrevDate  = "";
                    String ColorCode="";
                    java.util.List<Double> arrTotUper;
                    java.util.List<Double> arrTotCvm;
                    java.util.List<Double> arrTotThin;
                    java.util.List<Double> arrTotThick;
                    java.util.List<Double> arrTotNeps;
                    java.util.List<Double> arrTotNeps280;                    
                    java.util.List<Double> arrTotTotal200;
                    java.util.List<Double> arrTotTotal280;
                    java.util.List<Double> arrTotCnt;
                    java.util.List<Double> arrTotCntCv;
                    java.util.List<Double> arrTotStr;
                    java.util.List<Double> arrTotStrCv;
                    java.util.List<Double> arrTotCsp;
                    java.util.List<Double> arrTotSys;
                    java.util.List<Double> arrTotRkm;
                    java.util.List<Double> arrTotRkmCv;
                    java.util.List<Double> arrTotElong;
                    java.util.List<Double> arrTotElongCv;
                   for (int i = 0; i < TestUt3list.size(); i++) {
                       Ut3Details details = (Ut3Details) TestUt3list.get(i);
                       java.util.List ls = details.getOrderList();
                       arrTotUper = new ArrayList<Double>();
                        arrTotCvm = new ArrayList<Double>();
                        arrTotThin = new ArrayList<Double>();
                        arrTotThick = new ArrayList<Double>();
                        arrTotNeps = new ArrayList<Double>();
                        arrTotNeps280 = new ArrayList<Double>();                        
                        arrTotTotal200 = new ArrayList<Double>();
                        arrTotTotal280 = new ArrayList<Double>();
                        arrTotCnt= new ArrayList<Double>();
                        arrTotCntCv= new ArrayList<Double>();
                        arrTotStr= new ArrayList<Double>();
                        arrTotStrCv= new ArrayList<Double>();
                        arrTotCsp= new ArrayList<Double>();
                        arrTotSys = new ArrayList<Double>();
                        arrTotRkm = new ArrayList<Double>();
                        arrTotRkmCv = new ArrayList<Double>();
                        arrTotElong = new ArrayList<Double>();
                        arrTotElongCv = new ArrayList<Double>();
                       if(!sPrevOrdNo.equals(Common.parseNull(details.getOrderno()))){%>
                       <tr>
                        <td><%=Common.parseNull(details.getCountname())%></td>
                        <td colspan="48"><%=Common.parseNull(details.getBlendname())%></td>
                        </tr>
                    <%}
                       for(int j=0;j<ls.size();j++){
                       theDataMap     =  (HashMap)ls.get(j);
                       dCnt = Common.toDouble(Common.getRound((String)theDataMap.get("COUNT"),2));
                       dStr = Common.toDouble(Common.getRound((String)theDataMap.get("STRENGTH"),2));
                       dCsp = dCnt*dStr;
                       dUper = Common.toDouble(Common.getRound((String)theDataMap.get("UPER"),2));
                       dvCvm = Common.toDouble(Common.getRound((String)theDataMap.get("CVM"),2));
                       dThin = Common.toDouble(Common.getRound((String)theDataMap.get("THIN"),2));
                       dThick = Common.toDouble(Common.getRound((String)theDataMap.get("THICK"),2));
                       dNeps  = Common.toDouble(Common.getRound((String)theDataMap.get("NEPS"),2));
                       dNeps280 = Common.toDouble(Common.getRound((String)theDataMap.get("NEPS280"),2));
                       dTotal = Common.toDouble(Common.getRound((String)theDataMap.get("TOT200"),2));
                       dTotal280 = Common.toDouble(Common.getRound((String)theDataMap.get("TOTAL280"),2));
                       dCount = Common.toDouble(Common.getRound((String)theDataMap.get("COUNT"),2));
                       dCountCv = Common.toDouble(Common.getRound((String)theDataMap.get("COUNTCV"),2));
                       dStrCv  = Common.toDouble(Common.getRound((String)theDataMap.get("STRENGTHCV"),2));
                       dSys = Common.toDouble(Common.getRound((String)theDataMap.get("SYS"),2));
                       dRkm = Common.toDouble(Common.getRound((String)theDataMap.get("RKM"),2));
                       dRkmCv = Common.toDouble(Common.getRound((String)theDataMap.get("RKMCV"),2));
                       dElang = Common.toDouble(Common.getRound((String)theDataMap.get("ELANG"),2));
                       dElangCv = Common.toDouble(Common.getRound((String)theDataMap.get("ELANGCV"),2));
                       String sLink  = "<a class='ajax' href='Ut3ReportDetails?Unit=10&sorderno="+details.getOrderno()+"&datefrom="+(String)theDataMap.get("DATE")+"'>"+Common.parseDate((String)theDataMap.get("DATE"))+"</a>";
                       //String sLink  = "<a class='ajax' href='Ut3ReportDetails?sorderno="+(String)theDataMap.get("ORDNO")+"&datefrom="+(String)theDataMap.get("DATE")+"'>"+(String)theDataMap.get("ORDNO")+"</a>";
                       if (dUper != 0) {
                           iUperCount +=1;
                           dUperTot +=dUper;
                           arrTotUper.add(dUper);
                       }
                       if (dvCvm != 0) {
                           iCvmCount +=1;
                           dTotCvm +=dvCvm;
                           arrTotCvm.add(dvCvm);
                       }
                       if (dThin != 0) {
                           iThinCount +=1;
                           dThinTot +=dThin;
                           arrTotThin.add(dThin);
                       }
                       if (dThick != 0) {
                           iThickCount +=1;
                           dThickTot +=dThick;
                           arrTotThick.add(dThick);
                       }
                       if (dNeps != 0) {
                           iNepsCount +=1;
                           dNepsTot +=dNeps;
                           arrTotNeps.add(dNeps);
                       }
                       if (dNeps280 != 0) {
                           iNeps280Count +=1;
                           dNeps280Tot +=dNeps280;
                           arrTotNeps280.add(dNeps280);
                       }
                       if (dTotal != 0) {
                           iTotalCount +=1;
                           dTotalTot +=dTotal;
                           arrTotTotal200.add(dTotal);
                       }
                       if (dTotal280 != 0) {
                           iTotal280Count +=1;
                           dTotal280Tot +=dTotal280;
                           arrTotTotal280.add(dTotal280);
                       }
                       if (dCount != 0) {
                           iCountCount +=1;
                           dCountTot +=dCount;
                           arrTotCnt.add(dCount);
                       }
                       if (dCountCv != 0) {
                           iCountCvCount +=1;
                           dCountCvTot +=dCountCv;
                           arrTotCntCv.add(dCountCv);
                       }
                       if (dStr !=0){
                           iStrCount += 1;
                           dStrTot += dStr;
                           arrTotStr.add(dStr);
                       }
                       if (dStrCv !=0){
                           iStrCvCount += 1;
                           dStrCvTot += dStrCv;
                           arrTotStrCv.add(dStrCv);
                       }
                       if (dCsp !=0){
                           iCspCount += 1;
                           dCspTot += dCsp;
                           arrTotCsp.add(dCsp);
                       }
                       if (dSys !=0){
                           iSysCount += 1;
                           dSysTot += dSys;
                           arrTotSys.add(dSys);
                       }
                       if (dRkm !=0){
                           iRkmCount += 1;
                           dRkmTot += dRkm;
                           arrTotRkm.add(dRkm);
                       }
                       if (dRkmCv !=0){
                           iRkmCvCount += 1;
                           dRkmCvTot += dRkmCv;
                           arrTotRkmCv.add(dRkmCv);
                       }
                       if (dElang !=0){
                           iElangCount += 1;
                           dElangTot += dElang;
                           arrTotElong.add(dElang);
                       }
                       if (dElangCv !=0){
                           iElangCvCount += 1;
                           dElangCvTot += dElangCv;
                           arrTotElongCv.add(dElangCv);
                       }
                       dMixPer = Common.toDouble(Common.getRound((String)theDataMap.get("MIXTOT"),0));                       
                       if(dMixPer<100 || dMixPer>100){
                           ColorCode="#E91717";
                       }
                       else
                       {
                          ColorCode=""; 
                       }
                    %>
                    <tr>
               <%if(j==0){%>
                    <td style="background-color:<%=ColorCode%>"><%=Common.parseNull(details.getOrderno())%></td>
                    <td style="background-color:<%=ColorCode%>"><%=Common.parseNull(details.getShadename())%></td>
                    <td style="background-color:<%=ColorCode%>"><%=Common.parseNull(details.getPartyname())%></td>
                    <td style="background-color:<%=ColorCode%>"><%=Common.parseNull(details.getOrdweight())%></td>
                <%}else{%>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                <%}%>
                <td><%=sLink%></td>
                <td><%=details.getNull(Common.parseNull((String)theDataMap.get("MACHINE")))%></td>
                <td><%=details.getNull(Common.parseNull((String)theDataMap.get("UPER")))%></td>
                <td><%=details.getNull(Common.parseNull((String)theDataMap.get("CVM")))%></td>
                <td><%=details.getNull(Common.parseNull((String)theDataMap.get("THIN")))%></td>
                <td><%=details.getNull(Common.parseNull((String)theDataMap.get("THICK")))%></td>
                <td><%=details.getNull(Common.parseNull((String)theDataMap.get("NEPS")))%></td>
                <td><%=details.getNull(Common.parseNull((String)theDataMap.get("NEPS280")))%></td>
                <td><%=details.getNull(Common.parseNull((String)theDataMap.get("TOT200")))%></td>
                <td><%=details.getNull(Common.parseNull((String)theDataMap.get("TOT280")))%></td>
                <td><%=details.getNull(Common.parseNull((String)theDataMap.get("CNTREADING")))%></td>
                <td><%=details.getNull(Common.parseNull((String)theDataMap.get("STRREADING")))%></td>
                <td><%=details.getNull(Common.parseNull((String)theDataMap.get("CSPREADING")))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("COUNT"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("COUNTCV"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("STRENGTH"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("STRENGTHCV"),2)))%></td>
                <td><%=details.getNull(Common.getRound(dCsp,2))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("SYS"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("RKM"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("RKMCV"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("ELANG"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("ELANGCV"),2)))%></td>
                <%if(j==0){%>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("VSF"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("DVSF"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("GV"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("DGV"),2)))%></td>                
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("POCDCH"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("POCLMC"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("POCOTHERS"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("DCCOM"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("DPOCDCH"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("DPOCLMC"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("DPOCOTHERS"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("KARD"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("COMBED"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("BCOTTON"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("USABLE"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("MIXED"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("DEPTH"),2)))%></td>
                <td><%=details.getNull(Common.parseNull((String)theDataMap.get("KARDVAR")))%></td>
                <td><%=details.getNull(Common.parseNull((String)theDataMap.get("COMBVAR")))%></td>
                <td><%=details.getNull(Common.parseNull((String)theDataMap.get("LOTNO")))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("OTHERS"),2)))%></td>
                <td><%=details.getNull(Common.parseNull(Common.getRound((String)theDataMap.get("MIXTOT"),0)))%></td>
                 <%}else{%>
                 <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <%}%>
            </tr>
            <%
                        sPrevOrdNo = Common.parseNull(details.getOrderno());
                        sPrevDate = Common.parseNull((String)theDataMap.get("DATE"));
                        sPrevParty = Common.parseNull((String)theDataMap.get("PARTY"));
                    }%>
                    <tr style="background-color: #C7EFF2;"> 
                    <td>AVG</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       
                       <td><%=details.getNull(Common.getRound((dUperTot/iUperCount),2))%></td>
                       <td><%=details.getNull(Common.getRound((dTotCvm/iCvmCount),2))%></td>
                       <td><%=details.getNull(Common.getRound((dThinTot/iThinCount),2))%></td>
                       <td><%=details.getNull(Common.getRound((dThickTot/iThickCount),2))%></td>
                       <td><%=details.getNull(Common.getRound((dNepsTot/iNepsCount),2))%></td>
                       <td><%=details.getNull(Common.getRound((dNeps280Tot/iNeps280Count),2))%></td>
                       <td><%=details.getNull(Common.getRound((dTotalTot/iTotalCount),2))%></td>
                       <td><%=details.getNull(Common.getRound((dTotal280Tot/iTotal280Count),2))%></td>
                       <td>&nbsp;</td>                       
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       
                       <td><%=details.getNull(Common.getRound((dCountTot/iCountCount),2))%></td>
                       <td><%=details.getNull(Common.getRound((dCountCvTot/iCountCvCount),2))%></td>
                       <td><%=details.getNull(Common.getRound((dStrTot/iStrCount),2))%></td>
                       <td><%=details.getNull(Common.getRound((dStrCvTot/iStrCvCount),2))%></td>
                       <td><%=details.getNull(Common.getRound((dCspTot/iCspCount),2))%></td>
                       
                       <td><%=details.getNull(Common.getRound((dSysTot/iSysCount),2))%></td>
                       <td><%=details.getNull(Common.getRound((dRkmTot/iRkmCount),2))%></td>
                       <td><%=details.getNull(Common.getRound((dRkmCvTot/iRkmCvCount),2))%></td>
                       
                       <td><%=details.getNull(Common.getRound((dElangTot/iElangCount),2))%></td>
                       <td><%=details.getNull(Common.getRound((dElangCvTot/iElangCvCount),2))%></td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                    </tr>
                    <!-- MinMax -->
                    <%
                    double uperTot[] = new double[arrTotUper.size()];
                    double cvmTot[] = new double[arrTotCvm.size()];
                    double thinTot[] = new double[arrTotThin.size()];
                    double thickTot[] = new double[arrTotThick.size()];
                    double nepsTot[] = new double[arrTotNeps.size()];
                    double nepsTot280[] = new double[arrTotNeps280.size()];
                    double total200Tot[] = new double[arrTotTotal200.size()];
                    double total280Tot[] = new double[arrTotTotal280.size()];
                    double cntTot[] = new double[arrTotCnt.size()];
                    double cntcvTot[] = new double[arrTotCntCv.size()];
                    double strTot[] = new double[arrTotStr.size()];
                    double strcvTot[] = new double[arrTotStrCv.size()];
                    double cspTot[] = new double[arrTotCsp.size()];
                    double sysTot[] = new double[arrTotSys.size()];
                    double rkmTot[] = new double[arrTotRkm.size()];
                    double rkmcvTot[] = new double[arrTotRkmCv.size()];
                    double elongTot[] = new double[arrTotElong.size()];
                    double elongcvTot[] = new double[arrTotElongCv.size()];
                    for(int j = 0; j < uperTot.length; j++){
                            uperTot[j] = arrTotUper.get(j);
                    }
                    for(int j = 0; j < cvmTot.length; j++){
                            cvmTot[j] = arrTotCvm.get(j);
                    }
                    for(int j = 0; j < thinTot.length; j++){
                            thinTot[j] = arrTotThin.get(j);
                    }
                    for(int j = 0; j < thickTot.length; j++){
                            thickTot[j] = arrTotThick.get(j);
                    }
                    for(int j = 0; j < nepsTot.length; j++){
                            nepsTot[j] = arrTotNeps.get(j);
                    }
                    for(int j = 0; j < nepsTot280.length; j++){
                            nepsTot280[j] = arrTotNeps280.get(j);
                    }
                    for(int j = 0; j < total200Tot.length; j++){
                            total200Tot[j] = arrTotTotal200.get(j);
                    }
                    for(int j = 0; j < total280Tot.length; j++){
                            total280Tot[j] = arrTotTotal280.get(j);
                    }
                    for(int j = 0; j < cntTot.length; j++){
                            cntTot[j] = arrTotCnt.get(j);
                    }
                    for(int j = 0; j < cntcvTot.length; j++){
                            cntcvTot[j] = arrTotCntCv.get(j);
                    }
                    for(int j = 0; j < strTot.length; j++){
                            strTot[j] = arrTotStr.get(j);
                    }
                    for(int j = 0; j < strcvTot.length; j++){
                            strcvTot[j] = arrTotStrCv.get(j);
                    }
                    for(int j = 0; j < cspTot.length; j++){
                            cspTot[j] = arrTotCsp.get(j);
                    }
                    for(int j = 0; j < sysTot.length; j++){
                            sysTot[j] = arrTotSys.get(j);
                    }
                    for(int j = 0; j < rkmTot.length; j++){
                            rkmTot[j] = arrTotRkm.get(j);
                    }
                    for(int j = 0; j < rkmcvTot.length; j++){
                            rkmcvTot[j] = arrTotRkmCv.get(j);
                    }
                    for(int j = 0; j < elongTot.length; j++){
                            elongTot[j] = arrTotElong.get(j);
                    }
                    for(int j = 0; j < elongcvTot.length; j++){
                            elongcvTot[j] = arrTotElongCv.get(j);
                    }
                    %>
                    <tr style="background-color: rgb(240, 206, 138);">
                    <td>MIN</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       
                       <%if(uperTot.length>1){%><td><%=Common.getRound(mval.getMin(uperTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(cvmTot.length>1){%><td><%=Common.getRound(mval.getMin(cvmTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(thinTot.length>1){%><td><%=Common.getRound(mval.getMin(thinTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(thickTot.length>1){%><td><%=Common.getRound(mval.getMin(thickTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(nepsTot.length>1){%><td><%=Common.getRound(mval.getMin(nepsTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(nepsTot280.length>1){%><td><%=Common.getRound(mval.getMin(nepsTot280),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(total200Tot.length>1){%><td><%=Common.getRound(mval.getMin(total200Tot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(total280Tot.length>1){%><td><%=Common.getRound(mval.getMin(total280Tot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <td>&nbsp;</td>                       
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       
                       <%if(cntTot.length>1){%><td><%=Common.getRound(mval.getMin(cntTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(cntcvTot.length>1){%><td><%=Common.getRound(mval.getMin(cntcvTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(strTot.length>1){%><td><%=Common.getRound(mval.getMin(strTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(strcvTot.length>1){%><td><%=Common.getRound(mval.getMin(strcvTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(cspTot.length>1){%><td><%=Common.getRound(mval.getMin(cspTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(sysTot.length>1){%><td><%=Common.getRound(mval.getMin(sysTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(rkmTot.length>1){%><td><%=Common.getRound(mval.getMin(rkmTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(rkmcvTot.length>1){%><td><%=Common.getRound(mval.getMin(rkmcvTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(elongTot.length>1){%><td><%=Common.getRound(mval.getMin(elongTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(elongcvTot.length>1){%><td><%=Common.getRound(mval.getMin(elongcvTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                    </tr>
                    <tr style="background-color: rgb(240, 206, 138);">
                    <td>MAX</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       
                       <%if(uperTot.length>1){%><td><%=Common.getRound(mval.getMax(uperTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(cvmTot.length>1){%><td><%=Common.getRound(mval.getMax(cvmTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(thinTot.length>1){%><td><%=Common.getRound(mval.getMax(thinTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(thickTot.length>1){%><td><%=Common.getRound(mval.getMax(thickTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>                       
                       <%if(nepsTot.length>1){%><td><%=Common.getRound(mval.getMax(nepsTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(nepsTot280.length>1){%><td><%=Common.getRound(mval.getMax(nepsTot280),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(total200Tot.length>1){%><td><%=Common.getRound(mval.getMax(total200Tot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(total280Tot.length>1){%><td><%=Common.getRound(mval.getMax(total280Tot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <td>&nbsp;</td>                       
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       
                       <%if(cntTot.length>1){%><td><%=Common.getRound(mval.getMax(cntTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(cntcvTot.length>1){%><td><%=Common.getRound(mval.getMax(cntcvTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(strTot.length>1){%><td><%=Common.getRound(mval.getMax(strTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(strcvTot.length>1){%><td><%=Common.getRound(mval.getMax(strcvTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(cspTot.length>1){%><td><%=Common.getRound(mval.getMax(cspTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(sysTot.length>1){%><td><%=Common.getRound(mval.getMax(sysTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(rkmTot.length>1){%><td><%=Common.getRound(mval.getMax(rkmTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(rkmcvTot.length>1){%><td><%=Common.getRound(mval.getMax(rkmcvTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(elongTot.length>1){%><td><%=Common.getRound(mval.getMax(elongTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(elongcvTot.length>1){%><td><%=Common.getRound(mval.getMax(elongcvTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                    </tr>
                    <td>RANGE</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       
                       <%if(uperTot.length>1){%><td><%=Common.getRound(mval.getMax(uperTot)-mval.getMin(uperTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(cvmTot.length>1){%><td><%=Common.getRound(mval.getMax(cvmTot)-mval.getMin(cvmTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(thinTot.length>1){%><td><%=Common.getRound(mval.getMax(thinTot)-mval.getMin(thinTot),2)%><%}else{%><td>&nbsp;</td><%}%>
                       <%if(thickTot.length>1){%><td><%=Common.getRound(mval.getMax(thickTot)-mval.getMin(thickTot),2)%><%}else{%><td>&nbsp;</td><%}%>
                       <%if(nepsTot.length>1){%><td><%=Common.getRound(mval.getMax(nepsTot)-mval.getMin(nepsTot),2)%><%}else{%><td>&nbsp;</td><%}%>
                       <%if(nepsTot280.length>1){%><td><%=Common.getRound(mval.getMax(nepsTot280)-mval.getMin(nepsTot280),2)%><%}else{%><td>&nbsp;</td><%}%>
                       <%if(total200Tot.length>1){%><td><%=Common.getRound(mval.getMax(total200Tot)-mval.getMin(total200Tot),2)%><%}else{%><td>&nbsp;</td><%}%>
                       <%if(total280Tot.length>1){%><td><%=Common.getRound(mval.getMax(total280Tot)-mval.getMin(total280Tot),2)%><%}else{%><td>&nbsp;</td><%}%>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       
                       <%if(cntTot.length>1){%><td><%=Common.getRound(mval.getMax(cntTot)-mval.getMin(cntTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(cntcvTot.length>1){%><td><%=Common.getRound(mval.getMax(cntcvTot)-mval.getMin(cntcvTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(strTot.length>1){%><td><%=Common.getRound(mval.getMax(strTot)-mval.getMin(strTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(strcvTot.length>1){%><td><%=Common.getRound(mval.getMax(strcvTot)-mval.getMin(strcvTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(cspTot.length>1){%><td><%=Common.getRound(mval.getMax(cspTot)-mval.getMin(cspTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>

                       <%if(sysTot.length>1){%><td><%=Common.getRound(mval.getMax(sysTot)-mval.getMin(sysTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(rkmTot.length>1){%><td><%=Common.getRound(mval.getMax(rkmTot)-mval.getMin(rkmTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(rkmcvTot.length>1){%><td><%=Common.getRound(mval.getMax(rkmcvTot)-mval.getMin(rkmcvTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>

                       <%if(elongTot.length>1){%><td><%=Common.getRound(mval.getMax(elongTot)-mval.getMin(elongTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <%if(elongcvTot.length>1){%><td><%=Common.getRound(mval.getMax(elongcvTot)-mval.getMin(elongcvTot),2)%></td><%}else{%><td>&nbsp;</td><%}%>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                       <td>&nbsp;</td>
                    </tr>
                   <%
                        dUperTot =0;dUper = 0;iUperCount =0;dvCvm = 0;dTotCvm = 0;iCvmCount = 0;dThin = 0;dThinTot =0;iThinCount=0;
                        dThick = 0;dThickTot =0;iThickCount=0;dNeps = 0;dNepsTot = 0;iNepsCount = 0;dNeps280 = 0;dNeps280Tot = 0;iNeps280Count = 0;
                        dTotal = 0;dTotalTot = 0;iTotalCount = 0;dTotal280 = 0;dTotal280Tot = 0;iTotal280Count = 0;dCount = 0;dCountTot = 0;
                        iCountCount = 0;dCountCv=0;dCountCvTot = 0;iCountCvCount = 0;dStr = 0;dStrTot = 0;iStrCount = 0;dStrCv = 0;
                        dStrCvTot = 0;iStrCvCount = 0;dCsp = 0;dCspTot = 0;iCspCount = 0;dSys =0;dSysTot = 0;iSysCount = 0;dRkm = 0;dRkmTot = 0;
                        iRkmCount = 0;dRkmCv = 0;dRkmCvTot = 0;iRkmCvCount = 0;dElang = 0;dElangTot = 0;iElangCount = 0;dElangCv = 0;dElangCvTot = 0;
                        iElangCvCount = 0;
                   }
            %>
        </table>
        <%
                   /*
                    Ut3Cunit_Print ut3print = new Ut3Cunit_Print();
                    ut3print.InitExcel();
                    ut3print.WriteStandards(thelist,Common.parseDate((String) request.getAttribute("Fdate")),Common.parseDate((String) request.getAttribute("Tdate")));
                    ut3print.WriteHeading();
                    ut3print.WriteReportData(TestUt3list);
                    ut3print.CloseExcel();
                    */ 
         }%>
    </body>
</html>
<!--
DM05180
DM05199
DMI00184
-->