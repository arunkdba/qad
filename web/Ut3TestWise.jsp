<%-- 
    Document   : Ut3TestWise
    Created on : 21 Nov, 2017, 2:17:05 PM
    Author     : Administrator
--%>

<%@page import="com.reports.print.Ut3TestWisePrint"%>
<%@page import="com.reports.pdf.UT3TestWisePDF"%>
<%@page import="com.reports.classes.CoEffVarient"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.common.Common"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="css/styles-menu.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.css" type="text/css">
        <script src="script/jquery-latest.min.js" type="text/javascript"></script>
        <script src="script/script-menu.js" type="text/javascript"></script>
        <script src="script/jquery-ui.js"></script>
        <script src="script/ut3testwiseformvalidate.js" type="text/javascript"></script>
        <title>Spinning Wrapping Test</title>
        <script>
            $(document).ready(function(){
                //$('tr:not(:has(td[rowspan])):even').addClass('oddrow');
            });
        </script>
        <script type="text/javascript">
             function showAlert(){
                var msg = "PDF File Created in D:\UT3TestwiseNewPDF, PDF in Legal Sheet";
                alert(msg);
            }
        </script>
        
    </head>
    <body>
        <%!
            java.util.List Testlist = new java.util.ArrayList();
            Common common = new Common();
            String SprnBody = "",SprnBody2="";
            double dnominalCnt=0,dnominalStr=0;
            double dprocess =0;
            String SProcess="",SRh="";
            int iTotalTest = 0;
        %>
        <%@include file="header.jsp" %>
        <div class="elegant-aero-demo">
            <form action="Ut3TestWiseReport" method="post" class="elegant-aero">
                <h1>UT3 Test<span>Please fill the texts in the fields.</span></h1>
                <span id="error"></span>
                <p>
                    <!--<label><span>Test As  </span><input id="tdatefrom" name="tdatefrom" placeholder="FromDate" type="text" class="sdate"/></label>-->
                    <label><span>As On Date </span><input id="tdateto" name="tdateto" placeholder="ToDate" type="text" class="sdate"/></label>
                    <label><span>Test No </span><select name="testno" id="testno" class="testno">
                    </select></label>
                    <label><span>&nbsp;</span><input class="button" value="Submit" type="submit" id="btnSubmit" name="btnSubmit" onClick="showAlert()"/>
                    <input class="button" value="Reset" type="reset" id="btnreset" name="btnreset"/>
                    </label>
                </p>
            </form>
        </div>
        <br/><br/>
        <%if (request.getAttribute("theTestDetailsList") != null) {
            java.util.List TBase = new java.util.ArrayList();
            TBase = (java.util.List) request.getAttribute("theTestBase");
            java.util.HashMap theBaseMap = (java.util.HashMap)TBase.get(0);
            iTotalTest = common.toInt((String)theBaseMap.get("TESTS"));
            CoEffVarient cev = new CoEffVarient();
            Ut3TestWisePrint sprint = new Ut3TestWisePrint();
            UT3TestWisePDF   spdf   = new UT3TestWisePDF();
                        
            sprint.createPrn();
            sprint.setTestBase((String) request.getAttribute("Fdate"), (String) request.getAttribute("Tdate"), theBaseMap);
            sprint.setHead((String) request.getAttribute("Fdate"));
            
            spdf.createPDFFile();
            spdf.setTestBase((String) request.getAttribute("Fdate"), (String) request.getAttribute("Tdate"), theBaseMap);
            spdf.setHead((String) request.getAttribute("Fdate"));
            
        %>
        <table width="100%" border="1" class="spinningwrapping">
            <tr>
                <td width="183">Art No : <%=common.parseNull((String)theBaseMap.get("ARTNO"))%> </td>
                <td width="236">Test No: <%=common.parseNull((String)theBaseMap.get("TESTID"))%> </td>
                <td width="209">Fiber Assembly:<%=common.parseNull((String)theBaseMap.get("FIBREASSEMBLY"))%> </td>
            </tr>
            <tr>
                <td>Fiber :<%=common.parseNull((String)theBaseMap.get("FIBER"))%></td>
                <td>Order No : <%=common.parseNull((String)theBaseMap.get("ORDERNO"))%></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>V: <%=common.parseNull((String)theBaseMap.get("VDATA"))%></td>
                <td>T : <%=common.parseNull((String)theBaseMap.get("TDATA"))%></td>
                <td>Tests :  <%=common.parseNull((String)theBaseMap.get("TESTS"))%></td>
            </tr>
            <tr>
                <td>Slot :  <%=common.parseNull((String)theBaseMap.get("SLOT"))%></td>
                <td>Yarn Tension :  <%=common.parseNull((String)theBaseMap.get("YARNTENSION"))%></td>
                <td>Imperfections : <%=common.parseNull((String)theBaseMap.get("IMPERFECTIONS"))%></td>
            </tr>
        </table>
        <table width="100%" border="1" class="spinningwrapping">
            <tr>
                <th width="113" scope="col" align="center" valign="top">Test/<br>No</th>
                <th width="40" align="center" valign="top" scope="col">UM</th>
                <th width="82" scope="col" align="center" valign="top">CVM</th>
                <th width="96" scope="col" align="center" valign="top">CVM(1m)</th>
                <th width="92" scope="col" align="center" valign="top">CVM(3m)</th>
                <th width="89" scope="col" align="center" valign="top">Index</th>
                <th width="89" scope="col" align="center" valign="top">Thin(-30%)</th>
                <th width="76" scope="col" align="center" valign="top">Thin(-40%)</th>
                <th width="91" scope="col" align="center" valign="top">Thin(-50%) </th>
                <th width="92" scope="col" align="center" valign="top">Thin(+35%)</th>
                <th width="68" scope="col" align="center" valign="top">Thin(+50%)</th>                
                <th width="49" scope="col" align="center" valign="top">Neps(+140%)</th>
                <th width="41" scope="col" align="center" valign="top">Neps(+200%)</th>
                <th width="58" scope="col" align="center" valign="top">Neps(+280%)</th>
            </tr>
            <%
            String[] UmArray = new String[iTotalTest];
            String[] CvmArray = new String[iTotalTest];
            String[] Cvm1Array = new String[iTotalTest];
            String[] Cvm3Array = new String[iTotalTest];
            String[] IndexArray = new String[iTotalTest];
            String[] Thin30Array = new String[iTotalTest];
            String[] Thin40Array = new String[iTotalTest];
            String[] Thin50Array = new String[iTotalTest];
            String[] Thin35Array = new String[iTotalTest];
            String[] ThinP50Array = new String[iTotalTest];
            String[] Neps140Array = new String[iTotalTest];
            String[] Neps200Array = new String[iTotalTest];
            String[] Neps280Array = new String[iTotalTest];
            Testlist = (java.util.List) request.getAttribute("theTestDetailsList");
            spdf.setPrintData((String) request.getAttribute("Fdate"),Testlist,iTotalTest);
           
            
            for (int i = 0; i < Testlist.size(); i++) {
                SprnBody="";
                java.util.HashMap theMap = (java.util.HashMap)Testlist.get(i);
                UmArray[i] = common.parseNull((String)theMap.get("UM"));
                CvmArray[i]   = common.parseNull((String)theMap.get("CVM"));
                Cvm1Array[i]   = common.parseNull((String)theMap.get("CVM1M"));
                Cvm3Array[i]   = common.parseNull((String)theMap.get("CVM3M"));
                IndexArray[i]   = common.parseNull((String)theMap.get("INDEXX"));
                Thin30Array[i]  = common.parseNull((String)theMap.get("THINMI30"));      
                Thin40Array[i]  = common.parseNull((String)theMap.get("THINMI40"));
                Thin50Array[i]  = common.parseNull((String)theMap.get("THINMI50"));
                Thin35Array[i]  = common.parseNull((String)theMap.get("THICKPL35"));
                ThinP50Array[i] = common.parseNull((String)theMap.get("THICKPL50"));
                Neps140Array[i] = common.parseNull((String)theMap.get("NEPSPL140"));
                Neps200Array[i] = common.parseNull((String)theMap.get("NEPSPL200"));
                Neps280Array[i] = common.parseNull((String)theMap.get("NEPSPL280"));
            %>
                    <tr>
                    <td><%=common.parseNull((String)theMap.get("TESTNO"))%></td>
                    <td><%=common.parseNull((String)theMap.get("UM"))%></td>
                    <td><%=common.parseNull((String)theMap.get("CVM"))%></td>
                    <td><%=common.parseNull((String)theMap.get("CVM1M"))%></td>
                    <td><%=common.parseNull((String)theMap.get("CVM3M"))%></td>
                    <td><%=common.parseNull((String)theMap.get("INDEXX"))%></td>
                    <td><%=common.parseNull((String)theMap.get("THINMI30"))%></td>
                    <td><%=common.parseNull((String)theMap.get("THINMI40"))%></td>
                    <td><%=common.parseNull((String)theMap.get("THINMI50"))%></td>
                    <td><%=common.parseNull((String)theMap.get("THICKPL35"))%></td>
                    <td><%=common.parseNull((String)theMap.get("THICKPL50"))%></td>
                    <td><%=common.parseNull((String)theMap.get("NEPSPL140"))%></td>
                    <td><%=common.parseNull((String)theMap.get("NEPSPL200"))%></td>
                    <td><%=common.parseNull((String)theMap.get("NEPSPL280"))%></td>
                    </tr>
            <%
                SprnBody += common.Pad("| " + String.valueOf(i + 1), 6) + "|";
                SprnBody += common.Pad(common.parseNull((String)theMap.get("TESTNO")), 10) + "|";
                SprnBody += common.Cad(common.parseNull((String)theMap.get("UM")), 10) + "|";
                SprnBody += common.Cad(common.parseNull((String)theMap.get("CVM")), 10) + "|";
                SprnBody += common.Cad(common.parseNull((String)theMap.get("CVM1M")), 10) + "|";
                SprnBody += common.Cad(common.parseNull((String)theMap.get("CVM3M")), 10) + "|";
                SprnBody += common.Cad(common.parseNull((String)theMap.get("INDEXX")), 10) + "|";
                SprnBody += common.Cad(common.parseNull((String)theMap.get("THINMI30")), 10) + "|";
                SprnBody += common.Cad(common.parseNull((String)theMap.get("THINMI40")), 10) + "|";
                SprnBody += common.Cad(common.parseNull((String)theMap.get("THINMI50")), 10) + "|";
                SprnBody += common.Cad(common.parseNull((String)theMap.get("THICKPL35")), 10) + "|";
                SprnBody += common.Cad(common.parseNull((String)theMap.get("THICKPL50")), 10) + "|";
                SprnBody += common.Cad(common.parseNull((String)theMap.get("NEPSPL140")), 12) + "|";
                SprnBody += common.Cad(common.parseNull((String)theMap.get("NEPSPL200")), 12) + "|";
                SprnBody += common.Cad(common.parseNull((String)theMap.get("NEPSPL280")), 12) + "|\n";
                
                sprint.printData(SprnBody,(String) request.getAttribute("Fdate"),theBaseMap);
            }
            SprnBody2="";
            %>
            <tr>
                <td>Mean Value</td>
                <td><%=common.getRound(cev.getMean(UmArray), 2)%></td>
                <td><%=common.getRound(cev.getMean(CvmArray), 2)%></td>
                <td><%=common.getRound(cev.getMean(Cvm1Array), 2)%></td>
                <td><%=common.getRound(cev.getMean(Cvm3Array), 2)%></td>
                <td><%=common.getRound(cev.getMean(IndexArray), 2)%></td>
                <td><%=common.getRound(cev.getMean(Thin30Array), 2)%></td>
                <td><%=common.getRound(cev.getMean(Thin40Array), 2)%></td>
                <td><%=common.getRound(cev.getMean(Thin50Array), 2)%></td>
                <td><%=common.getRound(cev.getMean(Thin35Array), 2)%></td>
                <td><%=common.getRound(cev.getMean(ThinP50Array), 2)%></td>
                <td><%=common.getRound(cev.getMean(Neps140Array), 2)%></td>
                <td><%=common.getRound(cev.getMean(Neps200Array), 2)%></td>
                <td><%=common.getRound(cev.getMean(Neps280Array), 2)%></td>
            </tr>
            <tr>
                <td>CV(%)</td>
                <td><%=common.getRound(cev.getCoEffVant(UmArray), 2)%></td>
                <td><%=common.getRound(cev.getCoEffVant(CvmArray), 2)%></td>
                <td><%=common.getRound(cev.getCoEffVant(Cvm1Array), 2)%></td>
                <td><%=common.getRound(cev.getCoEffVant(Cvm3Array), 2)%></td>
                <td><%=common.getRound(cev.getCoEffVant(IndexArray), 2)%></td>
                <td><%=common.getRound(cev.getCoEffVant(Thin30Array), 2)%></td>
                <td><%=common.getRound(cev.getCoEffVant(Thin40Array), 2)%></td>
                <td><%=common.getRound(cev.getCoEffVant(Thin50Array), 2)%></td>
                <td><%=common.getRound(cev.getCoEffVant(Thin35Array), 2)%></td>
                <td><%=common.getRound(cev.getCoEffVant(ThinP50Array), 2)%></td>
                <td><%=common.getRound(cev.getCoEffVant(Neps140Array), 2)%></td>
                <td><%=common.getRound(cev.getCoEffVant(Neps200Array), 2)%></td>
                <td><%=common.getRound(cev.getCoEffVant(Neps280Array), 2)%></td>
            </tr>
            <tr>
                <td>Q95% +/-</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <%
        SprnBody2 += common.Pad("| " + "", 5) + "";
        SprnBody2 += common.Pad("Mean Value", 12) + "|";
        SprnBody2 += common.Cad(common.getRound(cev.getMean(UmArray), 2), 10) + "|";
        SprnBody2 += common.Cad(common.getRound(cev.getMean(CvmArray), 2), 10) + "|";
        SprnBody2 += common.Cad(common.getRound(cev.getMean(Cvm1Array), 2), 10) + "|";
        SprnBody2 += common.Cad(common.getRound(cev.getMean(Cvm3Array), 2), 10) + "|";
        SprnBody2 += common.Cad(common.getRound(cev.getMean(IndexArray), 2), 10) + "|";
        SprnBody2 += common.Cad(common.getRound(cev.getMean(Thin30Array), 2), 10) + "|";
        SprnBody2 += common.Cad(common.getRound(cev.getMean(Thin40Array), 2), 10) + "|";
        SprnBody2 += common.Cad(common.getRound(cev.getMean(Thin50Array), 2), 10) + "|";
        SprnBody2 += common.Cad(common.getRound(cev.getMean(Thin35Array), 2), 10) + "|";
        SprnBody2 += common.Cad(common.getRound(cev.getMean(ThinP50Array), 2), 10) + "|";
        SprnBody2 += common.Cad(common.getRound(cev.getMean(Neps140Array), 2), 12) + "|";
        SprnBody2 += common.Cad(common.getRound(cev.getMean(Neps200Array), 2), 12) + "|";
        SprnBody2 += common.Cad(common.getRound(cev.getMean(Neps280Array), 2), 12) + "|\n";
        
        SprnBody2 += common.Replicate("-",166)+"\n";
        
        SprnBody2 += common.Pad("| " + "", 5) + "";
        SprnBody2 += common.Pad("CV(%)", 12) + "|";
        SprnBody2 += common.Cad(common.getRound(cev.getCoEffVant(UmArray), 2), 10) + "|";
        SprnBody2 += common.Cad(common.getRound(cev.getCoEffVant(CvmArray), 2), 10) + "|";
        SprnBody2 += common.Cad(common.getRound(cev.getCoEffVant(Cvm1Array), 2), 10) + "|";
        SprnBody2 += common.Cad(common.getRound(cev.getCoEffVant(Cvm3Array), 2), 10) + "|";
        SprnBody2 += common.Cad(common.getRound(cev.getCoEffVant(IndexArray), 2), 10) + "|";
        SprnBody2 += common.Cad(common.getRound(cev.getCoEffVant(Thin30Array), 2), 10) + "|";
        SprnBody2 += common.Cad(common.getRound(cev.getCoEffVant(Thin40Array), 2), 10) + "|";
        SprnBody2 += common.Cad(common.getRound(cev.getCoEffVant(Thin50Array), 2), 10) + "|";
        SprnBody2 += common.Cad(common.getRound(cev.getCoEffVant(Thin35Array), 2), 10) + "|";
        SprnBody2 += common.Cad(common.getRound(cev.getCoEffVant(ThinP50Array), 2), 10) + "|";
        SprnBody2 += common.Cad(common.getRound(cev.getCoEffVant(Neps140Array), 2), 12) + "|";
        SprnBody2 += common.Cad(common.getRound(cev.getCoEffVant(Neps200Array), 2), 12) + "|";
        SprnBody2 += common.Cad(common.getRound(cev.getCoEffVant(Neps280Array), 2), 12) + "|\n";

        SprnBody2 += common.Replicate("-",166)+"\n";
        
        SprnBody2 += common.Pad("| " + "", 5) + "";
        SprnBody2 += common.Pad("Q95%", 12) + "|";
        SprnBody2 += common.Cad("", 10) + "|";
        SprnBody2 += common.Cad("", 10) + "|";
        SprnBody2 += common.Cad("", 10) + "|";
        SprnBody2 += common.Cad("", 10) + "|";
        SprnBody2 += common.Cad("", 10) + "|";
        SprnBody2 += common.Cad("", 10) + "|";
        SprnBody2 += common.Cad("", 10) + "|";
        SprnBody2 += common.Cad("", 10) + "|";
        SprnBody2 += common.Cad("", 10) + "|";
        SprnBody2 += common.Cad("", 10) + "|";
        SprnBody2 += common.Cad("", 12) + "|";
        SprnBody2 += common.Cad("", 12) + "|";
        SprnBody2 += common.Cad("", 12) + "|\n";

        sprint.printMean(SprnBody2);
        sprint.Closefile();
        }
        %>
</body>