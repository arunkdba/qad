<%-- 
    Document   : ajax_testno_mag
    Created on : Dec 15, 2017, 10:21:28 AM
    Author     : root
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="Common" class="com.common.Common"/>
<jsp:useBean id="MagSpinningWrappingTestData"  class="com.reports.data.MagSpinningWrappingTestData"/>
<%
        
        String SFromDate = Common.pureDate(request.getParameter("dateFrom"));
        String SToDate   = Common.pureDate(request.getParameter("dateTo"));
        String sOut = "";                
        
        java.util.List TestnoList    = new java.util.ArrayList();
        TestnoList = MagSpinningWrappingTestData.getTestNo(SToDate);
        for(int i=0; i<TestnoList.size(); i++) {
            String Testno = (String)TestnoList.get(i);            
            sOut +="<option value="+Testno+">"+Testno+"</option>";
        }        
        out.println(sOut);
%>