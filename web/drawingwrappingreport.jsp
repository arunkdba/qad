<%-- 
    Document   : drawingwrappingreport
    Created on : Apr 8, 2015, 4:34:07 PM
    Author     : admin
--%>

<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.drawing.wrapping.DrawingWrappingTestDto"%>
<%@page import="com.drawing.wrapping.DrawingWrappingTestDetails"%>
<%@page import="com.reports.pdf.DrawingWrappingTestPDF"%>
<%@page import="com.common.Common"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    com.reports.data.Units unit = new com.reports.data.Units();
    com.drawing.wrapping.DrawingWrappingTestDetails wrapping = new com.drawing.wrapping.DrawingWrappingTestDetails();
    java.util.List theList = unit.getUnit();
    java.util.List thePassList = wrapping.getPassLevel();
    request.setAttribute("theUnitList", theList);
    request.setAttribute("thePassList", thePassList);
    java.util.HashMap theMap =new java.util.HashMap();
    java.util.List Unitlist = new java.util.ArrayList();
    java.util.List Passlist = new java.util.ArrayList();
    String sMixNo="";
%>
<html>
    <head>
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="css/styles-menu.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.css" type="text/css">
        <script src="script/jquery-latest.min.js" type="text/javascript"></script>
        <script src="script/script-menu.js" type="text/javascript"></script>
        <script src="script/jquery-ui.js"></script>
        <script src="script/FormValidateDrawing.js" type="text/javascript"></script>
        <script type="text/javascript">
             function showAlert(){
                var msg = "PDF File Created in D:\DrawingWrappingTestPDF, PDF in Legal Sheet";
                alert(msg);
            }
        </script>
        
        
        <title>Drawing Hank Report</title>
    </head>
    <body>
        <%!
            Common common = new Common();
        %>
        <%@include file="header.jsp" %>
        <div class="elegant-aero-demo">

            <form action="DrawingWrappingReportServlet" method="post" class="elegant-aero">
                <h1>Drawing Wrapping Test<span>Please fill the texts in the fields.</span></h1>
                <span id="error"></span>
                <p>
                    <!--<label><span>Machine </span><select name="machine" id="machine" >
                            <option selected="selected" value="0">-SELECT-</option>
                    </select></label>-->
                    <label><span>Test From </span><input id="tdatefrom" name="tdatefrom" placeholder="FromDate" type="text" class="sdate"></label>
                    <label><span>Test To </span><input id="tdateto" name="tdateto" placeholder="ToDate" type="text" class="sdate"></label>
                    
                    <label><span>Order No</span><input id="orderno" name="orderno" placeholder="Valid Order No" type="text"></label>
                    <label>
                        <span>Unit</span><select name="sunits" id="sunits">
                            <%
                                Unitlist = (java.util.List) request.getAttribute("theUnitList");
                                for(int i=0;i<Unitlist.size();i++) {
                                    theMap     =  (HashMap)Unitlist.get(i);%>
                            <option value="<%=(String)theMap.get("UNITCODE")%>"><%=(String)theMap.get("UNITNAME")%></option>
                            <%}%>
                        </select></label>
                    <label>
                            <span>&nbsp;</span>
                            <input type="checkbox" id="ptype" name="ptype" value="43"/>OE
                            <input type="checkbox" id="yarntype" name="yarntype" value="4"/>Grindle Yarn
                    </label>
                        <label><span>Machine </span><select name="machine" id="machine" class="machine">
                                <!-- Load Machine Using Ajax -->
                        </select></label>
                        <label>
                        <span>Passage</span><select name="spass" id="spass">
                            <option value="0" >All</option>
                            <%
                                Passlist = (java.util.List) request.getAttribute("thePassList");
                                for(int i=0;i<Passlist.size();i++) {
                                    theMap     =  (HashMap)Passlist.get(i);%>
                            <option value="<%=(String)theMap.get("ID")%>"><%=(String)theMap.get("PROCESSNAME")%></option>
                            <% }%>
                        </select></label>
                        <label>
                        <span>Shift</span><select name="sshift" id="sshift">
                            <option value="0" >All</option>
                            <option value="1" >Shift - I</option>
                            <option value="2" >Shift - II</option>
                            <option value="3" >Shift - III</option>
                        </select></label>
                    <label><span>&nbsp;</span><input class="button" value="Submit" type="submit" id="btnSubmit" name="btnSubmit" onClick="showAlert()">
                        <input class="button" value="Reset" type="reset" id="btnreset" name="btnreset">
                    </label>
                </p>
            </form>
        </div>
        <br/><br/>
        <%
            if (request.getAttribute("theDrawingWrappingTestDetails") != null) {
                java.util.List TBase = new java.util.ArrayList();
                               TBase = (java.util.List) request.getAttribute("theDrawingWrappingTestDetails");
                               
                               DrawingWrappingTestPDF   spdf   = new DrawingWrappingTestPDF();                     
                               spdf.createPDFFile();
                               spdf.setTestBase((String) request.getAttribute("Fdate"),(String) request.getAttribute("Tdate"),(String)request.getAttribute("UnitName"),(String)request.getAttribute("Shift"));
                               spdf.setHead();
                               spdf.printPDFData(TBase); 
        %>
        <h4 style="margin:0px;">Drawing Wrapping  : From  <%=common.parseDate((String) request.getAttribute("Fdate"))%> To <%=common.parseDate((String) request.getAttribute("Tdate"))%></h4>
        <h4 style="margin:0px;">Unit : <%=request.getAttribute("UnitName")%></h4>
        <h4 style="margin:0px;">Shift : <%=request.getAttribute("Shift")%></h4>
        <table width="100%" border="1" class="spinningwrapping">
            <tr>
                <th width="42" align="center" valign="top" scope="col">S.No</th>
                <th width="43" scope="col" align="center" valign="top">Test No</th>
                <th width="43" scope="col" align="center" valign="top">Machine</th>
                <th width="40" scope="col" align="center" valign="top">Passage</th>
                <th width="40" scope="col" align="center" valign="top">Order No</th>
                <th width="40" scope="col" align="center" valign="top">Order Weight</th>
                <th width="40" scope="col" align="center" valign="top">Shade</th>
                <th width="40" scope="col" align="center" valign="top">Count</th>
                <th width="40" scope="col" align="center" valign="top">Standard Hank</th>
                <th width="82" scope="col" align="center" valign="top">1</th>
                <th width="96" scope="col" align="center" valign="top">2</th>
                <th width="92" scope="col" align="center" valign="top">3</th>
                <th width="76" scope="col" align="center" valign="top">Avg Value </th>
                <th width="91" scope="col" align="center" valign="top">R.S.B</th>
                <th width="92" scope="col" align="center" valign="top">C.P. Wheel</th>
                <th width="92" scope="col" align="center" valign="top">Draft</th>
                <th width="92" scope="col" align="center" valign="top">No.Of.Ends</th>
                <th width="68" scope="col" align="center" valign="top">Time</th>
                <th width="49" scope="col" align="center" valign="top">Changes</th>
                <th width="49" scope="col" align="center" valign="top">Remark</th>
            </tr>
            <%
                SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
                String sStartTime ="",sEndTime="";
                String spreviousTime = "";
                for (int i = 0; i < TBase.size(); i++) 
                {
                    DrawingWrappingTestDto drawingWrappingTestDto = (DrawingWrappingTestDto)TBase.get(i);
                    
                    if(i==0){

                        sStartTime = drawingWrappingTestDto.getsEntryTime();
                        sEndTime = drawingWrappingTestDto.getsEntryTime();

                    }else{
                        //sStartTime = spreviousTime;
                        sEndTime = drawingWrappingTestDto.getsEntryTime();
                    }
                    Date d1 = null;
                    Date d2 = null;

                    d1 = format.parse(sStartTime);
                    d2 = format.parse(sEndTime);

                    long diff = d2.getTime() - d1.getTime();

                    long diffHours = diff / (60 * 60 * 1000) % 24;
                    long diffMinutes = diff / (60 * 1000) % 60;

                    //System.out.println("sStartTime-->"+sStartTime);
                    //System.out.println("sEndTime-->"+sEndTime);
                    
                    //System.out.println("diffHours : "+diffHours);
                    //System.out.println("diffMinutes : "+diffMinutes);

            %>
            <tr>
                <td><%=drawingWrappingTestDto.getsSino()%></td>
                <td><%=drawingWrappingTestDto.getsTestno()%></td>
                <td><%=drawingWrappingTestDto.getSmach_name()%></td>
                <td><%=drawingWrappingTestDto.getsProcessLevel()%></td>
                <td><%=drawingWrappingTestDto.getsOrderNo()%>(<%=drawingWrappingTestDto.getsMixno()%>)</td>
                <td><%=drawingWrappingTestDto.getsOrderWeight()%></td>
                <td><%=drawingWrappingTestDto.getsShade()%></td>
                <td><%=drawingWrappingTestDto.getsCountName()%></td>
                <td><%=drawingWrappingTestDto.getsStdHank()%></td>
                <td><%=drawingWrappingTestDto.getsHank1()%></td>
                <td><%=drawingWrappingTestDto.getsHank2()%></td>
                <td><%=drawingWrappingTestDto.getsHank3()%></td>
                <td><%=drawingWrappingTestDto.getsHankAvg()%></td>
                <td><%=drawingWrappingTestDto.getsRsb()%></td>
                <td><%=drawingWrappingTestDto.getsCpWheel()%></td>
                <td><%=drawingWrappingTestDto.getsDraft()%></td>
                <td><%=drawingWrappingTestDto.getsEndno()%></td>
                <td><%=drawingWrappingTestDto.getSentrydate()%></td>
                <td><%=drawingWrappingTestDto.getSstatus()%></td>
                <td><%=drawingWrappingTestDto.getsRemark()%></td>
            </tr>
            <%
                if(diffHours >= 2)
                {
                    sStartTime = sEndTime;
                %>

                <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>

               <%}%>
            <%
                spreviousTime = drawingWrappingTestDto.getsEntryTime();
                }
            %>
        </table>
        <%}%>
</body>
</html>
