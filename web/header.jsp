<%--
    Document   : header
    Created on : Oct 29, 2014, 3:53:37 PM
    Author     : admin
--%>
        <div id='cssmenu'>
            <ul>
                <!--<li><a href='#'><span>Home</span></a></li>-->
                <li class='has-sub'><a href='#'><span>Spinning Wrapping</span></a>
                    <ul>
                        <li class='last'><a href='SpinningWrappingTest.jsp'><span>Spinning Wrapping Test Report</span></a>
                            <!--<li class='last'><a href='SpinningWrappingAbstract.jsp'><span>Spinning Wrapping Abstract</span></a>-->
                            <li class='last'><a href='MachineReport.jsp'><span>Machine Wise Test</span></a>
                            <!--<li class='last'><a href='MachineWiseReport.jsp'><span>Machine Wise Report</span></a>-->
                            <li class='last'><a href='CVReport.jsp'><span>CV Report</span></a>
                            <li class='last'><a href='LowCspReport.jsp'><span>Low Csp Report</span></a>
                            <li class='last'><a href='magSpinningWrappingTest.jsp'><span>Spinning Wrapping Test(Mag)</span></a>
                            <li class='last'><a href='magSpinningWrappingTestabstract.jsp'><span>Spinning Wrapping Abstract(Mag)</span></a>
                            
                            <!--<ul>
                               <li><a href='#'><span>Sub Product</span></a></li>
                               <li class='last'><a href='#'><span>Sub Product</span></a></li>
                            </ul>-->
                        </li>
                        <!--<li class='has-sub'><a href='#'><span>Product 2</span></a>
                           <ul>
                              <li><a href='#'><span>Sub Product</span></a></li>
                              <li class='last'><a href='#'><span>Sub Product</span></a></li>
                           </ul>
                        </li>-->
                    </ul>
                </li>
                <li class='has-sub'><a href='#'><span>UT3 Reports</span></a>
                    <ul>
                        <li class='last'><a href='Ut3Aunit.jsp'><span>UT3 A-Unit</span></a></li>
                        <li class='last'><a href='Ut3Bunit.jsp'><span>UT3 B-Unit</span></a></li>
                        <li class='last'><a href='Ut3Cunit.jsp'><span>UT3 C-Unit</span></a></li>
                        <li class='last'><a href='prepcomfull.jsp'><span>UT3-RKM-CSP</span></a></li>
                        <li class='last'><a href='ut3rkmcsp.jsp'><span>UT3-RKM-CSP(New)</span></a></li>
                        <li class='last'><a href='Ut3AunitMin.jsp'><span>UT3-A-Unit(Min & Max)</span></a></li>
                        <li class='last'><a href='Ut3BunitMin.jsp'><span>UT3-B-Unit(Min & Max)</span></a></li>
                        <li class='last'><a href='Ut3CunitMin.jsp'><span>UT3-C-Unit(Min & Max)</span></a></li>

                        <li class='last'><a href='runningnorms.jsp'><span>Running Norms</span></a></li>
                        <li class='last'><a href='Ut3TestWise.jsp'><span>Ut3 Test</span></a></li>
                        
                        
                        <!--<li class='last'><a href='#'><span>---------------------</span></a></li>
                        <li class='last'><a href='OrderWiseUt3Bunit.jsp'><span>UT3-BUnit</span></a></li>-->
                    </ul>
                </li>
                <li class='has-sub'><a href='#'><span>Reports</span></a>
                    <ul>
                        <li class='last'><a href='ut3crit.jsp'><span>UT3 Report</span></a></li>
                        <li class='last'><a href='ut3critnew.jsp'><span>UT3 Report-New</span></a></li>
                        <li class='last'><a href='rkmcrit.jsp'><span>RKM Report</span></a></li>
                        <li class='last'><a href='rkmcritnew.jsp'><span>RKM Report-New</span></a></li>
                        <li class='last'><a href='ErrorMessageReport.jsp'><span>Error Message Report</span></a></li>

                    </ul>
                </li>
                <li class='has-sub'><a href='#'><span>Wrapping Reports</span></a>
                    <ul>
                        <li class='last'><a href='cardingwrappingreport.jsp'><span>Carding Wrapping</span></a></li>
                        <li class='last'><a href='drawingwrappingreport.jsp'><span>Drawing Wrapping </span></a></li>
                        <li class='last'><a href='simplexwrappingreport.jsp'><span>Simplex Wrapping </span></a></li>
                    </ul>
                </li>
                <li class='has-sub'><a href='#'><span>Twist Reports</span></a>
                    <ul>
                        <li class='last'><a href='twisttpi.jsp'><span>Twist Tpi Report</span></a></li>                        
                    </ul>
                </li>
            </ul>
        </div>