<%--
    Document   : index
    Created on : Oct 29, 2014, 3:35:22 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Quality Control Reports</title>
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="css/styles-menu.css" type="text/css">
        <script src="script/jquery-latest.min.js" type="text/javascript"></script>
        <script src="script/script-menu.js" type="text/javascript"></script>
        <script src="script/FormValidate.js" type="text/javascript"></script>
   </head>
    <body>
    <%@include file="header.jsp" %>
   </body>
</html>