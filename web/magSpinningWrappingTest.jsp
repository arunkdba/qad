<%--
    Document   : magSpinningWrappingTest
    Created on : Dec 14, 2017, 1:11:10 PM
    Author     : root
--%>

<%@page import="com.reports.data.MagSpinningWrappingTestData"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.reports.print.MagSpinningWrapingTestPrint"%>
<%@page import="com.reports.pdf.MagSpinningWrapingTestPDF"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.common.Common"%>

<!DOCTYPE html>

<html>
    <head>
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="css/styles-menu.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.css" type="text/css">
        <script src="script/jquery-latest.min.js" type="text/javascript"></script>
        <script src="script/script-menu.js" type="text/javascript"></script>
        <script src="script/jquery-ui.js"></script>
        <script src="script/spinningwrappingmag.js" type="text/javascript"></script>
        <title>(Mag) Spinning Wrapping Test</title>
        <script>
            $(document).ready(function(){
                //$('tr:not(:has(td[rowspan])):even').addClass('oddrow');
            });
        </script>
        
        <script type="text/javascript">
             function showAlert(){
                var msg = "PDF File Created in D:\MagSpinningTest, PDF in A4 Sheet";
                alert(msg);
            }
        </script>
        
    </head>
    <body>
        <%!
            java.util.List Testlist = new java.util.ArrayList();
            Common common = new Common();
            String SprnBody = "",SprnBody2="";
            double dnominalCnt=0,dnominalStr=0;
            double dprocess =0;
            String SProcess="",SRh="";
        %>
        <%@include file="header.jsp" %>
        <div class="elegant-aero-demo">
            <form action="MagSpinningWrapingTestReport" method="post" class="elegant-aero">
                <h1>Spinning Wrapping Test (Mag)<span>Please fill the texts in the fields.</span></h1>
                <span id="error"></span>
                 <p>
                    <!--<label><span>Test As  </span><input id="tdatefrom" name="tdatefrom" placeholder="FromDate" type="text" class="sdate"/></label>-->
                    <label><span>As On Date</span><input id="tdateto" name="tdateto" placeholder="ToDate" type="text" class="sdate"/></label>
                    <label><span>Test No </span><select name="testno" id="testno" class="testno">
                    </select></label>
                    <label><span>&nbsp;</span><input class="button" value="Submit" type="submit" id="btnSubmit" name="btnSubmit"/>
                    <input class="button" value="Reset" type="reset" id="btnreset" name="btnreset"/>
                    </label>
                </p>
            </form>
        </div>
        <br><br>
        <%
            if (request.getAttribute("theTestDetailsList") != null) {
                java.util.List TBase = new java.util.ArrayList();
                TBase = (java.util.List) request.getAttribute("theTestBase");
                java.util.HashMap theBaseMap        = (java.util.HashMap)TBase.get(0);
                MagSpinningWrapingTestPrint sprint  = new MagSpinningWrapingTestPrint();
                MagSpinningWrapingTestPDF spdf      = new MagSpinningWrapingTestPDF();
                
                sprint.createPrn();
                sprint.setTestBase((String) request.getAttribute("Fdate"), (String) request.getAttribute("Tdate"), theBaseMap);
                sprint.setHead((String) request.getAttribute("Fdate"));
                
                spdf.createPDFFile();
                spdf.setTestBase((String) request.getAttribute("Fdate"),TBase);
                spdf.setHead();

        %>

        <table width="100%" border="1" class="spinningwrapping">
            <tr>
                <td width="183">Dry : <%=common.parseNull((String)theBaseMap.get("DBT"))%> F </td>
                <td width="236">Wet :  <%=common.parseNull((String)theBaseMap.get("WBT"))%> F </td>
                <td width="209">RH  :  <%=common.parseNull((String)theBaseMap.get("RH"))%></td>
                <td width="183">Test Date : <%=common.parseDate(common.parseNull((String)theBaseMap.get("ENTRYDATE")))%></td>
            </tr>
            <tr>
                <td>Test ID : <%=common.parseNull((String)theBaseMap.get("TESTID"))%></td>
                <td>No.Of.Test : <%=common.parseNull((String)theBaseMap.get("TEST"))%></td>
                <td>Count Type : <%=common.parseNull((String)theBaseMap.get("COUNTTYPE"))%> </td>
                <td>Nom.Strength : <%=common.parseNull((String)theBaseMap.get("NOMSTRENGTH"))%></td>
            </tr>
            <tr>
                <td>Dept : <%=common.parseNull((String)theBaseMap.get("DEPARTMENT"))%></td>
                <td>Lot No : <%=common.parseNull((String)theBaseMap.get("MACHINE_NAME"))%> </td>
                <td>Nom.Count : <%=common.parseNull((String)theBaseMap.get("NOMCOUNT"))%> </td>
                <td>Sam.Length : <%=common.parseNull((String)theBaseMap.get("SAMPLE_LENGTH"))%> yard</td>
            </tr>
            <tr>
                <td>Shift :  <%=common.parseNull((String)theBaseMap.get("SHIFT"))%></td>
                <td>Operator : <%=common.parseNull((String)theBaseMap.get("OPERATOR"))%></td>
                <td>Machine : <%=common.parseNull((String)theBaseMap.get("FRAME"))%></td>
                <td>Time : <%=common.parseNull((String)theBaseMap.get("TIME"))%></td>
            </tr>
        </table>
        <table width="100%" border="1" class="spinningwrapping">
            <tr>
                
                <th width="40" align="center" valign="top" scope="col">Test No</th>
                <th width="82" scope="col" align="center" valign="top">Weight(g)</th>
                <th width="96" scope="col" align="center" valign="top">Count(Nec)</th>
                <th width="96" scope="col" align="center" valign="top">RH Count(Nec)</th>
                <th width="92" scope="col" align="center" valign="top">Strength(lbf)</th>
                <th width="89" scope="col" align="center" valign="top">CSP</th>
                <th width="89" scope="col" align="center" valign="top">RH CSP</th>                
            </tr>
            <%
                Testlist = (java.util.List) request.getAttribute("theTestDetailsList");
                double dweightTotal = 0,dweightCnt=0;
                for (int i = 0; i < Testlist.size(); i++) {
                    SprnBody = "";
                    java.util.HashMap theMap = (java.util.HashMap)Testlist.get(i);
                    dweightTotal += common.toDouble(common.parseNull((String)theMap.get("WEIGHT")));
                    if(dweightTotal>0){
                        dweightCnt +=1;
                    }
             %>
            <tr>
                <td><%=common.parseNull((String)theMap.get("TEST"))%></td>
                <td><%=common.parseNull(common.getRound((String)theMap.get("WEIGHT"),3))%></td>
                <td><%=common.parseNull(common.getRound((String)theMap.get("COUNT"),3))%></td>
                <td><%=common.parseNull(common.getRound((String)theMap.get("CORRCOUNT"),3))%></td>
                <td><%=common.parseNull(common.getRound((String)theMap.get("STRENGTH"),2))%></td>
                <td><%=common.parseNull(common.getRound((String)theMap.get("CSP"),0))%></td>
                <td><%=common.parseNull((String)theMap.get("CORRCSP"))%></td>
            </tr>
               <%
                    SprnBody += common.Pad(" "+common.parseNull((String)theMap.get("TEST")), 10) + "|";
                    SprnBody += common.Rad(" "+common.parseNull(common.getRound((String)theMap.get("WEIGHT"),3)), 17) + " |";
                    SprnBody += common.Rad(" "+common.parseNull(common.getRound((String)theMap.get("COUNT"),3)), 17) + " |";
                    SprnBody += common.Rad(" "+common.parseNull(common.getRound((String)theMap.get("CORRCOUNT"),3)), 17) + " |";
                    SprnBody += common.Rad(" "+common.parseNull(common.getRound((String)theMap.get("STRENGTH"),2)), 17) + " |";
                    SprnBody += common.Rad(" "+common.parseNull(common.getRound((String)theMap.get("CSP"),0)), 17) + " |";
                    SprnBody += common.Rad(" "+common.parseNull((String)theMap.get("CORRCSP")), 17) + " \n";
                    
                    sprint.printData(SprnBody,(String) request.getAttribute("Fdate"),theBaseMap);
                }
                    spdf.printData(Testlist,TBase);

                SprnBody2="";
                %>
               <tr>
                   <td colspan="7">&nbsp;</td>
               </tr>
               <tr>
                   <td>Average</td>
                   <td><%=common.getRound((dweightTotal/dweightCnt),3)%></td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("COUNTAVG"),3))%></td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("RHCOUNTAVG"),3))%></td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("STRAVG"),2))%></td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("CSPAVG"),0))%></td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("RHCSPAVG"),0))%></td>
               </tr>
               <tr>
                   <td>CV%</td>
                   <td>&nbsp;</td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("COUNTCV"),2))%></td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("RHCOUNTCV"),2))%></td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("STRCV"),2))%></td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("CSPCV"),2))%></td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("RHCSPCV"),2))%></td>
               </tr>
               <tr>
                   <td>Min</td>
                   <td>&nbsp;</td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("COUNTMIN"),3))%></td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("RHCOUNTMIN"),3))%></td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("STRMIN"),2))%></td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("CSPMIN"),0))%></td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("RHCSPMIN"),0))%></td>
               </tr>
               <tr>
                   <td>Max</td>
                   <td>&nbsp;</td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("COUNTMAX"),3))%></td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("RHCOUNTMAX"),3))%></td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("STRMAX"),2))%></td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("CSPMAX"),0))%></td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("RHCSPMAX"),0))%></td>
               </tr>
               <tr>
                   <td>Range</td>
                   <td>&nbsp;</td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("COUNTRANGE"),3))%></td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("RHCOUNTRANGE"),3))%></td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("STRRANGE"),2))%></td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("CSPRANGE"),0))%></td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("RHCSPRANGE"),0))%></td>
               </tr>
               <tr>
                   <td>SD</td>
                   <td>&nbsp;</td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("COUNTSD"),2))%></td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("RHCOUNTSD"),2))%></td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("STRSD"),2))%></td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("CSPSD"),2))%></td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("RHCSPSD"),2))%></td>
               </tr>
               <tr>
                   <td>Q95(+/-)</td>
                   <td>&nbsp;</td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("COUNTQ95"),2))%></td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("RHCOUNTQ95"),2))%></td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("STRQ95"),2))%></td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("CSPQ95"),2))%></td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("RHCSPQ95"),2))%></td>
               </tr>
               <tr>
                   <td>Q99(+/-)</td>
                   <td>&nbsp;</td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("COUNTQ99"),2))%></td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("RHCOUNTQ99"),2))%></td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("STRQ99"),2))%></td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("CSPQ99"),2))%></td>
                   <td><%=common.parseNull(common.getRound((String)theBaseMap.get("RHCSPQ99"),2))%></td>
               </tr>
               <tr>
                   <td colspan="3">RKM Value : <%=common.parseNull((String)theBaseMap.get("RKMVAL"))%></td>
                   <td colspan="4">RH RKM Value : <%=common.parseNull((String)theBaseMap.get("ARKMVAL"))%></td>
               </tr>
               
               <tr>
                   <td colspan="7">Remarks : <%=common.parseNull((String)theBaseMap.get("REMARKS"))%></td>                   
               </tr>
        </table>
            <%
                SprnBody2 += common.Pad(" Average", 10) + "|";
                SprnBody2 += common.Rad(" "+common.getRound((dweightTotal/dweightCnt),3), 17)+ " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("COUNTAVG"),3)), 17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("RHCOUNTAVG"),3)), 17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("STRAVG"),2)),17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("CSPAVG"),0)),17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("RHCSPAVG"),0)),17) + " \n";

                SprnBody2 += common.Pad(" CV%", 10) + "|";
                SprnBody2 += common.Rad(" ",17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("COUNTCV"),2)),17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("RHCOUNTCV"),2)),17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("STRCV"),2)),17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("CSPCV"),2)),17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("RHCSPCV"),2)),17) + " \n";
                
                SprnBody2 += common.Pad(" MIN", 10) + "|";
                SprnBody2 += common.Rad(" ",17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("COUNTMIN"),3)),17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("RHCOUNTMIN"),3)),17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("STRMIN"),2)),17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("CSPMIN"),0)),17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("RHCSPMIN"),0)),17) + " \n";
                
                SprnBody2 += common.Pad(" MAX", 10) + "|";
                SprnBody2 += common.Rad(" ",17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("COUNTMAX"),3)),17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("RHCOUNTMAX"),3)),17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("STRMAX"),2)),17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("CSPMAX"),0)),17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("RHCSPMAX"),0)),17) + " \n";

                SprnBody2 += common.Pad(" Range", 10) + "|";
                SprnBody2 += common.Rad(" ",17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("COUNTRANGE"),3)),17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("RHCOUNTRANGE"),3)),17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("STRRANGE"),2)),17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("CSPRANGE"),0)),17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("RHCSPRANGE"),0)),17) + " \n";
                
                SprnBody2 += common.Pad(" SD", 10) + "|";
                SprnBody2 += common.Rad(" ",17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("COUNTSD"),2)),17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("RHCOUNTSD"),2)),17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("STRSD"),2)),17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("CSPSD"),2)),17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("RHCSPSD"),2)),17) + " \n";

                SprnBody2 += common.Pad(" Q95(+/-)", 10) + "|";
                SprnBody2 += common.Rad(" ",17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("COUNTQ95"),2)),17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("RHCOUNTQ95"),2)),17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("STRQ95"),2)),17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("CSPQ95"),2)),17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("RHCSPQ95"),2)),17) + " \n";

                SprnBody2 += common.Pad(" Q99(+/-)", 10) + "|";
                SprnBody2 += common.Rad(" ",17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("COUNTQ99"),2)),17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("RHCOUNTQ99"),2)),17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("STRQ99"),2)),17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("CSPQ99"),2)),17) + " |";
                SprnBody2 += common.Rad(" "+common.parseNull(common.getRound((String)theBaseMap.get("RHCSPQ99"),2)),17) + " \n";

                SprnBody2 += common.Replicate("-",125)+"\n";

                SprnBody2 += common.Pad(" RKM Value : "+common.parseNull((String)theBaseMap.get("RKMVAL")), 50) +  "|";
                SprnBody2 += common.Pad(" RH RKM Value : "+common.parseNull((String)theBaseMap.get("ARKMVAL")), 55) +  "\n";

                SprnBody2 += common.Replicate("-",125)+"\n";

                SprnBody2 += common.Pad(" Remarks Value : "+common.parseNull((String)theBaseMap.get("REMARKS")), 106) +  "\n";

                SprnBody2 += common.Replicate("-",125)+"\n";

                sprint.printMean(SprnBody2);

                sprint.Closefile();
            }%>
</body>