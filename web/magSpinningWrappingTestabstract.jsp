<%-- 
    Document   : magSpinningWrappingTestabstract
    Created on : Dec 18, 2017, 3:29:45 PM
    Author     : root
--%>

<%@page import="com.reports.print.MagSpinningWrapingTestAbstractPrint"%>
<%@page import="com.reports.pdf.MagSpinningWrapingTestAbstractPDF"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.reports.print.MagSpinningWrapingTestPrint"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.common.Common"%>

<!DOCTYPE html>

<html>
    <head>
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="css/styles-menu.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.css" type="text/css">
        <script src="script/jquery-latest.min.js" type="text/javascript"></script>
        <script src="script/script-menu.js" type="text/javascript"></script>
        <script src="script/jquery-ui.js"></script>
        <title>(Mag) Spinning Wrapping Test</title>
        <script>
            $(document).ready(function()
            {
                $('#tdateto').datepicker({ dateFormat: 'dd.mm.yy' });
                $('#btnSubmit').click(function(e)
                {
                    var isValid = true;
                    var validdate = true;
                    var re = "^(3[01]|[12][0-9]|0[1-9]).(1[0-2]|0[1-9]).[0-9]{4}$";
                    $('input[name="tdateto"],input[name="tdateto"]').each(function()
                    {
                        if ($.trim($(this).val()) == '')
                        {
                            isValid = false;
                            $(this).css({
                                "border": "1px solid red",
                                "background": "#FFCECE"
                            });
                            $("#error").fadeOut("slow", function()
                            {
                                $("#error").html("Enter Date Values").fadeIn("fast");
                            });
                        }
                        else
                        {
                            $(this).css({
                                "border": "",
                                "background": ""
                            });
                            $("#error").fadeOut(1000);
                        }
                        if(!$.trim($(this).val().match(re)))
                        {
                            validdate = false;
                            //alert("Not Valid");
                        }
                    });
                    if (isValid == false )
                    {
                        e.preventDefault();
                    }
                    else if (validdate == false)
                    {
                        e.preventDefault();
                        $("#error").fadeOut("fast", function()
                        {
                            $("#error").html("Date is not valid Format").fadeIn("fast");
                        });
                    }
                    else
                    {
                        $("#error").fadeIn("fast", function()
                        {

                        });
                    }
                });
            });
        </script>
        
        <script type="text/javascript">
             function showAlert(){
                var msg = "PDF File Created in D:\MagSpinningTestAbstract, PDF in A4 Sheet";
                alert(msg);
            }
        </script>
        
    </head>
    <body>
        <%!            
            java.util.List Testlist = new java.util.ArrayList();
            Common common = new Common();
            String SprnBody = "", SprnBody2 = "";
        %>
        <%@include file="header.jsp" %>
        <div class="elegant-aero-demo">
            <form action="MagSpinningWrapingTestReport" method="post" class="elegant-aero">
                <h1>Spinning Wrapping Abstract (Mag)<span>Please fill the texts in the fields.</span></h1>
                <span id="error"></span>
                <p>                    
                    <label><span>As On Date</span><input id="tdateto" name="tdateto" placeholder="ToDate" type="text" class="sdate"/></label>                    
                    <label><span>&nbsp;</span><input class="button" value="Submit" type="submit" id="btnSubmit" name="btnSubmit" onClick="showAlert()"/>
                        <input class="button" value="Reset" type="reset" id="btnreset" name="btnreset"/>
                    </label>
                </p>
                <input type="hidden" name="abst" value="1"/>
            </form>
        </div>
        <br><br>
        <%
            if (request.getAttribute("theTestDetailsList") != null) {
                MagSpinningWrapingTestAbstractPrint sprint = new MagSpinningWrapingTestAbstractPrint();
                sprint.createPrn();
                sprint.setHead((String) request.getAttribute("Fdate"));
                
                MagSpinningWrapingTestAbstractPDF  spdf    = new MagSpinningWrapingTestAbstractPDF();
                spdf.createPDFFile();
                spdf.setTestBase((String) request.getAttribute("Fdate"));
                spdf.setHead();

        %>
        <h2>Spinning Wrapping Test (Mag)</h2>
        <table width="100%" border="1" class="spinningwrapping">
            <tr>

                <th width="40" align="center" valign="top" scope="col">Test No</th>
                <th width="82" scope="col" align="center" valign="top">Entry Date</th>
                <th width="96" scope="col" align="center" valign="top">Shift</th>
                <th width="96" scope="col" align="center" valign="top">Nom Count</th>
                <th width="92" scope="col" align="center" valign="top">OrderNo</th>
                <th width="89" scope="col" align="center" valign="top">Machine</th>
                <th width="89" scope="col" align="center" valign="top">TPI</th>
                <th width="89" scope="col" align="center" valign="top">Draft</th>
                <th width="89" scope="col" align="center" valign="top">Cont</th>
                <th width="89" scope="col" align="center" valign="top">Strength</th>
                <th width="89" scope="col" align="center" valign="top">Csp</th>
                <th width="89" scope="col" align="center" valign="top">Count CV</th>
                <th width="89" scope="col" align="center" valign="top">Count Range</th>
                <th width="89" scope="col" align="center" valign="top">Rkm Value</th>
                <th width="89" scope="col" align="center" valign="top">Operator</th>
            </tr>
            <%
                Testlist = (java.util.List) request.getAttribute("theTestDetailsList");
                System.out.println("Test List Size-->"+Testlist.size());
                for (int i = 0; i < Testlist.size(); i++) {
                    SprnBody = "";
                    java.util.HashMap theMap = (java.util.HashMap) Testlist.get(i);
            %>
            <tr>
                <td><%=common.parseNull((String) theMap.get("TESTID"))%></td>
                <td><%=common.parseNull(common.parseDate((String) theMap.get("ENTRYDATE")))%></td>
                <td><%=common.parseNull((String) theMap.get("SHIFT"))%></td>
                <td><%=common.parseNull((String) theMap.get("NOMCOUNT"))%></td>
                <td><%=common.parseNull((String) theMap.get("MACHINE_NAME"))%></td>
                <td><%=common.parseNull((String) theMap.get("FRAME"))%></td>
                <td><%=common.parseNull(common.getRound((String) theMap.get("TPI"),3))%></td>
                <td><%=common.parseNull(common.getRound((String) theMap.get("DRAFT"),3))%></td>
                <td><%=common.parseNull(common.getRound((String) theMap.get("COUNTAVG"),3))%></td>
                <td><%=common.parseNull(common.getRound((String) theMap.get("STRAVG"),3))%></td>
                <td><%=common.parseNull(common.getRound((String) theMap.get("CSPAVG"),0))%></td>
                <td><%=common.parseNull((String) theMap.get("COUNTCV"))%></td>
                <td><%=common.parseNull((String) theMap.get("COUNTRANGE"))%></td>
                <td><%=common.parseNull((String) theMap.get("RKMVAL"))%></td>
                <td><%=common.parseNull((String) theMap.get("OPERATOR"))%></td>
                
            </tr>
            <%
                    SprnBody += common.Pad(common.parseNull((String) theMap.get("TESTID")), 10) + "|";
                    SprnBody += common.Pad(common.parseNull(common.parseDate((String) theMap.get("ENTRYDATE"))), 10) + "|";
                    SprnBody += common.Cad(common.parseNull((String) theMap.get("SHIFT")), 5) + "|";
                    SprnBody += common.Cad(common.parseNull((String) theMap.get("NOMCOUNT")), 7) + "|";
                    SprnBody += common.Pad(" "+common.parseNull((String) theMap.get("MACHINE_NAME")), 11) + " |";
                    SprnBody += common.Pad(" "+common.parseNull((String) theMap.get("FRAME")), 7) + "|";
                    SprnBody += common.Rad(common.parseNull(common.getRound((String) theMap.get("TPI"),3)), 7) + " |";
                    SprnBody += common.Rad(common.parseNull(common.getRound((String) theMap.get("DRAFT"),3)), 5) + " |";
                    SprnBody += common.Rad(common.parseNull(common.getRound((String) theMap.get("COUNTAVG"),3)), 8) + "|";
                    SprnBody += common.Rad(common.parseNull(common.getRound((String) theMap.get("STRAVG"),3)), 8) + "|";
                    SprnBody += common.Rad(common.parseNull(common.getRound((String) theMap.get("CSPAVG"),0)), 6) + "|";                    
                    SprnBody += common.Rad(common.parseNull((String) theMap.get("COUNTCV")), 6) + "|";
                    SprnBody += common.Rad(common.parseNull((String) theMap.get("COUNTRANGE")), 6) + "|";
                    SprnBody += common.Rad(common.parseNull((String) theMap.get("RKMVAL")), 6) + "|";
                    SprnBody += common.Pad(common.parseNull((String) theMap.get("OPERATOR")), 10) + "\n";

                    sprint.printData(SprnBody, (String) request.getAttribute("Fdate"));
                }
                    spdf.printData(Testlist);
                SprnBody2 = "";
            %>
            
        </table>
        <%
                sprint.Closefile();
        }%>
    </body>