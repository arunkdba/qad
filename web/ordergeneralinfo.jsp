<%-- 
    Document   : ordergeneralinfo
    Created on : Feb 7, 2013, 12:11:04 PM
    Author     : root
--%>

<%
           if (session.getAttribute("sorderno") != null) {
               
                String rorderno = session.getAttribute("sorderno").toString();
                
                com.common.Common com = new com.common.Common();
                int iUnitCode = com.toInt(session.getAttribute("Unit").toString());
                
                System.out.println("rorderno-->"+rorderno);
                System.out.println("UnitCode-->"+iUnitCode);
                
                com.prepcom.setting.OrderGeneralDetails        orderGeneralDetails = new com.prepcom.setting.OrderGeneralDetails();
                com.prepcom.setting.OrderGeneralDetailsClass   generalDetailsClass = orderGeneralDetails . getOrderGeneralDetails(rorderno, 0,iUnitCode);
                java.util.HashMap                   Fibrehm  = orderGeneralDetails . getFibreDetails(rorderno, 0);
%>
            <table width="100%"  border="1">
            <tr>
                <td colspan="8" bgcolor="#FFE4E1">
                <div align="center" class="style5"><cite><span class="style1"><u> Order Basic Information</u></span></cite></div></td>
            </tr>
            <tr>
                <td width="11%" bgcolor="#FFFBF0" class="style3"><div align="right" class="style4">OrderNo</div></td>
                <td width="15%" bgcolor="#FFFBF0" class="style3 style7"><div align="center" class="style8"><%=generalDetailsClass.getRorderno()%></div></td>
                <td width="13%" bgcolor="#FFFBF0" class="style3"><div align="right" class="style4">Process Type</div></td>
                <td width="15%" bgcolor="#FFFBF0" class="style3 style7"><div align="center" class="style8"><%=com.parseNull((String) generalDetailsClass.getProcesstype())%></div></td>
                <td width="11%" bgcolor="#FFFBF0" class="style3"><div align="right" class="style4">Party Name</div></td>
                <td bgcolor="#FFFBF0" class="style3 style7" colspan="3"><div align="center" class="style4"><%=com.parseNull((String) generalDetailsClass.getPartyname())%></div></td>

            </tr>
            <tr>
                <td width="11%"  bgcolor="#FFFBF0" class="style3"><div align="right" class="style4">Order Date</div></td>
                <td width="15%" bgcolor="#FFFBF0" class="style3 style7"><div align="center" class="style8"><%=com.parseDate((String) (generalDetailsClass.getOrderdate()))%></div></td>
                <td width="13%" bgcolor="#FFFBF0" class="style3"><div align="right" class="style4">Corr. Mixing</div></td>
                <%String cmx = com.parseNull((String) generalDetailsClass.getCorrectionmixing());
                  int c = com.toInt(cmx);
                  if (c == 0) {
                %>
                <td width="15%" bgcolor="#FFFBF0" class="style3 style7"><div align="center" class="style8">No</div></td>
                <%} else if (c == 1) {%>
                <td width="15%" bgcolor="#FFFBF0" class="style3 style7"><div align="center" class="style8">Yes</div></td>
                <%}%>
                <td width="11%" bgcolor="#FFFBF0" class="style3"><div align="right" class="style4">Count Name</div></td>
                <td width="12%" bgcolor="#FFFBF0" class="style3 style7"><div align="center" class="style8"><%=com.parseNull((String) generalDetailsClass.getCountname())%></div></td>
                <td width="9%" bgcolor="#FFFBF0" class="style3"><div align="right"  class="style4">&nbsp;Order Status</div></td>
                <td width="14%" bgcolor="#FFFBF0" class="style3 style7"><div align="center"  class="style8"><%=com.parseNull(generalDetailsClass.getSordercancelstatus())%></div></td>
            </tr>
            <tr>
                <td width="11%" bgcolor="#FFFBF0" class="style3"><div align="right" class="style4">OrderQty</div></td>
                <td width="15%" bgcolor="#FFFBF0" class="style3 style7"><div align="center" class="style8"><%=com.parseNull((String) generalDetailsClass.getOrderweight())%></div></td>
                <td width="13%" bgcolor="#FFFBF0" class="style3"><div align="right" style="color:#C71585;" >Contamination</div></td>
                <td width="15%" bgcolor="#FFFBF0" class="style3 style7"><div align="center" style="color:#C71585;"><%=com.parseNull((String) generalDetailsClass.getContamination())%></div></td>
                <td width="11%" bgcolor="#FFFBF0" class="style3"><div align="right" class="style4">Cloth Check</div></td>
                <%
                  String schk = null;
                  int iclthchk = com.toInt(com.parseNull((String) generalDetailsClass.getClothcheckstatus()));
                  if (iclthchk == 1) {
                      schk = "yes";
                  }
                  if (iclthchk == 0) {
                      schk = "No";
                  }
                %>
                <td width="12%" bgcolor="#FFFBF0" class="style3 style7"><div align="center" class="style8"><%=schk%></div></td>
                <td width="9%" bgcolor="#FFFBF0" class="style3"><div align="right" style="color:#C71585;">Cloth check TPI & Count</div></td>
                <td width="14%" bgcolor="#FFFBF0" class="style3 style7"><div align="center" style="color:#C71585;">
                        <%if (generalDetailsClass.getClothchecktpi() == null && generalDetailsClass.getClothcheckcountname() == null) {%>
                        <%} else {%>
                        <%=com.parseNull((String) generalDetailsClass.getClothchecktpi())%> & <%=com.parseNull((String) generalDetailsClass.getClothcheckcountname())%>
                        <%}%>
                    </div></td>
            </tr>
            <tr>
                <td width="11%" bgcolor="#FFFBF0" class="style3"><div align="right" class="style4">Mixing Qty</div></td>
                <td width="15%" bgcolor="#FFFBF0" class="style3 style7"><div align="center" class="style8"><%=com.parseNull((String) generalDetailsClass.getMixweight())%></div></td>
                <td width="13%" bgcolor="#FFFBF0" class="style3"><div align="right" class="style4">Depth</div></td>
                <td width="15%" bgcolor="#FFFBF0" class="style3 style7"><div align="center" class="style8"><%=com.parseNull((String) generalDetailsClass.getDepth())%></div></td>
                <td width="11%" bgcolor="#FFFBF0" class="style3"><div align="right" class="style4">Shade</div></td>
                <td bgcolor="#FFFBF0" class="style3 style7" colspan="3"><div align="center" class="style8"><%=com.parseNull((String) generalDetailsClass.getShade())%></div></td>

            </tr>
            <tr>
                <td width="11%" bgcolor="#FFFBF0" class="style3"><div align="right" class="style4">Indent No</div></td>
                <td width="15%" bgcolor="#FFFBF0" class="style3 style7"><div align="center" class="style8"><%=com.parseNull((String) generalDetailsClass.getIndentno())%></div></td>
                <td width="13%" bgcolor="#FFFBF0" class="style3"><div align="right" style="color:#C71585;">Waste</div></td>
                <td width="15%" bgcolor="#FFFBF0" class="style3 style7"><div align="center" style="color:#C71585;"><%=com.parseNull((String) generalDetailsClass.getWaste())%></div></td>
                <td width="11%" bgcolor="#FFFBF0" class="style3"><div align="right" class="style4">Blend</div></td>
                <td bgcolor="#FFFBF0" class="style3 style7" colspan="3"><div align="center" class="style8"><%=com.parseNull((String) generalDetailsClass.getBlend())%></div></td>
            </tr> 
            <%if(generalDetailsClass.getiProcessgroupcode()==2 && generalDetailsClass.getIdarkbase()>0 && generalDetailsClass.getIwhitebase()>0){%>
            <tr>
                <td width="11%" bgcolor="#FFFBF0" class="style3"><div align="right" class="style4">Dark Base Mix Weight</div></td>
                <td width="15%" bgcolor="#FFFBF0" class="style3 style7"><div align="center" class="style8"><%=com.getRound(com.toDouble(generalDetailsClass.getMixweight())*generalDetailsClass.getIdarkbase()/(generalDetailsClass.getIdarkbase()+generalDetailsClass.getIwhitebase()),2)%></div></td>
                <td width="13%" bgcolor="#FFFBF0" class="style3"><div align="right" class="style4">White Base Mix Weight</div></td>
                <td width="15%" bgcolor="#FFFBF0" class="style3 style7"><div align="center" class="style8"><%=com.getRound(com.toDouble(generalDetailsClass.getMixweight())*generalDetailsClass.getIwhitebase()/(generalDetailsClass.getIdarkbase()+generalDetailsClass.getIwhitebase()),2)%></div></td>
                <td width="11%" bgcolor="#FFFBF0" class="style3"><div align="right" class="style4">&nbsp;</div></td>
                <td bgcolor="#FFFBF0" class="style3 style7" colspan="3"><div align="center" class="style8">&nbsp;</div></td>
            </tr> 
            <%}%>
        </table>
        <br/>
        <%
    
              java.util.List list = (java.util.List) Fibrehm.get("FIBREDETAILSLIST");
              java.util.HashMap fibretothm = (java.util.HashMap) Fibrehm.get("TOTALMAP"); 
        %>

        <table width="100%" border="1">
            <tr>
                <td colspan="4" bgcolor="#FFE4E1">
                    <div align="center" class="style5"><cite><span class="style1"><u>Order Fibre Details</u></span></cite></div></td>
            </tr>
            <tr>
                <td width="11%"><div align="center"><span class="style101"><cite>Fibre Code </cite></span></div></td>
                <td width="65%" class="style101"><div align="center">Fibre Name </div></td>
                <td width="10%"><div align="center" class="style101">Mixing Qty </div></td>
                <td width="14%" class="style101"><div align="center">Mix. Percentage </div></td>
            </tr>
            <%
            for (int i = 0; i < list.size(); i++) {
                java.util.HashMap fibrehm = (java.util.HashMap) list.get(i);
            %>
            <tr>
                <td><div align="left" class="style301"><%=com.parseNull((String)fibrehm  . get("FIBRECODE"))%></div></td>
                <td><div align="left" class="style301"><%=com.parseNull((String)fibrehm  . get("FIBRENAME"))%></div></td>
                <td><div align="right" class="style301"><%=com.parseNull((String)fibrehm . get("MIXQTY"))%></div></td>
                <td><div align="right" class="style301"><%=com.parseNull((String)fibrehm . get("MIXPER"))%></div></td>
            </tr>
            <%}%>
            <tr>
                <td height="23">&nbsp;</td>
                <td><div align="center" class="style101">Total</div></td>
                <td><div align="right" class="style101"><%=com.parseNull((String)fibretothm.get("TOTALWEIGHT"))%></div></td>
                <td><div align="right" class="style101"><%=com.parseNull((String)fibretothm.get("TOTPER"))%></div></td>
            </tr>
        </table>
            
                <%if(generalDetailsClass.getOs_splittype()!=0) {%>
                
                <%--Grouped Order Details--%>
                
                <%
                    java.util.List theGroupedOrderList = orderGeneralDetails.getGroupedOrderDetails(rorderno, 2, generalDetailsClass.getOs_baseorsuborder());    //Group Order Details
                    if(theGroupedOrderList.size()>0){
                        double dTotMixWeight = 0;
                        double dTotOrdWeight = 0;
                %>
                <br/>
                <table border="1">
                        <tr>
                            <td colspan="6" bgcolor="#FFE4E1">
                                <div align="center" class="style5"><cite><span class="style1"><u>GROUPED ORDERS DETAILS</u></span></cite></div>
                            </td>
                        </tr>
                        <tr>
                            <td width="151" bgcolor="#FFFBF0" class="style3"><div align="center" class="style4">Order No</div></td>
                            <td width="151" bgcolor="#FFFBF0" class="style3"><div align="center" class="style4">Count Name</div></td>
                            <td width="151" bgcolor="#FFFBF0" class="style3"><div align="center" class="style4">Shade</div></td>
                            <td width="151" bgcolor="#FFFBF0" class="style3"><div align="center" class="style4">Order Weight</div></td>
                            <td width="151" bgcolor="#FFFBF0" class="style3"><div align="center" class="style4">Mix Weight</div></td>
                            <td width="151" bgcolor="#FFFBF0" class="style3"><div align="center" class="style4">Group  Order Split Dept</div></td>
                        </tr>
                        <%
                            for(int i=0;i<theGroupedOrderList.size();i++){
                                java.util.HashMap theMap = (java.util.HashMap)theGroupedOrderList.get(i);    
                                
                                dTotMixWeight +=  com.toDouble(com.parseNull((String) theMap.get("MIXWEIGHT")));
                                dTotOrdWeight +=  com.toDouble(com.parseNull((String) theMap.get("ORDWEIGHT")));
                        %>
                        <tr>
                            <td width="28"><div align="center" class="style6"><%=com.parseNull((String) theMap.get("RORDERNO"))%></div></td>
                            <td width="34"><div align="center" class="style6"><%=com.parseNull((String) theMap.get("COUNTNAME"))%></div></td>
                            <td width="28"><div align="center" class="style6"><%=com.parseNull((String) theMap.get("SHADE"))%></div></td>
                            <td width="35"><div align="right" class="style6"><%=com.parseNull((String) theMap.get("ORDWEIGHT"))%></div></td>
                            <td width="30"><div align="right" class="style6"><%=com.parseNull((String) theMap.get("MIXWEIGHT"))%></div></td>
                            <td width="30"><div align="center" class="style6"><%=com.parseNull((String) theMap.get("SPLITDEPTNAME"))%></div></td>
                        </tr>
                        <%}%>
                        <tr>
                            <td width="90" colspan="3"><div align="center" class="style6">Total</div></td>
                            <td width="35"><div align="right" class="style6"><%=com.getRound(dTotOrdWeight,2)%></div></td>
                            <td width="30"><div align="right" class="style6"><%=com.getRound(dTotMixWeight,2)%></div></td>
                            <td width="30"><div align="center" class="style6">&nbsp;</div></td>
                        </tr>
                </table>
                <br/>
                <%}%>
                
                <%--Split Order Details--%>                
                <%
                    if(theGroupedOrderList.size()>0) {
                            for(int j=0;j<theGroupedOrderList.size();j++) {
                                java.util.HashMap theOrdMap = (java.util.HashMap)theGroupedOrderList.get(j);
                
                    java.util.List theSplitdOrderList = orderGeneralDetails.getGroupedOrderDetails(com.parseNull((String) theOrdMap.get("RORDERNO")), 1, generalDetailsClass.getOs_baseorsuborder());    //Split Order Details
                    
                    if(theSplitdOrderList.size()>0){
                        double dTotMixWeight = 0;
                        double dTotOrdWeight = 0;
                %>
                <br/>
                <table border="1">
                        <tr>
                            <td colspan="6" bgcolor="#FFE4E1">
                                <div align="center" class="style5"><cite><span class="style1"><u>SPLITTED ORDERS DETAILS</u></span></cite></div>
                            </td>
                        </tr>
                        <tr>
                            <td width="151" bgcolor="#FFFBF0" class="style3"><div align="center" class="style4">Order No</div></td>
                            <td width="151" bgcolor="#FFFBF0" class="style3"><div align="center" class="style4">Count Name</div></td>
                            <td width="151" bgcolor="#FFFBF0" class="style3"><div align="center" class="style4">Shade</div></td>
                            <td width="151" bgcolor="#FFFBF0" class="style3"><div align="center" class="style4">Order Weight</div></td>
                            <td width="151" bgcolor="#FFFBF0" class="style3"><div align="center" class="style4">Mix Weight</div></td>
                            <td width="151" bgcolor="#FFFBF0" class="style3"><div align="center" class="style4">Split Dept</div></td>
                        </tr>
                        <%
                            for(int i=0;i<theSplitdOrderList.size();i++){
                                java.util.HashMap theMap = (java.util.HashMap)theSplitdOrderList.get(i);    
                                
                                dTotMixWeight +=  com.toDouble(com.parseNull((String) theMap.get("MIXWEIGHT")));
                                dTotOrdWeight +=  com.toDouble(com.parseNull((String) theMap.get("ORDWEIGHT")));
                        %>
                        <tr>
                            <td width="28"><div align="center" class="style6"><%=com.parseNull((String) theMap.get("RORDERNO"))%></div></td>
                            <td width="34"><div align="center" class="style6"><%=com.parseNull((String) theMap.get("COUNTNAME"))%></div></td>
                            <td width="28"><div align="center" class="style6"><%=com.parseNull((String) theMap.get("SHADE"))%></div></td>
                            <td width="35"><div align="right" class="style6"><%=com.parseNull((String) theMap.get("ORDWEIGHT"))%></div></td>
                            <td width="30"><div align="right" class="style6"><%=com.parseNull((String) theMap.get("MIXWEIGHT"))%></div></td>
                            <td width="30"><div align="center" class="style6"><%=com.parseNull((String) theMap.get("SPLITDEPTNAME"))%></div></td>
                        </tr>
                        <%}%>
                        <tr>
                            <td width="90" colspan="3"><div align="center" class="style6">Total</div></td>
                            <td width="35"><div align="right" class="style6"><%=com.getRound(dTotOrdWeight,2)%></div></td>
                            <td width="30"><div align="right" class="style6"><%=com.getRound(dTotMixWeight,2)%></div></td>
                            <td width="30"><div align="center" class="style6">&nbsp;</div></td>
                        </tr>
                </table>
                <br/>
                <%}}%>
                            
    <%}%>
    
    
    
    <%--Split Orders --%>
                <%
                    if(theGroupedOrderList.size()==0 && (generalDetailsClass.getOs_baseorsuborder()==1 || generalDetailsClass.getOs_baseorsuborder()==2)) {
                
                    java.util.List theSplitdOrderList = orderGeneralDetails.getGroupedOrderDetails(rorderno, 1, generalDetailsClass.getOs_baseorsuborder());    //Split Order Details
                    
                    if(theSplitdOrderList.size()>0){
                        double dTotMixWeight = 0;
                        double dTotOrdWeight = 0;
                %>
                <br/>
                <table border="1">
                        <tr>
                            <td colspan="6" bgcolor="#FFE4E1">
                                <div align="center" class="style5"><cite><span class="style1"><u>SPLITTED ORDERS DETAILS</u></span></cite></div>
                            </td>
                        </tr>
                        <tr>
                            <td width="151" bgcolor="#FFFBF0" class="style3"><div align="center" class="style4">Order No</div></td>
                            <td width="151" bgcolor="#FFFBF0" class="style3"><div align="center" class="style4">Count Name</div></td>
                            <td width="151" bgcolor="#FFFBF0" class="style3"><div align="center" class="style4">Shade</div></td>
                            <td width="151" bgcolor="#FFFBF0" class="style3"><div align="center" class="style4">Order Weight</div></td>
                            <td width="151" bgcolor="#FFFBF0" class="style3"><div align="center" class="style4">Mix Weight</div></td>
                            <td width="151" bgcolor="#FFFBF0" class="style3"><div align="center" class="style4">Split Dept</div></td>
                        </tr>
                        <%
                            for(int i=0;i<theSplitdOrderList.size();i++){
                                java.util.HashMap theMap = (java.util.HashMap)theSplitdOrderList.get(i);    
                                
                                dTotMixWeight +=  com.toDouble(com.parseNull((String) theMap.get("MIXWEIGHT")));
                                dTotOrdWeight +=  com.toDouble(com.parseNull((String) theMap.get("ORDWEIGHT")));
                        %>
                        <tr>
                            <td width="28"><div align="center" class="style6"><%=com.parseNull((String) theMap.get("RORDERNO"))%></div></td>
                            <td width="34"><div align="center" class="style6"><%=com.parseNull((String) theMap.get("COUNTNAME"))%></div></td>
                            <td width="28"><div align="center" class="style6"><%=com.parseNull((String) theMap.get("SHADE"))%></div></td>
                            <td width="35"><div align="right" class="style6"><%=com.parseNull((String) theMap.get("ORDWEIGHT"))%></div></td>
                            <td width="30"><div align="right" class="style6"><%=com.parseNull((String) theMap.get("MIXWEIGHT"))%></div></td>
                            <td width="30"><div align="center" class="style6"><%=com.parseNull((String) theMap.get("SPLITDEPTNAME"))%></div></td>
                        </tr>
                        <%}%>
                        <tr>
                            <td width="90" colspan="3"><div align="center" class="style6">Total</div></td>
                            <td width="35"><div align="right" class="style6"><%=com.getRound(dTotOrdWeight,2)%></div></td>
                            <td width="30"><div align="right" class="style6"><%=com.getRound(dTotMixWeight,2)%></div></td>
                            <td width="30"><div align="center" class="style6">&nbsp;</div></td>
                        </tr>
                </table>
                <br/>
                <%}%>
                            
    <%}%>
    <%--Split Orders --%>
    
   <%}%>
<%}%>


