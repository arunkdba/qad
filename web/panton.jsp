<%--
    Document   : panton
    Created on : Oct 26, 2015, 4:29:19 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Pantone colors</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script>
        $(document).ready(function()
        {
            $("#Pantone").change(function() {
            var pan1rgb ="0.952941176470588,0.925490196078431,0.87843137254902";
            var pan2rgb ="0.949019607843137,0.941176470588235,0.92156862745098";
            var pan3rgb ="0.956862745098039,0.96078431372549,0.941176470588235";
            var pan4rgb = "0.941176470588235,0.933333333333333,0.913725490196078"
            var pan5rgb = "0.945098039215686,0.909803921568627,0.874509803921569";
            var pan6rgb = "0.941176470588235,0.933333333333333,0.894117647058824"
            var pantype =$('#Pantone').val();
            if(pantype==1)
            {
                var RgbArray = pan1rgb.split(',');
                var R   =   parseFloat(RgbArray[0])*255;
                var G   =   parseFloat(RgbArray[1])*255;
                var B   =   parseFloat(RgbArray[2])*255;
                var NewRgb = 'R: '+R+' G: '+G+' B: '+B;
                $('.rgbvalues').html(NewRgb);
                $('.chcolor').css("background-color", "rgb("+R.toFixed(0)+","+G.toFixed(0)+ ","+B.toFixed(0)+ ")");
            }
            else if(pantype==2){
                var RgbArray = pan2rgb.split(',');
                var R   =   parseFloat(RgbArray[0])*255;
                var G   =   parseFloat(RgbArray[1])*255;
                var B   =   parseFloat(RgbArray[2])*255;
                var NewRgb = 'R: '+R+' G: '+G+' B: '+B;
                $('.rgbvalues').html(NewRgb);
                $('.chcolor').css("background-color", "rgb("+R.toFixed(0)+","+G.toFixed(0)+ ","+B.toFixed(0)+ ")");
            }
            else if(pantype==3){
                var RgbArray = pan3rgb.split(',');
                var R   =   parseFloat(RgbArray[0])*255;
                var G   =   parseFloat(RgbArray[1])*255;
                var B   =   parseFloat(RgbArray[2])*255;
                var NewRgb = 'R: '+R+' G: '+G+' B: '+B;
                $('.rgbvalues').html(NewRgb);
                $('.chcolor').css("background-color", "rgb("+R.toFixed(0)+","+G.toFixed(0)+ ","+B.toFixed(0)+ ")");
            }
            else if(pantype==4){
                var RgbArray = pan4rgb.split(',');
                var R   =   parseFloat(RgbArray[0])*255;
                var G   =   parseFloat(RgbArray[1])*255;
                var B   =   parseFloat(RgbArray[2])*255;
                var NewRgb = 'R: '+R+' G: '+G+' B: '+B;
                $('.rgbvalues').html(NewRgb);
                $('.chcolor').css("background-color", "rgb("+R.toFixed(0)+","+G.toFixed(0)+ ","+B.toFixed(0)+ ")");
            }
             else if(pantype==5){
                var RgbArray = pan5rgb.split(',');
                var R   =   parseFloat(RgbArray[0])*255;
                var G   =   parseFloat(RgbArray[1])*255;
                var B   =   parseFloat(RgbArray[2])*255;
                var NewRgb = 'R: '+R+' G: '+G+' B: '+B;
                $('.rgbvalues').html(NewRgb);
                $('.chcolor').css("background-color", "rgb("+R.toFixed(0)+","+G.toFixed(0)+ ","+B.toFixed(0)+ ")");
            }
            else if(pantype==6){
                var RgbArray = pan6rgb.split(',');
                var R   =   parseFloat(RgbArray[0])*255;
                var G   =   parseFloat(RgbArray[1])*255;
                var B   =   parseFloat(RgbArray[2])*255;
                var NewRgb = 'R: '+R+' G: '+G+' B: '+B;
                $('.rgbvalues').html(NewRgb);
                $('.chcolor').css("background-color", "rgb("+R.toFixed(0)+","+G.toFixed(0)+ ","+B.toFixed(0)+ ")");
            }
            });
        });
        </script>
</head>
<body>
    <table width="50%" border="1">
        <tr>
            <th scope="col">Select Pantone </th>
            <th scope="col"><select name="Pantone" id="Pantone">
                    <option value="1">PANTONE 11-0103 TCX</option>
                    <option value="2">PANTONE 11-0602 TCX</option>
                    <option value="3">PANTONE 11-0601 TCX</option>
                    <option value="4">PANTONE 11-4201 TCX</option>
                    <option value="5">PANTONE 11-0604 TCX</option>
                    <option value="6">PANTONE 11-4300 TCX</option>
                </select>
            </th>
        </tr>
        <tr>
            <th scope="col">RGB Values </th>
            <th scope="col" class="rgbvalues">&nbsp;</th>
        </tr>
        <tr>
            <th scope="col">Color</th>
            <th scope="col" class="chcolor">&nbsp;</th>
        </tr>
    </table>
</body>
</html>