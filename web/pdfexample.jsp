<%-- 
    Document   : pdfexample
    Created on : Aug 5, 2015, 10:41:54 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="script/jquery-latest.min.js" type="text/javascript"></script>
        <script src="script/jspdf.js" type="text/javascript"></script>
        <script src="script/standard_fonts_metrics.js" type="text/javascript"></script>
        <script src="script/split_text_to_size.js" type="text/javascript"></script>
        <script src="script/from_html.js" type="text/javascript"></script>
        <title>JSP Page</title>
        <script>
            $(document).ready(function(){
            $(function () {
            var specialElementHandlers = {
                '#editor': function (element,renderer) {
                return true;
            }
            };
        $('#cmd').click(function () {
        var doc = new jsPDF();
        doc.fromHTML($('#target').html(), 15, 15, {
            'width': 170,'elementHandlers': specialElementHandlers
        });
        //doc.save('d:/sample-file.pdf');
        doc.output('dataurlnewwindow');
    });
});
            });
        </script>
    </head>
    <body id="target">
    <div id="content">
     <h3>Hello, this is a H3 tag</h3>
      <a class="upload"  >Upload to Imgur</a>    
    <h2>this is <b>bold</b> <span style="color:red">red</span></h2>   
    <p> Feedback form with screenshot This script allows you to create feedback forms which include a screenshot, 
    created on the clients browser, along with the form. 
    The screenshot is based on the DOM and as such may not be 100% accurate to the real 
    representation as it does not make an actual screenshot, but builds the screenshot based on the 
    information available on the page. How does it work? The script is based on the html2canvas library,
     which renders the current page as a canvas image, by reading the DOM and the different styles applied 
     to the elements. This script adds the options for the user to draw elements on top of that image, 
     such as mark points of interest on the image along with the feedback they send.
      It does not require any rendering from the server, as the whole image is created on the clients browser.
       No plugins, no flash, no interaction needed from the server, just pure JavaScript! Browser compatibility Firefox 3.5+ Newer versions of Google Chrome, Safari & Opera IE9
    </p>
  </div>  
 <button id="cmd">generate PDF</button>
</body>
</html>
