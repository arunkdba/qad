<%--
    Document   : rkmcrit
    Created on : Dec 16, 2014, 12:43:23 PM
    Author     : admin
--%>

<%@page import="com.reports.print.RkmDateWisePrint"%>
<%@page import="com.reports.classes.CoEffVarient"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="SitraStandards" class="com.reports.data.SitraStandardData"/>
<jsp:useBean id="Common" class="com.common.Common"/>
<!DOCTYPE html>
<%
    com.reports.data.Units unit = new com.reports.data.Units();
    java.util.List theList = unit.getUnit();
    request.setAttribute("theUnitList", theList);
    java.util.HashMap theMap =new java.util.HashMap();
    java.util.List Unitlist = new java.util.ArrayList();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="css/styles-menu.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.css" type="text/css"/>
        <script src="script/jquery-latest.min.js" type="text/javascript"></script>
        <script src="script/script-menu.js" type="text/javascript"></script>
        <script src="script/jquery-ui.js"></script>
        <script src="script/FormValidateUt3C.js" type="text/javascript"></script>
        
        <title>Ut3 A-unit</title>

    </head>
    
    
    <body>
        <%!
            java.util.List TestRkmlist = new java.util.ArrayList();            
            String SprnBody = "",SprnBody2="";
            String SUnitName ="";
            String SProcessType="";
            double dnominalCnt=0,dnominalStr=0;
        %>
        <%@include file="header.jsp" %>
        <div class="elegant-aero-demo">

            <form action="RkmReportDateWise" method="post" class="elegant-aero">
                <h1>RKM Report<span>Please fill the texts in the fields.</span></h1>
                <span id="error"></span>
                <p>
                    <label><span>From Date </span><input id="tdatefrom" name="tdatefrom" placeholder="FromDate" type="text" class="sdate"></label>
                    <label><span>To Date </span><input id="tdateto" name="tdateto" placeholder="Date To" type="text" class="sdate"></label>
                    <label>
                        <span>Shift</span><select name="shift" id="shift">
                            <option value="All">ALL</option>
                            <option value="1">Shift - I</option>
                            <option value="2">Shift - II</option>
                            <option value="3">Shift - III</option>
                    </select></label>
                    <label>
                        <span>Unit</span><select name="sunits" id="sunits">
                            <option value="All">ALL</option>
                            <%
                                Unitlist = (java.util.List) request.getAttribute("theUnitList");
                                for(int i=0;i<Unitlist.size();i++) {
                                    theMap     =  (HashMap)Unitlist.get(i);%>
                            <option value="<%=Common.parseNull((String)theMap.get("UNITCODE"))%>"><%=Common.parseNull((String)theMap.get("UNITNAME"))%></option>
                            <% }%>
                        </select></label>
                      <label>
                            <span>&nbsp;</span>
                            <input type="checkbox" id="ptype" name="ptype" value="1"/>OE
                      </label>
                    <label><span>&nbsp;</span><input class="button" value="Submit" type="submit" id="btnSubmit" name="btnSubmit">
                        <input class="button" value="Reset" type="reset" id="btnreset" name="btnreset">
                    </label>
                </p>
            </form>
        </div>
        <br><br>
        <%if (request.getAttribute("theRkmList") != null ) {
            TestRkmlist = (java.util.List) request.getAttribute("theRkmList");
            SUnitName = session.getAttribute("UnitName").toString();
            SProcessType = (String)request.getAttribute("Process");
                if(SUnitName.equals("All")){
                    SProcessType      ="SPINNING AND OE";
                }
            RkmDateWisePrint sprint = new RkmDateWisePrint();
        %>
        <h2 style="color: #465AA1;">RKM Details From <%=Common.parseDate((String) request.getAttribute("Fdate"))%> TO <%=Common.parseDate((String) request.getAttribute("Tdate"))%></h2>
        <h2 style="color: #465AA1;">Unit :<%=SUnitName%></h2> 
        <h2 style="color: #465AA1;">Shift :<%=request.getAttribute("Shift")%></h2>
        <h2 style="color: #465AA1;">ProcessType : <%=SProcessType%></h2>
        <table width="100%" border="1" class="ut3report">
            <tr>
                <th align="center" valign="top" scope="col">S.No </th>
                <th align="center" valign="top" scope="col">Order No </th>
                <th align="center" valign="top" scope="col">Count</th>
                <th align="center" valign="top" scope="col">Shade</th>
                <th align="center" valign="top" scope="col">Test No </th>
                <th align="center" valign="top" scope="col">Entry Date </th>
                <th align="center" valign="top" scope="col">Rkm</th>
                <th align="center" valign="top" scope="col">Rkm CV</th>
                <th align="center" valign="top" scope="col">Elong</th>
                <th align="center" valign="top" scope="col">Elong CV</th>
                <th align="center" valign="top" scope="col">Sys</th>
                <th align="center" valign="top" scope="col">Sys Cv</th>                                
                <th align="center" valign="top" scope="col">Remarks</th>
                <%
                    if(SUnitName.equals("All"))
                    {%>
                    <th align="center" valign="top" scope="col">Unit</th>
                    <%}%>
            </tr>
            <%
                sprint.createPrn();
                sprint.setHead((String) request.getAttribute("Fdate"),(String) request.getAttribute("Tdate"),SUnitName,SProcessType,(String)request.getAttribute("Shift"));
                for (int i = 0; i < TestRkmlist.size(); i++) {
                    com.reports.classes.RkmDetails Td = (com.reports.classes.RkmDetails) TestRkmlist.get(i);
            %>
            <tr>
                <td><%=i + 1%></td>
                <td><%=Common.parseNull(Td.getOrderno())%></td>
                 <td><%=Common.parseNull(Td.getCountname())%></td>
                 <td><%=Common.parseNull(Td.getShadename())%></td>
                <td><%=Common.parseNull(Td.getTestno())%></td>
                <td><%=Common.parseDate(Td.getEntrydate())%></td>
                <td><%=Common.parseNull(Td.getRkm())%></td>
                <td><%=Common.parseNull(Td.getRkmcv())%></td>
                <td><%=Common.parseNull(Td.getElg())%></td>
                <td><%=Common.parseNull(Td.getElgcv())%></td>
                <td><%=Common.parseNull(Td.getSys())%></td>
                <td><%=Common.parseNull(Td.getSyscv())%></td>
                <td><%=Common.parseNull(Td.getRemarks())%></td>
                <%if(SUnitName.equals("All")){%>
                <td><%=Common.parseNull(Td.getUnitname())%></td>
                <%}%>
            </tr>
            <%
            SprnBody = Common.Pad("| " + String.valueOf(i + 1), 6) + "|";
            SprnBody = SprnBody + Common.Rad(Common.parseNull(Td.getOrderno()), 10) + "|";
            SprnBody = SprnBody + Common.Rad(Common.parseNull(Td.getCountname()), 8) + "|";
            SprnBody = SprnBody + Common.Rad(Common.parseNull(Td.getShadename()), 22) + "|";
            SprnBody = SprnBody + Common.Rad(Common.parseNull(Td.getTestno()), 10) + "|";
            SprnBody = SprnBody + Common.Rad(Common.parseDate(Td.getEntrydate()), 10) + "|";
            SprnBody = SprnBody + Common.Rad(Common.parseNull(Td.getRkm()), 10) + "|";
            SprnBody = SprnBody + Common.Rad(Common.parseNull(Td.getRkmcv()), 10) + "|";
            SprnBody = SprnBody + Common.Rad(Common.parseNull(Td.getElg()), 10) + "|";
            SprnBody = SprnBody + Common.Rad(Common.parseNull(Td.getElgcv()), 10) + "|";
            SprnBody = SprnBody + Common.Rad(Common.parseNull(Td.getSys()), 10) + "|";
            SprnBody = SprnBody + Common.Rad(Common.parseNull(Td.getSyscv()), 10) + "|";
                        
            
            if(SUnitName.equals("All"))
            {
                SprnBody = SprnBody + Common.Rad(Common.parseNull(Td.getRemarks()), 10) + "|";
                SprnBody = SprnBody + Common.Rad(Common.parseNull(Td.getUnitname()), 10) + "|\n";
            }
            else
            {
                SprnBody = SprnBody + Common.Rad(Td.getRemarks(), 10) + "|\n";
            }
            sprint.printData(SprnBody, (String) request.getAttribute("Fdate"),(String) request.getAttribute("Tdate"), SUnitName,SProcessType,(String)request.getAttribute("Shift"));
            
            }
            SprnBody="";
            %>
            </table>
            <%
                sprint.Closefile();           
            }%>
    </body>
</html>