<%-- 
    Document   : runningnorms
    Created on : Jan 11, 2017, 4:07:46 PM
    Author     : root
--%>

<jsp:useBean id="Common" class="com.common.Common"/>
<%@page import="java.util.HashMap"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/ut3report.css" type="text/css">
        <link rel="stylesheet" href="css/styles-menu.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.css" type="text/css"/>
        <script src="script/jquery-latest.min.js" type="text/javascript"></script>
        <script src="script/script-menu.js" type="text/javascript"></script>
        <script src="script/jquery-ui.js"></script>
        <title>JSP Page</title>
        <style>
            .elegant-aero input[type="text"]{
                margin: 1px 6px 5px 0px;
            }
            .elegant-aero label > span {
                float: left;
                width: 23%;
                text-align: left;
                padding-right: 15px;
                margin-top: 10px;
                font-weight: bold;
            }
        </style>
        <script>
            $(document).ready(function()
            {
                $('.sdate').each(function()
                {
                    $(this).datepicker({ dateFormat: 'dd.mm.yy' });
                });    

                $('#btnSubmit').click(function(e)
                {
                    var isValid = true;
                    var validdate = true;
                    var re = "^(3[01]|[12][0-9]|0[1-9]).(1[0-2]|0[1-9]).[0-9]{4}$";
                    $('input[name="tdatefrom"],input[name="tdateto"]').each(function()
                    {
                        if ($.trim($(this).val()) == '')
                        {
                            isValid = false;
                            $(this).css({
                            "border": "1px solid red",
                            "background": "#FFCECE"
                        });
                        $("#error").fadeOut("slow", function()
                        {
                            $("#error").html("Enter Date Values").fadeIn("fast");
                        });
                        }
                        else
                        {
                        $(this).css({
                                "border": "",
                                "background": ""
                        });
                        $("#error").fadeOut(1000);
                        }
                        if(!$.trim($(this).val().match(re)))
                        {
                            validdate = false;
                            //alert("Not Valid");
                        }
                    });
                        if (isValid == false )
                        {
                            e.preventDefault();
                        }
                        else if (validdate == false)
                        {
                            e.preventDefault();
                            $("#error").fadeOut("fast", function()
                            {
                                $("#error").html("Date is not valid Format").fadeIn("fast");
                            });
                        }
                        else
                        {
                            $("#error").fadeIn("fast", function()
                        {
                        });
                    }
                    
                    
                    var sdepthfrom = $("#depthfrom").val();
                    var sdepthto   = $("#depthto").val();
                    var sbleachabove = $("#bleachabove").val();
                    var sbleachbelow = $("#bleachbelow").val();
                    var sPolyfrom = $("#polyfrom").val();
                    var sPolyto = $("#polyto").val();
                    if(sdepthfrom.length>0 && sdepthto.length==0){
                        alert("Enter Depth To Value");
                        return false;
                    }
                    else if(sdepthfrom.length==0 && sdepthto.length>0){
                        alert("Enter Depth From Value");
                        return false;
                    }
                    if(sbleachabove.length>0 && sbleachbelow.length==0){
                        alert("Enter Bleach Below Value");
                        return false;
                    }
                    else if(sbleachabove.length==0 && sbleachbelow.length>0){
                        alert("Enter Bleach Above Value");
                        return false;
                    }
                    if(sPolyfrom.length>0 && sPolyto.length==0){
                        alert("Enter Poly To Value");
                        return false;
                    }
                    else if(sPolyfrom.length==0 && sPolyto.length>0){
                        alert("Enter Poly From Value");
                        return false;
                    }
            });
            
            $("#polyfrom").blur(function(){

                var sPolyfrom = $(this).val();                
                if(sPolyfrom.length>0){
                    $(".unitss").prop( "checked", true );
                }
                else{
                    $(".unitss").prop( "checked", false );
                }                
            });
            $('#bleachout').change(function() {
                
                if ($("#bleachout").is(':checked')) {
                    $("#bleachabove").val('');
                    $("#bleachbelow").val('');
                    $("#bleachabove").attr("disabled", "disabled"); 
                    $("#bleachbelow").attr("disabled", "disabled");                     
                }
                else{
                    $("#bleachabove").removeAttr("disabled"); 
                    $("#bleachbelow").removeAttr("disabled"); 
                }
            });
        });
        </script>
    </head>
    <body>
         <%!
            java.util.List OrderList = new java.util.ArrayList();
            String sProcess ="";
        %>
        <%@include file="header.jsp" %>
        
        <div class="elegant-aero-demo">

            <form action="RunningNormsServlet" method="post" class="elegant-aero">
                <h1>Running Norms Report<span>Please fill the texts in the fields.</span></h1>
                <span id="error"></span>
                 <p>
                    <label><span>From </span><input id="tdatefrom" name="tdatefrom" placeholder="FromDate" type="text" class="sdate"></label>
                    <label><span>To </span><input id="tdateto" name="tdateto" placeholder="ToDate" type="text" class="sdate"></label>
                    
                    <label><span>Depth From </span><input id="depthfrom" name="depthfrom" placeholder="" type="text" ></label>
                    <label><span>Depth To </span><input id="depthto" name="depthto" placeholder="" type="text" ></label>
                    <label>
                        <input type="checkbox" id="bleachout" name="bleachout" value="1"/>WithOut Bleach
                    </label>
                    <label><span>Bleach Above </span><input id="bleachabove" name="bleachabove" placeholder="" type="text" ></label>
                    <label><span>Bleach Below </span><input id="bleachbelow" name="bleachbelow" placeholder="" type="text" ></label>

                    <label>
                            <input type ="radio" name="units" class="unitss" value="1">A-Unit
                            <input type ="radio" name="units" class="unitss" value="2">B-Unit                            
                    </label>

                    <label><span>Poly Form </span><input id="polyfrom" name="polyfrom" placeholder="" type="text" ></label>
                    <label><span>Poly To </span><input id="polyto" name="polyto" placeholder="" type="text" ></label>

                    <!-- <label><span>Poly Above </span><input id="polyabove" name="polyabove" placeholder="" type="text" ></label>
                    <label><span>Poly Below </span><input id="polybelow" name="polybelow" placeholder="" type="text" ></label>
                    
                    <label><span>Poly Upto </span><input id="polyupto" name="polyupto" placeholder="" type="text" ></label>-->

                    <label><span>&nbsp;</span><input class="button" value="Submit" type="submit" id="btnSubmit" name="btnSubmit">
                    <input class="button" value="Reset" type="reset" id="btnreset" name="btnreset">
                    </label>
                </p>
                <input type="hidden" name="Unit" id="Unit" value="1"/>
                <input type="hidden" name="Orders" id="Orders" value="1"/>
            </form>
        </div>
        <br><br>
        <%if (request.getAttribute("theOrderList") != null) {%>
        <form method="POST" action="RunningNormsServlet" name= "" id="">
            <table width="45%" border="1" align="center" class="standards">
            <tr>
                <th>S.No</th>
                <th>OrderNo</th>
                <th>Count</th>
                <th>Select</th>
            </tr>
            <%OrderList = (java.util.List) request.getAttribute("theOrderList");
            for(int i=0;i<OrderList.size();i++) {
            HashMap theMap     =  (HashMap)OrderList.get(i);
            String sLink  = "<a class='ajax' href='RunningNormsServlet?Unit=1&ROrderNo="+Common.parseNull((String)theMap.get("ORDNO"))+"'>"+Common.parseNull((String)theMap.get("ORDNO"))+"</a>";
            %>
            <tr>
                <td><%=(i+1)%></td>
                <td><%=Common.parseNull((String)theMap.get("ORDNO"))%></td>
                <td><%=Common.parseNull((String)theMap.get("COUNTNAME"))%></td>
                <td><input type="checkbox" name="ROrderNo" id='R<%=theMap.get("ORDNO")%>' value="<%=theMap.get("ORDNO")%>" class="chkbox"></td>
            </tr>
            <%}%>
            </table>
            <div style="text-align: center;">
            <input class="button" value="Submit" type="submit" id="" name="btnSubmit">
            <input class="button" value="Reset" type="reset" id="btnreset" name="btnreset">
            </div>
            <input type="hidden" name="Unit" id="Unit" value="1"/>
            <input type="hidden" name="Orders" id="Orders" value="0"/>
        </form>
        <%}%>
    </body>
</html>
