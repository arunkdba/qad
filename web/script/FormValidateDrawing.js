/*
 * Script For Validating Date Field
 * And To Add Jquery Date Picker
 * And handling form action in index.jsp fiel
 */

$(document).ready(function()
{
    $(document).on('click', 'input[type="checkbox"]', function() {      
        $('input[type="checkbox"]').not(this).prop('checked', false);      
    });
    
    $('.sdate').each(function()
    {
        $(this).datepicker({ dateFormat: 'dd.mm.yy' });
    });
});

$(document).ready(function()
{
    $('#btnSubmit').click(function(e)
    {
        var isValid = true;
        var validdate = true;
        var re = "^(3[01]|[12][0-9]|0[1-9]).(1[0-2]|0[1-9]).[0-9]{4}$";
        $('input[name="tdatefrom"],input[name="tdateto"]').each(function()
        {
            if ($.trim($(this).val()) == '')
            {
                isValid = false;
                $(this).css({
                    "border": "1px solid red",
                    "background": "#FFCECE"
                });
                $("#error").fadeOut("slow", function()
                {
                    $("#error").html("Enter Date Values").fadeIn("fast");
                });
            }
            else
            {
                $(this).css({
                    "border": "",
                    "background": ""
                });
                $("#error").fadeOut(1000);
            }
            if(!$.trim($(this).val().match(re)))
            {
                validdate = false;
                    //alert("Not Valid");
            }
        });
        if (isValid == false )
        {
            e.preventDefault();
        }
        else if (validdate == false)
        {
            e.preventDefault();
             $("#error").fadeOut("fast", function()
                {
                    $("#error").html("Date is not valid Format").fadeIn("fast");
                });
        }
        else
        {
                $("#error").fadeIn("fast", function()
                {
                    //$(this).css({ "border": "1px solid green","background": "lime"});
                    //$("#error").html("Date Accepted").fadeIn("fast");
                   // $("#msgbox").toggle(400);
                });
        }
    });
    var unit = $('#sunits').val();
    var dataString = 'sunits='+ unit;
    $.ajax
        ({
            type: "POST",
            url: "ajax_machine.jsp",
            data: dataString,
            cache: false,
            success: function(html)
            {
                $(".machine").html(html);
            }
        });
        $("#sunits").change(function() {
            var unit = $('#sunits').val();
            var dataString = 'sunits='+ unit;

            if(unit ==1){
                
                $("#ptype").prop( "disabled", false );
                $("#yarntype").prop( "disabled", false );

            }
            else if(unit ==2){

                $("#ptype").prop( "checked", false );
                $("#yarntype").prop( "checked", false );
                
                $("#ptype").prop( "disabled", true );
                $("#yarntype").prop( "disabled", true );
            }
            else if(unit == 10)
            {
                    $("#ptype").prop( "disabled", false );
                    $("#ptype").prop( "checked", true );

                    $("#yarntype").prop( "disabled", true );
                    $("#yarntype").prop( "checked", false );
            }

            $.ajax
            ({
                type: "POST",
                url: "ajax_machine.jsp",
                data: dataString,
                cache: false,
                success: function(html)
                {
                    $(".machine").html(html);
                }
            });
        });
});