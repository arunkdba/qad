/* 
 * Script For Validating Date Field
 * And To Add Jquery Date Picker
 * And handling form action in index.jsp fiel
 */

$(document).ready(function()
{
    $('.sdate').each(function()
    {
        $(this).datepicker({ dateFormat: 'dd.mm.yy' });
    });    
 });
  
$(document).ready(function()
{
    $('#btnSubmit').click(function(e) 
    {
        var isValid = true;
        var validdate = true;
        var re = "^(3[01]|[12][0-9]|0[1-9]).(1[0-2]|0[1-9]).[0-9]{4}$";
        $('#orderno').each(function() 
        {
            if ($.trim($(this).val()) == '') 
            {
                isValid = false;
                $(this).css({
                    "border": "1px solid red",
                    "background": "#FFCECE"
                });
                $("#error").fadeOut("slow", function()
                {
                    $("#error").html("Enter OrderNo").fadeIn("fast");
                });
            }
            else
            {
                $(this).css({
                    "border": "",
                    "background": ""
                });
                $("#error").fadeOut(1000);
            }
        });
        if (isValid == false )
        {
            e.preventDefault();
        }
        else 
        {
                $("#error").fadeIn("fast", function()
                {

                });
        }
    });
    
    $("#sunits").change(function () {
        var firstDropVal = $(this).val();
        if(firstDropVal == 10)
        {
               $("#ptype").prop( "checked", true );
        }
        else
        {
                $("#ptype").prop( "checked", false );
        }
        if(firstDropVal ==2){
            $("#ptype").prop( "disabled", true );
        }
        else
        {
                $("#ptype").prop( "disabled", false );
        }
    });
});