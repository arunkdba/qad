$(document).ready(function(){
	$("#ele4").find('button').on('click', function() {
             //Print ele4 with custom options
            $("#ele4").print({
                //Use Global styles
                globalStyles : false,
                //Add link with attrbute media=print
                mediaPrint : true,
                //Custom stylesheet
                stylesheet : null,
                //Print in a hidden iframe
                iframe : false,
                //Don't print this
                noPrintSelector : ".avoid-this",
                //Add this at top
                prepend : null,
                //Add this on bottom
                append : null,
                manuallyCopyFormValues: true,
                deferred: $.Deferred()
            });
            });
});        