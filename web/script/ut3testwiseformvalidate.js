/*
 * Script For Validating Date Field
 * And To Add Jquery Date Picker
 * And handling form action in index.jsp fiel
 */

$(document).ready(function()
{
   /* $('.sdate').each(function()
    {
        $(this).datepicker({ dateFormat: 'dd.mm.yy' });
    });*/
   $(document).ready(function()
     {
                    $('#tdatefrom').datepicker({ dateFormat: 'dd.mm.yy' });
                    $('#tdateto').datepicker({ dateFormat: 'dd.mm.yy',
                    onSelect: function () {
                        var mechine = $('#machine').val();
                        var dateFrom = $('#tdatefrom').val();
                        var dateTo = $('#tdateto').val();
                        var dataString = 'dateFrom='+ dateFrom+'&dateTo='+dateTo+'&mechine='+mechine;
                        /*if($.trim(dateFrom).length == 0)
                        {
                                $("#error").fadeOut("slow", function()
                                {
                                    $("#error").html("Enter From Date").fadeIn("fast");
                                });
                                $('#tdateto').val('');
                                return false;
                        }*/
                        if (!$.trim(dateTo).length == 0 && !$.trim(dateTo).length == 0)
                        {
                         //   alert("haiiii");
                          $.ajax
                                ({
                                    type: "POST",
                                    url: "ajax_ut3_testno.jsp",
                                    data: dataString,
                                    cache: false,
                                    success: function(html)
                                    {
                                       // alert(html);
                                        $(".testno").html(html);
                                    }
                                });
                        }
                    }
                    });
                });
                $("#machine").change(function() {

                    $('#tdatefrom').val('');
                    $('#tdateto').val('');
                    $('#testno').text('');
                });
});

$(document).ready(function()
{
    $('#btnSubmit').click(function(e)
    {
        var isValid = true;
        var validdate = true;
        var re = "^(3[01]|[12][0-9]|0[1-9]).(1[0-2]|0[1-9]).[0-9]{4}$";
        $('input[name="tdateto"],input[name="tdateto"]').each(function()
        {
            if ($.trim($(this).val()) == '')
            {
                isValid = false;
                $(this).css({
                    "border": "1px solid red",
                    "background": "#FFCECE"
                });
                $("#error").fadeOut("slow", function()
                {
                    $("#error").html("Enter Date Values").fadeIn("fast");
                });
            }
            else
            {
                $(this).css({
                    "border": "",
                    "background": ""
                });
                $("#error").fadeOut(1000);
            }
            if(!$.trim($(this).val().match(re)))
            {
                validdate = false;
                    //alert("Not Valid");
            }
        });
        if (isValid == false )
        {
            e.preventDefault();
        }
        else if (validdate == false)
        {
            e.preventDefault();
             $("#error").fadeOut("fast", function()
                {
                    $("#error").html("Date is not valid Format").fadeIn("fast");
                });
        }
        else
        {
                $("#error").fadeIn("fast", function()
                {
                    //$(this).css({ "border": "1px solid green","background": "lime"});
                    //$("#error").html("Date Accepted").fadeIn("fast");
                   // $("#msgbox").toggle(400);
                });
        }
    });
});