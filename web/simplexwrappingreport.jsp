<%--
    Document   : simplexwrappingreport
    Created on : May 19, 2017, 9:51:02 AM
    Author     : root
--%>

<%@page import="java.util.HashMap"%>
<%@page import="com.simplex.wrapping.SimplexWrappingTestData"%>
<%@page import="com.reports.pdf.SimplexWrappingTestPDF"%>
<%@page import="com.common.Common"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    com.reports.data.Units unit = new com.reports.data.Units();
    java.util.List theList = unit.getUnit();
    request.setAttribute("theUnitList", theList);
    java.util.HashMap theMap =new java.util.HashMap();
    java.util.List Unitlist = new java.util.ArrayList();
%>
<html>
    <head>
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="css/styles-menu.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.css" type="text/css">
        <script src="script/jquery-latest.min.js" type="text/javascript"></script>
        <script src="script/script-menu.js" type="text/javascript"></script>
        <script src="script/jquery-ui.js"></script>
        <script src="script/FormValidateDrawing.js" type="text/javascript"></script>
        
        <script type="text/javascript">
             function showAlert(){
                var msg = "PDF File Created in D:\SimplexWrappingTestPDF, PDF in Legal Sheet";
                alert(msg);
            }
        </script>
        
        <title>Simplex Hank Report</title>
    </head>
    <body>
        <%!
            Common common = new Common();
        %>
        <%@include file="header.jsp" %>
        <div class="elegant-aero-demo">

            <form action="SimplexWrappingReportServlet" method="post" class="elegant-aero">
                <h1>Simplex Wrapping Test<span>Please fill the texts in the fields.</span></h1>
                <span id="error"></span>
                <p>
                    <!--<label><span>Machine </span><select name="machine" id="machine" >
                            <option selected="selected" value="0">-SELECT-</option>
                    </select></label>-->
                    <label><span>Test From </span><input id="tdatefrom" name="tdatefrom" placeholder="FromDate" type="text" class="sdate"></label>
                    <label><span>Test To </span><input id="tdateto" name="tdateto" placeholder="ToDate" type="text" class="sdate"></label>
                    <!--<label><span>Test No </span><select name="testno" id="testno" class="testno">
                           <!-- <option selected="selected">--Select Testno--</option>
                            <option value="All" >All</option>
                    </select></label>-->
                    <label><span>Order No</span><input id="orderno" name="orderno" placeholder="Valid Order No" type="text"></label>
                    <label>
                        <span>Unit</span><select name="sunits" id="sunits">                            
                            <%
                                Unitlist = (java.util.List) request.getAttribute("theUnitList");
                                for(int i=0;i<Unitlist.size();i++) { 
                                    theMap     =  (HashMap)Unitlist.get(i);%>                                    
                            <option value="<%=(String)theMap.get("UNITCODE")%>"><%=(String)theMap.get("UNITNAME")%></option>
                            <% }%>
                        </select></label>
                        <label>
                        <span>Shift</span><select name="sshift" id="sshift">
                            <option value="0" >All</option>
                            <option value="1" >Shift - I</option>
                            <option value="2" >Shift - II</option>
                            <option value="3" >Shift - III</option>
                        </select></label>
                    <label><span>&nbsp;</span><input class="button" value="Submit" type="submit" id="btnSubmit" name="btnSubmit" onClick="showAlert()">
                        <input class="button" value="Reset" type="reset" id="btnreset" name="btnreset">
                    </label>
                </p>
            </form>
        </div>
        <br><br>
        <%
            if (request.getAttribute("theSimplexWrappingTestDetails") != null) {
                java.util.List TBase = new java.util.ArrayList();
                               TBase = (java.util.List) request.getAttribute("theSimplexWrappingTestDetails");
                               
                               SimplexWrappingTestPDF   spdf   = new SimplexWrappingTestPDF();                     
                               spdf.createPDFFile();
                               spdf.setTestBase((String) request.getAttribute("Fdate"),(String) request.getAttribute("Tdate"),(String)request.getAttribute("UnitName"),(String)request.getAttribute("Shift"));
                               spdf.setHead();
                               spdf.printPDFData(TBase); 
        %>
        <h4 style="margin:0px;">Simplex Wrapping  : From  <%=common.parseDate((String) request.getAttribute("Fdate"))%> To <%=common.parseDate((String) request.getAttribute("Tdate"))%></h4>
        <h4 style="margin:0px;">Unit : <%=request.getAttribute("UnitName")%></h4>
        <h4 style="margin:0px;">Shift : <%=request.getAttribute("Shift")%></h4>
        <table width="100%" border="1" class="spinningwrapping">
            <tr>
                <th width="42" align="center" valign="top" scope="col">S.No</th>
                <th width="43" scope="col" align="center" valign="top">Test No</th>
                <th width="40" scope="col" align="center" valign="top">Order No</th>
                <th width="40" scope="col" align="center" valign="top">Shade</th>
                <th width="40" scope="col" align="center" valign="top">Standard Hank</th>
                <th width="82" scope="col" align="center" valign="top">1</th>
                <th width="96" scope="col" align="center" valign="top">2</th>
                <th width="92" scope="col" align="center" valign="top">3</th>
                <th width="92" scope="col" align="center" valign="top">4</th>
                <th width="76" scope="col" align="center" valign="top">Avg Value </th>
                <th width="91" scope="col" align="center" valign="top">PumpColor</th>
                <th width="92" scope="col" align="center" valign="top">B/D Wheel</th>
                <th width="92" scope="col" align="center" valign="top">BreakDraft</th>
                <th width="92" scope="col" align="center" valign="top">CpWheel</th>
                <th width="92" scope="col" align="center" valign="top">TotalDraft</th>
                <th width="92" scope="col" align="center" valign="top">TwistWheel</th>
                <th width="92" scope="col" align="center" valign="top">Tpi</th>
                <th width="92" scope="col" align="center" valign="top">SpendleNo</th>
                <th width="68" scope="col" align="center" valign="top">Time</th>
                <th width="49" scope="col" align="center" valign="top">Changes</th>
            </tr>
            <%
                for (int i = 0; i < TBase.size(); i++) {
                    SimplexWrappingTestData simplexWrappingTestData = (SimplexWrappingTestData)TBase.get(i);
            %>
            <tr>
                <td><%=simplexWrappingTestData.getsSino()%></td>
                <td><%=simplexWrappingTestData.getsTestno()%></td>
                <td><%=simplexWrappingTestData.getsOrderNo()%></td>
                <td><%=simplexWrappingTestData.getsShade()%></td>
                <td><%=simplexWrappingTestData.getsStdHank()%></td>
                <td><%=simplexWrappingTestData.getsHank1()%></td>
                <td><%=simplexWrappingTestData.getsHank2()%></td>
                <td><%=simplexWrappingTestData.getsHank3()%></td>
                <td><%=simplexWrappingTestData.getsHank4()%></td>
                <td><%=simplexWrappingTestData.getsHankAvg()%></td>
                <td><%=simplexWrappingTestData.getsPumpColor()%></td>
                <td><%=simplexWrappingTestData.getsBdWheel()%></td>
                <td><%=simplexWrappingTestData.getsBreakDraft()%></td>
                <td><%=simplexWrappingTestData.getsCpWheel()%></td>
                <td><%=simplexWrappingTestData.getsTotalDraft()%></td>
                <td><%=simplexWrappingTestData.getsTwistWheel()%></td>
                <td><%=simplexWrappingTestData.getsTpi()%></td>
                <td><%=simplexWrappingTestData.getsSpendleNo()%></td>
                <td><%=simplexWrappingTestData.getsEntryDate()%></td>
                <td><%=simplexWrappingTestData.getsStatus()%></td>
            </tr>
            <%}%>
        </table>
        <%}%>
</body>
</html>