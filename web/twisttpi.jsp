<%--
    Document   : twisttpi
    Created on : Nov 8, 2016, 11:31:54 AM
    Author     : root
--%>

<%@page import="com.reports.print.TwistTpiPrint"%>
<%@page import="com.reports.pdf.TwistTpiPDF"%>
<%@page import="com.common.Common"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Twist Tpi Report</title>
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="css/styles-menu.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.css" type="text/css">
        <script src="script/jquery-latest.min.js" type="text/javascript"></script>
        <script src="script/script-menu.js" type="text/javascript"></script>
        <script src="script/jquery-ui.js"></script>
        <script src="script/FormValidateDrawing.js" type="text/javascript"></script>
        <script type="text/javascript">
             function showAlert(){
                var msg = "PDF File Created in D:\TwistTPIPDF, PDF in A3 Sheet";
                alert(msg);
            }
        </script>
        
    </head>
    <body>
        <%!
            java.util.List Testlist = new java.util.ArrayList();
            Common common = new Common();
            String SprnBody = "";
        %>
        <%@include file="header.jsp" %>
        <div class="elegant-aero-demo">

            <form action="TwistTpiReportServlet" method="post" class="elegant-aero">
                <h1>Twist TPI Test<span>Please fill the texts in the fields.</span></h1>
                <span id="error"></span>
                <p>
                    <label><span>Test From </span><input id="tdatefrom" name="tdatefrom" placeholder="FromDate" type="text" class="sdate"></label>
                    <label><span>Test To </span><input id="tdateto" name="tdateto" placeholder="ToDate" type="text" class="sdate"></label>
                    <label><span>Order No</span><input id="orderno" name="orderno" placeholder="Valid Order No" type="text"></label>
                    <label><span>&nbsp;</span><input class="button" value="Submit" type="submit" id="btnSubmit" name="btnSubmit" onClick="showAlert()">
                    <input class="button" value="Reset" type="reset" id="btnreset" name="btnreset">
                    </label>
                </p>
            </form>
        </div>
        <br><br>
        <%if (request.getAttribute("theTestDetailsList") != null) {
                TwistTpiPrint sprint = new TwistTpiPrint();
                sprint.createPrn();
                sprint.setTestBase((String) request.getAttribute("Fdate"),(String)request.getAttribute("Tdate"));
                sprint.setHead((String) request.getAttribute("Fdate"));
                
                TwistTpiPDF  spdf       = new TwistTpiPDF();
                spdf.createPDFFile();
                spdf.setTestBase((String) request.getAttribute("Fdate"),(String)request.getAttribute("Tdate"));
                spdf.setHead();
                
                
            %>
        <h4 style="margin:0px;">Twist Tpi Reports From : <%=common.parseDate((String) request.getAttribute("Fdate"))%> To <%=common.parseDate((String) request.getAttribute("Tdate"))%></h4>
        <br>
        <table width="100%" border="1" class="spinningwrapping">
            <tr>
                <th scope="col">S.No</th>
                <th scope="col">Test No</th>
                <th scope="col">No of Test</th>
                <th scope="col">Machine</th>
                <th scope="col">Order No</th>
                <th scope="col">Shade</th>
                <th scope="col">Count</th>
                <th scope="col">N.Twist</th>
                <th scope="col">Min Tpi</th>
                <th scope="col">Max Tpi</th>
                <th scope="col">Avg Tpi</th>
                <th scope="col">Rang Tpi</th>
                <th scope="col">Tpi Cv</th>
            </tr>
            <%
                Testlist = (java.util.List) request.getAttribute("theTestDetailsList");
                spdf.setPrintData(Testlist);
                for (int i = 0; i < Testlist.size(); i++) {
                    com.reports.classes.TwistTpiDetails ttd = (com.reports.classes.TwistTpiDetails)Testlist.get(i);%>
                    <tr>
                    <td><%=i + 1%></td>
                    <td><%=ttd.getTestno()%></td>
                    <td><%=ttd.getTests()%></td>
                    <td><%=ttd.getFrame()%></td>
                    <td><%=ttd.getLot()%></td>
                    <td><%=ttd.getShade()%></td>
                    <td><%=ttd.getCounttype()%></td>
                    <td><%=ttd.getNomtwist()%></td>
                    <td><%=ttd.getMintpi()%></td>
                    <td><%=ttd.getMaxtpi()%></td>
                    <td><%=ttd.getAvgtpi()%></td>
                    <td><%=ttd.getRangtpi()%></td>
                    <td><%=ttd.getTpicv()%></td>
                    </tr>

                    <%
                    SprnBody = common.Pad("| " + String.valueOf(i + 1), 4) + "|";
                    SprnBody = SprnBody + common.Pad(ttd.getTestno(), 7) + "|";
                    SprnBody = SprnBody + common.Rad(ttd.getTests(), 8) + "|";
                    SprnBody = SprnBody + common.Rad(ttd.getFrame(), 12) + "|";
                    SprnBody = SprnBody + common.Rad(ttd.getLot(), 17) + "|";
                    SprnBody = SprnBody + common.Rad(ttd.getShade(), 20) + "|";
                    SprnBody = SprnBody + common.Rad(ttd.getCounttype(), 8) + "|";
                    SprnBody = SprnBody + common.Rad(ttd.getNomtwist(), 7) + "|";
                    SprnBody = SprnBody + common.Rad(common.getRound(ttd.getMintpi(), 2), 7) + "|";
                    SprnBody = SprnBody + common.Rad(common.getRound(ttd.getMaxtpi(), 2), 7) + "|";
                    SprnBody = SprnBody + common.Rad(common.getRound(ttd.getAvgtpi(), 2), 7) + "|";
                    SprnBody = SprnBody + common.Rad(common.getRound(ttd.getRangtpi(), 2), 8) + "|";
                    SprnBody = SprnBody + common.Rad(common.getRound(ttd.getTpicv(), 2), 6) + "|\n";
                    sprint.printData(SprnBody, (String) request.getAttribute("Fdate"),(String)request.getAttribute("Tdate"));
                    %>

           <% }
                sprint.Closefile();
            %>
        </table>
        <%}%>
    </body>
</html>