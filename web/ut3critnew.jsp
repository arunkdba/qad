<%--
    Document   : ut3critnew
    Created on : 27 Nov, 2017, 10:33:57 AM
    Author     : Administrator
--%>

<%@page import="com.reports.print.Ut3DateWisePrintNew"%>
<%@page import="com.reports.pdf.UT3DateWiseReportNewPDF"%>
<%@page import="com.reports.classes.CoEffVarient"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="SitraStandards" class="com.reports.data.SitraStandardData"/>
<jsp:useBean id="Common" class="com.common.Common"/>
<!DOCTYPE html>
<%
    com.reports.data.Units unit = new com.reports.data.Units();
    java.util.List theList = unit.getUnit();
    java.util.List theDepList = unit.getDepartment();
    request.setAttribute("theUnitList", theList);
    request.setAttribute("theDeptList", theDepList);
    java.util.HashMap theMap =new java.util.HashMap();
    java.util.HashMap thedeptMap =new java.util.HashMap();
    java.util.List Unitlist = new java.util.ArrayList();
    java.util.List Deptlist = new java.util.ArrayList();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="css/styles-menu.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.css" type="text/css"/>
        <script src="script/jquery-latest.min.js" type="text/javascript"></script>
        <script src="script/script-menu.js" type="text/javascript"></script>
        <script src="script/jquery-ui.js"></script>
        <title>Ut3 Test Report</title>
        <script>
         $(document).ready(function()
         {
            $('.sdate').each(function()
            {
                $(this).datepicker({ dateFormat: 'dd.mm.yy'});
            });
            $('#btnSubmit').click(function(e)
            {
            var isValid = true;
            var validdate = true;
            var re = "^(3[01]|[12][0-9]|0[1-9]).(1[0-2]|0[1-9]).[0-9]{4}$";
            $('input[name="tdatefrom"],input[name="tdateto"]').each(function()
            {
                if ($.trim($(this).val()) == '')
                {
                    isValid = false;
                    $(this).css({
                    "border": "1px solid red",
                    "background": "#FFCECE"
                });
                $("#error").fadeOut("slow", function()
                {
                    $("#error").html("Enter Date Values").fadeIn("fast");
                });
            }
            else
            {
                $(this).css({
                    "border": "",
                    "background": ""
                });
                $("#error").fadeOut(1000);
            }
            if(!$.trim($(this).val().match(re)))
            {
                validdate = false;
                    //alert("Not Valid");
            }
         });
         if (isValid == false )
         {
            e.preventDefault();
         }
         else if (validdate == false)
         {
            e.preventDefault();
             $("#error").fadeOut("fast", function()
                {
                    $("#error").html("Date is not valid Format").fadeIn("fast");
                });
         }
         else
         {
            var sdept = $("#department option:selected").text();
            $("#seldept").val($.trim(sdept));
         }
        });

        $("#sunits").change(function () {
        var firstDropVal = $(this).val();
        if(firstDropVal == 10)
        {
               $("#ptype").prop( "checked", true );
        }
        else
        {
                $("#ptype").prop( "checked", false );
        }
        if(firstDropVal ==2){
            $("#ptype").prop( "disabled", true );
        }
        else
        {
            $("#ptype").prop( "disabled", false );
        }
    });

    });
        </script>
        <script type="text/javascript">
             function showAlert(){
                var msg = "PDF File Created in D:\Ut3DateWiseNewPDF, PDF in Legal Sheet";
                alert(msg);
            }
        </script>
        
    </head>
    <body>
        <%!
            java.util.List TestUt3list = new java.util.ArrayList();
            String SprnBody = "",SprnBody2="";
            String SUnitName ="";
            String SProcessType="";
            double dnominalCnt=0,dnominalStr=0;
        %>
        <%@include file="header.jsp" %>
        <div class="elegant-aero-demo">

            <form action="Ut3ReportDateWiseNew" method="post" class="elegant-aero">
                <h1>UT3 Report<span>Please fill the texts in the fields.</span></h1>
                <span id="error"></span>
                <p>
                    <label><span>From Date</span><input id="tdatefrom" name="tdatefrom" placeholder="FromDate" type="text" class="sdate"/></label>
                    <label><span>To Date</span><input id="tdateto" name="tdateto" placeholder="tdateto" type="text" class="sdate"/></label>
                    <label>
                        <span>Shift</span><select name="shift" id="shift">
                            <option value="All">ALL</option>
                            <option value="1">Shift - I</option>
                            <option value="2">Shift - II</option>
                            <option value="3">Shift - III</option>
                    </select></label>
                    <label>
                        <span>Department</span><select name="department" id="department">
                            <%
                                Deptlist = (java.util.List) request.getAttribute("theDeptList");
                                for(int i=0;i<Deptlist.size();i++) { 
                                    thedeptMap     =  (HashMap)Deptlist.get(i);%>                                    
                            <option value="<%=Common.parseNull((String)thedeptMap.get("DEPTCODE"))%>"><%=Common.parseNull((String)thedeptMap.get("DEPTNAME"))%></option>
                            <% }%>
                        </select></label>
                    <label>
                        <span>Unit</span><select name="sunits" id="sunits">
                            <option value="All">ALL</option>
                            <%
                                Unitlist = (java.util.List) request.getAttribute("theUnitList");
                                for(int i=0;i<Unitlist.size();i++) { 
                                    theMap     =  (HashMap)Unitlist.get(i);%>
                            <option value="<%=Common.parseNull((String)theMap.get("UNITCODE"))%>"><%=Common.parseNull((String)theMap.get("UNITNAME"))%></option>
                            <% }%>
                        </select></label>
                    <label>
                            <span>&nbsp;</span>
                            <input type="checkbox" id="ptype" name="ptype" value="1"/>OE
                    </label>
                    <label>
                            <span>Order by</span>
                            <input type="radio" name="orderby" value="1"/> Count
                            <input type="radio" name="orderby" value="2"/> Shade
                            <input type="radio" name="orderby" value="3"/> Count & Shade
                            <input type="radio" name="orderby" value="4" checked="checked"/> None
                    </label>
                    <label><span>&nbsp;</span><input class="button" value="Submit" type="submit" id="btnSubmit" name="btnSubmit" onClick="showAlert()">
                        <input class="button" value="Reset" type="reset" id="btnreset" name="btnreset">
                    </label>
                </p>
                <input type="hidden" name="seldept" id="seldept" value=""/>
            </form>
        </div>
        <br><br>
        <%if (request.getAttribute("theUt3List") != null ) {
            TestUt3list = (java.util.List) request.getAttribute("theUt3List");
            SUnitName = session.getAttribute("UnitName").toString();
            SProcessType = (String)request.getAttribute("Process");
                if(SUnitName.equals("All")){
                    SProcessType        = "SPINNING AND OE";
                }
            Ut3DateWisePrintNew      sprint = new Ut3DateWisePrintNew();
            UT3DateWiseReportNewPDF  spdf   = new UT3DateWiseReportNewPDF();
            
        %>
        <h2 style="color: #465AA1;">UT3 Details &nbsp;From <%=Common.parseDate((String) request.getAttribute("Fdate"))%> To <%=Common.parseDate((String) request.getAttribute("Tdate"))%></h2>
        <h2 style="color: #465AA1;">Unit : <%=SUnitName%></h2>
        <h2 style="color: #465AA1;">Shift : <%=request.getAttribute("Shift")%></h2>
        <h2 style="color: #465AA1;">Department : <%=request.getAttribute("DeptName")%></h2>
        <h2 style="color: #465AA1;">ProcessType : <%=SProcessType%></h2>
        <table width="100%" border="1" class="ut3report">
            <tr>
                <th align="center" valign="top" scope="col">S.No </th>
                <th align="center" valign="top" scope="col">Test No </th>
                <th align="center" valign="top" scope="col">PartyName</th>
                <th align="center" valign="top" scope="col">Order No </th>
                <th align="center" valign="top" scope="col">Ord.Qty</th>
                <th align="center" valign="top" scope="col">Count</th>
                <th align="center" valign="top" scope="col">Shade</th>
                <th align="center" valign="top" scope="col">Entry Date </th>
                <th align="center" valign="top" scope="col">Department</th>
                <th align="center" valign="top" scope="col">U%</th>
                <th align="center" valign="top" scope="col">Cvw</th>
                <th align="center" valign="top" scope="col">Cvm</th>
                <th align="center" valign="top" scope="col">Remarks</th>
                <th align="center" valign="top" scope="col">Thin30</th>
                <th align="center" valign="top" scope="col">Thin40</th>
                <th align="center" valign="top" scope="col">Thin50</th>
                <th align="center" valign="top" scope="col">Thick35</th>
                <th align="center" valign="top" scope="col">Thick50</th>
                <th align="center" valign="top" scope="col">Neps140</th>
                <th align="center" valign="top" scope="col">Neps200</th>
                <th align="center" valign="top" scope="col">Neps280</th>
                <th align="center" valign="top" scope="col">Total200</th>
                <th align="center" valign="top" scope="col">Total280</th>
                <%
                    if(SUnitName.equals("All"))
                    {%>
                    <th align="center" valign="top" scope="col">Unit</th>
                    <%}%>
            </tr>
            <%
                sprint.createPrn();
                sprint.setHead((String) request.getAttribute("Fdate"), SUnitName,SProcessType,(String)request.getAttribute("Shift"),(String) request.getAttribute("Tdate"),(String)request.getAttribute("DeptName"));
                
                spdf.createPDFFile();
                spdf.setHead((String) request.getAttribute("Fdate"), SUnitName,SProcessType,(String)request.getAttribute("Shift"),(String) request.getAttribute("Tdate"),(String)request.getAttribute("DeptName"));
                spdf.printPDFData(TestUt3list);
                
                for (int i = 0; i < TestUt3list.size(); i++) {
                    HashMap hm = (HashMap)TestUt3list.get(i);

                String sDeptcode   = Common.parseNull((String)hm.get("DEPTCODE"));
                String sOrderno    = Common.parseNull((String)hm.get("ORDERNO"));
                String sSufdeptname= Common.parseNull((String)hm.get("DEPTNAME"));
                String sDeptname   = Common.parseNull((String)hm.get("SUFFDEPTNAME"));
                String sTestid     = Common.parseNull((String)hm.get("TESTID"));
                String sTestdate   = Common.parseNull((String)hm.get("TESTDATE"));
                String sUm         = Common.parseNull((String)hm.get("UM"));
                String sCvUm       = Common.parseNull((String)hm.get("CVUM"));
                String sCvm        = Common.parseNull((String)hm.get("CVM"));
                String sThin30     = Common.parseNull((String)hm.get("THIN30"));
                String sThin40     = Common.parseNull((String)hm.get("THIN40"));
                String sThin50     = Common.parseNull((String)hm.get("THIN50"));
                String sThick35    = Common.parseNull((String)hm.get("THICK35"));
                String sThick50    = Common.parseNull((String)hm.get("THICK50"));
                String sNeps140    = Common.parseNull((String)hm.get("NEPS140"));
                String sNeps200    = Common.parseNull((String)hm.get("NEPS200"));
                String sNeps280    = Common.parseNull((String)hm.get("NEPS280"));
                String sOestatus   = Common.parseNull((String)hm.get("OESTATUS"));
                String sRemarks    = Common.parseNull((String)hm.get("REMARKS"));
                String sCount      = Common.parseNull((String)hm.get("COUNTNAME"));
                String sShade      = Common.parseNull((String)hm.get("SHADE"));
                String sUnit       = Common.parseNull((String)hm.get("UNITNAME"));
                String sPartyName  = Common.parseNull((String)hm.get("PARTYNAME"));
                String sOrderWt    = Common.parseNull((String)hm.get("WEIGHT"));

                double dthin50    = Common.toDouble(sThin50);
                double dthick50   = Common.toDouble(sThick50);
                double dnep200    = Common.toDouble(sNeps200);
                double dnep280    = Common.toDouble(sNeps280);

                double dttnep200  = dthin50+dthick50+dnep200;
                double dttnep280  = dthin50+dthick50+dnep280;

                String sTNeps200   = Common.getRound(Common.parseNull(String.valueOf(dttnep200)),0);
                String sTNeps280   = Common.getRound(Common.parseNull(String.valueOf(dttnep280)),0);
            %>
            <tr>
                <td><%=i + 1%></td>
                <td><%=sTestid%></td>
                <td><%=sPartyName%></td>
                <td><%=sOrderno%></td>
                <td><%=sOrderWt%></td>
                <td><%=sCount%></td>
                <td><%=sShade%></td>
                <td><%=Common.parseDate(sTestdate)%></td>
                <td><%=sDeptname%></td>
                <td><%=Common.getRound(sUm,2)%></td>
                <td><%=Common.getRound(sCvUm,2)%></td>
                <td><%=Common.getRound(sCvm,2)%></td>
                <td><%=sRemarks%></td>
                <td><%=Common.getRound(sThin30,0)%></td>
                <td><%=Common.getRound(sThin40,0)%></td>
                <td><%=Common.getRound(sThin50,0)%></td>
                <td><%=Common.getRound(sThick35,0)%></td>
                <td><%=Common.getRound(sThick50,0)%></td>
                <td><%=Common.getRound(sNeps140,0)%></td>
                <td><%=Common.getRound(sNeps200,0)%></td>
                <td><%=Common.getRound(sNeps280,0)%></td>
                <td><%=sTNeps200%></td>
                <td><%=sTNeps280%></td>
                <%if(SUnitName.equals("All")){%>
                <td><%=sUnit%></td>
                <%}%>
            </tr>
             <%

            String spartyname1="",spartyname2="",spartyname3="",spartyTemp="";
            String sShade1="",sShade2="",sShade3="";
            int linecnt = 1;

            if(sPartyName.length()>27){
                spartyname1 = sPartyName.substring(0,27);
                spartyname2 = sPartyName.substring(27,sPartyName.length());

                //sShade1  = sShade.substring(0,27);
                //sShade2 = sShade.substring(27,sShade.length());

                if(spartyname2.length()>27){

                    spartyTemp = spartyname2.substring(0,27);
                    spartyname3 = spartyname2.substring(27,spartyname2.length());
                    spartyname2 = spartyTemp;

                    //sShade2 = sShade2.substring(0,25);
                    //sShade3 = sShade2.substring(25,sShade2.length());
                }
            }

            if(sPartyName.length()>27){

                //System.out.println("spartyname1-->"+spartyname1);
                //System.out.println("spartyname2-->"+spartyname2);
                //System.out.println("spartyname3-->"+spartyname3);
                linecnt = 1;

                SprnBody = Common.Pad("|" + String.valueOf(i + 1), 4) + "|";
                SprnBody = SprnBody + Common.Rad(sTestid, 5) + "|";
                SprnBody = SprnBody + Common.Pad(spartyname1, 27) + "|";
                SprnBody = SprnBody + Common.Rad(sOrderno, 9) + "|";
                SprnBody = SprnBody + Common.Rad(sOrderWt, 6) + "|";
                SprnBody = SprnBody + Common.Rad(sCount, 3) + "|";
                SprnBody = SprnBody + Common.Pad(" "+sShade, 22) + "|";
                SprnBody = SprnBody + Common.Rad(Common.parseDate(sTestdate), 10) + "|";
                SprnBody = SprnBody + Common.Rad(sDeptname, 7) + "|";
                SprnBody = SprnBody + Common.Rad(Common.getRound(sUm,2), 7) + "|";
                SprnBody = SprnBody + Common.Rad(Common.getRound(sCvUm,2), 7) + "|";
                SprnBody = SprnBody + Common.Rad(Common.getRound(sCvm,2), 7) + "|";
                SprnBody = SprnBody + Common.Rad(sRemarks, 4) + "|";
                SprnBody = SprnBody + Common.Rad(Common.getRound(sThin30,0), 5) + "|";
                SprnBody = SprnBody + Common.Rad(Common.getRound(sThin40,0), 5) + "|";
                SprnBody = SprnBody + Common.Rad(Common.getRound(sThin50,0), 5) + "|";
                SprnBody = SprnBody + Common.Rad(Common.getRound(sThick35,0), 5) + "|";
                SprnBody = SprnBody + Common.Rad(Common.getRound(sThick50,0), 5) + "|";
                SprnBody = SprnBody + Common.Rad(Common.getRound(sNeps140,0), 5) + "|";
                SprnBody = SprnBody + Common.Rad(Common.getRound(sNeps200,0), 5) + "|";
                SprnBody = SprnBody + Common.Rad(Common.getRound(sNeps280,0), 5) + "|";
                SprnBody = SprnBody + Common.Rad(sTNeps200, 6) + "|";

                if(SUnitName.equals("All"))
                {
                    SprnBody = SprnBody + Common.Rad(sTNeps280, 6) + "|";
                    SprnBody = SprnBody + Common.Rad(sUnit, 10) + "|\n";
                }
                else{
                    SprnBody = SprnBody + Common.Rad(sTNeps280, 6) + "|\n";
                }
                linecnt +=1;
                SprnBody = SprnBody + Common.Pad("|" + "", 4) + "|";
                SprnBody = SprnBody + Common.Rad("", 5) + "|";
                SprnBody = SprnBody + Common.Pad(spartyname2, 27) + "|";
                SprnBody = SprnBody + Common.Rad("", 9) + "|";
                SprnBody = SprnBody + Common.Rad("", 6) + "|";
                SprnBody = SprnBody + Common.Rad("", 3) + "|";
                SprnBody = SprnBody + Common.Pad("", 22) + "|";
                SprnBody = SprnBody + Common.Rad("", 10) + "|";
                SprnBody = SprnBody + Common.Rad("", 7) + "|";
                SprnBody = SprnBody + Common.Rad("", 7) + "|";
                SprnBody = SprnBody + Common.Rad("", 7) + "|";
                SprnBody = SprnBody + Common.Rad("", 7) + "|";
                SprnBody = SprnBody + Common.Rad("", 4) + "|";
                SprnBody = SprnBody + Common.Rad("", 5) + "|";
                SprnBody = SprnBody + Common.Rad("", 5) + "|";
                SprnBody = SprnBody + Common.Rad("", 5) + "|";
                SprnBody = SprnBody + Common.Rad("", 5) + "|";
                SprnBody = SprnBody + Common.Rad("", 5) + "|";
                SprnBody = SprnBody + Common.Rad("", 5) + "|";
                SprnBody = SprnBody + Common.Rad("", 5) + "|";
                SprnBody = SprnBody + Common.Rad("", 5) + "|";
                SprnBody = SprnBody + Common.Rad("", 6) + "|";

                if(SUnitName.equals("All"))
                {
                    SprnBody = SprnBody + Common.Rad("", 6) + "|";
                    SprnBody = SprnBody + Common.Rad("", 10) + "|\n";
                }
                else{
                    SprnBody = SprnBody + Common.Rad("", 6) + "|\n";
                }

                if(spartyname3.length()>0){
                    linecnt +=1;

                SprnBody = SprnBody + Common.Pad("|" + "", 4) + "|";
                SprnBody = SprnBody + Common.Rad("", 5) + "|";
                SprnBody = SprnBody + Common.Pad(spartyname3, 27) + "|";
                SprnBody = SprnBody + Common.Rad("", 9) + "|";
                SprnBody = SprnBody + Common.Rad("", 6) + "|";
                SprnBody = SprnBody + Common.Rad("", 3) + "|";
                SprnBody = SprnBody + Common.Pad("", 22) + "|";
                SprnBody = SprnBody + Common.Rad("", 10) + "|";
                SprnBody = SprnBody + Common.Rad("", 7) + "|";
                SprnBody = SprnBody + Common.Rad("", 7) + "|";
                SprnBody = SprnBody + Common.Rad("", 7) + "|";
                SprnBody = SprnBody + Common.Rad("", 7) + "|";
                SprnBody = SprnBody + Common.Rad("", 4) + "|";
                SprnBody = SprnBody + Common.Rad("", 5) + "|";
                SprnBody = SprnBody + Common.Rad("", 5) + "|";
                SprnBody = SprnBody + Common.Rad("", 5) + "|";
                SprnBody = SprnBody + Common.Rad("", 5) + "|";
                SprnBody = SprnBody + Common.Rad("", 5) + "|";
                SprnBody = SprnBody + Common.Rad("", 5) + "|";
                SprnBody = SprnBody + Common.Rad("", 5) + "|";
                SprnBody = SprnBody + Common.Rad("", 5) + "|";
                SprnBody = SprnBody + Common.Rad("", 6) + "|";

                if(SUnitName.equals("All"))
                {
                    SprnBody = SprnBody + Common.Rad("", 6) + "|";
                    SprnBody = SprnBody + Common.Rad("", 10) + "|\n";
                }
                else{
                    SprnBody = SprnBody + Common.Rad("", 6) + "|\n";
                }
                }
            }
            else{
            linecnt=1;

            SprnBody = Common.Pad("|" + String.valueOf(i + 1), 4) + "|";
            SprnBody = SprnBody + Common.Rad(sTestid, 5) + "|";
            SprnBody = SprnBody + Common.Pad(sPartyName, 27) + "|";
            SprnBody = SprnBody + Common.Rad(sOrderno, 9) + "|";
            SprnBody = SprnBody + Common.Rad(sOrderWt, 6) + "|";
            SprnBody = SprnBody + Common.Rad(sCount, 3) + "|";
            SprnBody = SprnBody + Common.Pad(" "+sShade, 22) + "|";
            SprnBody = SprnBody + Common.Rad(Common.parseDate(sTestdate), 10) + "|";
            SprnBody = SprnBody + Common.Rad(sDeptname, 7) + "|";
            SprnBody = SprnBody + Common.Rad(Common.getRound(sUm,2), 7) + "|";
            SprnBody = SprnBody + Common.Rad(Common.getRound(sCvUm,2), 7) + "|";
            SprnBody = SprnBody + Common.Rad(Common.getRound(sCvm,2), 7) + "|";
            SprnBody = SprnBody + Common.Rad(sRemarks, 4) + "|";
            SprnBody = SprnBody + Common.Rad(Common.getRound(sThin30,0), 5) + "|";
            SprnBody = SprnBody + Common.Rad(Common.getRound(sThin40,0), 5) + "|";
            SprnBody = SprnBody + Common.Rad(Common.getRound(sThin50,0), 5) + "|";
            SprnBody = SprnBody + Common.Rad(Common.getRound(sThick35,0), 5) + "|";
            SprnBody = SprnBody + Common.Rad(Common.getRound(sThick50,0), 5) + "|";
            SprnBody = SprnBody + Common.Rad(Common.getRound(sNeps140,0), 5) + "|";
            SprnBody = SprnBody + Common.Rad(Common.getRound(sNeps200,0), 5) + "|";
            SprnBody = SprnBody + Common.Rad(Common.getRound(sNeps280,0), 5) + "|";
            SprnBody = SprnBody + Common.Rad(sTNeps200, 6) + "|";

            if(SUnitName.equals("All"))
            {
                SprnBody = SprnBody + Common.Rad(sTNeps280, 6) + "|";
                SprnBody = SprnBody + Common.Rad(sUnit, 10) + "|\n";
            }
            else{
                SprnBody = SprnBody + Common.Rad(sTNeps280, 6) + "|\n";
            }
            }
            //System.out.println("linecnt-->"+linecnt);
            sprint.printData(SprnBody, (String) request.getAttribute("Fdate"),SUnitName,SProcessType,(String)request.getAttribute("Shift"),(String) request.getAttribute("Tdate"),(String)request.getAttribute("DeptName"),linecnt);
            }
            
            SprnBody="";
            %>
                </table>
            <%
                sprint.Closefile();
        }%>
    </body>
</html>