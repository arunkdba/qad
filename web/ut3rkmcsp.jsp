<%-- 
    Document   : ut3rkmcsp
    Created on : Nov 29, 2018, 5:20:26 PM
    Author     : root
--%>

<%@page import="java.util.HashMap"%>
<%@page import="com.reports.classes.MinAndMaxValue"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.reports.classes.CoEffVarient"%>
<%@page import="com.common.Common"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%
    com.communication.Communication communication = new com.communication.Communication();
    java.util.List theList = communication.getCommunicationOrderNos();
    request.setAttribute("theOrdersList", theList);
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ut3-Rkm-Csp Report</title>
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="css/styles-menu.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.css" type="text/css"/>
        <script src="script/jquery-latest.min.js" type="text/javascript"></script>
        <script src="script/script-menu.js" type="text/javascript"></script>
        <script src="script/jquery-ui.js"></script>
        <script type="text/javascript">

            function ChangeOrderno(typedorderno)
            {
                var typedstring = typedorderno;
                for (var ctr = 0; ctr < document.getElementById("sorderno").options.length; ctr++)
                {
                    var str = document.getElementById("sorderno").options[ctr].value;
                    if (typedstring == str.substring(0, typedstring.length))
                        break;
                }
                if (ctr < document.getElementById("sorderno").options.length)
                    document.getElementById("sorderno").options[ctr].selected = true;
                return;
            }
        </script>
    </head>
    <body>
        <%@include file="header.jsp" %>
        <%!
            java.util.List Orderlist    = new java.util.ArrayList();
            java.util.List Ut3list      = new java.util.ArrayList();
            java.util.List Rkmlist      = new java.util.ArrayList();
            java.util.List CspList      = new java.util.ArrayList();
            java.util.List TpiList      = new java.util.ArrayList();
            java.util.List BreakList    = new java.util.ArrayList();
            java.util.List ListUt3      = new java.util.ArrayList();
            java.util.List ut3Test      = new java.util.ArrayList();
            Common common               = new Common();
            MinAndMaxValue mval = new MinAndMaxValue();

        %>
        <div class="elegant-aero-demo">

            <form action="Ut3rkmcspReport" method="post" class="elegant-aero" style="padding-bottom: 5px;">
                <!--<h1>UT3,RKM,CSP Report<span>Select OrderNo To View The Report</span></h1> -->
                <span id="error"></span>
                <p>
                    <label><span>Order No</span><input id="typetext" name="typetext" placeholder="Valid Order No" type="text" onkeyup="ChangeOrderno(this.value)"></label>
                    <label>
                        <span>&nbsp;</span><select name="sorderno" id="sorderno">
                            <%
                                Orderlist = (java.util.List) request.getAttribute("theOrdersList");
                                for (int i = 0; i < Orderlist.size(); i++) {
                                    //com.reports.classes.SpinningWrappingTestDetails Td = (com.reports.classes.SpinningWrappingTestDetails) Testlist.get(i);
                                    com.communication.CommunicationClass cc = (com.communication.CommunicationClass) Orderlist.get(i);%>
                            <option value="<%=cc.getSorderno()%>"><%=cc.getSorderno()%></option>
                            <% }%>
                        </select></label><BR>
                    <label><span>&nbsp;</span><input class="button" value="Submit" type="submit" id="btnSubmit" name="btnSubmit">
                        <input class="button" value="Reset" type="reset" id="btnreset" name="btnreset">
                    </label>
                </p>
            </form>
        </div>
        <br>
        <%@include  file="ordergeneralinfo.jsp"%>
        <%if (request.getAttribute("theUt3List") != null) {%>
        <br>
        <h2 style="color:#465AA1;">UT3-Details</h2>
        <table width="100%" border="1" class="ut3report">
            <tr>
                <th align="center" valign="top" scope="col">Order No </th>
                <!--<th align="center" valign="top" scope="col">Test No </th>
                <th align="center" valign="top" scope="col">Entry Date </th>!-->
                <th align="center" valign="top" scope="col">U%</th>
                <th align="center" valign="top" scope="col">Thin</th>
                <th align="center" valign="top" scope="col">Thick</th>
                <th align="center" valign="top" scope="col">Neps200</th>
                <th align="center" valign="top" scope="col">Neps280</th>
                <th align="center" valign="top" scope="col">Total200</th>
                <th align="center" valign="top" scope="col">Total280</th>
                <th align="center" valign="top" scope="col">Cvm</th>
                <!--<th align="center" valign="top" scope="col">Remarks</th>-->
            </tr>
            <%
                Ut3list = (java.util.List) request.getAttribute("theUt3List");
                String sDepName ="",sPrevDep="";
                for (int i = 0; i < Ut3list.size(); i++) {
                    //com.reports.classes.PrepcommUt3Details Td = (com.reports.classes.PrepcommUt3Details) Ut3list.get(i);
                    HashMap theMap = (HashMap)Ut3list.get(i);
                    String sdate = common.parseNull(common.parseDate((String)theMap.get("TESTDATE")));
                    String sTestno = common.parseNull((String)theMap.get("TESTNO"));
                    String sorderno = common.parseNull((String)theMap.get("ORDERNO"));
                    String sum = common.parseNull((String)theMap.get("UM"));
                    String scvm = common.parseNull((String)theMap.get("CVM"));
                    String sThin50 = common.parseNull((String)theMap.get("THINMI50"));
                    String sThick50 = common.parseNull((String)theMap.get("THICKPL50"));
                    String sNeps200 = common.parseNull((String)theMap.get("NEPSPL200"));
                    String sNeps280 = common.parseNull((String)theMap.get("NEPSPL280"));
                    String sremarks = common.parseNull((String)theMap.get("REMARKS"));
                    String soe = common.parseNull((String)theMap.get("OESTATUS"));

                    double dthin50    = common.toDouble(sThin50)*2.5;
                    double dthick50   = common.toDouble(sThick50)*2.5;
                    double dnep200    = common.toDouble(sNeps200)*2.5;
                    double dnep280    = common.toDouble(sNeps280)*2.5;
                    double dttnep200  = 0,dttnep280=0;

                    dttnep200  = dthin50+dthick50+dnep200;
                    dttnep280  = dthin50+dthick50+dnep280;

                    String sTNeps200   = common.getRound(String.valueOf(dttnep200),0);
                    String sTNeps280   = common.getRound(String.valueOf(dttnep280),0);

                    sDepName = common.parseNull((String)theMap.get("DEPTNAME"));%>

                  <%if(!sDepName.equals(sPrevDep)){%>
                        <tr style="background: #E2E9C7;">
                            <td colspan="19"><%=sDepName%></td>
                        </tr>
                        <%}%>
                <tr>
                <td><%=sorderno%></td>
                
                <td><%=common.getRound(sum,2)%></td>
                <td><%=common.getRound(dthin50,2)%></td>
                <td><%=common.getRound(dthick50,2)%></td>
                <td><%=common.getRound(dnep200,2)%></td>
                <td><%=common.getRound(dnep280,2)%></td>
                <td><%=sTNeps200%></td>
                <td><%=sTNeps280%></td>
                <td><%=common.getRound(scvm,2)%></td>
                
            </tr>
            <%
                sPrevDep= sDepName;
            }
            %>
            </table>
            <%}%>
            <%if (request.getAttribute("theRkmList") != null) {%>
            <br>
            <h2 style="color: #465AA1;">RKM-Details</h2>
        <table width="100%" border="1" class="ut3report">
            <tr>
                <th align="center" valign="top" scope="col">Order No </th>
                <th align="center" valign="top" scope="col">Test No </th>
                <th align="center" valign="top" scope="col">Entry Date </th>
                <th align="center" valign="top" scope="col">Rkm</th>
                <th align="center" valign="top" scope="col">Rkm CV</th>
                <th align="center" valign="top" scope="col">Elong</th>
                <th align="center" valign="top" scope="col">Elong CV</th>
                <th align="center" valign="top" scope="col">Sys</th>
                <th align="center" valign="top" scope="col">Sys Cv</th>
                <th align="center" valign="top" scope="col">Min RKM</th>
                <th align="center" valign="top" scope="col">Remarks</th>
            </tr>

            <%
                Rkmlist = (java.util.List) request.getAttribute("theRkmList");
                for (int i = 0; i < Rkmlist.size(); i++) {
                    com.reports.classes.PrepcommRkmDetails Td = (com.reports.classes.PrepcommRkmDetails) Rkmlist.get(i);
            %>
            <tr>
                <td><%=common.parseNull(Td.getOrderno())%></td>
                <td><%=common.parseNull(Td.getTestno())%></td>
                <td><%=common.parseDate(Td.getEntrydate())%></td>
                <td><%=common.parseNull(Td.getRkm())%></td>
                <td><%=common.parseNull(Td.getRkmcv())%></td>
                <td><%=common.parseNull(Td.getElg())%></td>
                <td><%=common.parseNull(Td.getElgcv())%></td>
                <td><%=common.parseNull(Td.getSys())%></td>
                <td><%=common.parseNull(Td.getSyscv())%></td>
                <td><%=common.parseNull(Td.getMinrk())%></td>
                <td><%=common.parseNull(Td.getRemarks())%></td>
            </tr>
            <%}%>
            </table>
            <%}%>
            <%if (request.getAttribute("theCspList") != null || request.getAttribute("theFCspList") != null) {%>
            <br>
            <h2 style="color: #465AA1;">CSP-Details</h2>
            <table width="100%" height="27"  border="1" class="ut3report" id="cspdetailstable">
            <tr>
                <th width="42" align="center" valign="top" scope="col">S.No</th>
                <th width="42" align="center" valign="top" scope="col">TestNo</th>
                <th width="42" align="center" valign="top" scope="col">EntryDate</th>
                <th width="113" scope="col" align="center" valign="top">Machine</th>
                <th width="42" align="center" valign="top" scope="col">Type</th>
                <th width="40" align="center" valign="top" scope="col">For</th>
                <th width="82" scope="col" align="center" valign="top">1</th>
                <th width="96" scope="col" align="center" valign="top">2</th>
                <th width="92" scope="col" align="center" valign="top">3</th>
                <th width="89" scope="col" align="center" valign="top">4</th>
                <th width="89" scope="col" align="center" valign="top">5</th>
                <th width="76" scope="col" align="center" valign="top">Avg Value </th>
                <th width="91" scope="col" align="center" valign="top">RHC Value </th>
                <th width="92" scope="col" align="center" valign="top">Cnt.Corr Strength </th>
                <th width="68" scope="col" align="center" valign="top">CV%</th>
                <th width="75" scope="col" align="center" valign="top">Change Advice </th>
                <th width="49" scope="col" align="center" valign="top">Cp</th>
                <th width="41" scope="col" align="center" valign="top">TPI</th>
                <th width="58" scope="col" align="center" valign="top">O.No</th>
                <th width="65" scope="col" align="center" valign="top" >Shade</th>
                <th width="65" scope="col" align="center" valign="top" >Count</th>
                <th width="71" scope="col" align="center" valign="top">Pump</th>
                <th width="63" scope="col" align="center" valign="top">Draft</th>
                <th width="63" scope="col" align="center" valign="top">B.D</th>
                <th width="63" scope="col" align="center" valign="top">QAD REMARKS</th>
            <tr>
                <%
                CspList = (java.util.List) request.getAttribute("theCspList");
                CoEffVarient cev = new CoEffVarient();
                int totrows=0;
                int icnt1=0;

                double dtotcount =0;
                double dtotstr = 0;
                double dtotcsp = 0;
                double dtotcountcv = 0;
                double dtotstrcv = 0;

                int incount =0;
                int instr =0;
                int incsp=0;
                int incountcv = 0;
                int instrcv = 0;

                java.util.List arrTotCntList = new ArrayList();
                java.util.List arrTotStrList = new ArrayList();
                java.util.List arrTotCspList = new ArrayList();

                java.util.List<Double> arrTotCnt= new ArrayList<Double>();
                java.util.List<Double> arrTotStr= new ArrayList<Double>();
                java.util.List<Double> arrTotCsp= new ArrayList<Double>();
                for (int i = 0; i < CspList.size(); i++) {
                    com.reports.classes.PrepcommCspDetails Td = (com.reports.classes.PrepcommCspDetails) CspList.get(i);
                    int iCnt = 0;
                    int iStr = 0;
                    int iCsp = 0;
                    double dcnt1 = common.toDouble(Td.getCnt1());
                    double dcnt2 = common.toDouble(Td.getCnt2());
                    double dcnt3 = common.toDouble(Td.getCnt3());
                    double dcnt4 = common.toDouble(Td.getCnt4());
                    double dcnt5 = common.toDouble(Td.getCnt5());
                    ArrayList<String> arrlistcnt = new ArrayList<String>();
                    if (dcnt1 != 0) {
                        iCnt += 1;
                        arrlistcnt.add(Td.getCnt1());
                        arrTotCntList.add(Td.getCnt1());
                        arrTotCnt.add(dcnt1);
                        icnt1 +=1;
                    }
                    if (dcnt2 != 0) {
                        iCnt += 1;
                        arrlistcnt.add(Td.getCnt2());
                        arrTotCntList.add(Td.getCnt2());                                           
                        arrTotCnt.add(dcnt2);
                        icnt1 +=1;
                    }
                    if (dcnt3 != 0) {
                        iCnt += 1;
                        arrlistcnt.add(Td.getCnt3());
                        arrTotCntList.add(Td.getCnt3());
                        arrTotCnt.add(dcnt3);
                        icnt1 +=1;
                    }
                    if (dcnt4 != 0) {
                        iCnt += 1;
                        arrlistcnt.add(Td.getCnt4());
                        arrTotCntList.add(Td.getCnt4());
                        arrTotCnt.add(dcnt4);
                        icnt1 +=1;
                    }
                    if (dcnt5 != 0) {
                        iCnt += 1;
                        arrlistcnt.add(Td.getCnt5());
                        arrTotCntList.add(Td.getCnt5());
                        arrTotCnt.add(dcnt5);
                        icnt1 +=1;
                    }
                    double dCntAvg = (dcnt1 + dcnt2 + dcnt3 + dcnt4 + dcnt5) / iCnt;
                    dtotcount += (dcnt1 + dcnt2 + dcnt3 + dcnt4+dcnt5);
                    incount += iCnt;
                    String[] CntArray = (String[]) arrlistcnt.toArray(new String[arrlistcnt.size()]);

                    double dsrten1 = common.toDouble(Td.getStrength1());
                    double dsrten2 = common.toDouble(Td.getStrength2());
                    double dsrten3 = common.toDouble(Td.getStrength3());
                    double dsrten4 = common.toDouble(Td.getStrength4());
                    double dsrten5 = common.toDouble(Td.getStrength5());
                    ArrayList<String> arrliststr = new ArrayList<String>();
                    if (dsrten1 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength1());
                        arrTotStrList.add(Td.getStrength1());
                        arrTotStr.add(dsrten1);
                    }
                    if (dsrten2 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength2());
                        arrTotStrList.add(Td.getStrength2());
                        arrTotStr.add(dsrten2);
                    }
                    if (dsrten3 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength3());
                        arrTotStrList.add(Td.getStrength3());
                        arrTotStr.add(dsrten3);
                    }
                    if (dsrten4 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength4());
                        arrTotStrList.add(Td.getStrength4());
                        arrTotStr.add(dsrten4);
                    }
                    if (dsrten5 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength5());
                        arrTotStrList.add(Td.getStrength5());
                        arrTotStr.add(dsrten5);
                    }
                    double dStrenAvg = (dsrten1 + dsrten2 + dsrten3 + dsrten4 + dsrten5) / iStr;
                    dtotstr += (dsrten1 + dsrten2 + dsrten3 + dsrten4 + dsrten5);
                    instr += iStr;
                    String[] StrArray = (String[]) arrliststr.toArray(new String[arrliststr.size()]);
                    
                    double dcsp1 = common.toDouble(Td.getCsp1());
                    double dcsp2 = common.toDouble(Td.getCsp2());
                    double dcsp3 = common.toDouble(Td.getCsp3());
                    double dcsp4 = common.toDouble(Td.getCsp4());
                    double dcsp5 = common.toDouble(Td.getCsp5());
                    ArrayList<String> arrlistcsp = new ArrayList<String>();
                    if (dcsp1 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp1());
                        arrTotCspList.add(Td.getCsp1());
                        arrTotCsp.add(dcsp1);
                    }
                    if (dcsp2 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp2());
                        arrTotCspList.add(Td.getCsp2());
                        arrTotCsp.add(dcsp2);
                    }
                    if (dcsp3 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp3());
                        arrTotCspList.add(Td.getCsp3());
                        arrTotCsp.add(dcsp3);
                    }
                    if (dcsp4 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp4());
                        arrTotCspList.add(Td.getCsp4());
                        arrTotCsp.add(dcsp4);
                    }
                    if (dcsp5 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp5());
                        arrTotCspList.add(Td.getCsp5());
                        arrTotCsp.add(dcsp5);
                    }
                    double dCspAvg = (dcsp1 + dcsp2 + dcsp3 + dcsp4 + dcsp5) / iCsp;
                    dtotcsp += (dcsp1 + dcsp2 + dcsp3 + dcsp4 + dcsp5);
                    incsp += iCsp;
                    String[] CspArray = (String[]) arrlistcsp.toArray(new String[arrlistcsp.size()]);

                    dtotcountcv += common.toDouble(common.getRound(cev.getCoEffVant(CntArray), 2));
                    incountcv += 1;

                    dtotstrcv += common.toDouble(common.getRound(cev.getCoEffVant(StrArray), 2));
                    instrcv   += 1;
                    
                    //System.out.println("dtotcountcv-->"+dtotcountcv);
                    //System.out.println("incountcv-->"+incountcv);
            %>
            <tr class="cnttr">
                <td rowspan="3"><%=i + 1%></td>
                <td rowspan="3"><%=Td.getTestno()%></td>
                <td rowspan="3"><%=common.parseDate(Td.getTestdate())%></td>
                <td rowspan="3"><%=Td.getMechinename()%></td>
                <td rowspan="3"><%=Td.getType()%></td>
                <td>Cnt</td>
                <td class="cntvalue"><%=Td.getCnt1()%></td>
                <td class="cntvalue"><%=Td.getCnt2()%></td>
                <td class="cntvalue"><%=Td.getCnt3()%></td>
                <td class="cntvalue"><%=Td.getCnt4()%></td>
                <td class="cntvalue"><%=common.removeNull(Td.getCnt5())%></td>
                <td><%=common.getRound(dCntAvg, 2)%></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td><%=common.getRound(cev.getCoEffVant(CntArray), 2)%></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr class="strtr">
                <td>Str</td>
                <td class="strvalue"><%=Td.getStrength1()%></td>
                <td class="strvalue"><%=Td.getStrength2()%></td>
                <td class="strvalue"><%=Td.getStrength3()%></td>
                <td class="strvalue"><%=Td.getStrength4()%></td>
                <td class="strvalue"><%=common.removeNull(Td.getStrength5())%></td>
                <td><%=common.getRound(dStrenAvg, 2)%></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td><%=common.getRound(cev.getCoEffVant(StrArray), 2)%></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr class="csptr">
                <td>CSP</td>
                <td class="cspvalue"><%=Td.getCsp1()%></td>
                <td class="cspvalue"><%=Td.getCsp2()%></td>
                <td class="cspvalue"><%=Td.getCsp3()%></td>
                <td class="cspvalue"><%=Td.getCsp4()%></td>
                <td class="cspvalue"><%=common.removeNull(Td.getCsp5())%></td>
                <td><%=common.getRound(dCspAvg, 0)%></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td><%=common.getRound(cev.getCoEffVant(CspArray), 2)%></td>
                <td><%=common.parseNull(Td.getTesttype())%></td>
                <td><%=common.parseNull(Td.getCpwheel())%></td>
                <td><%=common.parseNull(Td.getTpi())%></td>
                <td><%=common.parseNull(Td.getOrderno())%></td>
                <td><%=common.parseNull(Td.getShadename())%></td>
                <td>&nbsp;</td>
                <td><%=common.parseNull(Td.getPumpclr())%></td>
                <td><%=common.parseNull(Td.getDraft())%></td>
                <td><%=common.parseNull(Td.getBdraft())%></td>
                <td><%=common.parseNull(Td.getBqadremarks())%></td>
            </tr>
            <% totrows += 1;
                }
                CspList = (java.util.List) request.getAttribute("theFCspList");
                for(int i=0;i<CspList.size();i++)
                {
                    com.reports.classes.PrepcommCspDetailsFresh Td = (com.reports.classes.PrepcommCspDetailsFresh) CspList.get(i);
                    int iCnt = 0;
                    int iStr = 0;
                    int iCsp = 0;
                    double dcnt1 = common.toDouble(Td.getCnt1());
                    double dcnt2 = common.toDouble(Td.getCnt2());
                    double dcnt3 = common.toDouble(Td.getCnt3());
                    double dcnt4 = common.toDouble(Td.getCnt4());
                    double dcnt5 = common.toDouble(Td.getCnt5());

                    //double dCntAvg = (dcnt1 + dcnt2 + dcnt3 + dcnt4) / 4;

                    double dsrten1 = common.toDouble(Td.getStrength1());
                    double dsrten2 = common.toDouble(Td.getStrength2());
                    double dsrten3 = common.toDouble(Td.getStrength3());
                    double dsrten4 = common.toDouble(Td.getStrength4());
                    double dsrten5 = common.toDouble(Td.getStrength5());
                    //double dStrenAvg = (dsrten1 + dsrten2 + dsrten3 + dsrten4) / 4;

                    double dcsp1 = common.toDouble(Td.getCsp1());
                    double dcsp2 = common.toDouble(Td.getCsp2());
                    double dcsp3 = common.toDouble(Td.getCsp3());
                    double dcsp4 = common.toDouble(Td.getCsp4());
                    double dcsp5 = common.toDouble(Td.getCsp5());
                    //double dCspAvg = (dcsp1 + dcsp2 + dcsp3 + dcsp4) / 4;
                    ArrayList<String> arrlistcnt = new ArrayList<String>();
                    if (dcnt1 != 0) {
                        iCnt += 1;
                        arrlistcnt.add(Td.getCnt1());
                        arrTotCntList.add(Td.getCnt1());
                        arrTotCnt.add(dcnt1);
                    }
                    if (dcnt2 != 0) {
                        iCnt += 1;
                        arrlistcnt.add(Td.getCnt2());
                        arrTotCntList.add(Td.getCnt2());
                        arrTotCnt.add(dcnt2);
                    }
                    if (dcnt3 != 0) {
                        iCnt += 1;
                        arrlistcnt.add(Td.getCnt3());
                        arrTotCntList.add(Td.getCnt3());
                        arrTotCnt.add(dcnt3);
                    }
                    if (dcnt4 != 0) {
                        iCnt += 1;
                        arrlistcnt.add(Td.getCnt4());
                        arrTotCntList.add(Td.getCnt4());
                        arrTotCnt.add(dcnt4);
                    }
                    if (dcnt5 != 0) {
                        iCnt += 1;
                        arrlistcnt.add(Td.getCnt5());
                        arrTotCntList.add(Td.getCnt5());
                        icnt1 +=1;
                    }
                    double dCntAvg = (dcnt1 + dcnt2 + dcnt3 + dcnt4 + dcnt5) / iCnt;
                    dtotcount += (dcnt1 + dcnt2 + dcnt3 + dcnt4+dcnt5);
                    incount += iCnt;
                    String[] CntArray = (String[]) arrlistcnt.toArray(new String[arrlistcnt.size()]);

                    ArrayList<String> arrliststr = new ArrayList<String>();
                    if (dsrten1 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength1());
                        arrTotStrList.add(Td.getStrength1());
                        arrTotStr.add(dsrten1);
                    }
                    if (dsrten2 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength2());
                        arrTotStrList.add(Td.getStrength2());
                        arrTotStr.add(dsrten2);
                    }
                    if (dsrten3 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength3());
                        arrTotStrList.add(Td.getStrength3());
                        arrTotStr.add(dsrten3);
                    }
                    if (dsrten4 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength4());
                        arrTotStrList.add(Td.getStrength4());
                        arrTotStr.add(dsrten4);
                    }
                    if (dsrten5 != 0) {
                        iStr += 1;
                        arrliststr.add(Td.getStrength5());
                        arrTotStrList.add(Td.getStrength5());
                        arrTotStr.add(dsrten5);
                    }
                    double dStrenAvg = (dsrten1 + dsrten2 + dsrten3 + dsrten4 + dsrten5) / iStr;
                    dtotstr += (dsrten1 + dsrten2 + dsrten3 + dsrten4 + dsrten5);
                    instr += iStr;
                    String[] StrArray = (String[]) arrliststr.toArray(new String[arrliststr.size()]);

                    ArrayList<String> arrlistcsp = new ArrayList<String>();
                    if (dcsp1 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp1());
                        arrTotCspList.add(Td.getCsp1());
                        arrTotCsp.add(dcsp1);
                    }
                    if (dcsp2 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp2());
                        arrTotCspList.add(Td.getCsp2());
                        arrTotCsp.add(dcsp2);
                    }
                    if (dcsp3 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp3());
                        arrTotCspList.add(Td.getCsp3());
                        arrTotCsp.add(dcsp3);
                    }
                    if (dcsp4 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp4());
                        arrTotCspList.add(Td.getCsp4());
                        arrTotCsp.add(dcsp4);
                    }
                    if (dcsp5 != 0) {
                        iCsp += 1;
                        arrlistcsp.add(Td.getCsp5());
                        arrTotCspList.add(Td.getCsp5());
                        arrTotCsp.add(dcsp5);
                    }
                    double dCspAvg = (dcsp1 + dcsp2 + dcsp3 + dcsp4 + dcsp5) / iCsp;
                    dtotcsp += (dcsp1 + dcsp2 + dcsp3 + dcsp4 + dcsp5);
                    incsp += iCsp;
                    String[] CspArray = (String[]) arrlistcsp.toArray(new String[arrlistcsp.size()]);
                %>
                <tr class="cnttr">
                <td rowspan="3"><%=totrows + 1%></td>
                <td rowspan="3"><%=Td.getTestno()%></td>
                <td rowspan="3"><%=common.parseDate(Td.getTestdate())%></td>
                <td rowspan="3"><%=Td.getMechinename()%></td>
                <td rowspan="3"><%=Td.getType()%></td>
                <td>Cnt</td>
                <td class="cntvalue"><%=Td.getCnt1()%></td>
                <td class="cntvalue"><%=Td.getCnt2()%></td>
                <td class="cntvalue"><%=Td.getCnt3()%></td>
                <td class="cntvalue"><%=Td.getCnt4()%></td>
                <td class="cntvalue"><%=common.removeNull(Td.getCnt5())%></td>
                <td><%=common.getRound(dCntAvg, 2)%></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td><%=common.getRound(cev.getCoEffVant(CntArray), 2)%></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr class="strtr">
                <td>Str</td>
                <td class="strvalue"><%=Td.getStrength1()%></td>
                <td class="strvalue"><%=Td.getStrength2()%></td>
                <td class="strvalue"><%=Td.getStrength3()%></td>
                <td class="strvalue"><%=Td.getStrength4()%></td>
                <td class="strvalue"><%=common.removeNull(Td.getStrength5())%></td>
                <td><%=common.getRound(dStrenAvg, 2)%></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td><%=common.getRound(cev.getCoEffVant(StrArray), 2)%></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr class="csptr">
                <td>CSP</td>
                <td class="cspvalue"><%=Td.getCsp1()%></td>
                <td class="cspvalue"><%=Td.getCsp2()%></td>
                <td class="cspvalue"><%=Td.getCsp3()%></td>
                <td class="cspvalue"><%=Td.getCsp4()%></td>
                <td class="cspvalue"><%=common.removeNull(Td.getCsp5())%></td>
                <td><%=common.getRound(dCspAvg, 0)%></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td><%=common.getRound(cev.getCoEffVant(CspArray), 2)%></td>
                <td><%=common.parseNull(Td.getTesttype())%></td>
                <td><%=Td.getCpwheel()%></td>
                <td><%=Td.getTpi()%></td>
                <td><%=Td.getOrderno()%></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td><%=Td.getPumpclr()%></td>
                <td><%=Td.getDraft()%></td>
                <td><%=Td.getBdraft()%></td>
                <td><%=Td.getBqadremarks()%></td>
            </tr>                    
            <%}%>                       
            </table>
            
            <!-- Avg -->
            <table width="100%" border="1" class="spinningwrapping" id="minmaxtable">
                <!--<tr>
                    <th colspan="9" scope="col"><h3>Statistical Report </h3></th>
                </tr>-->
                <tr>
                    <th width="11%" scope="col">&nbsp;</th>
                    <th width="9%" scope="col">Avg</th>
                    <th width="9%" scope="col">Min</th>
                    <th width="10%" scope="col">Max</th>                    
                    <!--<th width="12%" scope="col">CV%</th>-->
                </tr>
                <%
                    String[] cntTotArray = (String[]) arrTotCntList.toArray(new String[arrTotCntList.size()]);
                    String[] strTotArray = (String[]) arrTotStrList.toArray(new String[arrTotStrList.size()]);
                    String[] cspTotArray = (String[]) arrTotCspList.toArray(new String[arrTotCspList.size()]);

                    System.out.println("Size-->"+arrTotCnt.size());

                    double cntTot[] = new double[arrTotCnt.size()];
                    double strTot[] = new double[arrTotStr.size()];
                    double cspTot[] = new double[arrTotCsp.size()];

                    for(int i = 0; i < strTot.length; i++){
                        strTot[i] = arrTotStr.get(i);
                    }
                    for(int i = 0; i < cspTot.length; i++){
                        cspTot[i] = arrTotCsp.get(i);
                    }
                    for(int i = 0; i < cntTot.length; i++){
                        cntTot[i] = arrTotCnt.get(i);
                    }
                    //double drhStr = (common.toDouble(common.getRound((dtotcount/incount),2))*common.toDouble(common.getRound((dtotstr/instr),2)));
                %>
                <tr>
                    <td><strong>Count</strong></td>
                    <td align="right" valign="middle"><%=common.getRound((dtotcount/incount),2)%></td>
                    <td align="right" valign="middle" id="mincount" style="background: #b6d101;"><%=common.getRound(mval.getMin(cntTot),3)%></td>
                    <td align="right" valign="middle" id="maxcount" style="background: #FFCECE;"><%=common.getRound(mval.getMax(cntTot),3)%></td>                    
                    <!--<td align="right" valign="middle"><%=common.getRound(cev.getCoEffVant(cntTotArray), 2)%></td>-->
                </tr>

                <tr>
                    <td><strong>Strength</strong></td>
                    <td align="right" valign="middle"><%=common.getRound((dtotstr/instr),2)%></td>
                    <td align="right" valign="middle" id="minstrength" style="background: #b6d101;"><%=common.getRound(mval.getMin(strTot),3)%></td>
                    <td align="right" valign="middle" id="maxstrength" style="background: #FFCECE;"><%=common.getRound(mval.getMax(strTot),3)%></td>                    
                   <!-- <td align="right" valign="middle"><%=common.getRound(cev.getCoEffVant(strTotArray), 2)%></td>-->
                </tr>

                <tr>
                    <td><strong>Csp</strong></td>
                    <td align="right" valign="middle"><%=common.getRound((dtotcsp/incsp),0)%></td>
                    <td align="right" valign="middle" id="mincsp" style="background: #b6d101;"><%=common.getRound(mval.getMin(cspTot),0)%></td>
                    <td align="right" valign="middle" id="maxcsp" style="background: #FFCECE;"><%=common.getRound(mval.getMax(cspTot),0)%></td>
                   <!-- <td align="right" valign="middle"><%=common.getRound(cev.getCoEffVant(cspTotArray), 2)%></td>-->
                </tr>
                <tr>
                    <td><strong>Count Cv</strong></td>
                    <td align="right" valign="middle"><%=common.getRound((dtotcountcv/incountcv),2)%></td>
                    <td align="right" valign="middle">&nbsp;</td>
                    <td align="right" valign="middle">&nbsp;</td>
                </tr>
                <tr>
                    <td><strong>Strength Cv</strong></td>
                    <td align="right" valign="middle"><%=common.getRound((dtotstrcv/instrcv),2)%></td>
                    <td align="right" valign="middle">&nbsp;</td>
                    <td align="right" valign="middle">&nbsp;</td>
                </tr>
        </table>

            <!-- Avg End -->

            <%}%>
            <%if (request.getAttribute("theTpiList") != null) {%>
            <br>
            <h2 style="color: #465AA1;">TPI-Details</h2>
        <table width="100%" border="1" class="ut3report">
            <tr>
                <th scope="col">S.No</th>
                <th scope="col">Test No</th>
                <th scope="col">No of Test</th>
                <th scope="col">Machine</th>
                <th scope="col">Order No</th>
                <th scope="col">Count</th>
                <th scope="col">N.Twist</th>
                <th scope="col">Min Tpi</th>
                <th scope="col">Max Tpi</th>
                <th scope="col">Avg Tpi</th>
                <th scope="col">Rang Tpi</th>
            </tr>
            <%
                TpiList = (java.util.List) request.getAttribute("theTpiList");
                for (int i = 0; i < TpiList.size(); i++) {
                    com.reports.classes.PrepcommTwistTpiDetails ttd = (com.reports.classes.PrepcommTwistTpiDetails)TpiList.get(i);%>
                    <tr>
                    <td><%=i + 1%></td>
                    <td><%=ttd.getTestno()%></td>
                    <td><%=ttd.getTests()%></td>
                    <td><%=ttd.getFrame()%></td>
                    <td><%=ttd.getLot()%></td>
                    <td><%=ttd.getCounttype()%></td>
                    <td><%=ttd.getNomtwist()%></td>
                    <td><%=ttd.getMintpi()%></td>
                    <td><%=ttd.getMaxtpi()%></td>
                    <td><%=ttd.getAvgtpi()%></td>
                    <td><%=ttd.getRangtpi()%></td>
                    </tr>
           <%}%>
        </table>
        <%}%>
        <%if (request.getAttribute("theBreakList") != null) {%>
             <br>
            <h2 style="color: #465AA1;">Breaks - Details</h2>
            <table width="100%" border="1" class="ut3report">
            <tr>
                <th scope="col">Date</th>
                <th scope="col">Order No</th>
                <th scope="col">A/C No</th>
                <th scope="col">Drum</th>
                <th scope="col">Shade</th>
                <th scope="col">Speed</th>
                <th valign="top" colspan="7">BREAK / 100 KM</th>

            </tr>
            <tr>
                <th scope="col">&nbsp;</th>
                <th scope="col">&nbsp;</th>
                <th scope="col">&nbsp;</th>
                <th scope="col">&nbsp;</th>
                <th scope="col">&nbsp;</th>
                <th scope="col">&nbsp;</th>
                <th scope="col">Neps</th>
                <th scope="col">Short</th>
                <th scope="col">Long</th>
                <th scope="col">Thin</th>
                <th scope="col">Off ct</th>
                <th scope="col">FF</th>
                <th scope="col">Total</th>
            </tr>
            <%BreakList = (java.util.List) request.getAttribute("theBreakList");
            String SCountName = "";
                String SData = "";

                double dNeps   = 0;
                double dShort  = 0;
                double dLong   = 0;
                double dThin   = 0;
                double dOffCnt = 0;
                double dEff    = 0;
                double dTotal  = 0;

                double dTot_Neps   = 0;
                double dTot_Short  = 0;
                double dTot_Long   = 0;
                double dTot_Thin   = 0;
                double dTot_OffCnt = 0;
                double dTot_Eff    = 0;
                double dTot_Total  = 0;
                int iNoofOrders = 0;
                for (int i = 0; i < BreakList.size(); i++) {
                    java.util.HashMap theMap = (java.util.HashMap)BreakList.get(i);
                    if(i==0){
                        iNoofOrders = 0;
                        SCountName = common.parseNull((String)theMap.get("COUNTNAME"));%>
                        <tr>
                            <td>Count Name </td>
                            <td colspan="12"><%=SCountName%></td>
                        </tr>
                   <%}%>
                   <%
                dNeps   = common.toDouble(common.getRound((common.toDouble(common.parseNull((String)theMap.get("CN")))/common.toDouble(common.parseNull((String)theMap.get("LEN"))))*100000,2));
                dShort  = common.toDouble(common.getRound((common.toDouble(common.parseNull((String)theMap.get("CS")))/common.toDouble(common.parseNull((String)theMap.get("LEN"))))*100000,2));
                dLong   = common.toDouble(common.getRound((common.toDouble(common.parseNull((String)theMap.get("CL")))/common.toDouble(common.parseNull((String)theMap.get("LEN"))))*100000,2));
                dThin   = common.toDouble(common.getRound((common.toDouble(common.parseNull((String)theMap.get("CT")))/common.toDouble(common.parseNull((String)theMap.get("LEN"))))*100000,2));
                dOffCnt = common.toDouble(common.getRound((common.toDouble(common.parseNull((String)theMap.get("COFFCNT")))/common.toDouble(common.parseNull((String)theMap.get("LEN"))))*100000,2));
                dEff    = common.toDouble(common.getRound((common.toDouble(common.parseNull((String)theMap.get("CVCV")))/common.toDouble(common.parseNull((String)theMap.get("LEN"))))*100000,2));
                dTotal  = common.toDouble(common.getRound((dNeps+dShort+dLong+dThin+dOffCnt+dEff),2));

                dTot_Neps   += dNeps;
                dTot_Short  += dShort;
                dTot_Long   += dLong;
                dTot_Thin   += dThin;
                dTot_OffCnt += dOffCnt;
                dTot_Eff    += dEff;
                dTot_Total  += dTotal;%>
                   <tr>
                       <td><%=common.parseDate(common.parseNull((String)theMap.get("SHIFTDATE")))%></td>
                       <td><%=common.parseNull((String)theMap.get("RORDERNO"))%></td>
                       <td><%=common.parseNull((String)theMap.get("MACHINE_NAME"))%></td>
                       <td><%=common.parseNull((String)theMap.get("SPINDLE_FIRST"))+"-"+common.parseNull((String)theMap.get("SPINDLE_LAST"))%></td>
                       <td><%=common.parseNull((String)theMap.get("SHADE_NAME"))%></td>
                       <td><%=common.parseNull((String)theMap.get("SPEED"))%></td>
                       <td><%=common.getRound(dNeps,2)%></td>
                       <td><%=common.getRound(dShort,2)%></td>
                       <td><%=common.getRound(dLong,2)%></td>
                       <td><%=common.getRound(dThin,2)%></td>
                       <td><%=common.getRound(dOffCnt,2)%></td>
                       <td><%=common.getRound(dEff,2)%></td>
                       <td><%=common.getRound(dTotal,2)%></td>

                  </tr>
                <%
                iNoofOrders++;
                }%>
                <tr>
                        <td colspan="6">Avg</td>
                        <td><%=common.getRound(dTot_Neps/iNoofOrders,2)%></td>
                        <td><%=common.getRound(dTot_Short/iNoofOrders,2)%></td>
                        <td><%=common.getRound(dTot_Long/iNoofOrders,2)%></td>
                        <td><%=common.getRound(dTot_Thin/iNoofOrders,2)%></td>
                        <td><%=common.getRound(dTot_OffCnt/iNoofOrders,2)%></td>
                        <td><%=common.getRound(dTot_Eff/iNoofOrders,2)%></td>
                        <td><%=common.getRound(dTot_Total/iNoofOrders,2)%></td>
                </tr>
            </table>
        <%}%>

        <%if (request.getAttribute("theut3Test") != null) {%>
             <br>
            <h2 style="color: #465AA1;">UT3 - Details</h2>
            <table width="100%" border="1" class="ut3report">
            <tr>
                <th scope="col">S.No</th>
                <th scope="col">Test No</th>
                <th scope="col">Test Date</th>
                <th scope="col">OrderNo</th>
                <th scope="col">DEPTNAME</th>
                <th scope="col">MACHNO</th>
                <th scope="col">MACHSIDE</th>
                <th scope="col">PASSAGE</th>
                <th scope="col">ARTNO</th>
                <th scope="col">FIBREASSEMBLY</th>
                <th scope="col">FIBER</th>
                <th scope="col">VDATA</th>
                <th scope="col">TDATA</th>
                <th scope="col">TESTS</th>
                <th scope="col">SLOT</th>
                <th scope="col">YARNTENSION</th>
                <th scope="col">IMPERFECTIONS</th>
            </tr>
            <%ut3Test = (java.util.List) request.getAttribute("theut3Test");            
            for (int i = 0; i < ut3Test.size(); i++) {
                    java.util.HashMap theMap = (java.util.HashMap)ut3Test.get(i);%>
                <tr>
                    <td><%=i + 1%></td>
                    <td><%=common.parseNull((String)theMap.get("TESTID"))%></td>
                    <td><%=common.parseNull(common.parseDate((String)theMap.get("TESTDATE")))%></td>                    
                    <td><%=common.parseNull(common.parseDate((String)theMap.get("ORDERNO")))%></td>
                    <td><%=common.parseNull(common.parseDate((String)theMap.get("DEPTNAME")))%></td>
                    <td><%=common.parseNull(common.parseDate((String)theMap.get("MACHNO")))%></td>
                    <td><%=common.parseNull(common.parseDate((String)theMap.get("MACHSIDE")))%></td>
                    <td><%=common.parseNull(common.parseDate((String)theMap.get("PASSAGE")))%></td>
                    <td><%=common.parseNull(common.parseDate((String)theMap.get("ARTNO")))%></td>
                    <td><%=common.parseNull(common.parseDate((String)theMap.get("FIBREASSEMBLY")))%></td>
                    <td><%=common.parseNull(common.parseDate((String)theMap.get("FIBER")))%></td>
                    <td><%=common.parseNull(common.parseDate((String)theMap.get("VDATA")))%></td>
                    <td><%=common.parseNull(common.parseDate((String)theMap.get("TDATA")))%></td>
                    <td><%=common.parseNull(common.parseDate((String)theMap.get("TESTS")))%></td>
                    <td><%=common.parseNull(common.parseDate((String)theMap.get("SLOT")))%></td>
                    <td><%=common.parseNull(common.parseDate((String)theMap.get("YARNTENSION")))%></td>
                    <td><%=common.parseNull(common.parseDate((String)theMap.get("IMPERFECTIONS")))%></td>                    
                </tr>
                <%}%>
            </table>
       <%}%>

        <%if (request.getAttribute("theListut3") != null) {%>
             <br>
            <table width="100%" border="1" class="ut3report">
            <tr>
                <th scope="col">S.No</th>
                <th scope="col">Test No</th>
                <th scope="col">Test Date</th>
                <th scope="col">No of Sample</th>
                <th scope="col">OrderNo</th>
                <th scope="col">UM</th>
                <th scope="col">CVM</th>
                <th scope="col">CVM1M</th>
                <th scope="col">CVM3M</th>
                <th scope="col">INDEXX</th>
                <th scope="col">THINMI30</th>
                <th scope="col">THINMI40</th>
                <th scope="col">THINMI50</th>
                <th scope="col">THICKPL35</th>
                <th scope="col">THICKPL50</th>
                <th scope="col">NEPSPL140</th>
                <th scope="col">NEPSPL200</th>
                <th scope="col">NEPSPL280</th>
                <th scope="col">RELCOUNT</th>
            </tr>
            <%ListUt3 = (java.util.List) request.getAttribute("theListut3");
            String sDeptName ="",sPrevDept="";
            for (int i = 0; i < ListUt3.size(); i++) {
                    java.util.HashMap theMap = (java.util.HashMap)ListUt3.get(i);
                        sDeptName = common.parseNull((String)theMap.get("DEPTNAME"));%>
                        <%if(!sDeptName.equals(sPrevDept)){%>
                        <tr style="background: #E2E9C7;">
                            <td colspan="19">DEPARTMENT NAME : <%=sDeptName%></td>                            
                        </tr>
                        <%}%>
                <tr>
                    <td><%=i + 1%></td>
                    <td><%=common.parseNull((String)theMap.get("TESTID"))%></td>
                    <td><%=common.parseNull(common.parseDate((String)theMap.get("TESTDATE")))%></td>
                    <td><%=common.parseNull((String)theMap.get("TESTNO"))%></td>
                    <td><%=common.parseNull((String)theMap.get("ORDERNO"))%></td>
                    <td><%=common.parseNull((String)theMap.get("UM"))%></td>
                    <td><%=common.parseNull((String)theMap.get("CVM"))%></td>
                    <td><%=common.parseNull((String)theMap.get("CVM1M"))%></td>
                    <td><%=common.parseNull((String)theMap.get("CVM3M"))%></td>
                    <td><%=common.parseNull((String)theMap.get("INDEXX"))%></td>
                    <td><%=common.parseNull((String)theMap.get("THINMI30"))%></td>
                    <td><%=common.parseNull((String)theMap.get("THINMI40"))%></td>
                    <td><%=common.parseNull((String)theMap.get("THINMI50"))%></td>
                    <td><%=common.parseNull((String)theMap.get("THICKPL35"))%></td>
                    <td><%=common.parseNull((String)theMap.get("THICKPL50"))%></td>
                    <td><%=common.parseNull((String)theMap.get("NEPSPL140"))%></td>
                    <td><%=common.parseNull((String)theMap.get("NEPSPL200"))%></td>
                    <td><%=common.parseNull((String)theMap.get("NEPSPL280"))%></td>
                    <td><%=common.parseNull((String)theMap.get("RELCOUNT"))%></td>
                </tr>
                <%
                    sPrevDept= sDeptName;
                %>
                <%}%>
            </table>
       <%}%>
       <script>
           $( document ).ready(function() {

             var mincntvalue = checkNaN(parseFloat($('#minmaxtable tr #mincount').html()));
             var maxcntvalue = checkNaN(parseFloat($('#minmaxtable tr #maxcount').html()));

             var minstrvalue = checkNaN(parseFloat($('#minmaxtable tr #minstrength').html()));
             var maxstrvalue = checkNaN(parseFloat($('#minmaxtable tr #maxstrength').html()));

             var mincspvalue = checkNaN(parseFloat($('#minmaxtable tr #mincsp').html()));
             var maxcspvalue = checkNaN(parseFloat($('#minmaxtable tr #maxcsp').html()));

             //alert("minvalue==>"+minvalue);

              //$('#cspdetailstable tr').each(function() {

               $('#cspdetailstable tr').find('.cntvalue').each(function(){
                    var fcnt = checkNaN(parseFloat($(this).html()));
                    //alert("cntvalue==>"+p1);
                    if(fcnt==mincntvalue){
                        $(this).css({
                            "border": "1px solid red",
                            "background": "#b6d101"
                        });
                    }
                    if(fcnt==maxcntvalue){
                        $(this).css({
                            "border": "1px solid red",
                            "background": "#FFCECE"
                        });
                    }
               });

               $('#cspdetailstable tr').find('.strvalue').each(function(){
                    var fstr = checkNaN(parseFloat($(this).html()));
                    //alert("cntvalue==>"+p1);
                    if(fstr==minstrvalue){
                        $(this).css({
                            "border": "1px solid red",
                            "background": "#b6d101"
                        });
                    }
                    if(fstr==maxstrvalue){
                        $(this).css({
                            "border": "1px solid red",
                            "background": "#FFCECE"
                        });
                    }
               });
               $('#cspdetailstable tr').find('.cspvalue').each(function(){
                    var fcsp = checkNaN(parseFloat($(this).html()));
                    //alert("cntvalue==>"+p1);
                    if(fcsp==mincspvalue){
                        $(this).css({
                            "border": "1px solid red",
                            "background": "#b6d101"
                        });
                    }
                    if(fcsp==maxcspvalue){
                        $(this).css({
                            "border": "1px solid red",
                            "background": "#FFCECE"
                        });
                    }
               });
           });
           function checkNaN(val){
              if(isNaN(val)){
                  return 0;
              }
            return val;
          }
       </script>
    </body>
</html>